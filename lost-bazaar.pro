#-------------------------------------------------
#
# Project created by QtCreator 2014-02-08T19:17:32
#
#-------------------------------------------------

QT += core gui network widgets qml opengl openglwidgets

!wasm {
QT += multimedia
}

android {
QT += androidextras
}

COMMON_DATA.path = /assets/data
COMMON_DATA.files = $$files($$PWD/data/*)
INSTALLS += COMMON_DATA

TARGET = lost-bazaar
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++17 -Wall -Wextra -pedantic
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE += -O3
TRANSLATIONS += data/translations/lostbazaar_en.ts \
                data/translations/lostbazaar_hu.ts \
                data/translations/lostbazaar_pl.ts
INCLUDEPATH += ./
INCLUDEPATH += ./src/
SOURCES += \
    src/main.cpp \
    src/mainwindow.cpp \
    src/cfgfile.cpp \
    src/card.cpp \
    src/cardinventory.cpp \
    src/datapackage.cpp \
    src/fileformat.cpp \
    src/random.cpp \
    src/graphicsviewmenu.cpp \
    src/board.cpp \
    src/network.cpp \
    src/slidingstackedwidget.cpp \
    src/fadinglabel.cpp \
    src/audio.cpp \
    src/utils.cpp \
    src/globals.cpp \
    src/gamestate.cpp \
    src/mainbkgwidget.cpp

!wasm {
SOURCES += \
    src/qmediaplaylist.cpp \
    src/qplaylistfileparser.cpp
}

lupdate_only {
SOURCES += data/cards/js/*.js \
           data/jsutils/*.js
}

#LIBS += ./sdk/redistributable_bin/linux64/libsteam_api.so
HEADERS += \
    src/mainwindow.h \
    src/cfgfile.h \
    src/card.h \
    src/cardinventory.h \
    src/datapackage.h \
    src/fileformat.h \
    src/random.h \
    src/graphicsviewmenu.h \
    src/board.h \
    src/network.h \
    src/slidingstackedwidget.h \
    src/fadinglabel.h \
    src/audio.h \
    src/utils.h \
    src/globals.h \
    src/gamestate.h \
    src/mainbkgwidget.h \

!wasm {
HEADERS += \
    src/qmediaplaylist.h \
    src/qmediaplaylist_p.h \
    src/qplaylistfileparser_p.h
}

FORMS += src/mainwindow.ui

DISTFILES += \
    CHANGELOG.txt \
    data/theme.css \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

wasm {
QT_WASM_INITIAL_MEMORY = 603979776
RESOURCES += $$files(*.qrc)
}
