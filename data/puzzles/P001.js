//@include jsutils.board
//@include jsutils.enums

function event(eventData)
{
  if(eventData[0] == "endOfTurn")
  {
    if(gameState.getScore()[0] >= 33)
    {
      menu.showInformation(qsTranslate("Puzzle", "PUZZLE SOLVED"), -1);
      choiceArray = [qsTranslate("Puzzle", "Back to the puzzle selection")]
      enableArray = ["true"];
      var choice = menu.getChoice(choiceArray, enableArray);
      menu.hideInformation();
      if(choice == 0) {gameState.returnToPuzzleScreen();}
    }
    else
    {
      menu.showInformation(qsTranslate("Puzzle", "INCORRECT SOLUTION"), -1);
      choiceArray = [qsTranslate("Puzzle", "Back to the puzzle selection"), qsTranslate("Puzzle", "Retry")]
      enableArray = ["true", "true"];
      var choice = menu.getChoice(choiceArray, enableArray);
      menu.hideInformation();
      if(choice == 0) {gameState.returnToPuzzleScreen();}
      if(choice == 1) {gameState.retryCurrentPuzzle();}
    }
  }
}
