A biztonságos mezőkön a tárgyakat nem lehet elforgatni vagy a szemétdombra küldeni, viszont továbbra is lehetséges őket áthelyezni, kézbe visszavenni, valamint elveszíthetik képességeiket.
Ha már csak egyetlen tárgy maradt a raktárban, egy olyan képességgel, ami tárgyat húz, meggátolhatod az ellenfelet abban, hogy legyen egy utolsó köre.
Ha a kirakandó tárgy pozíciója nem lényeges szempont a képességének, attól még alkalmas lehet arra, hogy az ellenfél Vázájától vagy Vonalzójától mezőket zárjon el.
Ha nem rendelkezel olyan nagy értékű kártyával, amit érdemes lenne egy profit mezőre helyezni, lefoglalhatod egy olyan tárggyal, ami képes áthelyezni magát, mint például a Világos Huszár.
Nem tehetsz le olyan tárgyat a bazárba, aminek nem tudod maradéktalanul teljesíteni a kijátszáskori képességét.
A Seprű egy sokoldalú képességgel rendelkezik, amivel egyaránt lehetséges saját tárgyakat előnyösebb mezőkre helyezni, és ellenfél tárgyait kiszedni egy biztonságos mezőről vagy olyanról, ami már minden oldalán tárgyakkal van körülzárva.
Azon tárgyak kihelyezését, amelyek képesek más tárgyakat elforgatni vagy a szemétdombra helyezni, általában dominó-szabály limitálja, így megsejtheted a fenyegető lépéseket.
A tárgyak áthelyezése a Csipkelegyező legerősebb választható képessége, ezért általában érdemes várni a kihelyezésével addig, amíg a körülmények lehetővé teszik, hogy ezt a képességét hasznosítsd.
Általánosságban igaz, hogy a magasabb értékű lapokat nehezebb megtámadni. Használj Szivacsot, hogy az ellenfél tárgyai sebezhetőek legyenek a Csipkelegyezővel vagy a Mérleggel szemben.
A Tekercsrugóval a saját tárgyaidat is visszaküldheted a kezedbe.
Győződj meg róla, hogy az ellenfél nem halmozott fel Bélyegeket a kezében, mielőtt kiteszed a saját egyedüli Bélyegedet.
A Kávédaráló képes végjátékokat nyerni, de nagy árat fizetsz, ha nem sokkal a kijátszása után baja esik. Próbáld meg elfalazni a tábla sarkában, hogy biztonságban lehessen.
Azok a Szuvenír által kirakott számlálók, amiknek látszólag nincs jelentőségük, később még jól jöhetnek, ha szerzel egy Madáretetőt.
Kattints jobb egérgombbal a Malacperselyedre, hogy vidám röfögést hallasson. Ez kiváló módja annak, hogy nyomatékosan tudasd az ellenféllel, mennyire jót léptél.
Amikor csak lehetséges, a Világos Gyalogot egy mezővel a profit mező alá helyezd, és az aktív képességével foglald el, hogy több pontot érjen.
Ha egy tárgy elhagyja a táblát, visszaáll az eredeti állapotába. Ennek értelmében egy Megafonnal azonnal vissza tudod állítani egy tárgyad elvesztett képességeit.
Ha van egy Vázád vagy Vonalzód egy profit mezőn, Szemüveggel egy kör alatt két tárgyat tudsz vele egy vonalba rakni, ezzel jócskán megnövelve az értékét.
Általánosságban véve első cselekvésként érdemes felhúzni a körönte járó tárgyat, de vannak kivételek. A Jósgömb hatékonyabb, ha még előtte helyezed ki, vagy ha különösen szerencsésnek érzed magad, a Ragasztótubussal visszaküldhetsz előtte egy vágyott tárgyat a raktárba.
A Lombfúvó egy kiváló tárgy a védelem meglazítására. Ha a profit mező már körbe van falazva, egyszerre két oldalát is meg tudja nyitni, így az ellenfél nem tudja a következő körödig az összes rést betömni.
A Védősisak hat más játékosok tárgyaira is. Azzal, ha egy Golfütő vagy Fogaskerék felé helyezed, működésképtelenné tudod tenni.
Használd játék közben a pakli előnézet menüpontot (gyorsbillentyű: Shift + D) hogy könnyen meg tudd állapítani, mennyi van még egy bizonyos tárgyból a pakliban.
A végjáték során lehetőség szerint kerüld el, hogy az elfoglalt profit mezőid mellett körülzárt üres mezők maradjanak. Az ellenfél oda tud tenni egy Fogaskereket vagy Tekercsrugót.
Ha egy pakliban vannak olyan tárgyak, amik az ellenfél kezéből dobatnak, érdemes magadnál tartani egy olyan tárgyat, amivel újakat tudsz húzni.
Az olyan tárgyak, amik képességükkel a saját értéküket növelik, a képességet elvevő tárgyak legfőbb célpontjai szoktak lenni. Azonban ha az értékük 5 fölé ment, már biztonságban vannak a Mérlegtől és a Baseball Ütőtől.
Olyan paklikban, amikben Dobónyíl található, kockázatos kezdésnek számít Irattartóval vagy Szivaccsal elfoglalni a profit mezőt.
A tárgyak hasznossága különböző paklikban némileg eltérhet. A Luxorban a Bélyegek kissé erősebbek a szokásosnál, mivel itt kevés támadó jellegű tárgy tud rájuk hatni. Hasonlóképp érdemes figyelni a Harapófogókra az Űrszemétben, valamint a Szuvenírekre a Neoklasszikusban.
A nyitólépések során lehetőség szerint 2 vagy magasabb alap értékű tárgyakkal foglald el a profit mezőket, különösen, ha nem te vagy a kezdőjátékos, különben egy Csipkelegyező miatt hamar hátrányos helyzetbe kerülhetsz.
Két egyforma tárgyat tartani a kézben az esetek többségében gyengébb stratégiának számít. Egy Kávédaráló vagy egy Olajlámpa segít megszabadulni a duplikátumoktól.
Azáltal, hogy az Üres Konzervdobozt bármelyik másik mezőre át tudod helyezni, gyakorlatilag meg tudod kerülni a dominó ikonos tárgyak korlátját.
Ha nem szerepel nyíl a cselekvő képesség szövegében, a teljes képességet maradéktalanul végre kell hajtanod.