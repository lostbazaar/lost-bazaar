Items on safe tiles cannot be rotated or sent to the trash, but they still can be moved, taken back to hand, and they can lose their abilities.
If there is only one item left in the storage, you can deny your opponent to have a last turn with an ability that draws it from the storage.
If the position of your item to be placed is not important for its abilities, deny tiles from the opponent's Ruler or Vase.
If you do not have a high value card fitting for a profit tile, you can reserve it with an item that is able to move itself with its ability, such as White Knight.
You cannot place an item in the bazaar if you cannot fully execute its entry ability.
The Broom has a versatile ability that is able to move your other items to a more advantageous position, remove your opponent's items from safe tiles or from tiles that are surrounded on all sides.
Items that are able to rotate or send your items to the trash are usually limited by the domino rule, so you can predict the possible threats.
The item moving is the most powerful optional ability of the Lace Fan out of the three, therefore it is usually advised to delay its placement until circumstances allow to use its fullest potential.
Generally items with higher value are harder to attack. You can use the Sponge to make the opponent's items exposed to the Lace Fan or the Scales.
Coil Spring can send back your own items to your hand as well.
Make sure your opponent is not stockpiling Stamps in the hand before placing your own single Stamp on the board.
Coffee Grinder can win endgames, but you pay a high price if it falls apart shortly after you placed it on the board. Try to wall it off in a corner to keep it safe.
Counters placed by a Souvenir which seemingly have no purpose can be still important later when you get a Bird Feeder.
Right-click on your Piggy Bank to make it snort happily. This helps to underline for the opponent how great your last move was.
Whenever possible, place the White Pawn below the empty profit tile and occupy it with its active ability instead to earn more points.
When an item leaves the board, it returns to its original state. Therefore with a Megaphone, you can instantly restore the lost ability of your item.
If you have a Vase or Ruler on a profit tile, with Spectacles you can place two items in line in the same turn to boost its value.
Generally, your first action should be drawing a card from the storage, but there are exceptions. Crystal Ball is more effective to be placed before the mandatory draw, or if you feel particularly lucky, with the Glue Tube, you can return a desired item to the storage before.
The Leaf Blower is an excellent item to loosen up defenses. When a profit tile is already walled off, you can open it from two directions simultaneously, so the opponent will not be able to fill all gaps before your next turn.
The Hard Hat affects other players' item as well. By placing it above a Cog or Golf Club, you can render in nonfunctional.
Use the deck preview (hotkey: Shift + D) anytime during game to help you count how many of a specific item are still in the deck.
During the endgame avoid leaving surrounded empty tiles next to your occupied profit tiles. Your opponent may place a Cog or Coil Spring there.
If there are such items in the deck which can discard items from the hand of the opponent, it is advised to always keep an item that is capable to draw some new ones.
Items that increase their own value are a primary target for disabling abilities. However if their value goes above 5, they will be safe from Scales and Baseball Bat attacks.
In a deck that has Darts, it is considered a risky opening move to place a Sponge or a Document Holder on a profit tile.
The importance of items may change in different decks. In Luxor, Stamps are slighty more powerful than usually, as there are less offensive items that can affect them. Pincers in Space Debris or Souvenirs in Neoclassical are also worth to watch out for.
During the opening moves, prefer taking profit tiles with 2 or higher base value items, especially if you are not the first player in order, otherwise a single Lace Fan can give you a major disadvantage.
Keeping two of the same items in hand is generally considered an inferior strategy. A Coffee Grinder or Oil Lamp can help you to get rid of the duplicates.
By being able to move an Empty Can to any other tile, you can overcome the limitation of your items with domino icon.
If there is no arrow in the description of the active ability, you are obliged to perform all actions in it.