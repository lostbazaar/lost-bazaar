<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>Puzzle</name>
    <message>
        <location filename="../puzzles/P001.js" line="10"/>
        <location filename="../puzzles/P002.js" line="10"/>
        <location filename="../puzzles/P003.js" line="10"/>
        <location filename="../puzzles/P004.js" line="10"/>
        <location filename="../puzzles/P005.js" line="10"/>
        <location filename="../puzzles/P006.js" line="10"/>
        <location filename="../puzzles/P007.js" line="10"/>
        <source>PUZZLE SOLVED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../puzzles/P001.js" line="11"/>
        <location filename="../puzzles/P001.js" line="20"/>
        <location filename="../puzzles/P002.js" line="11"/>
        <location filename="../puzzles/P002.js" line="20"/>
        <location filename="../puzzles/P003.js" line="11"/>
        <location filename="../puzzles/P003.js" line="20"/>
        <location filename="../puzzles/P004.js" line="11"/>
        <location filename="../puzzles/P004.js" line="20"/>
        <location filename="../puzzles/P005.js" line="11"/>
        <location filename="../puzzles/P005.js" line="20"/>
        <location filename="../puzzles/P006.js" line="11"/>
        <location filename="../puzzles/P006.js" line="20"/>
        <location filename="../puzzles/P007.js" line="11"/>
        <location filename="../puzzles/P007.js" line="20"/>
        <source>Back to the puzzle selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../puzzles/P001.js" line="19"/>
        <location filename="../puzzles/P002.js" line="19"/>
        <location filename="../puzzles/P003.js" line="19"/>
        <location filename="../puzzles/P004.js" line="19"/>
        <location filename="../puzzles/P005.js" line="19"/>
        <location filename="../puzzles/P006.js" line="19"/>
        <location filename="../puzzles/P007.js" line="19"/>
        <source>INCORRECT SOLUTION</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../puzzles/P001.js" line="20"/>
        <location filename="../puzzles/P002.js" line="20"/>
        <location filename="../puzzles/P003.js" line="20"/>
        <location filename="../puzzles/P004.js" line="20"/>
        <location filename="../puzzles/P005.js" line="20"/>
        <location filename="../puzzles/P006.js" line="20"/>
        <location filename="../puzzles/P007.js" line="20"/>
        <source>Retry</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QPlatformTheme</name>
    <message>
        <location filename="../../src/main.cpp" line="67"/>
        <source>&amp;Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="68"/>
        <source>&amp;No</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>adhesivebandage</name>
    <message>
        <location filename="../cards/js/adhesivebandage.js" line="137"/>
        <source>Choose an item from the trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/adhesivebandage.js" line="147"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 played the %2 from the trash to %3%4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>antiquewatch</name>
    <message>
        <location filename="../cards/js/antiquewatch.js" line="59"/>
        <source>Choose an item to play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/antiquewatch.js" line="79"/>
        <location filename="../cards/js/antiquewatch.js" line="100"/>
        <location filename="../cards/js/antiquewatch.js" line="121"/>
        <location filename="../cards/js/antiquewatch.js" line="142"/>
        <source>No item in your hand fulfills the criteria!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/antiquewatch.js" line="150"/>
        <source>Choose where to play the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/antiquewatch.js" line="163"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 played the %2 from %3&apos;s hand to %4%5.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/antiquewatch.js" line="167"/>
        <source>%1 cannot use active abilities for the remaining turn.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>baseballbat</name>
    <message>
        <location filename="../cards/js/baseballbat.js" line="66"/>
        <source>Choose target</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/baseballbat.js" line="77"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 disabled %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>beermug</name>
    <message>
        <location filename="../cards/js/beermug.js" line="33"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 gained %4 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/beermug.js" line="37"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 gained 1 counter.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/beermug.js" line="45"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;The %1 has been disabled.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/beermug.js" line="65"/>
        <source>Choose which card would you like to rotate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/beermug.js" line="80"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 cast 6 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/beermug.js" line="81"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 rotated %2%3 %4 by 180°.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/beermug.js" line="86"/>
        <source>You cannot use this ability!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>binoculars</name>
    <message>
        <location filename="../cards/js/binoculars.js" line="165"/>
        <source>Which player&apos;s hand would you like to view?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/binoculars.js" line="187"/>
        <location filename="../cards/js/binoculars.js" line="211"/>
        <source>Choose an item to discard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/binoculars.js" line="250"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 revealed %2&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/binoculars.js" line="251"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 discarded the %2 from %3&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/binoculars.js" line="255"/>
        <source>You have drawn an item!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/binoculars.js" line="262"/>
        <source>The %1 has been disabled.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>birdfeeder</name>
    <message>
        <location filename="../cards/js/birdfeeder.js" line="21"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 gained 3 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/birdfeeder.js" line="51"/>
        <source>Choose which item would you like to place the counter on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/birdfeeder.js" line="73"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 transfered 1 counter from itself to %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/birdfeeder.js" line="80"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 has been rotated toward %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/birdfeeder.js" line="88"/>
        <source>You cannot use this ability!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>board</name>
    <message>
        <location filename="../../src/board.cpp" line="250"/>
        <source>Error: invalid board tile map (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/board.cpp" line="334"/>
        <source>Error: cannot open board data file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/board.cpp" line="532"/>
        <source>Error: there is already a card at the target position (in board::moveCard)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/board.cpp" line="551"/>
        <source>Script error: invalid &apos;from&apos; argument in moveCard, this is probably a bug in the following script:
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/board.cpp" line="590"/>
        <source>Error: two cards are needed for swap (in board::swapCards)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/board.cpp" line="604"/>
        <source>Script error: empty list of targets in tileChoice, this is probably a bug in the following script:
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/board.cpp" line="626"/>
        <source>Error: invalid point (%1,%2) in tileChoice(..)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/board.cpp" line="687"/>
        <source>Error: invalid position in getTileSize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/board.cpp" line="784"/>
        <source>Script error: trying to add a card without an owner to the board, this is probably a bug in the following script:
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/board.cpp" line="789"/>
        <source>Error: there is already a card at the target position (in board::addCardToBoard)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../jsutils/board.js" line="273"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;%1 placed the %2 on %3%4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../jsutils/board.js" line="1443"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;%1 used the active ability of %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>bomb</name>
    <message>
        <location filename="../cards/js/bomb.js" line="24"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 gained 3 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/bomb.js" line="35"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 cast 1 counter.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/bomb.js" line="117"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 has been destroyed.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/bomb.js" line="127"/>
        <location filename="../cards/js/dart.js" line="241"/>
        <location filename="../cards/js/envelope.js" line="115"/>
        <location filename="../cards/js/fryingpan.js" line="98"/>
        <location filename="../cards/js/hammer.js" line="123"/>
        <location filename="../cards/js/handmirror.js" line="319"/>
        <location filename="../cards/js/handmirror.js" line="921"/>
        <location filename="../cards/js/scissors.js" line="159"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;The %1 destroyed %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>broom</name>
    <message>
        <location filename="../cards/js/broom.js" line="155"/>
        <source>Choose target</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/broom.js" line="161"/>
        <source>Choose where would you like to move the selected item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/broom.js" line="179"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 moved %2%3 %4 to %5%6.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/broom.js" line="186"/>
        <source>The %1 has been disabled.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>buzzsaw</name>
    <message>
        <location filename="../cards/js/buzzsaw.js" line="42"/>
        <location filename="../cards/js/buzzsaw.js" line="53"/>
        <location filename="../cards/js/buzzsaw.js" line="64"/>
        <location filename="../cards/js/buzzsaw.js" line="75"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 discarded the %2 from %3&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/buzzsaw.js" line="84"/>
        <source>The %1 has been disabled.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>card</name>
    <message>
        <location filename="../../src/card.cpp" line="892"/>
        <source>Script error: trying to change owner of card to invalid player, this is probably a bug in the following script:
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/card.cpp" line="1049"/>
        <source>Error: duplicate card UID, game out of sync or script error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/card.cpp" line="1075"/>
        <source>Error: cannot open script file: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>cardInventory</name>
    <message>
        <location filename="../../src/cardinventory.cpp" line="64"/>
        <source>Script error: trying to to add a card to an inventory but the card is already present in another inventory, this is probably a bug in the following script:
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/cardinventory.cpp" line="101"/>
        <source>Script error: empty card list in cardInventory::cardChoice, this is probably a bug in the following script:
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/cardinventory.cpp" line="172"/>
        <source>Script error: trying to move a card with an invalid owner to the board, this is probably a bug in the following script:
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/cardinventory.cpp" line="200"/>
        <source>Storage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/cardinventory.cpp" line="216"/>
        <source>Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/cardinventory.cpp" line="494"/>
        <source>Choose which item you want to trade with the Huckster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/cardinventory.cpp" line="521"/>
        <source>%1 discarded %2 at the end of the turn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/cardinventory.cpp" line="717"/>
        <location filename="../../src/cardinventory.cpp" line="752"/>
        <source>Invalid player in cardRightClicked(...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/cardinventory.cpp" line="723"/>
        <source>Invalid card in cardRightClicked(...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/cardinventory.cpp" line="777"/>
        <source>Huckster</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>celticshield</name>
    <message>
        <location filename="../cards/js/celticshield.js" line="46"/>
        <source>Choose an item to discard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/celticshield.js" line="75"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 discarded the %2 from %3&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>coffeegrinder</name>
    <message>
        <location filename="../cards/js/coffeegrinder.js" line="124"/>
        <source>Choose an item to discard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/coffeegrinder.js" line="63"/>
        <source>Choose the first item to discard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/coffeegrinder.js" line="68"/>
        <source>Choose the second item to discard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/coffeegrinder.js" line="108"/>
        <location filename="../cards/js/coffeegrinder.js" line="109"/>
        <location filename="../cards/js/coffeegrinder.js" line="178"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 discarded the %2 from %3&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/coffeegrinder.js" line="113"/>
        <location filename="../cards/js/coffeegrinder.js" line="182"/>
        <source>You have drawn an item!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/coffeegrinder.js" line="189"/>
        <source>No items in hand to discard!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>cog</name>
    <message>
        <location filename="../cards/js/cog.js" line="24"/>
        <source>%1 cannot use active abilities for the remaining turn.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/cog.js" line="33"/>
        <source>Choose the direction for rotating the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/cog.js" line="37"/>
        <source>Rotate counter-clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/cog.js" line="38"/>
        <source>Rotate clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/cog.js" line="62"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 has been rotated by 90° counter-clockwise.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/cog.js" line="73"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 has been rotated by 90° clockwise.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/cog.js" line="92"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 rotated %2%3 %4 by 90° clockwise.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/cog.js" line="104"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 rotated %2%3 %4 by 90° counter-clockwise.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>coilspring</name>
    <message>
        <location filename="../cards/js/coilspring.js" line="22"/>
        <source>%1 cannot use active abilities for the remaining turn.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/coilspring.js" line="59"/>
        <source>Choose target</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/coilspring.js" line="80"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 returned %2%3 %4 to %5&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/coilspring.js" line="81"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 has been disabled.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/coilspring.js" line="86"/>
        <source>No item is selectable for ability!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>compasses</name>
    <message>
        <location filename="../cards/js/compasses.js" line="111"/>
        <source>Choose target</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/compasses.js" line="133"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 rotated %2%3 %4 towards %5.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>crystalball</name>
    <message>
        <location filename="../cards/js/crystalball.js" line="58"/>
        <source>You already have the most items in hand!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/crystalball.js" line="49"/>
        <source>You have drawn %1 items!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/crystalball.js" line="53"/>
        <source>You have drawn an item!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/crystalball.js" line="131"/>
        <source>Which player&apos;s item would you like to view?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/crystalball.js" line="157"/>
        <source>You revealed this item:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/crystalball.js" line="158"/>
        <source>%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/crystalball.js" line="167"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 revealed the %2 in %3&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/crystalball.js" line="178"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 revealed an item in %2&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/crystalball.js" line="183"/>
        <source>No items to reveal!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dart</name>
    <message>
        <location filename="../cards/js/dart.js" line="203"/>
        <source>Choose target</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/dart.js" line="216"/>
        <source>%1%2 %3: %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/dart.js" line="220"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;%1 rolled a die: %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/dart.js" line="229"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 destroyed %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/dart.js" line="255"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 has been moved to %2%3.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/dart.js" line="262"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 has been destroyed.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dictionary</name>
    <message>
        <location filename="../cards/js/dictionary.js" line="200"/>
        <source>Choose which card would you like to rotate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/dictionary.js" line="242"/>
        <source>Toward who would you like to rotate the chosen item?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/dictionary.js" line="299"/>
        <location filename="../cards/js/dictionary.js" line="304"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;%1%2 %3 has been rotated towards %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>documentholder</name>
    <message>
        <location filename="../cards/js/documentholder.js" line="47"/>
        <source>You have drawn an item!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>emptycan</name>
    <message>
        <location filename="../cards/js/emptycan.js" line="34"/>
        <source>Choose an item to discard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/emptycan.js" line="57"/>
        <source>Choose where to move the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/emptycan.js" line="79"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 discarded the %2.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/emptycan.js" line="80"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 has been moved to %2%3.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/emptycan.js" line="85"/>
        <source>You cannot move this item!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/emptycan.js" line="90"/>
        <source>No items in hand to discard!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/emptycan.js" line="95"/>
        <source>Nowhere to move!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>envelope</name>
    <message>
        <location filename="../cards/js/envelope.js" line="25"/>
        <source>%1 cannot use active abilities for the remaining turn.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/envelope.js" line="61"/>
        <source>Choose from the abilities of the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/envelope.js" line="63"/>
        <location filename="../cards/js/handmirror.js" line="869"/>
        <source>Destroy an adjacent item you own</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/envelope.js" line="64"/>
        <location filename="../cards/js/handmirror.js" line="870"/>
        <source>Swap tiles with an adjacent item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/envelope.js" line="87"/>
        <source>Choose an item to trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/envelope.js" line="130"/>
        <source>Choose which item would you like to move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/envelope.js" line="95"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 destroyed %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/envelope.js" line="24"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 gained 3 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/envelope.js" line="105"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 gained %2 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/envelope.js" line="140"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 cast %2 counter.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/envelope.js" line="144"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 cast %2 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/envelope.js" line="146"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 switched tiles with %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/envelope.js" line="154"/>
        <source>You cannot use this ability!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>eraser</name>
    <message>
        <location filename="../cards/js/eraser.js" line="52"/>
        <source>Choose target (click on the %1 to dismiss it)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/eraser.js" line="67"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 disabled %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/eraser.js" line="72"/>
        <source>No card is selectable for ability!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>fakepainting</name>
    <message>
        <location filename="../cards/js/fakepainting.js" line="39"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 revealed %4&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/fakepainting.js" line="64"/>
        <source>Choose a wooden item to discard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/fakepainting.js" line="146"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 discarded the %2 from %3&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/fakepainting.js" line="147"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 has been returned to %2&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>fishingrod</name>
    <message>
        <location filename="../cards/js/fishingrod.js" line="27"/>
        <source>The %1 lost its abilities.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/fishingrod.js" line="73"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 returned to %4&apos;s hand.&lt;/khaki&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/fishingrod.js" line="74"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 returned %4%5 %6 to %7&apos;s hand.&lt;/khaki&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>flag</name>
    <message>
        <location filename="../cards/js/flag.js" line="67"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 rotated %4%5 %6 by 180°.&lt;/khaki&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/flag.js" line="71"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 has been disabled.&lt;/khaki&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>flyswatter</name>
    <message>
        <location filename="../cards/js/flyswatter.js" line="89"/>
        <source>Choose target</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/flyswatter.js" line="110"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 destroyed %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>football</name>
    <message>
        <location filename="../cards/js/football.js" line="61"/>
        <source>You have drawn 2 items!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>fryingpan</name>
    <message>
        <location filename="../cards/js/fryingpan.js" line="74"/>
        <source>%1%2 %3: %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/fryingpan.js" line="77"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;%1 rolled a die: %2.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/fryingpan.js" line="86"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 destroyed %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/fryingpan.js" line="114"/>
        <source>%1 cannot use active abilities for the remaining turn.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>gameState</name>
    <message>
        <location filename="../../src/gamestate.cpp" line="309"/>
        <location filename="../../src/gamestate.cpp" line="1307"/>
        <source>Turn started&lt;br&gt;&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;b&gt;Turn of %1&lt;/b&gt;&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="533"/>
        <source>%1 drew an item (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="534"/>
        <source>%1 drew an item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="979"/>
        <source>End of turn confirmation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="956"/>
        <source>You have too many items in your hand.
To end your turn, you need to send some to the trash. Select which ones by right clicking on them.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="956"/>
        <location filename="../../src/gamestate.cpp" line="958"/>
        <source>Too many items in hand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="958"/>
        <source>You have too many items in your hand.
To end your turn, you need to send some to the trash. Select which ones by keeping your finger on them for a few seconds.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="964"/>
        <source>You can&apos;t go under 5 items by throwing items into the trash at the end of the turn.
Unmark some of the items which you have marked for throwing away.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="964"/>
        <source>Too few items in hand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="979"/>
        <source>Are you sure you want to end your turn without placing an item?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="1017"/>
        <source>Error: invalid player in endTurnButtonClicked(...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="1268"/>
        <source>Invalid player number in isLocalUser(...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="1360"/>
        <source>Back to main menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="1363"/>
        <source>Back to tutorial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="1495"/>
        <source>%1 (UID: %2)
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="1499"/>
        <source>%1 (UID: %2), locations: %3
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="1504"/>
        <source>The game is in an inconsistent state. You may continue, but it will not end well.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="1507"/>
        <source>The following cards are not in any inventory or on the board:
%1They are forever lost to the void.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="1511"/>
        <source>The following cards appear in multiple places:
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="1513"/>
        <source>Please submit a bug report.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="1771"/>
        <source>Error: invalid player in currentPlayerCanEndTurn(...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="1786"/>
        <source>Missing file: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>gardenspade</name>
    <message>
        <location filename="../cards/js/gardenspade.js" line="123"/>
        <location filename="../cards/js/gardenspade.js" line="131"/>
        <source>Choose from the abilities of the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gardenspade.js" line="125"/>
        <source>Return the target and an item from the trash to the storage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gardenspade.js" line="126"/>
        <source>Draw an item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gardenspade.js" line="133"/>
        <source>Return the target and %1 items from the trash to the storage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gardenspade.js" line="107"/>
        <source>Choose target</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gardenspade.js" line="134"/>
        <source>Draw %1 items and discard %2 of them</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gardenspade.js" line="139"/>
        <source>Not enough items in trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gardenspade.js" line="139"/>
        <source>Not enough items in storage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gardenspade.js" line="150"/>
        <source>Choose an item to recycle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gardenspade.js" line="159"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 recycled the %2.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gardenspade.js" line="164"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 returned %2%3 %4 to the storage.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gardenspade.js" line="219"/>
        <source>Choose an item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gardenspade.js" line="237"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 sent the %2 from the storage to the trash.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gardenspade.js" line="247"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 drew the %2 from the storage.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gardenspade.js" line="248"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 drew an item from the storage.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>giftbox</name>
    <message>
        <location filename="../cards/js/giftbox.js" line="121"/>
        <source>Toward who would you like to rotate the %1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/giftbox.js" line="151"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 has been rotated towards %2.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/giftbox.js" line="191"/>
        <source>Choose target (click on the %1 to dismiss it)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/giftbox.js" line="207"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 returned %2%3 %4 to %5&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/giftbox.js" line="212"/>
        <source>You have drawn an item!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/giftbox.js" line="238"/>
        <source>No item in the storage!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/giftbox.js" line="243"/>
        <source>No item is selectable for ability!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/giftbox.js" line="262"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 has been destroyed.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/giftbox.js" line="267"/>
        <source>You have drawn 2 items!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>global</name>
    <message>
        <location filename="../../src/fileformat.cpp" line="41"/>
        <source>Error: cannot open file for writing: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/fileformat.cpp" line="126"/>
        <source>Error: cannot open file for reading: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/fileformat.cpp" line="140"/>
        <source>Error: invalid file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/fileformat.cpp" line="151"/>
        <source>Error: invalid file or wrong file version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/fileformat.cpp" line="174"/>
        <source>XML error: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gamestate.cpp" line="710"/>
        <source>Error: scriptEngine has uncaught exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/globals.cpp" line="49"/>
        <location filename="../../src/globals.cpp" line="67"/>
        <source>Error: requested file or path does not exist: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="152"/>
        <source>Error: cannot open stylesheet file (%1)!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>gluetube</name>
    <message>
        <location filename="../cards/js/gluetube.js" line="57"/>
        <source>Choose from the abilities of the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gluetube.js" line="59"/>
        <location filename="../cards/js/handmirror.js" line="1017"/>
        <source>Return an item from the trash to the storage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gluetube.js" line="60"/>
        <location filename="../cards/js/handmirror.js" line="1018"/>
        <source>Reclaim an item to your hand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gluetube.js" line="63"/>
        <location filename="../cards/js/handmirror.js" line="1021"/>
        <source>No item with positive value in trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gluetube.js" line="63"/>
        <location filename="../cards/js/handmirror.js" line="1021"/>
        <source>Not enough counters / No reclaimable item in the trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gluetube.js" line="72"/>
        <source>Choose an item to recycle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gluetube.js" line="105"/>
        <source>Choose an item to reclaim</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gluetube.js" line="127"/>
        <source>No items in trash fulfill the criteria!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gluetube.js" line="132"/>
        <source>No items in trash!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gluetube.js" line="89"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 recycled the %2.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gluetube.js" line="92"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 gained %2 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gluetube.js" line="96"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 gained 1 counter.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gluetube.js" line="103"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 cast 11 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/gluetube.js" line="121"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 returned the %2 to %3&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>goldencup</name>
    <message>
        <location filename="../cards/js/goldencup.js" line="110"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 gained %2 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/goldencup.js" line="114"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 gained 1 counter.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>golfclub</name>
    <message>
        <location filename="../cards/js/golfclub.js" line="52"/>
        <source>Choose which item should be taken to hand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/golfclub.js" line="73"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 has been rotated by 90° counter-clockwise.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/golfclub.js" line="103"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 returned %2%3 %4 to %5&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/golfclub.js" line="108"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 returned %2%3 %4 to the storage.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/golfclub.js" line="114"/>
        <source>No item is selectable for ability!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>graphicsViewMenu</name>
    <message>
        <location filename="../../src/graphicsviewmenu.cpp" line="256"/>
        <source>It&apos;s a draw!&lt;br&gt;What would you like to do?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/graphicsviewmenu.cpp" line="260"/>
        <source>%1 won the game!&lt;br&gt;What would you like to do?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/graphicsviewmenu.cpp" line="264"/>
        <source>%1 and %2 won the game!&lt;br&gt;What would you like to do?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/graphicsviewmenu.cpp" line="270"/>
        <source>%1, %2 and %3 won the game!&lt;br&gt;What would you like to do?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/graphicsviewmenu.cpp" line="277"/>
        <source>%1, %2, %3 and %4 won the game!&lt;br&gt;What would you like to do?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/graphicsviewmenu.cpp" line="287"/>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/graphicsviewmenu.h" line="64"/>
        <location filename="../../src/graphicsviewmenu.h" line="69"/>
        <source>Back to main menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/graphicsviewmenu.cpp" line="316"/>
        <source>Error: invalid team (%1) in declareWinnerTeam(..)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>hammer</name>
    <message>
        <location filename="../cards/js/hammer.js" line="88"/>
        <source>Choose target</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/hammer.js" line="111"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 destroyed %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>handmirror</name>
    <message>
        <location filename="../cards/js/handmirror.js" line="424"/>
        <location filename="../cards/js/handmirror.js" line="488"/>
        <location filename="../cards/js/handmirror.js" line="960"/>
        <location filename="../cards/js/handmirror.js" line="1283"/>
        <location filename="../cards/js/handmirror.js" line="1471"/>
        <location filename="../cards/js/handmirror.js" line="1740"/>
        <location filename="../cards/js/handmirror.js" line="1854"/>
        <location filename="../cards/js/handmirror.js" line="1859"/>
        <location filename="../cards/js/handmirror.js" line="1987"/>
        <location filename="../cards/js/handmirror.js" line="2491"/>
        <source>You cannot use this ability!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="46"/>
        <location filename="../cards/js/handmirror.js" line="373"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 gained %4 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="50"/>
        <location filename="../cards/js/handmirror.js" line="369"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 gained 1 counter.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="70"/>
        <location filename="../cards/js/handmirror.js" line="198"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 cast 1 counter.&lt;font color=&quot;khaki&quot;&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="117"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 returned to %4&apos;s hand.&lt;/khaki&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="118"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 returned %4%5 %6 to %7&apos;s hand.&lt;/khaki&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="182"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 rotated %4%5 %6 by 180°.&lt;/khaki&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="186"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 has been disabled.&lt;/khaki&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="206"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 has been returned to %4&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="271"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 destroyed %4%5 %6.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="280"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;The %1 has been moved to %2%3.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="335"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;The %1 cast 1 counter.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="340"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;The %1 cast %2 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="364"/>
        <source>%1%2 %3: %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="366"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1 rolled a die: %2.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="418"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 cast 6 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="419"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 rotated %2%3 %4 by 180°.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="451"/>
        <source>Choose which item would you like to place the counter on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="473"/>
        <location filename="../cards/js/handmirror.js" line="1841"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 transfered 1 counter from itself to %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="573"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 has been destroyed.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="583"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;The %1 destroyed %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="603"/>
        <location filename="../cards/js/handmirror.js" line="768"/>
        <location filename="../cards/js/handmirror.js" line="1484"/>
        <location filename="../cards/js/handmirror.js" line="1925"/>
        <source>Choose an item to discard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="657"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 discarded the %2 from %3&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="668"/>
        <location filename="../cards/js/handmirror.js" line="824"/>
        <location filename="../cards/js/handmirror.js" line="1574"/>
        <source>No items in hand to discard!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="683"/>
        <source>Rotate counter-clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="661"/>
        <location filename="../cards/js/handmirror.js" line="2689"/>
        <location filename="../cards/js/handmirror.js" line="2722"/>
        <source>You have drawn an item!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="403"/>
        <source>Choose which item would you like to rotate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="480"/>
        <location filename="../cards/js/handmirror.js" line="1848"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 has been rotated toward %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="679"/>
        <location filename="../cards/js/handmirror.js" line="1996"/>
        <source>Choose the direction for rotating the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="684"/>
        <source>Rotate clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="708"/>
        <location filename="../cards/js/handmirror.js" line="1140"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 has been rotated by 90° counter-clockwise.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="719"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 has been rotated by 90° clockwise.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="738"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 rotated %2%3 %4 by 90° clockwise.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="750"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 rotated %2%3 %4 by 90° counter-clockwise.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="791"/>
        <location filename="../cards/js/handmirror.js" line="1272"/>
        <location filename="../cards/js/handmirror.js" line="1785"/>
        <location filename="../cards/js/handmirror.js" line="2277"/>
        <location filename="../cards/js/handmirror.js" line="2461"/>
        <location filename="../cards/js/handmirror.js" line="2747"/>
        <source>Choose where to move the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="829"/>
        <location filename="../cards/js/handmirror.js" line="2312"/>
        <location filename="../cards/js/handmirror.js" line="2778"/>
        <source>Nowhere to move!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1181"/>
        <location filename="../cards/js/handmirror.js" line="1982"/>
        <location filename="../cards/js/handmirror.js" line="2557"/>
        <source>No item is selectable for ability!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1223"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 restored the abilities of %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1351"/>
        <source>Which player&apos;s hand would you like to view?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1456"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 disabled the %2 and played it from %3&apos;s hand to %4%5.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1402"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 revealed %2&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1460"/>
        <source>No metal items in this hand!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="819"/>
        <location filename="../cards/js/handmirror.js" line="2347"/>
        <location filename="../cards/js/handmirror.js" line="2783"/>
        <source>You cannot move this item!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1432"/>
        <source>Choose the tile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1512"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 cast 2 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1539"/>
        <source>Choose an item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2138"/>
        <location filename="../cards/js/handmirror.js" line="2263"/>
        <source>%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1565"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 drew the %2 from the storage.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1566"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 drew an item from the storage.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1642"/>
        <source>There is no adjacent empty tile!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="813"/>
        <location filename="../cards/js/handmirror.js" line="1270"/>
        <location filename="../cards/js/handmirror.js" line="1513"/>
        <location filename="../cards/js/handmirror.js" line="1956"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 discarded the %2.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="58"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;The %1 has been disabled.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="814"/>
        <location filename="../cards/js/handmirror.js" line="1278"/>
        <location filename="../cards/js/handmirror.js" line="1798"/>
        <location filename="../cards/js/handmirror.js" line="2308"/>
        <location filename="../cards/js/handmirror.js" line="2474"/>
        <location filename="../cards/js/handmirror.js" line="2773"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 has been moved to %2%3.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="901"/>
        <location filename="../cards/js/handmirror.js" line="1730"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 destroyed %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="911"/>
        <location filename="../cards/js/handmirror.js" line="1050"/>
        <location filename="../cards/js/handmirror.js" line="2619"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 gained %2 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="946"/>
        <location filename="../cards/js/handmirror.js" line="2437"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 cast %2 counter.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="950"/>
        <location filename="../cards/js/handmirror.js" line="2441"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 cast %2 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="952"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 switched tiles with %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="974"/>
        <location filename="../cards/js/handmirror.js" line="2707"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 has been destroyed.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1015"/>
        <location filename="../cards/js/handmirror.js" line="2589"/>
        <source>Choose from the abilities of the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1047"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 recycled the %2.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1054"/>
        <location filename="../cards/js/handmirror.js" line="2623"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 gained 1 counter.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1061"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 cast 11 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1079"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 returned the %2 to %3&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1085"/>
        <source>No items in trash fulfill the criteria!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1090"/>
        <source>No items in trash!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1170"/>
        <location filename="../cards/js/handmirror.js" line="2551"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 returned %2%3 %4 to %5&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1175"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 returned %2%3 %4 to the storage.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1119"/>
        <location filename="../cards/js/handmirror.js" line="2530"/>
        <source>Choose which item should be taken to hand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2137"/>
        <location filename="../cards/js/handmirror.js" line="2262"/>
        <source>You revealed this item:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2163"/>
        <location filename="../cards/js/handmirror.js" line="2342"/>
        <source>No items to reveal!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="867"/>
        <location filename="../cards/js/handmirror.js" line="2386"/>
        <source>Select an ability</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="893"/>
        <source>Choose an item to trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1030"/>
        <source>Choose an item to recycle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1063"/>
        <source>Choose an item to reclaim</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1659"/>
        <location filename="../cards/js/handmirror.js" line="2423"/>
        <source>Choose where would you like to move the selected item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1668"/>
        <location filename="../cards/js/handmirror.js" line="2443"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 moved %2%3 %4 to %5%6.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1715"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 removed 1 counter from %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1819"/>
        <source>Choose on which item would you like to place a counter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1975"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 disabled %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2021"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 have been rotated by 90° counter-clockwise.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2032"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 have been rotated by 180°.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2043"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 have been rotated by 90° clockwise.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2111"/>
        <location filename="../cards/js/handmirror.js" line="2236"/>
        <source>Which player&apos;s item would you like to view?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2147"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 revealed the %2 in %3&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2158"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 revealed an item in %2&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2301"/>
        <location filename="../cards/js/handmirror.js" line="2330"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 revealed an item of %2 value in %3&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2302"/>
        <location filename="../cards/js/handmirror.js" line="2306"/>
        <location filename="../cards/js/handmirror.js" line="2331"/>
        <location filename="../cards/js/handmirror.js" line="2335"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 revealed %2 in %3&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2389"/>
        <source>Move an adjacent item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="936"/>
        <location filename="../cards/js/handmirror.js" line="2416"/>
        <source>Choose which item would you like to move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1678"/>
        <location filename="../cards/js/handmirror.js" line="2454"/>
        <location filename="../cards/js/handmirror.js" line="2485"/>
        <source>The %1 has been disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2473"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 cast 5 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1224"/>
        <location filename="../cards/js/handmirror.js" line="1464"/>
        <location filename="../cards/js/handmirror.js" line="2552"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 has been disabled.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2592"/>
        <source>Double the counters on itself</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2593"/>
        <source>Restore and refresh an adjacent item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2634"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 cast 3 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1649"/>
        <location filename="../cards/js/handmirror.js" line="1706"/>
        <location filename="../cards/js/handmirror.js" line="1966"/>
        <location filename="../cards/js/handmirror.js" line="2639"/>
        <source>Choose target</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2390"/>
        <source>Move itself</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2594"/>
        <source>Draw an item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2653"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 restored and refreshed %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2657"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 restored %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2664"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 refreshed %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2668"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 did nothing to %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2674"/>
        <source>No available targets!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2681"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 cast 7 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="979"/>
        <location filename="../cards/js/handmirror.js" line="2715"/>
        <source>You have drawn 2 items!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2792"/>
        <source>I am afraid it is not going to work...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>hardhat</name>
    <message>
        <location filename="../cards/js/hardhat.js" line="58"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 restored the abilities of %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/hardhat.js" line="59"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 has been disabled.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>honeydipper</name>
    <message>
        <location filename="../cards/js/honeydipper.js" line="60"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 discarded the %2.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/honeydipper.js" line="62"/>
        <source>Choose where to move the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/honeydipper.js" line="68"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 has been moved to %2%3.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/honeydipper.js" line="73"/>
        <source>You cannot use this ability!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>horseshoemagnet</name>
    <message>
        <location filename="../cards/js/horseshoemagnet.js" line="143"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 revealed %2&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/horseshoemagnet.js" line="203"/>
        <source>No metal items in this hand!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/horseshoemagnet.js" line="173"/>
        <source>Choose the tile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/horseshoemagnet.js" line="92"/>
        <source>Which player&apos;s hand would you like to view?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/horseshoemagnet.js" line="197"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 disabled the %2 and played it from %3&apos;s hand to %4%5.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/horseshoemagnet.js" line="207"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 lost its abilities.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/horseshoemagnet.js" line="214"/>
        <source>You cannot use this ability!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>hourglass</name>
    <message>
        <location filename="../cards/js/hourglass.js" line="121"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 has been rotated by 180°.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/hourglass.js" line="126"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 gained 3 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/hourglass.js" line="137"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 cast 1 counter.&lt;font color=&quot;khaki&quot;&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/hourglass.js" line="145"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 has been returned to %4&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>innovation</name>
    <message>
        <location filename="../jsutils/board.js" line="282"/>
        <source>You have drawn an item!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>joker</name>
    <message>
        <location filename="../cards/js/joker.js" line="76"/>
        <source>Choose target</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/joker.js" line="88"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 returned %2%3 %4 to %5&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/joker.js" line="114"/>
        <source>Choose where would you like to move the selected card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/joker.js" line="123"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 moved %2%3 %4 to %5%6.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/joker.js" line="130"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 destroyed %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/joker.js" line="141"/>
        <source>No items are selectable for the ability!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>lacefan</name>
    <message>
        <location filename="../cards/js/lacefan.js" line="24"/>
        <source>Choose from the abilities of the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/lacefan.js" line="68"/>
        <source>View the hand of all players</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/lacefan.js" line="69"/>
        <source>Move another player&apos;s item of 3 or less value to an adjacent tile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/lacefan.js" line="70"/>
        <source>Draw an item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/lacefan.js" line="75"/>
        <source>All opponents&apos; hand are already visible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/lacefan.js" line="76"/>
        <source>There are no items on the board that satisfy the conditions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/lacefan.js" line="113"/>
        <source>Choose which item would you like to move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/lacefan.js" line="119"/>
        <source>Choose where would you like to move the selected item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/lacefan.js" line="135"/>
        <source>You have drawn an item!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/lacefan.js" line="143"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 revealed the hands of all players.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/lacefan.js" line="147"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 moved %2%3 %4 to %5%6.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>leafblower</name>
    <message>
        <location filename="../cards/js/leafblower.js" line="55"/>
        <source>Choose target (click on the Leaf Blower to dismiss it)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/leafblower.js" line="66"/>
        <source>Choose where would you like to move the selected item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/leafblower.js" line="82"/>
        <source>No item is selectable for ability!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/leafblower.js" line="90"/>
        <location filename="../cards/js/leafblower.js" line="94"/>
        <location filename="../cards/js/leafblower.js" line="98"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 moved %2%3 %4 to %5%6.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>mainWindow</name>
    <message>
        <location filename="../../src/mainwindow.ui" line="20"/>
        <source>mainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="324"/>
        <source>View boards &amp;&amp; decks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="349"/>
        <location filename="../../src/mainwindow.ui" line="2911"/>
        <location filename="../../src/mainwindow.ui" line="4633"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="374"/>
        <location filename="../../src/mainwindow.ui" line="4607"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="485"/>
        <source>tip_of_the_day_text tip_of_the_day_text tip_of_the_day_text tip_of_the_day_text tip_of_the_day_text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="510"/>
        <source>Next tip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="571"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="596"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="305"/>
        <source>Puzzle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="1490"/>
        <location filename="../../src/mainwindow.ui" line="1581"/>
        <location filename="../../src/mainwindow.ui" line="1662"/>
        <location filename="../../src/mainwindow.ui" line="1791"/>
        <location filename="../../src/mainwindow.ui" line="2936"/>
        <location filename="../../src/mainwindow.ui" line="3274"/>
        <location filename="../../src/mainwindow.ui" line="3395"/>
        <location filename="../../src/mainwindow.ui" line="4686"/>
        <location filename="../../src/mainwindow.ui" line="4975"/>
        <location filename="../../src/mainwindow.ui" line="5109"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="637"/>
        <source>Gameplay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="754"/>
        <source>Require extra click when switching turns in hotseat mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="710"/>
        <source>Order cards in hand alphabetically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="703"/>
        <source>Ask for confirmation when ending turn without placing a card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="807"/>
        <source>Enable GPU acceleration for the game board (OpenGL)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="870"/>
        <source>Fullscreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="856"/>
        <source>Display coordinate grid on the board</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="901"/>
        <source>Sound &amp;&amp; Music</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="1352"/>
        <source>Enable music</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="1359"/>
        <source>Enable soundeffects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="1369"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="1388"/>
        <source>If this setting is set to &quot;Detect System Language&quot;, but there is no translation for the system language, English (US) will be used as a fallback.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="1452"/>
        <source>Reset to defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="1685"/>
        <source>Server address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="1695"/>
        <source>lost-bazaar.org</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="1728"/>
        <location filename="../../src/mainwindow.cpp" line="500"/>
        <location filename="../../src/mainwindow.cpp" line="1556"/>
        <source>Loading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="1759"/>
        <source>Start new public game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="1990"/>
        <source>Deck:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="1949"/>
        <source>Board:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2053"/>
        <source>Game mode:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2095"/>
        <source>1 vs. 1 (opposite players)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2100"/>
        <source>1 vs. 1 (adjacent players)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2105"/>
        <source>2 vs. 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2110"/>
        <source>3 players (FFA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2115"/>
        <source>4 players (FFA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2276"/>
        <source>Enable Huckster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="4620"/>
        <source>Decks &amp;&amp; boards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2474"/>
        <source>Player 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2380"/>
        <location filename="../../src/mainwindow.ui" line="2496"/>
        <location filename="../../src/mainwindow.ui" line="2675"/>
        <location filename="../../src/mainwindow.ui" line="2791"/>
        <source>Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3175"/>
        <location filename="../../src/mainwindow.cpp" line="3176"/>
        <location filename="../../src/mainwindow.cpp" line="3177"/>
        <location filename="../../src/mainwindow.cpp" line="3178"/>
        <source>AI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2403"/>
        <location filename="../../src/mainwindow.ui" line="2519"/>
        <location filename="../../src/mainwindow.ui" line="2698"/>
        <location filename="../../src/mainwindow.ui" line="2814"/>
        <source>Nickname:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2427"/>
        <location filename="../../src/mainwindow.ui" line="2543"/>
        <location filename="../../src/mainwindow.ui" line="2722"/>
        <location filename="../../src/mainwindow.ui" line="2838"/>
        <source>Team:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="1965"/>
        <location filename="../../src/mainwindow.ui" line="2012"/>
        <location filename="../../src/mainwindow.ui" line="2434"/>
        <location filename="../../src/mainwindow.ui" line="2550"/>
        <location filename="../../src/mainwindow.ui" line="2729"/>
        <location filename="../../src/mainwindow.ui" line="2845"/>
        <location filename="../../src/mainwindow.ui" line="4242"/>
        <location filename="../../src/mainwindow.ui" line="4285"/>
        <location filename="../../src/mainwindow.ui" line="4298"/>
        <location filename="../../src/mainwindow.ui" line="4321"/>
        <location filename="../../src/mainwindow.ui" line="4460"/>
        <location filename="../../src/mainwindow.ui" line="4723"/>
        <location filename="../../src/mainwindow.ui" line="4877"/>
        <location filename="../../src/mainwindow.ui" line="4926"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2653"/>
        <source>Player 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2358"/>
        <source>Player 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="258"/>
        <location filename="../../src/mainwindow.ui" line="3242"/>
        <source>Start game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="286"/>
        <source>Network multiplayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="764"/>
        <source>Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="779"/>
        <location filename="../../src/mainwindow.cpp" line="3392"/>
        <source>unnamed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="794"/>
        <source>Display &amp;&amp; Graphics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2135"/>
        <location filename="../../src/mainwindow.cpp" line="5015"/>
        <source>Load a saved game...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2128"/>
        <location filename="../../src/mainwindow.cpp" line="2136"/>
        <location filename="../../src/mainwindow.cpp" line="5029"/>
        <source>New game (no saved game loaded)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2769"/>
        <source>Player 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="1848"/>
        <source>Connected players:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2885"/>
        <location filename="../../src/mainwindow.ui" line="5071"/>
        <source>Start Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3081"/>
        <source>New public game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3104"/>
        <source>Game title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3121"/>
        <source>Game description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3623"/>
        <source>Game log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3651"/>
        <source>Chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2601"/>
        <location filename="../../src/mainwindow.ui" line="3709"/>
        <source>Send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="1817"/>
        <source>Network status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="772"/>
        <source>Username:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3363"/>
        <location filename="../../src/mainwindow.cpp" line="1623"/>
        <source>Join</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3746"/>
        <source>Execute script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3965"/>
        <source>Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3999"/>
        <location filename="../../src/mainwindow.ui" line="4072"/>
        <location filename="../../src/mainwindow.ui" line="4094"/>
        <location filename="../../src/mainwindow.ui" line="4116"/>
        <location filename="../../src/mainwindow.ui" line="4138"/>
        <location filename="../../src/mainwindow.ui" line="4160"/>
        <source>Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3898"/>
        <location filename="../../src/mainwindow.ui" line="4226"/>
        <location filename="../../src/mainwindow.ui" line="4249"/>
        <location filename="../../src/mainwindow.ui" line="4259"/>
        <location filename="../../src/mainwindow.ui" line="4269"/>
        <location filename="../../src/mainwindow.ui" line="4305"/>
        <location filename="../../src/mainwindow.ui" line="4328"/>
        <location filename="../../src/mainwindow.ui" line="4364"/>
        <location filename="../../src/mainwindow.ui" line="4374"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="835"/>
        <source>Animate cards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="800"/>
        <source>Animate board rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="863"/>
        <source>Sliding animation when changing pages on the GUI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="1624"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2067"/>
        <source>Deck preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2060"/>
        <source>Board preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2159"/>
        <source>filter boards...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2185"/>
        <source>filter decks...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="96"/>
        <location filename="../../src/mainwindow.ui" line="546"/>
        <location filename="../../src/mainwindow.ui" line="3947"/>
        <source>Report a bug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="106"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If you think you found an error in the game, please write a detailed description below. Make sure to describe the steps needed to reproduce the error in as much detail as possible. Write in English or Hungarian. Include any information you think might be relevant. You may include contact information but this is not required. When you&apos;re done, click on the &amp;quot;Save report&amp;quot; button. You will be able to save your error report as a file with some additional information included (the current state of the game and a screenshot). Send this file to us either in an e-mail to &lt;span style=&quot; font-weight:600;&quot;&gt;bugs@lost-bazaar.org&lt;/span&gt; or in the Discord server. If you want to send any additional files, attach those to your message too.&lt;/p&gt;&lt;p&gt;It is very important to include as much information about the bug and how to reproduce it as possible. We can only fix a bug if we can reproduce it.&lt;/p&gt;&lt;p&gt;Example of a &lt;span style=&quot; font-weight:600;&quot;&gt;bad &lt;/span&gt;error report:&lt;/p&gt;&lt;p&gt;&amp;quot;the game crashes all the time&amp;quot;&lt;/p&gt;&lt;p&gt;Example of a &lt;span style=&quot; font-weight:600;&quot;&gt;good&lt;/span&gt; error report:&lt;/p&gt;&lt;p&gt;&amp;quot;The game crashes when I start a new game and place a Lace Fan in the top left corner in the first turn. It does not crash if I do this in any other turn or if I don&apos;t place it in the top left corner.&amp;quot;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="116"/>
        <source>description of the bug...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="131"/>
        <source>Save report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="394"/>
        <source>version %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="645"/>
        <source>AI difficulty:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="668"/>
        <source>Easy (fast AI turns)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="694"/>
        <source>Hard (slow AI turns)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="814"/>
        <source>Never change board perspective</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="821"/>
        <source>Do not change board perspective for network players</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="828"/>
        <source>Allow arbitrary resizing of the in-game chat panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2283"/>
        <source>The Huckster has no more than</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3026"/>
        <source>Previous page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3013"/>
        <source>Next page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="160"/>
        <location filename="../../src/mainwindow.ui" line="599"/>
        <location filename="../../src/mainwindow.ui" line="1493"/>
        <location filename="../../src/mainwindow.ui" line="1584"/>
        <location filename="../../src/mainwindow.ui" line="1665"/>
        <location filename="../../src/mainwindow.ui" line="1794"/>
        <location filename="../../src/mainwindow.ui" line="2939"/>
        <location filename="../../src/mainwindow.ui" line="3068"/>
        <location filename="../../src/mainwindow.ui" line="3277"/>
        <location filename="../../src/mainwindow.ui" line="3398"/>
        <location filename="../../src/mainwindow.ui" line="4978"/>
        <location filename="../../src/mainwindow.ui" line="5112"/>
        <source>Esc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="233"/>
        <source>Learn to play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="849"/>
        <source>Do not use prerendered full size card images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="884"/>
        <source>[EXPERIMENTAL] Enable high resolution mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="877"/>
        <source>Use a shrinking effect instead of blurring when cards temporarily lose their abilities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="842"/>
        <source>Disable font hinting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="891"/>
        <source>Override default font sizes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="1705"/>
        <source>Server info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="1712"/>
        <source>Ban</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="1719"/>
        <source>Unban</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2200"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2209"/>
        <source>Watch replay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2236"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2241"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2246"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2251"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2256"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2261"/>
        <source>6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2269"/>
        <source>Open hands (players can see each other&apos;s cards)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2303"/>
        <source>cards.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3039"/>
        <source>Try it out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3138"/>
        <location filename="../../src/mainwindow.ui" line="3313"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3154"/>
        <source>leave empty for no password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3161"/>
        <source>Tags:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3186"/>
        <source>anyone is welcome to join this game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3193"/>
        <source>this game is part of a tournament</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3290"/>
        <source>Password is required to join game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3478"/>
        <source>Replay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3498"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3505"/>
        <source>Step-by-step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3518"/>
        <source>Normal speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3531"/>
        <source>Fast forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3719"/>
        <source>Script console</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3727"/>
        <source>Write your scripts here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3885"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3923"/>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3935"/>
        <source>Game info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="4344"/>
        <source>Cards (inventory/board)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="4357"/>
        <source>Score</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="4396"/>
        <source>Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="4530"/>
        <location filename="../../src/mainwindow.cpp" line="3634"/>
        <source>End Turn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="4578"/>
        <source>Resume Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="4594"/>
        <source>Save Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="4804"/>
        <source>Category:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2151"/>
        <location filename="../../src/mainwindow.ui" line="4817"/>
        <source>Boards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="2177"/>
        <location filename="../../src/mainwindow.ui" line="4822"/>
        <source>Decks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="3065"/>
        <source>Back to menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="4646"/>
        <source>Leave Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="4867"/>
        <source>Select board:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="4916"/>
        <source>Select deck:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="5022"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="5027"/>
        <source>Difficulty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="5032"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="5125"/>
        <source>Turn switch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="5170"/>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1462"/>
        <location filename="../../src/mainwindow.cpp" line="1463"/>
        <location filename="../../src/mainwindow.cpp" line="1464"/>
        <location filename="../../src/mainwindow.cpp" line="1465"/>
        <source>%1/%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="392"/>
        <source>Load game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="392"/>
        <location filename="../../src/mainwindow.cpp" line="2255"/>
        <source>Lost Bazaar saved games (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="686"/>
        <source>Game hosted by %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1133"/>
        <source>Huckster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1135"/>
        <source>Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1137"/>
        <source>Main deck</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3424"/>
        <source>Error: cannot open deck file (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="4466"/>
        <source>Turn of %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1358"/>
        <source>Error: can&apos;t read saved game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1572"/>
        <source>Created by %1 at %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1574"/>
        <source>Description: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="525"/>
        <source>No connection to the server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="411"/>
        <source>Join our Discord server at &lt;a href=&quot;http://discord.lost-bazaar.org&quot;&gt;discord.lost-bazaar.org&lt;/a&gt;!&lt;br&gt;version %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="704"/>
        <location filename="../../src/mainwindow.cpp" line="2154"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="706"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="896"/>
        <source>You are about to start a network game where all sides are controlled by the host. Do you want to start the game like this?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="896"/>
        <source>All sides controlled by host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="915"/>
        <source>Leave game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="915"/>
        <source>Are you sure you want to leave the game?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1132"/>
        <source>Storage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1271"/>
        <source>Error: invalid game mode, game aborted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1466"/>
        <location filename="../../src/mainwindow.cpp" line="3610"/>
        <source>Items in deck/trash: %1/%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1579"/>
        <source>Status: ongoing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1583"/>
        <source>Status: in lobby (game not started yet)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1588"/>
        <source>Game mode: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1589"/>
        <source>Board: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1590"/>
        <source>Deck: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1601"/>
        <source>This game is password protected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1602"/>
        <source>Anyone is welcome to join this game.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1603"/>
        <source>This game is part of a tournament.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1624"/>
        <source>Join with password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1666"/>
        <source>No ongoing games</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1721"/>
        <location filename="../../src/mainwindow.cpp" line="3841"/>
        <location filename="../../src/mainwindow.cpp" line="3847"/>
        <source>No deck selected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1722"/>
        <location filename="../../src/mainwindow.cpp" line="3798"/>
        <location filename="../../src/mainwindow.cpp" line="3806"/>
        <source>No board selected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1932"/>
        <source>Lost Bazaar error reports (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1933"/>
        <source>Save error report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1966"/>
        <source>Error report saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1966"/>
        <source>Thank you for your error report. Please do not forget to submit it either in e-mail to bugs@lost-bazaar.org or to our Discord server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="2132"/>
        <location filename="../../src/mainwindow.cpp" line="4987"/>
        <source>Loaded game: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="2151"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="2152"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="2222"/>
        <source>Error: invalid player in switchToPlayerInventory(...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="2256"/>
        <source>Save game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="2286"/>
        <source>Error: popInventoryTab() called but the stack is empty. There is a bug somewhere, please report this.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="2293"/>
        <source>Error: trying to switch to a hidden or invisible inventory tab (in popInventoryTab()). There is a bug in some script somewhere...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="2308"/>
        <source>Error: trying to switch to a hidden or invisible inventory tab. There is a bug in some script somewhere...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="2394"/>
        <source>Detect system language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="2395"/>
        <source>English (US)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="2396"/>
        <source>Hungarian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3584"/>
        <source>Can&apos;t connect, there is already a player with your username in the game!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3595"/>
        <source>Can&apos;t connect: wrong password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="5191"/>
        <location filename="../../src/mainwindow.cpp" line="5195"/>
        <location filename="../../src/mainwindow.cpp" line="5224"/>
        <location filename="../../src/mainwindow.cpp" line="5258"/>
        <source>&lt;Random&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="5193"/>
        <source>&lt;Random&gt; (compatible with deck selection)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="5197"/>
        <source>&lt;Random&gt; (compatible with board selection)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="5286"/>
        <source>No game mode selected yet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="2471"/>
        <location filename="../../src/mainwindow.cpp" line="2506"/>
        <location filename="../../src/mainwindow.cpp" line="2512"/>
        <location filename="../../src/mainwindow.cpp" line="2521"/>
        <location filename="../../src/mainwindow.cpp" line="4184"/>
        <location filename="../../src/mainwindow.cpp" line="4193"/>
        <location filename="../../src/mainwindow.cpp" line="4199"/>
        <location filename="../../src/mainwindow.cpp" line="4208"/>
        <location filename="../../src/mainwindow.cpp" line="4217"/>
        <source>You must restart the application for this change to fully take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="707"/>
        <source>It looks like this is the first time you are connecting to a multiplayer server. Please set the username you would like to use (you can change this later in the settings):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="2560"/>
        <location filename="../../src/mainwindow.cpp" line="4247"/>
        <source>You must restart the application for the language switch to fully take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="2627"/>
        <source>Observers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="2628"/>
        <source>Observers:
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="2906"/>
        <source>Error: invalid game event received from the network (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3051"/>
        <source>Huckster #%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3171"/>
        <location filename="../../src/mainwindow.cpp" line="3172"/>
        <location filename="../../src/mainwindow.cpp" line="3173"/>
        <location filename="../../src/mainwindow.cpp" line="3174"/>
        <source>Local player (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3183"/>
        <location filename="../../src/mainwindow.cpp" line="3184"/>
        <location filename="../../src/mainwindow.cpp" line="3185"/>
        <location filename="../../src/mainwindow.cpp" line="3186"/>
        <location filename="../../src/mainwindow.cpp" line="4871"/>
        <location filename="../../src/mainwindow.cpp" line="4872"/>
        <location filename="../../src/mainwindow.cpp" line="4873"/>
        <location filename="../../src/mainwindow.cpp" line="4874"/>
        <source>Network player: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3264"/>
        <source>Error: invalid include directive (wrong path or filename): %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3520"/>
        <source>[!!ERROR!!, PLAYER %1] %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3522"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3535"/>
        <source>Connecting to game...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.ui" line="157"/>
        <location filename="../../src/mainwindow.cpp" line="705"/>
        <location filename="../../src/mainwindow.cpp" line="2153"/>
        <location filename="../../src/mainwindow.cpp" line="3614"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3776"/>
        <source>Random board.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3816"/>
        <source>Random deck.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3830"/>
        <source>
Huckster:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3872"/>
        <source>This game is local. It will not be listed on any public server and network players will not be able to connect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3876"/>
        <source>This game is public. It will be listed on the server you connected to on the previous screen. Network players can join. You can start private games from the main menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3997"/>
        <source>Team A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3997"/>
        <source>Team B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="4299"/>
        <location filename="../../src/mainwindow.cpp" line="4317"/>
        <source>Not selected yet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="4303"/>
        <source>Random board</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="4309"/>
        <source>Unknown board</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="4321"/>
        <source>Random deck</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="4327"/>
        <source>Unknown deck</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="4375"/>
        <source>Can&apos;t generate random deck (not enough decks to choose from)!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="4413"/>
        <source>Can&apos;t generate random board (not enough boards to choose from)!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="4575"/>
        <source>Local: [%1], remote: [%2]
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="4580"/>
        <source>Local: [%1], remote: none
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="4585"/>
        <source>The game is out of sync. It is highly recommended to save now and reopen.
If you report this bug, all players should save now and all saves should be submitted with the report.
Details:
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="4901"/>
        <source>Error: invalid game event received</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="4982"/>
        <source>Error: invalid or unreadable saved game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="2760"/>
        <location filename="../../src/mainwindow.cpp" line="2772"/>
        <location filename="../../src/mainwindow.cpp" line="2783"/>
        <location filename="../../src/mainwindow.cpp" line="2794"/>
        <location filename="../../src/mainwindow.cpp" line="2805"/>
        <location filename="../../src/mainwindow.cpp" line="2816"/>
        <location filename="../../src/mainwindow.cpp" line="2830"/>
        <location filename="../../src/mainwindow.cpp" line="2849"/>
        <location filename="../../src/mainwindow.cpp" line="2865"/>
        <location filename="../../src/mainwindow.cpp" line="2877"/>
        <source>Error: invalid card UID received in network message, out of sync</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1823"/>
        <source>Player disconnected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1823"/>
        <source>The game cannot continue because a player disconnected.
Would you like to save the current state of the game?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="1836"/>
        <source>The host has abandoned the game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="2471"/>
        <location filename="../../src/mainwindow.cpp" line="2506"/>
        <location filename="../../src/mainwindow.cpp" line="2512"/>
        <location filename="../../src/mainwindow.cpp" line="2521"/>
        <location filename="../../src/mainwindow.cpp" line="2560"/>
        <location filename="../../src/mainwindow.cpp" line="4184"/>
        <location filename="../../src/mainwindow.cpp" line="4193"/>
        <location filename="../../src/mainwindow.cpp" line="4199"/>
        <location filename="../../src/mainwindow.cpp" line="4208"/>
        <location filename="../../src/mainwindow.cpp" line="4217"/>
        <location filename="../../src/mainwindow.cpp" line="4247"/>
        <source>Applying configuration change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="2835"/>
        <location filename="../../src/mainwindow.cpp" line="2854"/>
        <source>Error: invalid inventory ID received in network message, out of sync</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3620"/>
        <source>Draw Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3626"/>
        <source>Too many items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3631"/>
        <source>Too few items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3670"/>
        <source>No replay data found in the save file!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="3821"/>
        <source>
Items:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/mainwindow.cpp" line="5003"/>
        <source>Clear loaded game</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>mask</name>
    <message>
        <location filename="../cards/js/mask.js" line="41"/>
        <source>You cannot move this item!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/mask.js" line="45"/>
        <source>Choose target (click on the %1 to dismiss it)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/mask.js" line="61"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 returned %2%3 %4 to %5&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/mask.js" line="62"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 has been moved to %2%3.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/mask.js" line="67"/>
        <source>No available target for this ability!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/mask.js" line="77"/>
        <source>The %1 lost its abilities.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>megaphone</name>
    <message>
        <location filename="../cards/js/megaphone.js" line="103"/>
        <source>Choose target</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/megaphone.js" line="110"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 returned %2%3 %4 to %5&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/megaphone.js" line="111"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 played the %2 from %3&apos;s hand to %4%5.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../jsutils/megap.js" line="652"/>
        <source>%1 %2 %3 %4</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>musicPlayer</name>
    <message>
        <location filename="../../src/audio.cpp" line="82"/>
        <source>Error: cannot open playlist (%1)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>net</name>
    <message>
        <location filename="../cards/js/net.js" line="29"/>
        <source>%1%2 %3: %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/net.js" line="33"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;%1 rolled a die: %2.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/net.js" line="36"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 gained 1 counter.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/net.js" line="40"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 gained %2 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/net.js" line="48"/>
        <source>The %1 has been disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/net.js" line="60"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 cast 1 counter.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/net.js" line="66"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 cast 2 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>network::networkConnectionManager</name>
    <message>
        <location filename="../../src/network.cpp" line="35"/>
        <source>Network error: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/network.cpp" line="135"/>
        <source>Error: invalid network message received</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/network.cpp" line="383"/>
        <source>Error: invalid user (%1) in sendMessageToUser(...)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>oillamp</name>
    <message>
        <location filename="../cards/js/oillamp.js" line="27"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 gained 6 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/oillamp.js" line="34"/>
        <source>The %1 has been disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/oillamp.js" line="51"/>
        <source>Choose an item to discard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/oillamp.js" line="79"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 cast 2 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/oillamp.js" line="80"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 discarded the %2.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/oillamp.js" line="106"/>
        <source>Choose an item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/oillamp.js" line="132"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 drew the %2 from the storage.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/oillamp.js" line="133"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 drew an item from the storage.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/oillamp.js" line="141"/>
        <source>No items in hand to discard!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>operaglove</name>
    <message>
        <location filename="../cards/js/operaglove.js" line="113"/>
        <source>Choose which item should be taken to your hand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/operaglove.js" line="129"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 returned %2%3 %4 to %5&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/operaglove.js" line="137"/>
        <source>Choose an item to discard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/operaglove.js" line="177"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 discarded the %2.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/operaglove.js" line="182"/>
        <source>No item is selectable for ability!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>perfumebottle</name>
    <message>
        <location filename="../cards/js/perfumebottle.js" line="28"/>
        <source>The %1 has been disabled.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>piggybank</name>
    <message>
        <location filename="../cards/js/handmirror.js" line="221"/>
        <location filename="../cards/js/piggybank.js" line="29"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 gained 1 counter.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/piggybank.js" line="21"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 gained 1 counter.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pincers</name>
    <message>
        <location filename="../cards/js/pincers.js" line="73"/>
        <source>There is no adjacent empty tile!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/pincers.js" line="80"/>
        <source>Choose target</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/pincers.js" line="90"/>
        <source>Choose where would you like to move the selected item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/pincers.js" line="99"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 moved %2%3 %4 to %5%6.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/pincers.js" line="109"/>
        <source>The %1 lost its abilities.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>rasp</name>
    <message>
        <location filename="../cards/js/rasp.js" line="49"/>
        <source>Choose target</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/rasp.js" line="58"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 removed 1 counter from %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/rasp.js" line="73"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 destroyed %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/rasp.js" line="84"/>
        <source>You cannot use this ability!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>rng</name>
    <message>
        <location filename="../../src/random.cpp" line="95"/>
        <source>Script error: randomBetween was called with an invalid range (%2, %3), this is probably a bug in the following script:
%1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>saltshaker</name>
    <message>
        <location filename="../cards/js/saltshaker.js" line="27"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 gained 8 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/saltshaker.js" line="71"/>
        <source>Choose where to move the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/saltshaker.js" line="84"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 has been moved to %2%3.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/saltshaker.js" line="105"/>
        <source>Choose on which item would you like to place a counter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/saltshaker.js" line="127"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 transfered 1 counter from itself to %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/saltshaker.js" line="134"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 has been rotated toward %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/saltshaker.js" line="140"/>
        <location filename="../cards/js/saltshaker.js" line="145"/>
        <source>You cannot use this ability!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>scales</name>
    <message>
        <location filename="../cards/js/scales.js" line="82"/>
        <source>Choose an item to discard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/scales.js" line="113"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 discarded the %2.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/scales.js" line="123"/>
        <source>Choose target</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/scales.js" line="132"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 disabled %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/scales.js" line="139"/>
        <source>No item is selectable for ability!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/scales.js" line="144"/>
        <source>You cannot use this ability!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>scissors</name>
    <message>
        <location filename="../cards/js/scissors.js" line="27"/>
        <source>Choose the direction for rotating the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/scissors.js" line="52"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 have been rotated by 90° counter-clockwise.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/scissors.js" line="63"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 have been rotated by 180°.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/scissors.js" line="74"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 have been rotated by 90° clockwise.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/scissors.js" line="111"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 destroyed %4%5 %6.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/scissors.js" line="120"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;The %1 has been moved to %2%3.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/scissors.js" line="175"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;The %1 cast 1 counter.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/scissors.js" line="180"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;The %1 cast %2 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="245"/>
        <location filename="../cards/js/scissors.js" line="85"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 gained 1 counter.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="1999"/>
        <location filename="../cards/js/scissors.js" line="30"/>
        <source>Rotate counter-clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2000"/>
        <location filename="../cards/js/scissors.js" line="31"/>
        <source>Rotate 180 degrees</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/handmirror.js" line="2001"/>
        <location filename="../cards/js/scissors.js" line="32"/>
        <source>Rotate clockwise</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>sneaker</name>
    <message>
        <location filename="../cards/js/sneaker.js" line="95"/>
        <source>Which player&apos;s item would you like to view?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/sneaker.js" line="121"/>
        <source>You revealed this item:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/sneaker.js" line="122"/>
        <source>%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/sneaker.js" line="136"/>
        <source>Choose where to move the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/sneaker.js" line="160"/>
        <location filename="../cards/js/sneaker.js" line="189"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 revealed an item of %2 value in %3&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/sneaker.js" line="161"/>
        <location filename="../cards/js/sneaker.js" line="165"/>
        <location filename="../cards/js/sneaker.js" line="190"/>
        <location filename="../cards/js/sneaker.js" line="194"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 revealed %2 in %3&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/sneaker.js" line="167"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 has been moved to %2%3.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/sneaker.js" line="171"/>
        <source>Nowhere to move!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/sneaker.js" line="201"/>
        <source>No items to reveal!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/sneaker.js" line="206"/>
        <source>You cannot move this item!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>snookerball</name>
    <message>
        <location filename="../cards/js/snookerball.js" line="43"/>
        <source>%1%2 %3: %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/snookerball.js" line="48"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 gained 1 counter.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/snookerball.js" line="52"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 gained %4 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/snookerball.js" line="97"/>
        <source>Move an adjacent item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/snookerball.js" line="145"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 cast %2 counter.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/snookerball.js" line="149"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 cast %2 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/snookerball.js" line="151"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 moved %2%3 %4 to %5%6.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/snookerball.js" line="162"/>
        <location filename="../cards/js/snookerball.js" line="193"/>
        <source>The %1 has been disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/snookerball.js" line="181"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 cast 5 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/snookerball.js" line="182"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 has been moved to %2%3.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/snookerball.js" line="169"/>
        <source>Choose where to move the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/snookerball.js" line="45"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1 rolled a die: %2.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/snookerball.js" line="94"/>
        <source>Choose from the abilities of the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/snookerball.js" line="98"/>
        <source>Move itself</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/snookerball.js" line="124"/>
        <source>Choose which item would you like to move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/snookerball.js" line="131"/>
        <source>Choose where would you like to move the selected item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/snookerball.js" line="199"/>
        <source>You cannot use this ability!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>souvenir</name>
    <message>
        <location filename="../cards/js/souvenir.js" line="261"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 has been rotated towards %2.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/souvenir.js" line="165"/>
        <source>Toward who would you like to rotate the %1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/souvenir.js" line="278"/>
        <source>Choose on which item would you like to place a counter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/souvenir.js" line="282"/>
        <source>Choose on which item would you like to place %1 counters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/souvenir.js" line="305"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 took %2&apos;s %3 away.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/souvenir.js" line="316"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 took an item of %2 value from %3&apos;s hand away.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/souvenir.js" line="320"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 placed %2 counters on %3%4 %5.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/souvenir.js" line="324"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 placed 1 counter on %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/souvenir.js" line="328"/>
        <source>&lt;font color=&quot;khaki&quot;&gt;%1%2 %3 has been rotated toward %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>spectacles</name>
    <message>
        <location filename="../cards/js/spectacles.js" line="38"/>
        <source>Which player&apos;s hand would you like to view?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/spectacles.js" line="115"/>
        <source>Choose an item to play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/spectacles.js" line="177"/>
        <source>Choose where to play the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/spectacles.js" line="560"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 revealed %2&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/spectacles.js" line="561"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 played the %2 from %3&apos;s hand to %4%5.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/spectacles.js" line="581"/>
        <source>No item in your hand fulfills the criteria!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/spectacles.js" line="588"/>
        <source>No available tiles for the ability!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>teakettle</name>
    <message>
        <location filename="../cards/js/teakettle.js" line="30"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 gained 1 counter.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/teakettle.js" line="37"/>
        <source>The %1 has been disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/teakettle.js" line="68"/>
        <source>Choose from the abilities of the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/teakettle.js" line="71"/>
        <source>Double the counters on itself</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/teakettle.js" line="72"/>
        <source>Restore and refresh an adjacent item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/teakettle.js" line="73"/>
        <source>Draw an item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/teakettle.js" line="98"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 gained %2 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/teakettle.js" line="102"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 gained 1 counter.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/teakettle.js" line="113"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 cast 3 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/teakettle.js" line="118"/>
        <source>Choose target</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/teakettle.js" line="132"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 restored and refreshed %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/teakettle.js" line="136"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 restored %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/teakettle.js" line="143"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 refreshed %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/teakettle.js" line="147"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 did nothing to %2%3 %4.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/teakettle.js" line="153"/>
        <source>No available targets!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/teakettle.js" line="160"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 cast 7 counters.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/teakettle.js" line="168"/>
        <source>You have drawn an item!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>treasurechest</name>
    <message>
        <location filename="../cards/js/treasurechest.js" line="52"/>
        <source>To which player you would like to give the %1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/treasurechest.js" line="96"/>
        <source>%1%2 %3: %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/treasurechest.js" line="23"/>
        <source>The %1 has been disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/treasurechest.js" line="83"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 has been sent to %2&apos;s hand.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/treasurechest.js" line="88"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;%1 rolled a die: 6.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/treasurechest.js" line="94"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;%1 rolled a die: %2.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/treasurechest.js" line="121"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 drew the %2 for %3.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/treasurechest.js" line="125"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 drew the %2 for %3, which is immediately returned to the storage.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/treasurechest.js" line="130"/>
        <source>You have drawn %1 items!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/treasurechest.js" line="134"/>
        <source>You have drawn an item!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/treasurechest.js" line="138"/>
        <source>No items were drawn!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>trickydie</name>
    <message>
        <location filename="../cards/js/trickydie.js" line="32"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 has been destroyed.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/trickydie.js" line="40"/>
        <source>You have drawn 2 items!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/trickydie.js" line="47"/>
        <source>You have drawn an item!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>trombone</name>
    <message>
        <location filename="../cards/js/trombone.js" line="43"/>
        <source>Choose target (click on the Trombone to dismiss it)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/trombone.js" line="54"/>
        <source>Choose where would you like to move the selected card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/trombone.js" line="60"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 moved %2%3 %4 to %5%6.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/trombone.js" line="65"/>
        <source>You cannot use this ability!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/trombone.js" line="72"/>
        <source>There is no adjacent empty tile!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>whiteknight</name>
    <message>
        <location filename="../cards/js/whiteknight.js" line="52"/>
        <source>You have drawn an item!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/whiteknight.js" line="64"/>
        <source>Choose where to move the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/whiteknight.js" line="90"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 has been moved to %2%3.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/whiteknight.js" line="95"/>
        <source>Nowhere to move!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/whiteknight.js" line="100"/>
        <source>You cannot move this item!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>whitepawn</name>
    <message>
        <location filename="../cards/js/whitepawn.js" line="71"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 has been moved to %2%3.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/whitepawn.js" line="77"/>
        <source>&lt;font color=&quot;paleturquoise&quot;&gt;The %1 gained 1 counter.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/whitepawn.js" line="85"/>
        <source>You cannot move this item!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/whitepawn.js" line="90"/>
        <source>Nowhere to move!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>wrench</name>
    <message>
        <location filename="../cards/js/wrench.js" line="112"/>
        <source>Choose from the abilities of the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/wrench.js" line="115"/>
        <source>Take back a metal item from the trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/wrench.js" line="116"/>
        <source>Rotate an item of 1 value or less</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/wrench.js" line="121"/>
        <source>No metal item in the trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/wrench.js" line="121"/>
        <source>There are no cards on the board that satisfy the conditions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/wrench.js" line="129"/>
        <source>Choose a metal item to reclaim</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/wrench.js" line="144"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 reclaimed the %2 for %3.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/wrench.js" line="148"/>
        <source>Choose which item would you like to rotate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/wrench.js" line="153"/>
        <source>Choose the direction for rotating the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/wrench.js" line="172"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 rotated %2%3 %4 by 90° counter-clockwise.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/wrench.js" line="182"/>
        <source>&lt;font color=&quot;bisque&quot;&gt;The %1 rotated %2%3 %4 by 90° clockwise.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/wrench.js" line="155"/>
        <source>Rotate counter-clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cards/js/wrench.js" line="156"/>
        <source>Rotate clockwise</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
