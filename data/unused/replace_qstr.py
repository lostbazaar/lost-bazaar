#!/usr/bin/env python3
import fileinput
import os

rootDir = '.'
for dirName, subdirList, fileList in os.walk(rootDir):
    for fname in fileList:
        if fname.endswith('.js'):
            context = fname[:-3]
            print("Processing file: "+fname)
            with fileinput.FileInput(dirName+'/'+fname, inplace=True, backup='.bak') as f:
                for line in f:
                    print(line.replace('qsTr(', 'qsTranslate("'+context+'", ').replace('qsTr (', 'qsTranslate("'+context+'", '),end='')
