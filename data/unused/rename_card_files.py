#!/usr/bin/env python3
import fileinput
import os

replacements = {"ajandekdoboz": "giftbox", "alarc": "mask", "antikzsebora": "antiquewatch",
                "aranyserleg": "goldencup", "baseballuto": "baseballbat", "belyeg": "stamp",
                "biliardgolyo": "snookerball", "bomba": "bomb", "boritek": "envelope",
                "csavarkulcs": "wrench", "csipkelegyezo": "lacefan", "dobonyil": "dart",
                "edzocipo": "sneaker", "fogaskerek": "cog", "golfuto": "golfclub", "halo": "net",
                "hamisfestmeny": "fakepainting", "harapofogo": "pincers", "harsona": "trombone",
                "homokora": "hourglass", "irattarto": "documentholder", "joker": "joker", "josgomb": "scryingorb",
                "kalapacs": "hammer", "kavedaralo": "coffeegrinder", "keltapajzs": "celticshield", "kertilapat": "gardenspade",
                "kezitukor": "handmirror", "kincsesladika": "treasurechest", "korfuresz": "buzzsaw",
                "korzo": "compasses", "lakat": "padlock", "legycsapo": "flyswatter",
                "lombfuvo": "leafblower", "madareteto": "birdfeeder", "magnespatko": "horseshoemagnet",
                "malacpersely": "piggybank", "megafon": "megaphone", "merleg": "scales",
                "mezcsurgato": "honeydipper", "nagyszotar": "dictionary", "olajlampa": "oillamp",
                "ollo": "scissors", "operakesztyu": "operaglove", "parfumosuveg": "perfumebottle",
                "pecabot": "fishingrod", "radir": "eraser", "ragasztotubus": "gluetube", "reszelo": "rasp",
                "rogbilabda": "rugbyball", "sebtapasz": "adhesivebandage", "sepru": "broom", "serpenyo": "fryingpan",
                "soroskorso": "beermug", "sotarto": "saltshaker", "spiralrugo": "spiralspring", "szemuveg": "spectacles",
                "szivacs": "sponge", "szuvenir": "souvenir", "tavcso": "binoculars", "teaskanna": "teakettle",
                "trukkoskocka": "trickydice", "ureskonzervdoboz": "emptycan", "vaza": "vase",
                "vedosisak": "hardhat", "vilagosgyalog": "whitepawn", "vilagoshuszar": "whiteknight",
                "vonalzo": "ruler", "zaszlo": "flag", "egerfogo": "mousetrap", "fesu": "comb",
                "gomblombik": "roundbottomflask", "jatekkatona": "toysoldier", "kazetta": "tape",
                "levelnehezek": "paperweight", "szemszimbolum": "eyesymbol",
                "szivargohordo": "leakykeg", "taposoakna": "landmine", "teszt": "test"
                }

rootDir = '.'
for dirName, subdirList, fileList in os.walk(rootDir):
    for fname in fileList:
        parts = fname.split('.')
        if len(parts) == 2 and parts[0] in replacements and not "/names/" in dirName:
            newname = replacements[parts[0]] + "." + parts[1]
            print("Renaming: "+fname+" -> "+newname)
            os.rename(dirName+"/"+fname,dirName+"/"+newname)
