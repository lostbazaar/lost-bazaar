#Lost Bazaar
#Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys

dry = not ("--apply" in sys.argv)

def repl(fname):
    print("Replacing in file:"+fname)
    filedata = ""
    with open(fname, 'r') as file:
        filedata = file.read()
    
    #keep=["reportError","getPlayerUserNames","getConfigVariable","newPoint"]
    #for k in keep:
    #    filedata = filedata.replace("mainWindow."+k,"mainWindow!!!!!"+k)

    filedata = filedata.replace("mainWindow.","gameState.")
    filedata = filedata.replace("getPlayerUserNames","getPlayerNickNames")
    #filedata = filedata.replace("mainWindow!!!!!","mainWindow.")
    
    if not dry:
        with open(fname, 'w') as file:
            file.write(filedata)

for root, dirs, files in os.walk('.'):
    for f in files:
        if f.endswith('.js') or f.endswith('.qjs'):
            repl(root+'/'+f)


