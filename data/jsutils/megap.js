//@include jsutils.board
//@include jsutils.point


var megaputils = new Object();


megaputils.secondMegaphoneCondition = function(choiceForMegaphone, xMega, yMega)
{
  switch(choiceForMegaphone.getID())
  {
    case "giftbox":
      var xVaria = choiceForMegaphone.getPosX();
      var yVaria = choiceForMegaphone.getPosY();
      if(board.isSafeTile(Point(xVaria,yVaria)) == false)
      {
        cardsCFUR = board.getAllCards();
        instantlyRotatable = true;
        for(cfur = 0; cfur < cardsCFUR.length; cfur++)
        {
          if((cardsCFUR[cfur].getID() == "celticshield" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "celticshield")) && cardsCFUR[cfur].isGrayScale() == false && cardsCFUR[cfur].getPlayer() == gameState.getCurrentPlayer())
          {
            var shieldDistance = Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2)
            if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
            {
              instantlyRotatable = false;
              break;
            }
          }

          if(card.getID() != "padlock" || (card.getID() == "padlock" && card.isGrayScale() == true))
          {
            if(cardsCFUR[cfur].getID() == "padlock" && cardsCFUR[cfur].isGrayScale() == false && Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2) == 1)
            {
              instantlyRotatable = false;
              break;
            }
          }
          else
          {
            instantlyRotatable = false;
            break;
          }

          if((cardsCFUR[cfur].getID() == "hardhat" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "hardhat")) && cardsCFUR[cfur].isGrayScale() == false)
          {
            switch(cardsCFUR[cfur].getPlayer())
            {
              case 1: if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria-1) {instantlyRotatable = false;} break;
              case 2: if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria+1) {instantlyRotatable = false;} break;
              case 3: if(cardsCFUR[cfur].getPosX() == xVaria+1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;} break;
              case 4: if(cardsCFUR[cfur].getPosX() == xVaria-1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;} break;
            }
          }

        }
        if(instantlyRotatable == true)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
      return false;
      break;

    case "goldencup":
      var xVaria = choiceForMegaphone.getPosX();
      var yVaria = choiceForMegaphone.getPosY();
      var adjacentToCup = 1;
      for(ttc = 2; ttc < 10; ttc = ttc+2)
      {
        var xCorri = 0;
        var yCorri = 0;
        if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
        if(ttc == 4 && xVaria > 0) {xCorri = -1;}
        if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
        if(ttc == 8 && yVaria > 0) {yCorri = -1;}
        CTC = board.getCardAtPosition(Point(xVaria+xCorri,yVaria+yCorri));
        if(CTC != null && CTC.getUID() != choiceForMegaphone.getUID())
        {
          adjacentToCup++;
        }
        if(ttc == 8)
        {
          if( ((gameState.getGameMode() == 0 || gameState.getGameMode() == 4) && mainInventory.getAllCards().length >= adjacentToCup) || 
              ((gameState.getGameMode() == 1 || gameState.getGameMode() == 2) && mainInventory.getAllCards().length >= adjacentToCup*2) ||
              ((gameState.getGameMode() == 3) && mainInventory.getAllCards().length >= adjacentToCup*3) )
          {
            return true;
          }
          else
          {
            return false;
          }
        }
      }
      break;

    case "wrench":
      var firstAbilityAvailable = false;
      var secondAbilityAvailable = false;
      var trashCards = trash.getAllCards();
      for(i = 0; i < trashCards.length; i++)
      {
        if(trashCards[i].getBackground() == "metal")
        {
          firstAbilityAvailable = true;
          break;
        }
      }

      if(firstAbilityAvailable == false)
      {
        var xVaria = choiceForMegaphone.getPosX();
        var yVaria = choiceForMegaphone.getPosY();
        for(j = 1; j < 9; j++)
        {
          var xCorri = 0;
          var yCorri = 0;
          if(j == 1) {xCorri = 1; yCorri = -2;}
          if(j == 2) {xCorri = 2; yCorri = -1;}
          if(j == 3) {xCorri = 2; yCorri = 1;}
          if(j == 4) {xCorri = 1; yCorri = 2;}
          if(j == 5) {xCorri = -1; yCorri = 2;}
          if(j == 6) {xCorri = -2; yCorri = 1;}
          if(j == 7) {xCorri = -2; yCorri = -1;}
          if(j == 8) {xCorri = -1; yCorri = -2;}

          var CTC = board.getCardAtPosition(Point(xVaria+xCorri,yVaria+yCorri));
          if(CTC != null && CTC.getCurrentValue() <= 1 && boardutils.checkForUnrotatable(CTC) == false && boardutils.isUntargetableByBronze(CTC) == false)
          {
            secondAbilityAvailable = true;
            break;
          }
        }
      }
      if(firstAbilityAvailable == true || secondAbilityAvailable == true)
      {
        return true;
      }
      else
      {
        return false;
      }
      break;

    case "dart":
      var result = false;

      cards = board.getAllCards();
      var entryCondition = false;
      var instantlyTrashable = true;
      var instantlyMovable = true;
      var xVaria = choiceForMegaphone.getPosX();
      var yVaria = choiceForMegaphone.getPosY();

      cardsCF = board.getAllCards();
      for(cfut = 0; cfut < cardsCF.length; cfut++)
      {
        if(board.isSafeTile(Point(xVaria,yVaria)) == true)
        {
          instantlyTrashable = false;
          break;
        }

        if((cardsCF[cfut].getID() == "celticshield" || (cardsCF[cfut].getID() == "handmirror" && boardutils.itemMirrored(cardsCF[cfut]) == "celticshield")) && cardsCF[cfut].isGrayScale() == false && cardsCF[cfut].getPlayer() == gameState.getCurrentPlayer())
        {
          var shieldDistance = Math.pow(cardsCF[cfut].getPosX()-xVaria, 2) + Math.pow(cardsCF[cfut].getPosY()-yVaria, 2)
          if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
          {
            instantlyTrashable = false;
            break;
          }
        }

        if((cardsCF[cfut].getID() == "hardhat" || (cardsCF[cfut].getID() == "handmirror" && boardutils.itemMirrored(cardsCF[cfut]) == "hardhat")) && cardsCF[cfut].isGrayScale() == false)
        {
          switch(cardsCF[cfut].getPlayer())
          {
            case 1:
              if(cardsCF[cfut].getPosX() == xVaria && cardsCF[cfut].getPosY() == yVaria-1) {instantlyTrashable = false;}
              break;
            case 2:
              if(cardsCF[cfut].getPosX() == xVaria && cardsCF[cfut].getPosY() == yVaria+1) {instantlyTrashable = false;}
              break;
            case 3:
              if(cardsCF[cfut].getPosX() == xVaria+1 && cardsCF[cfut].getPosY() == yVaria) {instantlyTrashable = false;}
              break;
            case 4:
              if(cardsCF[cfut].getPosX() == xVaria-1 && cardsCF[cfut].getPosY() == yVaria) {instantlyTrashable = false;}
              break;
          }
        }
      }
      if(board.isInnovativeTile(Point(xVaria,yVaria)) == false)
      {
        for(cfim = 0; cfim < cardsCF.length; cfim++)
        {
          if((cardsCF[cfim].getID() == "gluetube" || (cardsCF[cfim].getID() == "handmirror" && boardutils.itemMirrored(cardsCF[cfim]) == "gluetube")) && cardsCF[cfim].isGrayScale() == false)
          {
            var tubeDistance = Math.pow(cardsCF[cfim].getPosX()-xVaria, 2) + Math.pow(cardsCF[cfim].getPosY()-yVaria, 2)
            if(tubeDistance == 0 || tubeDistance == 1 || tubeDistance == 2)
            {
              instantlyMovable = false;
              break;
            }
          }
        }
      }
      else
      {
        instantlyMovable = false;
      }

      if(instantlyTrashable == true)
      {
        for(xVariab = 0; xVariab < board.getWidth(); xVariab++)
        {
          if(xVariab == xVaria)
          {
            xVariab++;
          }
          //If the target tile is not safe
          var CTC = board.getCardAtPosition(Point(xVariab,yVaria));
          if(CTC != null && boardutils.checkForUntrashable(CTC) == false && instantlyMovable == true)
          {
            result = true;
            break;
          }
          if(CTC != null && boardutils.checkForUntrashable(CTC) == true && CTC.getCurrentValue() > 3)
          {
            result = true;
            break;
          }
          if(CTC != null && instantlyMovable == false && CTC.getCurrentValue() > 3)
          {
            result = true;
            break;
          }
        }

        for(yVariab = 0; yVariab < board.getHeight(); yVariab++)
        {
          if(yVariab == yVaria)
          {
            yVariab++;
          }
          var CTC = board.getCardAtPosition(Point(xVaria,yVariab));
          if(CTC != null && boardutils.checkForUntrashable(CTC) == false && instantlyMovable == true)
          {
            result = true;
            break;
          }
          if(CTC != null && boardutils.checkForUntrashable(CTC) == true && CTC.getCurrentValue() > 3)
          {
            result = true;
            break;
          }
          if(CTC != null && instantlyMovable == false && CTC.getCurrentValue() > 3)
          {
            result = true;
            break;
          }
        }

        if(boardutils.doesTilePreventDamaging(xMega, yMega) == false && instantlyMovable == true)
        {
          result = true;
        }
        if(boardutils.doesTilePreventDamaging(xMega, yMega) == true && board.isDoubleTile(Point(xMega,yMega)) == true)
        {
          result = true;
        }
        if(instantlyMovable == false && board.isDoubleTile(Point(xMega,yMega)) == true)
        {
          result = true;
        }
      }

      if(instantlyTrashable == false && instantlyMovable == true)
      {
        //If the tile is safe, the only allowed targets are the ones with 100% likeliness to be hit
        for(xVariab = 0; xVariab < board.getWidth(); xVariab++)
        {
          var CTC = board.getCardAtPosition(Point(xVariab,yVaria));
          if(CTC != null && boardutils.checkForUntrashable(CTC) == false && ((CTC.getCurrentValue() <= 0 && boardutils.isSixFix(gameState.getCurrentPlayer()) == false) || (CTC.getCurrentValue() <= 3 && boardutils.isSixFix(gameState.getCurrentPlayer()) == true)))
          {
            result = true;
          }

          for(yVariab = 0; yVariab < board.getHeight(); yVariab++)
          {
            var CTC = board.getCardAtPosition(Point(xVaria,yVariab));
            if(CTC != null && boardutils.checkForUntrashable(CTC) == false && ((CTC.getCurrentValue() <= 0 && boardutils.isSixFix(gameState.getCurrentPlayer()) == false) || (CTC.getCurrentValue() <= 3 && boardutils.isSixFix(gameState.getCurrentPlayer()) == true)))
            {
              result = true;
            }
          }
        }
        if(board.isDoubleTile(Point(xMega,yMega)) == false && boardutils.doesTilePreventDamaging(xMega, yMega) == false && boardutils.isSixFix(gameState.getCurrentPlayer()) == true)
        {
          result = true;
        }
      }
      return result;
      break;

    case "hourglass":
      var xVaria = choiceForMegaphone.getPosX();
      var yVaria = choiceForMegaphone.getPosY();
      if(!board.isSafeTile(choiceForMegaphone.getPosition()))
      {
        cardsCFUR = board.getAllCards();
        var instantlyRotatable = true;
        for(cfur = 0; cfur < cardsCFUR.length; cfur++)
        {

          if((cardsCFUR[cfur].getID() == "celticshield" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "celticshield")) && cardsCFUR[cfur].isGrayScale() == false && cardsCFUR[cfur].getPlayer() == gameState.getCurrentPlayer())
          {
            var shieldDistance = Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2)
            if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
            {
              instantlyRotatable = false;
              break;
            }
          }

          if(card.getID() != "padlock" || (card.getID() == "padlock" && card.isGrayScale() == true))
          {
            if(cardsCFUR[cfur].getID() == "padlock" && cardsCFUR[cfur].isGrayScale() == false && Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2) == 1)
            {
              instantlyRotatable = false;
              break;
            }
          }
          else
          {
            instantlyRotatable = false;
            break;
          }

          if((cardsCFUR[cfur].getID() == "hardhat" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "hardhat")) && cardsCFUR[cfur].isGrayScale() == false)

          {
            switch(cardsCFUR[cfur].getPlayer())
            {
              case 1: if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria-1) {instantlyRotatable = false;} break;
              case 2: if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria+1) {instantlyRotatable = false;} break;
              case 3: if(cardsCFUR[cfur].getPosX() == xVaria+1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;} break;
              case 4: if(cardsCFUR[cfur].getPosX() == xVaria-1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;} break;
            }
          }

        }

        if(instantlyRotatable == true)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
      else
      {
        return false;
      }
      break;

    case "documentholder":
      if(mainInventory.getAllCards().length > 0)
      {
        return true;
      }
      else
      {
        return false;
      }
      break;

    case "hammer":
      var xVaria = choiceForMegaphone.getPosX();
      var yVaria = choiceForMegaphone.getPosY();
      for(ttc = 2; ttc < 10; ttc = ttc+2)
      {
        var xCorri = 0;
        var yCorri = 0;
        if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
        if(ttc == 4 && xVaria > 0) {xCorri = -1;}
        if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
        if(ttc == 8 && yVaria > 0) {yCorri = -1;}
        CTC = board.getCardAtPosition(Point(xVaria+xCorri,yVaria+yCorri));
        if(CTC != null && CTC.getUID() != choiceForMegaphone.getUID() && boardutils.checkForUntrashable(CTC) == false && boardutils.doesTilePreventDamaging(xMega, yMega) == false)
        {
          return true;
          break; break;
        }
      }
      return false;
      break;

    case "coffeegrinder":
      if((gameState.getCurrentPlayer() == 1 && p1Inventory.getAllCards().length > 2) || (gameState.getCurrentPlayer() == 2 && p2Inventory.getAllCards().length > 2) || (gameState.getCurrentPlayer() == 3 && p3Inventory.getAllCards().length > 2) || (gameState.getCurrentPlayer() == 4 && p4Inventory.getAllCards().length > 2))
      {
        if(mainInventory.getAllCards().length > 0)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
      else
      {
        return false;
      }
      break;

    case "celticshield":
      if((gameState.getCurrentPlayer() == 1 && p1Inventory.getAllCards().length > 1) || (gameState.getCurrentPlayer() == 2 && p2Inventory.getAllCards().length > 1) || (gameState.getCurrentPlayer() == 3 && p3Inventory.getAllCards().length > 1) || (gameState.getCurrentPlayer() == 4 && p4Inventory.getAllCards().length > 1))
      {
        return true;
      }
      else
      {
        return false;
      }
      break;

    case "gardenspade":
      //In the extremely unlikely case neither the trash nor the storage contains two items
      if(2 > (trash.getAllCards()).length && 2 > (mainInventory.getAllCards()).length)
      {
        var xVaria = choiceForMegaphone.getPosX();
        var yVaria = choiceForMegaphone.getPosY();
        for(ttc = 2; ttc < 10; ttc = ttc+2)
        {
          var xCorri = 0;
          var yCorri = 0;
          if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
          if(ttc == 4 && xVaria > 0) {xCorri = -1;}
          if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
          if(ttc == 8 && yVaria > 0) {yCorri = -1;}
          CTC = board.getCardAtPosition(Point(xVaria+xCorri,yVaria+yCorri));

          if(CTC != null && CTC.getUID() != choiceForMegaphone.getUID())
          {
            var targetValue = CTC.getBaseValue()
            if(targetValue > 0 && boardutils.isUntargetableByBronze(CTC) == false && (targetValue <= (trash.getAllCards()).length || targetValue <= (mainInventory.getAllCards()).length))
            {
              return true;
              break; break;
            }
          }
        }
        return false;
      }
      else
      {
        return true;
      }
      break;

    case "compasses":
      var xVaria = choiceForMegaphone.getPosX();
      var yVaria = choiceForMegaphone.getPosY();
      for(ttc = 2; ttc < 10; ttc = ttc+2)
      {
        var xCorri = 0;
        var yCorri = 0;
        if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
        if(ttc == 4 && xVaria > 0) {xCorri = -1;}
        if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
        if(ttc == 8 && yVaria > 0) {yCorri = -1;}
        CTC = board.getCardAtPosition(Point(xVaria+xCorri,yVaria+yCorri));
        if(CTC != null && CTC.getPlayer() != gameState.getCurrentPlayer() && boardutils.checkForUnrotatable(CTC) == false && CTC.getBackground() == "wood" && CTC.getCurrentValue() <= 4)
        {
          return true;
          break; break;
        }
      }
      return false;
      break;

    case "flyswatter":
      var xVaria = choiceForMegaphone.getPosX();
      var yVaria = choiceForMegaphone.getPosY();
      for(ctc = 1; ctc < 10; ctc++)
      {
        var xCorri = 0;
        var yCorri = 0;
        if(ctc == 5) {ctc++;}

        if(ctc == 1 && xVaria > 0 && yVaria < board.getHeight() - 1) {xCorri = -1; yCorri = 1;}
        if(ctc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
        if(ctc == 3 && xVaria < board.getWidth() - 1 && yVaria < board.getHeight() - 1) {xCorri = 1; yCorri = 1;}
        if(ctc == 4 && xVaria > 0) {xCorri = -1;}
        if(ctc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
        if(ctc == 7 && xVaria > 0 && yVaria > 0) {xCorri = -1; yCorri = -1;}
        if(ctc == 8 && yVaria > 0) {yCorri = -1;}
        if(ctc == 9 && xVaria < board.getWidth() - 1 && yVaria > 0) {xCorri = 1; yCorri = -1;}

        CTC = board.getCardAtPosition(Point(xVaria+xCorri,yVaria+yCorri));
        if(CTC != null && CTC.getUID() != choiceForMegaphone.getUID() && CTC.getBaseValue() < 2 && boardutils.checkForUntrashable(CTC) == false)
        {
          return true;
          break; break;
        }
      }
      return false;
      break;

    case "operaglove":
      if(gameState.getCurrentPlayer() == 1) {var cardsInCurrentHand = p1Inventory.getAllCards().length;}
      if(gameState.getCurrentPlayer() == 2) {var cardsInCurrentHand = p2Inventory.getAllCards().length;}
      if(gameState.getCurrentPlayer() == 3) {var cardsInCurrentHand = p3Inventory.getAllCards().length;}
      if(gameState.getCurrentPlayer() == 4) {var cardsInCurrentHand = p4Inventory.getAllCards().length;}

      var xVaria = choiceForMegaphone.getPosX();
      var yVaria = choiceForMegaphone.getPosY();
      for(ttc = 2; ttc < 10; ttc = ttc+2)
      {
        var xCorri = 0;
        var yCorri = 0;
        if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
        if(ttc == 4 && xVaria > 0) {xCorri = -1;}
        if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
        if(ttc == 8 && yVaria > 0) {yCorri = -1;}
        TTC = Point(xVaria+xCorri,yVaria+yCorri);
        CTC = board.getCardAtPosition(TTC);
        if(CTC != null && CTC.getUID() != choiceForMegaphone.getUID() && boardutils.checkForUnpickable(CTC) == false)
        {
          var amountToDiscard = Math.round(CTC.getCurrentValue()/2);
          if(amountToDiscard < 1)
          {
            amountToDiscard = 1;
          }
          if(amountToDiscard <= cardsInCurrentHand + 1)
          {
            return true;
            break; break; break; break;
          }
        }
      }
      return false;
      break;

    case "football":
      if(!board.isDoubleTile(choiceForMegaphone.getPosition()) && !board.isSafeTile(choiceForMegaphone.getPosition()))
      {
        if(mainInventory.getAllCards().length > 1)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
      else
      {
        return true;
      }
      break;

    case "adhesivebandage":
      var patchedTile = 0;
      var entryCondition = false;
      var xVaria = choiceForMegaphone.getPosX();
      var yVaria = choiceForMegaphone.getPosY();
      switch(gameState.getCurrentPlayer())
      {
        case 1:
          if(board.getCardAtPosition(Point(xVaria,yVaria+1)) == null && yVaria < board.getHeight()-1)
          {
            patchedTile = Point(xVaria,yVaria+1);
          }
          break;
        case 2:
          if(board.getCardAtPosition(Point(xVaria,yVaria-1)) == null && yVaria > 0)
          {
            patchedTile = Point(xVaria,yVaria-1);
          }
          break;
        case 3:
          if(board.getCardAtPosition(Point(xVaria-1,yVaria)) == null && xVaria > 0)
          {
            patchedTile = Point(xVaria-1,yVaria);
          }
          break;
        case 4:
          if(board.getCardAtPosition(Point(xVaria+1,yVaria)) == null && xVaria < board.getWidth()-1)
          {
            patchedTile = Point(xVaria+1,yVaria);
          }
          break;
      }
      if(patchedTile != 0 && board.getCardAtPosition(patchedTile) == null && board.isDisabledTile(patchedTile) == false)
      {
        var trashCards = trash.getAllCards();
        for(i = 0; i < trashCards.length; i++)
        {
          if(patchutils.secondBandageCondition(trashCards[i], patchedTile.x, patchedTile.y) == true)
          {
            entryCondition = true;
            break;
          }
        }
      }
      if(entryCondition == true)
      {
        return true;
      }
      else
      {
        return false;
      }
      break;

    case "broom":
      eT = boardutils.getEmptyTiles();
      var xVaria = choiceForMegaphone.getPosX();
      var yVaria = choiceForMegaphone.getPosY();
      if(boardutils.isHaltedByOne(eT, xMega, yMega) == false && board.isDoubleTile(Point(xVaria,yVaria)) == false)
      {
        cards = board.getAllCards();
        var countMovableX = 0
        var countMovableY = 0
        var countImmovableX = 0
        var countImmovableY = 0
        var xDisab = 0
        var yDisab = 0

        if(gameState.getCardsWithActiveBronzeAbility().length == 0)
        {
          if(xVaria == xMega)
          {
            if(boardutils.doesTilePreventMoving(xMega, yMega) == false)
            {
              countMovableX++;
            }
            else
            {
              countImmovableX++;
            }
          }
          if(yVaria == yMega)
          {
            if(boardutils.doesTilePreventMoving(xMega, yMega) == false)
            {
              countMovableY++;
            }
            else
            {
              countImmovableY++;
            }
          }
        }

        for(xVariab = 0; xVariab < board.getWidth(); xVariab++)
        {
          if(board.isDisabledTile(Point(xVariab,yVaria)) == true)
          {
            xDisab++;
          }
        }
        for(yVariab = 0; yVariab < board.getHeight(); yVariab++)
        {
          if(board.isDisabledTile(Point(xVaria,yVariab)) == true)
          {
            yDisab++;
          }
        }

        for(i = 0; i < cards.length; i++)
        {
          if(cards[i].getPosX() == xVaria && cards[i].getPosY() != yVaria)
          {
            if(boardutils.checkForImmovable(cards[i]) == true || boardutils.isUntargetableByBronze(cards[i]) == true)
            {
              countImmovableX++;
            }
            else
            {
              countMovableX++;
            }
          }
          if(cards[i].getPosY() == yVaria && cards[i].getPosX() != xVaria)
          {
            if(boardutils.checkForImmovable(cards[i]) == true || boardutils.isUntargetableByBronze(cards[i]) == true)
            {
              countImmovableY++;
            }
            else
            {
              countMovableY++;
            }
          }
        }

if(xMega == 3 && yMega == 3)
{
        menu.showInformation(qsTranslate("megaphone", "%1 %2 %3 %4").arg(countMovableX).arg(countImmovableX).arg(countMovableY).arg(countImmovableY));
}

        if(countMovableX + countImmovableX + countMovableY + countImmovableY < (board.getWidth() + board.getHeight() - xDisab - yDisab - 2) && countMovableX + countMovableY > 0)
        {
          if((countMovableX == 0 && countMovableY + countImmovableY == board.getWidth() - xDisab - 1) || (countMovableY == 0 && countMovableX + countImmovableX == board.getHeight() - yDisab - 1))
          {
            return false;
          }
          else
          {
            return true;
          }
        }
        else
        {
          return false;
        }
      }
      else
      {
        return true;
      }
      break;

    case "fryingpan":
      var xVaria = choiceForMegaphone.getPosX();
      var yVaria = choiceForMegaphone.getPosY();
      var xCorri = 0;
      var yCorri = 0;
      if(card.getPlayer() == 1 && yVaria < board.getHeight() - 1) {yCorri = -1;}
      if(card.getPlayer() == 2 && yVaria > 0) {yCorri = 1;}
      if(card.getPlayer() == 3 && xVaria > 0) {xCorri = 1;}
      if(card.getPlayer() == 4 && xVaria < board.getWidth() - 1) {xCorri = -1;}
      CTC = board.getCardAtPosition(Point(xVaria+xCorri,yVaria+yCorri));
      if(CTC == null)
      {
        return true;
      }
      if(CTC != null && CTC != choiceForMegaphone)
      {
        if(boardutils.checkForUntrashable(CTC) == false || CTC.getCurrentValue() >= 7)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
      else
      {
        return true;
      }
      break;

    case "souvenir":
      var sumCardsInHands = 0;
      switch(gameState.getGameMode())
      {
        case 0:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length);
          break;
        case 1:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length + p4Inventory.getAllCards().length);
          break;
        case 2:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length);
          break;
        case 3:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length + p4Inventory.getAllCards().length);
          break;
        case 4:
          sumCardsInHands = (p1Inventory.getAllCards().length + p3Inventory.getAllCards().length);
          break;
      }
      switch(gameState.getCurrentPlayer())
      {
        case 1:
          sumCardsInHands = sumCardsInHands - p1Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p4Inventory.getAllCards().length;}
          break;
        case 2:
          sumCardsInHands = sumCardsInHands - p2Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p3Inventory.getAllCards().length;}
          break;
        case 3:
          sumCardsInHands = sumCardsInHands - p3Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p2Inventory.getAllCards().length;}
          break;
        case 4:
          sumCardsInHands = sumCardsInHands - p4Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p1Inventory.getAllCards().length;}
          break;
      }
      //If there is at least one card in the hand of one opponent
      if(sumCardsInHands > 0)
      {
        var xVaria = choiceForMegaphone.getPosX();
        var yVaria = choiceForMegaphone.getPosY();
        if(!board.isSafeTile(choiceForMegaphone.getPosition()))
        {
          cardsCFUR = board.getAllCards();
          var instantlyRotatable = true;
          for(cfur = 0; cfur < cardsCFUR.length; cfur++)
          {

            if((cardsCFUR[cfur].getID() == "celticshield" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "celticshield")) && cardsCFUR[cfur].isGrayScale() == false && cardsCFUR[cfur].getPlayer() == gameState.getCurrentPlayer())
            {
              var shieldDistance = Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2)
              if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
              {
                instantlyRotatable = false;
                break;
              }
            }

            if(card.getID() != "padlock" || (card.getID() == "padlock" && card.isGrayScale() == true))
            {
              if(cardsCFUR[cfur].getID() == "padlock" && cardsCFUR[cfur].isGrayScale() == false && Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2) == 1)
              {
                instantlyRotatable = false;
                break;
              }
            }
            else
            {
              instantlyRotatable = false;
              break;
            }

            if((cardsCFUR[cfur].getID() == "hardhat" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "hardhat")) && cardsCFUR[cfur].isGrayScale() == false)

            {
              switch(cardsCFUR[cfur].getPlayer())
              {
                case 1: if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria-1) {instantlyRotatable = false;} break;
                case 2: if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria+1) {instantlyRotatable = false;} break;
                case 3: if(cardsCFUR[cfur].getPosX() == xVaria+1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;} break;
                case 4: if(cardsCFUR[cfur].getPosX() == xVaria-1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;} break;
              }
            }

          }

          if(instantlyRotatable == true)
          {
            return true;
          }
          else
          {
            return false;
          }
        }
        else
        {
          return false;
        }
      }
      else
      {
        return false;
      }
      break;

    case "binoculars":
      eT = boardutils.getEmptyTiles();
      if(boardutils.isHaltedByOne(eT, xMega, yMega) == false)
      {
        if(mainInventory.getAllCards().length == 0)
        {
          if(board.isDoubleTile(choiceForMegaphone.getPosition()))
          {
            return true;
          }
          else
          {
            return false;
          }
        }
        else
        {
          return true;
        }
      }
      else
      {
        return true;
      }
      break;

    case "whiteknight":
      if(mainInventory.getAllCards().length > 0)
      {
        return true;
      }
      else
      {
        return false;
      }
      break;

    default:
      return true;
      break;

  }
}
