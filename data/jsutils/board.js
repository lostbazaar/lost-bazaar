//@include jsutils.point


var boardutils = new Object();

boardutils.getEmptyTiles = function()
{
    var result = [];
    var emptyTiles = board.getEmptyTilesAsIntList();
    
    for(var i = 0; i < emptyTiles.length; i+=2)
    {
        result.push(Point(emptyTiles[i], emptyTiles[i+1]));
    }
    return result;
}

if(typeof Qt === "undefined") {
    boardutils.tileChoice = board.tileChoiceInternal;
} else {
    boardutils.tileChoice = function(points, cancellable, overlayList) {
        if(overlayList === undefined) overlayList = [];
        var newPoints = [];
        for(var i = 0; i < points.length; i++) {
            newPoints.push(points[i].x, points[i].y)
        }
        return board.tileChoiceInternalIntList(newPoints, cancellable, overlayList);
    }
}

boardutils.getAdjacentEmptyTiles = function(pos)
{
  result = new Array();
  if(board.getCardAtPosition(Point(pos.x,pos.y-1)) == null && 0 < pos.y)
  {
    if(board.isDisabledTile(Point(pos.x,pos.y-1)) == false)
    {
      result.push(Point(pos.x,pos.y-1));
    }
  }
  if(board.getCardAtPosition(Point(pos.x-1,pos.y)) == null && 0 < pos.x)
  {
    if(board.isDisabledTile(Point(pos.x-1,pos.y)) == false)
    {
      result.push(Point(pos.x-1,pos.y));
    }
  }
  if(board.getCardAtPosition(Point(pos.x+1,pos.y)) == null && pos.x < board.getWidth()-1)
  {
    if(board.isDisabledTile(Point(pos.x+1,pos.y)) == false)
    {
      result.push(Point(pos.x+1,pos.y));
    }
  }
  if(board.getCardAtPosition(Point(pos.x,pos.y+1)) == null && pos.y < board.getHeight()-1)
  {
    if(board.isDisabledTile(Point(pos.x,pos.y+1)) == false)
    {
      result.push(Point(pos.x,pos.y+1));
    }
  }
return result;
}


boardutils.getPossibleKnightMoves = function(pos)
{
  result = new Array();
  if(board.getCardAtPosition(Point(pos.x+1,pos.y+2)) == null && pos.x < board.getWidth()-1 && pos.y < board.getHeight()-2)
  {
    if(board.isDisabledTile(Point(pos.x+1,pos.y+2)) == false)
    {
      result.push(Point(pos.x+1,pos.y+2));
    }
  }
  if(board.getCardAtPosition(Point(pos.x+2,pos.y+1)) == null && pos.x < board.getWidth()-2 && pos.y < board.getHeight()-1)
  {
    if(board.isDisabledTile(Point(pos.x+2,pos.y+1)) == false)
    {
      result.push(Point(pos.x+2,pos.y+1));
    }
  }
  if(board.getCardAtPosition(Point(pos.x+2,pos.y-1)) == null && pos.x < board.getWidth()-2 && 0 < pos.y)
  {
    if(board.isDisabledTile(Point(pos.x+2,pos.y-1)) == false)
    {
      result.push(Point(pos.x+2,pos.y-1));
    }
  }
  if(board.getCardAtPosition(Point(pos.x+1,pos.y-2)) == null && pos.x < board.getWidth()-1 && 1 < pos.y)
  {
    if(board.isDisabledTile(Point(pos.x+1,pos.y-2)) == false)
    {
      result.push(Point(pos.x+1,pos.y-2));
    }
  }
  if(board.getCardAtPosition(Point(pos.x-1,pos.y-2)) == null && 0 < pos.x && 1 < pos.y)
  {
    if(board.isDisabledTile(Point(pos.x-1,pos.y-2)) == false)
    {
      result.push(Point(pos.x-1,pos.y-2));
    }
  }
  if(board.getCardAtPosition(Point(pos.x-2,pos.y-1)) == null && 1 < pos.x && 0 < pos.y)
  {
    if(board.isDisabledTile(Point(pos.x-2,pos.y-1)) == false)
    {
      result.push(Point(pos.x-2,pos.y-1));
    }
  }
  if(board.getCardAtPosition(Point(pos.x-2,pos.y+1)) == null && 1 < pos.x && pos.y < (board.getHeight()-1))
  {
    if(board.isDisabledTile(Point(pos.x-2,pos.y+1)) == false)
    {
      result.push(Point(pos.x-2,pos.y+1));
    }
  }
  if(board.getCardAtPosition(Point(pos.x-1,pos.y+2)) == null && 0 < pos.x && pos.y < (board.getHeight()-2))
  {
    if(board.isDisabledTile(Point(pos.x-1,pos.y+2)) == false)
    {
      result.push(Point(pos.x-1,pos.y+2));
    }
  }
return result;
}


boardutils.getInCrossEmptyTiles = function(pos)
{
  result = new Array();
  for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
  {
    if(board.getCardAtPosition(Point(xVaria,pos.y)) == null && card.getPosY() == pos.y)
    {
      if(board.isDisabledTile(Point(xVaria,pos.y)) == false)
      {
        result.push(Point(xVaria,pos.y));
      }
    }
  }
  for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
  {
    if(board.getCardAtPosition(Point(pos.x,yVaria)) == null && card.getPosX() == pos.x)
    {
      if(board.isDisabledTile(Point(pos.x,yVaria)) == false)
      {
        result.push(Point(pos.x,yVaria));
      }
    }
  }
return result;
}


boardutils.isSixFix = function(currentPlayer)
{
  var result = false
  cardsISF = board.getAllCards();
  for(isf = 0; isf < cardsISF.length; isf++)
  {
    if((cardsISF[isf].getID() == "trickydie" || (cardsISF[isf].getID() == "handmirror" && boardutils.itemMirrored(cardsISF[isf]) == "trickydie")) && cardsISF[isf].getPlayer() == currentPlayer && cardsISF[isf].isGrayScale() == false)
    {
      result = true;
      break;
    }
  }
  return result;
}


boardutils.numberToFace = function(numberRolled)
{
  var result = ""
  switch(numberRolled)
  {
    case 1: result="⚀"; break;
    case 2: result="⚁"; break;
    case 3: result="⚂"; break;
    case 4: result="⚃"; break;
    case 5: result="⚄"; break;
    case 6: result="⚅"; break;
  }
  return result;
}


boardutils.checkForUnpickable = function(cardToCheck)
{
  var result = false
  cardsCFUP = board.getAllCards()
  for(cfup = 0; cfup < cardsCFUP.length; cfup++)
  {
    if(cardToCheck.hasIcon("noup") == false)
    {
      if(cardsCFUP[cfup].getID() == "padlock" && cardsCFUP[cfup].isGrayScale() == false && Math.pow(cardsCFUP[cfup].getPosX()-cardToCheck.getPosX(), 2) + Math.pow(cardsCFUP[cfup].getPosY()-cardToCheck.getPosY(), 2) == 1)
      {
        result = true;
        break;
      }
    }
    else
    {
      result = true;
      break;
    }
  }
  return result;
}


boardutils.isHalted = function(emptyTiles)
{
  var result = false
  for(IH = 0; IH < emptyTiles.length; IH++)
  {
    if(board.isDoubleTile(emptyTiles[IH]))
    {
      result = true;
      break;
    }
  }
  return result;
}


boardutils.isHaltedByOne = function(emptyTiles, xOccupy, yOccupy)
{
  var result = false
  for(IH = 0; IH < emptyTiles.length; IH++)
  {
    if(board.isDoubleTile(emptyTiles[IH]) && (emptyTiles[IH].x != xOccupy || emptyTiles[IH].y != yOccupy))
    {
      result = true;
      break;
    }
  }
  return result;
}



boardutils.haltedByNet = function(baseValue)
{
  var result = false;
  cardsIHBN = board.getAllCards();
  netCounters = []
  for(IHBN = 0; IHBN < cardsIHBN.length; IHBN++)
  {
    if(cardsIHBN[IHBN].getID() == "net" && cardsIHBN[IHBN].isGrayScale() == false && cardsIHBN[IHBN].getCounter() > 0)
    {
      netCounters.push(cardsIHBN[IHBN].getCounter());
    }
  }
  if(cardsIHBN.length > 0 && baseValue < Math.max.apply(Math, netCounters))
  {
    result = true;
  }
  return result;
}


boardutils.convertToABC = function(xCoord)
{
  var lettersOfABC = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
  return lettersOfABC[xCoord];
}


boardutils.logCardPlacement = function(placedCardName)
{
  var userNames = gameState.getPlayerNickNames();
  gameState.writeToGameLog(qsTranslate("board", "<font color=\"bisque\">%1 placed the %2 on %3%4.</font>").arg(userNames[gameState.getCurrentPlayer()-1]).arg(placedCardName).arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1), [1, 2, 3, 4], 1, 0);
}


boardutils.Innovate = function(xCoord, yCoord, currentPlayer)
{
  if(board.isInnovativeTile(Point(xCoord,yCoord)) == true)
  {
    gameState.drawCard(currentPlayer, 0, 0, 0, 0);
    menu.showInformation(qsTranslate("innovation", "You have drawn an item!"));
  }
}


boardutils.Enpower = function(xCoord, yCoord)
{
  if(board.isPowerTile(Point(xCoord,yCoord)) == true && card.getCounter() < 12)
  {
    card.setCounter(card.getCounter()+1);
    //Log
    gameState.writeToGameLog(qsTranslate("power", "%1%2 %3 gained 1 counter.").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()), [1, 2, 3, 4], 1, 0);
  }
}

boardutils.getFashionBonus = function(playerWithFashion)
{
  var fashionControl = 0;
  cardsGFB = board.getAllCards();
  for(GFB = 0; GFB < cardsGFB.length; GFB++)
  {
    if(board.isFashionTile(cardsGFB[GFB].getPosition()) == true && cardsGFB[GFB].getPlayer() == playerWithFashion)
    {
      fashionControl++;
    }
  }
  return fashionControl;
}


boardutils.isThereAdjacentSponge = function()
{
  var result = false;
  cardsITAS = board.getAllCards();
  for(ITAS = 0; ITAS < cardsITAS.length; ITAS++)
  {
    if((cardsITAS[ITAS].getID() == "sponge" || (cardsITAS[ITAS].getID() == "handmirror" && boardutils.itemMirrored(cardsITAS[ITAS]) == "sponge")) && cardsITAS[ITAS].isGrayScale() == false && cardsITAS[ITAS].getPlayer != card.getPlayer())
    {
      if(Math.pow(card.getPosX()-cardsITAS[ITAS].getPosX(), 2) + Math.pow(card.getPosY()-cardsITAS[ITAS].getPosY(), 2) == 1)
      {
        result = true;
        break;
      }
    }
  }
  return result;
}


boardutils.isThereAdjacentHat = function()
{
  var result = false;
  cardsITAH = board.getAllCards();
  for(ITAH = 0; ITAH < cardsITAH.length; ITAH++)
  {
    if((cardsITAH[ITAH].getID() == "hardhat" || (cardsITAH[ITAH].getID() == "handmirror" && boardutils.itemMirrored(cardsITAH[ITAH]) == "hardhat")) && cardsITAH[ITAH].isGrayScale() == false)
    {
      if(Math.pow(card.getPosX()-cardsITAH[ITAH].getPosX(), 2) + Math.pow(card.getPosY()-cardsITAH[ITAH].getPosY(), 2) == 1)
      {
        if( (cardsITAH[ITAH].getPlayer() == 1 && cardsITAH[ITAH].getPosX() == card.getPosX() && cardsITAH[ITAH].getPosY()+1 == card.getPosY()) ||
            (cardsITAH[ITAH].getPlayer() == 2 && cardsITAH[ITAH].getPosX() == card.getPosX() && cardsITAH[ITAH].getPosY()-1 == card.getPosY()) ||
            (cardsITAH[ITAH].getPlayer() == 3 && cardsITAH[ITAH].getPosX()-1 == card.getPosX() && cardsITAH[ITAH].getPosY() == card.getPosY()) ||
            (cardsITAH[ITAH].getPlayer() == 4 && cardsITAH[ITAH].getPosX()+1 == card.getPosX() && cardsITAH[ITAH].getPosY() == card.getPosY()) )
        {
          result = true;
          break;
        }
      }
    }
  }
  return result;
}


boardutils.isThereAdjacentBat = function()
{
  var result = false;
  cardsITAB = board.getAllCards();
  for(ITAB = 0; ITAB < cardsITAB.length; ITAB++)
  {
    if((cardsITAB[ITAB].getID() == "baseballbat" || (cardsITAB[ITAB].getID() == "handmirror" && boardutils.itemMirrored(cardsITAB[ITAB]) == "baseballbat")) && cardsITAB[ITAB].isGrayScale() == false && cardsITAB[ITAB].getPlayer != card.getPlayer())
    {
      if(Math.pow(card.getPosX()-cardsITAB[ITAB].getPosX(), 2) + Math.pow(card.getPosY()-cardsITAB[ITAB].getPosY(), 2) == 1)
      {
        result = true;
        break;
      }
    }
  }
  return result;
}


//This version of the function is redundant and will be removed later
boardutils.isProtectedByBirdfeeder = function(cardToCheck)
{
  var result = false;
  if(cardToCheck.getID() == "mask")
  {
    result = true;
  }
  if(cardToCheck.getCounter() > 0)
  {
    cardsIPBB = board.getAllCards();
    inGameFeeders = []
    for(IPBB = 0; IPBB < cardsIPBB.length; IPBB++)
    {
      if((cardsIPBB[IPBB].getID() == "birdfeeder" || (cardsIPBB[IPBB].getID() == "handmirror" && boardutils.itemMirrored(cardsIPBB[IPBB]) == "handmirror")) && cardsIPBB[IPBB].isGrayScale() == false && cardsIPBB[IPBB].getPlayer() == cardToCheck.getPlayer())
      {
        inGameFeeders.push(cardsIPBB[IPBB]);
      }
    }
    if(inGameFeeders.length > 0)
    {
      for(feederNr = 0; feederNr < inGameFeeders.length; feederNr++)
      {
        if(cardToCheck.getPosX() == inGameFeeders[feederNr].getPosX() || cardToCheck.getPosY() == inGameFeeders[feederNr].getPosY())
        {
          result = true;
          break;
        }
      }
    }
  }
  return result;
}


boardutils.isUntargetableBySilver = function(cardToCheck)
{
  var result = false;
  if(cardToCheck.getID() == "mask" || (cardToCheck.getID() == "handmirror" && boardutils.itemMirrored(cardToCheck) == "mask"))
  {
    result = true;
  }
  if(cardToCheck.getCounter() > 0)
  {
    cardsIUBS = board.getAllCards();
    inGameFeeders = []
    for(IUBS = 0; IUBS < cardsIUBS.length; IUBS++)
    {
      if((cardsIUBS[IUBS].getID() == "birdfeeder" || (cardsIUBS[IUBS].getID() == "handmirror" && boardutils.itemMirrored(cardsIUBS[IUBS]) == "birdfeeder")) && cardsIUBS[IUBS].isGrayScale() == false && cardsIUBS[IUBS].getPlayer() == cardToCheck.getPlayer())
      {
        inGameFeeders.push(cardsIUBS[IUBS]);
      }
    }
    if(inGameFeeders.length > 0)
    {
      for(feederNr = 0; feederNr < inGameFeeders.length; feederNr++)
      {
        if(cardToCheck.getPosX() == inGameFeeders[feederNr].getPosX() || cardToCheck.getPosY() == inGameFeeders[feederNr].getPosY())
        {
          result = true;
          break;
        }
      }
    }
  }
  return result;
}


boardutils.isUntargetableByBronze = function(cardToCheck)
{
  var result = false;
  if((cardToCheck.getID() == "mask" || (cardToCheck.getID() == "handmirror" && boardutils.itemMirrored(cardToCheck) == "mask")) && cardToCheck.isGrayScale() == false)
  {
    result = true;
  }
  return result;
}


boardutils.hasAnyIcons = function(cardToCheck)
{
  var result = false;
  var allIcons = ["optu", "noup", "dmno", "cafe", "crar", "tcan", "stop"];
  for(alli = 0; alli < 7; alli++)
  {
    if(cardToCheck.hasIcon(allIcons[alli]))
    {
      return true;
      break;
    }
  }
  return result;
}


boardutils.defaultCurrentValue = function()
{
  var calcValue = card.getBaseValue();

  if(card.hasIcon("stop") == true && board.isDoubleTile(card.getPosition()))
  {
    calcValue = calcValue - 1;
  }

  if(boardutils.isThereAdjacentSponge() == true)
  {
    var spongeMalus = 0;
    cardsITAS = board.getAllCards();
    for(ITAS = 0; ITAS < cardsITAS.length; ITAS++)
    {
      if((cardsITAS[ITAS].getID() == "sponge" || (cardsITAS[ITAS].getID() == "handmirror" && boardutils.itemMirrored(cardsITAS[ITAS]) == "sponge")) && cardsITAS[ITAS].isGrayScale() == false && cardsITAS[ITAS].getPlayer() != card.getPlayer())
      {
        if(Math.pow(card.getPosX()-cardsITAS[ITAS].getPosX(), 2) + Math.pow(card.getPosY()-cardsITAS[ITAS].getPosY(), 2) == 1)
        {
          spongeMalus++;
        }
      }
    }
    calcValue = calcValue - spongeMalus;
  }

  if(boardutils.isThereAdjacentHat() == true)
  {
    var hatBonus = 1;
    var hatMultiplier = 0;
    if(card.hasIcon("optu") == true) {hatBonus++;}
    if(card.hasIcon("noup") == true) {hatBonus++;}
    if(card.hasIcon("dmno") == true) {hatBonus++;}
    if(card.hasIcon("cafe") == true) {hatBonus++;}
    if(card.hasIcon("crar") == true) {hatBonus++;}
    if(card.hasIcon("tcan") == true) {hatBonus++;}
    if(card.hasIcon("stop") == true) {hatBonus++;}
    for(HTC = 1; HTC < 5; HTC++)
    {
      var xCorri = 0;
      var yCorri = 0;
      if(HTC == 1) {yCorri = -1;}
      if(HTC == 2) {yCorri = 1;}
      if(HTC == 3) {xCorri = 1;}
      if(HTC == 4) {xCorri = -1;}
      var hatToCheck = board.getCardAtPosition(Point(card.getPosX()+xCorri,card.getPosY()+yCorri));
      if(hatToCheck != null && hatToCheck.getID() == "hardhat" && hatToCheck.getPlayer() == HTC && hatToCheck.isGrayScale() == false)
      {
        hatMultiplier++;
      }
    }
    calcValue = calcValue + hatBonus * hatMultiplier;
  }

  if(boardutils.isThereAdjacentBat() == true && card.isGrayScale() == true)
  {
    var batMalus = 0;
    cardsITAB = board.getAllCards();
    for(ITAB = 0; ITAB < cardsITAB.length; ITAB++)
    {
      if((cardsITAB[ITAB].getID() == "baseballbat" || (cardsITAB[ITAB].getID() == "handmirror" && boardutils.itemMirrored(cardsITAB[ITAB]) == "baseballbat")) && cardsITAB[ITAB].isGrayScale() == false)
      {
        if(Math.pow(card.getPosX()-cardsITAB[ITAB].getPosX(), 2) + Math.pow(card.getPosY()-cardsITAB[ITAB].getPosY(), 2) == 1)
        {
          batMalus++;
        }
      }
    }
    calcValue = calcValue - batMalus;
  }
  
  if(card.getCounter() == card.getBaseValue())
  {
    var radioBonus = 0;
    cardsITR = board.getAllCards();
    for(ITR = 0; ITR < cardsITR.length; ITR++)
    {
      if((cardsITR[ITR].getID() == "radio" || (cardsITR[ITR].getID() == "handmirror" && boardutils.itemMirrored(cardsITR[ITR]) == "radio")) && cardsITR[ITR].isGrayScale() == false)
      {
        radioBonus++;
      }
    }
    calcValue = calcValue + radioBonus;
  }

  if(board.isDoubleTile(card.getPosition()) == true)
  {
    var dipperMalus = 0;
    cardsIDIC = board.getAllCards();
    for(IDIC = 0; IDIC < cardsIDIC.length; IDIC++)
    {
      if((cardsIDIC[IDIC].getID() == "honeydipper" || (cardsIDIC[IDIC].getID() == "handmirror" && boardutils.itemMirrored(cardsIDIC[IDIC]) == "honeydipper")) && cardsIDIC[IDIC].isGrayScale() == false)
      {
        if((cardsIDIC[IDIC].getPosX() == card.getPosX() || cardsIDIC[IDIC].getPosY() == card.getPosY()) && cardsIDIC[IDIC].getUID() != card.getUID())
        {
          dipperMalus++;
        }
      }
    }
    calcValue = calcValue - dipperMalus;
  }

  if(board.isDoubleTile(card.getPosition()))
  {
    calcValue = calcValue * 2;
  }
  if(board.isFashionTile(card.getPosition()))
  {
    calcValue = calcValue + boardutils.getFashionBonus(card.getPlayer());
  }

  return calcValue;
}


boardutils.limitedByPerfume = function()
{
  var result = false
  cardsLBP = board.getAllCards();
  for(lbp = 0; lbp < cardsLBP.length; lbp++)
  {
    if(cardsLBP[lbp].getID() == "perfumebottle" && cardsLBP[lbp].isGrayScale() == false)
    {
      //Ability works if there is an unoccupied tile above
      if(
      (cardsLBP[lbp].getPlayer() == 1 && cardsLBP[lbp].getPosY() > 0 && board.getCardAtPosition(Point(cardsLBP[lbp].getPosX(),cardsLBP[lbp].getPosY()-1)) == null && !board.isDisabledTile(Point(cardsLBP[lbp].getPosX(),cardsLBP[lbp].getPosY()-1))) ||
      (cardsLBP[lbp].getPlayer() == 2 && cardsLBP[lbp].getPosY() < board.getHeight()-1 && board.getCardAtPosition(Point(cardsLBP[lbp].getPosX(),cardsLBP[lbp].getPosY()+1)) == null && !board.isDisabledTile(Point(cardsLBP[lbp].getPosX(),cardsLBP[lbp].getPosY()+1))) ||
      (cardsLBP[lbp].getPlayer() == 3 && cardsLBP[lbp].getPosX() < board.getWidth()-1 && board.getCardAtPosition(Point(cardsLBP[lbp].getPosX()+1,cardsLBP[lbp].getPosY())) == null && !board.isDisabledTile(Point(cardsLBP[lbp].getPosX()+1,cardsLBP[lbp].getPosY()))) ||
      (cardsLBP[lbp].getPlayer() == 4 && cardsLBP[lbp].getPosX() > 0 && board.getCardAtPosition(Point(cardsLBP[lbp].getPosX()-1,cardsLBP[lbp].getPosY())) == null && !board.isDisabledTile(Point(cardsLBP[lbp].getPosX()-1,cardsLBP[lbp].getPosY())))
      )
      {
        result = true;
        break;
      }
    }
  }
  return result;
}


boardutils.getTilesAbovePerfume = function()
{
  result = new Array();
  eT = boardutils.getEmptyTiles();
  for(i = 0; i < eT.length; i++)
  {
    for(ptc = 1; ptc < 5; ptc++)
    {
      var xCorri = 0;
      var yCorri = 0;
      if(ptc == 1 && eT[i].y < board.getHeight() - 1) {yCorri = 1;}
      if(ptc == 2 && eT[i].y > 0) {yCorri = -1;}
      if(ptc == 3 && eT[i].x > 0) {xCorri = -1;}
      if(ptc == 4 && eT[i].x < board.getWidth() - 1) {xCorri = 1;}
      CTC = board.getCardAtPosition(Point(eT[i].x+xCorri,eT[i].y+yCorri));
      if(CTC != null && CTC != card && CTC.getID() == "perfumebottle" && CTC.getPlayer() == ptc && CTC.isGrayScale() == false)
      {
        result.push(eT[i]);
        break;
      }
    }
  }
  return result;
}


boardutils.tileAffectedByPerfume = function(xAffected, yAffected)
{
  var result = false
  for(ptc = 2; ptc < 10; ptc = ptc+2)
  {
    var xCorri = 0;
    var yCorri = 0;
    var parfumeOwner = 0;
    if(ptc == 2 && yAffected < board.getHeight() - 1) {yCorri = 1; parfumeOwner = 1;}
    if(ptc == 4 && xAffected > 0) {xCorri = -1; parfumeOwner = 3;}
    if(ptc == 6 && xAffected < board.getWidth() - 1) {xCorri = 1; parfumeOwner = 4;}
    if(ptc == 8 && yAffected > 0) {yCorri = -1; parfumeOwner = 2;}
    PTC = board.getCardAtPosition(Point(xAffected+xCorri,yAffected+yCorri));
    if(PTC != null && PTC.getID() == "perfumebottle" && PTC.getPlayer() == parfumeOwner)
    {
      result = true;
      break;
    }
  }
  return result;
}


boardutils.defaultEntryConditions = function()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    if(boardutils.limitedByPerfume() == false)
    {
      //All empty tiles are fine
      array = boardutils.getEmptyTiles();
    }
    else
    {
      array = boardutils.getTilesAbovePerfume();
    }
  }
  return array;
}


boardutils.dominoEntryConditions = function()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    var arrayNumber = 0;
    cards = board.getAllCards();
    for(i = 0; i < cards.length; i++)
    {
      if(cards[i].getPlayer() == gameState.getCurrentPlayer())
      {
        if(board.getCardAtPosition(Point(cards[i].getPosX(),cards[i].getPosY()-1)) == null && 0 < cards[i].getPosY())
        {
          if(board.isDisabledTile(Point(cards[i].getPosX(),cards[i].getPosY()-1)) == false)
          {
            array[arrayNumber] = new Point(cards[i].getPosX(),cards[i].getPosY()-1);
            arrayNumber++
          }
        }
        if(board.getCardAtPosition(Point(cards[i].getPosX()-1,cards[i].getPosY())) == null && 0 < cards[i].getPosX())
        {
          if(board.isDisabledTile(Point(cards[i].getPosX()-1,cards[i].getPosY())) == false)
          {
            array[arrayNumber] = new Point(cards[i].getPosX()-1,cards[i].getPosY());
            arrayNumber++
          }
        }
        if(board.getCardAtPosition(Point(cards[i].getPosX()+1,cards[i].getPosY())) == null && cards[i].getPosX() < board.getWidth()-1)
        {
          if(board.isDisabledTile(Point(cards[i].getPosX()+1,cards[i].getPosY())) == false)
          {
            array[arrayNumber] = new Point(cards[i].getPosX()+1,cards[i].getPosY());
            arrayNumber++
          }
        }
        if(board.getCardAtPosition(Point(cards[i].getPosX(),cards[i].getPosY()+1)) == null && cards[i].getPosY() < board.getHeight()-1)
        {
          if(board.isDisabledTile(Point(cards[i].getPosX(),cards[i].getPosY()+1)) == false)
          {
            array[arrayNumber] = new Point(cards[i].getPosX(),cards[i].getPosY()+1);
            arrayNumber++
          }
        }
      }
    }
    return array;
  }
  else
  {
    return array;
  }
}


boardutils.getNonAdjacentEmptyTiles = function()
{
  result = new Array();
  var xVaria = 0;
  var yVaria = 0;
  for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
  {
    if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false && board.getCardAtPosition(Point(xVaria+1,yVaria)) == null && board.getCardAtPosition(Point(xVaria,yVaria+1)) == null && board.getCardAtPosition(Point(xVaria-1,yVaria)) == null && board.getCardAtPosition(Point(xVaria,yVaria-1)) == null)
    {
      result.push(Point(xVaria,yVaria));
    }
    if(xVaria == board.getWidth() - 1 && yVaria < board.getHeight() - 1)
    {
      xVaria = -1;
      yVaria++;
    }
  }
return result;
}


boardutils.secondSpectaclesCondition = function(choiceForSpectacles)
{
  switch(choiceForSpectacles)
  {
    case "baseballbat":
      return false;
      break;
    case "wrench":
      var result = false;
      var xVaria = 0;
      var yVaria = 0;
      var wrenchAllowedEverywhere = false;
      var trashCards = trash.getAllCards()
      if(trashCards.length > 0)
      {
        for(j = 0; j < trashCards.length; j++)
        {
          if(trashCards[j].getBackground() == "metal")
          {
            wrenchAllowedEverywhere = true;
            break;
          }
        }
      }

      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if((board.getCardAtPosition(Point(xVaria,yVaria)) == null) && board.isDisabledTile(Point(xVaria,yVaria)) == false && (board.getCardAtPosition(Point(xVaria+1,yVaria)) == null) && (board.getCardAtPosition(Point(xVaria,yVaria+1)) == null) && (board.getCardAtPosition(Point(xVaria-1,yVaria)) == null) && (board.getCardAtPosition(Point(xVaria,yVaria-1)) == null))
          {
            if(wrenchAllowedEverywhere == true)
            {
              result = true;
              break; break;
            }
            else
            {
              for(k = 1; k < 9; k++)
              {
                var xCorri = 0;
                var yCorri = 0;
                if(k == 1) {xCorri = 1; yCorri = -2;}
                if(k == 2) {xCorri = 2; yCorri = -1;}
                if(k == 3) {xCorri = 2; yCorri = 1;}
                if(k == 4) {xCorri = 1; yCorri = 2;}
                if(k == 5) {xCorri = -1; yCorri = 2;}
                if(k == 6) {xCorri = -2; yCorri = 1;}
                if(k == 7) {xCorri = -2; yCorri = -1;}
                if(k == 8) {xCorri = -1; yCorri = -2;}

                var CTC = board.getCardAtPosition(Point(xVaria+xCorri,yVaria+yCorri));
                if(CTC != null && CTC.getCurrentValue() <= 1 && boardutils.checkForUnrotatable(CTC) == false && boardutils.isUntargetableByBronze(CTC) == false)
                {
                  result = true;
                  break; break; break;
                }
              }
            }
          }
        }
      }
      return result;
      break;
    case "dart":

      var result = false;
      var nAET = boardutils.getNonAdjacentEmptyTiles();

      cards = board.getAllCards();
      for(j = 0; j < nAET.length; j++)
      {
        var entryCondition = false;
        var instantlyTrashable = true;
        var instantlyMovable = true;
        var xVaria = nAET[j].x;
        var yVaria = nAET[j].y;

        cardsCF = board.getAllCards();
        for(cfut = 0; cfut < cardsCF.length; cfut++)
        {
          if(board.isSafeTile(nAET[j]) == true)
          {
            instantlyTrashable = false;
            break;
          }

          if((cardsCF[cfut].getID() == "celticshield" || (cardsCF[cfut].getID() == "handmirror" && boardutils.itemMirrored(cardsCF[cfut]) == "celticshield")) && cardsCF[cfut].isGrayScale() == false && cardsCF[cfut].getPlayer() == gameState.getCurrentPlayer())
          {
            var shieldDistance = Math.pow(cardsCF[cfut].getPosX()-xVaria, 2) + Math.pow(cardsCF[cfut].getPosY()-yVaria, 2)
            if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
            {
              instantlyTrashable = false;
              break;
            }
          }

          if((cardsCF[cfut].getID() == "hardhat" || (cardsCF[cfut].getID() == "handmirror" && boardutils.itemMirrored(cardsCF[cfut]) == "hardhat")) && cardsCF[cfut].isGrayScale() == false)
          {
            switch(cardsCF[cfut].getPlayer())
            {
              case 1:
                if(cardsCF[cfut].getPosX() == xVaria && cardsCF[cfut].getPosY() == yVaria-1) {instantlyTrashable = false;}
                break;
              case 2:
                if(cardsCF[cfut].getPosX() == xVaria && cardsCF[cfut].getPosY() == yVaria+1) {instantlyTrashable = false;}
                break;
              case 3:
                if(cardsCF[cfut].getPosX() == xVaria+1 && cardsCF[cfut].getPosY() == yVaria) {instantlyTrashable = false;}
                break;
              case 4:
                if(cardsCF[cfut].getPosX() == xVaria-1 && cardsCF[cfut].getPosY() == yVaria) {instantlyTrashable = false;}
                break;
            }
          }
        }
        for(cfim = 0; cfim < cardsCF.length; cfim++)
        {
          if((cardsCF[cfim].getID() == "gluetube" || (cardsCF[cfim].getID() == "handmirror" && boardutils.itemMirrored(cardsCF[cfim]) == "gluetube")) && cardsCF[cfim].isGrayScale() == false)
          {
            var tubeDistance = Math.pow(cardsCF[cfim].getPosX()-xVaria, 2) + Math.pow(cardsCF[cfim].getPosY()-yVaria, 2)
            if(tubeDistance == 0 || tubeDistance == 1 || tubeDistance == 2)
            {
              instantlyMovable = false;
              break;
            }
          }
        }

        if(instantlyTrashable == true)
        {
          for(xVariab = 0; xVariab < board.getWidth(); xVariab++)
          {
            //If the target tile is not safe
            var CTC = board.getCardAtPosition(Point(xVariab,yVaria));
            if(CTC != null && boardutils.checkForUntrashable(CTC) == false && instantlyMovable == true)
            {
              result = true;
              break;
            }
            if(CTC != null && boardutils.checkForUntrashable(CTC) == true && CTC.getCurrentValue() > 3)
            {
              result = true;
              break;
            }
            if(CTC != null && instantlyMovable == false && CTC.getCurrentValue() > 3)
            {
              result = true;
              break;
            }
          }

          for(yVariab = 0; yVariab < board.getHeight(); yVariab++)
          {
            var CTC = board.getCardAtPosition(Point(xVaria,yVariab));
            if(CTC != null && boardutils.checkForUntrashable(CTC) == false && instantlyMovable == true)
            {
              result = true;
              break;
            }
            if(CTC != null && boardutils.checkForUntrashable(CTC) == true && CTC.getCurrentValue() > 3)
            {
              result = true;
              break;
            }
            if(CTC != null && instantlyMovable == false && CTC.getCurrentValue() > 3)
            {
              result = true;
              break;
            }
          }

          if(result == true)
          {
            break;
          }
        }

        if(instantlyTrashable == false && instantlyMovable == true)
        {
          //If the tile is safe, the only allowed targets are the ones with 100% likeliness to be hit
          for(xVariab = 0; xVariab < board.getWidth(); xVariab++)
          {
            var CTC = board.getCardAtPosition(Point(xVariab,yVaria));
            if(CTC != null && boardutils.checkForUntrashable(CTC) == false && ((CTC.getCurrentValue() <= 0 && boardutils.isSixFix(gameState.getCurrentPlayer()) == false) || (CTC.getCurrentValue() <= 3 && boardutils.isSixFix(gameState.getCurrentPlayer()) == true)))
            {
              result = true;
            }

            for(yVariab = 0; yVariab < board.getHeight(); yVariab++)
            {
              var CTC = board.getCardAtPosition(Point(xVaria,yVariab));
              if(CTC != null && boardutils.checkForUntrashable(CTC) == false && ((CTC.getCurrentValue() <= 0 && boardutils.isSixFix(gameState.getCurrentPlayer()) == false) || (CTC.getCurrentValue() <= 3 && boardutils.isSixFix(gameState.getCurrentPlayer()) == true)))
              {
                result = true;
              }
            }
            if(result == true)
            {
              break;
            }
          }
        }

      }

      return result;
      break;
    case "documentholder":
      if(mainInventory.getAllCards().length > 0)
      {
        return true;
      }
      else
      {
        return false;
      }
      break;
    case "coffeegrinder":
      if((gameState.getCurrentPlayer() == 1 && p1Inventory.getAllCards().length > 2) || (gameState.getCurrentPlayer() == 2 && p2Inventory.getAllCards().length > 2) || (gameState.getCurrentPlayer() == 3 && p3Inventory.getAllCards().length > 2) || (gameState.getCurrentPlayer() == 4 && p4Inventory.getAllCards().length > 2))
      {
        if(mainInventory.getAllCards().length > 0)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
      else
      {
        return false;
      }
      break;
    case "flyswatter":
      var xVaria = 0;
      var yVaria = 0;
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        if((board.getCardAtPosition(Point(xVaria,yVaria)) == null) && (board.getCardAtPosition(Point(xVaria+1,yVaria)) == null) && (board.getCardAtPosition(Point(xVaria,yVaria+1)) == null) && (board.getCardAtPosition(Point(xVaria-1,yVaria)) == null) && (board.getCardAtPosition(Point(xVaria,yVaria-1)) == null))
        {
          if((board.getCardAtPosition(Point(xVaria+1,yVaria+1)) != null && (board.getCardAtPosition(Point(xVaria+1,yVaria+1))).getBaseValue() < 2) || (board.getCardAtPosition(Point(xVaria+1,yVaria-1)) != null && (board.getCardAtPosition(Point(xVaria+1,yVaria-1))).getBaseValue() < 2) || (board.getCardAtPosition(Point(xVaria-1,yVaria-1)) != null && (board.getCardAtPosition(Point(xVaria-1,yVaria-1))).getBaseValue() < 2) || (board.getCardAtPosition(Point(xVaria-1,yVaria+1)) != null && (board.getCardAtPosition(Point(xVaria-1,yVaria+1))).getBaseValue() < 2))
          {
            return true;
            break;
          }
        }
        if(xVaria == board.getWidth() - 1 && yVaria < board.getHeight() - 1)
        {
          xVaria = -1;
          yVaria++;
        }
        if(xVaria == board.getWidth() - 1 && yVaria == board.getHeight() - 1)
        {
          return false;
        }
      }
      break;
    case "football":
      var result = false;
      var xVaria = 0;
      var yVaria = 0;
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if((board.getCardAtPosition(Point(xVaria,yVaria)) == null) && (board.getCardAtPosition(Point(xVaria+1,yVaria)) == null) && (board.getCardAtPosition(Point(xVaria,yVaria+1)) == null) && (board.getCardAtPosition(Point(xVaria-1,yVaria)) == null) && (board.getCardAtPosition(Point(xVaria,yVaria-1)) == null))
          {
            if(board.isDoubleTile(Point(xVaria,yVaria)) == true || mainInventory.getAllCards().length > 1)
            {
              result = true;
              break; break;
            }
          }
        }
      }
      return result;
      break;
    case "broom":
      var result = false;
      var nAET = boardutils.getNonAdjacentEmptyTiles();

      cards = board.getAllCards();
      for(j = 0; j < nAET.length; j++)
      {
        var xVaria = nAET[j].x;
        var yVaria = nAET[j].y;
        var countMovableX = 0
        var countMovableY = 0
        var countImmovableX = 0
        var countImmovableY = 0
        var xDisab = 0
        var yDisab = 0

        for(xVariab = 0; xVariab < board.getWidth(); xVariab++)
        {
          if(board.isDisabledTile(Point(xVariab,yVaria)) == true)
          {
            xDisab++;
          }
        }
        for(yVariab = 0; yVariab < board.getHeight(); yVariab++)
        {
          if(board.isDisabledTile(Point(xVaria,yVariab)) == true)
          {
            yDisab++;
          }
        }

        for(k = 0; k < cards.length; k++)
        {
          if(cards[k].getPosX() == xVaria)
          {
            if(boardutils.checkForImmovable(cards[k]) == true || boardutils.isUntargetableByBronze(cards[k]) == true)
            {
              countImmovableX++;
            }
            else
            {
              countMovableX++;
            }
          }
          if(cards[k].getPosY() == yVaria)
          {
            if(boardutils.checkForImmovable(cards[k]) == true || boardutils.isUntargetableByBronze(cards[k]) == true)
            {
              countImmovableY++;
            }
            else
            {
              countMovableY++;
            }
          }
        }

        if(countMovableX + countImmovableX + countMovableY + countImmovableY < (board.getWidth() + board.getHeight() - xDisab - yDisab - 2) && countMovableX + countMovableY > 0)
        {
          if((countMovableX == 0 && countMovableY + countImmovableY == board.getWidth() - xDisab - 1) || (countMovableY == 0 && countMovableX + countImmovableX == board.getHeight() - yDisab - 1))
          {
          }
          else
          {
            result = true;
            break;
          }
        }
      }
      return result;
      break;
    case "souvenir":
      var sumCardsInHands = 0;
      switch(gameState.getGameMode())
      {
        case 0:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length);
          break;
        case 1:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length + p4Inventory.getAllCards().length);
          break;
        case 2:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length);
          break;
        case 3:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length + p4Inventory.getAllCards().length);
          break;
        case 4:
          sumCardsInHands = (p1Inventory.getAllCards().length + p3Inventory.getAllCards().length);
          break;
      }
      switch(gameState.getCurrentPlayer())
      {
        case 1:
          sumCardsInHands = sumCardsInHands - p1Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1)
          {
            sumCardsInHands = sumCardsInHands - p4Inventory.getAllCards().length;
          }
          break;
        case 2:
          sumCardsInHands = sumCardsInHands - p2Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1)
          {
            sumCardsInHands = sumCardsInHands - p3Inventory.getAllCards().length;
          }
          break;
        case 3:
          sumCardsInHands = sumCardsInHands - p3Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1)
          {
            sumCardsInHands = sumCardsInHands - p2Inventory.getAllCards().length;
          }
          break;
        case 4:
          sumCardsInHands = sumCardsInHands - p4Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1)
          {
            sumCardsInHands = sumCardsInHands - p1Inventory.getAllCards().length;
          }
          break;
      }
      //If there is at least one card in the hand of one opponent
      if(sumCardsInHands > 0)
      {
        for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
        {
          for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
          {
            if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
            {
              if(board.isSafeTile(Point(xVaria,yVaria)) == false)
              {
                cardsCFUR = board.getAllCards();
                var result = true;
                for(cfur = 0; cfur < cardsCFUR.length; cfur++)
                {

                  if((cardsCFUR[cfur].getID() == "celticshield" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "celticshield")) && cardsCFUR[cfur].isGrayScale() == false && cardsCFUR[cfur].getPlayer() == card.getPlayer())
                  {
                    var shieldDistance = Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2)
                    if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
                    {
                      result = false;
                      break;
                    }
                  }

                  if(cardsCFUR[cfur].getID() == "padlock" && cardsCFUR[cfur].isGrayScale() == false && Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2) == 1)
                  {
                    result = false;
                    break;
                  }

                  if((cardsCFUR[cfur].getID() == "hardhat" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "hardhat")) && cardsCFUR[cfur].isGrayScale() == false)
                  {
                    switch(cardsCFUR[cfur].getPlayer())
                    {
                      case 1:
                        if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria-1) {result = false;}
                        break;
                      case 2:
                        if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria+1) {result = false;}
                        break;
                      case 3:
                        if(cardsCFUR[cfur].getPosX() == xVaria+1 && cardsCFUR[cfur].getPosY() == yVaria) {result = false;}
                        break;
                      case 4:
                        if(cardsCFUR[cfur].getPosX() == xVaria-1 && cardsCFUR[cfur].getPosY() == yVaria) {result = false;}
                        break;
                    }
                  }
                }
              }
            }
          }
        }
        return result;
      }
      else
      {
        return false
      }
      break;
    case "binoculars":
      eT = boardutils.getEmptyTiles();
      if(boardutils.isHalted(eT) == false)
      {
        if(mainInventory.getAllCards().length > 0)
        {
          //Check for the amount of cards in the hands of the opponents
          var sumCardsInHands = 0;
          switch(gameState.getGameMode())
          {
            case 0:
              sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length);
              break;
            case 1:
              sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length + p4Inventory.getAllCards().length);
             break;
            case 2:
              sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length);
              break;
            case 3:
              sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length + p4Inventory.getAllCards().length);
              break;
            case 4:
              sumCardsInHands = (p1Inventory.getAllCards().length + p3Inventory.getAllCards().length);
              break;
          }
          switch(gameState.getCurrentPlayer())
          {
            case 1:
              sumCardsInHands = sumCardsInHands - p1Inventory.getAllCards().length;
              if(gameState.getGameMode() == 1)
              {
                sumCardsInHands = sumCardsInHands - p4Inventory.getAllCards().length;
              }
              break;
            case 2:
              sumCardsInHands = sumCardsInHands - p2Inventory.getAllCards().length;
              if(gameState.getGameMode() == 1)
              {
                sumCardsInHands = sumCardsInHands - p3Inventory.getAllCards().length;
              }
              break;
            case 3:
              sumCardsInHands = sumCardsInHands - p3Inventory.getAllCards().length;
              if(gameState.getGameMode() == 1)
              {
                sumCardsInHands = sumCardsInHands - p2Inventory.getAllCards().length;
              }
              break;
            case 4:
              sumCardsInHands = sumCardsInHands - p4Inventory.getAllCards().length;
              if(gameState.getGameMode() == 1)
              {
                sumCardsInHands = sumCardsInHands - p1Inventory.getAllCards().length;
              }
              break;
          }
          //If there is at least one card in the hand of one opponent
          if(sumCardsInHands > 0)
          {
            return true;
          }
          else
          {
            return false;
          }
        }
        else
        {
          return false;
        }
      }
      else
      {
        return true;
      }
      break;
    case "whiteknight":
      if(mainInventory.getAllCards().length > 0)
      {
        return true;
      }
      else
      {
        return false;
      }
      break;
    default:
      return true;
      break;
  }
}


boardutils.itemMirrored = function(specificMirror)
{
  switch(specificMirror.getPlayer())
  {
    case 1:
      var cardMirrored = board.getCardAtPosition(Point(specificMirror.getPosX(),specificMirror.getPosY()-1))
      break;
    case 2:
      var cardMirrored = board.getCardAtPosition(Point(specificMirror.getPosX(),specificMirror.getPosY()+1))
      break;
    case 3:
      var cardMirrored = board.getCardAtPosition(Point(specificMirror.getPosX()+1,specificMirror.getPosY()))
      break;
    case 4:
      var cardMirrored = board.getCardAtPosition(Point(specificMirror.getPosX()-1,specificMirror.getPosY()))
      break;
  }
  if(cardMirrored != null && cardMirrored.isGrayScale() == false)
  {
    return cardMirrored.getID();
  }
  else
  {
    return 0;
  }
}


boardutils.checkForUnrotatable = function(cardToCheck)
{
  var result = false
  if(board.isSafeTile(cardToCheck.getPosition()) == false)
  {
    cardsCFUR = board.getAllCards()
    for(cfur = 0; cfur < cardsCFUR.length; cfur++)
    {
      if((cardsCFUR[cfur].getID() == "celticshield" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "celticshield")) && cardsCFUR[cfur].isGrayScale() == false && cardsCFUR[cfur].getPlayer() == cardToCheck.getPlayer())
      {
        var shieldDistance = Math.pow(cardsCFUR[cfur].getPosX()-cardToCheck.getPosX(), 2) + Math.pow(cardsCFUR[cfur].getPosY()-cardToCheck.getPosY(), 2)
        if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
        {
          result = true;
          break;
        }
      }

      if(cardToCheck.getID() != "padlock" || (cardToCheck.getID() == "padlock" && cardToCheck.isGrayScale() == true))
      {
        if(cardsCFUR[cfur].getID() == "padlock" && cardsCFUR[cfur].isGrayScale() == false && Math.pow(cardsCFUR[cfur].getPosX()-cardToCheck.getPosX(), 2) + Math.pow(cardsCFUR[cfur].getPosY()-cardToCheck.getPosY(), 2) == 1)
        {
          result = true;
          break;
        }
      }
      else
      {
        result = true;
        break;
      }

      if((cardsCFUR[cfur].getID() == "hardhat" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "celticshield")) && cardsCFUR[cfur].isGrayScale() == false)
      {
        switch(cardsCFUR[cfur].getPlayer())
        {
          case 1:
            if(cardsCFUR[cfur].getPosX() == cardToCheck.getPosX() && cardsCFUR[cfur].getPosY() == cardToCheck.getPosY()-1)
            {
              result = true;
            }
            break;
          case 2:
            if(cardsCFUR[cfur].getPosX() == cardToCheck.getPosX() && cardsCFUR[cfur].getPosY() == cardToCheck.getPosY()+1)
            {
              result = true;
            }
            break;
          case 3:
            if(cardsCFUR[cfur].getPosX() == cardToCheck.getPosX()+1 && cardsCFUR[cfur].getPosY() == cardToCheck.getPosY())
            {
              result = true;
            }
            break;
          case 4:
            if(cardsCFUR[cfur].getPosX() == cardToCheck.getPosX()-1 && cardsCFUR[cfur].getPosY() == cardToCheck.getPosY())
            {
              result = true;
            }
            break;
        }
      }

      if(cardToCheck.getID() == "net" && cardToCheck.isGrayScale() == false)
      {
        result = true;
        break;
      }

    }
  }
  else
  {
    result = true;
  }
  return result;
}


boardutils.checkForUntrashable = function(cardToCheck)
{
  var result = false
  if(board.isSafeTile(cardToCheck.getPosition()) == false)
  {
    cardsCFUT = board.getAllCards()
    for(cfut = 0; cfut < cardsCFUT.length; cfut++)
    {
      if((cardsCFUT[cfut].getID() == "celticshield" || (cardsCFUT[cfut].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUT[cfut]) == "celticshield")) && cardsCFUT[cfut].isGrayScale() == false && cardsCFUT[cfut].getPlayer() == cardToCheck.getPlayer())
      {
        var shieldDistance = Math.pow(cardsCFUT[cfut].getPosX()-cardToCheck.getPosX(), 2) + Math.pow(cardsCFUT[cfut].getPosY()-cardToCheck.getPosY(), 2)
        if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
        {
          result = true;
          break;
        }
      }

      if((cardsCFUT[cfut].getID() == "hardhat" && cardsCFUT[cfut].isGrayScale() == false) || (cardsCFUT[cfut].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUT[cfut]) == "hardhat"))
      {
        if(cardsCFUT[cfut] != cardToCheck)
        {
          switch(cardsCFUT[cfut].getPlayer())
          {
            case 1:
              if(cardsCFUT[cfut].getPosX() == cardToCheck.getPosX() && cardsCFUT[cfut].getPosY() == cardToCheck.getPosY()-1)
              {
                result = true;
              }
              break;
            case 2:
              if(cardsCFUT[cfut].getPosX() == cardToCheck.getPosX() && cardsCFUT[cfut].getPosY() == cardToCheck.getPosY()+1)
              {
                result = true;
              }
              break;
            case 3:
              if(cardsCFUT[cfut].getPosX() == cardToCheck.getPosX()+1 && cardsCFUT[cfut].getPosY() == cardToCheck.getPosY())
              {
                result = true;
              }
              break;
            case 4:
              if(cardsCFUT[cfut].getPosX() == cardToCheck.getPosX()-1 && cardsCFUT[cfut].getPosY() == cardToCheck.getPosY())
              {
                result = true;
              }
              break;
          }
        }
        else
        {
          result = true;
        }
      }
    }
  }
  else
  {
    result = true;
  }
  return result;
}


boardutils.logSilverActivation = function(usedCardName)
{
  var userNames = gameState.getPlayerNickNames();
  gameState.writeToGameLog(qsTranslate("board", "<font color=\"paleturquoise\">%1 used the active ability of %2%3 %4.</font>").arg(userNames[gameState.getCurrentPlayer()-1]).arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(usedCardName), [1, 2, 3, 4], 1, 0);
}


boardutils.getPossibleSneakerMoves = function(pos, dis)
{
  result = new Array();
  if(board.getCardAtPosition(Point(pos.x,pos.y-dis)) == null && dis-1 < pos.y)
  {
    if(board.isDisabledTile(Point(pos.x,pos.y-dis)) == false)
    {
      result.push(Point(pos.x,pos.y-dis));
    }
  }
  if(board.getCardAtPosition(Point(pos.x-dis,pos.y)) == null && dis-1 < pos.x)
  {
    if(board.isDisabledTile(Point(pos.x-dis,pos.y)) == false)
    {
      result.push(Point(pos.x-dis,pos.y));
    }
  }
  if(board.getCardAtPosition(Point(pos.x+dis,pos.y)) == null && pos.x < board.getWidth()-dis)
  {
    if(board.isDisabledTile(Point(pos.x+dis,pos.y)) == false)
    {
      result.push(Point(pos.x+dis,pos.y));
    }
  }
  if(board.getCardAtPosition(Point(pos.x,pos.y+dis)) == null && pos.y < board.getHeight()-dis)
  {
    if(board.isDisabledTile(Point(pos.x,pos.y+dis)) == false)
    {
      result.push(Point(pos.x,pos.y+dis));
    }
  }
  return result;
}


boardutils.checkForImmovable = function(cardToCheck)
{
  var result = false

  if(board.isInnovativeTile(cardToCheck.getPosition()))
  {
    result = true;
  }

  if(result == false)
  {
    if((cardToCheck.getID() == "stamp" || (cardToCheck.getID() == "handmirror" && boardutils.itemMirrored(cardToCheck) == "stamp")) && cardToCheck.isGrayScale() == false)
    {
      result = true;
    }
  }

  if(result == false)
  {
    cardsCFIM = board.getAllCards()
    for(CFIM = 0; CFIM < cardsCFIM.length; CFIM++)
    {
      if((cardsCFIM[CFIM].getID() == "gluetube" || (cardsCFIM[CFIM].getID() == "handmirror" && boardutils.itemMirrored(cardsCFIM[CFIM]) == "gluetube")) && cardsCFIM[CFIM].isGrayScale() == false)
      {
        var tubeDistance = Math.pow(cardsCFIM[CFIM].getPosX()-cardToCheck.getPosX(), 2) + Math.pow(cardsCFIM[CFIM].getPosY()-cardToCheck.getPosY(), 2)
        if(tubeDistance == 0 || tubeDistance == 1 || tubeDistance == 2)
        {
          result = true;
          break;
        }
      }
    }
  }
  return result;
}


boardutils.doesTilePreventDamaging = function(xOccupy, yOccupy)
{
  var result = false
  if(board.isSafeTile(Point(xOccupy,yOccupy)) == false)
  {
    cardsDTPD = board.getAllCards()
    for(dtpd = 0; dtpd < cardsDTPD.length; dtpd++)
    {
      if((cardsDTPD[dtpd].getID() == "celticshield" || (cardsDTPD[dtpd].getID() == "handmirror" && boardutils.itemMirrored(cardsDTPD[dtpd]) == "celticshield")) && cardsDTPD[dtpd].isGrayScale() == false && cardsDTPD[dtpd].getPlayer() == gameState.getCurrentPlayer())
      {
        var shieldDistance = Math.pow(cardsDTPD[dtpd].getPosX()-xOccupy, 2) + Math.pow(cardsDTPD[dtpd].getPosY()-yOccupy, 2)
        if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
        {
          result = true;
          break;
        }
      }

      if((cardsDTPD[dtpd].getID() == "hardhat" || (cardsDTPD[dtpd].getID() == "handmirror" && boardutils.itemMirrored(cardsDTPD[dtpd]) == "hardhat")) && cardsDTPD[dtpd].isGrayScale() == false)
      {
        switch(cardsDTPD[dtpd].getPlayer())
        {
          case 1:
            if(cardsDTPD[dtpd].getPosX() == xOccupy && cardsDTPD[dtpd].getPosY() == yOccupy-1)
            {
              result = true;
            }
            break;
          case 2:
            if(cardsDTPD[dtpd].getPosX() == xOccupy && cardsDTPD[dtpd].getPosY() == yOccupy+1)
            {
              result = true;
            }
            break;
          case 3:
            if(cardsDTPD[dtpd].getPosX() == xOccupy+1 && cardsDTPD[dtpd].getPosY() == yOccupy)
            {
              result = true;
            }
            break;
          case 4:
            if(cardsDTPD[dtpd].getPosX() == xOccupy-1 && cardsDTPD[dtpd].getPosY() == yOccupy)
            {
              result = true;
            }
            break;
        }
      }
    }
  }
  else
  {
    result = true;
  }
  return result;
}


boardutils.doesTilePreventMoving = function(xOccupy, yOccupy)
{
  var result = false
  cardsDTPM = board.getAllCards()
  for(dtpm = 0; dtpm < cardsDTPM.length; dtpm++)
  {
    if((cardsDTPM[dtpm].getID() == "gluetube" || (cardsDTPM[dtpm].getID() == "handmirror" && boardutils.itemMirrored(cardsDTPM[dtpm]) == "gluetube")) && cardsDTPM[dtpm].isGrayScale() == false)
    {
      var tubeDistance = Math.pow(cardsDTPM[dtpm].getPosX()-xOccupy, 2) + Math.pow(cardsDTPM[dtpm].getPosY()-yOccupy, 2)
      if(tubeDistance == 0 || tubeDistance == 1 || tubeDistance == 2)
      {
        result = false;
        break;
      }
    }
  }
  return result;
}


boardutils.informedPlayers = function()
{
  var result = [];
  if(gameState.getGameMode() == 2)
  {
    switch(gameState.getCurrentPlayer())
    {
      case 1: result = [1, 3]; break;
      case 2: result = [2, 4]; break;
      case 3: result = [1, 3]; break;
      case 4: result = [2, 4]; break;
    }
  }
  else
  {
    result.push(gameState.getCurrentPlayer());
  }
  return result;
}


boardutils.unexpectingPlayers = function()
{
  var result = [];
  if(gameState.getGameMode() == 2)
  {
    switch(gameState.getCurrentPlayer())
    {
      case 1: result = [2, 4]; break;
      case 2: result = [1, 3]; break;
      case 3: result = [2, 4]; break;
      case 4: result = [1, 3]; break;
    }
  }
  else
  {
    switch(gameState.getCurrentPlayer())
    {
      case 1: result = [2, 3, 4]; break;
      case 2: result = [1, 3, 4]; break;
      case 3: result = [1, 2, 4]; break;
      case 4: result = [1, 2, 3]; break;
    }
  }
  return result;
}


boardutils.damageEvents = function(damagingSource, targetCard, xTarget, yTarget)
{
  var pigDraw1 = 0;
  var pigDraw2 = 0;
  var pigDraw3 = 0;
  var pigDraw4 = 0;

  if((targetCard.getID() == "piggybank" && targetCard.isGrayScale() == false) || (targetCard.getID() == "handmirror" && boardutils.itemMirrored(targetCard) == "piggybank"))
  {
    switch(targetCard.getPlayer())
    {
      case 1:
        pigDraw1 = pigDraw1 + targetCard.getCounter();
        break;
      case 2:
        pigDraw2 = pigDraw2 + targetCard.getCounter();
        break;
      case 3:
        pigDraw3 = pigDraw3 + targetCard.getCounter();
        break;
      case 4:
        pigDraw4 = pigDraw4 + targetCard.getCounter();
        break;
    }
  }
  if((targetCard.getID() == "bomb" && targetCard.isGrayScale() == false) || (targetCard.getID() == "handmirror" && boardutils.itemMirrored(targetCard) == "bomb"))
  {

    cards = board.getAllCards(); //Retrieve all cards on the board
    cardsBomb = []; //Store the bomb cards
    cardsBombX = []; //Store the X coordinate of bombs
    cardsBombY = []; //Store the Y coordinate of bombs
    cardsUIDBOOM = []; //Stores UIDs for exploding cards

    //Include the initiator on the list
    cardsBomb.push(targetCard);
    cardsBombX.push(targetCard.getPosX());
    cardsBombY.push(targetCard.getPosY());
    cardsUIDBOOM.push(targetCard.getUID());

    for(bN = 0; bN < cardsBombX.length; bN++)
    {
      for(ctc = 2; ctc < 10; ctc = ctc+2)
      {
        var xCorri = 0;
        var yCorri = 0;
        if(ctc == 2 && cardsBombY[bN] < board.getHeight() - 1) {yCorri = 1;}
        if(ctc == 4 && cardsBombX[bN] > 0) {xCorri = -1;}
        if(ctc == 6 && cardsBombX[bN] < board.getWidth() - 1) {xCorri = 1;}
        if(ctc == 8 && cardsBombY[bN] > 0) {yCorri = -1;}
        CTC = board.getCardAtPosition(Point(cardsBombX[bN]+xCorri,cardsBombY[bN]+yCorri));
        //If the card is trashable then it can be marked
        if(CTC != null && CTC != cardsBomb[bN] && boardutils.checkForUntrashable(CTC) == false)
        {
          //Check whether it was marked previously
          var alreadyMarked = false;
          for(eN = 0; eN < cardsUIDBOOM.length; eN++)
          {
            if(cardsUIDBOOM[eN] == CTC.getUID())
            {
              alreadyMarked = true;
            }
          }
          if(alreadyMarked == false)
          {
            //Store the coordinates and UIDs of the good card
            cardsUIDBOOM.push(CTC.getUID());
            //If the marked card is a Bomb, add it to the chain reaction
            if((CTC.getID() == "bomb" && CTC.isGrayScale() == false) || (CTC.getID() == "handmirror" && boardutils.itemMirrored(CTC) == "bomb"))
            {
              cardsBomb.push(CTC);
              cardsBombX.push(CTC.getPosX());
              cardsBombY.push(CTC.getPosY());
            }
            //If the marked card is a Piggy Bank, remember the amount of counters on it
            if((CTC.getID() == "piggybank" && CTC.isGrayScale() == false) || (CTC.getID() == "handmirror" && boardutils.itemMirrored(CTC) == "piggybank"))
            {
              switch(CTC.getPlayer())
              {
                case 1:
                  pigDraw1 = pigDraw1 + CTC.getCounter();
                  break;
                case 2:
                  pigDraw2 = pigDraw2 + CTC.getCounter();
                  break;
                case 3:
                  pigDraw3 = pigDraw3 + CTC.getCounter();
                  break;
                case 4:
                  pigDraw4 = pigDraw4 + CTC.getCounter();
                  break;
              }
            }
          }
        }
      }
    }
    var explosionAndPigs = [cardsUIDBOOM, pigDraw1, pigDraw2, pigDraw3, pigDraw4];
    return explosionAndPigs;

  }
  else
  {
    var pigs = [[], pigDraw1, pigDraw2, pigDraw3, pigDraw4];
    return pigs;
  }
} 


boardutils.complexCurrentValue = function()
{
  var abilityOfTheCard = 0;
  var calcValue = card.getBaseValue();

  if(card.hasIcon("stop") == true && board.isDoubleTile(card.getPosition()))
  {
    calcValue = calcValue - 1;
  }

  if(boardutils.isThereAdjacentSponge() == true)
  {
    var spongeMalus = 0;
    cardsITAS = board.getAllCards();
    for(ITAS = 0; ITAS < cardsITAS.length; ITAS++)
    {
      if((cardsITAS[ITAS].getID() == "sponge" || (cardsITAS[ITAS].getID() == "handmirror" && boardutils.itemMirrored(cardsITAS[ITAS]) == "sponge")) && cardsITAS[ITAS].isGrayScale() == false && cardsITAS[ITAS].getPlayer() != card.getPlayer())
      {
        if(Math.pow(card.getPosX()-cardsITAS[ITAS].getPosX(), 2) + Math.pow(card.getPosY()-cardsITAS[ITAS].getPosY(), 2) == 1)
        {
          spongeMalus++;
        }
      }
    }
    calcValue = calcValue - spongeMalus;
  }
  if(boardutils.isThereAdjacentHat() == true)
  {
    var hatBonus = 1;
    var hatMultiplier = 0;
    if(card.hasIcon("optu") == true) {hatBonus++;}
    if(card.hasIcon("noup") == true) {hatBonus++;}
    if(card.hasIcon("dmno") == true) {hatBonus++;}
    if(card.hasIcon("cafe") == true) {hatBonus++;}
    if(card.hasIcon("crar") == true) {hatBonus++;}
    if(card.hasIcon("tcan") == true) {hatBonus++;}
    if(card.hasIcon("stop") == true) {hatBonus++;}
    for(HTC = 1; HTC < 5; HTC++)
    {
      var xCorri = 0;
      var yCorri = 0;
      if(HTC == 1) {yCorri = -1;}
      if(HTC == 2) {yCorri = 1;}
      if(HTC == 3) {xCorri = 1;}
      if(HTC == 4) {xCorri = -1;}
      var hatToCheck = board.getCardAtPosition(Point(card.getPosX()+xCorri,card.getPosY()+yCorri));
      if(hatToCheck != null && hatToCheck.getID() == "hardhat" && hatToCheck.getPlayer() == HTC && hatToCheck.isGrayScale() == false)
      {
        hatMultiplier++;
      }
    }
    calcValue = calcValue + hatBonus * hatMultiplier;
  }

  if(boardutils.isThereAdjacentBat() == true && card.isGrayScale() == true)
  {
    var batMalus = 0;
    cardsITAB = board.getAllCards();
    for(ITAB = 0; ITAB < cardsITAB.length; ITAB++)
    {
      if((cardsITAB[ITAB].getID() == "baseballbat" || (cardsITAB[ITAB].getID() == "handmirror" && boardutils.itemMirrored(cardsITAB[ITAB]) == "baseballbat")) && cardsITAB[ITAB].isGrayScale() == false)
      {
        if(Math.pow(card.getPosX()-cardsITAB[ITAB].getPosX(), 2) + Math.pow(card.getPosY()-cardsITAB[ITAB].getPosY(), 2) == 1)
        {
          batMalus++;
        }
      }
    }
    calcValue = calcValue - batMalus;
  }
  
  if(card.getCounter() == card.getBaseValue())
  {
    var radioBonus = 0;
    cardsITR = board.getAllCards();
    for(ITR = 0; ITR < cardsITR.length; ITR++)
    {
      if((cardsITR[ITR].getID() == "radio" || (cardsITR[ITR].getID() == "handmirror" && boardutils.itemMirrored(cardsITR[ITR]) == "radio")) && cardsITR[ITR].isGrayScale() == false)
      {
        radioBonus++;
      }
    }
    calcValue = calcValue + radioBonus;
  }

  if(board.isDoubleTile(card.getPosition()) == true)
  {
    var dipperMalus = 0;
    cardsIDIC = board.getAllCards();
    for(IDIC = 0; IDIC < cardsIDIC.length; IDIC++)
    {
      if((cardsIDIC[IDIC].getID() == "honeydipper" || (cardsIDIC[IDIC].getID() == "handmirror" && boardutils.itemMirrored(cardsIDIC[IDIC]) == "honeydipper")) && cardsIDIC[IDIC].isGrayScale() == false)
      {
        if((cardsIDIC[IDIC].getPosX() == card.getPosX() || cardsIDIC[IDIC].getPosY() == card.getPosY()) && cardsIDIC[IDIC].getUID() != card.getUID())
        {
          dipperMalus++;
        }
      }
    }
    calcValue = calcValue - dipperMalus;
  }

  if(card.isGrayScale() == false)
  {
    if(card.getID() != "handmirror")
    {
      abilityOfTheCard = card.getID();
    }
    else
    {
      abilityOfTheCard = boardutils.itemMirrored(card);
    }

    switch(abilityOfTheCard)
    {
      case "goldencup":
        calcValue = calcValue + card.getCounter();
        break;
      case "stamp":
        cards = board.getAllCards(); //Retrieve all cards on the board
        var stampBonus = 0; //This will store the amount of stamps
        for(i = 0; i < cards.length; i++) //Check all cards
        {
          //Checks for other stamps
          if(cards[i].getID() == card.getID())
          {
            //Store the coordinates of the good card
            stampBonus++;
          }
        }
        calcValue = calcValue + stampBonus * 2;
        break;
      case "fakepainting":
        cardsInHand = [];
        cardValueInHand = [];
        if(card.getPlayer() == 1) {cardsInHand = p1Inventory.getAllCards();}
        if(card.getPlayer() == 2) {cardsInHand = p2Inventory.getAllCards();}
        if(card.getPlayer() == 3) {cardsInHand = p3Inventory.getAllCards();}
        if(card.getPlayer() == 4) {cardsInHand = p4Inventory.getAllCards();}
        for(i = 0; i < cardsInHand.length; i++)
        {
          cardValueInHand.push(cardsInHand[i].getBaseValue());
        }
        if(gameState.getCardsWithActiveBronzeAbility().length > 0)
        {
          cardValueInHand.push(gameState.cardForUID(gameState.getCardsWithActiveBronzeAbility()[0]).getBaseValue());
        }
        var paintingBonus = (Math.max.apply(Math, cardValueInHand)) * 2;
        if(paintingBonus > 0)
        {
          calcValue = calcValue + paintingBonus;
        }
        break;
      case "documentholder":
        var woodBonus = 0;
        var metalBonus = 0;
        var glassBonus = 0;
        var plasticBonus = 0;
        var textileBonus = 0;
        cards = board.getAllCards(); //Retrieve all cards on the board
        cardsOK = []; //This will store the cards which give bonus
        for(i = 0; i < cards.length; i++) //Check all cards
        {
          //If the card is a king move away then it might be good
          if(card.getPosX()-1 <= cards[i].getPosX() && cards[i].getPosX() <= card.getPosX()+1 && card.getPosY()-1 <= cards[i].getPosY() && cards[i].getPosY() <= card.getPosY()+1)
          {
            //If the card is not the document holder itself and not a card being played
            if(card.getUID() == cards[i].getUID() || (gameState.getCardsWithActiveBronzeAbility().length > 0 && cards[i].getUID() == gameState.getCardsWithActiveBronzeAbility()[0]))
            {
            }
            else
            {
              switch(cards[i].getBackground())
              {
                case "wood":
                  woodBonus++;
                  break;
                case "metal":
                  metalBonus++;
                  break;
                case "glass":
                  glassBonus++;
                  break;
                case "plastic":
                  plasticBonus++;
                  break;
                case "textile":
                  textileBonus++;
                  break;
              }
            }
          }
        }
        materialCollection = [];
        materialCollection.push(woodBonus);
        materialCollection.push(metalBonus);
        materialCollection.push(glassBonus);
        materialCollection.push(plasticBonus);
        materialCollection.push(textileBonus);
        var holderBonus = Math.max.apply(Math, materialCollection);
        calcValue = calcValue + holderBonus;
        break;
      case "padlock":
        var padlockBonus = 0
        cards = board.getAllCards(); //Retrieve all cards on the board
        for(i = 0; i < cards.length; i++) //Check all cards
        {
          //Checks for adjacent cards and cards being played
          if(cards[i].getPlayer() != card.getPlayer() && (gameState.getCardsWithActiveBronzeAbility().length == 0 || cards[i].getUID() != gameState.getCardsWithActiveBronzeAbility()[0]))
          {
            if(card.getPosX() == cards[i].getPosX() && card.getPosY() == cards[i].getPosY()+1)
            {
              padlockBonus++;
            }
            if(card.getPosX() == cards[i].getPosX()-1 && card.getPosY() == cards[i].getPosY())
            {
              padlockBonus++;
            }
            if(card.getPosX() == cards[i].getPosX() && card.getPosY() == cards[i].getPosY()-1)
            {
              padlockBonus++;
            }
            if(card.getPosX() == cards[i].getPosX()+1 && card.getPosY() == cards[i].getPosY())
            {
              padlockBonus++;
            }
          }
        }
        calcValue = calcValue + padlockBonus;
        break;
      case "football":
        if(board.isDoubleTile(card.getPosition()))
        {
          calcValue = calcValue * 2;
        }
        break;
      case "sponge":
        cards = board.getAllCards(); //Retrieve all cards on the board
        cardsOK = []; //This will store the cards which give bonus
        for(i = 0; i < cards.length; i++) //Check all cards
        {
          //If the card's owner is not the owner of the Sponge
          if(cards[i].getPlayer() != card.getPlayer())
          {
            //If the card is on an adjacent tile then it is good
            if(Math.pow(card.getPosX()-cards[i].getPosX(), 2) + Math.pow(card.getPosY()-cards[i].getPosY(), 2) == 1)
            {
              //Cards during executing an entry ability do not count
              if(gameState.getCardsWithActiveBronzeAbility().length == 0 || cards[i].getUID() != gameState.getCardsWithActiveBronzeAbility()[0])
              {
                //Store the coordinates of the good card
                cardsOK.push(cards[i].getPosition());
              }
            }
          }
        }
        calcValue = calcValue + cardsOK.length;
        break;
      case "vase":
        cards = board.getAllCards(); //Retrieve all cards on the board
        var vaseBonus = 0; //This will store the bonus
        for(i = 0; i < cards.length; i++) //Check all cards
        {
          //Differentiates column for p3 and p4
          if(card.getPlayer() == 1 || card.getPlayer() == 2)
          {
            //If the card's owner is the owner of vase and it is in the same column with it then it is good
            if(cards[i].getPlayer() == card.getPlayer() && cards[i].getPosX() == card.getPosX())
            {
              //Cards during executing an entry ability do not count
              if(gameState.getCardsWithActiveBronzeAbility().length == 0 || cards[i].getUID() != gameState.getCardsWithActiveBronzeAbility()[0])
              {
                //Store the coordinates of the good card
                vaseBonus++;
              }
            }
          }
          if(card.getPlayer() == 3 || card.getPlayer() == 4)
          {
            if(cards[i].getPlayer() == card.getPlayer() && cards[i].getPosY() == card.getPosY())
            {
              //Cards during executing an entry ability do not count
              if(gameState.getCardsWithActiveBronzeAbility().length == 0 || cards[i].getUID() != gameState.getCardsWithActiveBronzeAbility()[0])
              {
                //Store the coordinates of the good card
                vaseBonus++;
              }
            }
          }
        }
        calcValue = calcValue + vaseBonus - 1;
        break;
      case "whitepawn":
        calcValue = calcValue + card.getCounter();
        break;
      case "ruler":
        cards = board.getAllCards(); //Retrieve all cards on the board
        var rulerBonus = 0; //This will store the bonus
        for(i = 0; i < cards.length; i++) //Check all cards
        {
          //Differentiates line for p3 and p4
          if(card.getPlayer() == 1 || card.getPlayer() == 2)
          {  
            //If the card's owner is the owner of ruler and it is in the same line with it then it is good
            if(cards[i].getPlayer() == card.getPlayer() && cards[i].getPosY() == card.getPosY())
            {
              //Cards during executing an entry ability do not count
              if(gameState.getCardsWithActiveBronzeAbility().length == 0 || cards[i].getUID() != gameState.getCardsWithActiveBronzeAbility()[0])
              {
                //Store the coordinates of the good card
                rulerBonus++;
              }
            }
          }
          if(card.getPlayer() == 3 || card.getPlayer() == 4)
          {
            if(cards[i].getPlayer() == card.getPlayer() && cards[i].getPosX() == card.getPosX())
            {
              //Cards during executing an entry ability do not count
              if(gameState.getCardsWithActiveBronzeAbility().length == 0 || cards[i].getUID() != gameState.getCardsWithActiveBronzeAbility()[0])
              {
                //Store the coordinates of the good card
                rulerBonus++;
              }
            }
          }
        }
        calcValue = calcValue + rulerBonus - 1;
        break;
    }
  }

  if(board.isDoubleTile(card.getPosition()))
  {
    calcValue = calcValue * 2;
  }
  if(board.isFashionTile(card.getPosition()))
  {
    calcValue = calcValue + boardutils.getFashionBonus(card.getPlayer());
  }
  return calcValue;
}
