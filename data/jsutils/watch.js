//@include jsutils.board
//@include jsutils.point


var watchutils = new Object();


watchutils.secondWatchCondition = function(choiceForWatch)
{
  switch(choiceForWatch.getID())
  {
  
    case "giftbox":
      var arrayNumber = 0;
      var instantlyRotatable = true;
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            if(board.isSafeTile(Point(xVaria,yVaria)) == false)
            {
              cardsCFUR = board.getAllCards();
              instantlyRotatable = true;
              for(cfur = 0; cfur < cardsCFUR.length; cfur++)
              {
                if((cardsCFUR[cfur].getID() == "celticshield" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "celticshield")) && cardsCFUR[cfur].isGrayScale() == false && cardsCFUR[cfur].getPlayer() == gameState.getCurrentPlayer())
                {
                  var shieldDistance = Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2)
                  if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
                  {
                    instantlyRotatable = false;
                    break;
                  }
                }

                if(card.getID() != "padlock" || (card.getID() == "padlock" && card.isGrayScale() == true))
                {
                  if(cardsCFUR[cfur].getID() == "padlock" && cardsCFUR[cfur].isGrayScale() == false && Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2) == 1)
                  {
                    instantlyRotatable = false;
                    break;
                  }
                }
                else
                {
                  instantlyRotatable = false;
                  break;
                }

                if((cardsCFUR[cfur].getID() == "hardhat" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "hardhat")) && cardsCFUR[cfur].isGrayScale() == false)
                {
                  switch(cardsCFUR[cfur].getPlayer())
                  {
                    case 1: if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria-1) {instantlyRotatable = false;} break;
                    case 2: if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria+1) {instantlyRotatable = false;} break;
                    case 3: if(cardsCFUR[cfur].getPosX() == xVaria+1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;} break;
                    case 4: if(cardsCFUR[cfur].getPosX() == xVaria-1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;} break;
                  }
                }

              }
              if(instantlyRotatable == true)
              {
                return true;
              }
            }
          }
        }
      }
      if(instantlyRotatable == false)
      {
        return false
      }
      break;

    case "goldencup":
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            var dominoCondition = false;
            var entryCondition = false;
            var adjacentToCup = 0;

            for(ttc = 2; ttc < 10; ttc = ttc+2)
            {
              var xCorri = 0;
              var yCorri = 0;
              if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
              if(ttc == 4 && xVaria > 0) {xCorri = -1;}
              if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
              if(ttc == 8 && yVaria > 0) {yCorri = -1;}
              TTC = Point(xVaria+xCorri,yVaria+yCorri);
              if(board.getCardAtPosition(TTC) != null && (board.getCardAtPosition(TTC)).getPlayer() == gameState.getCurrentPlayer())
              {
                dominoCondition = true;
              }
              if(board.getCardAtPosition(TTC) != null)
              {
                adjacentToCup++;
              }
              if(dominoCondition == true && ttc == 8)
              {
                if( ((gameState.getGameMode() == 0 || gameState.getGameMode() == 4) && mainInventory.getAllCards().length >= adjacentToCup) || 
                    ((gameState.getGameMode() == 1 || gameState.getGameMode() == 2) && mainInventory.getAllCards().length >= adjacentToCup*2) ||
                    ((gameState.getGameMode() == 3) && mainInventory.getAllCards().length >= adjacentToCup*3) )
                {
                  entryCondition = true;
                }
              }
              if(dominoCondition == true && entryCondition == true)
              {
                return true;
              }
            }
          }
        }
      }
      return false;
      break;

    case "baseballbat":
      if(boardutils.limitedByPerfume() == false)
      {
        eT = boardutils.getEmptyTiles();
      }
      else
      {
        eT = boardutils.getTilesAbovePerfume();
      }
      for(i = 0; i < eT.length; i++)
      {
        for(ctc = 2; ctc < 10; ctc = ctc+2)
        {
          var xCorri = 0;
          var yCorri = 0;
          if(ctc == 2 && eT[i].y < board.getHeight() - 1) {yCorri = 1;}
          if(ctc == 4 && eT[i].x > 0) {xCorri = -1;}
          if(ctc == 6 && eT[i].x < board.getWidth() - 1) {xCorri = 1;}
          if(ctc == 8 && eT[i].y > 0) {yCorri = -1;}
          CTC = board.getCardAtPosition(Point(eT[i].x+xCorri,eT[i].y+yCorri));
          if(CTC != null && CTC != card && CTC.isGrayScale() == false && CTC.getCurrentValue() <= 5 && boardutils.isUntargetableByBronze(CTC) == false)
          {
            return true;
          }
        }
      }
      return false;
      break;
      
    case "envelope":
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            for(ttc = 2; ttc < 10; ttc = ttc+2)
            {
              var xCorri = 0;
              var yCorri = 0;
              if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
              if(ttc == 4 && xVaria > 0) {xCorri = -1;}
              if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
              if(ttc == 8 && yVaria > 0) {yCorri = -1;}
              TTC = Point(xVaria+xCorri,yVaria+yCorri);
              CTC = board.getCardAtPosition(TTC);
              if(CTC != null && (xVaria != 0 || yVaria != 0) && CTC.getPlayer() == gameState.getCurrentPlayer() && boardutils.isUntargetableByBronze(CTC) == false)
              {
                return true;
                break;
              }
            }
          }
        }
      }
      return false;
      break;

    case "wrench":
      if(boardutils.limitedByPerfume() == false)
      {
        eT = boardutils.getEmptyTiles();
      }
      else
      {
        eT = boardutils.getTilesAbovePerfume();
      }

      var wrenchAllowedEverywhere = false;
      var trashCards = trash.getAllCards()
      if(trashCards.length > 0)
      {
        for(i = 0; i < trashCards.length; i++)
        {
          if(trashCards[i].getBackground() == "metal")
          {
            wrenchAllowedEverywhere = true;
            return true;
          }
        }
      }

      if(wrenchAllowedEverywhere == false)
      {
        //First condition is not fulfilled, check for the availability of the second ability
        for(i = 0; i < eT.length; i++)
        {
          for(j = 1; j < 9; j++)
          {
            var xCorri = 0;
            var yCorri = 0;
            if(j == 1) {xCorri = 1; yCorri = -2;}
            if(j == 2) {xCorri = 2; yCorri = -1;}
            if(j == 3) {xCorri = 2; yCorri = 1;}
            if(j == 4) {xCorri = 1; yCorri = 2;}
            if(j == 5) {xCorri = -1; yCorri = 2;}
            if(j == 6) {xCorri = -2; yCorri = 1;}
            if(j == 7) {xCorri = -2; yCorri = -1;}
            if(j == 8) {xCorri = -1; yCorri = -2;}

            var CTC = board.getCardAtPosition(Point(eT[i].x+xCorri,eT[i].y+yCorri));
            if(CTC != null && CTC.getCurrentValue() <= 1 && boardutils.checkForUnrotatable(CTC) == false && boardutils.isUntargetableByBronze(CTC) == false)
            {
              return true;
            }
          }
        }
      }
      return false;
      break;

    case "dart":
      var result = false;
      var eT = boardutils.getEmptyTiles();
      var cardsCF = board.getAllCards();

      if(boardutils.limitedByPerfume() == true)
      {
        eT = boardutils.getTilesAbovePerfume();
      }

      for(i = 0; i < eT.length; i++)
      {
        var entryCondition = false;
        var instantlyTrashable = true;
        var instantlyMovable = true;
        var xVaria = eT[i].x;
        var yVaria = eT[i].y;
        
        for(cfut = 0; cfut < cardsCF.length; cfut++)
        {
          if(board.isSafeTile(eT[i]) == true)
          {
            instantlyTrashable = false;
            break;
          }

          if((cardsCF[cfut].getID() == "celticshield" || (cardsCF[cfut].getID() == "handmirror" && boardutils.itemMirrored(cardsCF[cfut]) == "celticshield")) && cardsCF[cfut].isGrayScale() == false && cardsCF[cfut].getPlayer() == gameState.getCurrentPlayer())
          {
            var shieldDistance = Math.pow(cardsCF[cfut].getPosX()-xVaria, 2) + Math.pow(cardsCF[cfut].getPosY()-yVaria, 2)
            if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
            {
              instantlyTrashable = false;
              break;
            }
          }

          if((cardsCF[cfut].getID() == "hardhat" || (cardsCF[cfut].getID() == "handmirror" && boardutils.itemMirrored(cardsCF[cfut]) == "hardhat")) && cardsCF[cfut].isGrayScale() == false)
          {
            switch(cardsCF[cfut].getPlayer())
            {
              case 1:
                if(cardsCF[cfut].getPosX() == xVaria && cardsCF[cfut].getPosY() == yVaria-1) {instantlyTrashable = false;}
                break;
              case 2:
                if(cardsCF[cfut].getPosX() == xVaria && cardsCF[cfut].getPosY() == yVaria+1) {instantlyTrashable = false;}
                break;
              case 3:
                if(cardsCF[cfut].getPosX() == xVaria+1 && cardsCF[cfut].getPosY() == yVaria) {instantlyTrashable = false;}
                break;
              case 4:
                if(cardsCF[cfut].getPosX() == xVaria-1 && cardsCF[cfut].getPosY() == yVaria) {instantlyTrashable = false;}
                break;
            }
          }
        }
        if(board.isInnovativeTile(Point(xVaria,yVaria)) == false)
        {
          for(cfim = 0; cfim < cardsCF.length; cfim++)
          {
            if((cardsCF[cfim].getID() == "gluetube" || (cardsCF[cfim].getID() == "handmirror" && boardutils.itemMirrored(cardsCF[cfim]) == "gluetube")) && cardsCF[cfim].isGrayScale() == false)
            {
              var tubeDistance = Math.pow(cardsCF[cfim].getPosX()-xVaria, 2) + Math.pow(cardsCF[cfim].getPosY()-yVaria, 2)
              if(tubeDistance == 0 || tubeDistance == 1 || tubeDistance == 2)
              {
                instantlyMovable = false;
                break;
              }
            }
          }
        }
        else
        {
          instantlyMovable = false;
        }

        if(instantlyTrashable == true)
        {
          for(xVariab = 0; xVariab < board.getWidth(); xVariab++)
          {
            //If the target tile is not safe
            var CTC = board.getCardAtPosition(Point(xVariab,yVaria));
            if(CTC != null && boardutils.checkForUntrashable(CTC) == false && instantlyMovable == true)
            {
              entryCondition = true;
              break;
            }
            if(CTC != null && boardutils.checkForUntrashable(CTC) == true && CTC.getCurrentValue() > 3)
            {
              entryCondition = true;
              break;
            }
            if(CTC != null && instantlyMovable == false && CTC.getCurrentValue() > 3)
            {
              entryCondition = true;
              break;
            }
          }

          if(entryCondition == false)
          {
            for(yVariab = 0; yVariab < board.getHeight(); yVariab++)
            {
              var CTC = board.getCardAtPosition(Point(xVaria,yVariab));
              if(CTC != null && boardutils.checkForUntrashable(CTC) == false && instantlyMovable == true)
              {
                entryCondition = true;
                break;
              }
              if(CTC != null && boardutils.checkForUntrashable(CTC) == true && CTC.getCurrentValue() > 3)
              {
                entryCondition = true;
                break;
              }
              if(CTC != null && instantlyMovable == false && CTC.getCurrentValue() > 3)
              {
                entryCondition = true;
                break;
              }
            }
          }
        }

        if(instantlyTrashable == false && instantlyMovable == true)
        {
          //If the tile is safe, the only allowed targets are the ones with 100% likeliness to be hit
          for(xVariab = 0; xVariab < board.getWidth(); xVariab++)
          {
            var CTC = board.getCardAtPosition(Point(xVariab,yVaria));
            if(CTC != null && boardutils.checkForUntrashable(CTC) == false && ((CTC.getCurrentValue() <= 0 && boardutils.isSixFix(gameState.getCurrentPlayer()) == false) || (CTC.getCurrentValue() <= 3 && boardutils.isSixFix(gameState.getCurrentPlayer()) == true)))
            {
              entryCondition = true;
              break;
            }

            if(entryCondition == false)
            {
              for(yVariab = 0; yVariab < board.getHeight(); yVariab++)
              {
                var CTC = board.getCardAtPosition(Point(xVaria,yVariab));
                if(CTC != null && boardutils.checkForUntrashable(CTC) == false && ((CTC.getCurrentValue() <= 0 && boardutils.isSixFix(gameState.getCurrentPlayer()) == false) || (CTC.getCurrentValue() <= 3 && boardutils.isSixFix(gameState.getCurrentPlayer()) == true)))
                {
                  entryCondition = true;
                  break;
                }
              }
            }
          }
        }

        if(entryCondition == true)
        {
          result = true;
          break;
        }
      }
      return result;
      break;
      
    case "hourglass":
      var arrayNumber = 0;
      var instantlyRotatable = true;
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            if(board.isSafeTile(Point(xVaria,yVaria)) == false)
            {
              for(ttc = 2; ttc < 10; ttc = ttc+2)
              {
                var xCorri = 0;
                var yCorri = 0;
                if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
                if(ttc == 4 && xVaria > 0) {xCorri = -1;}
                if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
                if(ttc == 8 && yVaria > 0) {yCorri = -1;}
                TTC = Point(xVaria+xCorri,yVaria+yCorri);
                if(board.getCardAtPosition(TTC) != null && (board.getCardAtPosition(TTC)).getPlayer() == gameState.getCurrentPlayer())
                {
                  dominoCondition = true;
                  break;
                }
              }
              if(dominoCondition == true)
              {
                cardsCFUR = board.getAllCards();
                instantlyRotatable = true;
                for(cfur = 0; cfur < cardsCFUR.length; cfur++)
                {
                  if((cardsCFUR[cfur].getID() == "celticshield" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "celticshield")) && cardsCFUR[cfur].isGrayScale() == false && cardsCFUR[cfur].getPlayer() == gameState.getCurrentPlayer())
                  {
                    var shieldDistance = Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2)
                    if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
                    {
                      instantlyRotatable = false;
                      break;
                    }
                  }

                  if(card.getID() != "padlock" || (card.getID() == "padlock" && card.isGrayScale() == true))
                  {
                    if(cardsCFUR[cfur].getID() == "padlock" && cardsCFUR[cfur].isGrayScale() == false && Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2) == 1)
                    {
                      instantlyRotatable = false;
                      break;
                    }
                  }
                  else
                  {
                    instantlyRotatable = false;
                    break;
                  }

                  if((cardsCFUR[cfur].getID() == "hardhat" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "hardhat")) && cardsCFUR[cfur].isGrayScale() == false)
                  {
                    switch(cardsCFUR[cfur].getPlayer())
                    {
                      case 1: if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria-1) {instantlyRotatable = false;} break;
                      case 2: if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria+1) {instantlyRotatable = false;} break;
                      case 3: if(cardsCFUR[cfur].getPosX() == xVaria+1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;} break;
                      case 4: if(cardsCFUR[cfur].getPosX() == xVaria-1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;} break;
                    }
                  }

                }
                if(instantlyRotatable == true)
                {
                  return true;
                  break; break;
                }
              }
            }
          }
        }
      }
      if(instantlyRotatable == false)
      {
        return false
      }
      break;

    case "documentholder":
      if(mainInventory.getAllCards().length > 0)
      {
        return true;
      }
      else
      {
        return false;
      }
      break;

    case "hammer":
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            var dominoCondition = false;
            var entryCondition = false;

            for(ttc = 2; ttc < 10; ttc = ttc+2)
            {
              var xCorri = 0;
              var yCorri = 0;
              if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
              if(ttc == 4 && xVaria > 0) {xCorri = -1;}
              if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
              if(ttc == 8 && yVaria > 0) {yCorri = -1;}
              TTC = Point(xVaria+xCorri,yVaria+yCorri);
              if(board.getCardAtPosition(TTC) != null && (board.getCardAtPosition(TTC)).getPlayer() == gameState.getCurrentPlayer())
              {
                dominoCondition = true;
              }
              if(board.getCardAtPosition(TTC) != null && boardutils.checkForUntrashable(board.getCardAtPosition(TTC)) == false)
              {
                entryCondition = true;
              }
              if(dominoCondition == true && entryCondition == true)
              {
                return true;
                break; break; break; break;
              }
            }
          }
        }
      }
      return false;
      break;

    case "coffeegrinder":
      if((gameState.getCurrentPlayer() == 1 && p1Inventory.getAllCards().length > 2) || (gameState.getCurrentPlayer() == 2 && p2Inventory.getAllCards().length > 2) || (gameState.getCurrentPlayer() == 3 && p3Inventory.getAllCards().length > 2) || (gameState.getCurrentPlayer() == 4 && p4Inventory.getAllCards().length > 2))
      {
        if(mainInventory.getAllCards().length > 0)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
      else
      {
        return false;
      }
      break;

    case "celticshield":
      if((gameState.getCurrentPlayer() == 1 && p1Inventory.getAllCards().length > 1) || (gameState.getCurrentPlayer() == 2 && p2Inventory.getAllCards().length > 1) || (gameState.getCurrentPlayer() == 3 && p3Inventory.getAllCards().length > 1) || (gameState.getCurrentPlayer() == 4 && p4Inventory.getAllCards().length > 1))
      {
        return true;
      }
      else
      {
        return false;
      }
      break;

    case "gardenspade":
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            var dominoCondition = false;
            var entryCondition = false;

            for(ttc = 2; ttc < 10; ttc = ttc+2)
            {
              var xCorri = 0;
              var yCorri = 0;
              if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
              if(ttc == 4 && xVaria > 0) {xCorri = -1;}
              if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
              if(ttc == 8 && yVaria > 0) {yCorri = -1;}
              CTC = board.getCardAtPosition(Point(xVaria+xCorri,yVaria+yCorri));

              if(CTC != null && CTC.getUID() != card.getUID())
              {
                var targetValue = CTC.getBaseValue()

                if(CTC.getPlayer() == gameState.getCurrentPlayer())
                {
                  dominoCondition = true;
                }
                if(targetValue > 0 && boardutils.isUntargetableByBronze(CTC) == false && (targetValue <= (trash.getAllCards()).length || targetValue <= (mainInventory.getAllCards()).length))
                {
                  entryCondition = true;
                }
                if(dominoCondition == true && entryCondition == true)
                {
                  return true;
                  break; break; break;
                }
              }
            }
          }
        }
      }
      if(dominoCondition != true || entryCondition != true)
      {
        return false;
      }
      else
      {
        return true;
      }
      break;

    case "compasses":
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            var dominoCondition = false;
            var entryCondition = false;

            for(ttc = 2; ttc < 10; ttc = ttc+2)
            {
              var xCorri = 0;
              var yCorri = 0;
              if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
              if(ttc == 4 && xVaria > 0) {xCorri = -1;}
              if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
              if(ttc == 8 && yVaria > 0) {yCorri = -1;}
              CTC = board.getCardAtPosition(Point(xVaria+xCorri,yVaria+yCorri));
              if(CTC != null && CTC.getPlayer() == gameState.getCurrentPlayer())
              {
                dominoCondition = true;
              }
              if(CTC != null && CTC.getPlayer() != gameState.getCurrentPlayer() && boardutils.checkForUnrotatable(CTC) == false && CTC.getBackground() == "wood" && CTC.getCurrentValue() <= 4)
              {
                entryCondition = true;
              }
              if(dominoCondition == true && entryCondition == true)
              {
                return true;
                break; break; break; break;
              }
            }
          }
        }
      }
      return false;
      break;

    case "flyswatter":

      var result = false;
      if(boardutils.limitedByPerfume() == false)
      {
        eT = boardutils.getEmptyTiles();
      }
      else
      {
        eT = boardutils.getTilesAbovePerfume();
      }
      for(i = 0; i < eT.length; i++)
      {
        for(ctc = 1; ctc < 10; ctc++)
        {
          var xCorri = 0;
          var yCorri = 0;
          if(ctc == 5) {ctc++;}

          if(ctc == 1 && eT[i].x > 0 && eT[i].y < board.getHeight() - 1) {xCorri = -1; yCorri = 1;}
          if(ctc == 2 && eT[i].y < board.getHeight() - 1) {yCorri = 1;}
          if(ctc == 3 && eT[i].x < board.getWidth() - 1 && eT[i].y < board.getHeight() - 1) {xCorri = 1; yCorri = 1;}
          if(ctc == 4 && eT[i].x > 0) {xCorri = -1;}
          if(ctc == 6 && eT[i].x < board.getWidth() - 1) {xCorri = 1;}
          if(ctc == 7 && eT[i].x > 0 && eT[i].y > 0) {xCorri = -1; yCorri = -1;}
          if(ctc == 8 && eT[i].y > 0) {yCorri = -1;}
          if(ctc == 9 && eT[i].x < board.getWidth() - 1 && eT[i].y > 0) {xCorri = 1; yCorri = -1;}

          CTC = board.getCardAtPosition(Point((eT[i].x)+xCorri,(eT[i].y)+yCorri));
          if(CTC != null && CTC.getBaseValue() < 2 && boardutils.checkForUntrashable(CTC) == false)
          {
            result = true;
            break; break;
          }
        }
      }
      return result;
      break;

    case "megaphone":
      var result = false;
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            //Changed the variable names to avoid conflict during called condition checks
            for(ttcm = 2; ttcm < 10; ttcm = ttcm+2)
            {
              var xCorri = 0;
              var yCorri = 0;
              if(ttcm == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
              if(ttcm == 4 && xVaria > 0) {xCorri = -1;}
              if(ttcm == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
              if(ttcm == 8 && yVaria > 0) {yCorri = -1;}
              TTCM = Point(xVaria+xCorri,yVaria+yCorri);
              CTCM = board.getCardAtPosition(TTCM);
              if(CTCM != null && CTCM.getPlayer() == gameState.getCurrentPlayer() && boardutils.checkForUnpickable(CTCM) == false && boardutils.isUntargetableByBronze(CTCM) == false)
              {
                if(boardutils.limitedByPerfume() == false || boardutils.tileAffectedByPerfume(xVaria+xCorri, yVaria+yCorri) == true || ((boardutils.getTilesAbovePerfume()).length == 1 && boardutils.tileAffectedByPerfume(xVaria, yVaria) == true))
                {
                  if(megaputils.secondMegaphoneCondition(CTCM, xVaria, yVaria) == true)
                  {
                    result = true;
                    break; break; break;
                  }
                }
              }
            }
          }
        }
      }
      return result;
      break;

    case "dictionary":
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            var entryCondition = false;
            var itemsToTranslate = [];
            for(ctc = 2; ctc < 10; ctc = ctc+2)
            {
              var xCorri = 0;
              var yCorri = 0;
              if(ctc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
              if(ctc == 4 && xVaria > 0) {xCorri = -1;}
              if(ctc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
              if(ctc == 8 && yVaria > 0) {yCorri = -1;}
              CTC = board.getCardAtPosition(Point(xVaria+xCorri,yVaria+yCorri));
              if(CTC != null && CTC != card)
              {
                itemsToTranslate.push(CTC);
              }
            }

            if(itemsToTranslate.length > 0)
            {
              for(i = 0; i < itemsToTranslate.length; i++)
              {
                if(itemsToTranslate[i].getPlayer() == gameState.getCurrentPlayer())
                {
                  if(boardutils.checkForUnrotatable(itemsToTranslate[i]) == false)
                  {
                    entryCondition = true;
                    var translationToP1 = true;
                    var translationToP2 = true;
                    var translationToP3 = true;
                    var translationToP4 = true;
                    switch(gameState.getCurrentPlayer())
                    {
                      case 1: translationToP1 = false; break;
                      case 2: translationToP2 = false; break;
                      case 3: translationToP3 = false; break;
                      case 4: translationToP4 = false; break;
                    }
                    switch(gameState.getGameMode())
                    {
                      case 0: translationToP3 = false; translationToP4 = false; break;
                      case 2: translationToP4 = false; break;
                      case 4: translationToP2 = false; translationToP4 = false; break;
                    }

                    for(j = 0; j < itemsToTranslate.length; j++)
                    {
                      if(itemsToTranslate[j].getPlayer() != gameState.getCurrentPlayer())
                      {
                        if(!board.isDoubleTile(itemsToTranslate[j].getPosition()) && !board.isSafeTile(itemsToTranslate[j].getPosition()))
                        {
                          if(boardutils.checkForUnrotatable(itemsToTranslate[j]) == true)
                          {
                            switch(itemsToTranslate[j].getPlayer())
                            {
                              case 1: translationToP1 = false; break;
                              case 2: translationToP2 = false; break;
                              case 3: translationToP3 = false; break;
                              case 4: translationToP4 = false; break;
                            }
                          }
                        }
                      }
                    }

                    if(translationToP1 == true || translationToP2 == true || translationToP3 == true || translationToP4 == true)
                    {
                      return true;
                      break; break; break; break; break;
                    }

                  }
                }
              }
            }
          }
        }
      }
      return false;
      break;

    case "operaglove":
      if(gameState.getCurrentPlayer() == 1) {var cardsInCurrentHand = p1Inventory.getAllCards().length;}
      if(gameState.getCurrentPlayer() == 2) {var cardsInCurrentHand = p2Inventory.getAllCards().length;}
      if(gameState.getCurrentPlayer() == 3) {var cardsInCurrentHand = p3Inventory.getAllCards().length;}
      if(gameState.getCurrentPlayer() == 4) {var cardsInCurrentHand = p4Inventory.getAllCards().length;}
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            var dominoCondition = false;
            var entryCondition = false;

            for(ttc = 2; ttc < 10; ttc = ttc+2)
            {
              var xCorri = 0;
              var yCorri = 0;
              if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
              if(ttc == 4 && xVaria > 0) {xCorri = -1;}
              if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
              if(ttc == 8 && yVaria > 0) {yCorri = -1;}
              TTC = Point(xVaria+xCorri,yVaria+yCorri);
              if(board.getCardAtPosition(TTC) != null && (board.getCardAtPosition(TTC)).getPlayer() == gameState.getCurrentPlayer())
              {
                dominoCondition = true;
              }
              if(board.getCardAtPosition(TTC) != null && boardutils.checkForUnpickable(board.getCardAtPosition(TTC)) == false)
              {
                var amountToDiscard = Math.round((board.getCardAtPosition(TTC)).getCurrentValue()/2);
                if(amountToDiscard < 1)
                {
                  amountToDiscard = 1;
                }
                if(amountToDiscard <= cardsInCurrentHand + 1)
                {
                  entryCondition = true;
                }
              }
              if(dominoCondition == true && entryCondition == true)
              {
                return true;
                break; break; break; break;
              }
            }
          }
        }
      }
      return false;
      break;

    case "football":
      var eT = [];
      if(boardutils.limitedByPerfume() == false)
      {
        eT = boardutils.getEmptyTiles();
      }
      else
      {
        eT = boardutils.getTilesAbovePerfume();
      }
      if(boardutils.isHalted(eT) == false)
      {
        if(mainInventory.getAllCards().length > 1)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
      else
      {
        return true;
      }
      break;

    case "adhesivebandage":
      if(trash.getAllCards().length > 0)
      {
        var arrayNumber = 0;
        for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
        {
          for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
          {
            if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
            {
              var patchedTile = 0;
              var entryCondition = false;
              var dominoCondition = false;
              switch(gameState.getCurrentPlayer())
              {
                case 1:
                  if(board.getCardAtPosition(Point(xVaria,yVaria+1)) == null && yVaria < board.getHeight()-1)
                  {
                    patchedTile = Point(xVaria,yVaria+1);
                  }
                  break;
                case 2:
                  if(board.getCardAtPosition(Point(xVaria,yVaria-1)) == null && yVaria > 0)
                  {
                    patchedTile = Point(xVaria,yVaria-1);
                  }
                  break;
                case 3:
                  if(board.getCardAtPosition(Point(xVaria-1,yVaria)) == null && xVaria > 0)
                  {
                    patchedTile = Point(xVaria-1,yVaria);
                  }
                  break;
                case 4:
                  if(board.getCardAtPosition(Point(xVaria+1,yVaria)) == null && xVaria < board.getWidth()-1)
                  {
                    patchedTile = Point(xVaria+1,yVaria);
                  }
                  break;
              }
              if(patchedTile != 0 && board.getCardAtPosition(patchedTile) == null && board.isDisabledTile(patchedTile) == false)
              {
                var trashCards = trash.getAllCards();
                for(i = 0; i < trashCards.length; i++)
                {
                  if(patchutils.secondBandageCondition(trashCards[i], patchedTile.x, patchedTile.y) == true)
                  {
                    entryCondition = true;
                    break;
                  }
                }
              }

              if(entryCondition == true)
              {
                //Checking for domino condition
                for(ttcp = 2; ttcp < 10; ttcp = ttcp+2)
                {
                  var xCorri = 0;
                  var yCorri = 0;
                  if(ttcp == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
                  if(ttcp == 4 && xVaria > 0) {xCorri = -1;}
                  if(ttcp == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
                  if(ttcp == 8 && yVaria > 0) {yCorri = -1;}
                  TTCP = Point(xVaria+xCorri,yVaria+yCorri);
                  CTCP = board.getCardAtPosition(TTCP);
                  if(CTCP != null && CTCP.getPlayer() == gameState.getCurrentPlayer())
                  {
                    return true;
                    break; break; break; break;
                  }
                }
              }

            }
          }
        }
      }
      return false;
      break;

    case "broom":
      var result = false;
      var eT = boardutils.getEmptyTiles();
      if(boardutils.isHalted(eT) == false)
      {
        if(boardutils.limitedByPerfume() == true)
        {
          eT = boardutils.getTilesAbovePerfume();
        }
        cards = board.getAllCards();
        for(i = 0; i < eT.length; i++)
        {
          var xVaria = eT[i].x;
          var yVaria = eT[i].y;
          var countMovableX = 0
          var countMovableY = 0
          var countImmovableX = 0
          var countImmovableY = 0
          var xDisab = 0
          var yDisab = 0

          for(xVariab = 0; xVariab < board.getWidth(); xVariab++)
          {
            if(board.isDisabledTile(Point(xVariab,yVaria)) == true)
            {
              xDisab++;
            }
          }
          for(yVariab = 0; yVariab < board.getHeight(); yVariab++)
          {
            if(board.isDisabledTile(Point(xVaria,yVariab)) == true)
            {
              yDisab++;
            }
          }

          for(j = 0; j < cards.length; j++)
          {
            if(cards[j].getPosX() == xVaria)
            {
              if(boardutils.checkForImmovable(cards[j]) == true || boardutils.isUntargetableByBronze(cards[j]) == true)
              {
                countImmovableX++;
              }
              else
              {
                countMovableX++;
              }
            }
            if(cards[j].getPosY() == yVaria)
            {
              if(boardutils.checkForImmovable(cards[j]) == true || boardutils.isUntargetableByBronze(cards[j]) == true)
              {
                countImmovableY++;
              }
              else
              {
                countMovableY++;
              }
            }
          }

          if(countMovableX + countImmovableX + countMovableY + countImmovableY < (board.getWidth() + board.getHeight() - xDisab - yDisab - 2) && countMovableX + countMovableY > 0)
          {
            if((countMovableX == 0 && countMovableY + countImmovableY == board.getWidth() - xDisab - 1) || (countMovableY == 0 && countMovableX + countImmovableX == board.getHeight() - yDisab - 1))
            {
            }
            else
            {
              result = true;
              break;
            }
          }
        }
      }
      else
      {
        result = true;
      }
      return result;
      break;

    case "souvenir":
      var result = false;
      var sumCardsInHands = 0;
      switch(gameState.getGameMode())
      {
        case 0:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length);
          break;
        case 1:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length + p4Inventory.getAllCards().length);
          break;
        case 2:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length);
          break;
        case 3:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length + p4Inventory.getAllCards().length);
          break;
        case 4:
          sumCardsInHands = (p1Inventory.getAllCards().length + p3Inventory.getAllCards().length);
          break;
      }
      switch(gameState.getCurrentPlayer())
      {
        case 1:
          sumCardsInHands = sumCardsInHands - p1Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p4Inventory.getAllCards().length;}
          break;
        case 2:
          sumCardsInHands = sumCardsInHands - p2Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p3Inventory.getAllCards().length;}
          break;
        case 3:
          sumCardsInHands = sumCardsInHands - p3Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p2Inventory.getAllCards().length;}
          break;
        case 4:
          sumCardsInHands = sumCardsInHands - p4Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p1Inventory.getAllCards().length;}
          break;
      }

      var eT = boardutils.getEmptyTiles();
      //If there is at least one card in the hand of one opponent
      if(sumCardsInHands > 0 && eT.length > 0)
      {
        if(boardutils.limitedByPerfume() == true)
        {
          eT = boardutils.getTilesAbovePerfume();
        }

        for(i = 0; i < eT.length; i++)
        {
          var xVaria = eT[i].x;
          var yVaria = eT[i].y;

          cardsCFUR = board.getAllCards();
          var instantlyRotatable = true;
          for(cfur = 0; cfur < cardsCFUR.length; cfur++)
          {
            if(board.isSafeTile(Point(xVaria,yVaria)) == true)
            {
              instantlyRotatable = false;
              break;
            }

            if((cardsCFUR[cfur].getID() == "celticshield" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "celticshield")) && cardsCFUR[cfur].isGrayScale() == false && cardsCFUR[cfur].getPlayer() == card.getPlayer())
            {
              var shieldDistance = Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2)
              if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
              {
                instantlyRotatable = false;
                break;
              }
            }

            if(cardsCFUR[cfur].getID() == "padlock" && cardsCFUR[cfur].isGrayScale() == false && Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2) == 1)
            {
              instantlyRotatable = false;
              break;
            }

            if((cardsCFUR[cfur].getID() == "hardhat" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "hardhat")) && cardsCFUR[cfur].isGrayScale() == false)
            {
              switch(cardsCFUR[cfur].getPlayer())
              {
                case 1:
                  if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria-1) {instantlyRotatable = false;}
                  break;
                case 2:
                  if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria+1) {instantlyRotatable = false;}
                  break;
                case 3:
                  if(cardsCFUR[cfur].getPosX() == xVaria+1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;}
                  break;
                case 4:
                  if(cardsCFUR[cfur].getPosX() == xVaria-1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;}
                  break;
              }
            }
          }

          if(instantlyRotatable == true)
          {
            result = true;
            break;
            array.push(eT[i]);
          }

        }
      }
      return result;
      break;

    case "binoculars":
      eT = boardutils.getEmptyTiles();
      if(boardutils.isHalted(eT) == false)
      {
        if(mainInventory.getAllCards().length > 0)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
      else
      {
        return true;
      }
      break;

    case "whiteknight":
      if(mainInventory.getAllCards().length > 0)
      {
        return true;
      }
      else
      {
        return false;
      }
      break;

    default:
      if(choiceForWatch.hasIcon("dmno"))
      {
        cards = board.getAllCards();
        for(isOwn = 0; isOwn < cards.length; isOwn++)
        {
          if(cards[isOwn].getPlayer() == gameState.getCurrentPlayer())
          {
            for(ttc = 2; ttc < 10; ttc = ttc+2)
            {
              var xCorri = 0;
              var yCorri = 0;
              if(ttc == 2 && cards[isOwn].getPosY() < board.getHeight() - 1) {yCorri = 1;}
              if(ttc == 4 && cards[isOwn].getPosX() > 0) {xCorri = -1;}
              if(ttc == 6 && cards[isOwn].getPosX() < board.getWidth() - 1) {xCorri = 1;}
              if(ttc == 8 && cards[isOwn].getPosY() > 0) {yCorri = -1;}
              TTC = Point(cards[isOwn].getPosX()+xCorri,cards[isOwn].getPosY()+yCorri);
              if(board.getCardAtPosition(TTC) == null && board.isDisabledTile(TTC) == false)
              {
                return true;
                break;
              }
            }
          }
        }
        return false;
        break;
      }
      return true;
  }
}

watchutils.getValidTiles = function(choiceForWatch)
{
  var result = new Array();

  switch(choiceForWatch.getID())
  {
    case "giftbox":
      var arrayNumber = 0;
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            if(board.isSafeTile(Point(xVaria,yVaria)) == false)
            {
              cardsCFUR = board.getAllCards();
              var instantlyRotatable = true;
              for(cfur = 0; cfur < cardsCFUR.length; cfur++)
              {

                if((cardsCFUR[cfur].getID() == "celticshield" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "celticshield")) && cardsCFUR[cfur].isGrayScale() == false && cardsCFUR[cfur].getPlayer() == gameState.getCurrentPlayer())
                {
                  var shieldDistance = Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2)
                  if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
                  {
                    instantlyRotatable = false;
                    break;
                  }
                }

                if(card.getID() != "padlock" || (card.getID() == "padlock" && card.isGrayScale() == true))
                {
                  if(cardsCFUR[cfur].getID() == "padlock" && cardsCFUR[cfur].isGrayScale() == false && Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2) == 1)
                  {
                    instantlyRotatable = false;
                    break;
                  }
                }
                else
                {
                  instantlyRotatable = false;
                  break;
                }

                if((cardsCFUR[cfur].getID() == "hardhat" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "hardhat")) && cardsCFUR[cfur].isGrayScale() == false)
                {
                  switch(cardsCFUR[cfur].getPlayer())
                  {
                    case 1: if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria-1) {instantlyRotatable = false;} break;
                    case 2: if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria+1) {instantlyRotatable = false;} break;
                    case 3: if(cardsCFUR[cfur].getPosX() == xVaria+1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;} break;
                    case 4: if(cardsCFUR[cfur].getPosX() == xVaria-1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;} break;
                  }
                }
              }

              if(instantlyRotatable == true)
              {
                result.push(Point(xVaria,yVaria));
              }
            }
          }
        }
      }
      return result;
  
    case "goldencup":
      var arrayNumber = 0;
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            var dominoCondition = false;
            var adjacentToCup = 0;
            for(ttc = 2; ttc < 10; ttc = ttc+2)
            {
              var xCorri = 0;
              var yCorri = 0;
              if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
              if(ttc == 4 && xVaria > 0) {xCorri = -1;}
              if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
              if(ttc == 8 && yVaria > 0) {yCorri = -1;}
              TTC = Point(xVaria+xCorri,yVaria+yCorri);
              CTC = board.getCardAtPosition(TTC);
              if(CTC != null)
              {
                adjacentToCup++;
                if(CTC.getPlayer() == gameState.getCurrentPlayer())
                {
                  dominoCondition = true;
                }
              }
              if(dominoCondition == true && ttc == 8)
              {
                if( ((gameState.getGameMode() == 0 || gameState.getGameMode() == 4) && mainInventory.getAllCards().length >= adjacentToCup) || 
                    ((gameState.getGameMode() == 1 || gameState.getGameMode() == 2) && mainInventory.getAllCards().length >= adjacentToCup*2) ||
                    ((gameState.getGameMode() == 3) && mainInventory.getAllCards().length >= adjacentToCup*3) )
                {
                  result.push(Point(xVaria,yVaria));
                  break;
                }
              }
            }
          }
        }
      }
      return result;
      break;

    case "baseballbat":
      if(boardutils.limitedByPerfume() == false)
      {
        eT = boardutils.getEmptyTiles();
      }
      else
      {
        eT = boardutils.getTilesAbovePerfume();
      }
      for(i = 0; i < eT.length; i++)
      {
        for(ctc = 2; ctc < 10; ctc = ctc+2)
        {
          var xCorri = 0;
          var yCorri = 0;
          if(ctc == 2 && eT[i].y < board.getHeight() - 1) {yCorri = 1;}
          if(ctc == 4 && eT[i].x > 0) {xCorri = -1;}
          if(ctc == 6 && eT[i].x < board.getWidth() - 1) {xCorri = 1;}
          if(ctc == 8 && eT[i].y > 0) {yCorri = -1;}
          CTC = board.getCardAtPosition(Point(eT[i].x+xCorri,eT[i].y+yCorri));
          if(CTC != null && CTC != card && CTC.isGrayScale() == false && CTC.getCurrentValue() <= 5 && boardutils.isUntargetableByBronze(CTC) == false)
          {
            result.push(eT[i]);
            break;
          }
        }
      }
      return result;
      break;

    case "wrench":
      if(boardutils.limitedByPerfume() == false)
      {
        eT = boardutils.getEmptyTiles();
      }
      else
      {
        eT = boardutils.getTilesAbovePerfume();
      }

      var wrenchAllowedEverywhere = false;
      var trashCards = trash.getAllCards()
      if(trashCards.length > 0)
      {
        for(i = 0; i < trashCards.length; i++)
        {
          if(trashCards[i].getBackground() == "metal")
          {
            wrenchAllowedEverywhere = true;
            return eT;
            break;
          }
        }
      }
      return result;
      break;
      
    case "envelope":
      // WIP
      //if(boardutils.limitedByPerfume() == true)
      //{
      //  return boardutils.getTilesAbovePerfume();
      //}

      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            for(ttc = 2; ttc < 10; ttc = ttc+2)
            {
              var xCorri = 0;
              var yCorri = 0;
              if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
              if(ttc == 4 && xVaria > 0) {xCorri = -1;}
              if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
              if(ttc == 8 && yVaria > 0) {yCorri = -1;}
              TTC = Point(xVaria+xCorri,yVaria+yCorri);
              CTC = board.getCardAtPosition(TTC);
              if(CTC != null && (xVaria != 0 || yVaria != 0) && CTC.getPlayer() == gameState.getCurrentPlayer() && boardutils.isUntargetableByBronze(CTC) == false)
              {
                result.push(Point(xVaria,yVaria));
                break;
              }
            }
          }
        }
      }
      return result;
      break;

    case "dart":
      var eT = boardutils.getEmptyTiles();
      if(boardutils.limitedByPerfume() == true)
      {
        eT = boardutils.getTilesAbovePerfume();
      }

      for(i = 0; i < eT.length; i++)
      {
        var entryCondition = false;
        var instantlyTrashable = true;
        var instantlyMovable = true;
        var xVaria = eT[i].x;
        var yVaria = eT[i].y;

        cardsCF = board.getAllCards();
        for(cfut = 0; cfut < cardsCF.length; cfut++)
        {
          if(board.isSafeTile(Point(xVaria,yVaria)) == true)
          {
            instantlyTrashable = false;
            break;
          }

          if((cardsCF[cfut].getID() == "celticshield" || (cardsCF[cfut].getID() == "handmirror" && boardutils.itemMirrored(cardsCF[cfut]) == "celticshield")) && cardsCF[cfut].isGrayScale() == false && cardsCF[cfut].getPlayer() == gameState.getCurrentPlayer())
          {
            var shieldDistance = Math.pow(cardsCF[cfut].getPosX()-xVaria, 2) + Math.pow(cardsCF[cfut].getPosY()-yVaria, 2)
            if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
            {
              instantlyTrashable = false;
              break;
            }
          }

          if((cardsCF[cfut].getID() == "hardhat" || (cardsCF[cfut].getID() == "handmirror" && boardutils.itemMirrored(cardsCF[cfut]) == "hardhat")) && cardsCF[cfut].isGrayScale() == false)
          {
            switch(cardsCF[cfut].getPlayer())
            {
              case 1:
                if(cardsCF[cfut].getPosX() == xVaria && cardsCF[cfut].getPosY() == yVaria-1) {instantlyTrashable = false;}
                break;
              case 2:
                if(cardsCF[cfut].getPosX() == xVaria && cardsCF[cfut].getPosY() == yVaria+1) {instantlyTrashable = false;}
                break;
              case 3:
                if(cardsCF[cfut].getPosX() == xVaria+1 && cardsCF[cfut].getPosY() == yVaria) {instantlyTrashable = false;}
                break;
              case 4:
                if(cardsCF[cfut].getPosX() == xVaria-1 && cardsCF[cfut].getPosY() == yVaria) {instantlyTrashable = false;}
                break;
            }
          }
        }
        for(cfim = 0; cfim < cardsCF.length; cfim++)
        {
          if((cardsCF[cfim].getID() == "gluetube" || (cardsCF[cfim].getID() == "handmirror" && boardutils.itemMirrored(cardsCF[cfim]) == "gluetube")) && cardsCF[cfim].isGrayScale() == false)
          {
            var tubeDistance = Math.pow(cardsCF[cfim].getPosX()-xVaria, 2) + Math.pow(cardsCF[cfim].getPosY()-yVaria, 2)
            if(tubeDistance == 0 || tubeDistance == 1 || tubeDistance == 2)
            {
              instantlyMovable = false;
              break;
            }
          }
        }

        if(instantlyTrashable == true)
        {
          for(xVariab = 0; xVariab < board.getWidth(); xVariab++)
          {
            //If the target tile is not safe
            var CTC = board.getCardAtPosition(Point(xVariab,yVaria));
            if(CTC != null && boardutils.checkForUntrashable(CTC) == false && instantlyMovable == true)
            {
              entryCondition = true;
              break;
            }
            if(CTC != null && boardutils.checkForUntrashable(CTC) == true && CTC.getCurrentValue() > 3)
            {
              entryCondition = true;
              break;
            }
            if(CTC != null && instantlyMovable == false && CTC.getCurrentValue() > 3)
            {
              entryCondition = true;
              break;
            }
          }

          if(entryCondition == false)
          {
            for(yVariab = 0; yVariab < board.getHeight(); yVariab++)
            {
              var CTC = board.getCardAtPosition(Point(xVaria,yVariab));
              if(CTC != null && boardutils.checkForUntrashable(CTC) == false && instantlyMovable == true)
              {
                entryCondition = true;
                break;
              }
              if(CTC != null && boardutils.checkForUntrashable(CTC) == true && CTC.getCurrentValue() > 3)
              {
                entryCondition = true;
                break;
              }
              if(CTC != null && instantlyMovable == false && CTC.getCurrentValue() > 3)
              {
                entryCondition = true;
                break;
              }
            }
          }
        }

        if(instantlyTrashable == false && instantlyMovable == true)
        {
          //If the tile is safe, the only allowed targets are the ones with 100% likeliness to be hit
          for(xVariab = 0; xVariab < board.getWidth(); xVariab++)
          {
            var CTC = board.getCardAtPosition(Point(xVariab,yVaria));
            if(CTC != null && boardutils.checkForUntrashable(CTC) == false && ((CTC.getCurrentValue() <= 0 && boardutils.isSixFix(gameState.getCurrentPlayer()) == false) || (CTC.getCurrentValue() <= 3 && boardutils.isSixFix(gameState.getCurrentPlayer()) == true)))
            {
              entryCondition = true;
              break;
            }

            if(entryCondition == false)
            {
              for(yVariab = 0; yVariab < board.getHeight(); yVariab++)
              {
                var CTC = board.getCardAtPosition(Point(xVaria,yVariab));
                if(CTC != null && boardutils.checkForUntrashable(CTC) == false && ((CTC.getCurrentValue() <= 0 && boardutils.isSixFix(gameState.getCurrentPlayer()) == false) || (CTC.getCurrentValue() <= 3 && boardutils.isSixFix(gameState.getCurrentPlayer()) == true)))
                {
                  entryCondition = true;
                  break;
                }
              }
            }
          }
        }

        if(entryCondition == true)
        {
          result.push(eT[i]);
        }
      }
      break;

    case "hourglass":
      var arrayNumber = 0;
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            if(board.isSafeTile(Point(xVaria,yVaria)) == false)
            {
              var dominoCondition = false;
              for(ttc = 2; ttc < 10; ttc = ttc+2)
              {
                var xCorri = 0;
                var yCorri = 0;
                if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
                if(ttc == 4 && xVaria > 0) {xCorri = -1;}
                if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
                if(ttc == 8 && yVaria > 0) {yCorri = -1;}
                TTC = Point(xVaria+xCorri,yVaria+yCorri);
                CTC = board.getCardAtPosition(TTC);
                if(CTC != null && CTC.getPlayer() == gameState.getCurrentPlayer())
                {
                  dominoCondition = true;
                }
              }
              if(dominoCondition == true)
              {
                cardsCFUR = board.getAllCards();
                var instantlyRotatable = true;
                for(cfur = 0; cfur < cardsCFUR.length; cfur++)
                {

                  if((cardsCFUR[cfur].getID() == "celticshield" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "celticshield")) && cardsCFUR[cfur].isGrayScale() == false && cardsCFUR[cfur].getPlayer() == gameState.getCurrentPlayer())
                  {
                    var shieldDistance = Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2)
                    if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
                    {
                      instantlyRotatable = false;
                      break;
                    }
                  }

                  if(card.getID() != "padlock" || (card.getID() == "padlock" && card.isGrayScale() == true))
                  {
                    if(cardsCFUR[cfur].getID() == "padlock" && cardsCFUR[cfur].isGrayScale() == false && Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2) == 1)
                    {
                      instantlyRotatable = false;
                      break;
                    }
                  }
                  else
                  {
                    instantlyRotatable = false;
                    break;
                  }

                  if((cardsCFUR[cfur].getID() == "hardhat" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "hardhat")) && cardsCFUR[cfur].isGrayScale() == false)
                  {
                    switch(cardsCFUR[cfur].getPlayer())
                    {
                      case 1: if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria-1) {instantlyRotatable = false;} break;
                      case 2: if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria+1) {instantlyRotatable = false;} break;
                      case 3: if(cardsCFUR[cfur].getPosX() == xVaria+1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;} break;
                      case 4: if(cardsCFUR[cfur].getPosX() == xVaria-1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;} break;
                    }
                  }
                }
              }

              if(dominoCondition == true && instantlyRotatable == true)
              {
                result.push(Point(xVaria,yVaria));
              }
            }
          }
        }
      }
      return result;

    case "hammer":
      var arrayNumber = 0;
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            var dominoCondition = false;
            var entryCondition = false;

            for(ttc = 2; ttc < 10; ttc = ttc+2)
            {
              var xCorri = 0;
              var yCorri = 0;
              if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
              if(ttc == 4 && xVaria > 0) {xCorri = -1;}
              if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
              if(ttc == 8 && yVaria > 0) {yCorri = -1;}
              TTC = Point(xVaria+xCorri,yVaria+yCorri);
              CTC = board.getCardAtPosition(TTC);
              if(CTC != null && CTC.getPlayer() == gameState.getCurrentPlayer())
              {
                dominoCondition = true;
              }
              if(CTC != null && boardutils.checkForUntrashable(CTC) == false && boardutils.isUntargetableByBronze(CTC) == false)
              {
                entryCondition = true;
              }
              if(dominoCondition == true && entryCondition == true)
              {
                result.push(Point(xVaria,yVaria));
                break;
              }
            }
          }
        }
      }
      return result;

    case "gardenspade":
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            var dominoCondition = false;
            var entryCondition = false;

            for(ttc = 2; ttc < 10; ttc = ttc+2)
            {
              var xCorri = 0;
              var yCorri = 0;
              if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
              if(ttc == 4 && xVaria > 0) {xCorri = -1;}
              if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
              if(ttc == 8 && yVaria > 0) {yCorri = -1;}
              CTC = board.getCardAtPosition(Point(xVaria+xCorri,yVaria+yCorri));

              if(CTC != null && CTC.getUID() != card.getUID())
              {
                var targetValue = CTC.getBaseValue()

                if(CTC.getPlayer() == gameState.getCurrentPlayer())
                {
                  dominoCondition = true;
                }
                if(targetValue > 0 && boardutils.isUntargetableByBronze(CTC) == false && (targetValue <= (trash.getAllCards()).length || targetValue <= (mainInventory.getAllCards()).length))
                {
                  entryCondition = true;
                }
                if(dominoCondition == true && entryCondition == true)
                {
                  result.push(Point(xVaria,yVaria));
                  break;
                }
              }
            }
          }
        }
      }
      return result;
      break;

    case "compasses":
      var arrayNumber = 0;
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            var dominoCondition = false;
            var entryCondition = false;

            for(ttc = 2; ttc < 10; ttc = ttc+2)
            {
              var xCorri = 0;
              var yCorri = 0;
              if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
              if(ttc == 4 && xVaria > 0) {xCorri = -1;}
              if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
              if(ttc == 8 && yVaria > 0) {yCorri = -1;}
              TTC = Point(xVaria+xCorri,yVaria+yCorri);
              CTC = board.getCardAtPosition(TTC);
              if(CTC != null && CTC.getPlayer() == gameState.getCurrentPlayer())
              {
                dominoCondition = true;
              }
              if(CTC != null && boardutils.checkForUnrotatable(CTC) == false && CTC.getPlayer() != gameState.getCurrentPlayer() && CTC.getBackground() == "wood" && CTC.getCurrentValue() <= 4 && boardutils.isUntargetableByBronze(CTC) == false)
              {
                entryCondition = true;
              }
              if(dominoCondition == true && entryCondition == true)
              {
                result.push(Point(xVaria,yVaria));
                break;
              }
            }
          }
        }
      }
      return result;
      break;

    case "flyswatter":
      if(boardutils.limitedByPerfume() == false)
      {
        eT = boardutils.getEmptyTiles();
      }
      else
      {
        eT = boardutils.getTilesAbovePerfume();
      }

      for(i = 0; i < eT.length; i++)
      {
        for(ctc = 1; ctc < 10; ctc++)
        {
          var xCorri = 0;
          var yCorri = 0;
          if(ctc == 5) {ctc++;}

          if(ctc == 1 && eT[i].x > 0 && eT[i].y < board.getHeight() - 1) {xCorri = -1; yCorri = 1;}
          if(ctc == 2 && eT[i].y < board.getHeight() - 1) {yCorri = 1;}
          if(ctc == 3 && eT[i].x < board.getWidth() - 1 && eT[i].y < board.getHeight() - 1) {xCorri = 1; yCorri = 1;}
          if(ctc == 4 && eT[i].x > 0) {xCorri = -1;}
          if(ctc == 6 && eT[i].x < board.getWidth() - 1) {xCorri = 1;}
          if(ctc == 7 && eT[i].x > 0 && eT[i].y > 0) {xCorri = -1; yCorri = -1;}
          if(ctc == 8 && eT[i].y > 0) {yCorri = -1;}
          if(ctc == 9 && eT[i].x < board.getWidth() - 1 && eT[i].y > 0) {xCorri = 1; yCorri = -1;}

          CTC = board.getCardAtPosition(Point((eT[i].x)+xCorri,(eT[i].y)+yCorri));
          if(CTC != null && CTC.getBaseValue() < 2 && boardutils.checkForUntrashable(CTC) == false)
          {
            result.push(eT[i]);
            break;
          }
        }
      }
      return result;
      break;

    case "megaphone":
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            //Changed the variable names to avoid conflict during called condition checks
            for(ttcm = 2; ttcm < 10; ttcm = ttcm+2)
            {
              var xCorri = 0;
              var yCorri = 0;
              if(ttcm == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
              if(ttcm == 4 && xVaria > 0) {xCorri = -1;}
              if(ttcm == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
              if(ttcm == 8 && yVaria > 0) {yCorri = -1;}
              TTCM = Point(xVaria+xCorri,yVaria+yCorri);
              CTCM = board.getCardAtPosition(TTCM);
              if(CTCM != null && CTCM.getPlayer() == gameState.getCurrentPlayer() && boardutils.checkForUnpickable(CTCM) == false && boardutils.isUntargetableByBronze(CTCM) == false)
              {
                if(boardutils.limitedByPerfume() == false || boardutils.tileAffectedByPerfume(xVaria+xCorri, yVaria+yCorri) == true || ((boardutils.getTilesAbovePerfume()).length == 1 && boardutils.tileAffectedByPerfume(xVaria, yVaria) == true))
                {
                  if(megaputils.secondMegaphoneCondition(CTCM, xVaria, yVaria) == true)
                  {
                    result.push(Point(xVaria,yVaria));
                    break; break; break;
                  }
                }
              }
            }
          }
        }
      }
      break;

    case "dictionary":
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          materialsAvailable = []; //This will store the materials which can be considered to be allowed to rotate
          materialsForbidden = []; //This will store the materials which are not possible to rotate together
          materialsForbidden1 = [];
          materialsForbidden2 = [];
          materialsForbidden3 = [];
          materialsForbidden4 = [];
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {

            var isAdjacent = false
            if(board.getCardAtPosition(Point(xVaria+1,yVaria)) != null && (board.getCardAtPosition(Point(xVaria+1,yVaria))).getPlayer() == gameState.getCurrentPlayer()) {isAdjacent = true}
            if(board.getCardAtPosition(Point(xVaria,yVaria+1)) != null && (board.getCardAtPosition(Point(xVaria,yVaria+1))).getPlayer() == gameState.getCurrentPlayer()) {isAdjacent = true}
            if(board.getCardAtPosition(Point(xVaria,yVaria-1)) != null && (board.getCardAtPosition(Point(xVaria,yVaria-1))).getPlayer() == gameState.getCurrentPlayer()) {isAdjacent = true}
          if(board.getCardAtPosition(Point(xVaria-1,yVaria)) != null && (board.getCardAtPosition(Point(xVaria-1,yVaria))).getPlayer() == gameState.getCurrentPlayer()) {isAdjacent = true}

            for(i = 0; i < 4; i++)
            {
              switch(i)
              {
                case 0:
                  var xVariab = xVaria + 1;
                  var yVariab = yVaria;
                  break;
                case 1:
                  var xVariab = xVaria;
                  var yVariab = yVaria + 1;
                  break;
                case 2:
                  var xVariab = xVaria - 1;
                  var yVariab = yVaria;
                  break;
                case 3:
                  var xVariab = xVaria;
                  var yVariab = yVaria - 1;
                  break;
              }
              if(isAdjacent && board.getCardAtPosition(Point(xVariab,yVariab)) != null && (board.getCardAtPosition(Point(xVariab,yVariab))).getPlayer() == gameState.getCurrentPlayer())
              {
                if(boardutils.checkForUnrotatable(board.getCardAtPosition(Point(xVariab,yVariab))) == false)
                {
                  //Set a new variable to avoid an infinite loop
                  var matAva = materialsAvailable.length
                  for(k = 0; k < matAva + 1; k++)
                  {
                    if(materialsAvailable[k] == (board.getCardAtPosition(Point(xVariab,yVariab))).getBackground())
                    {
                      break;
                    }
                    if(k == matAva)
                    {
                      materialsAvailable.push((board.getCardAtPosition(Point(xVariab,yVariab))).getBackground());
                    }
                  }
                }
              }
              if(isAdjacent && board.getCardAtPosition(Point(xVariab,yVariab)) != null && board.getCardAtPosition(Point(xVariab,yVariab)) != gameState.getCurrentPlayer())
              {
                //The ability does not try to affect items on double value and safe tiles
                if(boardutils.checkForUnrotatable(board.getCardAtPosition(Point(xVariab,yVariab))) == false && board.isDoubleTile(xVariab,yVariab) == false)
                {
                  {
                    switch((board.getCardAtPosition(Point(xVariab,yVariab))).getPlayer())
                    {
                      case 1:
                      for(k = 0; k < materialsForbidden1.length; k++)
                      {
                        if(materialsForbidden1[k] == (board.getCardAtPosition(Point(xVariab,yVariab))).getBackground())
                        {
                          break;
                        }
                        if(k == materialsForbidden1.length - 1)
                        {
                          materialsForbidden1.push((board.getCardAtPosition(Point(xVariab,yVariab))).getBackground());
                        }
                      }
                      break;
                      case 2:
                      for(k = 0; k < materialsForbidden2.length; k++)  
                      {
                        if(materialsForbidden2[k] == (board.getCardAtPosition(Point(xVariab,yVariab))).getBackground())
                        {
                          break;
                        }
                        if(k == materialsForbidden2.length - 1)
                        {
                          materialsForbidden2.push((board.getCardAtPosition(Point(xVariab,yVariab))).getBackground());
                        }
                      }
                      break;
                      case 3:
                      for(k = 0; k < materialsForbidden3.length; k++)
                      {
                        if(materialsForbidden3[k] == (board.getCardAtPosition(Point(xVariab,yVariab))).getBackground())
                        {
                          break;
                        }
                        if(k == materialsForbidden3.length - 1)
                        {
                          materialsForbidden3.push((board.getCardAtPosition(Point(xVariab,yVariab))).getBackground());
                        }
                      }
                      break;
                      case 4:
                      for(k = 0; k < materialsForbidden4.length; k++)
                      {
                        if(materialsForbidden4[k] == (board.getCardAtPosition(Point(xVariab,yVariab))).getBackground())
                        {
                          break;
                        }
                        if(k == materialsForbidden4.length - 1)
                        {
                          materialsForbidden4.push((board.getCardAtPosition(Point(xVariab,yVariab))).getBackground());
                        }
                      }
                      break;
                    }
                  }
                }
              }

              if(board.getCardAtPosition(Point(xVariab,yVariab)) != null)
              {
                if((board.getCardAtPosition(Point(xVariab,yVariab))).getPlayer() == 1)
                {
                  if(gameState.getGameMode() != 4)
                  {
                    Array.prototype.push.apply(materialsForbidden, materialsForbidden2);
                  }
                  if(gameState.getGameMode() != 0)
                  {
                    Array.prototype.push.apply(materialsForbidden, materialsForbidden3);
                  }
                }
                if((board.getCardAtPosition(Point(xVariab,yVariab))).getPlayer() == 2)
                {
                  Array.prototype.push.apply(materialsForbidden, materialsForbidden1);
                  if(gameState.getGameMode() > 1)
                  {
                    Array.prototype.push.apply(materialsForbidden, materialsForbidden3);
                  }
                }
                if((board.getCardAtPosition(Point(xVariab,yVariab))).getPlayer() == 3)
                {

                }
                if((board.getCardAtPosition(Point(xVariab,yVariab))).getPlayer() == 4)
                {

                }
              }
            }

            var filteredForbidden = [];
            var materialsDenied = 0;
            function filterSameMaterial(sameMaterial)
            {
              return sameMaterial = materialsForbidden[i];
            }
            for(i = 0; i < materialsForbidden.length; i++)
            {
              filteredForbidden = materialsForbidden.filter(filterSameMaterial)
              if((gameState.getGameMode() == 0 || gameState.getGameMode() == 4) && filteredForbidden.length == 1)
              {
                materialsDenied++;
              }
              if((gameState.getGameMode() == 1 || gameState.getGameMode() == 2) && filteredForbidden.length == 2)
              {
                materialsDenied++;
              }
              if(gameState.getGameMode() == 3 && filteredForbidden.length == 3)
              {
                materialsDenied++;
              }
            }
            if(materialsAvailable.length > materialsDenied)
            {
              result.push(Point(xVaria,yVaria));
            }
          }
        }
      }
      return result;
      break;

    case "operaglove":
      if(gameState.getCurrentPlayer() == 1) {var cardsInCurrentHand = p1Inventory.getAllCards().length;}
      if(gameState.getCurrentPlayer() == 2) {var cardsInCurrentHand = p2Inventory.getAllCards().length;}
      if(gameState.getCurrentPlayer() == 3) {var cardsInCurrentHand = p3Inventory.getAllCards().length;}
      if(gameState.getCurrentPlayer() == 4) {var cardsInCurrentHand = p4Inventory.getAllCards().length;}
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            var dominoCondition = false;
            var entryCondition = false;

            for(ttc = 2; ttc < 10; ttc = ttc+2)
            {
              var xCorri = 0;
              var yCorri = 0;
              if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
              if(ttc == 4 && xVaria > 0) {xCorri = -1;}
              if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
              if(ttc == 8 && yVaria > 0) {yCorri = -1;}
              TTC = Point(xVaria+xCorri,yVaria+yCorri);
              if(board.getCardAtPosition(TTC) != null && (board.getCardAtPosition(TTC)).getPlayer() == gameState.getCurrentPlayer())
              {
                dominoCondition = true;
              }
              if(board.getCardAtPosition(TTC) != null && boardutils.checkForUnpickable(board.getCardAtPosition(TTC)) == false)
              {
                var amountToDiscard = Math.round((board.getCardAtPosition(TTC)).getCurrentValue()/2);
                if(amountToDiscard < 1)
                {
                  amountToDiscard = 1;
                }
                if(amountToDiscard <= cardsInCurrentHand)
                {
                  entryCondition = true;
                }
              }
              if(dominoCondition == true && entryCondition == true)
              {
                result.push(Point(xVaria,yVaria));
              }
            }
          }
        }
      }
      return result;
      break;

    case "football":
      if(boardutils.limitedByPerfume() == false)
      {
        eT = boardutils.getEmptyTiles();
      }
      else
      {
        eT = boardutils.getTilesAbovePerfume();
      }
      if(mainInventory.getAllCards().length > 1)
      {
        result = eT;
      }
      else
      {
        if(boardutils.isHalted(eT) == true)
        {
          for(i = 0; i < eT.length; i++)
          {
            if(board.isDoubleTile(eT[i]))
            {
              result.push(eT[i]);
            }
          }
        }
      }
      break;

    case "adhesivebandage":
      if(trash.getAllCards().length > 0)
      {
        var arrayNumber = 0;
        for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
        {
          for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
          {
            if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
            {
              var patchedTile = 0;
              var entryCondition = false;
              switch(gameState.getCurrentPlayer())
              {
                case 1:
                  if(board.getCardAtPosition(Point(xVaria,yVaria+1)) == null && yVaria < board.getHeight()-1)
                  {
                    patchedTile = Point(xVaria,yVaria+1);
                  }
                  break;
                case 2:
                  if(board.getCardAtPosition(Point(xVaria,yVaria-1)) == null && yVaria > 0)
                  {
                    patchedTile = Point(xVaria,yVaria-1);
                  }
                  break;
                case 3:
                  if(board.getCardAtPosition(Point(xVaria-1,yVaria)) == null && xVaria > 0)
                  {
                    patchedTile = Point(xVaria-1,yVaria);
                  }
                  break;
                case 4:
                  if(board.getCardAtPosition(Point(xVaria+1,yVaria)) == null && xVaria < board.getWidth()-1)
                  {
                    patchedTile = Point(xVaria+1,yVaria);
                  }
                  break;
              }
              if(patchedTile != 0 && board.getCardAtPosition(patchedTile) == null && board.isDisabledTile(patchedTile) == false)
              {
                var trashCards = trash.getAllCards();
                for(i = 0; i < trashCards.length; i++)
                {
                  if(patchutils.secondBandageCondition(trashCards[i], patchedTile.x, patchedTile.y) == true)
                  {
                    entryCondition = true;
                    break;
                  }
                }
              }

              if(entryCondition == true)
              {
                //Checking for domino condition
                for(ttcp = 2; ttcp < 10; ttcp = ttcp+2)
                {
                  var xCorri = 0;
                  var yCorri = 0;
                  if(ttcp == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
                  if(ttcp == 4 && xVaria > 0) {xCorri = -1;}
                  if(ttcp == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
                  if(ttcp == 8 && yVaria > 0) {yCorri = -1;}
                  TTCP = Point(xVaria+xCorri,yVaria+yCorri);
                  CTCP = board.getCardAtPosition(TTCP);
                  if(CTCP != null && CTCP.getPlayer() == gameState.getCurrentPlayer())
                  {
                    result.push(Point(xVaria,yVaria));
                    break;
                  }
                }
              }

            }
          }
        }
      }
      return result;
      break;

    case "broom":
      var eT = boardutils.getEmptyTiles();
      if(boardutils.isHalted(eT) == false)
      {
        if(boardutils.limitedByPerfume() == true)
        {
          eT = boardutils.getTilesAbovePerfume();
        }
        cards = board.getAllCards();
        for(i = 0; i < eT.length; i++)
        {
          var xVaria = eT[i].x;
          var yVaria = eT[i].y;
          var countMovableX = 0
          var countMovableY = 0
          var countImmovableX = 0
          var countImmovableY = 0
          var xDisab = 0
          var yDisab = 0

          for(xVariab = 0; xVariab < board.getWidth(); xVariab++)
          {
            if(board.isDisabledTile(Point(xVariab,yVaria)) == true)
            {
              xDisab++;
            }
          }
          for(yVariab = 0; yVariab < board.getHeight(); yVariab++)
          {
            if(board.isDisabledTile(Point(xVaria,yVariab)) == true)
            {
              yDisab++;
            }
          }

          for(j = 0; j < cards.length; j++)
          {
            if(cards[j].getPosX() == xVaria)
            {
              if(boardutils.checkForImmovable(cards[j]) == true || boardutils.isUntargetableByBronze(cards[j]) == true)
              {
                countImmovableX++;
              }
              else
              {
                countMovableX++;
              }
            }
            if(cards[j].getPosY() == yVaria)
            {
              if(boardutils.checkForImmovable(cards[j]) == true || boardutils.isUntargetableByBronze(cards[j]) == true)
              {
                countImmovableY++;
              }
              else
              {
                countMovableY++;
              }
            }
          }

          if(countMovableX + countImmovableX + countMovableY + countImmovableY < (board.getWidth() + board.getHeight() - xDisab - yDisab - 2) && countMovableX + countMovableY > 0)
          {
            if((countMovableX == 0 && countMovableY + countImmovableY == board.getWidth() - xDisab - 1) || (countMovableY == 0 && countMovableX + countImmovableX == board.getHeight() - yDisab - 1))
            {
            }
            else
            {
              result.push(eT[i]);
              break;
            }
          }
        }
      }
      else
      {
        if(boardutils.limitedByPerfume() == false)
        {
          result = boardutils.getEmptyTiles();
        }
        else
        {
          result = boardutils.getTilesAbovePerfume();
        }
      }
      return result;
      break;

    case "fryingpan":
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            var xCorri = 0;
            var yCorri = 0;
            if(gameState.getCurrentPlayer() == 1 && yVaria < board.getHeight() - 1) {yCorri = 1;}
            if(gameState.getCurrentPlayer() == 2 && yVaria > 0) {yCorri = -1;}
            if(gameState.getCurrentPlayer() == 3 && xVaria > 0) {xCorri = -1;}
            if(gameState.getCurrentPlayer() == 4 && xVaria < board.getWidth() - 1) {xCorri = 1;}
            CTC = board.getCardAtPosition(Point(xVaria+xCorri,yVaria+yCorri));
            if(CTC == null)
            {
              result.push(Point(xVaria,yVaria));
            }
            if(CTC != null && (xCorri != 0 || yCorri != 0))
            {
              if(boardutils.checkForUntrashable(CTC) == false || CTC.getCurrentValue() >= 7)
              {
                result.push(Point(xVaria,yVaria));
              }
            }
          }
        }
      }
      return result;
      break;

    case "souvenir":
      var sumCardsInHands = 0;
      switch(gameState.getGameMode())
      {
        case 0:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length);
          break;
        case 1:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length + p4Inventory.getAllCards().length);
          break;
        case 2:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length);
          break;
        case 3:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length + p4Inventory.getAllCards().length);
          break;
        case 4:
          sumCardsInHands = (p1Inventory.getAllCards().length + p3Inventory.getAllCards().length);
          break;
      }
      switch(gameState.getCurrentPlayer())
      {
        case 1:
          sumCardsInHands = sumCardsInHands - p1Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p4Inventory.getAllCards().length;}
          break;
        case 2:
          sumCardsInHands = sumCardsInHands - p2Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p3Inventory.getAllCards().length;}
          break;
        case 3:
          sumCardsInHands = sumCardsInHands - p3Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p2Inventory.getAllCards().length;}
          break;
        case 4:
          sumCardsInHands = sumCardsInHands - p4Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p1Inventory.getAllCards().length;}
          break;
      }

      var eT = boardutils.getEmptyTiles();
      //If there is at least one card in the hand of one opponent
      if(sumCardsInHands > 0 && eT.length > 0)
      {
        if(boardutils.limitedByPerfume() == true)
        {
          eT = boardutils.getTilesAbovePerfume();
        }

        for(i = 0; i < eT.length; i++)
        {
          var xVaria = eT[i].x;
          var yVaria = eT[i].y;

          cardsCFUR = board.getAllCards();
          var instantlyRotatable = true;
          for(cfur = 0; cfur < cardsCFUR.length; cfur++)
          {
            if(board.isSafeTile(Point(xVaria,yVaria)) == true)
            {
              instantlyRotatable = false;
              break;
            }

            if((cardsCFUR[cfur].getID() == "celticshield" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "celticshield")) && cardsCFUR[cfur].isGrayScale() == false && cardsCFUR[cfur].getPlayer() == card.getPlayer())
            {
              var shieldDistance = Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2)
              if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
              {
                instantlyRotatable = false;
                break;
              }
            }

            if(cardsCFUR[cfur].getID() == "padlock" && cardsCFUR[cfur].isGrayScale() == false && Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2) == 1)
            {
              instantlyRotatable = false;
              break;
            }

            if((cardsCFUR[cfur].getID() == "hardhat" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "hardhat")) && cardsCFUR[cfur].isGrayScale() == false)
            {
              switch(cardsCFUR[cfur].getPlayer())
              {
                case 1:
                  if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria-1) {instantlyRotatable = false;}
                  break;
                case 2:
                  if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria+1) {instantlyRotatable = false;}
                  break;
                case 3:
                  if(cardsCFUR[cfur].getPosX() == xVaria+1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;}
                  break;
                case 4:
                  if(cardsCFUR[cfur].getPosX() == xVaria-1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;}
                  break;
              }
            }
          }

          if(instantlyRotatable == true)
          {
            result.push(eT[i]);
          }

        }
      }
      break;

    default:
      if(choiceForWatch.hasIcon("dmno"))
      {
        for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
        {
          for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
          {
            if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
            {
              for(ttc = 2; ttc < 10; ttc = ttc+2)
              {
                var xCorri = 0;
                var yCorri = 0;
                if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
                if(ttc == 4 && xVaria > 0) {xCorri = -1;}
                if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
                if(ttc == 8 && yVaria > 0) {yCorri = -1;}
                TTC = Point(xVaria+xCorri,yVaria+yCorri);
                CTC = board.getCardAtPosition(TTC);
                if(CTC != null && CTC.getPlayer() == gameState.getCurrentPlayer())
                {
                  result.push(Point(xVaria,yVaria));
                  break;
                }
              }
            }
          }
        }
      }
      else
      {
        if(boardutils.limitedByPerfume() == false)
        {
          result = boardutils.getEmptyTiles();
        }
        else
        {
          result = boardutils.getTilesAbovePerfume();
        }
      }

  }
  return result;
}
