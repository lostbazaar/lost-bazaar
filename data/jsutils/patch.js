//@include jsutils.board
//@include jsutils.point


var patchutils = new Object();


patchutils.secondBandageCondition = function(choiceForBandage, xVaria, yVaria)
{
  switch(choiceForBandage.getID())
  {

    case "dart":
      var result = false;
      cards = board.getAllCards();
      var entryCondition = false;
      var instantlyTrashable = true;
      var instantlyMovable = true;

      //Determine the position of the Adhesive Plaster itself
      switch(gameState.getCurrentPlayer())
      {
        case 1:
          var xBandage = xVaria;
          var yBandage = yVaria - 1;
          break;
        case 2:
          var xBandage = xVaria;
          var yBandage = yVaria + 1;
          break;
        case 3:
          var xBandage = xVaria + 1;
          var yBandage = yVaria;
          break;
        case 4:
          var xBandage = xVaria - 1;
          var yBandage = yVaria;
          break;
      }

      cardsCF = board.getAllCards();
      for(cfut = 0; cfut < cardsCF.length; cfut++)
      {
        if(board.isSafeTile(Point(xVaria,yVaria)) == true)
        {
          instantlyTrashable = false;
          break;
        }

        if((cardsCF[cfut].getID() == "celticshield" || (cardsCF[cfut].getID() == "handmirror" && boardutils.itemMirrored(cardsCF[cfut]) == "celticshield")) && cardsCF[cfut].isGrayScale() == false && cardsCF[cfut].getPlayer() == gameState.getCurrentPlayer())
        {
          var shieldDistance = Math.pow(cardsCF[cfut].getPosX()-xVaria, 2) + Math.pow(cardsCF[cfut].getPosY()-yVaria, 2)
          if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
          {
            instantlyTrashable = false;
            break;
          }
        }

        if((cardsCF[cfut].getID() == "hardhat" || (cardsCF[cfut].getID() == "handmirror" && boardutils.itemMirrored(cardsCF[cfut]) == "hardhat")) && cardsCF[cfut].isGrayScale() == false)
        {
          switch(cardsCF[cfut].getPlayer())
          {
            case 1:
              if(cardsCF[cfut].getPosX() == xVaria && cardsCF[cfut].getPosY() == yVaria-1) {instantlyTrashable = false;}
              break;
            case 2:
              if(cardsCF[cfut].getPosX() == xVaria && cardsCF[cfut].getPosY() == yVaria+1) {instantlyTrashable = false;}
              break;
            case 3:
              if(cardsCF[cfut].getPosX() == xVaria+1 && cardsCF[cfut].getPosY() == yVaria) {instantlyTrashable = false;}
              break;
            case 4:
              if(cardsCF[cfut].getPosX() == xVaria-1 && cardsCF[cfut].getPosY() == yVaria) {instantlyTrashable = false;}
              break;
          }
        }
      }
      if(board.isInnovativeTile(Point(xVaria,yVaria)) == false)
      {
        for(cfim = 0; cfim < cardsCF.length; cfim++)
        {
          if((cardsCF[cfim].getID() == "gluetube" || (cardsCF[cfim].getID() == "handmirror" && boardutils.itemMirrored(cardsCF[cfim]) == "gluetube")) && cardsCF[cfim].isGrayScale() == false)
          {
            var tubeDistance = Math.pow(cardsCF[cfim].getPosX()-xVaria, 2) + Math.pow(cardsCF[cfim].getPosY()-yVaria, 2)
            if(tubeDistance == 0 || tubeDistance == 1 || tubeDistance == 2)
            {
              instantlyMovable = false;
              break;
            }
          }
        }
      }
      else
      {
        instantlyMovable = false;
      }

      if(instantlyTrashable == true)
      {
        for(xVariab = 0; xVariab < board.getWidth(); xVariab++)
        {
          if(xVariab == xVaria)
          {
            xVariab++;
          }
          //If the target tile is not safe
          var CTC = board.getCardAtPosition(Point(xVariab,yVaria));
          if(CTC != null && boardutils.checkForUntrashable(CTC) == false && instantlyMovable == true)
          {
            result = true;
            break;
          }
          if(CTC != null && boardutils.checkForUntrashable(CTC) == true && CTC.getCurrentValue() > 3)
          {
            result = true;
            break;
          }
          if(CTC != null && instantlyMovable == false && CTC.getCurrentValue() > 3)
          {
            result = true;
            break;
          }
        }

        for(yVariab = 0; yVariab < board.getHeight(); yVariab++)
        {
          if(yVariab == yVaria)
          {
            yVariab++;
          }
          var CTC = board.getCardAtPosition(Point(xVaria,yVariab));
          if(CTC != null && boardutils.checkForUntrashable(CTC) == false && instantlyMovable == true)
          {
            result = true;
            break;
          }
          if(CTC != null && boardutils.checkForUntrashable(CTC) == true && CTC.getCurrentValue() > 3)
          {
            result = true;
            break;
          }
          if(CTC != null && instantlyMovable == false && CTC.getCurrentValue() > 3)
          {
            result = true;
            break;
          }
        }

        if(boardutils.doesTilePreventDamaging(xBandage, yBandage) == false && instantlyMovable == true)
        {
          result = true;
        }
      }

      if(instantlyTrashable == false && instantlyMovable == true)
      {
        //If the tile is safe, the only allowed targets are the ones with 100% likeliness to be hit
        for(xVariab = 0; xVariab < board.getWidth(); xVariab++)
        {
          var CTC = board.getCardAtPosition(Point(xVariab,yVaria));
          if(CTC != null && boardutils.checkForUntrashable(CTC) == false && ((CTC.getCurrentValue() <= 0 && boardutils.isSixFix(gameState.getCurrentPlayer()) == false) || (CTC.getCurrentValue() <= 3 && boardutils.isSixFix(gameState.getCurrentPlayer()) == true)))
          {
            result = true;
          }

          for(yVariab = 0; yVariab < board.getHeight(); yVariab++)
          {
            var CTC = board.getCardAtPosition(Point(xVaria,yVariab));
            if(CTC != null && boardutils.checkForUntrashable(CTC) == false && ((CTC.getCurrentValue() <= 0 && boardutils.isSixFix(gameState.getCurrentPlayer()) == false) || (CTC.getCurrentValue() <= 3 && boardutils.isSixFix(gameState.getCurrentPlayer()) == true)))
            {
              result = true;
            }
          }
        }
        if(boardutils.doesTilePreventDamaging(xBandage, yBandage) == false && boardutils.isSixFix(gameState.getCurrentPlayer()) == true)
        {
          result = true;
        }
      }
      return result;
      break;

    case "documentholder":
      if(mainInventory.getAllCards().length > 0)
      {
        return true;
      }
      else
      {
        return false;
      }
      break;

    case "gardenspade":
      //In the extremely unlikely case neither the trash (aside this Garden Spade) nor the storage contains items
      if(2 > (trash.getAllCards()).length && 1 > (mainInventory.getAllCards()).length)
      {
        return false;
      }
      else
      {
        return true;
      }
      break;

    case "compasses":
      var result = false;
      for(ttc = 2; ttc < 10; ttc = ttc+2)
      {
        var xCorri = 0;
        var yCorri = 0;
        if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
        if(ttc == 4 && xVaria > 0) {xCorri = -1;}
        if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
        if(ttc == 8 && yVaria > 0) {yCorri = -1;}
        CTC = board.getCardAtPosition(Point(xVaria+xCorri,yVaria+yCorri));
        if(CTC != null && CTC.getPlayer() != gameState.getCurrentPlayer() && boardutils.checkForUnrotatable(CTC) == false && CTC.getBackground() == "wood" && CTC.getCurrentValue() <= 4)
        {
          result = true;
          break;
        }
      }
      return result;
      break;

    case "flyswatter":
      var result = false;
      for(ctc = 1; ctc < 10; ctc++)
      {
        var xCorri = 0;
        var yCorri = 0;
        if(ctc == 5) {ctc++;}

        if(ctc == 1 && xVaria > 0 && yVaria < board.getHeight() - 1) {xCorri = -1; yCorri = 1;}
        if(ctc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
        if(ctc == 3 && xVaria < board.getWidth() - 1 && yVaria < board.getHeight() - 1) {xCorri = 1; yCorri = 1;}
        if(ctc == 4 && xVaria > 0) {xCorri = -1;}
        if(ctc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
        if(ctc == 7 && xVaria > 0 && yVaria > 0) {xCorri = -1; yCorri = -1;}
        if(ctc == 8 && yVaria > 0) {yCorri = -1;}
        if(ctc == 9 && xVaria < board.getWidth() - 1 && yVaria > 0) {xCorri = 1; yCorri = -1;}

        CTC = board.getCardAtPosition(Point(xVaria+xCorri,yVaria+yCorri));
        if(CTC != null && CTC.getBaseValue() < 2 && boardutils.checkForUntrashable(CTC) == false)
        {
          result = true;
          break;
        }
      }
      return result;
      break;

    case "souvenir":
      var sumCardsInHands = 0;
      switch(gameState.getGameMode())
      {
        case 0:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length);
          break;
        case 1:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length + p4Inventory.getAllCards().length);
          break;
        case 2:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length);
          break;
        case 3:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length + p4Inventory.getAllCards().length);
          break;
        case 4:
          sumCardsInHands = (p1Inventory.getAllCards().length + p3Inventory.getAllCards().length);
          break;
      }
      switch(gameState.getCurrentPlayer())
      {
        case 1:
          sumCardsInHands = sumCardsInHands - p1Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p4Inventory.getAllCards().length;}
          break;
        case 2:
          sumCardsInHands = sumCardsInHands - p2Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p3Inventory.getAllCards().length;}
          break;
        case 3:
          sumCardsInHands = sumCardsInHands - p3Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p2Inventory.getAllCards().length;}
          break;
        case 4:
          sumCardsInHands = sumCardsInHands - p4Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p1Inventory.getAllCards().length;}
          break;
      }
      //If there is at least one card in the hand of one opponent
      if(sumCardsInHands > 0)
      {
        if(board.isSafeTile(Point(xVaria,yVaria)) == false)
        {
          cardsCFUR = board.getAllCards();
          var instantlyRotatable = true;
          for(cfur = 0; cfur < cardsCFUR.length; cfur++)
          {

            if((cardsCFUR[cfur].getID() == "celticshield" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "celticshield")) && cardsCFUR[cfur].isGrayScale() == false && cardsCFUR[cfur].getPlayer() == gameState.getCurrentPlayer())
            {
              var shieldDistance = Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2)
              if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
              {
                instantlyRotatable = false;
                break;
              }
            }

            if(cardsCFUR[cfur].getID() == "padlock" && cardsCFUR[cfur].isGrayScale() == false && Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2) == 1)
            {
              instantlyRotatable = false;
              break;
            }

            if((cardsCFUR[cfur].getID() == "hardhat" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "hardhat")) && cardsCFUR[cfur].isGrayScale() == false)
            {
              switch(cardsCFUR[cfur].getPlayer())
              {
                case 1: if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria-1) {instantlyRotatable = false;} break;
                case 2: if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria+1) {instantlyRotatable = false;} break;
                case 3: if(cardsCFUR[cfur].getPosX() == xVaria+1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;} break;
                case 4: if(cardsCFUR[cfur].getPosX() == xVaria-1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;} break;
              }
            }

          }
          if(instantlyRotatable == true)
          {
            return true;
            break;
          }
        }
        else
        {
          instantlyRotatable = false;
        }
      }
      if(instantlyRotatable == false)
      {
        return false;
      }
      break;

    case "whiteknight":
      if(mainInventory.getAllCards().length > 0)
      {
        return true;
      }
      else
      {
        return false;
      }
      break;

    default:
      if(choiceForBandage.hasIcon("tcan") == true || choiceForBandage.getCurrentValue() > 1)
      {
        return false;
      }
      else
      {
        return true;
      }
  }
}
