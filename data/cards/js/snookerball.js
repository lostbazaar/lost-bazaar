//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "turnStarted")
  {
    card.setShowFullSize(1);
  }

  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer() && card.isGrayScale() == false)
  {
    if(card.getCounter() < 12)
    {
      var userNames = gameState.getPlayerNickNames();
      if(boardutils.isSixFix(gameState.getCurrentPlayer()) == true)
      {
        var diceRoll = 6;
      }
      else
      {
        var diceRoll = rng.randomBetween(1, 7);
      }
      card.setCounter(card.getCounter()+diceRoll);
      menu.showInformation(qsTranslate("snookerball", "%1%2 %3: %4").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()).arg(boardutils.numberToFace(diceRoll)), 4000);
      //Log
      gameState.writeToGameLog(qsTranslate("snookerball", "<font color=\"khaki\">%1 rolled a die: %2.</font>").arg(userNames[gameState.getCurrentPlayer()-1]).arg(diceRoll), [1, 2, 3, 4], 1, 0);
      if(diceRoll == 1)
      {
        gameState.writeToGameLog(qsTranslate("snookerball", "<font color=\"khaki\">%1%2 %3 gained 1 counter.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()), [1, 2, 3, 4], 1, 0);
      }
      else
      {
        gameState.writeToGameLog(qsTranslate("snookerball", "<font color=\"khaki\">%1%2 %3 gained %4 counters.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()).arg(diceRoll), [1, 2, 3, 4], 1, 0);
      }
    }
    //It gets capped at 12
    if(card.getCounter() > 11)
    {
      card.setCounter(12);
    }
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    cards = board.getAllCards(); //Retrieve all cards on the board
    cardsOK = []; //This will store the cards which can be moved
    for(i = 0; i < cards.length; i++) //Check all cards
    {
      //If the card is adjacent to the Snooker ball
      if(Math.pow(card.getPosX()-cards[i].getPosX(), 2) + Math.pow(card.getPosY()-cards[i].getPosY(), 2) == 1)
      {
        //If the card is not immovable then it might be good
        if(boardutils.checkForImmovable(cards[i]) == false)
        {
          //If there is an empty tile adjacent to the card then it might be good
          if(boardutils.getAdjacentEmptyTiles(cards[i].getPosition()).length > 0)
          {
            //If the value of the card is no greater than the number of counters on the Snooker ball, but greater or equal to zero
            if(card.getCounter() >= cards[i].getCurrentValue() && 0 <= cards[i].getCurrentValue())
            {
              //If the card is targetable by active abilities
              if(boardutils.isUntargetableBySilver(cards[i]) == false)
              {
                //Store the coordinates of the good card
                cardsOK.push(cards[i].getPosition());
              }
            }
          }
        }
      }
    }
    if(cardsOK.length > 0 || (card.getCounter() > 4 && boardutils.getAdjacentEmptyTiles(card.getPosition()).length > 0 &&  boardutils.checkForImmovable(card) == false))
    {
      //Ask the player to choose the ability
      menu.showInformation(qsTranslate("snookerball", "Choose from the abilities of the %1").arg(card.getName()), -1);
      //Stores the text of the choices
      choiceArray = [
      qsTranslate("snookerball", "Move an adjacent item"),
      qsTranslate("snookerball", "Move itself")
      ];
      //Stores which choices are enabled and which aren't
      if(cardsOK.length > 0 && card.getCounter() > 4 && boardutils.getAdjacentEmptyTiles(card.getPosition()).length > 0 && boardutils.checkForImmovable(card) == false)
      {
        enableArray = ["true", "true"];
      }
      if(cardsOK.length == 0 && card.getCounter() > 4 && boardutils.getAdjacentEmptyTiles(card.getPosition()).length > 0 &&  boardutils.checkForImmovable(card) == false)
      {
        enableArray = ["false", "true"];
      }
      if(cardsOK.length > 0 && (card.getCounter() < 5 || boardutils.getAdjacentEmptyTiles(card.getPosition()).length == 0 || boardutils.checkForImmovable(card) == true))
      {
        enableArray = ["true", "false"];
      }
      //Stores the tooltips for the choices
      toolTipArray = ["", ""];

      //Get the player's choice
      var choice = menu.getChoice(choiceArray, enableArray, toolTipArray);
      //Hide the label
      menu.hideInformation();

      if(choice == 0)
      {
        //Ask the player which card would he like to move
        menu.showInformation(qsTranslate("snookerball", "Choose which item would you like to move"), -1);
        //The player chooses one from the good cards
        selectedTile = boardutils.tileChoice(cardsOK, 0);
        selectedCard = board.getCardAtPosition(selectedTile);
        //Hide the 'Choose which card...' label
        menu.hideInformation();
        //Now the player must choose the new place for the card
        menu.showInformation(qsTranslate("snookerball", "Choose where would you like to move the selected item"), -1);
        //Only the empty tiles adjacent to the chosen card are available now
        selectedPos = boardutils.tileChoice(boardutils.getAdjacentEmptyTiles(selectedTile), 0);
        //We got the choice, hide the info
        menu.hideInformation();
        //Take down the required amount of counters
        var counterCost = selectedCard.getCurrentValue();
        card.setCounter(card.getCounter() - counterCost);
        //Move the card
        board.moveCard(selectedTile, selectedPos);
        //Log
        boardutils.logSilverActivation(card.getName());
        if(counterCost < 2)
        {
          gameState.writeToGameLog(qsTranslate("snookerball", "<font color=\"paleturquoise\">The %1 cast %2 counter.</font>").arg(card.getName()).arg(counterCost), [1, 2, 3, 4], 1, 0);
        }
        else
        {
          gameState.writeToGameLog(qsTranslate("snookerball", "<font color=\"paleturquoise\">The %1 cast %2 counters.</font>").arg(card.getName()).arg(counterCost), [1, 2, 3, 4], 1, 0);
        }
        gameState.writeToGameLog(qsTranslate("snookerball", "<font color=\"paleturquoise\">The %1 moved %2%3 %4 to %5%6.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()).arg(boardutils.convertToABC(selectedPos.x)).arg(selectedPos.y+1), [1, 2, 3, 4], 1, 0);

        //Crop or grayscale
        if(card.isFullSize() == true)
        {
          card.setShowFullSize(0)
        }
        else
        {
          card.setGrayScale(1);
          //Log
          gameState.writeToGameLog(qsTranslate("snookerball", "The %1 has been disabled.").arg(card.getName()), [1, 2, 3, 4], 1, 0);
        }
      }

      if(choice == 1)
      {
        //Ask the player where to move the ball
        menu.showInformation(qsTranslate("snookerball", "Choose where to move the %1").arg(card.getName()), -1);
        //The player chooses one from the good positions
        selectedPos = boardutils.tileChoice(boardutils.getAdjacentEmptyTiles(card.getPosition()), 0);
        //We got the choice, hide the info
        menu.hideInformation();
        //Take down the 5 counters
        card.setCounter(card.getCounter()-5);
        //Log
        boardutils.logSilverActivation(card.getName());
        //Move the card
        board.moveCard(card.getPosition(), selectedPos);
        //Log
        gameState.writeToGameLog(qsTranslate("snookerball", "<font color=\"paleturquoise\">The %1 cast 5 counters.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
        gameState.writeToGameLog(qsTranslate("snookerball", "<font color=\"paleturquoise\">The %1 has been moved to %2%3.</font>").arg(card.getName()).arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1), [1, 2, 3, 4], 1, 0);

        //Crop or grayscale
        if(card.isFullSize() == true)
        {
          card.setShowFullSize(0);
        }
        else
        {
          card.setGrayScale(1);
          //Log
          gameState.writeToGameLog(qsTranslate("snookerball", "The %1 has been disabled.").arg(card.getName()), [1, 2, 3, 4], 1, 0);
        }
      }
    }
    else
    {
      menu.showInformation(qsTranslate("snookerball", "You cannot use this ability!"));
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
