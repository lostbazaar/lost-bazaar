//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions()
}

//This function handles all events
function event(eventData)
{
  //Handle the cardPlacedOnBoard event
  //Check if this card is the one that was placed
  //If it is, then execute the bronze action
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();

    //There are three possible choiches:
    //View the inventory of all players
    //Move another player's card with a max value of 3 to an adjacent tile
    //Draw a card
    //The last two choices are not always available

    //Check if the first choice is available

    viewablePlayers = [] //This will store the possible players for the first choice

    //Check which inventories can be made visible
    if(!p1Inventory.isViewable()) {viewablePlayers.push(1);}
    if(gameState.getGameMode() != 4 && !p2Inventory.isViewable()) {viewablePlayers.push(2);}
    if(gameState.getGameMode() >= 1 && !p3Inventory.isViewable()) {viewablePlayers.push(3);}
    if((gameState.getGameMode() == 1 || gameState.getGameMode() == 3) && !p4Inventory.isViewable()) {viewablePlayers.push(4);}

    cards = board.getAllCards(); //Retrieve all cards on the board
    cardsOK = []; //This will store the cards which can be moved
    for(i = 0; i < cards.length; i++) //Check all cards
    {
      //If the card's owner is not the current player and the card's current value is <= 3 then it might be good
      if(cards[i].getPlayer() != gameState.getCurrentPlayer() && cards[i].getCurrentValue() <= 3)
      {
        //If the card is not immovable then it might be good
        if(boardutils.checkForImmovable(cards[i]) == false)
        {
          //If there is an empty tile adjacent to the card then it might be good
          if(boardutils.getAdjacentEmptyTiles(cards[i].getPosition()).length > 0)
          {
            //If the card is targetable by entry abilities then it might be good
            if(boardutils.isUntargetableByBronze(cards[i]) == false)
            {
              //Store the coordinates of the good card
              cardsOK.push(cards[i].getPosition());
            }
          }
        }
      }
    }

    var cancelToTop;
    do
    {
      cancelToTop = 0;
      //Display title
      menu.showInformation(qsTranslate("lacefan", "Choose from the abilities of the %1").arg(card.getName()), -1);
      //Stores the text of the choices
      choiceArray = [
      qsTranslate("lacefan", "View the hand of all players"),
      qsTranslate("lacefan", "Move another player's item of 3 or less value to an adjacent tile"),
      qsTranslate("lacefan", "Draw an item")
      ];
      //Stores which choices are enabled and which aren't
      enableArray = ["true", cardsOK.length > 0 ? "true" : "false", mainInventory.isEmpty() ? "false" :"true"];
      //Stores the tooltips for the choices
      toolTipArray = [viewablePlayers.length > 0 ? "" : qsTranslate("lacefan", "All opponents' hand are already visible"),
      cardsOK.length > 0 ? "" : qsTranslate("lacefan", "There are no items on the board that satisfy the conditions"), ""];

      //Get the player's choice
      var choice = menu.getChoice(choiceArray, enableArray, toolTipArray);
      //Hide the 'Which player's inventory...' label
      menu.hideInformation();
      
      //If the player chose to move a card
      if(choice == 1)
      {
        //Ask the player which card would he like to move
        menu.showInformation(qsTranslate("lacefan", "Choose which item would you like to move"), -1);
        //The player chooses one from the good cards
        selectedTile = boardutils.tileChoice(cardsOK, 1);
        //Hide the 'Choose which card...' label
        menu.hideInformation();
        //Unless canceled
        if(selectedTile.x != -1)
        {
          //Now the player must choose the new place for the card
          menu.showInformation(qsTranslate("lacefan", "Choose where would you like to move the selected item"), -1);
          //Only the empty tiles adjacent to the chosen card are available now
          selectedPos = boardutils.tileChoice(boardutils.getAdjacentEmptyTiles(selectedTile), 1);
          //We got the choice, hide the info
          menu.hideInformation();
          //Unless canceled
          if(selectedPos.x != -1)
          {
            //Move the card
            board.moveCard(selectedTile, selectedPos);
          }
          else
          {
            cancelToTop = 1;
          }
        }
        else
        {
          cancelToTop = 1;
        }
      }
    }
    while(cancelToTop == 1);

    //If the player chose to view the inventory of another player
    if(choice == 0)
    {
       if(viewablePlayers.length > 0)
       {
         for(i = 0; i < 5; i++)
         {
           var revealedPlayer = viewablePlayers[i]
           switch(revealedPlayer)
           {
             case 1:
               p1Inventory.setViewable(true, true);
               break;
             case 2:
               p2Inventory.setViewable(true, true);
               break;
             case 3:
               p3Inventory.setViewable(true, true);
               break;
             case 4:
               p4Inventory.setViewable(true, true);
               break;
           }
         }
       }
    }
    //If the player chose to draw a card
    if(choice == 2)
    {
      //Logging of placement has to be made prior the card draw
      boardutils.logCardPlacement(card.getName());
      //A card is drawn for the player
      gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "bisque");
      //An informational message is displayed (it will disappear automatically)
      menu.showInformation(qsTranslate("lacefan", "You have drawn an item!"));
    }

    //Log
    switch(choice)
    {
      case 0:
        boardutils.logCardPlacement(card.getName());
        gameState.writeToGameLog(qsTranslate("lacefan", "<font color=\"bisque\">The %1 revealed the hands of all players.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
        break;
      case 1:
        boardutils.logCardPlacement(card.getName());
        gameState.writeToGameLog(qsTranslate("lacefan", "<font color=\"bisque\">The %1 moved %2%3 %4 to %5%6.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg((board.getCardAtPosition(selectedPos)).getName()).arg(boardutils.convertToABC(selectedPos.x)).arg(selectedPos.y+1), [1, 2, 3, 4], 1, 0);
        break;
    }
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
