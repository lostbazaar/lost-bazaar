//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    if(boardutils.limitedByPerfume() == false)
    {
      eT = boardutils.getEmptyTiles();
    }
    else
    {
      eT = boardutils.getTilesAbovePerfume();
    }

    for(i = 0; i < eT.length; i++)
    {
      for(ctc = 2; ctc < 10; ctc = ctc+2)
      {
        var xCorri = 0;
        var yCorri = 0;
        if(ctc == 2 && eT[i].y < board.getHeight() - 1) {yCorri = 1;}
        if(ctc == 4 && eT[i].x > 0) {xCorri = -1;}
        if(ctc == 6 && eT[i].x < board.getWidth() - 1) {xCorri = 1;}
        if(ctc == 8 && eT[i].y > 0) {yCorri = -1;}
        CTC = board.getCardAtPosition(Point(eT[i].x+xCorri,eT[i].y+yCorri));
        if(CTC != null && CTC != card && CTC.isGrayScale() == false && CTC.getCurrentValue() <= 5 && boardutils.isUntargetableByBronze(CTC) == false)
        {
          array.push(eT[i]);
          break;
        }
      }
    }
  }
  return array;
}


function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    tilesOK = [];
    for(ctc = 2; ctc < 10; ctc = ctc+2)
    {
      var xCorri = 0;
      var yCorri = 0;
      if(ctc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
      if(ctc == 4 && card.getPosX() > 0) {xCorri = -1;}
      if(ctc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
      if(ctc == 8 && card.getPosY() > 0) {yCorri = -1;}
      TTC = Point(card.getPosX()+xCorri,card.getPosY()+yCorri);
      CTC = board.getCardAtPosition(TTC);
      if(CTC != null && CTC.getUID() != card.getUID() && CTC.isGrayScale() == false && CTC.getCurrentValue() <= 5 && boardutils.isUntargetableByBronze(CTC) == false)
      {
        tilesOK.push(TTC);
      }
    }
    menu.showInformation(qsTranslate("baseballbat", "Choose target"), -1);
    selectedTile = boardutils.tileChoice(tilesOK, 0);
    menu.hideInformation();
    selectedCard = board.getCardAtPosition(selectedTile);

    //Log
    boardutils.logCardPlacement(card.getName());

    selectedCard.setGrayScale(1);

    //Log
    gameState.writeToGameLog(qsTranslate("baseballbat", "<font color=\"bisque\">The %1 disabled %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);

    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
