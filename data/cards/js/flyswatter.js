//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums
//@include jsutils.point

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    if(boardutils.limitedByPerfume() == false)
    {
      eT = boardutils.getEmptyTiles();
    }
    else
    {
      eT = boardutils.getTilesAbovePerfume();
    }

    for(i = 0; i < eT.length; i++)
    {
      for(ctc = 1; ctc < 10; ctc++)
      {
        var xCorri = 0;
        var yCorri = 0;
        if(ctc == 5) {ctc++;}

        if(ctc == 1 && eT[i].x > 0 && eT[i].y < board.getHeight() - 1) {xCorri = -1; yCorri = 1;}
        if(ctc == 2 && eT[i].y < board.getHeight() - 1) {yCorri = 1;}
        if(ctc == 3 && eT[i].x < board.getWidth() - 1 && eT[i].y < board.getHeight() - 1) {xCorri = 1; yCorri = 1;}
        if(ctc == 4 && eT[i].x > 0) {xCorri = -1;}
        if(ctc == 6 && eT[i].x < board.getWidth() - 1) {xCorri = 1;}
        if(ctc == 7 && eT[i].x > 0 && eT[i].y > 0) {xCorri = -1; yCorri = -1;}
        if(ctc == 8 && eT[i].y > 0) {yCorri = -1;}
        if(ctc == 9 && eT[i].x < board.getWidth() - 1 && eT[i].y > 0) {xCorri = 1; yCorri = -1;}

        CTC = board.getCardAtPosition(Point((eT[i].x)+xCorri,(eT[i].y)+yCorri));
        if(CTC != null && CTC.getBaseValue() < 2 && boardutils.checkForUntrashable(CTC) == false)
        {
          array.push(eT[i]);
          break;
        }
      }
    }
  }
  return array;
}

//This function handles all events
function event(eventData)
{
  //Handle the cardPlacedOnBoard event
  //Check if this card is the one that was placed
  //If it is, then execute the bronze action
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    cards = board.getAllCards(); //Retrieve all cards on the board
    tilesOK = []; //This will store the tiles of the good cards
    for(i = 0; i < cards.length; i++) //Check all cards
    {
      //If the card's base value is <= 1 and not on a safe tile then it might be good
      if(cards[i].getBaseValue() <= 1 && boardutils.checkForUntrashable(cards[i]) == false)
      {
        //If the card is targetable by entry abilities then it might be good
        if(boardutils.isUntargetableByBronze(cards[i]) == false)
        {
          //If the card is a king move away then it might be good
          if(card.getPosX()-1 <= cards[i].getPosX() && cards[i].getPosX() <= card.getPosX()+1 && card.getPosY()-1 <= cards[i].getPosY() && cards[i].getPosY() <= card.getPosY()+1)
          {
            //If the card is not the flyswatter itself then it is good
            if(card.getPosX() == cards[i].getPosX() && card.getPosY() == cards[i].getPosY())
            {
            }
            else
            {
              //Store the tile of the good card
              tilesOK.push(Point(cards[i].getPosX(),cards[i].getPosY()));
            }
          }
        }
      }
    }

    //Ask the player which card would he like to trash
    menu.showInformation(qsTranslate("flyswatter", "Choose target"), -1);
    //The player chooses one from the good cards
    selectedTile = boardutils.tileChoice(tilesOK, 0);
    //Hide the 'Choose which card...' label
    menu.hideInformation();
    selectedCard = board.getCardAtPosition(selectedTile);

    //Sound effect
    var randomSound = rng.randomBetween(1, 7);
    switch(randomSound)
    {
      case 1: gameState.playSoundEffect("swat-1.wav", 1); break;
      case 2: gameState.playSoundEffect("swat-2.wav", 1); break;
      case 3: gameState.playSoundEffect("swat-3.wav", 1); break;
      case 4: gameState.playSoundEffect("swat-4.wav", 1); break;
      case 5: gameState.playSoundEffect("swat-5.wav", 1); break;
      case 6: gameState.playSoundEffect("swat-6.wav", 1); break;
    }

    //Log
    boardutils.logCardPlacement(card.getName());
    gameState.writeToGameLog(qsTranslate("flyswatter", "<font color=\"bisque\">The %1 destroyed %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);

    //The Flyswatter cannot hit the Bomb, Piggy Bank or Hand Mirror - no further events necessary
    board.moveCardToTrash(selectedCard.getUID());

    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
