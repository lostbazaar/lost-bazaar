//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "turnStarted")
  {
    card.setShowFullSize(1);
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    cards = board.getAllCards(); //Retrieve all cards on the board
    tilesOK = []; //This will store the cards which can be moved
    for(i = 0; i < cards.length; i++) //Check all cards
    {
      ////If there is an empty tile adjacent to the Pincers then ability can be used
      if(boardutils.getAdjacentEmptyTiles(card.getPosition()).length > 0)
      {
        //If the card is metal, yours, not the Pincers, not immovable and not untargetable by active abilities
        if(cards[i].getID() != "pincers" && cards[i].getPlayer() == gameState.getCurrentPlayer() && cards[i].getBackground() == "metal" && boardutils.checkForImmovable(cards[i]) == false && boardutils.isUntargetableBySilver(cards[i]) == false)
        {
          //If its value is equal to the number of cards in your hand, then it is good
          switch(gameState.getCurrentPlayer())
          {
            case 1:
              if(cards[i].getCurrentValue() == p1Inventory.getAllCards().length)
              {
                tilesOK.push(cards[i].getPosition());
              }
              break;
            case 2:
              if(cards[i].getCurrentValue() == p2Inventory.getAllCards().length)
              {
                tilesOK.push(cards[i].getPosition());
              }
              break;
            case 3:
              if(cards[i].getCurrentValue() == p3Inventory.getAllCards().length)
              {
                tilesOK.push(cards[i].getPosition());
              }
              break;
            case 4:
              if(cards[i].getCurrentValue() == p4Inventory.getAllCards().length)
              {
                tilesOK.push(cards[i].getPosition());
              }
              break;
          }
        }
      }
      else
      {
        menu.showInformation(qsTranslate("pincers", "There is no adjacent empty tile!"));
        break;
      }
    }
    if(tilesOK.length > 0)
    {
      //Ask the player which card would he like to move
      menu.showInformation(qsTranslate("pincers", "Choose target"), -1);
      //The player chooses one from the good cards
      selectedTile = boardutils.tileChoice(tilesOK, 1);
      selectedCard = board.getCardAtPosition(selectedTile);
      //Hide the 'Choose which card...' label
      menu.hideInformation();
      //Unless canceled
      if(selectedTile.x != -1)
      {
        //Now the player must choose the new place for the card
        menu.showInformation(qsTranslate("pincers", "Choose where would you like to move the selected item"), -1);
        //Only the empty tiles adjacent to the Pincers available now
        selectedPos = boardutils.tileChoice(boardutils.getAdjacentEmptyTiles(card.getPosition()), 0);
        //We got the choice, hide the info
        menu.hideInformation();
        //Move the card
        board.moveCard(selectedTile, selectedPos);
        //Log
        boardutils.logSilverActivation(card.getName());
        gameState.writeToGameLog(qsTranslate("pincers", "<font color=\"paleturquoise\">The %1 moved %2%3 %4 to %5%6.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg((board.getCardAtPosition(selectedPos)).getName()).arg(boardutils.convertToABC(selectedPos.x)).arg(selectedPos.y+1), [1, 2, 3, 4], 1, 0);
        //Crop or grayscale
        if(card.isFullSize() == true)
        {
          card.setShowFullSize(0);
        }
        else
        {
          card.setGrayScale(1);
          //Log
          gameState.writeToGameLog(qsTranslate("pincers", "The %1 lost its abilities.").arg(card.getName()), [1, 2, 3, 4], 1, 0);
        }
      }
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
