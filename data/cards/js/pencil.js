//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    if(mainInventory.getAllCards().length > 0)
    {
      if(boardutils.limitedByPerfume() == false)
      {
        //All empty tiles are fine
        return boardutils.getEmptyTiles();
      }
      else
      {
        return boardutils.getTilesAbovePerfume();
      }
    }
    else
    {
      var array = new Array();
      return array;
    }
  }
  else
  {
    var array = new Array();
    return array;
  }
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  if(eventData[0] == "turnStarted")
  {
    card.setShowFullSize(1);
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    var tilesOK = [];
    var cardsInHand = 0;
    switch(gameState.getCurrentPlayer())
    {
      case 1:
        cardsInHand = p1Inventory.getAllCards().length;
        break;
      case 2:
        cardsInHand = p2Inventory.getAllCards().length;
        break;
      case 3:
        cardsInHand = p3Inventory.getAllCards().length;
        break;
      case 4:
        cardsInHand = p4Inventory.getAllCards().length;
        break;
    }
    
    for(ctc = 2; ctc < 10; ctc = ctc+2)
    {
      var xCorri = 0;
      var yCorri = 0;
      if(ctc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
      if(ctc == 4 && card.getPosX() > 0) {xCorri = -1;}
      if(ctc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
      if(ctc == 8 && card.getPosY() > 0) {yCorri = -1;}
      TTC = Point(card.getPosX()+xCorri,card.getPosY()+yCorri);
      CTC = board.getCardAtPosition(TTC);
      if(CTC != null && CTC != card && Math.floor(CTC.getCounter()/2) <= cardsInHand && boardutils.isUntargetableBySilver(CTC) == false)
      {;
        tilesOK.push(TTC);
      }
    }
    if(tilesOK.length > 0)
    {
      menu.showInformation(qsTranslate("pencil", "Choose which item would you like to place the counter on"), -1);
      //The player chooses one from the good cards
      selectedTile = boardutils.tileChoice(tilesOK, 1);
      menu.hideInformation();
      //Unless canceled
      if(selectedTile.x != -1)
      {
        selectedCard = board.getCardAtPosition(selectedTile);
        var amountToDiscard = Math.floor(selectedCard.getCounter()/2);
        while(amountToDiscard > 0)
        {
          amountToDiscard--;
          menu.showInformation(qsTranslate("pencil", "Choose an item to discard"), -1);
          if(gameState.getCurrentPlayer() == 1)
          {
            pencilDiscard = p1Inventory.cardChoice(p1Inventory.getAllCards(), 0, -1);
            p1Inventory.removeCard(pencilDiscard.getUID());
          }
          if(gameState.getCurrentPlayer() == 2)
          {
            pencilDiscard = p2Inventory.cardChoice(p2Inventory.getAllCards(), 0, -1);
            p2Inventory.removeCard(pencilDiscard.getUID());
          }
          if(gameState.getCurrentPlayer() == 3)
          {
            pencilDiscard = p3Inventory.cardChoice(p3Inventory.getAllCards(), 0, -1);
            p3Inventory.removeCard(pencilDiscard.getUID());
          }
          if(gameState.getCurrentPlayer() == 4)
          {
            pencilDiscard = p4Inventory.cardChoice(p4Inventory.getAllCards(), 0, -1);
            p4Inventory.removeCard(pencilDiscard.getUID());
          }
          trash.addCard(pencilDiscard);
          menu.hideInformation();

          //Log
          gameState.writeToGameLog(qsTranslate("pencil", "<font color=\"paleturquoise\">The %1 discarded the %2.</font>").arg(card.getName()).arg(pencilDiscard.getName()), [1, 2, 3, 4], 1, 0);
        }
        selectedCard.setCounter(selectedCard.getCounter() + 1);
        //Log
        gameState.writeToGameLog(qsTranslate("pencil", "<font color=\"paleturquoise\">The %1 placed 1 counter to %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);
        //Crop or grayscale
        if(card.isFullSize() == true)
        {
          card.setShowFullSize(0);
        }
        else
        {
          card.setGrayScale(1);
          //Log
          gameState.writeToGameLog(qsTranslate("pencil", "The %1 lost its abilities.").arg(card.getName()), [1, 2, 3, 4], 1, 0);
        }
      }

    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
