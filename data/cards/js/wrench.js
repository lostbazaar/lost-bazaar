//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums
//@include jsutils.point

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    if(boardutils.limitedByPerfume() == false)
    {
      eT = boardutils.getEmptyTiles();
    }
    else
    {
      eT = boardutils.getTilesAbovePerfume();
    }

    var wrenchAllowedEverywhere = false;
    var trashCards = trash.getAllCards()
    if(trashCards.length > 0)
    {
      for(i = 0; i < trashCards.length; i++)
      {
        if(trashCards[i].getBackground() == "metal")
        {
          wrenchAllowedEverywhere = true;
          return eT;
          break;
        }
      }
    }

    if(wrenchAllowedEverywhere == false)
    {
      //First condition is not fulfilled, check for the availability of the second ability
      cards = board.getAllCards();

      for(i = 0; i < eT.length; i++)
      {
        for(j = 1; j < 9; j++)
        {
          var xCorri = 0;
          var yCorri = 0;
          if(j == 1) {xCorri = 1; yCorri = -2;}
          if(j == 2) {xCorri = 2; yCorri = -1;}
          if(j == 3) {xCorri = 2; yCorri = 1;}
          if(j == 4) {xCorri = 1; yCorri = 2;}
          if(j == 5) {xCorri = -1; yCorri = 2;}
          if(j == 6) {xCorri = -2; yCorri = 1;}
          if(j == 7) {xCorri = -2; yCorri = -1;}
          if(j == 8) {xCorri = -1; yCorri = -2;}

          var CTC = board.getCardAtPosition(Point(eT[i].x+xCorri,eT[i].y+yCorri));
          if(CTC != null && CTC.getCurrentValue() <= 1 && boardutils.checkForUnrotatable(CTC) == false && boardutils.isUntargetableByBronze(CTC) == false)
          {
            array.push(eT[i]);
            break;
          }
        }
      }
    }
  }
  return array;
}


function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    var firstAbilityAvailable = false;
    var secondAbilityAvailable = false;
    var trashCards = trash.getAllCards();
    for(i = 0; i < trashCards.length; i++)
    {
      if(trashCards[i].getBackground() == "metal")
      {
        firstAbilityAvailable = true;
        break;
      }
    }

    tilesOK = []; //This will store the cards which can be moved
    for(j = 1; j < 9; j++)
    {
      var xCorri = 0;
      var yCorri = 0;
      if(j == 1) {xCorri = 1; yCorri = -2;}
      if(j == 2) {xCorri = 2; yCorri = -1;}
      if(j == 3) {xCorri = 2; yCorri = 1;}
      if(j == 4) {xCorri = 1; yCorri = 2;}
      if(j == 5) {xCorri = -1; yCorri = 2;}
      if(j == 6) {xCorri = -2; yCorri = 1;}
      if(j == 7) {xCorri = -2; yCorri = -1;}
      if(j == 8) {xCorri = -1; yCorri = -2;}
      var TTC = Point(card.getPosX()+xCorri,card.getPosY()+yCorri);
      var CTC = board.getCardAtPosition(Point(card.getPosX()+xCorri,card.getPosY()+yCorri));
      if(CTC != null && CTC.getCurrentValue() <= 1 && boardutils.checkForUnrotatable(CTC) == false && boardutils.isUntargetableByBronze(CTC) == false)
      {
        tilesOK.push(TTC);
        secondAbilityAvailable = true;
      }
    }

    var cancelToTop;
    do
    {
      cancelToTop = 0;
      //Display title
      menu.showInformation(qsTranslate("wrench", "Choose from the abilities of the %1").arg(card.getName()), -1);
      //Stores the text of the choices
      choiceArray = [
      qsTranslate("wrench", "Take back a metal item from the trash"),
      qsTranslate("wrench", "Rotate an item of 1 value or less")
      ];
      //Stores which choices are enabled and which aren't
      enableArray = [firstAbilityAvailable == true ? "true" : "false", secondAbilityAvailable == true ? "true" : "false"];
      //Stores the tooltips for the choices
      toolTipArray = [firstAbilityAvailable == true ? "" : qsTranslate("wrench", "No metal item in the trash"), secondAbilityAvailable == true ? "" : qsTranslate("wrench", "There are no cards on the board that satisfy the conditions")];

      //Get the player's choice
      var choice = menu.getChoice(choiceArray, enableArray, toolTipArray);
      menu.hideInformation();

      if(choice == 0)
      {
        menu.showInformation(qsTranslate("wrench", "Choose a metal item to reclaim"), -1);
        do
        {
          selectedToReclaim = trash.cardChoice(trashCards, 1, 1);
        }
        while(selectedToReclaim != -1 || selectedToReclaim.getBackground() != "metal");
        menu.hideInformation();
        if(selectedToReclaim != -1)
        {
          trash.removeCard(selectedToReclaim.getUID());
          if(card.getPlayer() == 1) {p1Inventory.addCard(selectedToReclaim);}
          if(card.getPlayer() == 2) {p2Inventory.addCard(selectedToReclaim);}
          if(card.getPlayer() == 3) {p3Inventory.addCard(selectedToReclaim);}
          if(card.getPlayer() == 4) {p4Inventory.addCard(selectedToReclaim);}
          //Log
          boardutils.logCardPlacement(card.getName());
          var userNames = gameState.getPlayerNickNames();
          gameState.writeToGameLog(qsTranslate("wrench", "<font color=\"bisque\">The %1 reclaimed the %2 for %3.</font>").arg(card.getName()).arg(selectedToReclaim.getName()).arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
        }
        else
        {
          cancelToTop = 1;
        }
      }
      if(choice == 1)
      {
        menu.showInformation(qsTranslate("wrench", "Choose which item would you like to rotate"), -1);
        selectedTile = boardutils.tileChoice(tilesOK, 1);
        selectedCard = board.getCardAtPosition(selectedTile);
        menu.hideInformation();

        if(selectedTile.x != -1)
        {
          menu.showInformation(qsTranslate("wrench", "Choose the direction for rotating the %1").arg(selectedCard.getName()), -1);
          choiceArray = [
          qsTranslate("wrench", "Rotate counter-clockwise"),
          qsTranslate("wrench", "Rotate clockwise")
          ];
          enableArray = ["true", "true"];
          toolTipArray = ["", ""];
          var choice = menu.getChoice(choiceArray, enableArray, toolTipArray);
          menu.hideInformation();

          if(choice == 0)
          {
            var prevOwner = selectedCard.getPlayer();
            if(prevOwner == 1) {selectedCard.changeOwner(4);}
            if(prevOwner == 2) {selectedCard.changeOwner(3);}
            if(prevOwner == 3) {selectedCard.changeOwner(1);}
            if(prevOwner == 4) {selectedCard.changeOwner(2);}
            //Log
            boardutils.logCardPlacement(card.getName());
            gameState.writeToGameLog(qsTranslate("wrench", "<font color=\"bisque\">The %1 rotated %2%3 %4 by 90° counter-clockwise.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);
          }
          if(choice == 1)
          {
            var prevOwner = selectedCard.getPlayer();
            if(prevOwner == 1) {selectedCard.changeOwner(3);}
            if(prevOwner == 2) {selectedCard.changeOwner(4);}
            if(prevOwner == 3) {selectedCard.changeOwner(2);}
            if(prevOwner == 4) {selectedCard.changeOwner(1);}
            //Log
            gameState.writeToGameLog(qsTranslate("wrench", "<font color=\"bisque\">The %1 rotated %2%3 %4 by 90° clockwise.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);
          }
        }
        else
        {
          cancelToTop = 1;
        }
      }
    }
    while(cancelToTop == 1);
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
