//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    if((gameState.getCurrentPlayer() == 1 && p1Inventory.getAllCards().length > 1) || (gameState.getCurrentPlayer() == 2 && p2Inventory.getAllCards().length > 1) || (gameState.getCurrentPlayer() == 3 && p3Inventory.getAllCards().length > 1) || (gameState.getCurrentPlayer() == 4 && p4Inventory.getAllCards().length > 1))
    {
      if(boardutils.limitedByPerfume() == false)
      {
        //All empty tiles are fine
        return boardutils.getEmptyTiles();
      }
      else
      {
        return boardutils.getTilesAbovePerfume();
      }
    }
    else
    {
      var array = new Array();
      return array;
    }
  }
  else
  {
    var array = new Array();
    return array;
  }
}

function event(eventData)
{
  //Handle the cardPlacedOnBoard event
  //Check if this card is the one that was placed
  //If it is, then execute the bronze action
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Ask the player which card to discard
    menu.showInformation(qsTranslate("celticshield", "Choose an item to discard"), -1);

    //The player chooses them
    if(gameState.getCurrentPlayer() == 1)
    {
      selectedDiscard = p1Inventory.cardChoice(p1Inventory.getAllCards(), 0, -1);
      p1Inventory.removeCard(selectedDiscard.getUID());
    }
    if(gameState.getCurrentPlayer() == 2)
    {
      selectedDiscard = p2Inventory.cardChoice(p2Inventory.getAllCards(), 0, -1);
      p2Inventory.removeCard(selectedDiscard.getUID());
    }
    if(gameState.getCurrentPlayer() == 3)
    {
      selectedDiscard = p3Inventory.cardChoice(p3Inventory.getAllCards(), 0, -1);
      p3Inventory.removeCard(selectedDiscard.getUID());
    }
    if(gameState.getCurrentPlayer() == 4)
    {
      selectedDiscard = p4Inventory.cardChoice(p4Inventory.getAllCards(), 0, -1);
      p4Inventory.removeCard(selectedDiscard.getUID());
    }
    menu.hideInformation();
    trash.addCard(selectedDiscard);

    //Logging
    boardutils.logCardPlacement(card.getName());
    var userNames = gameState.getPlayerNickNames();
    gameState.writeToGameLog(qsTranslate("celticshield", "<font color=\"bisque\">The %1 discarded the %2 from %3's hand.</font>").arg(card.getName()).arg(selectedDiscard.getName()).arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
