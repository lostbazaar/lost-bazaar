//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Log
    boardutils.logCardPlacement(card.getName());
    eT = boardutils.getEmptyTiles();
    if(boardutils.isHalted(eT) == true || board.isDoubleTile(card.getPosition()) == true)
    {
      card.setGrayScale(1);
      gameState.writeToGameLog(qsTranslate("treasurechest", "The %1 has been disabled.").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1] && boardutils.checkForUnpickable(card) == false)
  {
    var prevOwner = card.getPlayer()
    if(gameState.getGameMode() > 0 && gameState.getGameMode() < 4)
    {
      giftablePlayers = [] //This will store the possible players for the first choice
      if(gameState.getCurrentPlayer() == 2 || (gameState.getCurrentPlayer() == 3 && gameState.getGameMode() != 1) || gameState.getCurrentPlayer() == 4)
      {
        giftablePlayers.push(1);
      }
      if(gameState.getCurrentPlayer() == 1 || gameState.getCurrentPlayer() == 3 || (gameState.getCurrentPlayer() == 4 && gameState.getGameMode() != 1))
      {
        giftablePlayers.push(2);
      }
      if((gameState.getCurrentPlayer() == 1 && gameState.getGameMode() != 1) || gameState.getCurrentPlayer() == 2 || gameState.getCurrentPlayer() == 4)
      {
        giftablePlayers.push(3);
      }
      if((gameState.getCurrentPlayer() == 1 && gameState.getGameMode() != 2) || (gameState.getCurrentPlayer() == 2 && gameState.getGameMode() == 3) || (gameState.getCurrentPlayer() == 3 && gameState.getGameMode() != 2 ))
      {
        giftablePlayers.push(4);
      }

      menu.showInformation(qsTranslate("treasurechest", "To which player you would like to give the %1?").arg(card.getName()), -1);
      choice = graphicsViewMenu.getPlayer(giftablePlayers, 0);
      board.removeCard(card.getUID())
      switch(choice)
      {
        case 1:
          p1Inventory.addCard(card);
          break;
        case 2:
          p2Inventory.addCard(card);
          break;
        case 3:
          p3Inventory.addCard(card);
          break;
        case 4:
          p4Inventory.addCard(card);
          break;
      }
      menu.hideInformation();
    }
    else
    {
      board.removeCard(card.getUID())
      if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 1) {p2Inventory.addCard(card); choice = 2;}
      if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 2) {p1Inventory.addCard(card); choice = 1;}
      if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 1) {p3Inventory.addCard(card); choice = 2;}
      if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 3) {p1Inventory.addCard(card); choice = 3;}
    }
    //Log
    boardutils.logSilverActivation(card.getName());
    var userNames = gameState.getPlayerNickNames();
    gameState.writeToGameLog(qsTranslate("treasurechest", "<font color=\"paleturquoise\">The %1 has been sent to %2's hand.</font>").arg(card.getName()).arg(userNames[choice-1]), [1, 2, 3, 4], 1, 0);
    if(boardutils.isSixFix(gameState.getCurrentPlayer()) == true)
    {
      var diceRoll = 6
      //Log
      gameState.writeToGameLog(qsTranslate("treasurechest", "<font color=\"paleturquoise\">%1 rolled a die: 6.</font>").arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
    }
    else
    {
      var diceRoll = rng.randomBetween(1, 7);
      //Log
      gameState.writeToGameLog(qsTranslate("treasurechest", "<font color=\"paleturquoise\">%1 rolled a die: %2.</font>").arg(userNames[gameState.getCurrentPlayer()-1]).arg(diceRoll), [1, 2, 3, 4], 1, 0);
    }
    menu.showInformation(qsTranslate("treasurechest", "%1%2 %3: %4").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()).arg(boardutils.numberToFace(diceRoll)), 4000);
    var amountDrawn = 0;
    for(i = 1; i < 5; i++)
    {
      var entireStorage = mainInventory.getAllCards()
      cardRandomlyDrawn = entireStorage[rng.randomBetween(0, entireStorage.length)];
      if(cardRandomlyDrawn.getBaseValue() <= diceRoll)
      {
        amountDrawn++
        mainInventory.removeCard(cardRandomlyDrawn.getUID());
        switch(gameState.getCurrentPlayer())
        {
          case 1:
            p1Inventory.addCard(cardRandomlyDrawn);
            break;
          case 2:
            p2Inventory.addCard(cardRandomlyDrawn);
            break;
          case 3:
            p3Inventory.addCard(cardRandomlyDrawn);
            break;
          case 4:
            p4Inventory.addCard(cardRandomlyDrawn);
            break;
        }
        gameState.writeToGameLog(qsTranslate("treasurechest", "<font color=\"paleturquoise\">The %1 drew the %2 for %3.</font>").arg(card.getName()).arg(cardRandomlyDrawn.getName()).arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
      }
      else
      {
        gameState.writeToGameLog(qsTranslate("treasurechest", "<font color=\"paleturquoise\">The %1 drew the %2 for %3, which is immediately returned to the storage.</font>").arg(card.getName()).arg(cardRandomlyDrawn.getName()).arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
      }
    }
    if(amountDrawn > 1)
    {
      menu.showInformation(qsTranslate("treasurechest", "You have drawn %1 items!").arg(amountDrawn));
    }
    if(amountDrawn == 1)
    {
      menu.showInformation(qsTranslate("treasurechest", "You have drawn an item!"));
    }
    if(amountDrawn == 0)
    {
      menu.showInformation(qsTranslate("treasurechest", "No items were drawn!"));
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
