//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    var arrayNumber = 0;
    cards = board.getAllCards();
    for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
    {
      for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
      {
        if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
        {
          var entryCondition = false;
          var itemsToTranslate = [];
          for(ctc = 2; ctc < 10; ctc = ctc+2)
          {
            var xCorri = 0;
            var yCorri = 0;
            if(ctc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
            if(ctc == 4 && xVaria > 0) {xCorri = -1;}
            if(ctc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
            if(ctc == 8 && yVaria > 0) {yCorri = -1;}
            CTC = board.getCardAtPosition(Point(xVaria+xCorri,yVaria+yCorri));
            if(CTC != null && CTC != card)
            {
              itemsToTranslate.push(CTC);
            }
          }

          if(itemsToTranslate.length > 0)
          {
            for(i = 0; i < itemsToTranslate.length; i++)
            {
              if(itemsToTranslate[i].getPlayer() == gameState.getCurrentPlayer())
              {
                if(boardutils.checkForUnrotatable(itemsToTranslate[i]) == false)
                {
                  entryCondition = true;
                  var translationToP1 = true;
                  var translationToP2 = true;
                  var translationToP3 = true;
                  var translationToP4 = true;
                  switch(gameState.getCurrentPlayer())
                  {
                    case 1: translationToP1 = false; break;
                    case 2: translationToP2 = false; break;
                    case 3: translationToP3 = false; break;
                    case 4: translationToP4 = false; break;
                  }
                  switch(gameState.getGameMode())
                  {
                    case 0: translationToP3 = false; translationToP4 = false; break;
                    case 2: translationToP4 = false; break;
                    case 4: translationToP2 = false; translationToP4 = false; break;
                  }

                  for(j = 0; j < itemsToTranslate.length; j++)
                  {
                    if(itemsToTranslate[j].getPlayer() != gameState.getCurrentPlayer())
                    {
                      if(!board.isDoubleTile(itemsToTranslate[j].getPosition()) && !board.isSafeTile(itemsToTranslate[j].getPosition()))
                      {
                        if(boardutils.checkForUnrotatable(itemsToTranslate[j]) == true)
                        {
                          switch(itemsToTranslate[j].getPlayer())
                          {
                            case 1: translationToP1 = false; break;
                            case 2: translationToP2 = false; break;
                            case 3: translationToP3 = false; break;
                            case 4: translationToP4 = false; break;
                          }
                        }
                      }
                    }
                  }

                  if(translationToP1 == true || translationToP2 == true || translationToP3 == true || translationToP4 == true)
                  {
                    array[arrayNumber] = new Point(xVaria,yVaria);
                    arrayNumber++;
                    break;
                  }

                }
              }
            }
          }
        }
      }
    }
    return array;
  }
  else
  {
    return array;
  }
}


//This function handles all events
function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    tilesOK = []; //This will store the tiles of the good cards
    var itemsToTranslate = []; //This will store the candidates
    var itemsOrientation = [];
    var translaTable = [];

    for(ctc = 2; ctc < 10; ctc = ctc+2)
    {
      var xCorri = 0;
      var yCorri = 0;
      if(ctc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
      if(ctc == 4 && card.getPosX() > 0) {xCorri = -1;}
      if(ctc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
      if(ctc == 8 && card.getPosY() > 0) {yCorri = -1;}
      TTC = Point(card.getPosX()+xCorri,card.getPosY()+yCorri);
      CTC = board.getCardAtPosition(TTC);
      if(CTC != null && CTC.getUID() != card.getUID() && CTC.getPlayer() == card.getPlayer() && boardutils.checkForUnrotatable(CTC) == false && boardutils.isUntargetableByBronze(CTC) == false)
      {
        itemsToTranslate.push(CTC);
        itemsOrientation.push(ctc);
      }
    }

    //Make itemstotranslate into tilesOK
    if(itemsToTranslate.length > 0)
    {
      for(i = 0; i < itemsToTranslate.length; i++)
      {
        var entrycondition = true;
        var translationToP1 = 1;
        var translationToP2 = 1;
        var translationToP3 = 1;
        var translationToP4 = 1;
        switch(gameState.getCurrentPlayer())
        {
          case 1: translationToP1 = 0; break;
          case 2: translationToP2 = 0; break;
          case 3: translationToP3 = 0; break;
          case 4: translationToP4 = 0; break;
        }
        switch(gameState.getGameMode())
        {
          case 0: translationToP3 = 0; translationToP4 = false; break;
          case 2: translationToP4 = 0; break;
          case 4: translationToP2 = 0; translationToP4 = false; break;
        }

        for(ctc = 2; ctc < 10; ctc = ctc+2)
        {
          var xCorri = 0;
          var yCorri = 0;
          if(ctc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
          if(ctc == 4 && card.getPosX() > 0) {xCorri = -1;}
          if(ctc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
          if(ctc == 8 && card.getPosY() > 0) {yCorri = -1;}
          TTC = Point(card.getPosX()+xCorri,card.getPosY()+yCorri);
          CTC = board.getCardAtPosition(TTC);
          if(CTC != null && CTC.getUID() != card.getUID() && CTC != itemsToTranslate[i])
          {
            if(board.isDoubleTile(TTC) == false && board.isSafeTile(TTC) == false && boardutils.checkForUnrotatable(CTC) == true && CTC.getBackground() == itemsToTranslate[i].getBackground())
            {
              switch(CTC.getPlayer())
              {
                case 1: translationToP1 = 0; break;
                case 2: translationToP2 = 0; break;
                case 3: translationToP3 = 0; break;
                case 4: translationToP4 = 0; break;
              }
            }
          }
        }

        if(translationToP1 == true || translationToP2 == true || translationToP3 == true || translationToP4 == true)
        {
          tilesOK.push(itemsToTranslate[i].getPosition());
          translaTable.push([itemsOrientation[i], translationToP1, translationToP2, translationToP3, translationToP4])
        }

      }
    }

    //Log
    boardutils.logCardPlacement(card.getName());

    if(tilesOK.length > 0)
    {
      //Ask the player which card would he like to rotate
      menu.showInformation(qsTranslate("dictionary", "Choose which card would you like to rotate"), -1);
      //The player chooses one from the good cards
      selectedTile = boardutils.tileChoice(tilesOK, 0);
      //Hide the 'Choose which card...' label
      menu.hideInformation();
      selectedCard = board.getCardAtPosition(selectedTile);

      //Choose target player
      if(gameState.getGameMode() > 0 && gameState.getGameMode() < 4)
      {
        targetablePlayers = [] //This will store the possible players for the first choice
        var selectedDirection = 0;
        if(selectedTile.x == card.getPosX() && selectedTile.y == card.getPosY()+1)
        {
          selectedDirection = 2;
        }
        if(selectedTile.x == card.getPosX()-1 && selectedTile.y == card.getPosY())
        {
          selectedDirection = 4;
        }
        if(selectedTile.x == card.getPosX()+1 && selectedTile.y == card.getPosY())
        {
          selectedDirection = 6;
        }
        if(selectedTile.x == card.getPosX() && selectedTile.y == card.getPosY()-1)
        {
          selectedDirection = 8;
        }
        for(i = 0; i < translaTable.length; i++)
        {
          if(translaTable[i][0] == selectedDirection)
          {
            for(j = 1; j < 5; j++)
            {
              if(translaTable[i][j] == true)
              {
                targetablePlayers.push(j);
              }
            }
          }
        }

        menu.showInformation(qsTranslate("dictionary", "Toward who would you like to rotate the chosen item?"), -1);
        //Get the player's answer
        choice = graphicsViewMenu.getPlayer(targetablePlayers, 0);
        //Now we have the choice
        menu.hideInformation();
        switch(choice)
        {
          case 1: var playerTranslatedFor = 1; break;
          case 2: var playerTranslatedFor = 2; break;
          case 3: var playerTranslatedFor = 3; break;
          case 4: var playerTranslatedFor = 4; break;
        }
      }
      else
      {
        //1v1 opposing
        if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 1)
        {
          var playerTranslatedFor = 2;
        }
        if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 2)
        {
          var playerTranslatedFor = 1;
        }

        //1v1 adjacent
        if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 1)
        {
          var playerTranslatedFor = 3;
        }
        if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 3)
        {
          var playerTranslatedFor = 1;
        }
      }

      //Searches again for the rotatable items
      cardsOK = [];
      for(ctc = 2; ctc < 10; ctc = ctc+2)
      {
        var xCorri = 0;
        var yCorri = 0;
        if(ctc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
        if(ctc == 4 && card.getPosX() > 0) {xCorri = -1;}
        if(ctc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
        if(ctc == 8 && card.getPosY() > 0) {yCorri = -1;}
        TTC = Point(card.getPosX()+xCorri,card.getPosY()+yCorri);
        CTC = board.getCardAtPosition(TTC);
        if(CTC != null && CTC.getUID() != card.getUID() && CTC.getPlayer() == playerTranslatedFor && CTC.getBackground() == selectedCard.getBackground() && board.isDoubleTile(TTC) == false && board.isSafeTile(TTC) == false && board.isInnovativeTile(TTC) == false)
        {
          cardsOK.push(CTC);
        }
      }

      selectedCard.changeOwner(playerTranslatedFor);
      //Log
      var userNames = gameState.getPlayerNickNames();
      gameState.writeToGameLog(qsTranslate("dictionary", "<font color=\"bisque\">%1%2 %3 has been rotated towards %4.</font>").arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()).arg(userNames[playerTranslatedFor-1]), [1, 2, 3, 4], 1, 0);

      for(i = 0; i < cardsOK.length; i++)
      {
        cardsOK[i].changeOwner(gameState.getCurrentPlayer());
        gameState.writeToGameLog(qsTranslate("dictionary", "<font color=\"bisque\">%1%2 %3 has been rotated towards %4.</font>").arg(boardutils.convertToABC(cardsOK[i].getPosX())).arg(cardsOK[i].getPosY()+1).arg(cardsOK[i].getName()).arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
      }

    }

    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
