//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}


function event(eventData)
{
  //Handle the cardPlacedOnBoard event
  //Check if this card is the one that was placed
  //If it is, then execute the bronze action
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    var selectedCard1 = 0; var selectedTile1 = 0; var selectedPos1 = 0;
    var selectedCard2 = 0; var selectedTile2 = 0; var selectedPos2 = 0;
    var selectedCard3 = 0; var selectedTile3 = 0; var selectedPos3 = 0;

    for(j = 0; j < 3; j++)
    {
      cards = board.getAllCards(); //Retrieve all cards on the board
      tilesOK = []; //This will store the position of the good cards
      for(i = 0; i < cards.length; i++) //Check all cards
      {
        if(Math.pow(card.getPosX()-cards[i].getPosX(), 2) + Math.pow(card.getPosY()-cards[i].getPosY(), 2) <= 1)
        {
          if(cards[i] == card)
          {
            tilesOK.push(cards[i].getPosition());
          }
          else if(board.isDoubleTile(cards[i].getPosition()) == false)
          {
            if(boardutils.getAdjacentEmptyTiles(cards[i].getPosition()).length > 0)
            {
              if(boardutils.checkForImmovable(cards[i]) == false)
              {
                if(boardutils.isUntargetableByBronze(cards[i]) == false)
                {
                  tilesOK.push(cards[i].getPosition());
                }
              }
            }
          }
        }
      }
      if(tilesOK.length > 1)
      {
        menu.showInformation(qsTranslate("leafblower", "Choose target (click on the Leaf Blower to dismiss it)"), -1);
        selectedTile = boardutils.tileChoice(tilesOK, 0);
        menu.hideInformation();
        selectedCard = board.getCardAtPosition(selectedTile);
        if(selectedCard == card)
        {
          //Cause to exit cycle
          j = 4;
        }
        else
        {
          menu.showInformation(qsTranslate("leafblower", "Choose where would you like to move the selected item"), -1);
          selectedPos = boardutils.tileChoice(boardutils.getAdjacentEmptyTiles(selectedTile), 0);
          menu.hideInformation();
          if(j == 0) {selectedCard1 = selectedCard; selectedTile1 = selectedTile; selectedPos1 = selectedPos;}
          if(j == 1) {selectedCard2 = selectedCard; selectedTile2 = selectedTile; selectedPos2 = selectedPos;}
          if(j == 2) {selectedCard3 = selectedCard; selectedTile3 = selectedTile; selectedPos3 = selectedPos;}
          board.moveCard(selectedTile, selectedPos);
        }
        //Exit loop if there are no choices to make
        if(tilesOK.length < j+2)
        {
          j = 4;
        }
      }
      else
      {
        menu.showInformation(qsTranslate("leafblower", "No item is selectable for ability!"));
        break;
      }
    }
    //Log
    boardutils.logCardPlacement(card.getName());
    if(selectedCard1 != 0)
    {
      gameState.writeToGameLog(qsTranslate("leafblower", "<font color=\"bisque\">The %1 moved %2%3 %4 to %5%6.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile1.x)).arg(selectedTile1.y+1).arg(selectedCard1.getName()).arg(boardutils.convertToABC(selectedPos1.x)).arg(selectedPos1.y+1), [1, 2, 3, 4], 1, 0);
    }
    if(selectedCard2 != 0)
    {
      gameState.writeToGameLog(qsTranslate("leafblower", "<font color=\"bisque\">The %1 moved %2%3 %4 to %5%6.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile2.x)).arg(selectedTile2.y+1).arg(selectedCard2.getName()).arg(boardutils.convertToABC(selectedPos2.x)).arg(selectedPos2.y+1), [1, 2, 3, 4], 1, 0);
    }
    if(selectedCard3 != 0)
    {
      gameState.writeToGameLog(qsTranslate("leafblower", "<font color=\"bisque\">The %1 moved %2%3 %4 to %5%6.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile3.x)).arg(selectedTile3.y+1).arg(selectedCard3.getName()).arg(boardutils.convertToABC(selectedPos3.x)).arg(selectedPos3.y+1), [1, 2, 3, 4], 1, 0);
    }
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
