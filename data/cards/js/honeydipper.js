//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    if(boardutils.getAdjacentEmptyTiles(card.getPosition()).length > 0 && boardutils.checkForImmovable(card) == false)
    {
      if((gameState.getCurrentPlayer() == 1 && p1Inventory.isEmpty() == 0) ||
         (gameState.getCurrentPlayer() == 2 && p2Inventory.isEmpty() == 0) ||
         (gameState.getCurrentPlayer() == 3 && p3Inventory.isEmpty() == 0) ||
         (gameState.getCurrentPlayer() == 4 && p4Inventory.isEmpty() == 0))
      {
        switch(gameState.getCurrentPlayer())
        {
          case 1:
            handDiscardedFrom = p1Inventory.getAllCards();
            cardRandomlyDiscarded = handDiscardedFrom[rng.randomBetween(0, handDiscardedFrom.length)];
            p1Inventory.removeCard(cardRandomlyDiscarded.getUID());
            break;
          case 2:
            handDiscardedFrom = p2Inventory.getAllCards();
            cardRandomlyDiscarded = handDiscardedFrom[rng.randomBetween(0, handDiscardedFrom.length)];
            p2Inventory.removeCard(cardRandomlyDiscarded.getUID());
            break;
          case 3:
            handDiscardedFrom = p3Inventory.getAllCards();
            cardRandomlyDiscarded = handDiscardedFrom[rng.randomBetween(0, handDiscardedFrom.length)];
            p3Inventory.removeCard(cardRandomlyDiscarded.getUID());
            break;
          case 4:
            handDiscardedFrom = p4Inventory.getAllCards();
            cardRandomlyDiscarded = handDiscardedFrom[rng.randomBetween(0, handDiscardedFrom.length)];
            p4Inventory.removeCard(cardRandomlyDiscarded.getUID());
            break;
        }
        trash.addCard(cardRandomlyDiscarded);

        //Log
        boardutils.logSilverActivation(card.getName());
        gameState.writeToGameLog(qsTranslate("honeydipper", "<font color=\"paleturquoise\">The %1 discarded the %2.</font>").arg(card.getName()).arg(cardRandomlyDiscarded.getName()), [1, 2, 3, 4], 1, 0);

        menu.showInformation(qsTranslate("honeydipper", "Choose where to move the %1").arg(card.getName()), -1);
        selectedPos = boardutils.tileChoice(boardutils.getAdjacentEmptyTiles(card.getPosition()), 0);
        menu.hideInformation();
        board.moveCard(card.getPosition(), selectedPos);

        //Log
        gameState.writeToGameLog(qsTranslate("honeydipper", "<font color=\"paleturquoise\">The %1 has been moved to %2%3.</font>").arg(card.getName()).arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1), [1, 2, 3, 4], 1, 0);
      }
    }
    else
    {
      menu.showInformation(qsTranslate("honeydipper", "You cannot use this ability!"));
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}


//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
