//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    var arrayNumber = 0;
    if(gameState.getCurrentPlayer() == 1) {var cardsInCurrentHand = p1Inventory.getAllCards().length;}
    if(gameState.getCurrentPlayer() == 2) {var cardsInCurrentHand = p2Inventory.getAllCards().length;}
    if(gameState.getCurrentPlayer() == 3) {var cardsInCurrentHand = p3Inventory.getAllCards().length;}
    if(gameState.getCurrentPlayer() == 4) {var cardsInCurrentHand = p4Inventory.getAllCards().length;}
    for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
    {
      for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
      {
        if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
        {
          var dominoCondition = false;
          var entryCondition = false;
          for(ttc = 2; ttc < 10; ttc = ttc+2)
          {
            var xCorri = 0;
            var yCorri = 0;
            if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
            if(ttc == 4 && xVaria > 0) {xCorri = -1;}
            if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
            if(ttc == 8 && yVaria > 0) {yCorri = -1;}
            TTC = Point(xVaria+xCorri,yVaria+yCorri);
            CTC = board.getCardAtPosition(TTC);
            if(CTC != null && CTC.getPlayer() == gameState.getCurrentPlayer())
            {
              dominoCondition = true;
            }
            if(CTC != null && boardutils.checkForUnpickable(CTC) == false && boardutils.isUntargetableByBronze(CTC) == false)
            {
              var amountToDiscard = Math.round(CTC.getCurrentValue()/2);
              if(amountToDiscard < 1)
              {
                amountToDiscard = 1;
              }
              if(amountToDiscard <= cardsInCurrentHand + 1)
              {
                entryCondition = true;
              }
            }
            if(dominoCondition == true && entryCondition == true)
            {
              array[arrayNumber] = new Point(xVaria,yVaria);
              arrayNumber++;
              break;
            }
          }
        }
      }
    }
    return array;
  }
  else
  {
    return array;
  }
}

//This function handles all events
function event(eventData)
{
  //Handle the cardPlacedOnBoard event
  //Check if this card is the one that was placed
  //If it is, then execute the bronze action
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Get the number of cards in current player's hand
    if(gameState.getCurrentPlayer() == 1) {var cardsInCurrentHand = p1Inventory.getAllCards().length;}
    if(gameState.getCurrentPlayer() == 2) {var cardsInCurrentHand = p2Inventory.getAllCards().length;}
    if(gameState.getCurrentPlayer() == 3) {var cardsInCurrentHand = p3Inventory.getAllCards().length;}
    if(gameState.getCurrentPlayer() == 4) {var cardsInCurrentHand = p4Inventory.getAllCards().length;}

    tilesOK = []; //This will store the cards which can be picked up
    for(ctc = 2; ctc < 10; ctc = ctc+2)
    {
      var xCorri = 0;
      var yCorri = 0;
      if(ctc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
      if(ctc == 4 && card.getPosX() > 0) {xCorri = -1;}
      if(ctc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
      if(ctc == 8 && card.getPosY() > 0) {yCorri = -1;}
      TTC = Point(card.getPosX()+xCorri,card.getPosY()+yCorri);
      CTC = board.getCardAtPosition(TTC);
      if(CTC != null && CTC.getUID() != card.getUID() && boardutils.checkForUnpickable(CTC) == false && boardutils.isUntargetableByBronze(CTC) == false)
      {
        var amountToDiscard = Math.round(CTC.getCurrentValue()/2);
        if(amountToDiscard < 1) {amountToDiscard = 1;}
        if(amountToDiscard <= cardsInCurrentHand + 1)
        {
          tilesOK.push(TTC);
        }
      }
    }

    //Log
    boardutils.logCardPlacement(card.getName());

    if(tilesOK.length > 0)
    {
      //Ask the player which card to pick up
      menu.showInformation(qsTranslate("operaglove", "Choose which item should be taken to your hand"), -1);
      //The player chooses one from the good cards
      selectedTile = boardutils.tileChoice(tilesOK, 0);
      //Hide the 'Choose which card...' label
      menu.hideInformation();
      selectedCard = board.getCardAtPosition(selectedTile);
      var selectedItemsValue = selectedCard.getCurrentValue();
      //Remove the card from the board
      board.removeCard(selectedCard.getUID());
      if(gameState.getCurrentPlayer() == 1) {p1Inventory.addCard(selectedCard);}
      if(gameState.getCurrentPlayer() == 2) {p2Inventory.addCard(selectedCard);}
      if(gameState.getCurrentPlayer() == 3) {p3Inventory.addCard(selectedCard);}
      if(gameState.getCurrentPlayer() == 4) {p4Inventory.addCard(selectedCard);}

      //Log
      var userNames = gameState.getPlayerNickNames();
      gameState.writeToGameLog(qsTranslate("operaglove", "<font color=\"bisque\">The %1 returned %2%3 %4 to %5's hand.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()).arg(userNames[selectedCard.getPlayer()-1]), [1, 2, 3, 4], 1, 0);

      for(i = -101; i <= selectedItemsValue; i = i+2)
      {
        if(i == -101)
        {
          i = 1;
        }
        menu.showInformation(qsTranslate("operaglove", "Choose an item to discard"), -1);
        if(gameState.getCurrentPlayer() == 1)
        {
          if(p1Inventory.getAllCards().length > 0)
          {
            glovesDiscard = p1Inventory.cardChoice(p1Inventory.getAllCards(), 0, -1);
            p1Inventory.removeCard(glovesDiscard.getUID());
            trash.addCard(glovesDiscard);
          }
        }
        if(gameState.getCurrentPlayer() == 2)
        {
          if(p2Inventory.getAllCards().length > 0)
          {
            glovesDiscard = p2Inventory.cardChoice(p2Inventory.getAllCards(), 0, -1);
            p2Inventory.removeCard(glovesDiscard.getUID());
            trash.addCard(glovesDiscard);
          }
        }
        if(gameState.getCurrentPlayer() == 3)
        {
          if(p3Inventory.getAllCards().length > 0)
          {
            glovesDiscard = p3Inventory.cardChoice(p3Inventory.getAllCards(), 0, -1);
            p3Inventory.removeCard(glovesDiscard.getUID());
            trash.addCard(glovesDiscard);
          }
        }
        if(gameState.getCurrentPlayer() == 4)
        {
          if(p4Inventory.getAllCards().length > 0)
          {
            glovesDiscard = p4Inventory.cardChoice(p4Inventory.getAllCards(), 0, -1);
            p4Inventory.removeCard(glovesDiscard.getUID());
            trash.addCard(glovesDiscard);
          }
        }
        menu.hideInformation();

        //Log
        gameState.writeToGameLog(qsTranslate("operaglove", "<font color=\"bisque\">The %1 discarded the %2.</font>").arg(card.getName()).arg(glovesDiscard.getName()), [1, 2, 3, 4], 1, 0);
      }
    }
    else
    {
      menu.showInformation(qsTranslate("operaglove", "No item is selectable for ability!"));
    }
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
