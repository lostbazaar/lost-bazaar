//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    var arrayNumber = 0;
    for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
    {
      for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
      {
        if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
        {
          var dominoCondition = false;
          var entryCondition = false;
          for(ttc = 2; ttc < 10; ttc = ttc+2)
          {
            var xCorri = 0;
            var yCorri = 0;
            if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
            if(ttc == 4 && xVaria > 0) {xCorri = -1;}
            if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
            if(ttc == 8 && yVaria > 0) {yCorri = -1;}
            TTC = Point(xVaria+xCorri,yVaria+yCorri);
            CTC = board.getCardAtPosition(TTC);
            if(CTC != null && CTC.getPlayer() == gameState.getCurrentPlayer())
            {
              dominoCondition = true;
            }
            if(CTC != null && boardutils.checkForUntrashable(CTC) == false && boardutils.isUntargetableByBronze(CTC) == false)
            {
              entryCondition = true;
            }
            if(dominoCondition == true && entryCondition == true)
            {
              array[arrayNumber] = new Point(xVaria,yVaria);
              arrayNumber++;
              break;
            }
          }
        }
      }
    }
    return array;
  }
  else
  {
    return array;
  }
}

//This function handles all events
function event(eventData)
{
  //Handle the cardPlacedOnBoard event
  //Check if this card is the one that was placed
  //If it is, then execute the bronze action
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    tilesOK = []; //This will store the tiles of the good cards
    for(ctc = 2; ctc < 10; ctc = ctc+2)
    {
      var xCorri = 0;
      var yCorri = 0;
      if(ctc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
      if(ctc == 4 && card.getPosX() > 0) {xCorri = -1;}
      if(ctc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
      if(ctc == 8 && card.getPosY() > 0) {yCorri = -1;}
      TTC = Point(card.getPosX()+xCorri,card.getPosY()+yCorri);
      CTC = board.getCardAtPosition(TTC);
      if(CTC != null && CTC.getUID() != card.getUID() && boardutils.checkForUntrashable(CTC) == false && boardutils.isUntargetableByBronze(CTC) == false)
      {
        tilesOK.push(TTC);
      }
    }

    //Log
    boardutils.logCardPlacement(card.getName());

    //Ask the player which card would he like to trash
    menu.showInformation(qsTranslate("hammer", "Choose target"), -1);
    //The player chooses one from the good tiles
    selectedTile = boardutils.tileChoice(tilesOK, 0);
    //Hide the 'Choose which card...' label
    menu.hideInformation();
    selectedCard = board.getCardAtPosition(selectedTile);

    //Sound effect
    var randomSound = rng.randomBetween(1, 6);
    switch(randomSound)
    {
      case 1: gameState.playSoundEffect("hammer-1.wav", 1); break;
      case 2: gameState.playSoundEffect("hammer-2.wav", 1); break;
      case 3: gameState.playSoundEffect("hammer-3.wav", 1); break;
      case 4: gameState.playSoundEffect("hammer-4.wav", 1); break;
      case 5: gameState.playSoundEffect("hammer-5.wav", 1); break;
    }

    var hitByDamage = selectedCard;
    damageOutcome = boardutils.damageEvents(card.getID(), hitByDamage, hitByDamage.getPosX(), hitByDamage.getPosY())
    var explodingItems = damageOutcome[0];

    //Log
    gameState.writeToGameLog(qsTranslate("hammer", "<font color=\"bisque\">The %1 destroyed %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(hitByDamage.getPosX())).arg(hitByDamage.getPosY()+1).arg(hitByDamage.getName()), [1, 2, 3, 4], 1, 0);

    board.moveCardToTrash(hitByDamage.getUID());

    if(explodingItems.length > 1)
    {
      //Bomb events
      for(boomOrder = 1; boomOrder < explodingItems.length; boomOrder++)
      {
        var BBV = gameState.cardForUID(damageOutcome[0][boomOrder]);

        //Log
        gameState.writeToGameLog(qsTranslate("bomb", "<font color=\"khaki\">The %1 destroyed %2%3 %4.</font>").arg(hitByDamage.getName()).arg(boardutils.convertToABC(BBV.getPosX())).arg(BBV.getPosY()+1).arg(BBV.getName()), [1, 2, 3, 4], 1, 0);

        board.moveCardToTrash(damageOutcome[0][boomOrder]);
      }
    }
    //Piggy Bank events
    if(damageOutcome[1] > 0) {for(drawCount = 0; drawCount < damageOutcome[1]; drawCount++) {gameState.drawCard(1, 0, 0, 0, 0, "khaki");}}
    if(damageOutcome[2] > 0) {for(drawCount = 0; drawCount < damageOutcome[2]; drawCount++) {gameState.drawCard(2, 0, 0, 0, 0, "khaki");}}
    if(damageOutcome[3] > 0) {for(drawCount = 0; drawCount < damageOutcome[3]; drawCount++) {gameState.drawCard(3, 0, 0, 0, 0, "khaki");}}
    if(damageOutcome[4] > 0) {for(drawCount = 0; drawCount < damageOutcome[4]; drawCount++) {gameState.drawCard(4, 0, 0, 0, 0, "khaki");}}

    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
