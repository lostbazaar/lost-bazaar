//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    var arrayNumber = 0;
    for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
    {
      for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
      {
        if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
        {
          var entryCondition = false;
          var xCorri = 0;
          var yCorri = 0;
          if(card.getPlayer() == 1 && yVaria < board.getHeight() - 1) {yCorri = 1;}
          if(card.getPlayer() == 2 && yVaria > 0) {yCorri = -1;}
          if(card.getPlayer() == 3 && xVaria > 0) {xCorri = -1;}
          if(card.getPlayer() == 4 && xVaria < board.getWidth() - 1) {xCorri = 1;}
          CTC = board.getCardAtPosition(Point(xVaria+xCorri,yVaria+yCorri));
          if(CTC == null)
          {
            array[arrayNumber] = new Point(xVaria,yVaria);
            arrayNumber++;
          }
          if(CTC != null)
          {
            if(boardutils.checkForUntrashable(CTC) == false || CTC.getCurrentValue() >= 7)
            {
              array[arrayNumber] = new Point(xVaria,yVaria);
              arrayNumber++;
            }
          }
        }
      }
    }
  }
  return array;
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Log
    var userNames = gameState.getPlayerNickNames();
    boardutils.logCardPlacement(card.getName());

    var xCorri = 0;
    var yCorri = 0;
    if(card.getPlayer() == 1 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
    if(card.getPlayer() == 2 && card.getPosY() > 0) {yCorri = -1;}
    if(card.getPlayer() == 3 && card.getPosX() > 0) {xCorri = -1;}
    if(card.getPlayer() == 4 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
    CTC = board.getCardAtPosition(Point(card.getPosX()+xCorri,card.getPosY()+yCorri));
    if(CTC != null && CTC != card)
    {
      if(boardutils.isSixFix(gameState.getCurrentPlayer()) == true)
      {
        var diceRoll = 6;
      }
      else
      {
        var diceRoll = rng.randomBetween(1, 7);
      }
      menu.showInformation(qsTranslate("fryingpan", "%1%2 %3: %4").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()).arg(boardutils.numberToFace(diceRoll)), 4000);

      //Log
      gameState.writeToGameLog(qsTranslate("fryingpan", "<font color=\"bisque\">%1 rolled a die: %2.</font>").arg(userNames[gameState.getCurrentPlayer()-1]).arg(diceRoll), [1, 2, 3, 4], 1, 0);

      if(CTC.getCurrentValue() <= diceRoll)
      {
        var hitByDamage = CTC;
        damageOutcome = boardutils.damageEvents(card.getID(), hitByDamage, hitByDamage.getPosX(), hitByDamage.getPosY())
        var explodingItems = damageOutcome[0];

        //Log
        gameState.writeToGameLog(qsTranslate("fryingpan", "<font color=\"bisque\">The %1 destroyed %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(hitByDamage.getPosX())).arg(hitByDamage.getPosY()+1).arg(hitByDamage.getName()), [1, 2, 3, 4], 1, 0);

        board.moveCardToTrash(hitByDamage.getUID());

        if(explodingItems.length > 1)
        {
          //Bomb events
          for(boomOrder = 1; boomOrder < explodingItems.length; boomOrder++)
          {
            var BBV = gameState.cardForUID(damageOutcome[0][boomOrder]);

            //Log
            gameState.writeToGameLog(qsTranslate("bomb", "<font color=\"khaki\">The %1 destroyed %2%3 %4.</font>").arg(hitByDamage.getName()).arg(boardutils.convertToABC(BBV.getPosX())).arg(BBV.getPosY()+1).arg(BBV.getName()), [1, 2, 3, 4], 1, 0);

            board.moveCardToTrash(damageOutcome[0][boomOrder]);
          }
        }
        //Piggy Bank events
        if(damageOutcome[1] > 0) {for(drawCount = 0; drawCount < damageOutcome[1]; drawCount++) {gameState.drawCard(1, 0, 0, 0, 0, "khaki");}}
        if(damageOutcome[2] > 0) {for(drawCount = 0; drawCount < damageOutcome[2]; drawCount++) {gameState.drawCard(2, 0, 0, 0, 0, "khaki");}}
        if(damageOutcome[3] > 0) {for(drawCount = 0; drawCount < damageOutcome[3]; drawCount++) {gameState.drawCard(3, 0, 0, 0, 0, "khaki");}}
        if(damageOutcome[4] > 0) {for(drawCount = 0; drawCount < damageOutcome[4]; drawCount++) {gameState.drawCard(4, 0, 0, 0, 0, "khaki");}}
      }
    }
    //Deny active abilities
    gameState.setSilverActionsDisabledForTurn(1);

    //Log
    gameState.writeToGameLog(qsTranslate("fryingpan", "%1 cannot use active abilities for the remaining turn.").arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
