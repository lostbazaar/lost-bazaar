//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    if((gameState.getCurrentPlayer() == 1 && p1Inventory.getAllCards().length > 2) || (gameState.getCurrentPlayer() == 2 && p2Inventory.getAllCards().length > 2) || (gameState.getCurrentPlayer() == 3 && p3Inventory.getAllCards().length > 2) || (gameState.getCurrentPlayer() == 4 && p4Inventory.getAllCards().length > 2))
    {
      if(mainInventory.getAllCards().length > 0)
      {
        if(boardutils.limitedByPerfume() == false)
        {
          //All empty tiles are fine
          return boardutils.getEmptyTiles();
        }
        else
        {
          return boardutils.getTilesAbovePerfume();
        }
      }
      else
      {
        var array = new Array();
        return array;
      }
    }
    else
    {
      var array = new Array();
      return array;
    }
  }
  else
  {
    var array = new Array();
    return array;
  }
}

function event(eventData)
{
  if(eventData[0] == "turnStarted")
  {
    card.setShowFullSize(1)
  }

  //Handle the cardPlacedOnBoard event
  //Check if this card is the one that was placed
  //If it is, then execute the bronze action
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    for(doTwice = 0; doTwice < 2; doTwice++) //Do this twice
    {
      //Ask the player which card to discard
      if(doTwice == 0)
      {
        menu.showInformation(qsTranslate("coffeegrinder", "Choose the first item to discard"), -1);
      }
      if(doTwice == 1)
      {
        var selectedDiscard1 = selectedDiscard;
        menu.showInformation(qsTranslate("coffeegrinder", "Choose the second item to discard"), -1);
      }
      //The player chooses them
      if(gameState.getCurrentPlayer() == 1)
      {
        selectedDiscard = p1Inventory.cardChoice(p1Inventory.getAllCards(), 0, -1);
        p1Inventory.removeCard(selectedDiscard.getUID());
      }
      if(gameState.getCurrentPlayer() == 2)
      {
        selectedDiscard = p2Inventory.cardChoice(p2Inventory.getAllCards(), 0, -1);
        p2Inventory.removeCard(selectedDiscard.getUID());
      }
      if(gameState.getCurrentPlayer() == 3)
      {
        selectedDiscard = p3Inventory.cardChoice(p3Inventory.getAllCards(), 0, -1);
        p3Inventory.removeCard(selectedDiscard.getUID());
      }
      if(gameState.getCurrentPlayer() == 4)
      {
        selectedDiscard = p4Inventory.cardChoice(p4Inventory.getAllCards(), 0, -1);
        p4Inventory.removeCard(selectedDiscard.getUID());
      }
      menu.hideInformation();
      trash.addCard(selectedDiscard);
    }
    //Sound effect
    var randomSound = rng.randomBetween(1, 7);
    switch(randomSound)
    {
      case 1: gameState.playSoundEffect("grind-1.wav", 1); break;
      case 2: gameState.playSoundEffect("grind-2.wav", 1); break;
      case 3: gameState.playSoundEffect("grind-3.wav", 1); break;
      case 4: gameState.playSoundEffect("grind-4.wav", 1); break;
      case 5: gameState.playSoundEffect("grind-5.wav", 1); break;
      case 6: gameState.playSoundEffect("grind-6.wav", 1); break;
    }
    //Log
    boardutils.logCardPlacement(card.getName());
    var userNames = gameState.getPlayerNickNames();
    gameState.writeToGameLog(qsTranslate("coffeegrinder", "<font color=\"paleturquoise\">The %1 discarded the %2 from %3's hand.</font>").arg(card.getName()).arg(selectedDiscard1.getName()).arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
    gameState.writeToGameLog(qsTranslate("coffeegrinder", "<font color=\"paleturquoise\">The %1 discarded the %2 from %3's hand.</font>").arg(card.getName()).arg(selectedDiscard.getName()).arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
    //A card is drawn for the player
    gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "bisque");
    //An informational message is displayed (it will disappear automatically)
    menu.showInformation(qsTranslate("coffeegrinder", "You have drawn an item!"));
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    //Check for hands not being empty
    if((gameState.getCurrentPlayer() == 1 && p1Inventory.isEmpty() == 0) || (gameState.getCurrentPlayer() == 2 && p2Inventory.isEmpty() == 0) || (gameState.getCurrentPlayer() == 3 && p3Inventory.isEmpty() == 0) || (gameState.getCurrentPlayer() == 1 && p1Inventory.isEmpty() == 0))
    {
      //Ask the player which card to discard
      menu.showInformation(qsTranslate("coffeegrinder", "Choose an item to discard"), -1);
      //The player chooses one
      if(gameState.getCurrentPlayer() == 1)
      {
        selectedDiscard = p1Inventory.cardChoice(p1Inventory.getAllCards(), 1, -1);
        if(selectedDiscard != null)
        {
          p1Inventory.removeCard(selectedDiscard.getUID());
        }
      }
      if(gameState.getCurrentPlayer() == 2)
      {
        selectedDiscard = p2Inventory.cardChoice(p2Inventory.getAllCards(), 1, -1);
        if(selectedDiscard != null)
        {
          p2Inventory.removeCard(selectedDiscard.getUID());
        }
      }
      if(gameState.getCurrentPlayer() == 3)
      {
        selectedDiscard = p3Inventory.cardChoice(p3Inventory.getAllCards(), 1, -1);
        if(selectedDiscard != null)
        {
          p3Inventory.removeCard(selectedDiscard.getUID());
        }
      }
      if(gameState.getCurrentPlayer() == 4)
      {
        selectedDiscard = p4Inventory.cardChoice(p4Inventory.getAllCards(), 1, -1);
        if(selectedDiscard != null)
        {
          p4Inventory.removeCard(selectedDiscard.getUID());
        }
      }

      menu.hideInformation();
      //Unless canceled
      if(selectedDiscard != null)
      {
        //Sound effect
        var randomSound = rng.randomBetween(1, 7);
        switch(randomSound)
        {
          case 1: gameState.playSoundEffect("grind-1.wav", 1); break;
          case 2: gameState.playSoundEffect("grind-2.wav", 1); break;
          case 3: gameState.playSoundEffect("grind-3.wav", 1); break;
          case 4: gameState.playSoundEffect("grind-4.wav", 1); break;
          case 5: gameState.playSoundEffect("grind-5.wav", 1); break;
          case 6: gameState.playSoundEffect("grind-6.wav", 1); break;
        }
        //It gets discarded
        trash.addCard(selectedDiscard);
        //Log
        var userNames = gameState.getPlayerNickNames();
        gameState.writeToGameLog(qsTranslate("coffeegrinder", "<font color=\"paleturquoise\">The %1 discarded the %2 from %3's hand.</font>").arg(card.getName()).arg(selectedDiscard.getName()).arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
        //A card is drawn for the player
        gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "paleturquoise");
        //An informational message is displayed (it will disappear automatically)
        menu.showInformation(qsTranslate("coffeegrinder", "You have drawn an item!"));
        //Crop
        card.setShowFullSize(0);
      }
    }
    else
    {
      menu.showInformation(qsTranslate("coffeegrinder", "No items in hand to discard!"));
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
