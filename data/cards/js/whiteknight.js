//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    if(mainInventory.getAllCards().length > 0)
    {
      if(boardutils.limitedByPerfume() == false)
      {
        //All empty tiles are fine
        return boardutils.getEmptyTiles();
      }
      else
      {
        return boardutils.getTilesAbovePerfume();
      }
    }
    else
    {
      var array = new Array();
      return array;
    }
  }
  else
  {
    var array = new Array();
    return array;
  }
}

function event(eventData)
{
  if(eventData[0] == "turnStarted")
  {
    card.setShowFullSize(1)
  }

  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());
    //A card is drawn for the player
    gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "bisque");
    //An informational message is displayed (it will disappear automatically)
    menu.showInformation(qsTranslate("whiteknight", "You have drawn an item!"));
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    if(boardutils.checkForImmovable(card) == false)
    {
      if(boardutils.getPossibleKnightMoves(card.getPosition()).length > 0)
      {
        //Ask the player where to move the knight
        menu.showInformation(qsTranslate("whiteknight", "Choose where to move the %1").arg(card.getName()), -1);
        //The player chooses one from the good positions
        selectedPos = boardutils.tileChoice(boardutils.getPossibleKnightMoves(card.getPosition()), 1);
        //We got the choice, hide the info
        menu.hideInformation();
        //Unless canceled
        if(selectedPos.x != -1)
        {
          //Log
          boardutils.logSilverActivation(card.getName());
          //Sound effect
          var randomSound = rng.randomBetween(1, 7);
          switch(randomSound)
          {
            case 1: gameState.playSoundEffect("knight-1.wav", 1); break;
            case 2: gameState.playSoundEffect("knight-2.wav", 1); break;
            case 3: gameState.playSoundEffect("knight-3.wav", 1); break;
            case 4: gameState.playSoundEffect("knight-4.wav", 1); break;
            case 5: gameState.playSoundEffect("knight-5.wav", 1); break;
            case 6: gameState.playSoundEffect("knight-6.wav", 1); break;
          }
          //Move the card
          board.moveCard(card.getPosition(), selectedPos);
          //Crop
          card.setShowFullSize(0);
          //Log
          gameState.writeToGameLog(qsTranslate("whiteknight", "<font color=\"paleturquoise\">The %1 has been moved to %2%3.</font>").arg(card.getName()).arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1), [1, 2, 3, 4], 1, 0);
        }
      }
      else
      {
        menu.showInformation(qsTranslate("whiteknight", "Nowhere to move!"));
      }
    }
    else
    {
      menu.showInformation(qsTranslate("whiteknight", "You cannot move this item!"));
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}


//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
