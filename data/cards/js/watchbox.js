//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums
//@include jsutils.patch
//@include jsutils.point


//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  var array = new Array();
  var firstWatchboxCondition = false;
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    if(trash.getAllCards().length > 0)
    {
      var trashCards = trash.getAllCards();
      for(i = 0; i < trashCards.length; i++)
      {
        if(trashCards[i].hasIcon("tcan") == false)
        {
          firstWatchboxCondition = true;
        }
      }
    }
    if(firstWatchboxCondition == true)
    {
      var arrayNumber = 0;
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            for(ttcp = 2; ttcp < 10; ttcp = ttcp+2)
            {
              var xCorri = 0;
              var yCorri = 0;
              if(ttcp == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
              if(ttcp == 4 && xVaria > 0) {xCorri = -1;}
              if(ttcp == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
              if(ttcp == 8 && yVaria > 0) {yCorri = -1;}
              boxedTile = Point(xVaria+xCorri,yVaria+yCorri);

              if(boxedTile != 0 && (xCorri != 0 || yCorri != 0) && board.getCardAtPosition(boxedTile) == null && board.isDisabledTile(boxedTile) == false)
              {
                array[arrayNumber] = new Point(xVaria,yVaria);
                arrayNumber++;
                break;
              }
            }
          }
        }
      }
    }
  }
  return array;
}


//This function handles all events
function event(eventData)
{
  //Handle the cardPlacedOnBoard event
  //Check if this card is the one that was placed
  //If it is, then execute the bronze action
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();

    menu.showInformation(qsTranslate("watchbox", "Choose an item from the trash"), -1);
    do
    {
      selectedForWatchbox = trash.cardChoice(trash.getAllCards(), 0, -1);
    }
    while(selectedForWatchbox.hasIcon("tcan") == true);
    menu.hideInformation();
    
    menu.showInformation(qsTranslate("watchbox", "Choose where to play the %1").arg(selectedForWatchbox.getName()), -1);
    selectedPos = boardutils.tileChoice(boardutils.getAdjacentEmptyTiles(card.getPosition()), 0);
    menu.hideInformation();
    
    //Log
    boardutils.logCardPlacement(card.getName());
    gameState.writeToGameLog(qsTranslate("watchbox", "<font color=\"bisque\">The %1 played the %2 from the trash to %3%4 and disabled it.</font>").arg(card.getName()).arg(selectedForWatchbox.getName()).arg(boardutils.convertToABC(selectedPos.x)).arg(selectedPos.y+1), [1, 2, 3, 4], 1, 0);
    selectedForWatchbox.changeOwner(card.getPlayer());
    trash.moveCardToBoard(selectedForWatchbox.getUID(), selectedPos, 0);
    selectedForWatchbox.setGrayScale(1);
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
