//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions()
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    if(boardutils.getEmptyTiles().length > 0)
    {
      //Check for hands not being empty
      if((gameState.getCurrentPlayer() == 1 && p1Inventory.isEmpty() == 0) || (gameState.getCurrentPlayer() == 2 && p2Inventory.isEmpty() == 0) || (gameState.getCurrentPlayer() == 3 && p3Inventory.isEmpty() == 0) || (gameState.getCurrentPlayer() == 4 && p1Inventory.isEmpty() == 0))
      {
        if(boardutils.checkForImmovable(card) == false)
        {
          //Ask the player which card to discard
          menu.showInformation(qsTranslate("emptycan", "Choose an item to discard"), -1);
          //The player chooses one
          switch(gameState.getCurrentPlayer())
          {
            case 1: selectedDiscard = p1Inventory.cardChoice(p1Inventory.getAllCards(), 1, -1); break;
            case 2: selectedDiscard = p2Inventory.cardChoice(p2Inventory.getAllCards(), 1, -1); break;
            case 3: selectedDiscard = p3Inventory.cardChoice(p3Inventory.getAllCards(), 1, -1); break;
            case 4: selectedDiscard = p4Inventory.cardChoice(p4Inventory.getAllCards(), 1, -1); break;
          }
          menu.hideInformation();
          //Unless canceled
          if(selectedDiscard != null)
          {
            switch(gameState.getCurrentPlayer())
            {
              case 1: p1Inventory.removeCard(selectedDiscard.getUID()); break;
              case 2: p2Inventory.removeCard(selectedDiscard.getUID()); break;
              case 3: p3Inventory.removeCard(selectedDiscard.getUID()); break;
              case 4: p4Inventory.removeCard(selectedDiscard.getUID()); break;
            }
            //It gets discarded
            trash.addCard(selectedDiscard);
            //Ask the player where to move the can
            menu.showInformation(qsTranslate("emptycan", "Choose where to move the %1").arg(card.getName()), -1);
            //The player chooses one from the good positions
            selectedPos = boardutils.tileChoice(boardutils.getEmptyTiles(), 0);
            //We got the choice, hide the info
            menu.hideInformation();
            //Store the previous position for the log
            var prevPosX = card.getPosX();
            var prevPosY = card.getPosY();
            //Sound effect
            var randomSound = rng.randomBetween(1, 6);
            switch(randomSound)
            {
              case 1: gameState.playSoundEffect("can-1.wav", 1); break;
              case 2: gameState.playSoundEffect("can-2.wav", 1); break;
              case 3: gameState.playSoundEffect("can-3.wav", 1); break;
              case 4: gameState.playSoundEffect("can-4.wav", 1); break;
              case 5: gameState.playSoundEffect("can-5.wav", 1); break;
            }
            //Move the card
            board.moveCard(card.getPosition(), selectedPos);
            //Log
            boardutils.logSilverActivation(card.getName());
            gameState.writeToGameLog(qsTranslate("emptycan", "<font color=\"paleturquoise\">The %1 discarded the %2.</font>").arg(card.getName()).arg(selectedDiscard.getName()), [1, 2, 3, 4], 1, 0);
            gameState.writeToGameLog(qsTranslate("emptycan", "<font color=\"paleturquoise\">The %1 has been moved to %2%3.</font>").arg(card.getName()).arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1), [1, 2, 3, 4], 1, 0);
          }
        }
        else
        {
          menu.showInformation(qsTranslate("emptycan", "You cannot move this item!"));
        }
      }
      else
      {
        menu.showInformation(qsTranslate("emptycan", "No items in hand to discard!"));
      }
    }
    else
    {
      menu.showInformation(qsTranslate("emptycan", "Nowhere to move!"));
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
