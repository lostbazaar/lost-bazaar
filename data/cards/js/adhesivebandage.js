//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums
//@include jsutils.patch
//@include jsutils.point


//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    if(trash.getAllCards().length > 0)
    {
      var arrayNumber = 0;
      for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
      {
        for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
        {
          if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
          {
            var dominoCondition = false;
            //Checking for domino condition
            for(ttcp = 2; ttcp < 10; ttcp = ttcp+2)
            {
              var xCorri = 0;
              var yCorri = 0;
              if(ttcp == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
              if(ttcp == 4 && xVaria > 0) {xCorri = -1;}
              if(ttcp == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
              if(ttcp == 8 && yVaria > 0) {yCorri = -1;}
              TTCP = Point(xVaria+xCorri,yVaria+yCorri);
              CTCP = board.getCardAtPosition(TTCP);
              if(CTCP != null && CTCP.getPlayer() == gameState.getCurrentPlayer())
              {
                dominoCondition = true;
                break;
              }
            }

            if(dominoCondition == true)
            {
              var patchedTile = 0;
              switch(gameState.getCurrentPlayer())
              {
                case 1:
                  if(board.getCardAtPosition(Point(xVaria,yVaria+1)) == null && yVaria < board.getHeight()-1)
                  {
                    patchedTile = Point(xVaria,yVaria+1);
                  }
                  break;
                case 2:
                  if(board.getCardAtPosition(Point(xVaria,yVaria-1)) == null && yVaria > 0)
                  {
                    patchedTile = Point(xVaria,yVaria-1);
                  }
                  break;
                case 3:
                  if(board.getCardAtPosition(Point(xVaria-1,yVaria)) == null && xVaria > 0)
                  {
                    patchedTile = Point(xVaria-1,yVaria);
                  }
                  break;
                case 4:
                  if(board.getCardAtPosition(Point(xVaria+1,yVaria)) == null && xVaria < board.getWidth()-1)
                  {
                    patchedTile = Point(xVaria+1,yVaria);
                  }
                  break;
              }
              if(patchedTile != 0 && board.getCardAtPosition(patchedTile) == null && board.isDisabledTile(patchedTile) == false)
              {
                var trashCards = trash.getAllCards();
                for(i = 0; i < trashCards.length; i++)
                {
                  if(patchutils.secondBandageCondition(trashCards[i], patchedTile.x, patchedTile.y) == true)
                  {
                    array[arrayNumber] = new Point(xVaria,yVaria);
                    arrayNumber++;
                    break;
                  }
                }
              }
            }


          }
        }
      }
    }
  }
  return array;
}


//This function handles all events
function event(eventData)
{
  //Handle the cardPlacedOnBoard event
  //Check if this card is the one that was placed
  //If it is, then execute the bronze action
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    var patchedTile = 0;
    switch(card.getPlayer())
    {
      case 1:
        if(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()+1)) == null && card.getPosY() < board.getHeight()-1)
        {
          patchedTile = Point(card.getPosX(),card.getPosY()+1);
        }
        break;
      case 2:
        if(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()-1)) == null && card.getPosY() > 0)
        {
          patchedTile = Point(card.getPosX(),card.getPosY()-1);
        }
        break;
      case 3:
        if(board.getCardAtPosition(Point(card.getPosX()-1,card.getPosY())) == null && card.getPosX() > 0)
        {
          patchedTile = Point(card.getPosX()-1,card.getPosY());
        }
        break;
      case 4:
        if(board.getCardAtPosition(Point(card.getPosX()+1,card.getPosY())) == null && card.getPosX() < board.getWidth()-1)
        {
          patchedTile = Point(card.getPosX()+1,card.getPosY());
        }
        break;
    }

    menu.showInformation(qsTranslate("adhesivebandage", "Choose an item from the trash"), -1);
    do
    {
      selectedForAdhesive = trash.cardChoice(trash.getAllCards(), 0, -1);
    }
    while(patchutils.secondBandageCondition(selectedForAdhesive, patchedTile.x, patchedTile.y) == false);

    menu.hideInformation();
    //Log
    boardutils.logCardPlacement(card.getName());
    gameState.writeToGameLog(qsTranslate("adhesivebandage", "<font color=\"bisque\">The %1 played the %2 from the trash to %3%4.</font>").arg(card.getName()).arg(selectedForAdhesive.getName()).arg(boardutils.convertToABC(patchedTile.x)).arg(patchedTile.y+1), [1, 2, 3, 4], 1, 0);
    selectedForAdhesive.changeOwner(card.getPlayer());
    trash.moveCardToBoard(selectedForAdhesive.getUID(), patchedTile, 1);
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
