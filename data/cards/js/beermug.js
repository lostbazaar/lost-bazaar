//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() != eventData[1] && gameState.getCurrentPlayer() == card.getPlayer() && card.isGrayScale() == false)
  {
    var EBC = (gameState.cardForUID(eventData[1])).getBaseValue();
    if(EBC > 0)
    {
      card.setCounter(card.getCounter()+EBC);
      //Log
      if(EBC > 1)
      {
        gameState.writeToGameLog(qsTranslate("beermug", "<font color=\"khaki\">%1%2 %3 gained %4 counters.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()).arg(EBC), [1, 2, 3, 4], 1, 0);
      }
      else
      {
        gameState.writeToGameLog(qsTranslate("beermug", "<font color=\"khaki\">%1%2 %3 gained 1 counter.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()), [1, 2, 3, 4], 1, 0);
      }
    }
  }

  if(card.getCounter() > 6 && card.isGrayScale() == false)
  {
    card.setGrayScale(1);
    gameState.writeToGameLog(qsTranslate("beermug", "<font color=\"khaki\">The %1 has been disabled.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    cards = board.getAllCards(); //Retrieve all cards on the board
    tilesOK = []; //This will store the tiles of the cards which can be moved
    if(card.getCounter() == 6)
    {
      for(i = 0; i < cards.length; i++) //Check all cards
      {
        if(boardutils.checkForUnrotatable(cards[i]) == false && boardutils.isUntargetableBySilver(cards[i]) == false && cards[i].getCurrentValue() == 1)
        {
          tilesOK.push(cards[i].getPosition());
        }
      }
    }

    if(tilesOK.length > 0)
    {
      menu.showInformation(qsTranslate("beermug", "Choose which card would you like to rotate"), -1);
      selectedTile = boardutils.tileChoice(tilesOK, 1);
      menu.hideInformation();
      //Unless canceled
      if(selectedTile.x != -1)
      {
        selectedCard = board.getCardAtPosition(selectedTile)
        card.setCounter(card.getCounter()-6);
        var prevOwner = selectedCard.getPlayer();
        if(prevOwner == 1) {selectedCard.changeOwner(2);}
        if(prevOwner == 2) {selectedCard.changeOwner(1);}
        if(prevOwner == 3) {selectedCard.changeOwner(4);}
        if(prevOwner == 4) {selectedCard.changeOwner(3);}

        //Log
        gameState.writeToGameLog(qsTranslate("beermug", "<font color=\"paleturquoise\">The %1 cast 6 counters.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
        gameState.writeToGameLog(qsTranslate("beermug", "<font color=\"paleturquoise\">The %1 rotated %2%3 %4 by 180°.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);
      }
    }
    else
    {
      menu.showInformation(qsTranslate("beermug", "You cannot use this ability!"));
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
