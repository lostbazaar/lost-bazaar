//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "turnStarted")
  {
    card.setShowFullSize(1);
  }

  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    //Ability can be activated if there is an unoccupied tile above
    if(
      (gameState.getCurrentPlayer() == 1 && card.getPosY() > 0 && board.getCardAtPosition(Point(card.getPosX(),card.getPosY()-1)) == null) && !board.isDisabledTile(Point(card.getPosX(),card.getPosY()-1)) ||
      (gameState.getCurrentPlayer() == 2 && card.getPosY() < board.getHeight()-1 && board.getCardAtPosition(Point(card.getPosX(),card.getPosY()+1)) == null) && !board.isDisabledTile(Point(card.getPosX(),card.getPosY()+1)) ||
      (gameState.getCurrentPlayer() == 3 && card.getPosX() < board.getWidth()-1 && board.getCardAtPosition(Point(card.getPosX()+1,card.getPosY())) == null) && !board.isDisabledTile(Point(card.getPosX()+1,card.getPosY())) ||
      (gameState.getCurrentPlayer() == 4 && card.getPosX() > 0 && board.getCardAtPosition(Point(card.getPosX()-1,card.getPosY())) == null && !board.isDisabledTile(Point(card.getPosX()-1,card.getPosY())))
      )
    {
      if(boardutils.checkForImmovable(card) == false)
      {
        //Log
        boardutils.logSilverActivation(card.getName());
        //Sound effect
        var randomSound = rng.randomBetween(1, 7);
        switch(randomSound)
        {
          case 1: gameState.playSoundEffect("pawn-1.wav", 1); break;
          case 2: gameState.playSoundEffect("pawn-2.wav", 1); break;
          case 3: gameState.playSoundEffect("pawn-3.wav", 1); break;
          case 4: gameState.playSoundEffect("pawn-4.wav", 1); break;
          case 5: gameState.playSoundEffect("pawn-5.wav", 1); break;
          case 6: gameState.playSoundEffect("pawn-6.wav", 1); break;
        }
        //Move the card
        switch(gameState.getCurrentPlayer())
        {
          case 1:
            board.moveCard(card.getPosition(), Point(card.getPosX(),card.getPosY()-1));
            break;
          case 2:
            board.moveCard(card.getPosition(), Point(card.getPosX(),card.getPosY()+1));
            break;
          case 3:
            board.moveCard(card.getPosition(), Point(card.getPosX()+1,card.getPosY()));
            break;
          case 4:
            board.moveCard(card.getPosition(), Point(card.getPosX()-1,card.getPosY()));
            break;
        }
        //Log
        gameState.writeToGameLog(qsTranslate("whitepawn", "<font color=\"paleturquoise\">The %1 has been moved to %2%3.</font>").arg(card.getName()).arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1), [1, 2, 3, 4], 1, 0);

        if(card.getCounter() < 12)
        {
          card.setCounter(card.getCounter()+1);
          //Log
          gameState.writeToGameLog(qsTranslate("whitepawn", "<font color=\"paleturquoise\">The %1 gained 1 counter.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
        }
        //Crop
        card.setShowFullSize(0)

      }
      else
      {
        menu.showInformation(qsTranslate("whitepawn", "You cannot move this item!"));
      }
    }
    else
    {
      menu.showInformation(qsTranslate("whitepawn", "Nowhere to move!"));
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.complexCurrentValue();
}
