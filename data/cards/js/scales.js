//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    if((gameState.getCurrentPlayer() == 1 && p1Inventory.getAllCards().length > 0) || (gameState.getCurrentPlayer() == 2 && p2Inventory.getAllCards().length > 0) || (gameState.getCurrentPlayer() == 3 && p3Inventory.getAllCards().length > 0) || (gameState.getCurrentPlayer() == 4 && p4Inventory.getAllCards().length > 0))
    {
      cards = board.getAllCards(); //Retrieve all cards on the board
      posOK = []; //This will store the tile of the possible targets
      tilesOK = []; //This will store the tile of the targets depending on the discard
      handValues = []; //This will store the values of the items in hand
      crossValues = []; //This will store the values of the items in the same line or column

      switch(gameState.getCurrentPlayer())
      {
        case 1:
          itemsInHands = p1Inventory.getAllCards();
          break;
        case 2:
          itemsInHands = p2Inventory.getAllCards();
          break;
        case 3:
          itemsInHands = p3Inventory.getAllCards();
          break;
        case 4:
          itemsInHands = p4Inventory.getAllCards();
          break;
      }
      for(i = 0; i < itemsInHands.length; i++)
      {
        handValues.push(itemsInHands[i].getBaseValue());
      }
      var highestValue = Math.max.apply(Math, handValues);
      var lowestValue = Math.min.apply(Math, handValues);

      for(i = 0; i < cards.length; i++) //Check all cards
      {
        //If the card is on a tile in the same row or column and not greyscale then it might be good
        if((card.getPosX() == cards[i].getPosX() || card.getPosY() == cards[i].getPosY()) && cards[i].isGrayScale() == false)
        {
          //If there is an item in hand with a value that is greater or equal to the target's value
          if(cards[i].getCurrentValue() <= highestValue)
          {
            //If it is not the Scales itself then it is good
            if(cards[i].getUID() != card.getUID())
            {
              //If the card is not protected by a Bird feeder
              if(boardutils.isProtectedByBirdfeeder(cards[i]) == false)
              {
                //Store the coordinates of the good card
                posOK.push(cards[i].getPosition());
                crossValues.push(cards[i].getCurrentValue());
              }
            }
          }
        }
      }

      if(posOK.length > 0)
      {
        var crossMinValue = Math.min.apply(Math, crossValues);
        //Ask the player which card to discard
        menu.showInformation(qsTranslate("scales", "Choose an item to discard"), -1);
        //The player chooses one
        var selectedDiscard = 0;
        do
        {
          switch(gameState.getCurrentPlayer())
          {
            case 1: selectedDiscard = p1Inventory.cardChoice(p1Inventory.getAllCards(), 1, -1); break;
            case 2: selectedDiscard = p2Inventory.cardChoice(p2Inventory.getAllCards(), 1, -1); break;
            case 3: selectedDiscard = p3Inventory.cardChoice(p3Inventory.getAllCards(), 1, -1); break;
            case 4: selectedDiscard = p4Inventory.cardChoice(p4Inventory.getAllCards(), 1, -1); break;
          }
        }
        while(selectedDiscard != null && selectedDiscard.getBaseValue() < crossMinValue);

        menu.hideInformation();
        //Unless canceled
        if(selectedDiscard != null)
        {
          var weightValue = selectedDiscard.getBaseValue();
          switch(gameState.getCurrentPlayer())
          {
            case 1: p1Inventory.removeCard(selectedDiscard.getUID()); break;
            case 2: p2Inventory.removeCard(selectedDiscard.getUID()); break;
            case 3: p3Inventory.removeCard(selectedDiscard.getUID()); break;
            case 4: p4Inventory.removeCard(selectedDiscard.getUID()); break;
          }
          trash.addCard(selectedDiscard);

          //Log
          boardutils.logSilverActivation(card.getName());
          gameState.writeToGameLog(qsTranslate("scales", "<font color=\"paleturquoise\">The %1 discarded the %2.</font>").arg(card.getName()).arg(selectedDiscard.getName()), [1, 2, 3, 4], 1, 0);
          for(i = 0; i < posOK.length; i++)
          {
            if(board.getCardAtPosition(posOK[i]).getCurrentValue() <= weightValue)
            {
              tilesOK.push(posOK[i]);
            }
          }

          //Ask the player which card's ability to erase
          menu.showInformation(qsTranslate("scales", "Choose target"), -1);
          //The player chooses one from the good cards
          selectedTile = boardutils.tileChoice(tilesOK, 0);
          //Hide the 'Choose target' label
          menu.hideInformation();
          selectedCard = board.getCardAtPosition(selectedTile);
          selectedCard.setGrayScale(1);

          //Log
          gameState.writeToGameLog(qsTranslate("scales", "<font color=\"paleturquoise\">The %1 disabled %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);

        }
      }
      else
      {
        menu.hideInformation();
        menu.showInformation(qsTranslate("scales", "No item is selectable for ability!"));
      }
    }
    else
    {
      menu.showInformation(qsTranslate("scales", "You cannot use this ability!"));
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
