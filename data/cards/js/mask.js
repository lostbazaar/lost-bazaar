//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  tilesOK = [Point(card.getPosX(),card.getPosY())];
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    eT = boardutils.getEmptyTiles();
    if(boardutils.isHalted(eT) == false && board.isDoubleTile(card.getPosition()) == false)
    {
      if(boardutils.checkForImmovable(card) == false)
      {
        for(ttc = 2; ttc < 10; ttc = ttc + 2)
        {
          xCorri = 0;
          yCorri = 0;
          if(ttc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
          if(ttc == 4 && card.getPosX() > 0) {xCorri = -1;}
          if(ttc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
          if(ttc == 8 && card.getPosY() > 0) {yCorri = -1;}
          CTC = board.getCardAtPosition(Point(card.getPosX()+xCorri,card.getPosY()+yCorri));
          if(CTC != null && CTC.getUID() != card.getUID() && CTC.getPlayer() == card.getPlayer() && boardutils.checkForUnpickable(CTC) == false && boardutils.isUntargetableByBronze(CTC) == false)
          {
            tilesOK.push(Point(card.getPosX()+xCorri,card.getPosY()+yCorri))
          }
        }
      }
      else
      {
        menu.showInformation(qsTranslate("mask", "You cannot move this item!"));
      }
      if(tilesOK.length > 1)
      {
        menu.showInformation(qsTranslate("mask", "Choose target (click on the %1 to dismiss it)").arg(card.getName()), -1);
        selectedTile = boardutils.tileChoice(tilesOK, 0);
        menu.hideInformation();
        selectedCard = board.getCardAtPosition(selectedTile);
        //Log
        boardutils.logCardPlacement(card.getName());
        if(selectedCard != card)
        {
          board.removeCard(selectedCard.getUID());
          if(card.getPlayer() == 1) {p1Inventory.addCard(selectedCard);}
          if(card.getPlayer() == 2) {p2Inventory.addCard(selectedCard);}
          if(card.getPlayer() == 3) {p3Inventory.addCard(selectedCard);}
          if(card.getPlayer() == 4) {p4Inventory.addCard(selectedCard);}
          board.moveCard(card.getPosition(), selectedTile);
          //Log
          var userNames = gameState.getPlayerNickNames();
          gameState.writeToGameLog(qsTranslate("mask", "<font color=\"bisque\">The %1 returned %2%3 %4 to %5's hand.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()).arg(userNames[selectedCard.getPlayer()-1]), [1, 2, 3, 4], 1, 0);
          gameState.writeToGameLog(qsTranslate("mask", "<font color=\"bisque\">The %1 has been moved to %2%3.</font>").arg(card.getName()).arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1), [1, 2, 3, 4], 1, 0);
        }
      }
      else
      {
        menu.showInformation(qsTranslate("mask", "No available target for this ability!"));
        //Log
        boardutils.logCardPlacement(card.getName());
      }
    }
    else
    {
      card.setGrayScale(1);
      //Log
      boardutils.logCardPlacement(card.getName());
      gameState.writeToGameLog(qsTranslate("mask", "The %1 lost its abilities.").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.complexCurrentValue();
}
