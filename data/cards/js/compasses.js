//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    var arrayNumber = 0;
    cards = board.getAllCards();
    for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
    {
      for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
      {
        var dominoRuleFulfilled = false;
        var conditionFulfilled = false;
        if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
        {
          for(i = 0; i < 4; i++)
          {
            switch(i)
            {
              case 0:
                xVariab = xVaria+1
                yVariab = yVaria
                break;
              case 1:
                xVariab = xVaria
                yVariab = yVaria+1
                break;
              case 2:
                xVariab = xVaria-1
                yVariab = yVaria
                break;
              case 3:
                xVariab = xVaria
                yVariab = yVaria-1
                break;
            }
            if(dominoRuleFulfilled == false && board.getCardAtPosition(Point(xVariab,yVariab)) != null && (board.getCardAtPosition(Point(xVariab,yVariab))).getPlayer() == gameState.getCurrentPlayer())
            {
              dominoRuleFulfilled = true;
            }

            if(conditionFulfilled == false && board.getCardAtPosition(Point(xVariab,yVariab)) != null && (board.getCardAtPosition(Point(xVariab,yVariab))).getPlayer() != gameState.getCurrentPlayer() && (board.getCardAtPosition(Point(xVariab,yVariab))).getBackground() == "wood" && (board.getCardAtPosition(Point(xVariab,yVariab))).getCurrentValue() <= 4 && boardutils.isUntargetableByBronze(board.getCardAtPosition(Point(xVariab,yVariab))) == false)
            {
              if(boardutils.checkForUnrotatable(board.getCardAtPosition(Point(xVariab,yVariab))) == false)
              {
                conditionFulfilled = true;
              }
            }
          }

          if(dominoRuleFulfilled && conditionFulfilled)
          {
            array[arrayNumber] = new Point(xVaria,yVaria);
            arrayNumber++
          }
        }
      }
    }
    return array;
  }
  else
  {
    return array;
  }
}

//This function handles all events
function event(eventData)
{
  //Handle the cardPlacedOnBoard event
  //Check if this card is the one that was placed
  //If it is, then execute the bronze action
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    cards = board.getAllCards(); //Retrieve all cards on the board
    tilesOK = []; //This will store the tiles of cards which can be rotated
    for(i = 0; i < cards.length; i++) //Check all cards
    {
      //If the card is on an adjacent tile then it might be good
      if(Math.pow(card.getPosX()-cards[i].getPosX(), 2) + Math.pow(card.getPosY()-cards[i].getPosY(), 2) == 1)
      {
        //If the card is wooden and not yours then it might be good
        if(cards[i].getBackground() == "wood" && cards[i].getPlayer() != gameState.getCurrentPlayer())
        {
          //If the card is not on safe tile then it might be good
          if(boardutils.checkForUnrotatable(cards[i]) == false)
          {
            //If the card has a value of 4 or less then it might be good
            if(cards[i].getCurrentValue() <= 4)
            {
              if(boardutils.isUntargetableByBronze(cards[i]) == false)
              {
                tilesOK.push(cards[i].getPosition());
              }
            }
          }
        }
      }
    }
    if(tilesOK.length > 0)
    {
      //Ask the player which card would he like to rotate
      menu.showInformation(qsTranslate("compasses", "Choose target"), -1);
      //The player chooses one from the good cards
      selectedTile = boardutils.tileChoice(tilesOK, 0);
      selectedCard = board.getCardAtPosition(selectedTile);
      //Hide the 'Choose which card...' label
      menu.hideInformation();
      //Sound effect
      var randomSound = rng.randomBetween(1, 7);
      switch(randomSound)
      {
        case 1: gameState.playSoundEffect("circledraw-1.wav", 1); break;
        case 2: gameState.playSoundEffect("circledraw-2.wav", 1); break;
        case 3: gameState.playSoundEffect("circledraw-3.wav", 1); break;
        case 4: gameState.playSoundEffect("circledraw-4.wav", 1); break;
        case 5: gameState.playSoundEffect("circledraw-5.wav", 1); break;
        case 6: gameState.playSoundEffect("circledraw-6.wav", 1); break;
      }
      selectedCard.changeOwner(gameState.getCurrentPlayer());
    }
    //Logging
    boardutils.logCardPlacement(card.getName());
    var userNames = gameState.getPlayerNickNames();
    gameState.writeToGameLog(qsTranslate("compasses", "<font color=\"bisque\">The %1 rotated %2%3 %4 towards %5.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()).arg(userNames[selectedCard.getPlayer()-1]), [1, 2, 3, 4], 1, 0);
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
