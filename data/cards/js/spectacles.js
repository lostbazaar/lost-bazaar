//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.dominoEntryConditions();
}

//This function handles all events
function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    if(gameState.getGameMode() > 0 && gameState.getGameMode() < 4)
    {
      viewablePlayers = [] //This will store the possible players for the first choice
      if(gameState.getCurrentPlayer() == 2 || (gameState.getCurrentPlayer() == 3 && gameState.getGameMode() != 1) || gameState.getCurrentPlayer() == 4)
      {
        viewablePlayers.push(1);
      }
      if((gameState.getCurrentPlayer() == 1) || gameState.getCurrentPlayer() == 3 || (gameState.getCurrentPlayer() == 4 && gameState.getGameMode() != 1))
      {
        viewablePlayers.push(2);
      }
      if((gameState.getCurrentPlayer() == 1 && gameState.getGameMode() != 1) || gameState.getCurrentPlayer() == 2 || gameState.getCurrentPlayer() == 4)
      {
        viewablePlayers.push(3);
      }
      if((gameState.getCurrentPlayer() == 1 && gameState.getGameMode() != 2) || (gameState.getCurrentPlayer() == 2 && gameState.getGameMode() == 3) || (gameState.getCurrentPlayer() == 3 && gameState.getGameMode() != 2))
      {
        viewablePlayers.push(4);
      }
      menu.showInformation(qsTranslate("spectacles", "Which player's hand would you like to view?"), -1);
      //Get the player's answer
      var choice = graphicsViewMenu.getPlayer(viewablePlayers, 0);
      //Now we have the choice, it's time to make the chosen player's inventory visible
      switch(choice)
      {
        case 1:
          p1Inventory.setViewable(true, true);
          break;
        case 2:
          p2Inventory.setViewable(true, true);
          break;
        case 3:
          p3Inventory.setViewable(true, true);
          break;
        case 4:
          p4Inventory.setViewable(true, true);
          break;
      }
      //Hide the 'Which player's inventory...' label
      menu.hideInformation();
    }
    else
    {
      //1v1 opposing
      if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 1)
      {
        p2Inventory.setViewable(true, true);
        var choice = 2;
      }
      if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 2)
      {
        p1Inventory.setViewable(true, true);
        var choice = 1;
      }
      //1v1 adjacent
      if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 1)
      {
        p3Inventory.setViewable(true, true);
        var choice = 3;
      }
      if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 3)
      {
        p1Inventory.setViewable(true, true);
        var choice = 1;
      }
    }

    var selectedToPlace = 0;
    var handToExamine = [];
    var hasAllowedItem = false;
    switch(gameState.getCurrentPlayer())
    {
      case 1: handToExamine = p1Inventory.getAllCards(); break;
      case 2: handToExamine = p2Inventory.getAllCards(); break;
      case 3: handToExamine = p3Inventory.getAllCards(); break;
      case 4: handToExamine = p4Inventory.getAllCards(); break;
    }
    for(i = 0; i < handToExamine.length; i++)
    {
      if(handToExamine[i].getBaseValue() < 3 && boardutils.secondSpectaclesCondition(handToExamine[i].getID()) == true)
      {
        if(handToExamine[i].hasIcon("dmno"))
        {
        }
        else
        {
          hasAllowedItem = true;
          break;
        }
      }
    }

    //The second part of the entry ability is conditional, check for available tiles before executing it
    if(boardutils.getNonAdjacentEmptyTiles().length > 0 && boardutils.limitedByPerfume() == false && hasAllowedItem == true)
    {
      //Ask the player which card to place
      menu.showInformation(qsTranslate("spectacles", "Choose an item to play").arg(card.getName()), -1);

      //The player chooses one
      if(gameState.getCurrentPlayer() == 1)
      {
        var isAllowedToPlace = p1Inventory.getAllCards()
        for(i = 0; i < p1Inventory.getAllCards().length; i++)
        {
          do
          {
            selectedToPlace = p1Inventory.cardChoice(p1Inventory.getAllCards(), 0, 1);
          }
          while(selectedToPlace.getBaseValue() > 2 || selectedToPlace.hasIcon("dmno") == true || boardutils.secondSpectaclesCondition(selectedToPlace.getID()) == false);
          break;
        }
      }

      if(gameState.getCurrentPlayer() == 2)
      {
        var isAllowedToPlace = p2Inventory.getAllCards()
        for(i = 0; i < p2Inventory.getAllCards().length; i++)
        {
          do
          {
            selectedToPlace = p2Inventory.cardChoice(p2Inventory.getAllCards(), 0, 1);
          }
          while(selectedToPlace.getBaseValue() > 2 || selectedToPlace.hasIcon("dmno") == true || boardutils.secondSpectaclesCondition(selectedToPlace.getID()) == false);
          break;
        }
      }

      if(gameState.getCurrentPlayer() == 3)
      {
        var isAllowedToPlace = p3Inventory.getAllCards()
        for(i = 0; i < p3Inventory.getAllCards().length; i++)
        {
          do
          {
            selectedToPlace = p3Inventory.cardChoice(p3Inventory.getAllCards(), 0, 1);
          }
          while(selectedToPlace.getBaseValue() > 2 || selectedToPlace.hasIcon("dmno") == true || boardutils.secondSpectaclesCondition(selectedToPlace.getID()) == false);
          break;
        }
      }

      if(gameState.getCurrentPlayer() == 4)
      {
        var isAllowedToPlace = p4Inventory.getAllCards()
        for(i = 0; i < p4Inventory.getAllCards().length; i++)
        {
          do
          {
            selectedToPlace = p4Inventory.cardChoice(p4Inventory.getAllCards(), 0, 1);
          }
          while(selectedToPlace.getBaseValue() > 2 || selectedToPlace.hasIcon("dmno") == true || boardutils.secondSpectaclesCondition(selectedToPlace.getID()) == false);
          break;
        }
      }

      if(selectedToPlace != 0)
      {
        menu.hideInformation();
        menu.showInformation(qsTranslate("spectacles", "Choose where to play the %1").arg(selectedToPlace.getName()), -1);
        //The player chooses one from the good positions

        switch(selectedToPlace.getID())
        {

          case "wrench":

            wrenchAllowedEverywhere = false;
            var trashCards = trash.getAllCards();
            if(trashCards.length > 0)
            {
              for(i = 0; i < trashCards.length; i++)
              {
                if(trashCards[i].getBackground() == "metal")
                {
                  wrenchAllowedEverywhere = true;
                  break;
                }
              }
            }

            if(wrenchAllowedEverywhere == false)
            {
              //First condition is not fulfilled, check for the availability of the second ability
              allowedPositions = new Array();
              cards = board.getAllCards();
              for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
              {
                for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
                {
                  if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false && (board.getCardAtPosition(Point(xVaria+1,yVaria)) == null) && (board.getCardAtPosition(Point(xVaria,yVaria+1)) == null) && (board.getCardAtPosition(Point(xVaria-1,yVaria)) == null) && (board.getCardAtPosition(Point(xVaria,yVaria-1)) == null))
                  {
                    for(j = 1; j < 9; j++)
                    {
                      var xCorri = 0;
                      var yCorri = 0;
                      if(j == 1) {xCorri = 1; yCorri = -2;}
                      if(j == 2) {xCorri = 2; yCorri = -1;}
                      if(j == 3) {xCorri = 2; yCorri = 1;}
                      if(j == 4) {xCorri = 1; yCorri = 2;}
                      if(j == 5) {xCorri = -1; yCorri = 2;}
                      if(j == 6) {xCorri = -2; yCorri = 1;}
                      if(j == 7) {xCorri = -2; yCorri = -1;}
                      if(j == 8) {xCorri = -1; yCorri = -2;}

                      var CTC = board.getCardAtPosition(Point(xVaria+xCorri,yVaria+yCorri));
                      if(CTC != null && CTC.getCurrentValue() <= 1 && boardutils.checkForUnrotatable(CTC) == false && boardutils.isUntargetableByBronze(CTC) == false)
                      {
                        allowedPositions.push(Point(xVaria,yVaria));
                      }
                    }
                  }
                }
              }
              selectedPos = boardutils.tileChoice(allowedPositions, 0);
            }
            else
            {
              selectedPos = boardutils.tileChoice((boardutils.getNonAdjacentEmptyTiles()), 0);
            }
            break;

          case "football":
            if(mainInventory.getAllCards().length > 1)
            {
              selectedPos = boardutils.tileChoice((boardutils.getNonAdjacentEmptyTiles()), 0);
            }
            else
            {
              allowedPositions = new Array();
              for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
              {
                for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
                {
                  if((board.getCardAtPosition(Point(xVaria,yVaria)) == null) && (board.getCardAtPosition(Point(xVaria+1,yVaria)) == null) && (board.getCardAtPosition(Point(xVaria,yVaria+1)) == null) && (board.getCardAtPosition(Point(xVaria-1,yVaria)) == null) && (board.getCardAtPosition(Point(xVaria,yVaria-1)) == null))
                  {
                    if(board.isDoubleTile(Point(xVaria,yVaria)) == true)
                    {
                      allowedPositions.push(Point(xVaria,yVaria));
                    }
                  }
                }
              }
              selectedPos = boardutils.tileChoice(allowedPositions, 0);
            }
            break;

          case "dart":

            allowedPositions = new Array();
            var nAET = boardutils.getNonAdjacentEmptyTiles();

            cards = board.getAllCards();
            for(j = 0; j < nAET.length; j++)
            {
              var entryCondition = false;
              var instantlyTrashable = true;
              var instantlyMovable = true;
              var xVaria = nAET[j].x;
              var yVaria = nAET[j].y;

              cardsCF = board.getAllCards();
              for(cfut = 0; cfut < cardsCF.length; cfut++)
              {
                if(board.isSafeTile(nAET[j]) == true)
                {
                  instantlyTrashable = false;
                  break;
                }

                if((cardsCF[cfut].getID() == "celticshield" || (cardsCF[cfut].getID() == "handmirror" && boardutils.itemMirrored(cardsCF[cfut]) == "celticshield")) && cardsCF[cfut].isGrayScale() == false && cardsCF[cfut].getPlayer() == gameState.getCurrentPlayer())
                {
                  var shieldDistance = Math.pow(cardsCF[cfut].getPosX()-xVaria, 2) + Math.pow(cardsCF[cfut].getPosY()-yVaria, 2)
                  if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
                  {
                    instantlyTrashable = false;
                    break;
                  }
                }

                if((cardsCF[cfut].getID() == "hardhat" || (cardsCF[cfut].getID() == "handmirror" && boardutils.itemMirrored(cardsCF[cfut]) == "hardhat")) && cardsCF[cfut].isGrayScale() == false)
                {
                  switch(cardsCF[cfut].getPlayer())
                  {
                    case 1:
                      if(cardsCF[cfut].getPosX() == xVaria && cardsCF[cfut].getPosY() == yVaria-1) {instantlyTrashable = false;}
                      break;
                    case 2:
                      if(cardsCF[cfut].getPosX() == xVaria && cardsCF[cfut].getPosY() == yVaria+1) {instantlyTrashable = false;}
                      break;
                    case 3:
                      if(cardsCF[cfut].getPosX() == xVaria+1 && cardsCF[cfut].getPosY() == yVaria) {instantlyTrashable = false;}
                      break;
                    case 4:
                      if(cardsCF[cfut].getPosX() == xVaria-1 && cardsCF[cfut].getPosY() == yVaria) {instantlyTrashable = false;}
                      break;
                  }
                }
              }

              if(board.isInnovativeTile(Point(xVaria,yVaria)) == false)
              {
                for(cfim = 0; cfim < cardsCF.length; cfim++)
                {
                  if((cardsCF[cfim].getID() == "gluetube" || (cardsCF[cfim].getID() == "handmirror" && boardutils.itemMirrored(cardsCF[cfim]) == "gluetube")) && cardsCF[cfim].isGrayScale() == false)
                  {
                    var tubeDistance = Math.pow(cardsCF[cfim].getPosX()-xVaria, 2) + Math.pow(cardsCF[cfim].getPosY()-yVaria, 2)
                    if(tubeDistance == 0 || tubeDistance == 1 || tubeDistance == 2)
                    {
                      instantlyMovable = false;
                      break;
                    }
                  }
                }
              }
              else
              {
                instantlyMovable = false;
              }

              if(instantlyTrashable == true)
              {
                for(xVariab = 0; xVariab < board.getWidth(); xVariab++)
                {
                  //If the target tile is not safe
                  var CTC = board.getCardAtPosition(Point(xVariab,yVaria));
                  if(CTC != null && boardutils.checkForUntrashable(CTC) == false && instantlyMovable == true)
                  {
                    entryCondition = true;
                    break;
                  }
                  if(CTC != null && boardutils.checkForUntrashable(CTC) == true && CTC.getCurrentValue() > 3)
                  {
                    entryCondition = true;
                    break;
                  }
                  if(CTC != null && instantlyMovable == false && CTC.getCurrentValue() > 3)
                  {
                    entryCondition = true;
                    break;
                  }
                }

                for(yVariab = 0; yVariab < board.getHeight(); yVariab++)
                {
                  var CTC = board.getCardAtPosition(Point(xVaria,yVariab));
                  if(CTC != null && boardutils.checkForUntrashable(CTC) == false && instantlyMovable == true)
                  {
                    entryCondition = true;
                    break;
                  }
                  if(CTC != null && boardutils.checkForUntrashable(CTC) == true && CTC.getCurrentValue() > 3)
                  {
                    entryCondition = true;
                    break;
                  }
                  if(CTC != null && instantlyMovable == false && CTC.getCurrentValue() > 3)
                  {
                    entryCondition = true;
                    break;
                  }
                }
              }

              if(instantlyTrashable == false && instantlyMovable == true)
              {
                //If the tile is safe, the only allowed targets are the ones with 100% likeliness to be hit
                for(xVariab = 0; xVariab < board.getWidth(); xVariab++)
                {
                  var CTC = board.getCardAtPosition(Point(xVariab,yVaria));
                  if(CTC != null && boardutils.checkForUntrashable(CTC) == false && ((CTC.getCurrentValue() <= 0 && boardutils.isSixFix(gameState.getCurrentPlayer()) == false) || (CTC.getCurrentValue() <= 3 && boardutils.isSixFix(gameState.getCurrentPlayer()) == true)))
                  {
                    entryCondition = true;
                  }

                  for(yVariab = 0; yVariab < board.getHeight(); yVariab++)
                  {
                    var CTC = board.getCardAtPosition(Point(xVaria,yVariab));
                    if(CTC != null && boardutils.checkForUntrashable(CTC) == false && ((CTC.getCurrentValue() <= 0 && boardutils.isSixFix(gameState.getCurrentPlayer()) == false) || (CTC.getCurrentValue() <= 3 && boardutils.isSixFix(gameState.getCurrentPlayer()) == true)))
                    {
                      entryCondition = true;
                    }
                  }
                  if(entryCondition == true)
                  {
                    break;
                  }
                }
              }

              if(entryCondition == true)
              {
                allowedPositions.push(Point(xVaria,yVaria));
              }
            }
            selectedPos = boardutils.tileChoice(allowedPositions, 0);
            break;

          case "broom":

            allowedPositions = new Array();
            var nAET = boardutils.getNonAdjacentEmptyTiles();

            cards = board.getAllCards();
            for(j = 0; j < nAET.length; j++)
            {
              var xVaria = nAET[j].x;
              var yVaria = nAET[j].y;
              var countMovableX = 0
              var countMovableY = 0
              var countImmovableX = 0
              var countImmovableY = 0
              var xDisab = 0
              var yDisab = 0

              for(xVariab = 0; xVariab < board.getWidth(); xVariab++)
              {
                if(board.isDisabledTile(Point(xVariab,yVaria)) == true)
                {
                  xDisab++;
                }
              }
              for(yVariab = 0; yVariab < board.getHeight(); yVariab++)
              {
                if(board.isDisabledTile(Point(xVaria,yVariab)) == true)
                {
                  yDisab++;
                }
              }

              for(k = 0; k < cards.length; k++)
              {
                if(cards[k].getPosX() == xVaria)
                {
                  if(boardutils.checkForImmovable(cards[k]) == true || boardutils.isUntargetableByBronze(cards[k]) == true)
                  {
                    countImmovableX++;
                  }
                  else
                  {
                    countMovableX++;
                  }
                }
                if(cards[k].getPosY() == yVaria)
                {
                  if(boardutils.checkForImmovable(cards[k]) == true || boardutils.isUntargetableByBronze(cards[k]) == true)
                  {
                    countImmovableY++;
                  }
                  else
                  {
                    countMovableY++;
                  }
                }
              }

              if(countMovableX + countImmovableX + countMovableY + countImmovableY < (board.getWidth() + board.getHeight() - xDisab - yDisab - 2) && countMovableX + countMovableY > 0)
              {
                if((countMovableX == 0 && countMovableY + countImmovableY == board.getWidth() - xDisab - 1) || (countMovableY == 0 && countMovableX + countImmovableX == board.getHeight() - yDisab - 1))
                {
                }
                else
                {
                  allowedPositions.push(Point(xVaria,yVaria));
                }
              }
            }
            selectedPos = boardutils.tileChoice(allowedPositions, 0);
            break;

          case "souvenir":
            allowedPositions = new Array();
            for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
            {
              for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
              {
                if((board.getCardAtPosition(Point(xVaria,yVaria)) == null) && (board.getCardAtPosition(Point(xVaria+1,yVaria)) == null) && (board.getCardAtPosition(Point(xVaria,yVaria+1)) == null) && (board.getCardAtPosition(Point(xVaria-1,yVaria)) == null) && (board.getCardAtPosition(Point(xVaria,yVaria-1)) == null))
                {
                  if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false && board.isSafeTile(Point(xVaria,yVaria)) == false)
                  {

                    cardsCFUR = board.getAllCards();
                    var instantlyRotatable = true;
                    for(cfur = 0; cfur < cardsCFUR.length; cfur++)
                    {
                      if((cardsCFUR[cfur].getID() == "celticshield" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "celticshield")) && cardsCFUR[cfur].isGrayScale() == false && cardsCFUR[cfur].getPlayer() == gameState.getCurrentPlayer())
                      {
                        var shieldDistance = Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2)
                        if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
                        {
                          instantlyRotatable = false;
                          break;
                        }
                      }

                      if(card.getID() != "padlock" || (card.getID() == "padlock" && card.isGrayScale() == true))
                      {
                        if(cardsCFUR[cfur].getID() == "padlock" && cardsCFUR[cfur].isGrayScale() == false && Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2) == 1)
                        {
                          instantlyRotatable = false;
                          break;
                        }
                      }
                      else
                      {
                        instantlyRotatable = false;
                        break;
                      }

                      if((cardsCFUR[cfur].getID() == "hardhat" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "hardhat")) && cardsCFUR[cfur].isGrayScale() == false)
                      {
                        switch(cardsCFUR[cfur].getPlayer())
                        {
                          case 1: if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria-1) {instantlyRotatable = false;} break;
                          case 2: if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria+1) {instantlyRotatable = false;} break;
                          case 3: if(cardsCFUR[cfur].getPosX() == xVaria+1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;} break;
                          case 4: if(cardsCFUR[cfur].getPosX() == xVaria-1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;} break;
                        }
                      }
                    }

                    if(instantlyRotatable == true)
                    {
                      allowedPositions.push(Point(xVaria,yVaria));
                    }
                  }
                }
              }
            }
            selectedPos = boardutils.tileChoice(allowedPositions, 0);
            break;

          default:
            selectedPos = boardutils.tileChoice((boardutils.getNonAdjacentEmptyTiles()), 0);
            break;
        }

        //We got the choice, hide the info
        menu.hideInformation();
        //Log
        boardutils.logCardPlacement(card.getName());
        var userNames = gameState.getPlayerNickNames();
        gameState.writeToGameLog(qsTranslate("spectacles", "<font color=\"bisque\">The %1 revealed %2's hand.</font>").arg(card.getName()).arg(userNames[choice-1]), [1, 2, 3, 4], 1, 0);
        gameState.writeToGameLog(qsTranslate("spectacles", "<font color=\"bisque\">The %1 played the %2 from %3's hand to %4%5.</font>").arg(card.getName()).arg(selectedToPlace.getName()).arg(userNames[gameState.getCurrentPlayer()-1]).arg(boardutils.convertToABC(selectedPos.x)).arg(selectedPos.y+1), [1, 2, 3, 4], 1, 0);
        switch(gameState.getCurrentPlayer())
        {
          case 1:
            p1Inventory.moveCardToBoard(selectedToPlace.getUID(), selectedPos, 1);
            break;
          case 2:
            p2Inventory.moveCardToBoard(selectedToPlace.getUID(), selectedPos, 1);
            break;
          case 3:
            p3Inventory.moveCardToBoard(selectedToPlace.getUID(), selectedPos, 1);
            break;
          case 4:
            p4Inventory.moveCardToBoard(selectedToPlace.getUID(), selectedPos, 1);
            break;
        }
      }
      else
      {
        menu.hideInformation();
        menu.showInformation(qsTranslate("spectacles", "No item in your hand fulfills the criteria!"));
        //Log
        boardutils.logCardPlacement(card.getName());
      }
    }
    else
    {
      menu.showInformation(qsTranslate("spectacles", "No available tiles for the ability!"));
      //Log
      boardutils.logCardPlacement(card.getName());
    }
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
