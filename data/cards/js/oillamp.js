//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  //Handle the cardPlacedOnBoard event
  //Check if this card is the one that was placed
  //If it is, then execute the bronze action
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    eT = boardutils.getEmptyTiles();
    if(boardutils.isHalted(eT) == false && board.isDoubleTile(card.getPosition()) == false)
    {
      card.setCounter(6)
      //Log
      boardutils.logCardPlacement(card.getName());
      gameState.writeToGameLog(qsTranslate("oillamp", "<font color=\"bisque\">The %1 gained 6 counters.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
    else
    {
      card.setGrayScale(1);
      //Log
      boardutils.logCardPlacement(card.getName());
      gameState.writeToGameLog(qsTranslate("oillamp", "The %1 has been disabled.").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "turnStarted")
  {
    card.setShowFullSize(1);
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1] && card.getCounter() > 1 && mainInventory.getAllCards().length > 2)
  {
    //Check for hands not being empty
    if((gameState.getCurrentPlayer() == 1 && p1Inventory.isEmpty() == 0) || (gameState.getCurrentPlayer() == 2 && p2Inventory.isEmpty() == 0) || (gameState.getCurrentPlayer() == 3 && p3Inventory.isEmpty() == 0) || (gameState.getCurrentPlayer() == 4 && p4Inventory.isEmpty() == 0))
    {
      //Ask the player which card to discard
      menu.showInformation(qsTranslate("oillamp", "Choose an item to discard"), -1);
      //The player chooses one
      switch(gameState.getCurrentPlayer())
      {
        case 1: selectedDiscard = p1Inventory.cardChoice(p1Inventory.getAllCards(), 1, -1); break;
        case 2: selectedDiscard = p2Inventory.cardChoice(p2Inventory.getAllCards(), 1, -1); break;
        case 3: selectedDiscard = p3Inventory.cardChoice(p3Inventory.getAllCards(), 1, -1); break;
        case 4: selectedDiscard = p4Inventory.cardChoice(p4Inventory.getAllCards(), 1, -1); break;
      }
      menu.hideInformation();
      //Unless canceled
      if(selectedDiscard != null)
      {
        //Remove 2 counters
        card.setCounter(card.getCounter()-2);

        switch(gameState.getCurrentPlayer())
        {
          case 1: p1Inventory.removeCard(selectedDiscard.getUID()); break;
          case 2: p2Inventory.removeCard(selectedDiscard.getUID()); break;
          case 3: p3Inventory.removeCard(selectedDiscard.getUID()); break;
          case 4: p4Inventory.removeCard(selectedDiscard.getUID()); break;
        }
        //It gets discarded
        trash.addCard(selectedDiscard);

        //Log
        boardutils.logSilverActivation(card.getName());
        gameState.writeToGameLog(qsTranslate("oillamp", "<font color=\"paleturquoise\">The %1 cast 2 counters.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
        gameState.writeToGameLog(qsTranslate("oillamp", "<font color=\"paleturquoise\">The %1 discarded the %2.</font>").arg(card.getName()).arg(selectedDiscard.getName()), [1, 2, 3, 4], 1, 0);

        storageSize = mainInventory.getAllCards().length
        storageCards = mainInventory.getAllCards()
        //Generate three different random numbers
        offer1 = storageCards[rng.randomBetween(0, storageSize)]
        do
        {
          offer2 = storageCards[rng.randomBetween(0, storageSize)]
        }
        while(offer1 == offer2);
        do
        {
          offer3 = storageCards[rng.randomBetween(0, storageSize)]
        }
        while(offer1 == offer3 || offer2 == offer3);

        menu.hideInformation();
        mainInventory.removeCard(offer1.getUID());
        mainInventory.removeCard(offer2.getUID());
        mainInventory.removeCard(offer3.getUID());
        storage.addCard(offer1);
        storage.addCard(offer2);
        storage.addCard(offer3);
        storage.setViewable(true, true);
        //Ask the player which card to take
        menu.showInformation(qsTranslate("oillamp", "Choose an item"), -1);
        var lampChoice = storage.cardChoice(storage.getAllCards(), 0, -1);
        //Hide the label
        menu.hideInformation();

        storage.setViewable(false, false);
        var leftover1 = 0;
        var leftover2 = 0;
        if(lampChoice == offer1) {leftover1 = offer2; leftover2 = offer3;}
        if(lampChoice == offer2) {leftover1 = offer1; leftover2 = offer3;}
        if(lampChoice == offer3) {leftover1 = offer1; leftover2 = offer2;}
        //Remove the cards from the storage tab
        storage.removeCard(offer1.getUID());
        storage.removeCard(offer2.getUID());
        storage.removeCard(offer3.getUID());
        switch(gameState.getCurrentPlayer())
        {
          case 1: p1Inventory.addCard(lampChoice); break;
          case 2: p2Inventory.addCard(lampChoice); break;
          case 3: p3Inventory.addCard(lampChoice); break;
          case 4: p4Inventory.addCard(lampChoice); break;
        }
        mainInventory.addCard(leftover1);
        mainInventory.addCard(leftover2);

        //Log
        gameState.writeToGameLog(qsTranslate("oillamp", "<font color=\"paleturquoise\">The %1 drew the %2 from the storage.</font>").arg(card.getName()).arg(lampChoice.getName()), boardutils.informedPlayers(), 1, 0);
        gameState.writeToGameLog(qsTranslate("oillamp", "<font color=\"paleturquoise\">The %1 drew an item from the storage.</font>").arg(card.getName()), boardutils.unexpectingPlayers(), 1, 0);

        //Crop
        card.setShowFullSize(0);
      }
    }
    else
    {
      menu.showInformation(qsTranslate("oillamp", "No items in hand to discard!"));
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}


//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
