//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    if(boardutils.limitedByPerfume() == false)
    {
      eT = boardutils.getEmptyTiles();
    }
    else
    {
      eT = boardutils.getTilesAbovePerfume();
    }
    if(boardutils.isHalted(eT) == false && board.isDoubleTile(card.getPosition()) == false)
    {
      if(mainInventory.getAllCards().length > 0)
      {
        //Check for the amount of cards in the hands of the opponents
        var sumCardsInHands = 0;
        switch(gameState.getGameMode())
        {
          case 0:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length);
          break;
          case 1:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length + p4Inventory.getAllCards().length);
          break;
          case 2:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length);
          break;
          case 3:
          sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length + p4Inventory.getAllCards().length);
          break;
          case 4:
          sumCardsInHands = (p1Inventory.getAllCards().length + p3Inventory.getAllCards().length);
          break;
        }
        switch(gameState.getCurrentPlayer())
        {
          case 1:
          sumCardsInHands = sumCardsInHands - p1Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1)
          {
            sumCardsInHands = sumCardsInHands - p4Inventory.getAllCards().length;
          }
          break;
          case 2:
          sumCardsInHands = sumCardsInHands - p2Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1)
          {
            sumCardsInHands = sumCardsInHands - p3Inventory.getAllCards().length;
          }
          break;
          case 3:
          sumCardsInHands = sumCardsInHands - p3Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1)
          {
            sumCardsInHands = sumCardsInHands - p2Inventory.getAllCards().length;
          }
          break;
          case 4:
          sumCardsInHands = sumCardsInHands - p4Inventory.getAllCards().length;
          if(gameState.getGameMode() == 1)
          {
            sumCardsInHands = sumCardsInHands - p1Inventory.getAllCards().length;
          }
          break;
        }
        //If there is at least one card in the hand of one opponent
        if(sumCardsInHands > 0)
        {
          if(boardutils.limitedByPerfume() == false)
          {
            //All empty tiles are fine
            return boardutils.getEmptyTiles();
          }
          else
          {
            return boardutils.getTilesAbovePerfume();
          }
        }
        else
        {
          var array = new Array();
          return array;
        }
      }
      else
      {
        var array = new Array();
        return array;
      }
    }
    else
    {
      if(boardutils.limitedByPerfume() == false)
      {
        //All empty tiles are fine
        return boardutils.getEmptyTiles();
      }
      else
      {
        return boardutils.getTilesAbovePerfume();
      }
    }
  }
  else
  {
    var array = new Array();
    return array;
  }
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    if(boardutils.limitedByPerfume() == false)
    {
      eT = boardutils.getEmptyTiles();
    }
    else
    {
      eT = boardutils.getTilesAbovePerfume();
    }
    if(boardutils.isHalted(eT) == false && board.isDoubleTile(card.getPosition()) == false)
    {
      if(gameState.getGameMode() > 0 && gameState.getGameMode() < 4)
      {
        viewablePlayers = [] //This will store the possible players for the first choice
        if(gameState.getCurrentPlayer() == 2 || gameState.getCurrentPlayer() == 3 || (gameState.getCurrentPlayer() == 4 && gameState.getGameMode() == 3))
        {
          if(!p1Inventory.isEmpty())
          {
            viewablePlayers.push(1);
          }
        }
        if(gameState.getCurrentPlayer() == 1 || (gameState.getCurrentPlayer() == 3 && (gameState.getGameMode() == 2 || gameState.getGameMode() == 3)) || gameState.getCurrentPlayer() == 4)
        {
          if(!p2Inventory.isEmpty())
          {
            viewablePlayers.push(2);
          }
        }
        if(gameState.getCurrentPlayer() == 1 || (gameState.getCurrentPlayer() == 2 && gameState.getGameMode() > 1) || gameState.getCurrentPlayer() == 4)
        {
          if(!p3Inventory.isEmpty())
          {
            viewablePlayers.push(3);
          }
        }
        if((gameState.getCurrentPlayer() == 1 && gameState.getGameMode() == 3) || (gameState.getCurrentPlayer() == 2 && gameState.getGameMode() != 2) || (gameState.getCurrentPlayer() == 3 && gameState.getGameMode() != 2 ))
        {
          if(!p4Inventory.isEmpty())
          {
            viewablePlayers.push(4);
          }
        }
        menu.showInformation(qsTranslate("binoculars", "Which player's hand would you like to view?"), -1);
        //Get the player's answer
        choice = graphicsViewMenu.getPlayer(viewablePlayers, 0);
        //Now we have the choice, it's time to make the chosen player's inventory visible
        switch(choice)
        {
          case 1:
            p1Inventory.setViewable(true, true);
            break;
          case 2:
            p2Inventory.setViewable(true, true);
            break;
          case 3:
            p3Inventory.setViewable(true, true);
            break;
          case 4:
            p4Inventory.setViewable(true, true);
            break;
        }
        //Hide the 'Which player's inventory...' label
        menu.hideInformation();
        //Ask the player which card to discard
        menu.showInformation(qsTranslate("binoculars", "Choose an item to discard"), -1);
        switch(choice)
        {
          case 1:
            selectedDiscard = p1Inventory.cardChoice(p1Inventory.getAllCards(), 0, -1);
            p1Inventory.removeCard(selectedDiscard.getUID());
            break;
          case 2:
            selectedDiscard = p2Inventory.cardChoice(p2Inventory.getAllCards(), 0, -1);
            p2Inventory.removeCard(selectedDiscard.getUID());
            break;
          case 3:
            selectedDiscard = p3Inventory.cardChoice(p3Inventory.getAllCards(), 0, -1);
            p3Inventory.removeCard(selectedDiscard.getUID());
            break;
          case 4:
            selectedDiscard = p4Inventory.cardChoice(p4Inventory.getAllCards(), 0, -1);
            p4Inventory.removeCard(selectedDiscard.getUID());
            break;
        }
      }
      else
      {
        //Ask the player which card to discard
        menu.showInformation(qsTranslate("binoculars", "Choose an item to discard"), -1);
        //1v1 opposing
        if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 1)
        {
          p2Inventory.setViewable(true, true);
          selectedDiscard = p2Inventory.cardChoice(p2Inventory.getAllCards(), 0, -1);
          p2Inventory.removeCard(selectedDiscard.getUID());
          choice = 2;
        }
        if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 2)
        {
          p1Inventory.setViewable(true, true);
          selectedDiscard = p1Inventory.cardChoice(p1Inventory.getAllCards(), 0, -1);
          p1Inventory.removeCard(selectedDiscard.getUID());
          choice = 1;
        }
        //1v1 adjacent
        if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 1)
        {
          p3Inventory.setViewable(true, true);
          selectedDiscard = p3Inventory.cardChoice(p3Inventory.getAllCards(), 0, -1);
          p3Inventory.removeCard(selectedDiscard.getUID());
          choice = 3;
        }
        if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 3)
        {
          p1Inventory.setViewable(true, true);
          selectedDiscard = p1Inventory.cardChoice(p1Inventory.getAllCards(), 0, -1);
          p1Inventory.removeCard(selectedDiscard.getUID());
          choice = 1;
        }
      }

      menu.hideInformation();
      //It gets discarded
      trash.addCard(selectedDiscard);
      //Log
      boardutils.logCardPlacement(card.getName());
      var userNames = gameState.getPlayerNickNames();
      gameState.writeToGameLog(qsTranslate("binoculars", "<font color=\"bisque\">The %1 revealed %2's hand.</font>").arg(card.getName()).arg(userNames[choice-1]), [1, 2, 3, 4], 1, 0);
      gameState.writeToGameLog(qsTranslate("binoculars", "<font color=\"bisque\">The %1 discarded the %2 from %3's hand.</font>").arg(card.getName()).arg(selectedDiscard.getName()).arg(userNames[choice-1]), [1, 2, 3, 4], 1, 0);
      //A card is drawn for the player
      gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "bisque");
      //An informational message is displayed (it will disappear automatically)
      menu.showInformation(qsTranslate("binoculars", "You have drawn an item!"));
    }
    else
    {
      card.setGrayScale(1);
      //Log
      boardutils.logCardPlacement(card.getName());
      gameState.writeToGameLog(qsTranslate("binoculars", "The %1 has been disabled.").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
