//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums
//@include jsutils.point

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

//This function handles all events
function event(eventData)
{
  //Handle the cardPlacedOnBoard event
  //Check if this card is the one that was placed
  //If it is, then execute the bronze action
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    cards = board.getAllCards(); //Retrieve all cards on the board
    tilesOK = []; //This will store the tiles of the cards which can be moved
    for(i = 0; i < cards.length; i++) //Check all cards
    {
      //If the card is on an adjacent tile then it might be good
      if(Math.pow(card.getPosX()-cards[i].getPosX(), 2) + Math.pow(card.getPosY()-cards[i].getPosY(), 2) == 1)
      {
        //If the value of the card is less than five then it might be good
        if(cards[i].getCurrentValue() < 5)
        {
          //If the material of the card is wood, plastic or glass then it might be good
          if(cards[i].getBackground() == "wood" || cards[i].getBackground() == "plastic" || cards[i].getBackground() == "glass")
          {
            if(boardutils.isUntargetableByBronze(cards[i]) == false)
            {
              //Material-related conditions...
              //If it is plastic, it must have an adjacent empty tile and must be moveable
              if(cards[i].getBackground() != "plastic" || (cards[i].getBackground() == "plastic" && boardutils.getAdjacentEmptyTiles(cards[i].getPosition()).length > 0 && boardutils.checkForImmovable(cards[i]) == false))
              {
                //If it is glass, it must be possible to move it to trash
                if(cards[i].getBackground() != "glass" || (cards[i].getBackground() == "glass" && boardutils.checkForUntrashable(cards[i]) == false))
                {
                  //If it is wooden, it must be possible to pick it up
                  if(cards[i].getBackground() != "wood" || (cards[i].getBackground() == "wood" && boardutils.checkForUnpickable(cards[i]) == false &&
                    ((cards[i].getPlayer() == 1) ||
                     (cards[i].getPlayer() == 2 && gameState.getGameMode() != 4) ||
                     (cards[i].getPlayer() == 3 && gameState.getGameMode() != 0) ||
                     (cards[i].getPlayer() == 4 && (gameState.getGameMode() == 1 || gameState.getGameMode() == 3)) )))
                  {
                    //Store the coordinates and UIDs of the good card
                    tilesOK.push(cards[i].getPosition());
                  }
                }
              }
            }
          }
        }
      }
    }

    //Sound effect
    var randomSound = rng.randomBetween(1, 5);
    switch(randomSound)
    {
      case 1: gameState.playSoundEffect("joker-1.wav", 1); break;
      case 2: gameState.playSoundEffect("joker-2.wav", 1); break;
      case 3: gameState.playSoundEffect("joker-3.wav", 1); break;
      case 4: gameState.playSoundEffect("joker-4.wav", 1); break;
    }

    if(tilesOK.length > 0)
    {
      var cancelToTop;
      do
      {
        cancelToTop = 0;
        //Ask the player which card would he like to target
        menu.showInformation(qsTranslate("joker", "Choose target"), -1);
        //The player chooses one from the good cards
        selectedTile = boardutils.tileChoice(tilesOK, 0);
        selectedCard = board.getCardAtPosition(selectedTile);
        //Hide the 'Choose a card...' label
        menu.hideInformation();

        if(selectedCard.getBackground() == "wood")
        {
          //Log
          boardutils.logCardPlacement(card.getName());
          var userNames = gameState.getPlayerNickNames();
          gameState.writeToGameLog(qsTranslate("joker", "<font color=\"bisque\">The %1 returned %2%3 %4 to %5's hand.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()).arg(userNames[selectedCard.getPlayer()-1]), [1, 2, 3, 4], 1, 0);
          //Remove the card from the board
          if(selectedCard.getPlayer() == 1)
          {
            board.removeCard(selectedCard.getUID());
            p1Inventory.addCard(selectedCard);
          }
          if(selectedCard.getPlayer() == 2)
          {
            board.removeCard(selectedCard.getUID());
            p2Inventory.addCard(selectedCard);
          }
          if(selectedCard.getPlayer() == 3)
          {
            board.removeCard(selectedCard.getUID());
            p3Inventory.addCard(selectedCard);
          }
          if(selectedCard.getPlayer() == 4)
          {
            board.removeCard(selectedCard.getUID());
            p4Inventory.addCard(selectedCard);
          }
        }
        else if(selectedCard.getBackground() == "plastic")
        {
          //Now the player must choose the new place for the card
          menu.showInformation(qsTranslate("joker", "Choose where would you like to move the selected card"), -1);
          //Only the empty tiles adjacent to the chosen card are available now
          selectedPos = boardutils.tileChoice(boardutils.getAdjacentEmptyTiles(selectedTile), 1);
          //We got the choice, hide the info
          menu.hideInformation();
          if(selectedPos.x != -1)
          {
            //Move the card
            board.moveCard(selectedTile, selectedPos);
            //Log
            boardutils.logCardPlacement(card.getName());
            gameState.writeToGameLog(qsTranslate("joker", "<font color=\"bisque\">The %1 moved %2%3 %4 to %5%6.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()).arg(boardutils.convertToABC(selectedPos.x)).arg(selectedPos.y+1), [1, 2, 3, 4], 1, 0);
          }
          else
          {
            cancelToTop = 1;
          }
        }
        else if(selectedCard.getBackground() == "glass")
        {
          damageOutcome = boardutils.damageEvents(card.getID(), selectedCard, selectedCard.getPosX(), selectedCard.getPosY());
          //Log
          boardutils.logCardPlacement(card.getName());
          gameState.writeToGameLog(qsTranslate("joker", "<font color=\"bisque\">The %1 destroyed %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);
          board.moveCardToTrash(selectedCard.getUID());
          //Piggy Bank events
          if(damageOutcome[1] > 0) {for(drawCount = 0; drawCount < damageOutcome[1]; drawCount++) {gameState.drawCard(1, 0, 0, 0, 0, "khaki");}}
          if(damageOutcome[2] > 0) {for(drawCount = 0; drawCount < damageOutcome[2]; drawCount++) {gameState.drawCard(2, 0, 0, 0, 0, "khaki");}}
          if(damageOutcome[3] > 0) {for(drawCount = 0; drawCount < damageOutcome[3]; drawCount++) {gameState.drawCard(3, 0, 0, 0, 0, "khaki");}}
          if(damageOutcome[4] > 0) {for(drawCount = 0; drawCount < damageOutcome[4]; drawCount++) {gameState.drawCard(4, 0, 0, 0, 0, "khaki");}}
        }
      }
      while(cancelToTop == 1);
    }
    else
    {
      menu.showInformation(qsTranslate("joker", "No items are selectable for the ability!"));
    }

    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
