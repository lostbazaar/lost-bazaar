//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    var arrayNumber = 0;
    for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
    {
      for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
      {
        if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
        {
          for(ttc = 2; ttc < 10; ttc = ttc+2)
          {
            var xCorri = 0;
            var yCorri = 0;
            if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
            if(ttc == 4 && xVaria > 0) {xCorri = -1;}
            if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
            if(ttc == 8 && yVaria > 0) {yCorri = -1;}
            TTC = Point(xVaria+xCorri,yVaria+yCorri);
            CTC = board.getCardAtPosition(TTC);
            if(CTC != null && CTC.getUID() != card.getUID() && CTC.getPlayer() == card.getPlayer() && boardutils.isUntargetableByBronze(CTC) == false)
            {
              array[arrayNumber] = new Point(xVaria,yVaria);
              arrayNumber++;
              break;
            }
          }
        }
      }
    }
    return array;
  }
  else
  {
    return array;
  }
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    
    
    tilesOK = [];
    for(ctc = 2; ctc < 10; ctc = ctc+2)
    {
      var xCorri = 0;
      var yCorri = 0;
      if(ctc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
      if(ctc == 4 && card.getPosX() > 0) {xCorri = -1;}
      if(ctc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
      if(ctc == 8 && card.getPosY() > 0) {yCorri = -1;}
      TTC = Point(card.getPosX()+xCorri,card.getPosY()+yCorri);
      CTC = board.getCardAtPosition(TTC);
      if(CTC != null && CTC.getUID() != card.getUID() && CTC.getPlayer() == gameState.getCurrentPlayer() && boardutils.isUntargetableByBronze(CTC) == false)
      {
        tilesOK.push(TTC);
      }
    }
    menu.showInformation(qsTranslate("envelope", "Choose target"), -1);
    selectedTile = boardutils.tileChoice(tilesOK, 0);
    menu.hideInformation();
    selectedCard = board.getCardAtPosition(selectedTile);
    selectedValue = selectedCard.getCurrentValue() - selectedCard.getBaseValue();
    
    //Log
    boardutils.logCardPlacement(card.getName());
    
    if(selectedValue > 0)
    {
      card.setCounter(selectedValue);
      gameState.writeToGameLog(qsTranslate("envelope", "<font color=\"bisque\">The %1 gained %2 counters.</font>").arg(card.getName()).arg(selectedValue), [1, 2, 3, 4], 1, 0);
    }

    //Deny active abilities
    gameState.setSilverActionsDisabledForTurn(1);
    var userNames = gameState.getPlayerNickNames();
    gameState.writeToGameLog(qsTranslate("envelope", "%1 cannot use active abilities for the remaining turn.").arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    cards = board.getAllCards();
    var tilesOK = [];

    for(ctc = 2; ctc < 10; ctc = ctc+2)
    {
      var xCorri = 0;
      var yCorri = 0;
      if(ctc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
      if(ctc == 4 && card.getPosX() > 0) {xCorri = -1;}
      if(ctc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
      if(ctc == 8 && card.getPosY() > 0) {yCorri = -1;}
      TTC = Point(card.getPosX()+xCorri,card.getPosY()+yCorri);
      CTC = board.getCardAtPosition(TTC);
      if(CTC != null && CTC.getUID() != card.getUID() && boardutils.isUntargetableBySilver(CTC) == false)
      {
        if(boardutils.checkForImmovable(CTC) == false && boardutils.checkForImmovable(card) == false && CTC.getBaseValue() >= 0 && card.getCounter() >= CTC.getBaseValue())
        {
          tilesOK.push(TTC);
        }
      }
    }

    if(tilesOK.length > 0)
    {
      menu.showInformation(qsTranslate("envelope", "Choose which item would you like to move"), -1);
      selectedTile = boardutils.tileChoice(tilesOK, 0);
      selectedCard = board.getCardAtPosition(selectedTile)
      menu.hideInformation();
      var counterCost = selectedCard.getBaseValue();

      //Log
      boardutils.logSilverActivation(card.getName());
      if(counterCost < 2)
      {
        gameState.writeToGameLog(qsTranslate("envelope", "<font color=\"paleturquoise\">The %1 cast %2 counter.</font>").arg(card.getName()).arg(counterCost), [1, 2, 3, 4], 1, 0);
      }
      else
      {
        gameState.writeToGameLog(qsTranslate("envelope", "<font color=\"paleturquoise\">The %1 cast %2 counters.</font>").arg(card.getName()).arg(counterCost), [1, 2, 3, 4], 1, 0);
      }
      gameState.writeToGameLog(qsTranslate("envelope", "<font color=\"paleturquoise\">The %1 switched tiles with %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);

      card.setCounter(card.getCounter() - counterCost);
      board.swapCards(card.getPosition(), selectedCard.getPosition());
    }
    else
    {
      menu.showInformation(qsTranslate("envelope", "You cannot use this ability!"));
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
