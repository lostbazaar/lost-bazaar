//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}


function event(eventData)
{
  if(eventData[0] == "turnStarted")
  {
    card.setShowFullSize(1);
  }

  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    card.setCounter(8);
    //Log
    boardutils.logCardPlacement(card.getName());
    gameState.writeToGameLog(qsTranslate("saltshaker", "<font color=\"bisque\">The %1 gained 8 counters.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1] && card.getCounter() > 0)
  {

    if(boardutils.getAdjacentEmptyTiles(card.getPosition()).length > 0 && boardutils.checkForImmovable(card) == false)
    {
      tilesOK = [];
      cardsOK = [];

      for(ttc = 2; ttc < 10; ttc = ttc+2)
      {
        var xCorri = 0;
        var yCorri = 0;
        if(ttc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
        if(ttc == 4 && card.getPosX() > 0) {xCorri = -1;}
        if(ttc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
        if(ttc == 8 && card.getPosY() > 0) {yCorri = -1;}

        if(board.getCardAtPosition(Point(card.getPosX()+xCorri,card.getPosY()+yCorri)) == null && board.isDisabledTile(Point(card.getPosX()+xCorri,card.getPosY()+yCorri)) == false)
        {
          for(ctc = 2; ctc < 10; ctc = ctc+2)
          {
            var xCorri2 = 0;
            var yCorri2 = 0;
            if(ctc == 2 && ttc != 8 && card.getPosY() < board.getHeight() - 1) {yCorri2 = 1;}
            if(ctc == 4 && ttc != 6 && card.getPosX() > 0) {xCorri2 = -1;}
            if(ctc == 6 && ttc != 4 && card.getPosX() < board.getWidth() - 1) {xCorri2 = 1;}
            if(ctc == 8 && ttc != 2 && card.getPosY() > 0) {yCorri2 = -1;}

            if(board.getCardAtPosition(Point(card.getPosX()+xCorri+xCorri2,card.getPosY()+yCorri+yCorri2)) != null && boardutils.isUntargetableBySilver(board.getCardAtPosition(Point(card.getPosX()+xCorri+xCorri2,card.getPosY()+yCorri+yCorri2))) == false)
            {
              tilesOK.push(Point(card.getPosX()+xCorri,card.getPosY()+yCorri));
              break;
            }
          }
        }
      }

      if(tilesOK.length > 0)
      {
        menu.showInformation(qsTranslate("saltshaker", "Choose where to move the %1").arg(card.getName()), -1);
        selectedPos = boardutils.tileChoice(tilesOK, 1);
        menu.hideInformation();
        //Unless canceled
        if(selectedPos.x != -1)
        {
          //Log
          boardutils.logSilverActivation(card.getName());

          //Move the card
          board.moveCard(card.getPosition(), selectedPos);

          //Log
          gameState.writeToGameLog(qsTranslate("saltshaker", "<font color=\"paleturquoise\">The %1 has been moved to %2%3.</font>").arg(card.getName()).arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1), [1, 2, 3, 4], 1, 0);

          for(stc = 2; stc < 10; stc = stc+2)
          {
            var xCorri3 = 0;
            var yCorri3 = 0;
            if(stc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri3 = 1;}
            if(stc == 4 && card.getPosX() > 0) {xCorri3 = -1;}
            if(stc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri3 = 1;}
            if(stc == 8 && card.getPosY() > 0) {yCorri3 = -1;}

            if(xCorri3 != 0 || yCorri3 != 0)
            {
              if(board.getCardAtPosition(Point(card.getPosX()+xCorri3,card.getPosY()+yCorri3)) != null && boardutils.isUntargetableBySilver(board.getCardAtPosition(Point(card.getPosX()+xCorri3,card.getPosY()+yCorri3))) == false)
              {
                cardsOK.push((Point(card.getPosX()+xCorri3,card.getPosY()+yCorri3)));
              }
            }
          }

          //Ask the player on which card would he like to place a counter
          menu.showInformation(qsTranslate("saltshaker", "Choose on which item would you like to place a counter"), -1);
          //The player chooses one from the good cards
          selectedTile = boardutils.tileChoice(cardsOK, 0);
          var targetedByShaker = board.getCardAtPosition(selectedTile);
          //Take down 1 counter
          card.setCounter(card.getCounter()-1);
          //Place 1 counter
          targetedByShaker.setCounter(targetedByShaker.getCounter()+1);
          //It gets capped at 12
          if(targetedByShaker.getCounter() > 11)
          {
            targetedByShaker.setCounter(12);
          }
          //Check for Golf Club
          var clubRotated = false;
          if( ( (targetedByShaker.getID() == "golfclub" && targetedByShaker.isGrayScale() == false) || (targetedByShaker.getID() == "handmirror" && boardutils.itemMirrored(targetedByShaker) == "golfclub") ) && boardutils.checkForUnrotatable(targetedByShaker) == false && targetedByShaker.getPlayer() != gameState.getCurrentPlayer())
          {
            clubRotated = true;
          }
          menu.hideInformation();
          card.setShowFullSize(0);
          //Log
          gameState.writeToGameLog(qsTranslate("saltshaker", "<font color=\"paleturquoise\">The %1 transfered 1 counter from itself to %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(targetedByShaker.getPosX())).arg(targetedByShaker.getPosY()+1).arg(targetedByShaker.getName()), [1, 2, 3, 4], 1, 0);
          if(clubRotated == true)
          {
            targetedByShaker.changeOwner(gameState.getCurrentPlayer());

            //Log
            var userNames = gameState.getPlayerNickNames();
            gameState.writeToGameLog(qsTranslate("saltshaker", "<font color=\"khaki\">%1%2 %3 has been rotated toward %4.</font>").arg(boardutils.convertToABC(targetedByShaker.getPosX())).arg(targetedByShaker.getPosY()+1).arg(targetedByShaker.getName()).arg(userNames[targetedByShaker.getPlayer()-1]), [1, 2, 3, 4], 1, 0);
          }
        }
      }
      else
      {
        menu.showInformation(qsTranslate("saltshaker", "You cannot use this ability!"));
      }
    }
    else
    {
      menu.showInformation(qsTranslate("saltshaker", "You cannot use this ability!"));
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
