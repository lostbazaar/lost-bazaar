//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}


function event(eventData)
{
  //Handle the cardPlacedOnBoard event
  //Check if this card is the one that was placed
  //If it is, then execute the bronze action
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    for(j = 0; j < 2; j++)
    {
      cards = board.getAllCards(); //Retrieve all cards on the board
      tilesOK = [card.getPosition()]; //Store the positions of the cards (itself added for the possibility to abort)

      for(ctc = 2; ctc < 10; ctc = ctc+2)
      {
        var xCorri = 0;
        var yCorri = 0;
        if(ctc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
        if(ctc == 4 && card.getPosX() > 0) {xCorri = -1;}
        if(ctc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
        if(ctc == 8 && card.getPosY() > 0) {yCorri = -1;}
        TTC = Point(card.getPosX()+xCorri,card.getPosY()+yCorri);
        CTC = board.getCardAtPosition(TTC);
        if(CTC != null && CTC != card && CTC.isGrayScale() == false && boardutils.isUntargetableByBronze(CTC) == false)
        {
          tilesOK.push(TTC);
        }
      }

      if(j == 0)
      {
        //Log
        boardutils.logCardPlacement(card.getName());
      }

      if(tilesOK.length > 1)
      {
        //Ask the player which card's ability to erase
        menu.showInformation(qsTranslate("eraser", "Choose target (click on the %1 to dismiss it)").arg(card.getName()), -1);
        //The player chooses one from the good cards
        selectedTile = boardutils.tileChoice(tilesOK, 0);
        //Hide the 'Choose target' label
        menu.hideInformation();
        selectedCard = board.getCardAtPosition(selectedTile);

        if(card == selectedCard)
        {
          break;
        }
        else
        {
          selectedCard.setGrayScale(1);
          //Log
          gameState.writeToGameLog(qsTranslate("eraser", "<font color=\"bisque\">The %1 disabled %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);
        }
      }
      else
      {
        menu.showInformation(qsTranslate("eraser", "No card is selectable for ability!"));
      }
      //Exit loop if there are no more choices to make
      if(tilesOK.length < 3)
      {
        break;
      }
    }
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
