//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums
//@include jsutils.point

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    //Check for the amount of cards in the hands of the opponents
    var sumCardsInHands = 0;
    switch(gameState.getGameMode())
    {
      case 0:
        sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length);
        break;
      case 1:
        sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length + p4Inventory.getAllCards().length);
        break;
      case 2:
        sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length);
        break;
      case 3:
        sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length + p4Inventory.getAllCards().length);
        break;
      case 4:
        sumCardsInHands = (p1Inventory.getAllCards().length + p3Inventory.getAllCards().length);
        break;
    }
    switch(gameState.getCurrentPlayer())
    {
      case 1:
        sumCardsInHands = sumCardsInHands - p1Inventory.getAllCards().length;
        if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p4Inventory.getAllCards().length;}
        break;
      case 2:
        sumCardsInHands = sumCardsInHands - p2Inventory.getAllCards().length;
        if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p3Inventory.getAllCards().length;}
        break;
      case 3:
        sumCardsInHands = sumCardsInHands - p3Inventory.getAllCards().length;
        if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p2Inventory.getAllCards().length;}
        break;
      case 4:
        sumCardsInHands = sumCardsInHands - p4Inventory.getAllCards().length;
        if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p1Inventory.getAllCards().length;}
        break;
    }

    var eT = boardutils.getEmptyTiles();
    //If there is at least one card in the hand of one opponent, and an empty tile
    if(sumCardsInHands > 0 && eT.length > 0)
    {
      if(boardutils.limitedByPerfume() == true)
      {
        eT = boardutils.getTilesAbovePerfume();
      }

      for(i = 0; i < eT.length; i++)
      {
        var xVaria = eT[i].x;
        var yVaria = eT[i].y;

        cardsCFUR = board.getAllCards();
        var instantlyRotatable = true;

        if(board.isSafeTile(Point(xVaria,yVaria)) == true)
        {
          instantlyRotatable = false;
        }
        else
        {
          for(cfur = 0; cfur < cardsCFUR.length; cfur++)
          {
            if((cardsCFUR[cfur].getID() == "celticshield" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "celticshield")) && cardsCFUR[cfur].isGrayScale() == false && cardsCFUR[cfur].getPlayer() == card.getPlayer())
            {
              var shieldDistance = Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2)
              if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
              {
                instantlyRotatable = false;
                break;
              }
            }

            if(cardsCFUR[cfur].getID() == "padlock" && cardsCFUR[cfur].isGrayScale() == false && Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2) == 1)
            {
              instantlyRotatable = false;
              break;
            }

            if((cardsCFUR[cfur].getID() == "hardhat" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "hardhat")) && cardsCFUR[cfur].isGrayScale() == false)
            {
              switch(cardsCFUR[cfur].getPlayer())
              {
                case 1:
                  if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria-1) {instantlyRotatable = false;}
                  break;
                case 2:
                  if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria+1) {instantlyRotatable = false;}
                  break;
                case 3:
                  if(cardsCFUR[cfur].getPosX() == xVaria+1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;}
                  break;
                case 4:
                  if(cardsCFUR[cfur].getPosX() == xVaria-1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;}
                  break;
              }
            }
          }
        }

        if(instantlyRotatable == true)
        {
          array.push(eT[i]);
        }

      }
    }
  }
  return array;
}



function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    if(gameState.getGameMode() > 0 && gameState.getGameMode() < 4)
    {
      targetablePlayers = [] //This will store the possible players for the first choice
      if(gameState.getCurrentPlayer() == 2 || (gameState.getCurrentPlayer() == 3 && gameState.getGameMode() != 1) || gameState.getCurrentPlayer() == 4)
      {
        if(!p1Inventory.isEmpty())
        {
          targetablePlayers.push(1);
        }
      }
      if((gameState.getCurrentPlayer() == 1) || gameState.getCurrentPlayer() == 3 || (gameState.getCurrentPlayer() == 4 && gameState.getGameMode() != 1))
      {
        if(!p2Inventory.isEmpty())
        {
          targetablePlayers.push(2);
        }
      }
      if((gameState.getCurrentPlayer() == 1 && gameState.getGameMode() != 1) || gameState.getCurrentPlayer() == 2 || gameState.getCurrentPlayer() == 4)
      {
        if(!p3Inventory.isEmpty())
        {
          targetablePlayers.push(3);
        }
      }
      if((gameState.getCurrentPlayer() == 1 && gameState.getGameMode() != 2) || (gameState.getCurrentPlayer() == 2 && gameState.getGameMode() == 3) || (gameState.getCurrentPlayer() == 3 && gameState.getGameMode() != 2))
      {
        if(!p4Inventory.isEmpty())
        {
          targetablePlayers.push(4);
        }
      }
      menu.showInformation(qsTranslate("souvenir", "Toward who would you like to rotate the %1?").arg(card.getName()), -1);
      //Get the player's answer
      choice = graphicsViewMenu.getPlayer(targetablePlayers, 0);
      //Now we have the choice
      switch(choice)
      {
        case 1:
          card.changeOwner(1)
          handTakenFrom = p1Inventory.getAllCards();
          cardRandomlyTaken = handTakenFrom[rng.randomBetween(0, p1Inventory.getAllCards().length)];
          p1Inventory.removeCard(cardRandomlyTaken.getUID());
          break;
        case 2:
          card.changeOwner(2)
          handTakenFrom = p2Inventory.getAllCards();
          cardRandomlyTaken = handTakenFrom[rng.randomBetween(0, p2Inventory.getAllCards().length)];
          p2Inventory.removeCard(cardRandomlyTaken.getUID());
          break;
        case 3:
          card.changeOwner(3)
          handTakenFrom = p3Inventory.getAllCards();
          cardRandomlyTaken = handTakenFrom[rng.randomBetween(0, p3Inventory.getAllCards().length)];
          p3Inventory.removeCard(cardRandomlyTaken.getUID());
          break;
        case 4:
          card.changeOwner(4)
          handTakenFrom = p4Inventory.getAllCards();
          cardRandomlyTaken = handTakenFrom[rng.randomBetween(0, p4Inventory.getAllCards().length)];
          p4Inventory.removeCard(cardRandomlyTaken.getUID());
          break;
      }

      if(gameState.getCurrentPlayer() == 1)
      {
        p1Inventory.addCard(cardRandomlyTaken);
      }
      if(gameState.getCurrentPlayer() == 2)
      {
        p2Inventory.addCard(cardRandomlyTaken);
      }
      if(gameState.getCurrentPlayer() == 3)
      {
        p3Inventory.addCard(cardRandomlyTaken);
      }
      if(gameState.getCurrentPlayer() == 4)
      {
        p4Inventory.addCard(cardRandomlyTaken);
      }
      menu.hideInformation();
    }
    else
    {
      //1v1 opposing
      if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 1)
      {
        card.changeOwner(2);
        handTakenFrom = p2Inventory.getAllCards();
        cardRandomlyTaken = handTakenFrom[rng.randomBetween(0, p2Inventory.getAllCards().length)];
        p2Inventory.removeCard(cardRandomlyTaken.getUID());
        p1Inventory.addCard(cardRandomlyTaken);
        choice = 2
      }
      if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 2)
      {
        card.changeOwner(1)
        handTakenFrom = p1Inventory.getAllCards();
        cardRandomlyTaken = handTakenFrom[rng.randomBetween(0, p1Inventory.getAllCards().length)];
        p1Inventory.removeCard(cardRandomlyTaken.getUID());
        p2Inventory.addCard(cardRandomlyTaken);
        choice = 1
      }
      //1v1 adjacent
      if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 1)
      {
        card.changeOwner(3);
        handTakenFrom = p3Inventory.getAllCards();
        cardRandomlyTaken = handTakenFrom[rng.randomBetween(0, p3Inventory.getAllCards().length)];
        p3Inventory.removeCard(cardRandomlyTaken.getUID());
        p1Inventory.addCard(cardRandomlyTaken);
        choice = 3
      }
      if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 3)
      {
        card.changeOwner(1)
        handTakenFrom = p1Inventory.getAllCards();
        cardRandomlyTaken = handTakenFrom[rng.randomBetween(0, p1Inventory.getAllCards().length)];
        p1Inventory.removeCard(cardRandomlyTaken.getUID());
        p3Inventory.addCard(cardRandomlyTaken);
        choice = 1
      }
      menu.hideInformation();
    }

    //Log
    var userNames = gameState.getPlayerNickNames();
    boardutils.logCardPlacement(card.getName());
    gameState.writeToGameLog(qsTranslate("souvenir", "<font color=\"bisque\">The %1 has been rotated towards %2.</font>").arg(card.getName()).arg(userNames[card.getPlayer()-1]), [1, 2, 3, 4], 1, 0);

    //Placing counters
    if(cardRandomlyTaken.getBaseValue() > 0)
    {
      cards = board.getAllCards(); //Retrieve all cards on the board
      cardsOK = []; //This will store the cards which can be moved
      for(i = 0; i < cards.length; i++) //Check all cards
      {
        if(boardutils.isUntargetableByBronze(cards[i]) == false)
        {
          cardsOK.push(cards[i].getPosition());
        }
      }
      //Ask the player on which card would he like to place the counters
      if(cardRandomlyTaken.getBaseValue() == 1)
      {
        menu.showInformation(qsTranslate("souvenir", "Choose on which item would you like to place a counter"), -1);
      }
      else
      {
        menu.showInformation(qsTranslate("souvenir", "Choose on which item would you like to place %1 counters").arg(cardRandomlyTaken.getBaseValue()), -1);
      }
      //The player chooses a card
      selectedTile = boardutils.tileChoice(cardsOK, 0);
      //Hide the 'Choose which card...' label
      menu.hideInformation();
      var targetedBySouvenir = (board.getCardAtPosition(selectedTile));
      //Check for Golf Club
      var clubRotated = false;
      if( ( (targetedBySouvenir.getID() == "golfclub" && targetedBySouvenir.isGrayScale() == false) || (targetedBySouvenir.getID() == "handmirror" && boardutils.itemMirrored(targetedBySouvenir) == "golfclub") ) && boardutils.checkForUnrotatable(targetedBySouvenir) == false && targetedBySouvenir.getPlayer() != gameState.getCurrentPlayer())
      {
        targetedBySouvenir.changeOwner(gameState.getCurrentPlayer());
        clubRotated = true;
      }
      //Add some counters
      targetedBySouvenir.setCounter(targetedBySouvenir.getCounter() + cardRandomlyTaken.getBaseValue());
      //It gets capped at 12
      if(targetedBySouvenir.getCounter() > 11)
      {
        targetedBySouvenir.setCounter(12);
      }
    }
    //Log
    gameState.writeToGameLog(qsTranslate("souvenir", "<font color=\"bisque\">The %1 took %2's %3 away.</font>").arg(card.getName()).arg(userNames[choice-1]).arg(cardRandomlyTaken.getName()), [gameState.getCurrentPlayer(), choice], 1, 0);
    if(gameState.getGameMode() != 0 && gameState.getGameMode() != 4)
    {
      var thirdParty = [];
      for(tp = 1; tp < 5; tp++)
      {
        if(tp != gameState.getCurrentPlayer() && tp != choice)
        {
          thirdParty.push(tp);
        }
      }
      gameState.writeToGameLog(qsTranslate("souvenir", "<font color=\"bisque\">The %1 took an item of %2 value from %3's hand away.</font>").arg(card.getName()).arg(cardRandomlyTaken.getBaseValue()).arg(userNames[choice-1]), [thirdParty[0], thirdParty[1]], 1, 0);
    }
    if(cardRandomlyTaken.getBaseValue() > 1)
    {
      gameState.writeToGameLog(qsTranslate("souvenir", "<font color=\"bisque\">The %1 placed %2 counters on %3%4 %5.</font>").arg(card.getName()).arg(cardRandomlyTaken.getBaseValue()).arg(boardutils.convertToABC(targetedBySouvenir.getPosX())).arg(targetedBySouvenir.getPosY()+1).arg(targetedBySouvenir.getName()), [1, 2, 3, 4], 1, 0);
    }
    if(cardRandomlyTaken.getBaseValue() == 1)
    {
      gameState.writeToGameLog(qsTranslate("souvenir", "<font color=\"bisque\">The %1 placed 1 counter on %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(targetedBySouvenir.getPosX())).arg(targetedBySouvenir.getPosY()+1).arg(targetedBySouvenir.getName()), [1, 2, 3, 4], 1, 0);
    }
    if(clubRotated == true)
    {
      gameState.writeToGameLog(qsTranslate("souvenir", "<font color=\"khaki\">%1%2 %3 has been rotated toward %4.</font>").arg(boardutils.convertToABC(targetedBySouvenir.getPosX())).arg(targetedBySouvenir.getPosY()+1).arg(targetedBySouvenir.getName()).arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
    }
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
