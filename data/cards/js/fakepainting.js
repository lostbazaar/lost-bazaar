//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "turnStarted" && card.getPlayer() != gameState.getCurrentPlayer() && card.isGrayScale() == false)
  {
    if( (card.getPlayer() == 1 && !p1Inventory.isViewable()) ||
        (card.getPlayer() == 2 && !p2Inventory.isViewable()) ||
        (card.getPlayer() == 3 && !p3Inventory.isViewable()) ||
        (card.getPlayer() == 4 && !p4Inventory.isViewable()) )
    {
      switch(card.getPlayer())
      {
        case 1: p1Inventory.setViewable(true, false); break;
        case 2: p2Inventory.setViewable(true, false); break;
        case 3: p3Inventory.setViewable(true, false); break;
        case 4: p4Inventory.setViewable(true, false); break;
      }
      //Log
      var userNames = gameState.getPlayerNickNames();
      gameState.writeToGameLog(qsTranslate("fakepainting", "<font color=\"khaki\">%1%2 %3 revealed %4's hand.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()).arg(userNames[card.getPlayer()-1]), [1, 2, 3, 4], 1, 0);
    }
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    var handToExamine = [];
    var hasWoodenItem = false;
    switch(gameState.getCurrentPlayer())
    {
      case 1: var handToExamine = p1Inventory.getAllCards(); break;
      case 2: var handToExamine = p2Inventory.getAllCards(); break;
      case 3: var handToExamine = p3Inventory.getAllCards(); break;
      case 4: var handToExamine = p4Inventory.getAllCards(); break;
    }
    for(i = 0; i < handToExamine.length; i++)
    {
      if(handToExamine[i].getBackground() == "wood")
      {
        hasWoodenItem = true;
        break;
      }
    }
    if(hasWoodenItem == true && boardutils.checkForUnpickable(card) == false)
    {
      menu.showInformation(qsTranslate("fakepainting", "Choose a wooden item to discard"), -1);
      if(gameState.getCurrentPlayer() == 1)
      {
        for(i = 0; i < handToExamine.length; i++)
        {
          do
          {
            selectedDiscard = p1Inventory.cardChoice(p1Inventory.getAllCards(), 1, -1);
          }
          while(selectedDiscard != null && selectedDiscard.getBackground() != "wood");
          if(selectedDiscard != null)
          {
            p1Inventory.removeCard(selectedDiscard.getUID());
            board.removeCard(card.getUID());
            p1Inventory.addCard(card);
          }
          break;
        }
      }
      if(gameState.getCurrentPlayer() == 2)
      {
        for(i = 0; i < handToExamine.length; i++)
        {
          do
          {
            selectedDiscard = p2Inventory.cardChoice(p2Inventory.getAllCards(), 1, -1);
          }
          while(selectedDiscard != null && selectedDiscard.getBackground() != "wood");
          if(selectedDiscard != null)
          {
            p2Inventory.removeCard(selectedDiscard.getUID());
            board.removeCard(card.getUID());
            p2Inventory.addCard(card);
          }
          break;
        }
      }
      if(gameState.getCurrentPlayer() == 3)
      {
        for(i = 0; i < handToExamine.length; i++)
        {
          do
          {
            selectedDiscard = p3Inventory.cardChoice(p3Inventory.getAllCards(), 1, -1);
          }
          while(selectedDiscard != null && selectedDiscard.getBackground() != "wood");
          if(selectedDiscard != null)
          {
            p3Inventory.removeCard(selectedDiscard.getUID());
            board.removeCard(card.getUID());
            p3Inventory.addCard(card);
          }
          break;
        }
      }
      if(gameState.getCurrentPlayer() == 4)
      {
        for(i = 0; i < handToExamine.length; i++)
        {
          do
          {
            selectedDiscard = p4Inventory.cardChoice(p4Inventory.getAllCards(), 1, -1);
          }
          while(selectedDiscard != null && selectedDiscard.getBackground() != "wood");
          if(selectedDiscard != null)
          {
            p4Inventory.removeCard(selectedDiscard.getUID());
            board.removeCard(card.getUID());
            p4Inventory.addCard(card);
          }
          break;
        }
      }
      menu.hideInformation();

      //Unless canceled
      if(selectedDiscard != null)
      {
        trash.addCard(selectedDiscard);
        //Log
        boardutils.logSilverActivation(card.getName());
        var userNames = gameState.getPlayerNickNames();
        gameState.writeToGameLog(qsTranslate("fakepainting", "<font color=\"paleturquoise\">The %1 discarded the %2 from %3's hand.</font>").arg(card.getName()).arg(selectedDiscard.getName()).arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
        gameState.writeToGameLog(qsTranslate("fakepainting", "<font color=\"paleturquoise\">The %1 has been returned to %2's hand.</font>").arg(card.getName()).arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
      }
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.complexCurrentValue();
}
