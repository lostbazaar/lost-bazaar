//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.dominoEntryConditions();
}

//This function handles all events
function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Deny active abilities
    gameState.setSilverActionsDisabledForTurn(1);
    //Logging
    boardutils.logCardPlacement(card.getName());
    var userNames = gameState.getPlayerNickNames();
    gameState.writeToGameLog(qsTranslate("cog", "%1 cannot use active abilities for the remaining turn.").arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1] && boardutils.checkForUnrotatable(card) == false)
  {
    cards = board.getAllCards(); //Retrieve all cards on the board
    //Ask the player in which direction should the cog rotate
    menu.showInformation(qsTranslate("cog", "Choose the direction for rotating the %1").arg(card.getName()), -1);

    //Stores the text of the choices
    choiceArray = [
    qsTranslate("cog", "Rotate counter-clockwise"),
    qsTranslate("cog", "Rotate clockwise")
    ];
    //Stores which choices are enabled and which aren't
    enableArray = ["true", "true"];
    //Stores the tooltips for the choices
    toolTipArray = ["", ""];

    //Get the player's choice
    var choice = menu.getChoice(choiceArray, enableArray, toolTipArray);
    //Hide the label
    menu.hideInformation();
    //Log
    boardutils.logSilverActivation(card.getName());

    //If the player chose counter-clockwise
    if(choice == 0)
    {
      //Rotate the cog
      var prevOwner = card.getPlayer()
      if(prevOwner == 1) {card.changeOwner(4)}
      if(prevOwner == 2) {card.changeOwner(3)}
      if(prevOwner == 3) {card.changeOwner(1)}
      if(prevOwner == 4) {card.changeOwner(2)}
      //Log
      gameState.writeToGameLog(qsTranslate("cog", "<font color=\"paleturquoise\">The %1 has been rotated by 90° counter-clockwise.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
    if(choice == 1)
    {
      //Rotate the cog
      var prevOwner = card.getPlayer()
      if(prevOwner == 1) {card.changeOwner(3)}
      if(prevOwner == 2) {card.changeOwner(4)}
      if(prevOwner == 3) {card.changeOwner(2)}
      if(prevOwner == 4) {card.changeOwner(1)}
      //Log
      gameState.writeToGameLog(qsTranslate("cog", "<font color=\"paleturquoise\">The %1 has been rotated by 90° clockwise.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }

    //Rotate the other cards
    for(i = 0; i < cards.length; i++) //Check all cards
    {
      //If the card is on an adjacent tile then it might be good
      if(Math.pow(card.getPosX()-cards[i].getPosX(), 2) + Math.pow(card.getPosY()-cards[i].getPosY(), 2) == 1)
      {
        //If the player chose counter-clockwise
        if(choice == 0 && boardutils.checkForUnrotatable(cards[i]) == false)
        {
          //Rotate the adjacent cards
          var affectedOwner = cards[i].getPlayer()
          if(affectedOwner == 1) {cards[i].changeOwner(3)}
          if(affectedOwner == 2) {cards[i].changeOwner(4)}
          if(affectedOwner == 3) {cards[i].changeOwner(2)}
          if(affectedOwner == 4) {cards[i].changeOwner(1)}
          //Log
          gameState.writeToGameLog(qsTranslate("cog", "<font color=\"paleturquoise\">The %1 rotated %2%3 %4 by 90° clockwise.</font>").arg(card.getName()).arg(boardutils.convertToABC(cards[i].getPosX())).arg(cards[i].getPosY()+1).arg(cards[i].getName()), [1, 2, 3, 4], 1, 0);
        }
        //If the player chose clockwise
        if(choice == 1 && boardutils.checkForUnrotatable(cards[i]) == false)
        {
          //Rotate the adjacent cards
          var affectedOwner = cards[i].getPlayer()
          if(affectedOwner == 1) {cards[i].changeOwner(4)}
          if(affectedOwner == 2) {cards[i].changeOwner(3)}
          if(affectedOwner == 3) {cards[i].changeOwner(1)}
          if(affectedOwner == 4) {cards[i].changeOwner(2)}
          //Log
          gameState.writeToGameLog(qsTranslate("cog", "<font color=\"paleturquoise\">The %1 rotated %2%3 %4 by 90° counter-clockwise.</font>").arg(card.getName()).arg(boardutils.convertToABC(cards[i].getPosX())).arg(cards[i].getPosY()+1).arg(cards[i].getName()), [1, 2, 3, 4], 1, 0);
        }
      }
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
