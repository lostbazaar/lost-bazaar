//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "turnStarted")
  {
    card.setShowFullSize(1)
  }

  var cardMirrored = 0;
  if(card.isGrayScale() == false)
  {
    cardMirrored = boardutils.itemMirrored(card);
  }


  // BEER MUG ////////////////////

  if(cardMirrored == "beermug" && eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() != eventData[1] && card.getPlayer() == (gameState.cardForUID(eventData[1])).getPlayer())
  {
    var EBC = (gameState.cardForUID(eventData[1])).getBaseValue();
    if(EBC > 0)
    {
      card.setCounter(card.getCounter()+EBC);
      //Log
      if(EBC > 1)
      {
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"khaki\">%1%2 %3 gained %4 counters.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()).arg(EBC), [1, 2, 3, 4], 1, 0);
      }
      else
      {
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"khaki\">%1%2 %3 gained 1 counter.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()), [1, 2, 3, 4], 1, 0);
      }
    }
  }

  if(cardMirrored == "beermug" && card.getCounter() > 6)
  {
    card.setGrayScale(1);
    gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"khaki\">The %1 has been disabled.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
  }


  // BOMB ////////////////////

  if(cardMirrored == "bomb" && eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    if(card.getCounter() > 0)
    {
      card.setCounter(card.getCounter()-1);
      //Log
      gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"khaki\">%1%2 %3 cast 1 counter.<font color=\"khaki\">").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
  }


  // FISHING ROD ////////////////////

  if(cardMirrored == "fishingrod" && eventData[0] == "endOfTurn" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    if(gameState.didPlayerPlaceCardOnBoardThisTurn() == false && boardutils.checkForUnpickable(card) == false)
    {
      cardsOK = [];
      cardsOK2 = [];
      cardsOKValue = [];
      if(board.getCardAtPosition(Point(card.getPosX()+1,card.getPosY())) != null && board.getCardAtPosition(Point(card.getPosX()+1,card.getPosY())).getPlayer() != card.getPlayer())
      {
        cardsOK.push(board.getCardAtPosition(Point(card.getPosX()+1,card.getPosY())));
      }
      if(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()+1)) != null && board.getCardAtPosition(Point(card.getPosX(),card.getPosY()+1)).getPlayer() != card.getPlayer())
      {
        cardsOK.push(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()+1)));
      }
      if(board.getCardAtPosition(Point(card.getPosX()-1,card.getPosY())) != null && board.getCardAtPosition(Point(card.getPosX()-1,card.getPosY())).getPlayer() != card.getPlayer())
      {
        cardsOK.push(board.getCardAtPosition(Point(card.getPosX()-1,card.getPosY())));
      }
      if(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()-1)) != null && board.getCardAtPosition(Point(card.getPosX(),card.getPosY()-1)).getPlayer() != card.getPlayer())
      {
        cardsOK.push(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()-1)));
      }
      for(i = 0; i < cardsOK.length; i++)
      {
        cardsOKValue.push(cardsOK[i].getCurrentValue());
      }
      var lowestValue = Math.min.apply(Math, cardsOKValue);
      for(i = 0; i < cardsOK.length; i++)
      {
        if(cardsOKValue[i] == lowestValue)
        {
          cardsOK2.push(cardsOK[i]);
        }
      }
      var caughtByRod = cardsOK2[rng.randomBetween(0, cardsOK2.length)];
      var userNames = gameState.getPlayerNickNames();
      if(boardutils.checkForUnpickable(caughtByRod) == false)
      {
        //Log
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"khaki\">%1%2 %3 returned to %4's hand.</khaki>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()).arg(userNames[card.getPlayer()-1]), [1, 2, 3, 4], 1, 0);
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"khaki\">%1%2 %3 returned %4%5 %6 to %7's hand.</khaki>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()).arg(boardutils.convertToABC(caughtByRod.getPosX())).arg(caughtByRod.getPosY()+1).arg(caughtByRod.getName()).arg(userNames[card.getPlayer()-1]), [1, 2, 3, 4], 1, 0);

        board.removeCard(card.getUID());
        if(gameState.getCurrentPlayer() == 1) {p1Inventory.addCard(card);}
        if(gameState.getCurrentPlayer() == 2) {p2Inventory.addCard(card);}
        if(gameState.getCurrentPlayer() == 3) {p3Inventory.addCard(card);}
        if(gameState.getCurrentPlayer() == 4) {p4Inventory.addCard(card);}

        board.removeCard(caughtByRod.getUID());
        if(gameState.getCurrentPlayer() == 1) {p1Inventory.addCard(caughtByRod);}
        if(gameState.getCurrentPlayer() == 2) {p2Inventory.addCard(caughtByRod);}
        if(gameState.getCurrentPlayer() == 3) {p3Inventory.addCard(caughtByRod);}
        if(gameState.getCurrentPlayer() == 4) {p4Inventory.addCard(caughtByRod);}
      }
    }
  }


  // FLAG ////////////////////

  if(cardMirrored == "flag" && eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    cardsOK = [];
    cardsOK2 = [];
    cardsOKValue = [];
    if(board.getCardAtPosition(Point(card.getPosX()+1,card.getPosY())) != null)
    {
      cardsOK.push(board.getCardAtPosition(Point(card.getPosX()+1,card.getPosY())));
    }
    if(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()+1)) != null)
    {
      cardsOK.push(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()+1)));
    }
    if(board.getCardAtPosition(Point(card.getPosX()-1,card.getPosY())) != null)
    {
      cardsOK.push(board.getCardAtPosition(Point(card.getPosX()-1,card.getPosY())));
    }
    if(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()-1)) != null)
    {
      cardsOK.push(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()-1)));
    }
    if(cardsOK.length > 0)
    {
      for(i = 0; i < cardsOK.length; i++)
      {
        cardsOKValue.push(cardsOK[i].getCurrentValue());
      }
      var lowestValue = Math.min.apply(Math, cardsOKValue);
      for(i = 0; i < cardsOK.length; i++)
      {
        if(cardsOKValue[i] == lowestValue)
        {
          cardsOK2.push(cardsOK[i]);
        }
      }
      var rotatedByFlag = cardsOK2[rng.randomBetween(0, cardsOK2.length)];
      if(boardutils.checkForUnrotatable(rotatedByFlag) == false)
      {
        var prevOwner = rotatedByFlag.getPlayer();
        if(prevOwner == 1) {rotatedByFlag.changeOwner(2)}
        if(prevOwner == 2) {rotatedByFlag.changeOwner(1)}
        if(prevOwner == 3) {rotatedByFlag.changeOwner(4)}
        if(prevOwner == 4) {rotatedByFlag.changeOwner(3)}
        //Log
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"khaki\">%1%2 %3 rotated %4%5 %6 by 180°.</khaki>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()).arg(boardutils.convertToABC(rotatedByFlag.getPosX())).arg(rotatedByFlag.getPosY()+1).arg(rotatedByFlag.getName()), [1, 2, 3, 4], 1, 0);
      }
    }
    card.setGrayScale(1);
    gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"khaki\">%1%2 %3 has been disabled.</khaki>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()), [1, 2, 3, 4], 1, 0);
  }


  // HOURGLASS ////////////////////

  if(cardMirrored == "hourglass" && eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    if(card.getCounter() > 0)
    {
      card.setCounter(card.getCounter()-1)
      //Log
      gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"khaki\">%1%2 %3 cast 1 counter.<font color=\"khaki\">").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
  }

  if(cardMirrored == "hourglass" && eventData[0] == "endOfTurn" && card.getPlayer() == gameState.getCurrentPlayer() &&  card.getCounter() == 0 &&  boardutils.checkForUnpickable(card) == false)
  {
    //Log
    var userNames = gameState.getPlayerNickNames();
    gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"khaki\">%1%2 %3 has been returned to %4's hand.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()).arg(userNames[card.getPlayer()-1]), [1, 2, 3, 4], 1, 0);

    board.removeCard(card.getUID());
    if(card.getPlayer() == 1) {p1Inventory.addCard(card);}
    if(card.getPlayer() == 2) {p2Inventory.addCard(card);}
    if(card.getPlayer() == 3) {p3Inventory.addCard(card);}
    if(card.getPlayer() == 4) {p4Inventory.addCard(card);}
  }


  // PIGGY BANK ////////////////////

  if(cardMirrored == "piggybank" && eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    //Log
    gameState.writeToGameLog(qsTranslate("piggybank", "<font color=\"khaki\">%1%2 %3 gained 1 counter.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()), [1, 2, 3, 4], 1, 0);
    if(card.getCounter() < 6)
    {
      card.setCounter(card.getCounter()+1);
    }
  }
  if(cardMirrored == "piggybank")
  {
    if(card.getCounter() > 6)
    {
      card.setCounter(6);
    }
  }


  // SCISSORS ////////////////////

  if(cardMirrored == "scissors" && eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    if(card.getCounter() < 12)
    {
      card.setCounter(card.getCounter()+1);

      //Log
      gameState.writeToGameLog(qsTranslate("scissors", "<font color=\"khaki\">%1%2 %3 gained 1 counter.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
  }

  if(cardMirrored == "scissors")
  {
    var xCorri = 0;
    var yCorri = 0;
    if(card.getPlayer() == 1) {xCorri = -1;}
    if(card.getPlayer() == 2) {xCorri = 1;}
    if(card.getPlayer() == 3) {yCorri = -1;}
    if(card.getPlayer() == 4) {yCorri = 1;}

    //Do it once, but repeat it if conditions were met
    for(i = 0; i < 1; i++)
    {
      var CTC = board.getCardAtPosition(Point(card.getPosX()+xCorri,card.getPosY()+yCorri));
      if(CTC != null && CTC.getCurrentValue() <= card.getCounter())
      {
        if(boardutils.checkForUntrashable(CTC) == false)
        {
          var hitByDamage = CTC;
          damageOutcome = boardutils.damageEvents(card.getID(), hitByDamage, hitByDamage.getPosX(), hitByDamage.getPosY())
          var explodingItems = damageOutcome[0];

          //Log
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"khaki\">%1%2 %3 destroyed %4%5 %6.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()).arg(boardutils.convertToABC(hitByDamage.getPosX())).arg(hitByDamage.getPosY()+1).arg(hitByDamage.getName()), [1, 2, 3, 4], 1, 0);

          board.moveCardToTrash(CTC.getUID());
          var scissorsPrevPosX = card.getPosX();
          var scissorsPrevPosY = card.getPosY();

          if(boardutils.checkForImmovable(card) == false)
          {
            //Log
            gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"khaki\">The %1 has been moved to %2%3.</font>").arg(card.getName()).arg(boardutils.convertToABC(card.getPosX()+xCorri)).arg(card.getPosY()+yCorri+1), [1, 2, 3, 4], 1, 0);

            board.moveCard(card.getPosition(), Point(card.getPosX()+xCorri,card.getPosY()+yCorri));
          }

          if(explodingItems.length > 1)
          {
            //It has to be decided whether the Scissors remain intact or not
            var scissorsSurvive = true;
            if(scissorsPrevPosX = card.getPosX() || scissorsPrevPosY != card.getPosY())
            {
              for(ctc = 2; ctc < 10; ctc = ctc+2)
              {
                var xCorri2 = 0;
                var yCorri2 = 0;
                if(ctc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri2 = 1;}
                if(ctc == 4 && card.getPosX() > 0) {xCorri2 = -1;}
                if(ctc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri2 = 1;}
                if(ctc == 8 && card.getPosY() > 0) {yCorri2 = -1;}
                TTC = Point(card.getPosX()+xCorri2,card.getPosY()+yCorri2);
                CTC = board.getCardAtPosition(TTC);
                if(CTC != null && CTC.getID() == "bomb" && CTC.isGrayScale() == false && boardutils.checkForUntrashable(CTC) == false)
                {
                  //There was another adjacent trashable Bomb
                  var scissorsSurvive = false;
                  break;
                }
              }
            }

            //Bomb events
            for(boomOrder = 1; boomOrder < explodingItems.length; boomOrder++)
            {
              var BBV = gameState.cardForUID(damageOutcome[0][boomOrder]);
              if(damageOutcome[0][boomOrder] != card.getUID() || scissorsSurvive == false)
              {
                board.moveCardToTrash(damageOutcome[0][boomOrder]);

                //Log
                gameState.writeToGameLog(qsTranslate("bomb", "<font color=\"khaki\">The %1 destroyed %2%3 %4.</font>").arg(hitByDamage.getName()).arg(boardutils.convertToABC(BBV.getPosX())).arg(BBV.getPosY()+1).arg(BBV.getName()), [1, 2, 3, 4], 1, 0);
              }
            }
          }
          //Piggy Bank events
          if(damageOutcome[1] > 0) {for(drawCount = 0; drawCount < damageOutcome[1]; drawCount++) {gameState.drawCard(1, 0, 0, 0, 0, "khaki");}}
          if(damageOutcome[2] > 0) {for(drawCount = 0; drawCount < damageOutcome[2]; drawCount++) {gameState.drawCard(2, 0, 0, 0, 0, "khaki");}}
          if(damageOutcome[3] > 0) {for(drawCount = 0; drawCount < damageOutcome[3]; drawCount++) {gameState.drawCard(3, 0, 0, 0, 0, "khaki");}}
          if(damageOutcome[4] > 0) {for(drawCount = 0; drawCount < damageOutcome[4]; drawCount++) {gameState.drawCard(4, 0, 0, 0, 0, "khaki");}}

          //Repeat the loop
          i--;
        }

        if(card.getCounter() == 1)
        {
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"khaki\">The %1 cast 1 counter.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
          card.setCounter(0);
        }
        if(card.getCounter() > 1)
        {
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"khaki\">The %1 cast %2 counters.</font>").arg(card.getName()).arg(card.getCounter()), [1, 2, 3, 4], 1, 0);
          card.setCounter(0);
        }
      }
    }
  }


  // SNOOKER BALL ////////////////////

  if(cardMirrored == "snookerball" && eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    if(card.getCounter() < 12)
    {
      var userNames = gameState.getPlayerNickNames();
      if(boardutils.isSixFix(gameState.getCurrentPlayer()) == true)
      {
        var diceRoll = 6;
      }
      else
      {
        var diceRoll = rng.randomBetween(1, 7);
      }
      card.setCounter(card.getCounter()+diceRoll);
      menu.showInformation(qsTranslate("handmirror", "%1%2 %3: %4").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()).arg(boardutils.numberToFace(diceRoll)), 4000);
      //Log
      gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"khaki\">%1 rolled a die: %2.</font>").arg(userNames[gameState.getCurrentPlayer()-1]).arg(diceRoll), [1, 2, 3, 4], 1, 0);
      if(diceRoll == 1)
      {
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"khaki\">%1%2 %3 gained 1 counter.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()), [1, 2, 3, 4], 1, 0);
      }
      else
      {
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"khaki\">%1%2 %3 gained %4 counters.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()).arg(diceRoll), [1, 2, 3, 4], 1, 0);
      }
    }
    //It gets capped at 12
    if(card.getCounter() > 11)
    {
      card.setCounter(12);
    }
  }


  // BEER MUG ////////////////////

  if(cardMirrored == "beermug" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    cards = board.getAllCards(); //Retrieve all cards on the board
    tilesOK = []; //This will store the tiles of the cards which can be moved
    if(card.getCounter() == 6)
    {
      for(i = 0; i < cards.length; i++) //Check all cards
      {
        if(boardutils.checkForUnrotatable(cards[i]) == false && boardutils.isUntargetableBySilver(cards[i]) == false && cards[i].getCurrentValue() == 1)
        {
          tilesOK.push(cards[i].getPosition());
        }
      }
    }

    if(tilesOK.length > 0)
    {
      menu.showInformation(qsTranslate("handmirror", "Choose which item would you like to rotate"), -1);
      selectedTile = boardutils.tileChoice(tilesOK, 1);
      menu.hideInformation();
      //Unless canceled
      if(selectedTile.x != -1)
      {
        selectedCard = board.getCardAtPosition(selectedTile)
        card.setCounter(card.getCounter()-6);
        var prevOwner = selectedCard.getPlayer();
        if(prevOwner == 1) {selectedCard.changeOwner(2);}
        if(prevOwner == 2) {selectedCard.changeOwner(1);}
        if(prevOwner == 3) {selectedCard.changeOwner(4);}
        if(prevOwner == 4) {selectedCard.changeOwner(3);}

        //Log
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 cast 6 counters.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 rotated %2%3 %4 by 180°.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);
      }
    }
    else
    {
      menu.showInformation(qsTranslate("handmirror", "You cannot use this ability!"));
    }
  }


  // BIRD FEEDER ////////////////////

  if(cardMirrored == "birdfeeder" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    cards = board.getAllCards(); //Retrieve all cards on the board
    cardsOK = []; //This will store the cards which can be targeted
    for(i = 0; i < cards.length; i++) //Check all cards
    {
      //If the card is adjacent to the Bird feeder
      if(Math.pow(card.getPosX()-cards[i].getPosX(), 2) + Math.pow(card.getPosY()-cards[i].getPosY(), 2) == 1)
      {
        //If it is targetable by active abilities - BIRD FEEDER ITSELF IS AN EXCEPTION
        if(cards[i].getID() != "mask" || (cards[i].getID() == "handmirror" && boardutils.itemMirrored(cards[i]) == "mask"))
        {
          //Store the coordinates of the good card
          cardsOK.push(cards[i].getPosition());
        }
      }
    }
    if(cardsOK.length > 0 && card.getCounter() > 0)
    {
      //Ask the player which card would he like to move
      menu.showInformation(qsTranslate("handmirror", "Choose which item would you like to place the counter on"), -1);
      //The player chooses one from the good cards
      selectedTile = boardutils.tileChoice(cardsOK, 1);
      selectedCard = board.getCardAtPosition(selectedTile);
      //Hide the 'Choose which card...' label
      menu.hideInformation();
      //Unless canceled
      if(selectedTile.x != -1)
      {
        //Log
        boardutils.logSilverActivation(card.getName());

        //Take down the 1 counter
        card.setCounter(card.getCounter()-1);
        //Add 1 counter
        selectedCard.setCounter(selectedCard.getCounter()+1);
        //It gets capped at 12
        if(selectedCard.getCounter() > 11)
        {
          selectedCard.setCounter(12);
        }
        //Log
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 transfered 1 counter from itself to %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);
        //Check for Golf Club
        if( ( (selectedCard.getID() == "golfclub" && selectedCard.isGrayScale() == false) || (selectedCard.getID() == "handmirror" && boardutils.itemMirrored(selectedCard) == "golfclub") ) && boardutils.checkForUnrotatable(selectedCard) == false && selectedCard.getPlayer() != gameState.getCurrentPlayer())
        {
          selectedCard.changeOwner(gameState.getCurrentPlayer());
          //Log
          var userNames = gameState.getPlayerNickNames();
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"khaki\">%1%2 %3 has been rotated toward %4.</font>").arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()).arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
        }
        //Crop
        card.setShowFullSize(0);
      }
    }
    else
    {
      menu.showInformation(qsTranslate("handmirror", "You cannot use this ability!"));
    }
  }


  // BOMB ////////////////////

  if(cardMirrored == "bomb" && card.getCounter() == 0 && boardutils.checkForUntrashable(card) == false)
  {

    cards = board.getAllCards(); //Retrieve all cards on the board
    cardsBomb = []; //Store the bomb cards
    cardsBombX = []; //Store the X coordinate of bombs
    cardsBombY = []; //Store the Y coordinate of bombs
    cardsUIDBOOM = []; //Stores UIDs for exploding cards
    var pigDraw1 = 0;
    var pigDraw2 = 0;
    var pigDraw3 = 0;
    var pigDraw4 = 0;

    //Include the initiator on the list

    cardsBomb.push(card);
    cardsBombX.push(card.getPosX());
    cardsBombY.push(card.getPosY());
    cardsUIDBOOM.push(card.getUID());

    for(bN = 0; bN < cardsBombX.length; bN++)
    {
      for(ctc = 2; ctc < 10; ctc = ctc+2)
      {
        var xCorri = 0;
        var yCorri = 0;
        if(ctc == 2 && cardsBombY[bN] < board.getHeight() - 1) {yCorri = 1;}
        if(ctc == 4 && cardsBombX[bN] > 0) {xCorri = -1;}
        if(ctc == 6 && cardsBombX[bN] < board.getWidth() - 1) {xCorri = 1;}
        if(ctc == 8 && cardsBombY[bN] > 0) {yCorri = -1;}
        CTC = board.getCardAtPosition(Point(cardsBombX[bN]+xCorri,cardsBombY[bN]+yCorri));
        //If the card is trashable then it can be marked
        if(CTC != null && CTC != cardsBomb[bN] && boardutils.checkForUntrashable(CTC) == false)
        {
          //Check whether it was marked previously
          var alreadyMarked = false;
          for(eN = 0; eN < cardsUIDBOOM.length; eN++)
          {
            if(cardsUIDBOOM[eN] == CTC.getUID())
            {
              alreadyMarked = true;
            }
          }
          if(alreadyMarked == false)
          {
            //Store the coordinates and UIDs of the good card
            cardsUIDBOOM.push(CTC.getUID());
            //If the marked card is a Bomb, add it to the chain reaction
            if((CTC.getID() == "bomb" && CTC.isGrayScale() == false) || (CTC.getID() == "handmirror" && boardutils.itemMirrored(CTC) == "bomb"))
            {
              cardsBomb.push(CTC);
              cardsBombX.push(CTC.getPosX());
              cardsBombY.push(CTC.getPosY());
            }
            //If the marked card is a Piggy Bank, remember the amount of counters on it
            if((CTC.getID() == "piggybank" && CTC.isGrayScale() == false) || (CTC.getID() == "handmirror" && boardutils.itemMirrored(CTC) == "piggybank"))
            {
              switch(CTC.getPlayer())
              {
                case 1:
                  pigDraw1 = pigDraw1 + CTC.getCounter();
                  break;
                case 2:
                  pigDraw2 = pigDraw2 + CTC.getCounter();
                  break;
                case 3:
                  pigDraw3 = pigDraw3 + CTC.getCounter();
                  break;
                case 4:
                  pigDraw4 = pigDraw4 + CTC.getCounter();
                  break;
              }
            }
          }
        }
      }
    }
    //Log
    gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"khaki\">%1%2 %3 has been destroyed.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()), [1, 2, 3, 4], 1, 0);
    //Remove the initiator
    board.moveCardToTrash(cardsUIDBOOM[0]);
    //Remove marked items
    if(cardsUIDBOOM.length > 1)
    {
      for(boomOrder = 1; boomOrder < cardsUIDBOOM.length; boomOrder++)
      {
        //Log
        var explodingCard = gameState.cardForUID(cardsUIDBOOM[boomOrder])
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"khaki\">The %1 destroyed %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(explodingCard.getPosX())).arg(explodingCard.getPosY()+1).arg(explodingCard.getName()), [1, 2, 3, 4], 1, 0);
        board.moveCardToTrash(cardsUIDBOOM[boomOrder]);
      }
    }
    if(pigDraw1 > 0) {for(drawCount = 0; drawCount < pigDraw1; drawCount++) {gameState.drawCard(1, 0, 0, 0, 0, "khaki");}}
    if(pigDraw2 > 0) {for(drawCount = 0; drawCount < pigDraw2; drawCount++) {gameState.drawCard(2, 0, 0, 0, 0, "khaki");}}
    if(pigDraw3 > 0) {for(drawCount = 0; drawCount < pigDraw3; drawCount++) {gameState.drawCard(3, 0, 0, 0, 0, "khaki");}}
    if(pigDraw4 > 0) {for(drawCount = 0; drawCount < pigDraw4; drawCount++) {gameState.drawCard(4, 0, 0, 0, 0, "khaki");}}

  }


  // COFFEE GRINDER ////////////////////

  if(cardMirrored == "coffeegrinder" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    //Check for hands not being empty
    if((gameState.getCurrentPlayer() == 1 && p1Inventory.isEmpty() == 0) || (gameState.getCurrentPlayer() == 2 && p2Inventory.isEmpty() == 0) || (gameState.getCurrentPlayer() == 3 && p3Inventory.isEmpty() == 0) || (gameState.getCurrentPlayer() == 1 && p1Inventory.isEmpty() == 0))
    {
      //Ask the player which card to discard
      menu.showInformation(qsTranslate("handmirror", "Choose an item to discard"), -1);
      //The player chooses one
      if(gameState.getCurrentPlayer() == 1)
      {
        selectedDiscard = p1Inventory.cardChoice(p1Inventory.getAllCards(), 1, -1);
        if(selectedDiscard != null)
        {
          p1Inventory.removeCard(selectedDiscard.getUID());
        }
      }
      if(gameState.getCurrentPlayer() == 2)
      {
        selectedDiscard = p2Inventory.cardChoice(p2Inventory.getAllCards(), 1, -1);
        if(selectedDiscard != null)
        {
          p2Inventory.removeCard(selectedDiscard.getUID());
        }
      }
      if(gameState.getCurrentPlayer() == 3)
      {
        selectedDiscard = p3Inventory.cardChoice(p3Inventory.getAllCards(), 1, -1);
        if(selectedDiscard != null)
        {
          p3Inventory.removeCard(selectedDiscard.getUID());
        }
      }
      if(gameState.getCurrentPlayer() == 4)
      {
        selectedDiscard = p4Inventory.cardChoice(p4Inventory.getAllCards(), 1, -1);
        if(selectedDiscard != null)
        {
          p4Inventory.removeCard(selectedDiscard.getUID());
        }
      }

      menu.hideInformation();
      //Unless canceled
      if(selectedDiscard != null)
      {
        //Sound effect
        var randomSound = rng.randomBetween(1, 7);
        switch(randomSound)
        {
          case 1: gameState.playSoundEffect("grind-1.wav", 1); break;
          case 2: gameState.playSoundEffect("grind-2.wav", 1); break;
          case 3: gameState.playSoundEffect("grind-3.wav", 1); break;
          case 4: gameState.playSoundEffect("grind-4.wav", 1); break;
          case 5: gameState.playSoundEffect("grind-5.wav", 1); break;
          case 6: gameState.playSoundEffect("grind-6.wav", 1); break;
        }
        //It gets discarded
        trash.addCard(selectedDiscard);
        //Log
        var userNames = gameState.getPlayerNickNames();
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 discarded the %2 from %3's hand.</font>").arg(card.getName()).arg(selectedDiscard.getName()).arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
        //A card is drawn for the player
        gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "paleturquoise");
        //An informational message is displayed (it will disappear automatically)
        menu.showInformation(qsTranslate("handmirror", "You have drawn an item!"));
        //Crop
        card.setShowFullSize(0);
      }
    }
    else
    {
      menu.showInformation(qsTranslate("handmirror", "No items in hand to discard!"));
    }
  }


  // COG ////////////////////

  if(cardMirrored == "cog" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1] && boardutils.checkForUnrotatable(card) == false)
  {
    cards = board.getAllCards(); //Retrieve all cards on the board
    //Ask the player in which direction should the cog rotate
    menu.showInformation(qsTranslate("handmirror", "Choose the direction for rotating the %1").arg(card.getName()), -1);

    //Stores the text of the choices
    choiceArray = [
    qsTranslate("handmirror", "Rotate counter-clockwise"),
    qsTranslate("handmirror", "Rotate clockwise")
    ];
    //Stores which choices are enabled and which aren't
    enableArray = ["true", "true"];
    //Stores the tooltips for the choices
    toolTipArray = ["", ""];

    //Get the player's choice
    var choice = menu.getChoice(choiceArray, enableArray, toolTipArray);
    //Hide the label
    menu.hideInformation();
    //Log
    boardutils.logSilverActivation(card.getName());

    //If the player chose counter-clockwise
    if(choice == 0)
    {
      //Rotate the cog
      var prevOwner = card.getPlayer()
      if(prevOwner == 1) {card.changeOwner(4)}
      if(prevOwner == 2) {card.changeOwner(3)}
      if(prevOwner == 3) {card.changeOwner(1)}
      if(prevOwner == 4) {card.changeOwner(2)}
      //Log
      gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 has been rotated by 90° counter-clockwise.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
    if(choice == 1)
    {
      //Rotate the cog
      var prevOwner = card.getPlayer()
      if(prevOwner == 1) {card.changeOwner(3)}
      if(prevOwner == 2) {card.changeOwner(4)}
      if(prevOwner == 3) {card.changeOwner(2)}
      if(prevOwner == 4) {card.changeOwner(1)}
      //Log
      gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 has been rotated by 90° clockwise.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }

    //Rotate the other cards
    for(i = 0; i < cards.length; i++) //Check all cards
    {
      //If the card is on an adjacent tile then it might be good
      if(Math.pow(card.getPosX()-cards[i].getPosX(), 2) + Math.pow(card.getPosY()-cards[i].getPosY(), 2) == 1)
      {
        //If the player chose counter-clockwise
        if(choice == 0 && boardutils.checkForUnrotatable(cards[i]) == false)
        {
          //Rotate the adjacent cards
          var affectedOwner = cards[i].getPlayer()
          if(affectedOwner == 1) {cards[i].changeOwner(3)}
          if(affectedOwner == 2) {cards[i].changeOwner(4)}
          if(affectedOwner == 3) {cards[i].changeOwner(2)}
          if(affectedOwner == 4) {cards[i].changeOwner(1)}
          //Log
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 rotated %2%3 %4 by 90° clockwise.</font>").arg(card.getName()).arg(boardutils.convertToABC(cards[i].getPosX())).arg(cards[i].getPosY()+1).arg(cards[i].getName()), [1, 2, 3, 4], 1, 0);
        }
        //If the player chose clockwise
        if(choice == 1 && boardutils.checkForUnrotatable(cards[i]) == false)
        {
          //Rotate the adjacent cards
          var affectedOwner = cards[i].getPlayer()
          if(affectedOwner == 1) {cards[i].changeOwner(4)}
          if(affectedOwner == 2) {cards[i].changeOwner(3)}
          if(affectedOwner == 3) {cards[i].changeOwner(1)}
          if(affectedOwner == 4) {cards[i].changeOwner(2)}
          //Log
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 rotated %2%3 %4 by 90° counter-clockwise.</font>").arg(card.getName()).arg(boardutils.convertToABC(cards[i].getPosX())).arg(cards[i].getPosY()+1).arg(cards[i].getName()), [1, 2, 3, 4], 1, 0);
        }
      }
    }
  }

  // EMPTY CAN ////////////////////

  if(cardMirrored == "emptycan" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    if(boardutils.getEmptyTiles().length > 0)
    {
      //Check for hands not being empty
      if((gameState.getCurrentPlayer() == 1 && p1Inventory.isEmpty() == 0) || (gameState.getCurrentPlayer() == 2 && p2Inventory.isEmpty() == 0) || (gameState.getCurrentPlayer() == 3 && p3Inventory.isEmpty() == 0) || (gameState.getCurrentPlayer() == 4 && p1Inventory.isEmpty() == 0))
      {
        if(boardutils.checkForImmovable(card) == false)
        {
          //Ask the player which card to discard
          menu.showInformation(qsTranslate("handmirror", "Choose an item to discard"), -1);
          //The player chooses one
          switch(gameState.getCurrentPlayer())
          {
            case 1: selectedDiscard = p1Inventory.cardChoice(p1Inventory.getAllCards(), 1, -1); break;
            case 2: selectedDiscard = p2Inventory.cardChoice(p2Inventory.getAllCards(), 1, -1); break;
            case 3: selectedDiscard = p3Inventory.cardChoice(p3Inventory.getAllCards(), 1, -1); break;
            case 4: selectedDiscard = p4Inventory.cardChoice(p4Inventory.getAllCards(), 1, -1); break;
          }
          menu.hideInformation();
          //Unless canceled
          if(selectedDiscard != null)
          {
            switch(gameState.getCurrentPlayer())
            {
              case 1: p1Inventory.removeCard(selectedDiscard.getUID()); break;
              case 2: p2Inventory.removeCard(selectedDiscard.getUID()); break;
              case 3: p3Inventory.removeCard(selectedDiscard.getUID()); break;
              case 4: p4Inventory.removeCard(selectedDiscard.getUID()); break;
            }
            //It gets discarded
            trash.addCard(selectedDiscard);
            //Ask the player where to move the can
            menu.showInformation(qsTranslate("handmirror", "Choose where to move the %1").arg(card.getName()), -1);
            //The player chooses one from the good positions
            selectedPos = boardutils.tileChoice(boardutils.getEmptyTiles(), 0);
            //We got the choice, hide the info
            menu.hideInformation();
            //Store the previous position for the log
            var prevPosX = card.getPosX();
            var prevPosY = card.getPosY();
            //Sound effect
            var randomSound = rng.randomBetween(1, 6);
            switch(randomSound)
            {
              case 1: gameState.playSoundEffect("can-1.wav", 1); break;
              case 2: gameState.playSoundEffect("can-2.wav", 1); break;
              case 3: gameState.playSoundEffect("can-3.wav", 1); break;
              case 4: gameState.playSoundEffect("can-4.wav", 1); break;
              case 5: gameState.playSoundEffect("can-5.wav", 1); break;
            }
            //Move the card
            board.moveCard(card.getPosition(), selectedPos);
            //Log
            boardutils.logSilverActivation(card.getName());
            gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 discarded the %2.</font>").arg(card.getName()).arg(selectedDiscard.getName()), [1, 2, 3, 4], 1, 0);
            gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 has been moved to %2%3.</font>").arg(card.getName()).arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1), [1, 2, 3, 4], 1, 0);
          }
        }
        else
        {
          menu.showInformation(qsTranslate("handmirror", "You cannot move this item!"));
        }
      }
      else
      {
        menu.showInformation(qsTranslate("handmirror", "No items in hand to discard!"));
      }
    }
    else
    {
      menu.showInformation(qsTranslate("handmirror", "Nowhere to move!"));
    }
  }


  // ENVELOPE ////////////////////

  if(cardMirrored == "envelope" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    cards = board.getAllCards();
    var tilesOK1 = [];
    var tilesOK2 = [];

    for(ctc = 2; ctc < 10; ctc = ctc+2)
    {
      var xCorri = 0;
      var yCorri = 0;
      if(ctc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
      if(ctc == 4 && card.getPosX() > 0) {xCorri = -1;}
      if(ctc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
      if(ctc == 8 && card.getPosY() > 0) {yCorri = -1;}
      TTC = Point(card.getPosX()+xCorri,card.getPosY()+yCorri);
      CTC = board.getCardAtPosition(TTC);
      if(CTC != null && CTC.getUID() != card.getUID() && boardutils.isUntargetableBySilver(CTC) == false)
      {
        if(CTC.getPlayer() == card.getPlayer() && CTC.getCurrentValue() > CTC.getBaseValue() && boardutils.checkForUntrashable(CTC) == false)
        {
          tilesOK1.push(TTC);
        }
        if(boardutils.checkForImmovable(CTC) == false && boardutils.checkForImmovable(card) == false && card.getCounter() >= CTC.getCurrentValue())
        {
          tilesOK2.push(TTC);
        }
      }
    }

    if(tilesOK1.length > 0 || tilesOK2.length > 0)
    {
      menu.showInformation(qsTranslate("handmirror", "Select an ability"), -1);
      choiceArray = [
      qsTranslate("envelope", "Destroy an adjacent item you own"),
      qsTranslate("envelope", "Swap tiles with an adjacent item")
      ];
      //Stores which choices are enabled and which aren't
      if(tilesOK1.length > 0 && tilesOK2.length > 0)
      {
        enableArray = ["true", "true"];
      }
      if(tilesOK1.length <= 0 && tilesOK2.length > 0)
      {
        enableArray = ["false", "true"];
      }
      if(tilesOK1.length > 0 && tilesOK2.length <= 0)
      {
        enableArray = ["true", "false"];
      }
      //Stores the tooltips for the choices
      toolTipArray = ["", ""];

      var choice = menu.getChoice(choiceArray, enableArray, toolTipArray);
      menu.hideInformation();

      if(choice == 0)
      {
        menu.showInformation(qsTranslate("handmirror", "Choose an item to trash"), -1);
        selectedTile = boardutils.tileChoice(tilesOK1, 0);
        selectedCard = board.getCardAtPosition(selectedTile)
        menu.hideInformation();
        var addedCounter = (selectedCard.getCurrentValue() - selectedCard.getBaseValue()) * 2;

        //Log
        boardutils.logSilverActivation(card.getName());
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 destroyed %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);

        var hitByDamage = selectedCard;
        damageOutcome = boardutils.damageEvents(card.getID(), hitByDamage, hitByDamage.getPosX(), hitByDamage.getPosY())
        var explodingItems = damageOutcome[0];

        board.moveCardToTrash(selectedCard.getUID())
        card.setCounter(card.getCounter() + addedCounter);

        //Log
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 gained %2 counters.</font>").arg(card.getName()).arg(addedCounter), [1, 2, 3, 4], 1, 0);

        if(explodingItems.length > 1)
        {
          //Bomb events
          for(boomOrder = 1; boomOrder < explodingItems.length; boomOrder++)
          {
            var BBV = gameState.cardForUID(damageOutcome[0][boomOrder]);

            //Log
            gameState.writeToGameLog(qsTranslate("bomb", "<font color=\"khaki\">The %1 destroyed %2%3 %4.</font>").arg(hitByDamage.getName()).arg(boardutils.convertToABC(BBV.getPosX())).arg(BBV.getPosY()+1).arg(BBV.getName()), [1, 2, 3, 4], 1, 0);

            board.moveCardToTrash(damageOutcome[0][boomOrder]);
          }
        }
        //Piggy Bank events
        if(damageOutcome[1] > 0) {for(drawCount = 0; drawCount < damageOutcome[1]; drawCount++) {gameState.drawCard(1, 0, 0, 0, 0, "khaki");}}
        if(damageOutcome[2] > 0) {for(drawCount = 0; drawCount < damageOutcome[2]; drawCount++) {gameState.drawCard(2, 0, 0, 0, 0, "khaki");}}
        if(damageOutcome[3] > 0) {for(drawCount = 0; drawCount < damageOutcome[3]; drawCount++) {gameState.drawCard(3, 0, 0, 0, 0, "khaki");}}
        if(damageOutcome[4] > 0) {for(drawCount = 0; drawCount < damageOutcome[4]; drawCount++) {gameState.drawCard(4, 0, 0, 0, 0, "khaki");}}

      }

      if(choice == 1)
      {
        menu.showInformation(qsTranslate("handmirror", "Choose which item would you like to move"), -1);
        selectedTile = boardutils.tileChoice(tilesOK2, 0);
        selectedCard = board.getCardAtPosition(selectedTile)
        menu.hideInformation();
        var counterCost = selectedCard.getCurrentValue();

        //Log
        boardutils.logSilverActivation(card.getName());
        if(counterCost < 2)
        {
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 cast %2 counter.</font>").arg(card.getName()).arg(counterCost), [1, 2, 3, 4], 1, 0);
        }
        else
        {
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 cast %2 counters.</font>").arg(card.getName()).arg(counterCost), [1, 2, 3, 4], 1, 0);
        }
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 switched tiles with %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);

        card.setCounter(card.getCounter() - counterCost);
        board.swapCards(card.getPosition(), selectedCard.getPosition());
      }
    }
    else
    {
      menu.showInformation(qsTranslate("handmirror", "You cannot use this ability!"));
    }
  }


  // GIFT BOX ////////////////////

  if(cardMirrored == "giftbox" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    //You cannot trash it if it is on safe tile
    if(boardutils.checkForUntrashable(card) == false)
    {
      //Log
      var userNames = gameState.getPlayerNickNames();
      gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 has been destroyed.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);

      gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "paleturquoise");
      gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "paleturquoise");
      board.moveCardToTrash(card.getUID());
      menu.showInformation(qsTranslate("handmirror", "You have drawn 2 items!"));
    }
  }


  // GLUE TUBE ////////////////////

  if(cardMirrored == "gluetube" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    if((trash.getAllCards()).length > 0)
    {
      var firstAbilityAvailable = false
      var secondAbilityAvailable = false
      var trashCards = trash.getAllCards()
      for(i = 0; i < trashCards.length; i++)
      {
        if(trashCards[i].getBaseValue() > 0)
        {
          firstAbilityAvailable = true;
          break;
        }
      }
      if(card.getCounter() >= 11)
      {
        for(i = 0; i < trashCards.length; i++)
        {
          if(trashCards[i].hasIcon("tcan") == false)
          {
            secondAbilityAvailable = true;
            break;
          }
        }
      }

      if(firstAbilityAvailable == true || secondAbilityAvailable == true)
      {
        menu.showInformation(qsTranslate("handmirror", "Choose from the abilities of the %1").arg(card.getName()), -1);
        choiceArray = [
        qsTranslate("gluetube", "Return an item from the trash to the storage"),
        qsTranslate("gluetube", "Reclaim an item to your hand")
        ];
        enableArray = [firstAbilityAvailable == true ? "true" : "false", secondAbilityAvailable == true ? "true" : "false"];
        toolTipArray = [firstAbilityAvailable == true ? "" : qsTranslate("gluetube", "No item with positive value in trash"), secondAbilityAvailable == true ? "" : qsTranslate("gluetube", "Not enough counters / No reclaimable item in the trash")];
        var choice = menu.getChoice(choiceArray, enableArray, toolTipArray);
        menu.hideInformation();

        if(choice == 0)
        {
          //Log
          boardutils.logSilverActivation(card.getName());

          menu.showInformation(qsTranslate("handmirror", "Choose an item to recycle"), -1);
          do
          {
            var selectedToRecycle = trash.cardChoice(trashCards, 0, 1);
          }
          while(selectedToRecycle.getBaseValue() <= 0);
          menu.hideInformation();
          var addedCounter = selectedToRecycle.getBaseValue()
          card.setCounter(card.getCounter() + addedCounter)
          if(card.getCounter() > 12)
          {
            card.setCounter(12);
          }
          trash.removeCard(selectedToRecycle.getUID());
          mainInventory.addCard(selectedToRecycle);

          //Log
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 recycled the %2.</font>").arg(card.getName()).arg(selectedToRecycle.getName()), [1, 2, 3, 4], 1, 0);
          if(addedCounter > 1)
          {
            gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 gained %2 counters.</font>").arg(card.getName()).arg(addedCounter), [1, 2, 3, 4], 1, 0);
          }
          else
          {
            gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 gained 1 counter.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
          }
        }
        if(choice == 1)
        {
          //Log
          boardutils.logSilverActivation(card.getName());
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 cast 11 counters.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);

          menu.showInformation(qsTranslate("handmirror", "Choose an item to reclaim"), -1);
          do
          {
            var selectedToReclaim = trash.cardChoice(trashCards, 0, 1);
          }
          while(selectedToReclaim.hasIcon("tcan") == true);
          menu.hideInformation();
          card.setCounter(card.getCounter() - 11);
          trash.removeCard(selectedToReclaim.getUID());
          if(card.getPlayer() == 1) {p1Inventory.addCard(selectedToReclaim);}
          if(card.getPlayer() == 2) {p2Inventory.addCard(selectedToReclaim);}
          if(card.getPlayer() == 3) {p3Inventory.addCard(selectedToReclaim);}
          if(card.getPlayer() == 4) {p4Inventory.addCard(selectedToReclaim);}

          //Log
          var userNames = gameState.getPlayerNickNames();
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 returned the %2 to %3's hand.</font>").arg(card.getName()).arg(selectedToReclaim.getName()).arg(userNames[card.getPlayer()-1]), [1, 2, 3, 4], 1, 0);
        }
        card.setShowFullSize(0);
      }
      else
      {
        menu.showInformation(qsTranslate("handmirror", "No items in trash fulfill the criteria!"));
      }
    }
    else
    {
      menu.showInformation(qsTranslate("handmirror", "No items in trash!"));
    }
  }


  // GOLF CLUB ////////////////////

  if(cardMirrored == "golfclub" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    tilesOK = []; //This will store the tiles of the good cards
    for(ctc = 2; ctc < 10; ctc = ctc+2)
    {
      var xCorri = 0;
      var yCorri = 0;
      if(ctc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
      if(ctc == 4 && card.getPosX() > 0) {xCorri = -1;}
      if(ctc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
      if(ctc == 8 && card.getPosY() > 0) {yCorri = -1;}
      TTC = Point(card.getPosX()+xCorri,card.getPosY()+yCorri);
      CTC = board.getCardAtPosition(TTC);
      if(CTC != null && CTC.getUID() != card.getUID() && boardutils.checkForUnpickable(CTC) == false && boardutils.isUntargetableBySilver(CTC) == false)
      {
        tilesOK.push(TTC);
      }
    }

    if(tilesOK.length > 0)
    {
      //Ask the player which card to pick up
      menu.showInformation(qsTranslate("handmirror", "Choose which item should be taken to hand"), -1);
      //The player chooses one from the good cards
      selectedTile = boardutils.tileChoice(tilesOK, 1);
      //Hide the 'Choose which card...' label
      menu.hideInformation();

      //Unless canceled
      if(selectedTile.x != -1)
      {
        //Log
        boardutils.logSilverActivation(card.getName());

        selectedCard = board.getCardAtPosition(selectedTile);
        //Rotate the Golf Club
        var prevOwner = card.getPlayer()
        if(prevOwner == 1) {card.changeOwner(4);}
        if(prevOwner == 2) {card.changeOwner(3);}
        if(prevOwner == 3) {card.changeOwner(1);}
        if(prevOwner == 4) {card.changeOwner(2);}

        //Log
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 has been rotated by 90° counter-clockwise.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);

        //Remove the card from the board
        board.removeCard(selectedCard.getUID());

        if(selectedCard.getPlayer() == 1)
        {
          p1Inventory.addCard(selectedCard);
        }
        else if(selectedCard.getPlayer() == 2 && gameState.getGameMode() != 4)
        {
          p2Inventory.addCard(selectedCard);
        }
        else if(selectedCard.getPlayer() == 3 && gameState.getGameMode() != 0)
        {
          p3Inventory.addCard(selectedCard);
        }
        else if(selectedCard.getPlayer() == 4 && (gameState.getGameMode() == 1 || gameState.getGameMode() == 3))
        {
          p4Inventory.addCard(selectedCard);
        }
        else
        {
          mainInventory.addCard(selectedCard);
        }

        if(selectedCard.getPlayer() > 0)
        {
          //Log
          var userNames = gameState.getPlayerNickNames();
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 returned %2%3 %4 to %5's hand.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()).arg(userNames[selectedCard.getPlayer()-1]), [1, 2, 3, 4], 1, 0);
        }
        else
        {
          //Log
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 returned %2%3 %4 to the storage.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);
        }
      }
    }
    else
    {
      menu.showInformation(qsTranslate("handmirror", "No item is selectable for ability!"));
    }
  }


  // HARD HAT ////////////////////

  if(cardMirrored == "hardhat" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    var cardHatted = 0;
    switch(card.getPlayer())
    {
      case 1:
        if(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()+1)) != null && board.getCardAtPosition(Point(card.getPosX(),card.getPosY()+1)).isGrayScale() == true)
        {
          cardHatted = board.getCardAtPosition(Point(card.getPosX(),card.getPosY()+1));
        }
        break;
      case 2:
        if(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()-1)) != null && board.getCardAtPosition(Point(card.getPosX(),card.getPosY()-1)).isGrayScale() == true)
        {
          cardHatted = board.getCardAtPosition(Point(card.getPosX(),card.getPosY()-1));
        }
        break;
      case 3:
        if(board.getCardAtPosition(Point(card.getPosX()-1,card.getPosY())) != null && board.getCardAtPosition(Point(card.getPosX()-1,card.getPosY())).isGrayScale() == true)
        {
          cardHatted = board.getCardAtPosition(Point(card.getPosX()-1,card.getPosY()));
        }
        break;
      case 4:
        if(board.getCardAtPosition(Point(card.getPosX()+1,card.getPosY())) != null && board.getCardAtPosition(Point(card.getPosX()+1,card.getPosY())).isGrayScale() == true)
        {
          cardHatted = board.getCardAtPosition(Point(card.getPosX()+1,card.getPosY()));
        }
        break;
    }

    if(cardHatted != 0)
    {
      //Log
      boardutils.logSilverActivation(card.getName());
      gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 restored the abilities of %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(cardHatted.getPosX())).arg(cardHatted.getPosY()+1).arg(cardHatted.getName()), [1, 2, 3, 4], 1, 0);
      gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 has been disabled.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);

      cardHatted.setGrayScale(0);
      card.setGrayScale(1);
    }
  }


  // HONEY DIPPER ////////////////////

  if(cardMirrored == "honeydipper" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    if(boardutils.getAdjacentEmptyTiles(card.getPosition()).length > 0 && boardutils.checkForImmovable(card) == false)
    {
      if((gameState.getCurrentPlayer() == 1 && p1Inventory.isEmpty() == 0) ||
         (gameState.getCurrentPlayer() == 2 && p2Inventory.isEmpty() == 0) ||
         (gameState.getCurrentPlayer() == 3 && p3Inventory.isEmpty() == 0) ||
         (gameState.getCurrentPlayer() == 4 && p4Inventory.isEmpty() == 0))
      {
        switch(gameState.getCurrentPlayer())
        {
          case 1:
            handDiscardedFrom = p1Inventory.getAllCards();
            cardRandomlyDiscarded = handDiscardedFrom[rng.randomBetween(0, handDiscardedFrom.length)];
            p1Inventory.removeCard(cardRandomlyDiscarded.getUID());
            break;
          case 2:
            handDiscardedFrom = p2Inventory.getAllCards();
            cardRandomlyDiscarded = handDiscardedFrom[rng.randomBetween(0, handDiscardedFrom.length)];
            p2Inventory.removeCard(cardRandomlyDiscarded.getUID());
            break;
          case 3:
            handDiscardedFrom = p3Inventory.getAllCards();
            cardRandomlyDiscarded = handDiscardedFrom[rng.randomBetween(0, handDiscardedFrom.length)];
            p3Inventory.removeCard(cardRandomlyDiscarded.getUID());
            break;
          case 4:
            handDiscardedFrom = p4Inventory.getAllCards();
            cardRandomlyDiscarded = handDiscardedFrom[rng.randomBetween(0, handDiscardedFrom.length)];
            p4Inventory.removeCard(cardRandomlyDiscarded.getUID());
            break;
        }
        trash.addCard(cardRandomlyDiscarded);

        //Log
        boardutils.logSilverActivation(card.getName());
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 discarded the %2.</font>").arg(card.getName()).arg(cardRandomlyDiscarded.getName()), [1, 2, 3, 4], 1, 0);

        menu.showInformation(qsTranslate("handmirror", "Choose where to move the %1").arg(card.getName()), -1);
        selectedPos = boardutils.tileChoice(boardutils.getAdjacentEmptyTiles(card.getPosition()), 0);
        menu.hideInformation();
        board.moveCard(card.getPosition(), selectedPos);

        //Log
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 has been moved to %2%3.</font>").arg(card.getName()).arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1), [1, 2, 3, 4], 1, 0);
      }
    }
    else
    {
      menu.showInformation(qsTranslate("handmirror", "You cannot use this ability!"));
    }
  }


  // HORSESHOE MAGNET ////////////////////

  if(cardMirrored == "horseshoemagnet" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    var eT = [];
    var eAT = boardutils.getAdjacentEmptyTiles(card.getPosition());
    if(eAT.length > 0)
    {
      //Check for Perfume Bottles, they can deny this ability
      if(boardutils.limitedByPerfume() == false)
      {
        eT = boardutils.getAdjacentEmptyTiles(card.getPosition());
      }
      else
      {
        var ePT = boardutils.getTilesAbovePerfume();
        for(i = 0; i < eAT.length; i++)
        {
          for(j = 0; j < ePT.length; j++)
          {
            if(ePT[j].x == eAT[i].x && ePT[j].y == eAT[i].y)
            {
              eT.push(eAT[i]);
              break;
            }
          }
        }
      }
    }

    if(eT.length > 0)
    {
      if(gameState.getGameMode() > 0 && gameState.getGameMode() < 4)
      {
        viewablePlayers = [] //This will store the possible players for the first choice
        if(gameState.getCurrentPlayer() == 2 || (gameState.getCurrentPlayer() == 3 && gameState.getGameMode() != 1) || gameState.getCurrentPlayer() == 4)
        {
          if(!p1Inventory.isEmpty())
          {
            viewablePlayers.push(1);
          }
        }
        if(gameState.getCurrentPlayer() == 1 || (gameState.getCurrentPlayer() == 3) || (gameState.getCurrentPlayer() == 4 && gameState.getGameMode() != 1))
        {
          if(!p2Inventory.isEmpty())
          {
            viewablePlayers.push(2);
          }
        }
        if((gameState.getCurrentPlayer() == 1 && gameState.getGameMode() != 1) || gameState.getCurrentPlayer() == 2 || gameState.getCurrentPlayer() == 4)
        {
          if(!p3Inventory.isEmpty())
          {
            viewablePlayers.push(3);
          }
        }
        if((gameState.getCurrentPlayer() == 1 && gameState.getGameMode() != 2) || (gameState.getCurrentPlayer() == 2 && gameState.getGameMode() == 3) || (gameState.getCurrentPlayer() == 3 && gameState.getGameMode() != 2 ))
        {
          if(!p4Inventory.isEmpty())
          {
            viewablePlayers.push(4);
          }
        }
        menu.showInformation(qsTranslate("handmirror", "Which player's hand would you like to view?"), -1);
        //Get the player's answer
        choice = graphicsViewMenu.getPlayer(viewablePlayers, 0);
        //Now we have the choice, it's time to make the chosen player's inventory visible
        switch(choice)
        {
          case 1:
            p1Inventory.setViewable(true, true);
            break;
          case 2:
            p2Inventory.setViewable(true, true);
            break;
          case 3:
            p3Inventory.setViewable(true, true);
            break;
          case 4:
            p4Inventory.setViewable(true, true);
            break;
        }
        //Hide the 'Which player's inventory...' label
        menu.hideInformation();
      }
      else
      {
        //1v1 opposing
        if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 1)
        {
          p2Inventory.setViewable(true, true);
          choice = 2;
        }
        if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 2)
        {
          p1Inventory.setViewable(true, true);
          choice = 1;
        }
        //1v1 adjacent
        if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 1)
        {
          p3Inventory.setViewable(true, true);
          choice = 3;
        }
        if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 3)
        {
          p1Inventory.setViewable(true, true);
          choice = 1;
        }
      }

      //Log
      boardutils.logSilverActivation(card.getName());
      var userNames = gameState.getPlayerNickNames();
      gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 revealed %2's hand.</font>").arg(card.getName()).arg(userNames[choice-1]), [1, 2, 3, 4], 1, 0);

      switch(choice)
      {
        case 1:
          var cardsInHand = p1Inventory.getAllCards();
          break;
        case 2:
          var cardsInHand = p2Inventory.getAllCards();
          break;
        case 3:
          var cardsInHand = p3Inventory.getAllCards();
          break;
        case 4:
          var cardsInHand = p4Inventory.getAllCards();
          break;
      }
      metalCardsInHand = [];
      for(i = 0; i < cardsInHand.length; i++)
      {
        if(cardsInHand[i].getBackground() == "metal")
        {
          metalCardsInHand.push(cardsInHand[i]);
        }
      }

      if(metalCardsInHand.length > 0)
      {
        var randomisedToAttract = 0
        //Ask the player where to place the metal item
        menu.showInformation(qsTranslate("handmirror", "Choose the tile"), -1);
        //The player chooses one from the good positions
        selectedPos = boardutils.tileChoice((eT), 0);
        //We got the choice, hide the info
        menu.hideInformation();
        randomlyPlacedMetal = metalCardsInHand[rng.randomBetween(0, metalCardsInHand.length)];
        randomlyPlacedMetal.setGrayScale(1);
        switch(choice)
        {
          case 1:
            p1Inventory.moveCardToBoard(randomlyPlacedMetal.getUID(), selectedPos, 0);
            break;
          case 2:
            p2Inventory.moveCardToBoard(randomlyPlacedMetal.getUID(), selectedPos, 0);
            break;
          case 3:
            p3Inventory.moveCardToBoard(randomlyPlacedMetal.getUID(), selectedPos, 0);
            break;
          case 4:
            p4Inventory.moveCardToBoard(randomlyPlacedMetal.getUID(), selectedPos, 0);
            break;
        }

        //Log
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 disabled the %2 and played it from %3's hand to %4%5.</font>").arg(card.getName()).arg(randomlyPlacedMetal.getName()).arg(userNames[choice-1]).arg(boardutils.convertToABC(selectedPos.x)).arg(selectedPos.y+1), [1, 2, 3, 4], 1, 0);
      }
      else
      {
        menu.showInformation(qsTranslate("handmirror", "No metal items in this hand!"));
        card.setGrayScale(1);

        //Log
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 has been disabled.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
      }
      //Crop
      card.setShowFullSize(0);
    }
    else
    {
      menu.showInformation(qsTranslate("handmirror", "You cannot use this ability!"));
    }
  }


  // OIL LAMP ////////////////////

  if(cardMirrored == "oillamp" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1] && card.getCounter() > 1 && mainInventory.getAllCards().length > 2)
  {
    //Check for hands not being empty
    if((gameState.getCurrentPlayer() == 1 && p1Inventory.isEmpty() == 0) || (gameState.getCurrentPlayer() == 2 && p2Inventory.isEmpty() == 0) || (gameState.getCurrentPlayer() == 3 && p3Inventory.isEmpty() == 0) || (gameState.getCurrentPlayer() == 4 && p4Inventory.isEmpty() == 0))
    {
      //Ask the player which card to discard
      menu.showInformation(qsTranslate("handmirror", "Choose an item to discard"), -1);
      //The player chooses one
      switch(gameState.getCurrentPlayer())
      {
        case 1: selectedDiscard = p1Inventory.cardChoice(p1Inventory.getAllCards(), 1, -1); break;
        case 2: selectedDiscard = p2Inventory.cardChoice(p2Inventory.getAllCards(), 1, -1); break;
        case 3: selectedDiscard = p3Inventory.cardChoice(p3Inventory.getAllCards(), 1, -1); break;
        case 4: selectedDiscard = p4Inventory.cardChoice(p4Inventory.getAllCards(), 1, -1); break;
      }
      menu.hideInformation();
      //Unless canceled
      if(selectedDiscard != null)
      {
        //Remove 2 counters
        card.setCounter(card.getCounter()-2);

        switch(gameState.getCurrentPlayer())
        {
          case 1: p1Inventory.removeCard(selectedDiscard.getUID()); break;
          case 2: p2Inventory.removeCard(selectedDiscard.getUID()); break;
          case 3: p3Inventory.removeCard(selectedDiscard.getUID()); break;
          case 4: p4Inventory.removeCard(selectedDiscard.getUID()); break;
        }
        //It gets discarded
        trash.addCard(selectedDiscard);

        //Log
        boardutils.logSilverActivation(card.getName());
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 cast 2 counters.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 discarded the %2.</font>").arg(card.getName()).arg(selectedDiscard.getName()), [1, 2, 3, 4], 1, 0);

        storageSize = mainInventory.getAllCards().length
        storageCards = mainInventory.getAllCards()
        //Generate three different random numbers
        offer1 = storageCards[rng.randomBetween(0, storageSize)]
        do
        {
          offer2 = storageCards[rng.randomBetween(0, storageSize)]
        }
        while(offer1 == offer2);
        do
        {
          offer3 = storageCards[rng.randomBetween(0, storageSize)]
        }
        while(offer1 == offer3 || offer2 == offer3);

        menu.hideInformation();
        mainInventory.removeCard(offer1.getUID());
        mainInventory.removeCard(offer2.getUID());
        mainInventory.removeCard(offer3.getUID());
        storage.addCard(offer1);
        storage.addCard(offer2);
        storage.addCard(offer3);
        storage.setViewable(true, true);
        //Ask the player which card to take
        menu.showInformation(qsTranslate("handmirror", "Choose an item"), -1);
        var lampChoice = storage.cardChoice(storage.getAllCards(), 0, -1);
        //Hide the label
        menu.hideInformation();

        storage.setViewable(false, false);
        var leftover1 = 0;
        var leftover2 = 0;
        if(lampChoice == offer1) {leftover1 = offer2; leftover2 = offer3;}
        if(lampChoice == offer2) {leftover1 = offer1; leftover2 = offer3;}
        if(lampChoice == offer3) {leftover1 = offer1; leftover2 = offer2;}
        //Remove the cards from the storage tab
        storage.removeCard(offer1.getUID());
        storage.removeCard(offer2.getUID());
        storage.removeCard(offer3.getUID());
        switch(gameState.getCurrentPlayer())
        {
          case 1: p1Inventory.addCard(lampChoice); break;
          case 2: p2Inventory.addCard(lampChoice); break;
          case 3: p3Inventory.addCard(lampChoice); break;
          case 4: p4Inventory.addCard(lampChoice); break;
        }
        mainInventory.addCard(leftover1);
        mainInventory.addCard(leftover2);

        //Log
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 drew the %2 from the storage.</font>").arg(card.getName()).arg(lampChoice.getName()), boardutils.informedPlayers(), 1, 0);
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 drew an item from the storage.</font>").arg(card.getName()), boardutils.unexpectingPlayers(), 1, 0);

        //Crop
        card.setShowFullSize(0);
      }
    }
    else
    {
      menu.showInformation(qsTranslate("handmirror", "No items in hand to discard!"));
    }
  }


  // PIGGY BANK ////////////////////

  if(cardMirrored == "piggybank" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    //Sound effect
    var randomSound = rng.randomBetween(1, 6);
    switch(randomSound)
    {
      case 1: gameState.playSoundEffect("snort-1.wav", 1); break;
      case 2: gameState.playSoundEffect("snort-2.wav", 1); break;
      case 3: gameState.playSoundEffect("snort-3.wav", 1); break;
      case 4: gameState.playSoundEffect("snort-4.wav", 1); break;
      case 5: gameState.playSoundEffect("snort-5.wav", 1); break;
    }
  }


  // PINCERS ////////////////////

  if(cardMirrored == "pincers" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    cards = board.getAllCards(); //Retrieve all cards on the board
    tilesOK = []; //This will store the cards which can be moved
    for(i = 0; i < cards.length; i++) //Check all cards
    {
      ////If there is an empty tile adjacent to the Pincers then ability can be used
      if(boardutils.getAdjacentEmptyTiles(card.getPosition()).length > 0)
      {
        //If the card is metal, yours, not the Pincers, not immovable and not untargetable by active abilities
        if(cards[i].getID() != "pincers" && cards[i].getPlayer() == gameState.getCurrentPlayer() && cards[i].getBackground() == "metal" && boardutils.checkForImmovable(cards[i]) == false && boardutils.isUntargetableBySilver(cards[i]) == false)
        {
          //If its value is equal to the number of cards in your hand, then it is good
          switch(gameState.getCurrentPlayer())
          {
            case 1:
              if(cards[i].getCurrentValue() == p1Inventory.getAllCards().length)
              {
                tilesOK.push(cards[i].getPosition());
              }
              break;
            case 2:
              if(cards[i].getCurrentValue() == p2Inventory.getAllCards().length)
              {
                tilesOK.push(cards[i].getPosition());
              }
              break;
            case 3:
              if(cards[i].getCurrentValue() == p3Inventory.getAllCards().length)
              {
                tilesOK.push(cards[i].getPosition());
              }
              break;
            case 4:
              if(cards[i].getCurrentValue() == p4Inventory.getAllCards().length)
              {
                tilesOK.push(cards[i].getPosition());
              }
              break;
          }
        }
      }
      else
      {
        menu.showInformation(qsTranslate("handmirror", "There is no adjacent empty tile!"));
        break;
      }
    }
    if(tilesOK.length > 0)
    {
      //Ask the player which card would he like to move
      menu.showInformation(qsTranslate("handmirror", "Choose target"), -1);
      //The player chooses one from the good cards
      selectedTile = boardutils.tileChoice(tilesOK, 1);
      selectedCard = board.getCardAtPosition(selectedTile);
      //Hide the 'Choose which card...' label
      menu.hideInformation();
      //Unless canceled
      if(selectedTile.x != -1)
      {
        //Now the player must choose the new place for the card
        menu.showInformation(qsTranslate("handmirror", "Choose where would you like to move the selected item"), -1);
        //Only the empty tiles adjacent to the Pincers available now
        selectedPos = boardutils.tileChoice(boardutils.getAdjacentEmptyTiles(card.getPosition()), 0);
        //We got the choice, hide the info
        menu.hideInformation();
        //Move the card
        board.moveCard(selectedTile, selectedPos);
        //Log
        boardutils.logSilverActivation(card.getName());
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 moved %2%3 %4 to %5%6.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg((board.getCardAtPosition(selectedPos)).getName()).arg(boardutils.convertToABC(selectedPos.x)).arg(selectedPos.y+1), [1, 2, 3, 4], 1, 0);
        //Crop or grayscale
        if(card.isFullSize() == true)
        {
          card.setShowFullSize(0);
        }
        else
        {
          card.setGrayScale(1);
          //Log
          gameState.writeToGameLog(qsTranslate("handmirror", "The %1 has been disabled.").arg(card.getName()), [1, 2, 3, 4], 1, 0);
        }
      }
    }
  }

  // RASP ////////////////////

  if(cardMirrored == "rasp" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    tilesOK = [];
    for(ctc = 2; ctc < 10; ctc = ctc+2)
    {
      var xCorri = 0;
      var yCorri = 0;
      if(ctc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
      if(ctc == 4 && card.getPosX() > 0) {xCorri = -1;}
      if(ctc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
      if(ctc == 8 && card.getPosY() > 0) {yCorri = -1;}
      TTC = Point(card.getPosX()+xCorri,card.getPosY()+yCorri);
      CTC = board.getCardAtPosition(TTC);
      if(CTC != null && CTC != card && CTC.getCounter() > 0 && boardutils.isUntargetableBySilver(CTC) == false)
      {
        tilesOK.push(TTC);
      }
    }
    if(tilesOK.length > 0)
    {
      menu.showInformation(qsTranslate("handmirror", "Choose target"), -1);
      selectedTile = boardutils.tileChoice(tilesOK, 1);
      menu.hideInformation();
      selectedCard = board.getCardAtPosition(selectedTile);
      //Unless canceled
      if(selectedTile.x != -1)
      {
        //Log
        boardutils.logSilverActivation(card.getName());
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 removed 1 counter from %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);

        if(selectedCard.getCounter() > 1)
        {
          selectedCard.setCounter(selectedCard.getCounter()-1);
        }
        else
        {
          selectedCard.setCounter(0);
          if(boardutils.checkForUntrashable(selectedCard) == false)
          {
            //Skip damaging Bomb - TEMPORARY SOLUTION
            if((selectedCard.getID() != "bomb" && (selectedCard.getID() != "handmirror" || boardutils.itemMirrored(targetedByShaker) != "bomb")) || selectedCard.isGrayScale() == true)
            {
              //Log
              gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 destroyed %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);
              board.moveCardToTrash(selectedCard.getUID());
            }
          }
        }
        card.setShowFullSize(0);
      }
    }
    else
    {
      menu.showInformation(qsTranslate("handmirror", "You cannot use this ability!"));
    }
  }

  // SALT SHAKER ////////////////////

  if(cardMirrored == "saltshaker" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {

    if(boardutils.getAdjacentEmptyTiles(card.getPosition()).length > 0 && boardutils.checkForImmovable(card) == false && card.getCounter() > 0)
    {
      tilesOK = [];
      cardsOK = [];

      for(ttc = 2; ttc < 10; ttc = ttc+2)
      {
        var xCorri = 0;
        var yCorri = 0;
        if(ttc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
        if(ttc == 4 && card.getPosX() > 0) {xCorri = -1;}
        if(ttc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
        if(ttc == 8 && card.getPosY() > 0) {yCorri = -1;}

        if(board.getCardAtPosition(Point(card.getPosX()+xCorri,card.getPosY()+yCorri)) == null && board.isDisabledTile(Point(card.getPosX()+xCorri,card.getPosY()+yCorri)) == false)
        {
          for(ctc = 2; ctc < 10; ctc = ctc+2)
          {
            var xCorri2 = 0;
            var yCorri2 = 0;
            if(ctc == 2 && ttc != 8 && card.getPosY() < board.getHeight() - 1) {yCorri2 = 1;}
            if(ctc == 4 && ttc != 6 && card.getPosX() > 0) {xCorri2 = -1;}
            if(ctc == 6 && ttc != 4 && card.getPosX() < board.getWidth() - 1) {xCorri2 = 1;}
            if(ctc == 8 && ttc != 2 && card.getPosY() > 0) {yCorri2 = -1;}

            if(board.getCardAtPosition(Point(card.getPosX()+xCorri+xCorri2,card.getPosY()+yCorri+yCorri2)) != null && boardutils.isUntargetableBySilver(board.getCardAtPosition(Point(card.getPosX()+xCorri+xCorri2,card.getPosY()+yCorri+yCorri2))) == false)
            {
              tilesOK.push(Point(card.getPosX()+xCorri,card.getPosY()+yCorri));
              break;
            }
          }
        }
      }

      if(tilesOK.length > 0)
      {
        menu.showInformation(qsTranslate("handmirror", "Choose where to move the %1").arg(card.getName()), -1);
        selectedPos = boardutils.tileChoice(tilesOK, 1);
        menu.hideInformation();
        //Unless canceled
        if(selectedPos.x != -1)
        {
          //Log
          boardutils.logSilverActivation(card.getName());

          //Move the card
          board.moveCard(card.getPosition(), selectedPos);

          //Log
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 has been moved to %2%3.</font>").arg(card.getName()).arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1), [1, 2, 3, 4], 1, 0);

          for(stc = 2; stc < 10; stc = stc+2)
          {
            var xCorri3 = 0;
            var yCorri3 = 0;
            if(stc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri3 = 1;}
            if(stc == 4 && card.getPosX() > 0) {xCorri3 = -1;}
            if(stc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri3 = 1;}
            if(stc == 8 && card.getPosY() > 0) {yCorri3 = -1;}

            if(xCorri3 != 0 || yCorri3 != 0)
            {
              if(board.getCardAtPosition(Point(card.getPosX()+xCorri3,card.getPosY()+yCorri3)) != null && boardutils.isUntargetableBySilver(board.getCardAtPosition(Point(card.getPosX()+xCorri3,card.getPosY()+yCorri3))) == false)
              {
                cardsOK.push((Point(card.getPosX()+xCorri3,card.getPosY()+yCorri3)));
              }
            }
          }

          //Ask the player on which card would he like to place a counter
          menu.showInformation(qsTranslate("handmirror", "Choose on which item would you like to place a counter"), -1);
          //The player chooses one from the good cards
          selectedTile = boardutils.tileChoice(cardsOK, 0);
          var targetedByShaker = board.getCardAtPosition(selectedTile);
          //Take down 1 counter
          card.setCounter(card.getCounter()-1);
          //Place 1 counter
          targetedByShaker.setCounter(targetedByShaker.getCounter()+1);
          //It gets capped at 12
          if(targetedByShaker.getCounter() > 11)
          {
            targetedByShaker.setCounter(12);
          }
          //Check for Golf Club
          var clubRotated = false;
          if( ( (targetedByShaker.getID() == "golfclub" && targetedByShaker.isGrayScale() == false) || (targetedByShaker.getID() == "handmirror" && boardutils.itemMirrored(targetedByShaker) == "golfclub") ) && boardutils.checkForUnrotatable(targetedByShaker) == false && targetedByShaker.getPlayer() != gameState.getCurrentPlayer())
          {
            clubRotated = true;
          }
          menu.hideInformation();
          card.setShowFullSize(0);
          //Log
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 transfered 1 counter from itself to %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(targetedByShaker.getPosX())).arg(targetedByShaker.getPosY()+1).arg(targetedByShaker.getName()), [1, 2, 3, 4], 1, 0);
          if(clubRotated == true)
          {
            targetedByShaker.changeOwner(gameState.getCurrentPlayer());

            //Log
            var userNames = gameState.getPlayerNickNames();
            gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"khaki\">%1%2 %3 has been rotated toward %4.</font>").arg(boardutils.convertToABC(targetedByShaker.getPosX())).arg(targetedByShaker.getPosY()+1).arg(targetedByShaker.getName()).arg(userNames[targetedByShaker.getPlayer()-1]), [1, 2, 3, 4], 1, 0);
          }
        }
      }
      else
      {
        menu.showInformation(qsTranslate("handmirror", "You cannot use this ability!"));
      }
    }
    else
    {
      menu.showInformation(qsTranslate("handmirror", "You cannot use this ability!"));
    }
  }


  // SCALES ////////////////////

  if(cardMirrored == "scales" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    if((gameState.getCurrentPlayer() == 1 && p1Inventory.getAllCards().length > 0) || (gameState.getCurrentPlayer() == 2 && p2Inventory.getAllCards().length > 0) || (gameState.getCurrentPlayer() == 3 && p3Inventory.getAllCards().length > 0) || (gameState.getCurrentPlayer() == 4 && p4Inventory.getAllCards().length > 0))
    {
      cards = board.getAllCards(); //Retrieve all cards on the board
      posOK = []; //This will store the tile of the possible targets
      tilesOK = []; //This will store the tile of the targets depending on the discard
      handValues = []; //This will store the values of the items in hand
      crossValues = []; //This will store the values of the items in the same line or column

      switch(gameState.getCurrentPlayer())
      {
        case 1:
          itemsInHands = p1Inventory.getAllCards();
          break;
        case 2:
          itemsInHands = p2Inventory.getAllCards();
          break;
        case 3:
          itemsInHands = p3Inventory.getAllCards();
          break;
        case 4:
          itemsInHands = p4Inventory.getAllCards();
          break;
      }
      for(i = 0; i < itemsInHands.length; i++)
      {
        handValues.push(itemsInHands[i].getBaseValue());
      }
      var highestValue = Math.max.apply(Math, handValues);
      var lowestValue = Math.min.apply(Math, handValues);

      for(i = 0; i < cards.length; i++) //Check all cards
      {
        //If the card is on a tile in the same row or column and not greyscale then it might be good
        if((card.getPosX() == cards[i].getPosX() || card.getPosY() == cards[i].getPosY()) && cards[i].isGrayScale() == false)
        {
          //If there is an item in hand with a value that is greater or equal to the target's value
          if(cards[i].getCurrentValue() <= highestValue)
          {
            //If it is not the Scales itself then it is good
            if(cards[i].getUID() != card.getUID())
            {
              //If the card is not protected by a Bird feeder
              if(boardutils.isProtectedByBirdfeeder(cards[i]) == false)
              {
                //Store the coordinates of the good card
                posOK.push(cards[i].getPosition());
                crossValues.push(cards[i].getCurrentValue());
              }
            }
          }
        }
      }

      if(posOK.length > 0)
      {
        var crossMinValue = Math.min.apply(Math, crossValues);
        //Ask the player which card to discard
        menu.showInformation(qsTranslate("handmirror", "Choose an item to discard"), -1);
        //The player chooses one
        var selectedDiscard = 0;
        do
        {
          switch(gameState.getCurrentPlayer())
          {
            case 1: selectedDiscard = p1Inventory.cardChoice(p1Inventory.getAllCards(), 1, -1); break;
            case 2: selectedDiscard = p2Inventory.cardChoice(p2Inventory.getAllCards(), 1, -1); break;
            case 3: selectedDiscard = p3Inventory.cardChoice(p3Inventory.getAllCards(), 1, -1); break;
            case 4: selectedDiscard = p4Inventory.cardChoice(p4Inventory.getAllCards(), 1, -1); break;
          }
        }
        while(selectedDiscard != null && selectedDiscard.getBaseValue() < crossMinValue);

        menu.hideInformation();
        //Unless canceled
        if(selectedDiscard != null)
        {
          var weightValue = selectedDiscard.getBaseValue();
          switch(gameState.getCurrentPlayer())
          {
            case 1: p1Inventory.removeCard(selectedDiscard.getUID()); break;
            case 2: p2Inventory.removeCard(selectedDiscard.getUID()); break;
            case 3: p3Inventory.removeCard(selectedDiscard.getUID()); break;
            case 4: p4Inventory.removeCard(selectedDiscard.getUID()); break;
          }
          trash.addCard(selectedDiscard);

          //Log
          boardutils.logSilverActivation(card.getName());
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 discarded the %2.</font>").arg(card.getName()).arg(selectedDiscard.getName()), [1, 2, 3, 4], 1, 0);
          for(i = 0; i < posOK.length; i++)
          {
            if(board.getCardAtPosition(posOK[i]).getCurrentValue() <= weightValue)
            {
              tilesOK.push(posOK[i]);
            }
          }

          //Ask the player which card's ability to erase
          menu.showInformation(qsTranslate("handmirror", "Choose target"), -1);
          //The player chooses one from the good cards
          selectedTile = boardutils.tileChoice(tilesOK, 0);
          //Hide the 'Choose target' label
          menu.hideInformation();
          selectedCard = board.getCardAtPosition(selectedTile);
          selectedCard.setGrayScale(1);

          //Log
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 disabled %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);

        }
      }
      else
      {
        menu.hideInformation();
        menu.showInformation(qsTranslate("handmirror", "No item is selectable for ability!"));
      }
    }
    else
    {
      menu.showInformation(qsTranslate("handmirror", "You cannot use this ability!"));
    }
  }


  // SCISSORS ////////////////////

  if(cardMirrored == "scissors" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    menu.showInformation(qsTranslate("handmirror", "Choose the direction for rotating the %1").arg(card.getName()), -1);

    choiceArray = [
    qsTranslate("scissors", "Rotate counter-clockwise"),
    qsTranslate("scissors", "Rotate 180 degrees"),
    qsTranslate("scissors", "Rotate clockwise")
    ];
    enableArray = ["true", "true", "true"];
    toolTipArray = ["", "", ""];

    var choice = menu.getChoice(choiceArray, enableArray, toolTipArray);
    menu.hideInformation();

    //Log
    boardutils.logSilverActivation(card.getName());

    if(choice == 0)
    {
      var prevOwner = card.getPlayer()
      if(prevOwner == 1) {card.changeOwner(4)}
      if(prevOwner == 2) {card.changeOwner(3)}
      if(prevOwner == 3) {card.changeOwner(1)}
      if(prevOwner == 4) {card.changeOwner(2)}

      //Log
      gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 have been rotated by 90° counter-clockwise.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
    if(choice == 1)
    {
      var prevOwner = card.getPlayer()
      if(prevOwner == 1) {card.changeOwner(2)}
      if(prevOwner == 2) {card.changeOwner(1)}
      if(prevOwner == 3) {card.changeOwner(4)}
      if(prevOwner == 4) {card.changeOwner(3)}

      //Log
      gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 have been rotated by 180°.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
    if(choice == 2)
    {
      var prevOwner = card.getPlayer()
      if(prevOwner == 1) {card.changeOwner(3)}
      if(prevOwner == 2) {card.changeOwner(4)}
      if(prevOwner == 3) {card.changeOwner(2)}
      if(prevOwner == 4) {card.changeOwner(1)}

      //Log
      gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 have been rotated by 90° clockwise.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
  }

  // CRYSTAL BALL ////////////////////

  if(cardMirrored == "crystalball" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    var divination = 0;
    if(gameState.getGameMode() == 0 || gameState.getGameMode() == 4)
    {
      //1v1 opposing
      if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 1 && p2Inventory.getAllCards().length > 0)
      {
        divination = p2Inventory.getAllCards()[rng.randomBetween(0, p2Inventory.getAllCards().length)];
        var choice = 2;
      }
      if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 2 && p1Inventory.getAllCards().length > 0)
      {
        divination = p1Inventory.getAllCards()[rng.randomBetween(0, p1Inventory.getAllCards().length)];
        var choice = 1;
      }
      //1v1 adjacent
      if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 1 && p3Inventory.getAllCards().length > 0)
      {
        divination = p3Inventory.getAllCards()[rng.randomBetween(0, p3Inventory.getAllCards().length)];
        var choice = 3;
      }
      if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 3 && p1Inventory.getAllCards().length > 0)
      {
        divination = p1Inventory.getAllCards()[rng.randomBetween(0, p1Inventory.getAllCards().length)];
        var choice = 1;
      }
    }
    else
    {
      viewablePlayers = [] //This will store the possible players for the first choice
      if(gameState.getCurrentPlayer() == 2 || gameState.getCurrentPlayer() == 3 || (gameState.getCurrentPlayer() == 4 && gameState.getGameMode() == 3))
      {
        if(!p1Inventory.isEmpty())
        {
          viewablePlayers.push(1);
        }
      }
      if(gameState.getCurrentPlayer() == 1 || (gameState.getCurrentPlayer() == 3 && (gameState.getGameMode() == 2 || gameState.getGameMode() == 3)) || gameState.getCurrentPlayer() == 4)
      {
        if(!p2Inventory.isEmpty())
        {
          viewablePlayers.push(2);
        }
      }
      if(gameState.getCurrentPlayer() == 1 || (gameState.getCurrentPlayer() == 2 && gameState.getGameMode() > 1) || gameState.getCurrentPlayer() == 4)
      {
        if(!p3Inventory.isEmpty())
        {
          viewablePlayers.push(3);
        }
      }
      if((gameState.getCurrentPlayer() == 1 && gameState.getGameMode() == 3) || (gameState.getCurrentPlayer() == 2 && gameState.getGameMode() != 2) || (gameState.getCurrentPlayer() == 3 && gameState.getGameMode() != 2 ))
      {
        if(!p4Inventory.isEmpty())
        {
          viewablePlayers.push(4);
        }
      }

      if(viewablePlayers.length > 0)
      {
        menu.showInformation(qsTranslate("handmirror", "Which player's item would you like to view?"), -1);
        //Get the player's answer
        var choice = graphicsViewMenu.getPlayer(viewablePlayers, 0);
        //Now we have the choice, it's time to make a random card of the chosen player's inventory visible
        switch(choice)
        {
          case 1:
            divination = p1Inventory.getAllCards()[rng.randomBetween(0, p1Inventory.getAllCards().length)];
            break;
          case 2:
            divination = p2Inventory.getAllCards()[rng.randomBetween(0, p2Inventory.getAllCards().length)];
            break;
          case 3:
            divination = p3Inventory.getAllCards()[rng.randomBetween(0, p3Inventory.getAllCards().length)];
            break;
          case 4:
            divination = p4Inventory.getAllCards()[rng.randomBetween(0, p4Inventory.getAllCards().length)];
            break;
        }
        //Hide the 'Which player's card...' label
        menu.hideInformation();
      }

    }
    if(divination != 0)
    {
      menu.showInformation(qsTranslate("handmirror", "You revealed this item:"), -1);
      choiceArray = [qsTranslate("handmirror", "%1").arg(divination.getName())]
      enableArray = ["true"];
      toolTipArray = [""];
      choice2 = menu.getChoice(choiceArray, enableArray, toolTipArray);
      menu.hideInformation();
      card.setShowFullSize(0);
      //Log
      boardutils.logSilverActivation(card.getName());
      var userNames = gameState.getPlayerNickNames();
      gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 revealed the %2 in %3's hand.</font>").arg(card.getName()).arg(divination.getName()).arg(userNames[choice-1]), [gameState.getCurrentPlayer(), choice], 1, 0);
      if(gameState.getGameMode() != 0 && gameState.getGameMode() != 4)
      {
        var thirdParty = [];
        for(tp = 1; tp < 5; tp++)
        {
          if(tp != gameState.getCurrentPlayer() && tp != choice)
          {
            thirdParty.push(tp);
          }
        }
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 revealed an item in %2's hand.</font>").arg(card.getName()).arg(userNames[choice-1]), [thirdParty[0], thirdParty[1]], 1, 0);
      }
    }
    else
    {
      menu.showInformation(qsTranslate("handmirror", "No items to reveal!"));
    }
  }


  // SNEAKER ////////////////////

  if(cardMirrored == "sneaker" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    if(boardutils.checkForImmovable(card) == false)
    {
      var divination = 0;
      var choice = 0;

      if(gameState.getGameMode() == 0 || gameState.getGameMode() == 4)
      {
        //1v1 opposing
        if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 1 && p2Inventory.getAllCards().length > 0)
        {
          divination = p2Inventory.getAllCards()[rng.randomBetween(0, p2Inventory.getAllCards().length)];
          choice = 1;
        }
        if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 2 && p1Inventory.getAllCards().length > 0)
        {
          divination = p1Inventory.getAllCards()[rng.randomBetween(0, p1Inventory.getAllCards().length)];
          choice = 2;
        }
        //1v1 adjacent
        if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 1 && p3Inventory.getAllCards().length > 0)
        {
          divination = p3Inventory.getAllCards()[rng.randomBetween(0, p3Inventory.getAllCards().length)];
          choice = 1;
        }
        if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 3 && p1Inventory.getAllCards().length > 0)
        {
          divination = p1Inventory.getAllCards()[rng.randomBetween(0, p1Inventory.getAllCards().length)];
          choice = 3;
        }
      }
      else
      {
        viewablePlayers = [] //This will store the possible players for the first choice
        if(gameState.getCurrentPlayer() == 2 || gameState.getCurrentPlayer() == 3 || (gameState.getCurrentPlayer() == 4 && gameState.getGameMode() == 3))
        {
          if(!p1Inventory.isEmpty())
          {
            viewablePlayers.push(1);
          }
        }
        if(gameState.getCurrentPlayer() == 1 || (gameState.getCurrentPlayer() == 3 && (gameState.getGameMode() == 2 || gameState.getGameMode() == 3)) || gameState.getCurrentPlayer() == 4)
        {
          if(!p2Inventory.isEmpty())
          {
            viewablePlayers.push(2);
          }
        }
        if(gameState.getCurrentPlayer() == 1 || (gameState.getCurrentPlayer() == 2 && gameState.getGameMode() > 1) || gameState.getCurrentPlayer() == 4)
        {
          if(!p3Inventory.isEmpty())
          {
            viewablePlayers.push(3);
          }
        }
        if((gameState.getCurrentPlayer() == 1 && gameState.getGameMode() == 3) || (gameState.getCurrentPlayer() == 2 && gameState.getGameMode() != 2) || (gameState.getCurrentPlayer() == 3 && gameState.getGameMode() != 2 ))
        {
          if(!p4Inventory.isEmpty())
          {
            viewablePlayers.push(4);
          }
        }

        if(viewablePlayers.length > 0)
        {
          menu.showInformation(qsTranslate("handmirror", "Which player's item would you like to view?"), -1);
          //Get the player's answer
          choice = graphicsViewMenu.getPlayer(viewablePlayers, 0);
          //Now we have the choice, it's time to make a random card of the chosen player's inventory visible
          switch(choice)
          {
            case 1:
              divination = p1Inventory.getAllCards()[rng.randomBetween(0, p1Inventory.getAllCards().length)];
              break;
            case 2:
              divination = p2Inventory.getAllCards()[rng.randomBetween(0, p2Inventory.getAllCards().length)];
              break;
            case 3:
              divination = p3Inventory.getAllCards()[rng.randomBetween(0, p3Inventory.getAllCards().length)];
              break;
            case 4:
              divination = p4Inventory.getAllCards()[rng.randomBetween(0, p4Inventory.getAllCards().length)];
              break;
          }
          //Hide the 'Which player's card...' label
          menu.hideInformation();
        }
      }

      if(divination != 0)
      {
        menu.showInformation(qsTranslate("handmirror", "You revealed this item:"), -1);
        choiceArray = [qsTranslate("handmirror", "%1").arg(divination.getName())]
        enableArray = ["true"];
        toolTipArray = [""];
        var choice2 = menu.getChoice(choiceArray, enableArray, toolTipArray);
        menu.hideInformation();
        var divinedDistance = divination.getBaseValue();
        if(divinedDistance < 0)
        {
          divinedDistance = (divinedDistance * -1)
        }

        if(boardutils.getPossibleSneakerMoves(card.getPosition(), divinedDistance).length > 0)
        {
          //Ask the player where to move the shoe
          menu.showInformation(qsTranslate("handmirror", "Choose where to move the %1").arg(card.getName()), -1);
          //The player chooses one from the good positions
          selectedPos = boardutils.tileChoice(boardutils.getPossibleSneakerMoves(card.getPosition(), divinedDistance), 0);
          //We got the choice, hide the info
          menu.hideInformation();
          //Log
          boardutils.logSilverActivation(card.getName());
          //Move the card
          board.moveCard(card.getPosition(), selectedPos);
          //Crop
          card.setShowFullSize(0);

          //Log
          var userNames = gameState.getPlayerNickNames();
          if(gameState.getGameMode() != 0 && gameState.getGameMode() != 4)
          {
            var thirdParty = [];
            for(tp = 1; tp < 5; tp++)
            {
              if(tp != gameState.getCurrentPlayer() && tp != choice)
              {
                thirdParty.push(tp);
              }
            }
            gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 revealed an item of %2 value in %3's hand.</font>").arg(card.getName()).arg(divinedDistance).arg(userNames[choice-1]), [thirdParty[0], thirdParty[1]], 1, 0);
            gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 revealed %2 in %3's hand.</font>").arg(card.getName()).arg(divination.getName()).arg(userNames[choice-1]), [gameState.getCurrentPlayer(), choice-1], 1, 0);
          }
          else
          {
            gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 revealed %2 in %3's hand.</font>").arg(card.getName()).arg(divination.getName()).arg(userNames[choice-1]), [1, 2, 3, 4], 1, 0);
          }
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 has been moved to %2%3.</font>").arg(card.getName()).arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1), [1, 2, 3, 4], 1, 0);
        }
        else
        {
          menu.showInformation(qsTranslate("handmirror", "Nowhere to move!"));
          //Crop
          card.setShowFullSize(0);
          //Log
          boardutils.logSilverActivation(card.getName());

          //Log
          var userNames = gameState.getPlayerNickNames();
          if(gameState.getGameMode() != 0 && gameState.getGameMode() != 4)
          {
            var thirdParty = [];
            for(tp = 1; tp < 5; tp++)
            {
              if(tp != gameState.getCurrentPlayer() && tp != choice)
              {
                thirdParty.push(tp);
              }
            }
            gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 revealed an item of %2 value in %3's hand.</font>").arg(card.getName()).arg(divinedDistance).arg(userNames[choice-1]), [thirdParty[0], thirdParty[1]], 1, 0);
            gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 revealed %2 in %3's hand.</font>").arg(card.getName()).arg(divination.getName()).arg(userNames[choice-1]), [gameState.getCurrentPlayer(), choice-1], 1, 0);
          }
          else
          {
            gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 revealed %2 in %3's hand.</font>").arg(card.getName()).arg(divination.getName()).arg(userNames[choice-1]), [1, 2, 3, 4], 1, 0);
          }

        }
      }
      else
      {
        menu.showInformation(qsTranslate("handmirror", "No items to reveal!"));
      }
    }
    else
    {
      menu.showInformation(qsTranslate("handmirror", "You cannot move this item!"));
    }
  }


  // SNOOKER BALL ////////////////////

  if(cardMirrored == "snookerball" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    cards = board.getAllCards(); //Retrieve all cards on the board
    cardsOK = []; //This will store the cards which can be moved
    for(i = 0; i < cards.length; i++) //Check all cards
    {
      //If the card is adjacent to the Snooker ball
      if(Math.pow(card.getPosX()-cards[i].getPosX(), 2) + Math.pow(card.getPosY()-cards[i].getPosY(), 2) == 1)
      {
        //If the card is not immovable then it might be good
        if(boardutils.checkForImmovable(cards[i]) == false)
        {
          //If there is an empty tile adjacent to the card then it might be good
          if(boardutils.getAdjacentEmptyTiles(cards[i].getPosition()).length > 0)
          {
            //If the value of the card is no greater than the number of counters on the Snooker ball, but greater or equal to zero
            if(card.getCounter() >= cards[i].getCurrentValue() && 0 <= cards[i].getCurrentValue())
            {
              //If the card is targetable by active abilities
              if(boardutils.isUntargetableBySilver(cards[i]) == false)
              {
                //Store the coordinates of the good card
                cardsOK.push(cards[i].getPosition());
              }
            }
          }
        }
      }
    }
    if(cardsOK.length > 0 || (card.getCounter() > 4 && boardutils.getAdjacentEmptyTiles(card.getPosition()).length > 0 &&  boardutils.checkForImmovable(card) == false))
    {
      //Ask the player to choose the ability
      menu.showInformation(qsTranslate("handmirror", "Select an ability").arg(card.getName()), -1);
      //Stores the text of the choices
      choiceArray = [
      qsTranslate("handmirror", "Move an adjacent item"),
      qsTranslate("handmirror", "Move itself")
      ];
      //Stores which choices are enabled and which aren't
      if(cardsOK.length > 0 && card.getCounter() > 4 && boardutils.getAdjacentEmptyTiles(card.getPosition()).length > 0 && boardutils.checkForImmovable(card) == false)
      {
        enableArray = ["true", "true"];
      }
      if(cardsOK.length == 0 && card.getCounter() > 4 && boardutils.getAdjacentEmptyTiles(card.getPosition()).length > 0 &&  boardutils.checkForImmovable(card) == false)
      {
        enableArray = ["false", "true"];
      }
      if(cardsOK.length > 0 && (card.getCounter() < 5 || boardutils.getAdjacentEmptyTiles(card.getPosition()).length == 0 || boardutils.checkForImmovable(card) == true))
      {
        enableArray = ["true", "false"];
      }
      //Stores the tooltips for the choices
      toolTipArray = ["", ""];

      //Get the player's choice
      var choice = menu.getChoice(choiceArray, enableArray, toolTipArray);
      //Hide the label
      menu.hideInformation();

      if(choice == 0)
      {
        //Ask the player which card would he like to move
        menu.showInformation(qsTranslate("handmirror", "Choose which item would you like to move"), -1);
        //The player chooses one from the good cards
        selectedTile = boardutils.tileChoice(cardsOK, 0);
        selectedCard = board.getCardAtPosition(selectedTile);
        //Hide the 'Choose which card...' label
        menu.hideInformation();
        //Now the player must choose the new place for the card
        menu.showInformation(qsTranslate("handmirror", "Choose where would you like to move the selected item"), -1);
        //Only the empty tiles adjacent to the chosen card are available now
        selectedPos = boardutils.tileChoice(boardutils.getAdjacentEmptyTiles(selectedTile), 0);
        //We got the choice, hide the info
        menu.hideInformation();
        //Take down the required amount of counters
        var counterCost = selectedCard.getCurrentValue();
        card.setCounter(card.getCounter() - counterCost);
        //Move the card
        board.moveCard(selectedTile, selectedPos);
        //Log
        boardutils.logSilverActivation(card.getName());
        if(counterCost < 1)
        {
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 cast %2 counter.</font>").arg(card.getName()).arg(counterCost), [1, 2, 3, 4], 1, 0);
        }
        else
        {
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 cast %2 counters.</font>").arg(card.getName()).arg(counterCost), [1, 2, 3, 4], 1, 0);
        }
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 moved %2%3 %4 to %5%6.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()).arg(boardutils.convertToABC(selectedPos.x)).arg(selectedPos.y+1), [1, 2, 3, 4], 1, 0);

        //Crop or grayscale
        if(card.isFullSize() == true)
        {
          card.setShowFullSize(0)
        }
        else
        {
          card.setGrayScale(1);
          //Log
          gameState.writeToGameLog(qsTranslate("handmirror", "The %1 has been disabled.").arg(card.getName()), [1, 2, 3, 4], 1, 0);
        }
      }

      if(choice == 1)
      {
        //Ask the player where to move the ball
        menu.showInformation(qsTranslate("handmirror", "Choose where to move the %1").arg(card.getName()), -1);
        //The player chooses one from the good positions
        selectedPos = boardutils.tileChoice(boardutils.getAdjacentEmptyTiles(card.getPosition()), 0);
        //We got the choice, hide the info
        menu.hideInformation();
        //Take down the 5 counters
        card.setCounter(card.getCounter()-5);
        //Log
        boardutils.logSilverActivation(card.getName());
        //Move the card
        board.moveCard(card.getPosition(), selectedPos);
        //Log
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 cast 5 counters.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 has been moved to %2%3.</font>").arg(card.getName()).arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1), [1, 2, 3, 4], 1, 0);

        //Crop or grayscale
        if(card.isFullSize() == true)
        {
          card.setShowFullSize(0);
        }
        else
        {
          card.setGrayScale(1);
          //Log
          gameState.writeToGameLog(qsTranslate("handmirror", "The %1 has been disabled.").arg(card.getName()), [1, 2, 3, 4], 1, 0);
        }
      }
    }
    else
    {
      menu.showInformation(qsTranslate("handmirror", "You cannot use this ability!"));
    }
  }


  // COIL SPRING ////////////////////

  if(cardMirrored == "coilspring" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    cards = board.getAllCards(); //Retrieve all cards on the board
    tilesOK = []; //This will store the cards which can be moved
    for(i = 0; i < cards.length; i++) //Check all cards
    {
      //If the card is adjacent to the Coil spring
      if(Math.pow(card.getPosX()-cards[i].getPosX(), 2) + Math.pow(card.getPosY()-cards[i].getPosY(), 2) == 1)
      {
        //If the card is not unpickable
        if(boardutils.checkForUnpickable(cards[i]) == false)
        {
          //If the card is targetable by active abilities
          if(boardutils.isUntargetableBySilver(cards[i]) == false)
          {
            //If the card has an owner
            if( (cards[i].getPlayer() == 1) ||
                (cards[i].getPlayer() == 2 && gameState.getGameMode() != 4) ||
                (cards[i].getPlayer() == 3 && gameState.getGameMode() != 0) ||
                (cards[i].getPlayer() == 4 && (gameState.getGameMode() == 1 || gameState.getGameMode() == 3)) )
            {
              //Store the coordinates of the good card
              tilesOK.push(cards[i].getPosition());
            }
          }
        }
      }
    }

    if(tilesOK.length > 0)
    {
      //Ask the player which card to pick up
      menu.showInformation(qsTranslate("handmirror", "Choose which item should be taken to hand"), -1);
      //The player chooses one from the good cards
      selectedTile = boardutils.tileChoice(tilesOK, 1);
      //Hide the 'Choose which card...' label
      menu.hideInformation();
      //Unless canceled
      if(selectedTile.x != -1)
      {
        selectedCard = board.getCardAtPosition(selectedTile);
        board.removeCard(selectedCard.getUID());
        switch(selectedCard.getPlayer())
        {
          case 1: p1Inventory.addCard(selectedCard); break;
          case 2: p2Inventory.addCard(selectedCard); break;
          case 3: p3Inventory.addCard(selectedCard); break;
          case 4: p4Inventory.addCard(selectedCard); break;
        }
        card.setGrayScale(1);
        //Log
        boardutils.logSilverActivation(card.getName());
        var userNames = gameState.getPlayerNickNames();
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 returned %2%3 %4 to %5's hand.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()).arg(userNames[selectedCard.getPlayer()-1]), [1, 2, 3, 4], 1, 0);
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 has been disabled.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
      }
    }
    else
    {
      menu.showInformation(qsTranslate("handmirror", "No item is selectable for ability!"));
    }
  }


  // TEA KETTLE ////////////////////

  if(cardMirrored == "teakettle" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    cards = board.getAllCards(); //Retrieve all cards on the board
    tilesOK = []; //This will store the cards which can be moved
    for(i = 0; i < cards.length; i++) //Check all cards
    {
      //If the card is adjacent to the Tea Kettle
      if(Math.pow(card.getPosX()-cards[i].getPosX(), 2) + Math.pow(card.getPosY()-cards[i].getPosY(), 2) == 1)
      {
        //If the card is targetable by active abilities
        if(boardutils.isUntargetableBySilver(cards[i]) == false)
        {
          //If the item is resting or grayed
          if(cards[i].isGrayScale() == true || cards[i].isFullSize() == false)
          {
            //Store the coordinates of the good card
            tilesOK.push(cards[i].getPosition());
          }
        }
      }
    }

    if(card.getCounter() > 0)
    {
      //Ask the player to choose the ability
      menu.showInformation(qsTranslate("handmirror", "Choose from the abilities of the %1").arg(card.getName()), -1);
      //Stores the text of the choices
      choiceArray = [
      qsTranslate("handmirror", "Double the counters on itself"),
      qsTranslate("handmirror", "Restore and refresh an adjacent item"),
      qsTranslate("handmirror", "Draw an item")
      ];
      //Stores which choices are enabled and which aren't
      enableArray = ["true", (card.getCounter() >= 3) ? "true" : "false", (mainInventory.isEmpty() || card.getCounter() < 7) ? "false" :"true"];
      //Stores the tooltips for the choices
      toolTipArray = ["", "", ""];
      //Get the player's choice
      var choice = menu.getChoice(choiceArray, enableArray, toolTipArray);
      //Hide the label
      menu.hideInformation();

      //If the player chose to double the counters
      if(choice == 0)
      {
        var prevCounter = card.getCounter();
        card.setCounter(card.getCounter()*2);
        var addedCounter = card.getCounter() - prevCounter;
        if(card.getCounter() > 11)
        {
          card.setCounter(12);
        }
        //Log
        boardutils.logSilverActivation(card.getName());
        if(addedCounter > 1)
        {
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 gained %2 counters.</font>").arg(card.getName()).arg(addedCounter), [1, 2, 3, 4], 1, 0);
        }
        else
        {
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 gained 1 counter.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
        }
      }
      //If the player chose to restore an item
      else if(choice == 1)
      {
        //Counters taken down
        card.setCounter(card.getCounter()-3);

        //Log
        boardutils.logSilverActivation(card.getName());
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 cast 3 counters.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);

        if(tilesOK.length > 0)
        {
          //Ask the player which card would he like to move
          menu.showInformation(qsTranslate("handmirror", "Choose target"), -1);
          //The player chooses one from the good cards
          selectedTile = boardutils.tileChoice(tilesOK, 0);
          selectedCard = board.getCardAtPosition(selectedTile);
          //Hide the label
          menu.hideInformation();
          var wasGray = false;
          var wasShrunken = false;
          if(selectedCard.isGrayScale() == true) {wasGray = true; selectedCard.setGrayScale(0);}
          if(selectedCard.isFullSize() == false) {wasShrunken = true; selectedCard.setShowFullSize(1);}
          if(wasGray == true)
          {
            if(wasShrunken == true)
            {
              gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 restored and refreshed %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);
            }
            else
            {
              gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 restored %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);
            }
          }
          else
          {
            if(wasShrunken == true)
            {
              gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 refreshed %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);
            }
            else
            {
              gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 did nothing to %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);
            }
          }
        }
        else
        {
          menu.showInformation(qsTranslate("handmirror", "No available targets!"));
        }
      }
      else if(choice == 2)
      {
        //Log
        boardutils.logSilverActivation(card.getName());
        gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 cast 7 counters.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);

        //Counters taken down
        card.setCounter(card.getCounter()-7);
        //A card is drawn for the player
        gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "paleturquoise");
        menu.hideInformation();
        //An informational message is displayed (it will disappear automatically)
        menu.showInformation(qsTranslate("handmirror", "You have drawn an item!"));
      }
      //Crop
      card.setShowFullSize(0);
    }
  }


  // TRICKY DIE ////////////////////

  if(cardMirrored == "trickydie" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    //You cannot trash it if it is on safe tile
    if(boardutils.checkForUntrashable(card) == false)
    {
      //Log
      var userNames = gameState.getPlayerNickNames();
      boardutils.logSilverActivation(card.getName());
      gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 has been destroyed.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);

      if(board.isDoubleTile(card.getPosition()) == true)
      {
        //Two cards are drawn for the player
        gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "paleturquoise");
        gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "paleturquoise");
        //An informational message is displayed (it will disappear automatically)
        menu.showInformation(qsTranslate("handmirror", "You have drawn 2 items!"));
      }
      else
      {
        //A card is drawn for the player
        gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "paleturquoise");
        //An informational message is displayed (it will disappear automatically)
        menu.showInformation(qsTranslate("handmirror", "You have drawn an item!"));
      }
      //Sound effect
      var randomSound = rng.randomBetween(1, 5);
      switch(randomSound)
      {
        case 1: gameState.playSoundEffect("dicethrow-1.wav", 1); break;
        case 2: gameState.playSoundEffect("dicethrow-2.wav", 1); break;
        case 3: gameState.playSoundEffect("dicethrow-3.wav", 1); break;
        case 4: gameState.playSoundEffect("dicethrow-4.wav", 1); break;
      }
      board.moveCardToTrash(card.getUID());
    }
  }


  // WHITE KNIGHT ////////////////////

  if(cardMirrored == "whiteknight" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    if(boardutils.checkForImmovable(card) == false)
    {
      if(boardutils.getPossibleKnightMoves(card.getPosition()).length > 0)
      {
        //Ask the player where to move the knight
        menu.showInformation(qsTranslate("handmirror", "Choose where to move the %1").arg(card.getName()), -1);
        //The player chooses one from the good positions
        selectedPos = boardutils.tileChoice(boardutils.getPossibleKnightMoves(card.getPosition()), 1);
        //We got the choice, hide the info
        menu.hideInformation();
        //Unless canceled
        if(selectedPos.x != -1)
        {
          //Log
          boardutils.logSilverActivation(card.getName());
          //Sound effect
          var randomSound = rng.randomBetween(1, 7);
          switch(randomSound)
          {
            case 1: gameState.playSoundEffect("knight-1.wav", 1); break;
            case 2: gameState.playSoundEffect("knight-2.wav", 1); break;
            case 3: gameState.playSoundEffect("knight-3.wav", 1); break;
            case 4: gameState.playSoundEffect("knight-4.wav", 1); break;
            case 5: gameState.playSoundEffect("knight-5.wav", 1); break;
            case 6: gameState.playSoundEffect("knight-6.wav", 1); break;
          }
          //Move the card
          board.moveCard(card.getPosition(), selectedPos);
          //Crop
          card.setShowFullSize(0);
          //Log
          gameState.writeToGameLog(qsTranslate("handmirror", "<font color=\"paleturquoise\">The %1 has been moved to %2%3.</font>").arg(card.getName()).arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1), [1, 2, 3, 4], 1, 0);
        }
      }
      else
      {
        menu.showInformation(qsTranslate("handmirror", "Nowhere to move!"));
      }
    }
    else
    {
      menu.showInformation(qsTranslate("handmirror", "You cannot move this item!"));
    }
  }


  // WHITE PAWN ////////////////////

  if(cardMirrored == "whitepawn" && eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    menu.showInformation(qsTranslate("handmirror", "I am afraid it is not going to work..."));
  }

  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}


//This function returns with the current value of this card which is increased for own cards in the same column
function getCurrentValue()
{
  var cardMirrored = 0;
  if(card.isGrayScale() == false)
  {
    var cardMirrored = boardutils.itemMirrored(card);
  }

  if(cardMirrored != "goldencup" && cardMirrored != "stamp" && cardMirrored != "documentholder" && cardMirrored != "football" && cardMirrored != "sponge" && cardMirrored != "vase" && cardMirrored != "whitepawn" && cardMirrored != "ruler")
  {
    return boardutils.defaultCurrentValue();
  }
  else
  {
    return boardutils.complexCurrentValue();
  }
}
