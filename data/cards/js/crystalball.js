//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Log
    boardutils.logCardPlacement(card.getName());

    var handSize1 = p1Inventory.getAllCards().length;
    var handSize2 = 0;
    var handSize3 = 0;
    var handSize4 = 0;
    var cardAdvantage = 0
    var mostCardsInHand = handSize1;
    
    if(gameState.getGameMode() != 4) {var handSize2 = p2Inventory.getAllCards().length;}
    if(gameState.getGameMode() != 0) {var handSize3 = p3Inventory.getAllCards().length;}
    if(gameState.getGameMode() == 1 || gameState.getGameMode() == 3) {var handSize4 = p4Inventory.getAllCards().length;}

    if(handSize2 > mostCardsInHand) {mostCardsInHand = handSize2;}
    if(handSize3 > mostCardsInHand) {mostCardsInHand = handSize3;}
    if(handSize4 > mostCardsInHand) {mostCardsInHand = handSize4;}

    if(gameState.getCurrentPlayer() == 1) {cardAdvantage = mostCardsInHand - p1Inventory.getAllCards().length;}
    if(gameState.getCurrentPlayer() == 2) {cardAdvantage = mostCardsInHand - p2Inventory.getAllCards().length;}
    if(gameState.getCurrentPlayer() == 3) {cardAdvantage = mostCardsInHand - p3Inventory.getAllCards().length;}
    if(gameState.getCurrentPlayer() == 4) {cardAdvantage = mostCardsInHand - p4Inventory.getAllCards().length;}

    if(cardAdvantage > 0)
    {
      for(i = 0; i < cardAdvantage; i++)
      {
        gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "bisque");
      }
      if(cardAdvantage > 1)
      {
        menu.showInformation(qsTranslate("crystalball", "You have drawn %1 items!").arg(cardAdvantage));
      }
      else
      {
        menu.showInformation(qsTranslate("crystalball", "You have drawn an item!"));
      }
    }
    if(cardAdvantage == 0)
    {
      menu.showInformation(qsTranslate("crystalball", "You already have the most items in hand!"));
    }
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "turnStarted")
  {
    card.setShowFullSize(1);
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    var divination = 0;
    if(gameState.getGameMode() == 0 || gameState.getGameMode() == 4)
    {
      //1v1 opposing
      if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 1 && p2Inventory.getAllCards().length > 0)
      {
        divination = p2Inventory.getAllCards()[rng.randomBetween(0, p2Inventory.getAllCards().length)];
        var choice = 2;
      }
      if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 2 && p1Inventory.getAllCards().length > 0)
      {
        divination = p1Inventory.getAllCards()[rng.randomBetween(0, p1Inventory.getAllCards().length)];
        var choice = 1;
      }
      //1v1 adjacent
      if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 1 && p3Inventory.getAllCards().length > 0)
      {
        divination = p3Inventory.getAllCards()[rng.randomBetween(0, p3Inventory.getAllCards().length)];
        var choice = 3;
      }
      if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 3 && p1Inventory.getAllCards().length > 0)
      {
        divination = p1Inventory.getAllCards()[rng.randomBetween(0, p1Inventory.getAllCards().length)];
        var choice = 1;
      }
    }
    else
    {
      viewablePlayers = [] //This will store the possible players for the first choice
      if(gameState.getCurrentPlayer() == 2 || gameState.getCurrentPlayer() == 3 || (gameState.getCurrentPlayer() == 4 && gameState.getGameMode() == 3))
      {
        if(!p1Inventory.isEmpty())
        {
          viewablePlayers.push(1);
        }
      }
      if(gameState.getCurrentPlayer() == 1 || (gameState.getCurrentPlayer() == 3 && (gameState.getGameMode() == 2 || gameState.getGameMode() == 3)) || gameState.getCurrentPlayer() == 4)
      {
        if(!p2Inventory.isEmpty())
        {
          viewablePlayers.push(2);
        }
      }
      if(gameState.getCurrentPlayer() == 1 || (gameState.getCurrentPlayer() == 2 && gameState.getGameMode() > 1) || gameState.getCurrentPlayer() == 4)
      {
        if(!p3Inventory.isEmpty())
        {
          viewablePlayers.push(3);
        }
      }
      if((gameState.getCurrentPlayer() == 1 && gameState.getGameMode() == 3) || (gameState.getCurrentPlayer() == 2 && gameState.getGameMode() != 2) || (gameState.getCurrentPlayer() == 3 && gameState.getGameMode() != 2 ))
      {
        if(!p4Inventory.isEmpty())
        {
          viewablePlayers.push(4);
        }
      }

      if(viewablePlayers.length > 0)
      {
        menu.showInformation(qsTranslate("crystalball", "Which player's item would you like to view?"), -1);
        //Get the player's answer
        var choice = graphicsViewMenu.getPlayer(viewablePlayers, 0);
        //Now we have the choice, it's time to make a random card of the chosen player's inventory visible
        switch(choice)
        {
          case 1:
            divination = p1Inventory.getAllCards()[rng.randomBetween(0, p1Inventory.getAllCards().length)];
            break;
          case 2:
            divination = p2Inventory.getAllCards()[rng.randomBetween(0, p2Inventory.getAllCards().length)];
            break;
          case 3:
            divination = p3Inventory.getAllCards()[rng.randomBetween(0, p3Inventory.getAllCards().length)];
            break;
          case 4:
            divination = p4Inventory.getAllCards()[rng.randomBetween(0, p4Inventory.getAllCards().length)];
            break;
        }
        //Hide the 'Which player's card...' label
        menu.hideInformation();
      }

    }
    if(divination != 0)
    {
      menu.showInformation(qsTranslate("crystalball", "You revealed this item:"), -1);
      choiceArray = [qsTranslate("crystalball", "%1").arg(divination.getName())]
      enableArray = ["true"];
      toolTipArray = [""];
      choice2 = menu.getChoice(choiceArray, enableArray, toolTipArray);
      menu.hideInformation();
      card.setShowFullSize(0);
      //Log
      boardutils.logSilverActivation(card.getName());
      var userNames = gameState.getPlayerNickNames();
      gameState.writeToGameLog(qsTranslate("crystalball", "<font color=\"paleturquoise\">The %1 revealed the %2 in %3's hand.</font>").arg(card.getName()).arg(divination.getName()).arg(userNames[choice-1]), [gameState.getCurrentPlayer(), choice], 1, 0);
      if(gameState.getGameMode() != 0 && gameState.getGameMode() != 4)
      {
        var thirdParty = [];
        for(tp = 1; tp < 5; tp++)
        {
          if(tp != gameState.getCurrentPlayer() && tp != choice)
          {
            thirdParty.push(tp);
          }
        }
        gameState.writeToGameLog(qsTranslate("crystalball", "<font color=\"paleturquoise\">The %1 revealed an item in %2's hand.</font>").arg(card.getName()).arg(userNames[choice-1]), [thirdParty[0], thirdParty[1]], 1, 0);
      }
    }
    else
    {
      menu.showInformation(qsTranslate("crystalball", "No items to reveal!"));
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}


//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
