//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    var cardHatted = 0;
    switch(card.getPlayer())
    {
      case 1:
        if(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()+1)) != null && board.getCardAtPosition(Point(card.getPosX(),card.getPosY()+1)).isGrayScale() == true)
        {
          cardHatted = board.getCardAtPosition(Point(card.getPosX(),card.getPosY()+1));
        }
        break;
      case 2:
        if(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()-1)) != null && board.getCardAtPosition(Point(card.getPosX(),card.getPosY()-1)).isGrayScale() == true)
        {
          cardHatted = board.getCardAtPosition(Point(card.getPosX(),card.getPosY()-1));
        }
        break;
      case 3:
        if(board.getCardAtPosition(Point(card.getPosX()-1,card.getPosY())) != null && board.getCardAtPosition(Point(card.getPosX()-1,card.getPosY())).isGrayScale() == true)
        {
          cardHatted = board.getCardAtPosition(Point(card.getPosX()-1,card.getPosY()));
        }
        break;
      case 4:
        if(board.getCardAtPosition(Point(card.getPosX()+1,card.getPosY())) != null && board.getCardAtPosition(Point(card.getPosX()+1,card.getPosY())).isGrayScale() == true)
        {
          cardHatted = board.getCardAtPosition(Point(card.getPosX()+1,card.getPosY()));
        }
        break;
    }

    if(cardHatted != 0)
    {
      //Log
      boardutils.logSilverActivation(card.getName());
      gameState.writeToGameLog(qsTranslate("hardhat", "<font color=\"paleturquoise\">The %1 restored the abilities of %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(cardHatted.getPosX())).arg(cardHatted.getPosY()+1).arg(cardHatted.getName()), [1, 2, 3, 4], 1, 0);
      gameState.writeToGameLog(qsTranslate("hardhat", "<font color=\"paleturquoise\">The %1 has been disabled.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);

      cardHatted.setGrayScale(0);
      card.setGrayScale(1);
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
