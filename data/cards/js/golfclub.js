//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

//This function handles all events
function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "turnStarted")
  {
    card.setShowFullSize(1);
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1] && boardutils.checkForUnrotatable(card) == false)
  {
    tilesOK = []; //This will store the tiles of the good cards
    for(ctc = 2; ctc < 10; ctc = ctc+2)
    {
      var xCorri = 0;
      var yCorri = 0;
      if(ctc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
      if(ctc == 4 && card.getPosX() > 0) {xCorri = -1;}
      if(ctc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
      if(ctc == 8 && card.getPosY() > 0) {yCorri = -1;}
      TTC = Point(card.getPosX()+xCorri,card.getPosY()+yCorri);
      CTC = board.getCardAtPosition(TTC);
      if(CTC != null && CTC.getUID() != card.getUID() && boardutils.checkForUnpickable(CTC) == false && boardutils.isUntargetableBySilver(CTC) == false)
      {
        tilesOK.push(TTC);
      }
    }

    if(tilesOK.length > 0)
    {
      //Ask the player which card to pick up
      menu.showInformation(qsTranslate("golfclub", "Choose which item should be taken to hand"), -1);
      //The player chooses one from the good cards
      selectedTile = boardutils.tileChoice(tilesOK, 1);
      //Hide the 'Choose which card...' label
      menu.hideInformation();

      //Unless canceled
      if(selectedTile.x != -1)
      {
        //Log
        boardutils.logSilverActivation(card.getName());

        selectedCard = board.getCardAtPosition(selectedTile);
        //Rotate the Golf Club
        var prevOwner = card.getPlayer()
        if(prevOwner == 1) {card.changeOwner(4);}
        if(prevOwner == 2) {card.changeOwner(3);}
        if(prevOwner == 3) {card.changeOwner(1);}
        if(prevOwner == 4) {card.changeOwner(2);}

        //Log
        gameState.writeToGameLog(qsTranslate("golfclub", "<font color=\"paleturquoise\">The %1 has been rotated by 90° counter-clockwise.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);

        //Remove the card from the board
        board.removeCard(selectedCard.getUID());

        if(selectedCard.getPlayer() == 1)
        {
          p1Inventory.addCard(selectedCard);
        }
        else if(selectedCard.getPlayer() == 2 && gameState.getGameMode() != 4)
        {
          p2Inventory.addCard(selectedCard);
        }
        else if(selectedCard.getPlayer() == 3 && gameState.getGameMode() != 0)
        {
          p3Inventory.addCard(selectedCard);
        }
        else if(selectedCard.getPlayer() == 4 && (gameState.getGameMode() == 1 || gameState.getGameMode() == 3))
        {
          p4Inventory.addCard(selectedCard);
        }
        else
        {
          mainInventory.addCard(selectedCard);
        }

        if(selectedCard.getPlayer() > 0)
        {
          //Log
          var userNames = gameState.getPlayerNickNames();
          gameState.writeToGameLog(qsTranslate("golfclub", "<font color=\"paleturquoise\">The %1 returned %2%3 %4 to %5's hand.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()).arg(userNames[selectedCard.getPlayer()-1]), [1, 2, 3, 4], 1, 0);
        }
        else
        {
          //Log
          gameState.writeToGameLog(qsTranslate("golfclub", "<font color=\"paleturquoise\">The %1 returned %2%3 %4 to the storage.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);
        }
      }
    }
    else
    {
      menu.showInformation(qsTranslate("golfclub", "No item is selectable for ability!"));
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
