//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Deny active abilities
    gameState.setSilverActionsDisabledForTurn(1);
    //Log
    boardutils.logCardPlacement(card.getName());
    var userNames = gameState.getPlayerNickNames();
    gameState.writeToGameLog(qsTranslate("coilspring", "%1 cannot use active abilities for the remaining turn.").arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    cards = board.getAllCards(); //Retrieve all cards on the board
    tilesOK = []; //This will store the cards which can be moved
    for(i = 0; i < cards.length; i++) //Check all cards
    {
      //If the card is adjacent to the Coil spring
      if(Math.pow(card.getPosX()-cards[i].getPosX(), 2) + Math.pow(card.getPosY()-cards[i].getPosY(), 2) == 1)
      {
        //If the card is not unpickable
        if(boardutils.checkForUnpickable(cards[i]) == false)
        {
          //If the card is targetable by active abilities
          if(boardutils.isUntargetableBySilver(cards[i]) == false)
          {
            //If the card has an owner
            if( (cards[i].getPlayer() == 1) ||
                (cards[i].getPlayer() == 2 && gameState.getGameMode() != 4) ||
                (cards[i].getPlayer() == 3 && gameState.getGameMode() != 0) ||
                (cards[i].getPlayer() == 4 && (gameState.getGameMode() == 1 || gameState.getGameMode() == 3)) )
            {
              //Store the coordinates of the good card
              tilesOK.push(cards[i].getPosition());
            }
          }
        }
      }
    }

    if(tilesOK.length > 0)
    {
      //Ask the player which card to pick up
      menu.showInformation(qsTranslate("coilspring", "Choose target"), -1);
      //The player chooses one from the good cards
      selectedTile = boardutils.tileChoice(tilesOK, 1);
      //Hide the 'Choose which card...' label
      menu.hideInformation();
      //Unless canceled
      if(selectedTile.x != -1)
      {
        selectedCard = board.getCardAtPosition(selectedTile);
        board.removeCard(selectedCard.getUID());
        switch(selectedCard.getPlayer())
        {
          case 1: p1Inventory.addCard(selectedCard); break;
          case 2: p2Inventory.addCard(selectedCard); break;
          case 3: p3Inventory.addCard(selectedCard); break;
          case 4: p4Inventory.addCard(selectedCard); break;
        }
        card.setGrayScale(1);
        //Log
        boardutils.logSilverActivation(card.getName());
        var userNames = gameState.getPlayerNickNames();
        gameState.writeToGameLog(qsTranslate("coilspring", "<font color=\"paleturquoise\">The %1 returned %2%3 %4 to %5's hand.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()).arg(userNames[selectedCard.getPlayer()-1]), [1, 2, 3, 4], 1, 0);
        gameState.writeToGameLog(qsTranslate("coilspring", "<font color=\"paleturquoise\">The %1 has been disabled.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
      }
    }
    else
    {
      menu.showInformation(qsTranslate("coilspring", "No item is selectable for ability!"));
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
