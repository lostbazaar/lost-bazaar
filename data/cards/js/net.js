//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    eT = boardutils.getEmptyTiles();
    if(boardutils.isHalted(eT) == false && board.isDoubleTile(card.getPosition()) == false)
    {
      if(boardutils.isSixFix(gameState.getCurrentPlayer()) == true)
      {
        var diceRoll = 6;
      }
      else
      {
        var diceRoll = (rng.randomBetween(1, 7))
      }
      card.setCounter(diceRoll);
      menu.showInformation(qsTranslate("net", "%1%2 %3: %4").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()).arg(boardutils.numberToFace(diceRoll)), 4000);
      //Log
      boardutils.logCardPlacement(card.getName());
      var userNames = gameState.getPlayerNickNames();
      gameState.writeToGameLog(qsTranslate("net", "<font color=\"bisque\">%1 rolled a die: %2.</font>").arg(userNames[gameState.getCurrentPlayer()-1]).arg(card.getCounter()), [1, 2, 3, 4], 1, 0);
      if(card.getCounter() == 1)
      {
        gameState.writeToGameLog(qsTranslate("net", "<font color=\"bisque\">The %1 gained 1 counter.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
      }
      else
      {
        gameState.writeToGameLog(qsTranslate("net", "<font color=\"bisque\">The %1 gained %2 counters.</font>").arg(card.getName()).arg(card.getCounter()), [1, 2, 3, 4], 1, 0);
      }
    }
    else
    {
      card.setGrayScale(1);
      //Logging
      boardutils.logCardPlacement(card.getName());
      gameState.writeToGameLog(qsTranslate("net", "The %1 has been disabled.").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer() && card.isGrayScale() == false)
  {
    if(card.getCounter() == 1)
    {
      card.setCounter(0);
      //Log
      gameState.writeToGameLog(qsTranslate("net", "<font color=\"khaki\">%1%2 %3 cast 1 counter.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
    if(card.getCounter() > 1)
    {
      card.setCounter(card.getCounter()-2);
      //Log
       gameState.writeToGameLog(qsTranslate("net", "<font color=\"khaki\">%1%2 %3 cast 2 counters.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
