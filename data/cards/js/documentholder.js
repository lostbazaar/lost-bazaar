//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    if(mainInventory.getAllCards().length > 0)
    {
      if(boardutils.limitedByPerfume() == false)
      {
        //All empty tiles are fine
        return boardutils.getEmptyTiles();
      }
      else
      {
        return boardutils.getTilesAbovePerfume();
      }
    }
    else
    {
      var array = new Array();
      return array;
    }
  }
  else
  {
    var array = new Array();
    return array;
  }
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Log
    boardutils.logCardPlacement(card.getName());
    //A card is drawn for the player
    gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "bisque");
    //An informational message is displayed (it will disappear automatically)
    menu.showInformation(qsTranslate("documentholder", "You have drawn an item!"));
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.complexCurrentValue();
}
