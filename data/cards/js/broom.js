//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    var eT = boardutils.getEmptyTiles();
    if(boardutils.isHalted(eT) == false)
    {
      if(boardutils.limitedByPerfume() == true)
      {
        eT = boardutils.getTilesAbovePerfume();
      }
      cards = board.getAllCards();
      for(i = 0; i < eT.length; i++)
      {
        var xVaria = eT[i].x;
        var yVaria = eT[i].y;
        var countMovableX = 0
        var countMovableY = 0
        var countImmovableX = 0
        var countImmovableY = 0
        var xDisab = 0
        var yDisab = 0

        for(xVariab = 0; xVariab < board.getWidth(); xVariab++)
        {
          if(board.isDisabledTile(Point(xVariab,yVaria)) == true)
          {
            xDisab++;
          }
        }
        for(yVariab = 0; yVariab < board.getHeight(); yVariab++)
        {
          if(board.isDisabledTile(Point(xVaria,yVariab)) == true)
          {
            yDisab++;
          }
        }

        for(j = 0; j < cards.length; j++)
        {
          if(cards[j].getPosX() == xVaria)
          {
            if(boardutils.checkForImmovable(cards[j]) == true || boardutils.isUntargetableByBronze(cards[j]) == true)
            {
              countImmovableX++;
            }
            else
            {
              countMovableX++;
            }
          }
          if(cards[j].getPosY() == yVaria)
          {
            if(boardutils.checkForImmovable(cards[j]) == true || boardutils.isUntargetableByBronze(cards[j]) == true)
            {
              countImmovableY++;
            }
            else
            {
              countMovableY++;
            }
          }
        }

        if(countMovableX + countImmovableX + countMovableY + countImmovableY < (board.getWidth() + board.getHeight() - xDisab - yDisab - 2) && countMovableX + countMovableY > 0)
        {
          if((countMovableX == 0 && countMovableY + countImmovableY == board.getWidth() - xDisab - 1) || (countMovableY == 0 && countMovableX + countImmovableX == board.getHeight() - yDisab - 1))
          {
          }
          else
          {
            array.push(eT[i]);
          }
        }
      }
    }
    else
    {
      if(boardutils.limitedByPerfume() == false)
      {
        array = boardutils.getEmptyTiles();
      }
      else
      {
        array = boardutils.getTilesAbovePerfume();
      }
    }
  }
  return array;
}


function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    eT = boardutils.getEmptyTiles();
    if(boardutils.isHalted(eT) == false && board.isDoubleTile(card.getPosition()) == false)
    {
      cards = board.getAllCards(); //Retrieve all cards on the board
      tilesOK = []; //This will store the cards which can be moved
      var freeTileX = false
      var freeTileY = false
      for(i = 0; i < cards.length; i++) //Check all cards
      {
        //If the card is not immovable then it might be good
        if(boardutils.checkForImmovable(cards[i]) == false)
        {
          //If the card is in the same line or column as the broom then it is good
          if(cards[i].getPosX() == card.getPosX() && cards[i].getPosY() != card.getPosY())
          {
            for(yVaria=0; yVaria < board.getHeight(); yVaria++)
            {
              if(board.getCardAtPosition(Point(card.getPosX(),yVaria)) == null && board.isDisabledTile(Point(card.getPosX(),yVaria)) == false)
              {
                var freeTileX = true;
                break;
              }
            }
            if(freeTileX == true)
            {
              //Store the coordinates of the good card
              tilesOK.push(cards[i].getPosition());
            }
          }
          if(cards[i].getPosY() == card.getPosY() && cards[i].getPosX() != card.getPosX())
          {
            for(xVaria=0; xVaria < board.getWidth(); xVaria++)
            {
              if(board.getCardAtPosition(Point(xVaria,card.getPosY())) == null && board.isDisabledTile(Point(xVaria,card.getPosY())) == false)
              {
                var freeTileY = true;
                break;
              }
            }
            if(freeTileY == true)
            {
              //Store the coordinates of the good card
              tilesOK.push(cards[i].getPosition());
            }
          }
        }
      }

      var cancelToTop;
      do
      {
        cancelToTop = 0;
        //Ask the player which card would he like to move
        menu.showInformation(qsTranslate("broom", "Choose target"), -1);
        //The player chooses one from the good cards
        selectedTile = boardutils.tileChoice(tilesOK, 0);
        //Hide the 'Choose which card...' label
        menu.hideInformation();
        //Now the player must choose the new place for the card
        menu.showInformation(qsTranslate("broom", "Choose where would you like to move the selected item"), -1);
        //Checks for suitable tiles
        selectedPos = boardutils.tileChoice(boardutils.getInCrossEmptyTiles(selectedTile), 1);
        //We got the choice, hide the info
        menu.hideInformation();
        if(selectedPos.x != -1)
        {
          //Sound effect
          var randomSound = rng.randomBetween(1, 5);
          switch(randomSound)
          {
            case 1: gameState.playSoundEffect("sweep-1.wav", 1); break;
            case 2: gameState.playSoundEffect("sweep-2.wav", 1); break;
            case 3: gameState.playSoundEffect("sweep-3.wav", 1); break;
            case 4: gameState.playSoundEffect("sweep-4.wav", 1); break;
          }
          //Move the card
          board.moveCard(selectedTile, selectedPos);
          //Logging
          boardutils.logCardPlacement(card.getName());
          gameState.writeToGameLog(qsTranslate("broom", "<font color=\"bisque\">The %1 moved %2%3 %4 to %5%6.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg((board.getCardAtPosition(selectedPos)).getName()).arg(boardutils.convertToABC(selectedPos.x)).arg(selectedPos.y+1), [1, 2, 3, 4], 1, 0);
        }
        else
        {
          cancelToTop = 1;
        }
      }
      while(cancelToTop == 1);
    }
    else
    {
      card.setGrayScale(1);
      //Log
      boardutils.logCardPlacement(card.getName());
      gameState.writeToGameLog(qsTranslate("broom", "The %1 has been disabled.").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
