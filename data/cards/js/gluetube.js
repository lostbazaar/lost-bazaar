//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}


function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "turnStarted")
  {
    card.setShowFullSize(1);
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    if((trash.getAllCards()).length > 0)
    {
      var firstAbilityAvailable = false
      var secondAbilityAvailable = false
      var trashCards = trash.getAllCards()
      for(i = 0; i < trashCards.length; i++)
      {
        if(trashCards[i].getBaseValue() > 0)
        {
          firstAbilityAvailable = true;
          break;
        }
      }
      if(card.getCounter() >= 11)
      {
        for(i = 0; i < trashCards.length; i++)
        {
          if(trashCards[i].hasIcon("tcan") == false)
          {
            secondAbilityAvailable = true;
            break;
          }
        }
      }

      if(firstAbilityAvailable == true || secondAbilityAvailable == true)
      {
        menu.showInformation(qsTranslate("gluetube", "Choose from the abilities of the %1").arg(card.getName()), -1);
        choiceArray = [
        qsTranslate("gluetube", "Return an item from the trash to the storage"),
        qsTranslate("gluetube", "Reclaim an item to your hand")
        ];
        enableArray = [firstAbilityAvailable == true ? "true" : "false", secondAbilityAvailable == true ? "true" : "false"];
        toolTipArray = [firstAbilityAvailable == true ? "" : qsTranslate("gluetube", "No item with positive value in trash"), secondAbilityAvailable == true ? "" : qsTranslate("gluetube", "Not enough counters / No reclaimable item in the trash")];
        var choice = menu.getChoice(choiceArray, enableArray, toolTipArray);
        menu.hideInformation();

        if(choice == 0)
        {
          //Log
          boardutils.logSilverActivation(card.getName());

          menu.showInformation(qsTranslate("gluetube", "Choose an item to recycle"), -1);
          do
          {
            var selectedToRecycle = trash.cardChoice(trashCards, 0, 1);
          }
          while(selectedToRecycle.getBaseValue() <= 0);
          menu.hideInformation();
          var addedCounter = selectedToRecycle.getBaseValue()
          card.setCounter(card.getCounter() + addedCounter)
          if(card.getCounter() > 12)
          {
            card.setCounter(12);
          }
          trash.removeCard(selectedToRecycle.getUID());
          mainInventory.addCard(selectedToRecycle);

          //Log
          gameState.writeToGameLog(qsTranslate("gluetube", "<font color=\"paleturquoise\">The %1 recycled the %2.</font>").arg(card.getName()).arg(selectedToRecycle.getName()), [1, 2, 3, 4], 1, 0);
          if(addedCounter > 1)
          {
            gameState.writeToGameLog(qsTranslate("gluetube", "<font color=\"paleturquoise\">The %1 gained %2 counters.</font>").arg(card.getName()).arg(addedCounter), [1, 2, 3, 4], 1, 0);
          }
          else
          {
            gameState.writeToGameLog(qsTranslate("gluetube", "<font color=\"paleturquoise\">The %1 gained 1 counter.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
          }
        }
        if(choice == 1)
        {
          //Log
          boardutils.logSilverActivation(card.getName());
          gameState.writeToGameLog(qsTranslate("gluetube", "<font color=\"paleturquoise\">The %1 cast 11 counters.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);

          menu.showInformation(qsTranslate("gluetube", "Choose an item to reclaim"), -1);
          do
          {
            var selectedToReclaim = trash.cardChoice(trashCards, 0, 1);
          }
          while(selectedToReclaim.hasIcon("tcan") == true);
          menu.hideInformation();
          card.setCounter(card.getCounter() - 11);
          trash.removeCard(selectedToReclaim.getUID());
          if(card.getPlayer() == 1) {p1Inventory.addCard(selectedToReclaim);}
          if(card.getPlayer() == 2) {p2Inventory.addCard(selectedToReclaim);}
          if(card.getPlayer() == 3) {p3Inventory.addCard(selectedToReclaim);}
          if(card.getPlayer() == 4) {p4Inventory.addCard(selectedToReclaim);}

          //Log
          var userNames = gameState.getPlayerNickNames();
          gameState.writeToGameLog(qsTranslate("gluetube", "<font color=\"paleturquoise\">The %1 returned the %2 to %3's hand.</font>").arg(card.getName()).arg(selectedToReclaim.getName()).arg(userNames[card.getPlayer()-1]), [1, 2, 3, 4], 1, 0);
        }
        card.setShowFullSize(0);
      }
      else
      {
        menu.showInformation(qsTranslate("gluetube", "No items in trash fulfill the criteria!"));
      }
    }
    else
    {
      menu.showInformation(qsTranslate("gluetube", "No items in trash!"));
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}


function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
