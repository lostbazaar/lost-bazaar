//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  if(eventData[0] == "turnStarted")
  {
    card.setShowFullSize(1)
  }
  
  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    if(gameState.hasCurrentPlayerDrawnCard() == false)
    {
      var tilesOK = [];
      var emptySpecialTiles = [];
    
      var eT = boardutils.getEmptyTiles();
      for(i = 0; i < eT.length; i++)
      {
        if(board.isDoubleTile(eT[i]) == true || board.isSafeTile(eT[i]) == true || board.isInnovativeTile(eT[i]) == true)
        {
          emptySpecialTiles.push(eT[i]);
        }
      }
      if(emptySpecialTiles.length > 0)
      {
        for(ctc = 2; ctc < 10; ctc = ctc+2)
        {
          var xCorri = 0;
          var yCorri = 0;
          if(ctc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
          if(ctc == 4 && card.getPosX() > 0) {xCorri = -1;}
          if(ctc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
          if(ctc == 8 && card.getPosY() > 0) {yCorri = -1;}
          TTC = Point(card.getPosX()+xCorri,card.getPosY()+yCorri);
          CTC = board.getCardAtPosition(TTC);
          if(CTC != null && CTC.getPlayer() != card.getPlayer() && boardutils.checkForImmovable(CTC) == false && boardutils.isUntargetableBySilver(CTC) == false)
          {
            tilesOK.push(TTC);
          }
        }
      }
      if(tilesOK.length > 0)
      {
        menu.showInformation(qsTranslate("busticket", "Choose which item would you like to move"), -1);
        selectedTile = boardutils.tileChoice(tilesOK, 1);
        menu.hideInformation();
        //Unless canceled
        if(selectedTile.x != -1)
        {
          selectedCard = board.getCardAtPosition(selectedTile);
          menu.showInformation(qsTranslate("busticket", "Choose where would you like to move the selected card"), -1);
          selectedPos = boardutils.tileChoice(emptySpecialTiles, 0);
          menu.hideInformation();
          board.moveCard(selectedTile, selectedPos);
          gameState.setCardDrawDisabledForTurn(true);

          //Log
          gameState.writeToGameLog(qsTranslate("busticket", "<font color=\"bisque\">The %1 moved %2%3 %4 to %5%6.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg((board.getCardAtPosition(selectedPos)).getName()).arg(boardutils.convertToABC(selectedPos.x)).arg(selectedPos.y+1), [1, 2, 3, 4], 1, 0);
          var userNames = gameState.getPlayerNickNames();
          gameState.writeToGameLog(qsTranslate("busticket", "%1 cannot draw items for the remaining turn.").arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
          //Crop
          if(card.isFullSize() == true)
          {
            card.setShowFullSize(0);
          }
        }
      }
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card which is increased for own stamps
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
