//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Log
    boardutils.logCardPlacement(card.getName());
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "turnStarted")
  {
    card.setShowFullSize(1);
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    tilesOK = [];
    for(ctc = 2; ctc < 10; ctc = ctc+2)
    {
      var xCorri = 0;
      var yCorri = 0;
      if(ctc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
      if(ctc == 4 && card.getPosX() > 0) {xCorri = -1;}
      if(ctc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
      if(ctc == 8 && card.getPosY() > 0) {yCorri = -1;}
      TTC = Point(card.getPosX()+xCorri,card.getPosY()+yCorri);
      CTC = board.getCardAtPosition(TTC);
      if(CTC != null && CTC != card && CTC.getCounter() > 0 && boardutils.isUntargetableBySilver(CTC) == false)
      {
        tilesOK.push(TTC);
      }
    }
    if(tilesOK.length > 0)
    {
      menu.showInformation(qsTranslate("rasp", "Choose target"), -1);
      selectedTile = boardutils.tileChoice(tilesOK, 1);
      menu.hideInformation();
      selectedCard = board.getCardAtPosition(selectedTile);
      //Unless canceled
      if(selectedTile.x != -1)
      {
        //Log
        boardutils.logSilverActivation(card.getName());
        gameState.writeToGameLog(qsTranslate("rasp", "<font color=\"paleturquoise\">The %1 removed 1 counter from %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);
        
        if(selectedCard.getCounter() > 1)
        {
          selectedCard.setCounter(selectedCard.getCounter()-1);
        }
        else
        {
          selectedCard.setCounter(0);
          if(boardutils.checkForUntrashable(selectedCard) == false)
          {
            //Skip damaging Bomb - TEMPORARY SOLUTION
            if((selectedCard.getID() != "bomb" && (selectedCard.getID() != "handmirror" || boardutils.itemMirrored(targetedByShaker) != "bomb")) || selectedCard.isGrayScale() == true)
            {
              //Log
              gameState.writeToGameLog(qsTranslate("rasp", "<font color=\"paleturquoise\">The %1 destroyed %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);

              board.moveCardToTrash(selectedCard.getUID());
            }
          }
        }
        card.setShowFullSize(0);
      }
    }
    else
    {
      menu.showInformation(qsTranslate("rasp", "You cannot use this ability!"));
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
