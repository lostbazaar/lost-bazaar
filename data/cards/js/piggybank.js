//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    card.setCounter(1);
    //Log
    boardutils.logCardPlacement(card.getName());
    gameState.writeToGameLog(qsTranslate("piggybank", "<font color=\"bisque\">The %1 gained 1 counter.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer() && card.isGrayScale() == false)
  {
    //Log
    gameState.writeToGameLog(qsTranslate("piggybank", "<font color=\"khaki\">%1%2 %3 gained 1 counter.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()), [1, 2, 3, 4], 1, 0);
    if(card.getCounter() < 6)
    {
      card.setCounter(card.getCounter()+1);
    }
  }
  if(card.isGrayScale() == false)
  {
    if(card.getCounter() > 6)
    {
      card.setCounter(6);
    }
  }
  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1] && gameState.getCurrentPlayer() == card.getPlayer() && card.isGrayScale() == false)
  {
    //Sound effect
    var randomSound = rng.randomBetween(1, 6);
    switch(randomSound)
    {
      case 1: gameState.playSoundEffect("snort-1.wav", 1); break;
      case 2: gameState.playSoundEffect("snort-2.wav", 1); break;
      case 3: gameState.playSoundEffect("snort-3.wav", 1); break;
      case 4: gameState.playSoundEffect("snort-4.wav", 1); break;
      case 5: gameState.playSoundEffect("snort-5.wav", 1); break;
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
    if(card.isGrayScale() == false)
    {
      if(card.getCounter() > 6)
      {
        card.setCounter(6);
      }
    }
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
