//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  //Handle the cardPlacedOnBoard event
  //Check if this card is the one that was placed
  //If it is, then execute the bronze action
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    card.setCounter(3);
    //Log
    boardutils.logCardPlacement(card.getName());
    gameState.writeToGameLog(qsTranslate("bomb", "<font color=\"bisque\">The %1 gained 3 counters.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer() && card.isGrayScale() == false)
  {
    if(card.getCounter() > 0)
    {
      card.setCounter(card.getCounter()-1);
      //Log
      gameState.writeToGameLog(qsTranslate("bomb", "<font color=\"khaki\">%1%2 %3 cast 1 counter.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
  }

  if(card.getCounter() == 0 && card.isGrayScale() == false && boardutils.checkForUntrashable(card) == false)
  {

    cards = board.getAllCards(); //Retrieve all cards on the board
    cardsBomb = []; //Store the bomb cards
    cardsBombX = []; //Store the X coordinate of bombs
    cardsBombY = []; //Store the Y coordinate of bombs
    cardsUIDBOOM = []; //Stores UIDs for exploding cards
    var pigDraw1 = 0;
    var pigDraw2 = 0;
    var pigDraw3 = 0;
    var pigDraw4 = 0;

    //Include the initiator on the list

    cardsBomb.push(card);
    cardsBombX.push(card.getPosX());
    cardsBombY.push(card.getPosY());
    cardsUIDBOOM.push(card.getUID());

    for(bN = 0; bN < cardsBombX.length; bN++)
    {
      for(ctc = 2; ctc < 10; ctc = ctc+2)
      {
        var xCorri = 0;
        var yCorri = 0;
        if(ctc == 2 && cardsBombY[bN] < board.getHeight() - 1) {yCorri = 1;}
        if(ctc == 4 && cardsBombX[bN] > 0) {xCorri = -1;}
        if(ctc == 6 && cardsBombX[bN] < board.getWidth() - 1) {xCorri = 1;}
        if(ctc == 8 && cardsBombY[bN] > 0) {yCorri = -1;}
        CTC = board.getCardAtPosition(Point(cardsBombX[bN]+xCorri,cardsBombY[bN]+yCorri));
        //If the card is trashable then it can be marked
        if(CTC != null && CTC != cardsBomb[bN] && boardutils.checkForUntrashable(CTC) == false)
        {
          //Check whether it was marked previously
          var alreadyMarked = false;
          for(eN = 0; eN < cardsUIDBOOM.length; eN++)
          {
            if(cardsUIDBOOM[eN] == CTC.getUID())
            {
              alreadyMarked = true;
            }
          }
          if(alreadyMarked == false)
          {
            //Store the coordinates and UIDs of the good card
            cardsUIDBOOM.push(CTC.getUID());
            //If the marked card is a Bomb, add it to the chain reaction
            if((CTC.getID() == "bomb" && CTC.isGrayScale() == false) || (CTC.getID() == "handmirror" && boardutils.itemMirrored(CTC) == "bomb"))
            {
              cardsBomb.push(CTC);
              cardsBombX.push(CTC.getPosX());
              cardsBombY.push(CTC.getPosY());
            }
            //If the marked card is a Piggy Bank, remember the amount of counters on it
            if((CTC.getID() == "piggybank" && CTC.isGrayScale() == false) || (CTC.getID() == "handmirror" && boardutils.itemMirrored(CTC) == "piggybank"))
            {
              switch(CTC.getPlayer())
              {
                case 1:
                  pigDraw1 = pigDraw1 + CTC.getCounter();
                  break;
                case 2:
                  pigDraw2 = pigDraw2 + CTC.getCounter();
                  break;
                case 3:
                  pigDraw3 = pigDraw3 + CTC.getCounter();
                  break;
                case 4:
                  pigDraw4 = pigDraw4 + CTC.getCounter();
                  break;
              }
            }
          }
        }
      }
    }
    //Log
    gameState.writeToGameLog(qsTranslate("bomb", "<font color=\"khaki\">%1%2 %3 has been destroyed.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()), [1, 2, 3, 4], 1, 0);
    //Remove the initiator
    board.moveCardToTrash(cardsUIDBOOM[0]);
    //Remove marked items
    if(cardsUIDBOOM.length > 1)
    {
      for(boomOrder = 1; boomOrder < cardsUIDBOOM.length; boomOrder++)
      {
        //Log
        var explodingCard = gameState.cardForUID(cardsUIDBOOM[boomOrder])
        gameState.writeToGameLog(qsTranslate("bomb", "<font color=\"khaki\">The %1 destroyed %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(explodingCard.getPosX())).arg(explodingCard.getPosY()+1).arg(explodingCard.getName()), [1, 2, 3, 4], 1, 0);
        board.moveCardToTrash(cardsUIDBOOM[boomOrder]);
      }
    }
    if(pigDraw1 > 0) {for(drawCount = 0; drawCount < pigDraw1; drawCount++) {gameState.drawCard(1, 0, 0, 0, 0, "khaki");}}
    if(pigDraw2 > 0) {for(drawCount = 0; drawCount < pigDraw2; drawCount++) {gameState.drawCard(2, 0, 0, 0, 0, "khaki");}}
    if(pigDraw3 > 0) {for(drawCount = 0; drawCount < pigDraw3; drawCount++) {gameState.drawCard(3, 0, 0, 0, 0, "khaki");}}
    if(pigDraw4 > 0) {for(drawCount = 0; drawCount < pigDraw4; drawCount++) {gameState.drawCard(4, 0, 0, 0, 0, "khaki");}}

  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
