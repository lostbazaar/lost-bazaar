//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.megap

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    var arrayNumber = 0;
    for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
    {
      for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
      {
        if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
        {
          //Changed the variable names to avoid conflict during called condition checks
          for(ttcm = 2; ttcm < 10; ttcm = ttcm+2)
          {
            var xCorri = 0;
            var yCorri = 0;
            if(ttcm == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
            if(ttcm == 4 && xVaria > 0) {xCorri = -1;}
            if(ttcm == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
            if(ttcm == 8 && yVaria > 0) {yCorri = -1;}
            TTCM = Point(xVaria+xCorri,yVaria+yCorri);
            CTCM = board.getCardAtPosition(TTCM);
            if(CTCM != null && CTCM.getPlayer() == gameState.getCurrentPlayer() && boardutils.checkForUnpickable(CTCM) == false && boardutils.isUntargetableByBronze(CTCM) == false)
            {
//              if(boardutils.limitedByPerfume() == false || boardutils.tileAffectedByPerfume(xVaria+xCorri, yVaria+yCorri) == true)
              if(boardutils.limitedByPerfume() == false || boardutils.tileAffectedByPerfume(xVaria+xCorri, yVaria+yCorri) == true || ((boardutils.getTilesAbovePerfume()).length == 1 && boardutils.tileAffectedByPerfume(xVaria, yVaria) == true))
              {
                if(megaputils.secondMegaphoneCondition(CTCM, xVaria, yVaria) == true)
                {
                  array[arrayNumber] = new Point(xVaria,yVaria);
                  arrayNumber++
                  break;
                }
              }
            }
          }
        }
      }
    }
  }
  return array;
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    tilesOK = [];
    //Changed the variable names to avoid conflict during called condition checks
    for(ctcm = 2; ctcm < 10; ctcm = ctcm+2)
    {
      var xCorri = 0;
      var yCorri = 0;
      if(ctcm == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
      if(ctcm == 4 && card.getPosX() > 0) {xCorri = -1;}
      if(ctcm == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
      if(ctcm == 8 && card.getPosY() > 0) {yCorri = -1;}
      TTCM = Point(card.getPosX()+xCorri,card.getPosY()+yCorri);
      CTCM = board.getCardAtPosition(TTCM);
      if(CTCM != null && CTCM.getPlayer() == gameState.getCurrentPlayer() && boardutils.checkForUnpickable(CTCM) == false && boardutils.isUntargetableByBronze(CTCM) == false)
      {
        if(boardutils.limitedByPerfume() == false || boardutils.tileAffectedByPerfume(card.getPosX()+xCorri, card.getPosY()+yCorri) == true)
        {
          if(megaputils.secondMegaphoneCondition(CTCM, card.getPosX(), card.getPosY()) == true)
          {
            if(boardutils.haltedByNet(CTCM.getBaseValue()) == false)
            {
              tilesOK.push(TTCM);
            }
            else
            {
              //The only Net with 2 counters is still a possible target
              var suchNets = 0;
              if(CTCM.getID() == "net" && CTCM.getCounter() == 2 && CTCM.isGrayScale() == false)
              {
                var netArray = board.getCardsWithName("net");
                for(netNr = 0; netNr < netArray.length; netNr++)
                {
                  if(netArray[netNr].getCounter() == 2 && netArray[netNr].isGrayScale() == false)
                  {
                    suchNets++;
                  }
                }
                if(suchNets < 2)
                {
                  tilesOK.push(TTCM);
                }
              }
            }
          }
        }
      }
    }
    menu.showInformation(qsTranslate("megaphone", "Choose target"), -1);
    selectedTile = boardutils.tileChoice(tilesOK, 0);
    menu.hideInformation();
    selectedCard = board.getCardAtPosition(selectedTile);
    //Logging
    boardutils.logCardPlacement(card.getName());
    var userNames = gameState.getPlayerNickNames();
    gameState.writeToGameLog(qsTranslate("megaphone", "<font color=\"bisque\">The %1 returned %2%3 %4 to %5's hand.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()).arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
    gameState.writeToGameLog(qsTranslate("megaphone", "<font color=\"bisque\">The %1 played the %2 from %3's hand to %4%5.</font>").arg(card.getName()).arg(selectedCard.getName()).arg(userNames[gameState.getCurrentPlayer()-1]).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1), [1, 2, 3, 4], 1, 0);

    board.removeCard(selectedCard.getUID());
    if(gameState.getCurrentPlayer() == 1)
    {
      p1Inventory.addCard(selectedCard);
      p1Inventory.moveCardToBoard(selectedCard.getUID(), selectedTile, 1);
    }
    if(gameState.getCurrentPlayer() == 2)
    {
      p2Inventory.addCard(selectedCard);
      p2Inventory.moveCardToBoard(selectedCard.getUID(), selectedTile, 1);
    }
    if(gameState.getCurrentPlayer() == 3)
    {
      p3Inventory.addCard(selectedCard);
      p3Inventory.moveCardToBoard(selectedCard.getUID(), selectedTile, 1);
    }
    if(gameState.getCurrentPlayer() == 4)
    {
      p4Inventory.addCard(selectedCard);
      p4Inventory.moveCardToBoard(selectedCard.getUID(), selectedTile, 1);
    }

    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card which is increased for own cards in the same column
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
