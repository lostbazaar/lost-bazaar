//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    var arrayNumber = 0;

    for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
    {
      for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
      {
        if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
        {
          if(board.isSafeTile(Point(xVaria,yVaria)) == false)
          {
            var dominoCondition = false;
            for(ttc = 2; ttc < 10; ttc = ttc+2)
            {
              var xCorri = 0;
              var yCorri = 0;
              if(ttc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
              if(ttc == 4 && xVaria > 0) {xCorri = -1;}
              if(ttc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
              if(ttc == 8 && yVaria > 0) {yCorri = -1;}
              TTC = Point(xVaria+xCorri,yVaria+yCorri);
              CTC = board.getCardAtPosition(TTC);
              if(CTC != null && CTC.getPlayer() == gameState.getCurrentPlayer())
              {
                dominoCondition = true;
                break;
              }
            }
            if(dominoCondition == true)
            {
              cardsCFUR = board.getAllCards();
              var instantlyRotatable = true;
              for(cfur = 0; cfur < cardsCFUR.length; cfur++)
              {

                if((cardsCFUR[cfur].getID() == "celticshield" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "celticshield")) && cardsCFUR[cfur].isGrayScale() == false && cardsCFUR[cfur].getPlayer() == card.getPlayer())
                {
                  var shieldDistance = Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2)
                  if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
                  {
                    instantlyRotatable = false;
                    break;
                  }
                }

                if(card.getID() != "padlock" || (card.getID() == "padlock" && card.isGrayScale() == true))
                {
                  if(cardsCFUR[cfur].getID() == "padlock" && cardsCFUR[cfur].isGrayScale() == false && Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2) == 1)
                  {
                    instantlyRotatable = false;
                    break;
                  }
                }
                else
                {
                  instantlyRotatable = false;
                  break;
                }

                if((cardsCFUR[cfur].getID() == "hardhat" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "celticshield")) && cardsCFUR[cfur].isGrayScale() == false)
                {
                  switch(cardsCFUR[cfur].getPlayer())
                  {
                    case 1:
                      if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria-1) {instantlyRotatable = false;}
                      break;
                    case 2:
                      if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria+1) {instantlyRotatable = false;}
                      break;
                    case 3:
                      if(cardsCFUR[cfur].getPosX() == xVaria+1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;}
                      break;
                    case 4:
                      if(cardsCFUR[cfur].getPosX() == xVaria-1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;}
                      break;
                  }
                }
              }
            }

            if(dominoCondition == true && instantlyRotatable == true)
            {
              array[arrayNumber] = new Point(xVaria,yVaria);
              arrayNumber++;
            }
          }
        }
      }
    }
    return array;
  }
  else
  {
    return array;
  }
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    var prevOwner = card.getPlayer();
    if(prevOwner == 1) {card.changeOwner(2); gameState.overrideNextPlayer(1);}
    if(prevOwner == 2) {card.changeOwner(1); gameState.overrideNextPlayer(2);}
    if(prevOwner == 3) {card.changeOwner(4); gameState.overrideNextPlayer(3);}
    if(prevOwner == 4) {card.changeOwner(3); gameState.overrideNextPlayer(4);}
    //Log
    boardutils.logCardPlacement(card.getName());
    gameState.writeToGameLog(qsTranslate("hourglass", "<font color=\"bisque\">The %1 has been rotated by 180°.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);

    card.setCounter(3);
    //Log
    boardutils.logCardPlacement(card.getName());
    gameState.writeToGameLog(qsTranslate("hourglass", "<font color=\"bisque\">The %1 gained 3 counters.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer() && card.isGrayScale() == false)
  {
    if(card.getCounter() > 0)
    {
      card.setCounter(card.getCounter()-1)
      //Log
      gameState.writeToGameLog(qsTranslate("hourglass", "<font color=\"khaki\">%1%2 %3 cast 1 counter.<font color=\"khaki\">").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
  }

  if(eventData[0] == "endOfTurn" && card.getPlayer() == gameState.getCurrentPlayer() &&  card.getCounter() == 0 && card.isGrayScale() == false && boardutils.checkForUnpickable(card) == false)
  {
    //Log
    var userNames = gameState.getPlayerNickNames();
    gameState.writeToGameLog(qsTranslate("hourglass", "<font color=\"khaki\">%1%2 %3 has been returned to %4's hand.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()).arg(userNames[card.getPlayer()-1]), [1, 2, 3, 4], 1, 0);

    board.removeCard(card.getUID());
    if(card.getPlayer() == 1) {p1Inventory.addCard(card);}
    if(card.getPlayer() == 2) {p2Inventory.addCard(card);}
    if(card.getPlayer() == 3) {p3Inventory.addCard(card);}
    if(card.getPlayer() == 4) {p4Inventory.addCard(card);}
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
