//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    //You cannot trash it if it is on safe tile
    if(boardutils.checkForUntrashable(card) == false)
    {
      //Log
      var userNames = gameState.getPlayerNickNames();
      boardutils.logSilverActivation(card.getName());
      gameState.writeToGameLog(qsTranslate("trickydie", "<font color=\"paleturquoise\">The %1 has been destroyed.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);

      if(board.isDoubleTile(card.getPosition()) == true)
      {
        //Two cards are drawn for the player
        gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "paleturquoise");
        gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "paleturquoise");
        //An informational message is displayed (it will disappear automatically)
        menu.showInformation(qsTranslate("trickydie", "You have drawn 2 items!"));
      }
      else
      {
        //A card is drawn for the player
        gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "paleturquoise");
        //An informational message is displayed (it will disappear automatically)
        menu.showInformation(qsTranslate("trickydie", "You have drawn an item!"));
      }
      //Sound effect
      var randomSound = rng.randomBetween(1, 5);
      switch(randomSound)
      {
        case 1: gameState.playSoundEffect("dicethrow-1.wav", 1); break;
        case 2: gameState.playSoundEffect("dicethrow-2.wav", 1); break;
        case 3: gameState.playSoundEffect("dicethrow-3.wav", 1); break;
        case 4: gameState.playSoundEffect("dicethrow-4.wav", 1); break;
      }
      board.moveCardToTrash(card.getUID());
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
