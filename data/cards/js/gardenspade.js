//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    var arrayNumber = 0;
    for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
    {
      for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
      {
        if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
        {
          var dominoCondition = false;
          var entryCondition = false;
          for(ctc = 2; ctc < 10; ctc = ctc+2)
          {
            var xCorri = 0;
            var yCorri = 0;
            if(ctc == 2 && yVaria < board.getHeight() - 1) {yCorri = 1;}
            if(ctc == 4 && xVaria > 0) {xCorri = -1;}
            if(ctc == 6 && xVaria < board.getWidth() - 1) {xCorri = 1;}
            if(ctc == 8 && yVaria > 0) {yCorri = -1;}
            CTC = board.getCardAtPosition(Point(xVaria+xCorri,yVaria+yCorri));

            if(CTC != null && CTC.getUID() != card.getUID())
            {
              var targetValue = CTC.getBaseValue();

              if(CTC.getPlayer() == gameState.getCurrentPlayer())
              {
                dominoCondition = true;
              }
              if(targetValue > 0 && boardutils.isUntargetableByBronze(CTC) == false && (targetValue <= (trash.getAllCards()).length || targetValue <= (mainInventory.getAllCards()).length))
              {
                entryCondition = true;
              }
              if(dominoCondition == true && entryCondition == true)
              {
                array[arrayNumber] = new Point(xVaria,yVaria);
                arrayNumber++;
                break;
              }
            }
          }
        }
      }
    }
  }
  return array;
}


//This function handles all events
function event(eventData)
{
  //Handle the cardPlacedOnBoard event
  //Check if this card is the one that was placed
  //If it is, then execute the bronze action
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    tilesOK = [];

    for(ctc = 2; ctc < 10; ctc = ctc+2)
    {
      var xCorri = 0;
      var yCorri = 0;
      firstAbilityAvailable = false;
      secondAbilityAvailable = false;
      if(ctc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
      if(ctc == 4 && card.getPosX() > 0) {xCorri = -1;}
      if(ctc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
      if(ctc == 8 && card.getPosY() > 0) {yCorri = -1;}
      TTC = Point(card.getPosX()+xCorri,card.getPosY()+yCorri);
      CTC = board.getCardAtPosition(TTC);

      if(CTC != null && CTC.getUID() != card.getUID())
      {
        var targetValue = CTC.getBaseValue();
        if(targetValue > 0 && boardutils.isUntargetableByBronze(CTC) == false)
        {
          if(targetValue <= (trash.getAllCards()).length)
          {
            firstAbilityAvailable = true;
          }
          if(boardutils.isUntargetableByBronze(CTC) == false && targetValue <= (mainInventory.getAllCards()).length)
          {
            secondAbilityAvailable = true;
          }
        }
        if(firstAbilityAvailable == true || secondAbilityAvailable == true)
        {
          tilesOK.push(TTC);
        }
      }
    }

    firstAbilityAvailable = false;
    secondAbilityAvailable = false;
    menu.showInformation(qsTranslate("gardenspade", "Choose target"), -1);
    selectedTile = boardutils.tileChoice(tilesOK, 0);
    menu.hideInformation();
    selectedCard = board.getCardAtPosition(selectedTile);
    var targetValue = selectedCard.getBaseValue();
    if(selectedCard.getBaseValue() <= (trash.getAllCards()).length)
    {
      firstAbilityAvailable = true;
    }
    if(selectedCard.getBaseValue() <= (mainInventory.getAllCards()).length)
    {
      secondAbilityAvailable = true;
    }

    if(selectedCard.getBaseValue() == 1)
    {
      menu.showInformation(qsTranslate("gardenspade", "Choose from the abilities of the %1").arg(card.getName()), -1);
      choiceArray = [
      qsTranslate("gardenspade", "Return the target and an item from the trash to the storage"),
      qsTranslate("gardenspade", "Draw an item")
      ];
    }
    if(selectedCard.getBaseValue() > 1)
    {
      menu.showInformation(qsTranslate("gardenspade", "Choose from the abilities of the %1").arg(card.getName()), -1);
      choiceArray = [
      qsTranslate("gardenspade", "Return the target and %1 items from the trash to the storage").arg(targetValue),
      qsTranslate("gardenspade", "Draw %1 items and discard %2 of them").arg(targetValue).arg(targetValue-1)
      ];
    }

    enableArray = [firstAbilityAvailable == true ? "true" : "false", secondAbilityAvailable == true ? "true" : "false"];
    toolTipArray = [firstAbilityAvailable == true ? "" : qsTranslate("gardenspade", "Not enough items in trash"), secondAbilityAvailable == true ? "" : qsTranslate("gardenspade", "Not enough items in storage")];

    var choice = menu.getChoice(choiceArray, enableArray, toolTipArray);
    menu.hideInformation();

    if(choice == 0)
    {
      //Log
      boardutils.logCardPlacement(card.getName());

      var trashCards = trash.getAllCards();
      menu.showInformation(qsTranslate("gardenspade", "Choose an item to recycle"), -1);
      for(j = 0; j < targetValue; j++)
      {
        var trashCards = trash.getAllCards()
        var selectedToRecycle = trash.cardChoice(trashCards, 0, 1);
        trash.removeCard(selectedToRecycle.getUID());
        mainInventory.addCard(selectedToRecycle);

        //Log
        gameState.writeToGameLog(qsTranslate("gardenspade", "<font color=\"bisque\">The %1 recycled the %2.</font>").arg(card.getName()).arg(selectedToRecycle.getName()), [1, 2, 3, 4], 1, 0);
      }
      menu.hideInformation();

      //Log
      gameState.writeToGameLog(qsTranslate("gardenspade", "<font color=\"bisque\">The %1 returned %2%3 %4 to the storage.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);

      board.removeCard(selectedCard.getUID());
      mainInventory.addCard(selectedCard);
    }
    if(choice == 1)
    {
      //Log
      boardutils.logCardPlacement(card.getName());

      storageSize = mainInventory.getAllCards().length
      storageCards = mainInventory.getAllCards()

      offer1 = storageCards[rng.randomBetween(0, storageSize)]
      if(targetValue >= 2)
      {
        do
        {
          offer2 = storageCards[rng.randomBetween(0, storageSize)]
        }
        while(offer1 == offer2);
      }
      if(targetValue >= 3)
      {
        do
        {
          offer3 = storageCards[rng.randomBetween(0, storageSize)]
        }
        while(offer1 == offer3 || offer2 == offer3);
      }
      if(targetValue >= 4)
      {
        do
        {
          offer4 = storageCards[rng.randomBetween(0, storageSize)]
        }
        while(offer1 == offer4 || offer2 == offer4 || offer3 == offer4);
      }
      if(targetValue == 5)
      {
        do
        {
          offer5 = storageCards[rng.randomBetween(0, storageSize)]
        }
        while(offer1 == offer5 || offer2 == offer5 || offer3 == offer5 || offer4 == offer5);
      }

      mainInventory.removeCard(offer1.getUID())
      storage.addCard(offer1)
      if(targetValue >= 2) {mainInventory.removeCard(offer2.getUID()); storage.addCard(offer2);}
      if(targetValue >= 3) {mainInventory.removeCard(offer3.getUID()); storage.addCard(offer3);}
      if(targetValue >= 4) {mainInventory.removeCard(offer4.getUID()); storage.addCard(offer4);}
      if(targetValue == 5) {mainInventory.removeCard(offer5.getUID()); storage.addCard(offer5);}
      storage.setViewable(true, true);

      menu.showInformation(qsTranslate("gardenspade", "Choose an item"), -1);
      var spadeChoice = storage.cardChoice(storage.getAllCards(), 0, -1);
      menu.hideInformation();

      storage.setViewable(false, false);
      var theRest = [];
      if(spadeChoice != offer1) {theRest.push(offer1);}
      if(targetValue >= 2 && spadeChoice != offer2) {theRest.push(offer2);}
      if(targetValue >= 3 && spadeChoice != offer3) {theRest.push(offer3);}
      if(targetValue >= 4 && spadeChoice != offer4) {theRest.push(offer4);}
      if(targetValue == 5 && spadeChoice != offer5) {theRest.push(offer5);}

      for(i = 0; i < theRest.length; i++)
      {
        storage.removeCard(theRest[i].getUID());
        trash.addCard(theRest[i]);

        //Log
        gameState.writeToGameLog(qsTranslate("gardenspade", "<font color=\"bisque\">The %1 sent the %2 from the storage to the trash.</font>").arg(card.getName()).arg(theRest[i].getName()), [1, 2, 3, 4], 1, 0);
      }

      storage.removeCard(spadeChoice.getUID());
      if(gameState.getCurrentPlayer() == 1) {p1Inventory.addCard(spadeChoice);}
      if(gameState.getCurrentPlayer() == 2) {p2Inventory.addCard(spadeChoice);}
      if(gameState.getCurrentPlayer() == 3) {p3Inventory.addCard(spadeChoice);}
      if(gameState.getCurrentPlayer() == 4) {p4Inventory.addCard(spadeChoice);}

      //Log
      gameState.writeToGameLog(qsTranslate("gardenspade", "<font color=\"bisque\">The %1 drew the %2 from the storage.</font>").arg(card.getName()).arg(spadeChoice.getName()), boardutils.informedPlayers(), 1, 0);
      gameState.writeToGameLog(qsTranslate("gardenspade", "<font color=\"bisque\">The %1 drew an item from the storage.</font>").arg(card.getName()), boardutils.unexpectingPlayers(), 1, 0);
    }

    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
