//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    var arrayNumber = 0;
    var dominoRuleFulfilled = false;
    for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
    {
      for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
      {
        if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
        {
          var adjacentToCup = 0;
          if(board.getCardAtPosition(Point(xVaria,yVaria-1)) != null)
          {
            adjacentToCup++;
            if((board.getCardAtPosition(Point(xVaria,yVaria-1))).getPlayer() == gameState.getCurrentPlayer())
            {
              dominoRuleFulfilled = true
            }
          }
          if(board.getCardAtPosition(Point(xVaria-1,yVaria)) != null)
          {
            adjacentToCup++;
            if((board.getCardAtPosition(Point(xVaria-1,yVaria))).getPlayer() == gameState.getCurrentPlayer())
            {
              dominoRuleFulfilled = true
            }
          }
          if(board.getCardAtPosition(Point(xVaria+1,yVaria)) != null)
          {
            adjacentToCup++;
            if((board.getCardAtPosition(Point(xVaria+1,yVaria))).getPlayer() == gameState.getCurrentPlayer())
            {
              dominoRuleFulfilled = true
            }
          }
          if(board.getCardAtPosition(Point(xVaria,yVaria+1)) != null)
          {
            adjacentToCup++;
            if((board.getCardAtPosition(Point(xVaria,yVaria+1))).getPlayer() == gameState.getCurrentPlayer())
            {
              dominoRuleFulfilled = true
            }
          }

          if(dominoRuleFulfilled == true)
          {
            dominoRuleFulfilled = false;
            if( ((gameState.getGameMode() == 0 || gameState.getGameMode() == 4) && mainInventory.getAllCards().length >= adjacentToCup) || 
                ((gameState.getGameMode() == 1 || gameState.getGameMode() == 2) && mainInventory.getAllCards().length >= adjacentToCup*2) ||
                ((gameState.getGameMode() == 3) && mainInventory.getAllCards().length >= adjacentToCup*3) )
            {
              array[arrayNumber] = new Point(xVaria,yVaria);
              arrayNumber++;
            }
          }
        }
      }
    }
  }
  return array;
}

function event(eventData)
{
  //Handle the cardPlacedOnBoard event
  //Check if this card is the one that was placed
  //If it is, then execute the bronze action

  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    cards = board.getAllCards(); //Retrieve all cards on the board
    cardsOK = []; //This will store the cards which give penalty
    var adjacentToCup = 0
    for(i = 0; i < cards.length; i++) //Check all cards
    {
      //If there is a card on an adjacent tile
      if(Math.pow(card.getPosX()-cards[i].getPosX(), 2) + Math.pow(card.getPosY()-cards[i].getPosY(), 2) == 1)
      {
        adjacentToCup++;
      }
    }

    //Setting the counter
    var cupCounter = 1;
    if(gameState.getGameMode() == 1 || gameState.getGameMode() == 2)
    {
      cupCounter = 2;
    }
    if(gameState.getGameMode() == 3)
    {
      cupCounter = 3;
    }
    card.setCounter(cupCounter);

    //Logging
    boardutils.logCardPlacement(card.getName());
    if(cupCounter > 1)
    {
      gameState.writeToGameLog(qsTranslate("goldencup", "<font color=\"bisque\">The %1 gained %2 counters.</font>").arg(card.getName()).arg(cupCounter), [1, 2, 3, 4], 1, 0);
    }
    else
    {
      gameState.writeToGameLog(qsTranslate("goldencup", "<font color=\"bisque\">The %1 gained 1 counter.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }

    var currentPlayer = gameState.getCurrentPlayer();
    var p1Draws = false;
    var p2Draws = false;
    var p3Draws = false;
    var p4Draws = false;

    if(currentPlayer != 1)
    {
      if(currentPlayer != 3 || gameState.getGameMode() != 1)
      {
        for(j = 0; j < adjacentToCup; j++)
        {
          gameState.drawCard(1);
        }
      }
    }

    if(currentPlayer != 2 && gameState.getGameMode() != 4)
    {
      if(currentPlayer != 4 || gameState.getGameMode() != 1)
      {
        for(j = 0; j < adjacentToCup; j++)
        {
          gameState.drawCard(2);
        }
      }
    }

    if(currentPlayer != 3 && gameState.getGameMode() != 0)
    {
      if(currentPlayer != 1 || gameState.getGameMode() != 1)
      {
        for(j = 0; j < adjacentToCup; j++)
        {
          gameState.drawCard(3);
        }
      }
    }

    if(currentPlayer != 4 && (gameState.getGameMode() == 1 || gameState.getGameMode() == 3))
    {
      if(currentPlayer != 2 || gameState.getGameMode() != 1)
      {
        for(j = 0; j < adjacentToCup; j++)
        {
          gameState.drawCard(4);
        }
      }
    }

    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.complexCurrentValue();
}
