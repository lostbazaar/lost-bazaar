//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    if(boardutils.getAdjacentEmptyTiles(card.getPosition()).length > 0)
    {
      cards = board.getAllCards(); //Retrieve all cards on the board
      tilesOK = []; //This will store the cards which can be moved
      for(i = 0; i < cards.length; i++) //Check all cards
      {
        //If the card is on a profit, safe, fashion, power tile (or it is the Trombone itself - required for the dismissing)
        if((board.isSafeTile(cards[i].getPosition()) == true || board.isDoubleTile(cards[i].getPosition()) == true || board.isFashionTile(cards[i].getPosition()) == true || board.isPowerTile(cards[i].getPosition()) == true) || cards[i] == card)
        {
          if(cards[i].getPlayer() == gameState.getCurrentPlayer())
          {
            //If the card is not immovable
            if(boardutils.checkForImmovable(cards[i]) == false)
            {
              //If the card is not untargetable
              if(boardutils.isUntargetableByBronze(cards[i]) == false)
              {
                tilesOK.push(cards[i].getPosition());
              }
            }
          }
        }
      }
      if(tilesOK.length > 1)
      {
        var cancelToTop;
        do
        {
          cancelToTop = 0;
          menu.showInformation(qsTranslate("trombone", "Choose target (click on the Trombone to dismiss it)"), -1);
          selectedTile = boardutils.tileChoice(tilesOK, 0);
          selectedCard = board.getCardAtPosition(selectedTile);
          menu.hideInformation();
          if(card.getPosX() == selectedTile.x && card.getPosY() == selectedTile.y)
          {
            //Log
            boardutils.logCardPlacement(card.getName());
          }
          else
          {
            menu.showInformation(qsTranslate("trombone", "Choose where would you like to move the selected card"), -1);
            selectedPos = boardutils.tileChoice(boardutils.getAdjacentEmptyTiles(card.getPosition()), 1);
            menu.hideInformation();
            if(selectedPos.x != -1)
            {
              board.moveCard(selectedTile, selectedPos);
              //Log
              boardutils.logCardPlacement(card.getName());
              gameState.writeToGameLog(qsTranslate("trombone", "<font color=\"bisque\">The %1 moved %2%3 %4 to %5%6.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg((board.getCardAtPosition(selectedPos)).getName()).arg(boardutils.convertToABC(selectedPos.x)).arg(selectedPos.y+1), [1, 2, 3, 4], 1, 0);
            }
            else
            {
              cancelToTop = 1;
            }
          }
        }
        while(cancelToTop == 1);
      }
      else
      {
        menu.showInformation(qsTranslate("trombone", "You cannot use this ability!"));
        //Logging
        boardutils.logCardPlacement(card.getName());
      }
    }
    else
    {
      menu.showInformation(qsTranslate("trombone", "There is no adjacent empty tile!"));
      //Logging
      boardutils.logCardPlacement(card.getName());
    }
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
