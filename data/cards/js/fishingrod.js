//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    eT = boardutils.getEmptyTiles();
    if(boardutils.isHalted(eT) == false && board.isDoubleTile(card.getPosition()) == false)
    {
      //Log
      boardutils.logCardPlacement(card.getName());
    }
    else
    {
      card.setGrayScale(1);

      //Log
      boardutils.logCardPlacement(card.getName());
      gameState.writeToGameLog(qsTranslate("fishingrod", "The %1 lost its abilities.").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
  }

  if(eventData[0] == "endOfTurn" && card.getPlayer() == gameState.getCurrentPlayer() && card.isGrayScale() == false)
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    if(gameState.didPlayerPlaceCardOnBoardThisTurn() == false && boardutils.checkForUnpickable(card) == false)
    {
      cardsOK = [];
      cardsOK2 = [];
      cardsOKValue = [];
      if(board.getCardAtPosition(Point(card.getPosX()+1,card.getPosY())) != null && board.getCardAtPosition(Point(card.getPosX()+1,card.getPosY())).getPlayer() != card.getPlayer())
      {
        cardsOK.push(board.getCardAtPosition(Point(card.getPosX()+1,card.getPosY())));
      }
      if(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()+1)) != null && board.getCardAtPosition(Point(card.getPosX(),card.getPosY()+1)).getPlayer() != card.getPlayer())
      {
        cardsOK.push(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()+1)));
      }
      if(board.getCardAtPosition(Point(card.getPosX()-1,card.getPosY())) != null && board.getCardAtPosition(Point(card.getPosX()-1,card.getPosY())).getPlayer() != card.getPlayer())
      {
        cardsOK.push(board.getCardAtPosition(Point(card.getPosX()-1,card.getPosY())));
      }
      if(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()-1)) != null && board.getCardAtPosition(Point(card.getPosX(),card.getPosY()-1)).getPlayer() != card.getPlayer())
      {
        cardsOK.push(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()-1)));
      }
      for(i = 0; i < cardsOK.length; i++)
      {
        cardsOKValue.push(cardsOK[i].getCurrentValue());
      }
      var lowestValue = Math.min.apply(Math, cardsOKValue);
      for(i = 0; i < cardsOK.length; i++)
      {
        if(cardsOKValue[i] == lowestValue)
        {
          cardsOK2.push(cardsOK[i]);
        }
      }
      var caughtByRod = cardsOK2[rng.randomBetween(0, cardsOK2.length)];
      var userNames = gameState.getPlayerNickNames();
      if(boardutils.checkForUnpickable(caughtByRod) == false)
      {
        //Log
        gameState.writeToGameLog(qsTranslate("fishingrod", "<font color=\"khaki\">%1%2 %3 returned to %4's hand.</khaki>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()).arg(userNames[card.getPlayer()-1]), [1, 2, 3, 4], 1, 0);
        gameState.writeToGameLog(qsTranslate("fishingrod", "<font color=\"khaki\">%1%2 %3 returned %4%5 %6 to %7's hand.</khaki>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()).arg(boardutils.convertToABC(caughtByRod.getPosX())).arg(caughtByRod.getPosY()+1).arg(caughtByRod.getName()).arg(userNames[card.getPlayer()-1]), [1, 2, 3, 4], 1, 0);

        board.removeCard(card.getUID());
        if(gameState.getCurrentPlayer() == 1) {p1Inventory.addCard(card);}
        if(gameState.getCurrentPlayer() == 2) {p2Inventory.addCard(card);}
        if(gameState.getCurrentPlayer() == 3) {p3Inventory.addCard(card);}
        if(gameState.getCurrentPlayer() == 4) {p4Inventory.addCard(card);}

        board.removeCard(caughtByRod.getUID());
        if(gameState.getCurrentPlayer() == 1) {p1Inventory.addCard(caughtByRod);}
        if(gameState.getCurrentPlayer() == 2) {p2Inventory.addCard(caughtByRod);}
        if(gameState.getCurrentPlayer() == 3) {p3Inventory.addCard(caughtByRod);}
        if(gameState.getCurrentPlayer() == 4) {p4Inventory.addCard(caughtByRod);}
      }
    }
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
