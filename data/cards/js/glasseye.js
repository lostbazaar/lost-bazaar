//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    //Check for the amount of cards in the hands of the opponents
    var sumCardsInHands = 0;
    switch(gameState.getGameMode())
    {
      case 0:
        sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length);
        break;
      case 1:
        sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length + p4Inventory.getAllCards().length);
        break;
      case 2:
        sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length);
        break;
      case 3:
        sumCardsInHands = (p1Inventory.getAllCards().length + p2Inventory.getAllCards().length + p3Inventory.getAllCards().length + p4Inventory.getAllCards().length);
        break;
      case 4:
        sumCardsInHands = (p1Inventory.getAllCards().length + p3Inventory.getAllCards().length);
        break;
    }
    switch(gameState.getCurrentPlayer())
    {
      case 1:
        sumCardsInHands = sumCardsInHands - p1Inventory.getAllCards().length;
        if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p4Inventory.getAllCards().length;}
        break;
      case 2:
        sumCardsInHands = sumCardsInHands - p2Inventory.getAllCards().length;
        if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p3Inventory.getAllCards().length;}
        break;
      case 3:
        sumCardsInHands = sumCardsInHands - p3Inventory.getAllCards().length;
        if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p2Inventory.getAllCards().length;}
        break;
      case 4:
        sumCardsInHands = sumCardsInHands - p4Inventory.getAllCards().length;
        if(gameState.getGameMode() == 1) {sumCardsInHands = sumCardsInHands - p1Inventory.getAllCards().length;}
        break;
    }

    var eT = boardutils.getEmptyTiles();
    //If there is at least one card in the hand of one opponent, and an empty tile
    if(sumCardsInHands > 0 && eT.length > 0)
    {
      if(boardutils.limitedByPerfume() == true)
      {
        eT = boardutils.getTilesAbovePerfume();
      }

      for(i = 0; i < eT.length; i++)
      {
        var xVaria = eT[i].x;
        var yVaria = eT[i].y;
        array.push(eT[i]);
      }
    }
  }
  return array;
}

//This function handles all events
function event(eventData)
{
  //Handle the cardPlacedOnBoard event
  //Check if this card is the one that was placed
  //If it is, then execute the bronze action
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    var gazedItems = []; //This will store the tiles of the adjacent cards
    for(ctc = 2; ctc < 10; ctc = ctc+2)
    {
      var xCorri = 0;
      var yCorri = 0;
      if(ctc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri = 1;}
      if(ctc == 4 && card.getPosX() > 0) {xCorri = -1;}
      if(ctc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri = 1;}
      if(ctc == 8 && card.getPosY() > 0) {yCorri = -1;}
      TTC = Point(card.getPosX()+xCorri,card.getPosY()+yCorri);
      CTC = board.getCardAtPosition(TTC);
      if(CTC != null && CTC.getUID() != card.getUID() && boardutils.checkForUntrashable(CTC) == false)
      {
        gazedItems.push(CTC);
        //gameState.writeToGameLog(qsTranslate("glasseye", "<font color=\"bisque\">%1</font>").arg(CTC.getName()), [1, 2, 3, 4], 1, 0);
      }
    }
    
    var handRevealed = 0;
    var choice = 0;
    if(gameState.getGameMode() > 0 && gameState.getGameMode() < 4)
    {
      viewablePlayers = [] //This will store the possible players
      if(gameState.getCurrentPlayer() == 2 || gameState.getCurrentPlayer() == 3 || (gameState.getCurrentPlayer() == 4 && gameState.getGameMode() == 3))
      {
        if(!p1Inventory.isEmpty())
        {
          viewablePlayers.push(1);
        }
      }
      if(gameState.getCurrentPlayer() == 1 || (gameState.getCurrentPlayer() == 3 && (gameState.getGameMode() == 2 || gameState.getGameMode() == 3)) || gameState.getCurrentPlayer() == 4)
      {
        if(!p2Inventory.isEmpty())
        {
          viewablePlayers.push(2);
        }
      }
      if(gameState.getCurrentPlayer() == 1 || (gameState.getCurrentPlayer() == 2 && gameState.getGameMode() > 1) || gameState.getCurrentPlayer() == 4)
      {
        if(!p3Inventory.isEmpty())
        {
          viewablePlayers.push(3);
        }
      }
      if((gameState.getCurrentPlayer() == 1 && gameState.getGameMode() == 3) || (gameState.getCurrentPlayer() == 2 && gameState.getGameMode() != 2) || (gameState.getCurrentPlayer() == 3 && gameState.getGameMode() != 2 ))
      {
        if(!p4Inventory.isEmpty())
        {
          viewablePlayers.push(4);
        }
      }
      menu.showInformation(qsTranslate("glasseye", "Which player's hand would you like to view?"), -1);
      //Get the player's answer
      choice = graphicsViewMenu.getPlayer(viewablePlayers, 0);
      //Now we have the choice, it's time to make the chosen player's inventory visible
      switch(choice)
      {
        case 1:
          p1Inventory.setViewable(true, true);
          handRevealed = p1Inventory.getAllCards();
          break;
        case 2:
          p2Inventory.setViewable(true, true);
          handRevealed = p2Inventory.getAllCards();
          break;
        case 3:
          p3Inventory.setViewable(true, true);
          handRevealed = p3Inventory.getAllCards();
          break;
        case 4:
          p4Inventory.setViewable(true, true);
          handRevealed = p4Inventory.getAllCards();
          break;
      }
      //Hide the 'Which player's inventory...' label
      menu.hideInformation();
    }
    else
    {
      //1v1 opposing
      if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 1)
      {
        p2Inventory.setViewable(true, true);
        handRevealed = p2Inventory.getAllCards();
        choice = 2;
      }
      if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 2)
      {
        p1Inventory.setViewable(true, true);
        handRevealed = p1Inventory.getAllCards();
        choice = 1;
      }
      //1v1 adjacent
      if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 1)
      {
        p3Inventory.setViewable(true, true);
        handRevealed = p3Inventory.getAllCards();
        choice = 3;
      }
      if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 3)
      {
        p1Inventory.setViewable(true, true);
        handRevealed = p1Inventory.getAllCards();
        choice = 1;
      }
    }
    var namesRevealed = [];
    for(HR = 0; HR < handRevealed.length; HR++)
    {
      namesRevealed.push(handRevealed[HR].getName());
    }

    //Log
    boardutils.logCardPlacement(card.getName());
    var userNames = gameState.getPlayerNickNames();
    gameState.writeToGameLog(qsTranslate("glasseye", "<font color=\"bisque\">The %1 revealed %2's hand.</font>").arg(card.getName()).arg(userNames[choice-1]), [1, 2, 3, 4], 1, 0);
    
    if(gazedItems.length > 0)
    {
      var itemsDestroyedByEye = [];
      var explodingItems = [];
      var pigDrawing1 = 0;
      var pigDrawing2 = 0;
      var pigDrawing3 = 0;
      var pigDrawing4 = 0;
      for(GI = 0; GI < gazedItems.length; GI++)
      {
        if(namesRevealed.indexOf(gazedItems[GI].getName()) > -1)
        {
          itemsDestroyedByEye.push(gazedItems[GI].getUID());
        }
      }
      for(GI = 0; GI < gazedItems.length; GI++)
      {
        if(namesRevealed.indexOf(gazedItems[GI].getName()) > -1)
        {
          var hitByDamage = gazedItems[GI];
          damageOutcome = boardutils.damageEvents(card.getID(), hitByDamage, hitByDamage.getPosX(), hitByDamage.getPosY())
          //Push results into a single array and check for duplicates
          for(DOC = 0; DOC < damageOutcome[0].length; DOC++)
          {
            if(explodingItems.indexOf(damageOutcome[0][DOC]) == -1)
            {
              if(itemsDestroyedByEye.indexOf(damageOutcome[0][DOC]) == -1)
              { 
                explodingItems.push(damageOutcome[0][DOC]);
              }
            }
          }
          pigDrawing1 = pigDrawing1 + damageOutcome[1];
          pigDrawing2 = pigDrawing2 + damageOutcome[2];
          pigDrawing3 = pigDrawing3 + damageOutcome[3];
          pigDrawing4 = pigDrawing4 + damageOutcome[4];

          //Log
          gameState.writeToGameLog(qsTranslate("glasseye", "<font color=\"bisque\">The %1 destroyed %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(hitByDamage.getPosX())).arg(hitByDamage.getPosY()+1).arg(hitByDamage.getName()), [1, 2, 3, 4], 1, 0);

          board.moveCardToTrash(hitByDamage.getUID());
        }
      }

      if(explodingItems.length > 0)
      {
        //Bomb events
        for(boomOrder = 0; boomOrder < explodingItems.length; boomOrder++)
        {
          var BBV = gameState.cardForUID(explodingItems[boomOrder]);

          //Log
          gameState.writeToGameLog(qsTranslate("bomb", "<font color=\"khaki\">The Bomb destroyed %1%2 %3.</font>").arg(boardutils.convertToABC(BBV.getPosX())).arg(BBV.getPosY()+1).arg(BBV.getName()), [1, 2, 3, 4], 1, 0);

          board.moveCardToTrash(explodingItems[boomOrder]);
        }
      }
      //Piggy Bank events
      if(pigDrawing1 > 0) {for(drawCount = 0; drawCount < damageOutcome[1]; drawCount++) {gameState.drawCard(1, 0, 0, 0, 0, "khaki");}}
      if(pigDrawing2 > 0) {for(drawCount = 0; drawCount < damageOutcome[2]; drawCount++) {gameState.drawCard(2, 0, 0, 0, 0, "khaki");}}
      if(pigDrawing3 > 0) {for(drawCount = 0; drawCount < damageOutcome[3]; drawCount++) {gameState.drawCard(3, 0, 0, 0, 0, "khaki");}}
      if(pigDrawing4 > 0) {for(drawCount = 0; drawCount < damageOutcome[4]; drawCount++) {gameState.drawCard(4, 0, 0, 0, 0, "khaki");}}
    }

    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
