//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums
//@include jsutils.point

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card

function getAvailableTiles()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    var eT = boardutils.getEmptyTiles();
    if(boardutils.limitedByPerfume() == true)
    {
      eT = boardutils.getTilesAbovePerfume();
    }

    for(i = 0; i < eT.length; i++)
    {
      var entryCondition = false;
      var instantlyTrashable = true;
      var instantlyMovable = true;
      var xVaria = eT[i].x;
      var yVaria = eT[i].y;

      cardsCF = board.getAllCards();
      for(cfut = 0; cfut < cardsCF.length; cfut++)
      {
        if(board.isSafeTile(eT[i]) == true)
        {
          instantlyTrashable = false;
          break;
        }

        if((cardsCF[cfut].getID() == "celticshield" || (cardsCF[cfut].getID() == "handmirror" && boardutils.itemMirrored(cardsCF[cfut]) == "celticshield")) && cardsCF[cfut].isGrayScale() == false && cardsCF[cfut].getPlayer() == gameState.getCurrentPlayer())
        {
          var shieldDistance = Math.pow(cardsCF[cfut].getPosX()-xVaria, 2) + Math.pow(cardsCF[cfut].getPosY()-yVaria, 2)
          if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
          {
            instantlyTrashable = false;
            break;
          }
        }

        if((cardsCF[cfut].getID() == "hardhat" || (cardsCF[cfut].getID() == "handmirror" && boardutils.itemMirrored(cardsCF[cfut]) == "hardhat")) && cardsCF[cfut].isGrayScale() == false)
        {
          switch(cardsCF[cfut].getPlayer())
          {
            case 1:
              if(cardsCF[cfut].getPosX() == xVaria && cardsCF[cfut].getPosY() == yVaria-1) {instantlyTrashable = false;}
              break;
            case 2:
              if(cardsCF[cfut].getPosX() == xVaria && cardsCF[cfut].getPosY() == yVaria+1) {instantlyTrashable = false;}
              break;
            case 3:
              if(cardsCF[cfut].getPosX() == xVaria+1 && cardsCF[cfut].getPosY() == yVaria) {instantlyTrashable = false;}
              break;
            case 4:
              if(cardsCF[cfut].getPosX() == xVaria-1 && cardsCF[cfut].getPosY() == yVaria) {instantlyTrashable = false;}
              break;
          }
        }
      }
      if(board.isInnovativeTile(Point(xVaria,yVaria)) == false)
      {
        for(cfim = 0; cfim < cardsCF.length; cfim++)
        {
          if((cardsCF[cfim].getID() == "gluetube" || (cardsCF[cfim].getID() == "handmirror" && boardutils.itemMirrored(cardsCF[cfim]) == "gluetube")) && cardsCF[cfim].isGrayScale() == false)
          {
            var tubeDistance = Math.pow(cardsCF[cfim].getPosX()-xVaria, 2) + Math.pow(cardsCF[cfim].getPosY()-yVaria, 2)
            if(tubeDistance == 0 || tubeDistance == 1 || tubeDistance == 2)
            {
              instantlyMovable = false;
              break;
            }
          }
        }
      }
      else
      {
        instantlyMovable = false;
      }

      if(instantlyTrashable == true)
      {
        for(xVariab = 0; xVariab < board.getWidth(); xVariab++)
        {
          //If the target tile is not safe
          var CTC = board.getCardAtPosition(Point(xVariab,yVaria));
          if(CTC != null && boardutils.checkForUntrashable(CTC) == false && instantlyMovable == true)
          {
            entryCondition = true;
            break;
          }
          if(CTC != null && boardutils.checkForUntrashable(CTC) == true && CTC.getCurrentValue() > 3)
          {
            entryCondition = true;
            break;
          }
          if(CTC != null && instantlyMovable == false && CTC.getCurrentValue() > 3)
          {
            entryCondition = true;
            break;
          }
        }

        if(entryCondition == false)
        {
          for(yVariab = 0; yVariab < board.getHeight(); yVariab++)
          {
            var CTC = board.getCardAtPosition(Point(xVaria,yVariab));
            if(CTC != null && boardutils.checkForUntrashable(CTC) == false && instantlyMovable == true)
            {
              entryCondition = true;
              break;
            }
            if(CTC != null && boardutils.checkForUntrashable(CTC) == true && CTC.getCurrentValue() > 3)
            {
              entryCondition = true;
              break;
            }
            if(CTC != null && instantlyMovable == false && CTC.getCurrentValue() > 3)
            {
              entryCondition = true;
              break;
            }
          }
        }
      }

      if(instantlyTrashable == false && instantlyMovable == true)
      {
        //If the tile is safe, the only allowed targets are the ones with 100% likeliness to be hit
        for(xVariab = 0; xVariab < board.getWidth(); xVariab++)
        {
          var CTC = board.getCardAtPosition(Point(xVariab,yVaria));
          if(CTC != null && boardutils.checkForUntrashable(CTC) == false && ((CTC.getCurrentValue() <= 0 && boardutils.isSixFix(gameState.getCurrentPlayer()) == false) || (CTC.getCurrentValue() <= 3 && boardutils.isSixFix(gameState.getCurrentPlayer()) == true)))
          {
            entryCondition = true;
            break;
          }

          if(entryCondition == false)
          {
            for(yVariab = 0; yVariab < board.getHeight(); yVariab++)
            {
              var CTC = board.getCardAtPosition(Point(xVaria,yVariab));
              if(CTC != null && boardutils.checkForUntrashable(CTC) == false && ((CTC.getCurrentValue() <= 0 && boardutils.isSixFix(gameState.getCurrentPlayer()) == false) || (CTC.getCurrentValue() <= 3 && boardutils.isSixFix(gameState.getCurrentPlayer()) == true)))
              {
                entryCondition = true;
                break;
              }
            }
          }
        }
      }

      if(entryCondition == true)
      {
        array.push(eT[i]);
      }
    }
  }
  return array;
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    cards = board.getAllCards(); //Retrieve all cards on the board
    tilesOK = []; //This will store the tiles which can be targeted
    for(i = 0; i < cards.length; i++) //Check all cards
    {
      if((cards[i].getPosX() == card.getPosX() && cards[i].getPosY() != card.getPosY()) || (cards[i].getPosX() != card.getPosX() && cards[i].getPosY() == card.getPosY()))
      {
        if(boardutils.isSixFix(gameState.getCurrentPlayer()) == true)
        {
          if((cards[i].getCurrentValue() > 3 && boardutils.checkForUntrashable(card) == false) || (cards[i].getCurrentValue() <= 3 && boardutils.checkForUntrashable(cards[i]) == false && boardutils.checkForImmovable(card) == false))
          {
            tilesOK.push(cards[i].getPosition());
          }
        }
        else
        {
          if((boardutils.checkForUntrashable(cards[i]) == false && boardutils.checkForUntrashable(card) == false && boardutils.checkForImmovable(card) == false) || (cards[i].getCurrentValue() > 3 && boardutils.checkForUntrashable(card) == false) || (cards[i].getCurrentValue() <= 0 && boardutils.checkForUntrashable(cards[i]) == false && boardutils.checkForImmovable(card) == false))
          {
            tilesOK.push(cards[i].getPosition());
          }
        }
      }
    }
    if(tilesOK.length > 0)
    {
      //Log
      boardutils.logCardPlacement(card.getName());

      //Ask the player which card would he like to target
      menu.showInformation(qsTranslate("dart", "Choose target"), -1);
      //The player chooses one from the good cards
      selectedTile = boardutils.tileChoice(tilesOK, 0);
      //Hide the 'Choose which card...' label
      menu.hideInformation();
      if(boardutils.isSixFix(gameState.getCurrentPlayer()) == true)
      {
        var diceRoll = 6;
      }
      else
      {
        var diceRoll = rng.randomBetween(1, 7);
      }
      menu.showInformation(qsTranslate("dart", "%1%2 %3: %4").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()).arg(boardutils.numberToFace(diceRoll)), 4000);

      //Log
      var userNames = gameState.getPlayerNickNames();
      gameState.writeToGameLog(qsTranslate("dart", "<font color=\"bisque\">%1 rolled a die: %2.").arg(userNames[gameState.getCurrentPlayer()-1]).arg(diceRoll), [1, 2, 3, 4], 1, 0);

      if(board.getCardAtPosition(selectedTile).getCurrentValue()*2 <= diceRoll)
      {
        var hitByDamage = board.getCardAtPosition(selectedTile);
        damageOutcome = boardutils.damageEvents(card.getID(), hitByDamage, hitByDamage.getPosX(), hitByDamage.getPosY())
        var explodingItems = damageOutcome[0];

        //Log
        gameState.writeToGameLog(qsTranslate("dart", "<font color=\"bisque\">The %1 destroyed %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(hitByDamage.getPosX())).arg(hitByDamage.getPosY()+1).arg(hitByDamage.getName()), [1, 2, 3, 4], 1, 0);

        board.moveCardToTrash(hitByDamage.getUID());

        if(explodingItems.length > 1)
        {
          //Bomb events
          for(boomOrder = 1; boomOrder < explodingItems.length; boomOrder++)
          {
            var BBV = gameState.cardForUID(damageOutcome[0][boomOrder]);

            //Log
            gameState.writeToGameLog(qsTranslate("bomb", "<font color=\"khaki\">The %1 destroyed %2%3 %4.</font>").arg(hitByDamage.getName()).arg(boardutils.convertToABC(BBV.getPosX())).arg(BBV.getPosY()+1).arg(BBV.getName()), [1, 2, 3, 4], 1, 0);

            board.moveCardToTrash(damageOutcome[0][boomOrder]);
          }
        }
        //Piggy Bank events
        if(damageOutcome[1] > 0) {for(drawCount = 0; drawCount < damageOutcome[1]; drawCount++) {gameState.drawCard(1, 0, 0, 0, 0, "khaki");}}
        if(damageOutcome[2] > 0) {for(drawCount = 0; drawCount < damageOutcome[2]; drawCount++) {gameState.drawCard(2, 0, 0, 0, 0, "khaki");}}
        if(damageOutcome[3] > 0) {for(drawCount = 0; drawCount < damageOutcome[3]; drawCount++) {gameState.drawCard(3, 0, 0, 0, 0, "khaki");}}
        if(damageOutcome[4] > 0) {for(drawCount = 0; drawCount < damageOutcome[4]; drawCount++) {gameState.drawCard(4, 0, 0, 0, 0, "khaki");}}

        board.moveCard(card.getPosition(), selectedTile);

        //Log
        gameState.writeToGameLog(qsTranslate("dart", "<font color=\"bisque\">The %1 has been moved to %2%3.</font>").arg(card.getName()).arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1), [1, 2, 3, 4], 1, 0);
      }
      else
      {
        board.moveCardToTrash(card.getUID());

        //Log
        gameState.writeToGameLog(qsTranslate("dart", "<font color=\"bisque\">The %1 has been destroyed.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
      }

    }
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
