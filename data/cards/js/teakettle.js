//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums
//@include jsutils.point

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "turnStarted")
  {
    card.setShowFullSize(1);
  }

  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    eT = boardutils.getEmptyTiles();
    if(boardutils.isHalted(eT) == false && board.isDoubleTile(card.getPosition()) == false)
    {
      card.setCounter(1);
      //Logging
      boardutils.logCardPlacement(card.getName());
      gameState.writeToGameLog(qsTranslate("teakettle", "<font color=\"bisque\">The %1 gained 1 counter.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
    else
    {
      card.setGrayScale(1);
      //Logging
      boardutils.logCardPlacement(card.getName());
      gameState.writeToGameLog(qsTranslate("teakettle", "The %1 has been disabled.").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    cards = board.getAllCards(); //Retrieve all cards on the board
    tilesOK = []; //This will store the cards which can be moved
    for(i = 0; i < cards.length; i++) //Check all cards
    {
      //If the card is adjacent to the Tea Kettle
      if(Math.pow(card.getPosX()-cards[i].getPosX(), 2) + Math.pow(card.getPosY()-cards[i].getPosY(), 2) == 1)
      {
        //If the card is targetable by active abilities
        if(boardutils.isUntargetableBySilver(cards[i]) == false)
        {
          //If the item is resting or grayed
          if(cards[i].isGrayScale() == true || cards[i].isFullSize() == false)
          {
            //Store the coordinates of the good card
            tilesOK.push(cards[i].getPosition());
          }
        }
      }
    }

    if(card.getCounter() > 0)
    {
      //Ask the player to choose the ability
      menu.showInformation(qsTranslate("teakettle", "Choose from the abilities of the %1").arg(card.getName()), -1);
      //Stores the text of the choices
      choiceArray = [
      qsTranslate("teakettle", "Double the counters on itself"),
      qsTranslate("teakettle", "Restore and refresh an adjacent item"),
      qsTranslate("teakettle", "Draw an item")
      ];
      //Stores which choices are enabled and which aren't
      enableArray = ["true", (card.getCounter() >= 3) ? "true" : "false", (mainInventory.isEmpty() || card.getCounter() < 7) ? "false" :"true"];
      //Stores the tooltips for the choices
      toolTipArray = ["", "", ""];
      //Get the player's choice
      var choice = menu.getChoice(choiceArray, enableArray, toolTipArray);
      //Hide the label
      menu.hideInformation();

      //If the player chose to double the counters
      if(choice == 0)
      {
        var prevCounter = card.getCounter();
        card.setCounter(card.getCounter()*2);
        var addedCounter = card.getCounter() - prevCounter;
        if(card.getCounter() > 11)
        {
          card.setCounter(12);
        }
        //Log
        boardutils.logSilverActivation(card.getName());
        if(addedCounter > 1)
        {
          gameState.writeToGameLog(qsTranslate("teakettle", "<font color=\"paleturquoise\">The %1 gained %2 counters.</font>").arg(card.getName()).arg(addedCounter), [1, 2, 3, 4], 1, 0);
        }
        else
        {
          gameState.writeToGameLog(qsTranslate("teakettle", "<font color=\"paleturquoise\">The %1 gained 1 counter.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
        }
      }
      //If the player chose to restore an item
      else if(choice == 1)
      {
        //Counters taken down
        card.setCounter(card.getCounter()-3);

        //Log
        boardutils.logSilverActivation(card.getName());
        gameState.writeToGameLog(qsTranslate("teakettle", "<font color=\"paleturquoise\">The %1 cast 3 counters.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);

        if(tilesOK.length > 0)
        {
          //Ask the player which card would he like to move
          menu.showInformation(qsTranslate("teakettle", "Choose target"), -1);
          //The player chooses one from the good cards
          selectedTile = boardutils.tileChoice(tilesOK, 0);
          selectedCard = board.getCardAtPosition(selectedTile);
          //Hide the label
          menu.hideInformation();
          var wasGray = false;
          var wasShrunken = false;
          if(selectedCard.isGrayScale() == true) {wasGray = true; selectedCard.setGrayScale(0);}
          if(selectedCard.isFullSize() == false) {wasShrunken = true; selectedCard.setShowFullSize(1);}
          if(wasGray == true)
          {
            if(wasShrunken == true)
            {
              gameState.writeToGameLog(qsTranslate("teakettle", "<font color=\"paleturquoise\">The %1 restored and refreshed %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);
            }
            else
            {
              gameState.writeToGameLog(qsTranslate("teakettle", "<font color=\"paleturquoise\">The %1 restored %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);
            }
          }
          else
          {
            if(wasShrunken == true)
            {
              gameState.writeToGameLog(qsTranslate("teakettle", "<font color=\"paleturquoise\">The %1 refreshed %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);
            }
            else
            {
              gameState.writeToGameLog(qsTranslate("teakettle", "<font color=\"paleturquoise\">The %1 did nothing to %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);
            }
          }
        }
        else
        {
          menu.showInformation(qsTranslate("teakettle", "No available targets!"));
        }
      }
      else if(choice == 2)
      {
        //Log
        boardutils.logSilverActivation(card.getName());
        gameState.writeToGameLog(qsTranslate("teakettle", "<font color=\"paleturquoise\">The %1 cast 7 counters.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);

        //Counters taken down
        card.setCounter(card.getCounter()-7);
        //A card is drawn for the player
        gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "paleturquoise");
        menu.hideInformation();
        //An informational message is displayed (it will disappear automatically)
        menu.showInformation(qsTranslate("teakettle", "You have drawn an item!"));
      }
      //Crop
      card.setShowFullSize(0);
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
