//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    var arrayNumber = 0;

    for(xVaria = 0; xVaria < board.getWidth(); xVaria++)
    {
      for(yVaria = 0; yVaria < board.getHeight(); yVaria++)
      {
        if(board.getCardAtPosition(Point(xVaria,yVaria)) == null && board.isDisabledTile(Point(xVaria,yVaria)) == false)
        {
          if(board.isSafeTile(Point(xVaria,yVaria)) == false)
          {
            cardsCFUR = board.getAllCards();
            var instantlyRotatable = true;
            for(cfur = 0; cfur < cardsCFUR.length; cfur++)
            {

              if((cardsCFUR[cfur].getID() == "celticshield" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "celticshield")) && cardsCFUR[cfur].isGrayScale() == false && cardsCFUR[cfur].getPlayer() == card.getPlayer())
              {
                var shieldDistance = Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2)
                if(shieldDistance == 0 || shieldDistance == 1 || shieldDistance == 2)
                {
                  instantlyRotatable = false;
                  break;
                }
              }

              if(card.getID() != "padlock" || (card.getID() == "padlock" && card.isGrayScale() == true))
              {
                if(cardsCFUR[cfur].getID() == "padlock" && cardsCFUR[cfur].isGrayScale() == false && Math.pow(cardsCFUR[cfur].getPosX()-xVaria, 2) + Math.pow(cardsCFUR[cfur].getPosY()-yVaria, 2) == 1)
                {
                  instantlyRotatable = false;
                  break;
                }
              }
              else
              {
                instantlyRotatable = false;
                break;
              }

              if((cardsCFUR[cfur].getID() == "hardhat" || (cardsCFUR[cfur].getID() == "handmirror" && boardutils.itemMirrored(cardsCFUR[cfur]) == "hardhat")) && cardsCFUR[cfur].isGrayScale() == false)
              {
                switch(cardsCFUR[cfur].getPlayer())
                {
                  case 1:
                    if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria-1) {instantlyRotatable = false;}
                    break;
                  case 2:
                    if(cardsCFUR[cfur].getPosX() == xVaria && cardsCFUR[cfur].getPosY() == yVaria+1) {instantlyRotatable = false;}
                    break;
                  case 3:
                    if(cardsCFUR[cfur].getPosX() == xVaria+1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;}
                    break;
                  case 4:
                    if(cardsCFUR[cfur].getPosX() == xVaria-1 && cardsCFUR[cfur].getPosY() == yVaria) {instantlyRotatable = false;}
                    break;
                }
              }
            }

            if(instantlyRotatable == true)
            {
              array[arrayNumber] = new Point(xVaria,yVaria);
              arrayNumber++;
            }
          }
        }
      }
    }
    return array;
  }
  else
  {
    return array;
  }
}


function event(eventData)
{
  //Handle the cardPlacedOnBoard event
  //Check if this card is the one that was placed
  //If it is, then execute the bronze action
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    var choice = 0;
    //Log
    boardutils.logCardPlacement(card.getName());

    if(gameState.getGameMode() > 0 && gameState.getGameMode() < 4)
    {
      targetablePlayers = [] //This will store the possible players for the first choice
      if(gameState.getCurrentPlayer() == 2 || gameState.getCurrentPlayer() == 3 || (gameState.getCurrentPlayer() == 4 && gameState.getGameMode() == 3))
      {
        targetablePlayers.push(1);
      }
      if(gameState.getCurrentPlayer() == 1 || (gameState.getCurrentPlayer() == 3 && (gameState.getGameMode() == 2 || gameState.getGameMode() == 3)) || gameState.getCurrentPlayer() == 4)
      {
        targetablePlayers.push(2);
      }
      if(gameState.getCurrentPlayer() == 1 || (gameState.getCurrentPlayer() == 2 && gameState.getGameMode() > 1) || gameState.getCurrentPlayer() == 4)
      {
        targetablePlayers.push(3);
      }
      if((gameState.getCurrentPlayer() == 1 && gameState.getGameMode() == 3) || (gameState.getCurrentPlayer() == 2 && gameState.getGameMode() != 2) || (gameState.getCurrentPlayer() == 3 && gameState.getGameMode() != 2 ))
      {
        targetablePlayers.push(4);
      }
      menu.showInformation(qsTranslate("giftbox", "Toward who would you like to rotate the %1?").arg(card.getName(), -1));
      choice = graphicsViewMenu.getPlayer(targetablePlayers, 0);
      menu.hideInformation();
    }
    else
    {
      //1v1 opposing
      if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 1)
      {
        choice = 2;
      }
      if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 2)
      {
        choice = 1;
      }
      //1v1 adjacent
      if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 1)
      {
        choice = 3;
      }
      if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 3)
      {
        choice = 1;
      }
    }

    card.changeOwner(choice);

    //Log
    var userNames = gameState.getPlayerNickNames();
    gameState.writeToGameLog(qsTranslate("giftbox", "<font color=\"bisque\">The %1 has been rotated towards %2.</font>").arg(card.getName()).arg(userNames[choice-1]), [1, 2, 3, 4], 1, 0);

    for(j = 0; j < 2; j++)
    {
      cards = board.getAllCards(); //Retrieve all cards on the board
      tilesOK = []; //This will store the position of the cards
      cardsOK = []; //This will store the cards themselves
      //Add the Gift Box itself to the list for the option to dismiss ability
      tilesOK.push(card.getPosition());
      cardsOK.push(card);

      for(i = 0; i < cards.length; i++) //Check all cards
      {
        //If the card is on an adjacent tile and not greyscale already then it is good
        if(Math.pow(card.getPosX()-cards[i].getPosX(), 2) + Math.pow(card.getPosY()-cards[i].getPosY(), 2) <= 1)
        {
          if(boardutils.checkForUnpickable(cards[i]) == false)
          {
            if(boardutils.isUntargetableByBronze(cards[i]) == false)
            {
              if(cards[i].getBaseValue() <= 3)
              {
                //If the card has an owner
                if( (cards[i].getPlayer() == 1) ||
                    (cards[i].getPlayer() == 2 && gameState.getGameMode() != 4) ||
                    (cards[i].getPlayer() == 3 && gameState.getGameMode() != 0) ||
                    (cards[i].getPlayer() == 4 && (gameState.getGameMode() == 1 || gameState.getGameMode() == 3)) )
                {
                  //Store the good card
                  tilesOK.push(cards[i].getPosition());
                  cardsOK.push(cards[i]);
                }
              }
            }
          }
        }
      }
      if(tilesOK.length > 1)
      {
        //Ask the player which card to return
        menu.showInformation(qsTranslate("giftbox", "Choose target (click on the %1 to dismiss it)").arg(card.getName()), -1);
        //The player chooses one from the good cards
        selectedTile = boardutils.tileChoice(tilesOK, 0);
        //Hide the 'Choose target' label
        menu.hideInformation();
        selectedCard = board.getCardAtPosition(selectedTile);
        if(mainInventory.getAllCards().length > 0)
        {
          if(selectedCard == card)
          {
            //Cause to exit cycle
            j = 2;
          }
          else
          {
            //Log
            gameState.writeToGameLog(qsTranslate("giftbox", "<font color=\"bisque\">The %1 returned %2%3 %4 to %5's hand.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedTile.x)).arg(selectedTile.y+1).arg(selectedCard.getName()).arg(userNames[selectedCard.getPlayer()-1]), [1, 2, 3, 4], 1, 0);

            board.removeCard(selectedCard.getUID());
            if(selectedCard.getPlayer() == gameState.getCurrentPlayer())
            {
              menu.showInformation(qsTranslate("giftbox", "You have drawn an item!"));
            }
            if(selectedCard.getPlayer() == 1)
            {
              p1Inventory.addCard(selectedCard);
              gameState.drawCard(1, 0, 0, 0, 0, "bisque");
            }
            if(selectedCard.getPlayer() == 2)
            {
              p2Inventory.addCard(selectedCard);
              gameState.drawCard(2, 0, 0, 0, 0, "bisque");
            }
            if(selectedCard.getPlayer() == 3)
            {
              p3Inventory.addCard(selectedCard);
              gameState.drawCard(3, 0, 0, 0, 0, "bisque");
            }
            if(selectedCard.getPlayer() == 4)
            {
              p4Inventory.addCard(selectedCard);
              gameState.drawCard(4, 0, 0, 0, 0, "bisque");
            }
          }
        }
        else
        {
          menu.showInformation(qsTranslate("giftbox", "No item in the storage!"));
        }
      }
      else
      {
        menu.showInformation(qsTranslate("giftbox", "No item is selectable for ability!"));
      }
      //Exit loop if there are no choices to make
      if(cardsOK.length == 1)
      {
        j = 2;
      }
    }
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    //You cannot trash it if it is on safe tile
    if(boardutils.checkForUntrashable(card) == false)
    {
      //Log
      var userNames = gameState.getPlayerNickNames();
      gameState.writeToGameLog(qsTranslate("giftbox", "<font color=\"paleturquoise\">The %1 has been destroyed.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);

      gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "paleturquoise");
      gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "paleturquoise");
      board.moveCardToTrash(card.getUID());
      menu.showInformation(qsTranslate("giftbox", "You have drawn 2 items!"));
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
