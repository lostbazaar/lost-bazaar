//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  var array = new Array();
  if(boardutils.haltedByNet(card.getBaseValue()) == false)
  {
    if(boardutils.limitedByPerfume() == false)
    {
      eT = boardutils.getEmptyTiles();
    }
    else
    {
      eT = boardutils.getTilesAbovePerfume();
    }
    ePT = [];
    if(mainInventory.getAllCards().length > 1)
    {
      array = eT;
    }
    else
    {
      if(boardutils.isHalted(eT) == true)
      {
        for(i = 0; i < eT.length; i++)
        {
          if(board.isDoubleTile(eT[i]))
          {
            ePT.push(eT[i]);
          }
        }
        array = ePT;
      }
    }
  }
  return array;
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());

    //If the card is not placed on a special tile it draws two cards
    if(!board.isDoubleTile(card.getPosition()))
    {
      if(!board.isSafeTile(card.getPosition()))
      {
        if(!board.isInnovativeTile(card.getPosition()))
        {
          if(!board.isFashionTile(card.getPosition()))
          {
            if(!board.isPowerTile(card.getPosition()))
            {
              gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "bisque");
              gameState.drawCard(card.getPlayer(), 0, 0, 0, 0, "bisque");
              menu.showInformation(qsTranslate("football", "You have drawn 2 items!"));
            }
          }
        }
      }
    }
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.complexCurrentValue();
}
