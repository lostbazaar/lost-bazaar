//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums
//@include jsutils.watch

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

//This function handles all events
function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1] && boardutils.checkForUnrotatable(card) == false)
  {
    var prevOwner = card.getPlayer()
    switch(gameState.getGameMode())
    {
      case 0:
        if(prevOwner == 1) {card.changeOwner(2)}
        if(prevOwner == 2) {card.changeOwner(1)}
        break;
      case 1:
        if(prevOwner == 1) {card.changeOwner(2)}
        if(prevOwner == 2) {card.changeOwner(3)}
        if(prevOwner == 3) {card.changeOwner(4)}
        if(prevOwner == 4) {card.changeOwner(1)}
        break;
      case 2:
        if(prevOwner == 1) {card.changeOwner(2)}
        if(prevOwner == 2) {card.changeOwner(3)}
        if(prevOwner == 3) {card.changeOwner(1)}
        break;
      case 3:
        if(prevOwner == 1) {card.changeOwner(2)}
        if(prevOwner == 2) {card.changeOwner(3)}
        if(prevOwner == 3) {card.changeOwner(4)}
        if(prevOwner == 4) {card.changeOwner(1)}
        break;
      case 4:
        if(prevOwner == 1) {card.changeOwner(3)}
        if(prevOwner == 3) {card.changeOwner(1)}
        break;
    }

    //Ask the player which card to place
    menu.showInformation(qsTranslate("antiquewatch", "Choose an item to play"), -1);
    //The player chooses one
    var selectedToPlace = 0;
    if(gameState.getCurrentPlayer() == 1)
    {
      var isAllowedToPlace = p1Inventory.getAllCards()
      for(i = 0; i < p1Inventory.getAllCards().length; i++)
      {
        if(boardutils.haltedByNet(isAllowedToPlace[i].getBaseValue()) == false && watchutils.secondWatchCondition(isAllowedToPlace[i]) == true)
        {
          do
          {
            selectedToPlace = p1Inventory.cardChoice(p1Inventory.getAllCards(), 0, 1);
          }
          while(boardutils.haltedByNet(selectedToPlace.getBaseValue()) == true || watchutils.secondWatchCondition(selectedToPlace) == false);
          break;
        }
        if(i == p1Inventory.getAllCards().length - 1)
        {
          menu.hideInformation();
          menu.showInformation(qsTranslate("antiquewatch", "No item in your hand fulfills the criteria!"));
        }
      }
    }
    if(gameState.getCurrentPlayer() == 2)
    {
      var isAllowedToPlace = p2Inventory.getAllCards()
      for(i = 0; i < p2Inventory.getAllCards().length; i++)
      {
        if(boardutils.haltedByNet(isAllowedToPlace[i].getBaseValue()) == false && watchutils.secondWatchCondition(isAllowedToPlace[i]) == true)
        {
          do
          {
            selectedToPlace = p2Inventory.cardChoice(p2Inventory.getAllCards(), 0, 1);
          }
          while(boardutils.haltedByNet(selectedToPlace.getBaseValue()) == true || watchutils.secondWatchCondition(selectedToPlace) == false);
          break;
        }
        if(i == p2Inventory.getAllCards().length - 1)
        {
          menu.hideInformation();
          menu.showInformation(qsTranslate("antiquewatch", "No item in your hand fulfills the criteria!"));
        }
      }
    }
    if(gameState.getCurrentPlayer() == 3)
    {
      var isAllowedToPlace = p3Inventory.getAllCards()
      for(i = 0; i < p3Inventory.getAllCards().length; i++)
      {
        if(boardutils.haltedByNet(isAllowedToPlace[i].getBaseValue()) == false && watchutils.secondWatchCondition(isAllowedToPlace[i]) == true)
        {
          do
          {
            selectedToPlace = p3Inventory.cardChoice(p3Inventory.getAllCards(), 0, 1);
          }
          while(boardutils.haltedByNet(selectedToPlace.getBaseValue()) == true || watchutils.secondWatchCondition(selectedToPlace) == false);
          break;
        }
        if(i == p3Inventory.getAllCards().length - 1)
        {
          menu.hideInformation();
          menu.showInformation(qsTranslate("antiquewatch", "No item in your hand fulfills the criteria!"));
        }
      }
    }
    if(gameState.getCurrentPlayer() == 4)
    {
      var isAllowedToPlace = p4Inventory.getAllCards()
      for(i = 0; i < p4Inventory.getAllCards().length; i++)
      {
        if(boardutils.haltedByNet(isAllowedToPlace[i].getBaseValue()) == false && watchutils.secondWatchCondition(isAllowedToPlace[i]) == true)
        {
          do
          {
            selectedToPlace = p4Inventory.cardChoice(p4Inventory.getAllCards(), 0, 1);
          }
          while(boardutils.haltedByNet(selectedToPlace.getBaseValue()) == true || watchutils.secondWatchCondition(selectedToPlace) == false);
          break;
        }
        if(i == p4Inventory.getAllCards().length - 1)
        {
          menu.hideInformation();
          menu.showInformation(qsTranslate("antiquewatch", "No item in your hand fulfills the criteria!"));
        }
      }
    }

    if(selectedToPlace != 0)
    {
      menu.hideInformation();
      menu.showInformation(qsTranslate("antiquewatch", "Choose where to play the %1").arg(selectedToPlace.getName()), -1);
      //The player chooses one from the good positions
      allEmptyTiles = boardutils.getEmptyTiles();
      if(watchutils.getValidTiles(selectedToPlace).length > 0)
      {
      //Előfordulhat, hogy watchutils.getValidTiles(selectedToPlace) üres, amikor az alábbi hibához vezet
      selectedPos = boardutils.tileChoice(watchutils.getValidTiles(selectedToPlace), 0);
      } else return; 
      //We got the choice, hide the info
      menu.hideInformation();
      //Log
      boardutils.logSilverActivation(card.getName());
      var userNames = gameState.getPlayerNickNames();
      gameState.writeToGameLog(qsTranslate("antiquewatch", "<font color=\"paleturquoise\">The %1 played the %2 from %3's hand to %4%5.</font>").arg(card.getName()).arg(selectedToPlace.getName()).arg(userNames[gameState.getCurrentPlayer()-1]).arg(boardutils.convertToABC(selectedPos.x)).arg(selectedPos.y+1), [1, 2, 3, 4], 1, 0);
      //Deny active abilities
      gameState.setSilverActionsDisabledForTurn(1);
      //Log
      gameState.writeToGameLog(qsTranslate("antiquewatch", "%1 cannot use active abilities for the remaining turn.").arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
      switch(gameState.getCurrentPlayer())
      {
        case 1:
          p1Inventory.moveCardToBoard(selectedToPlace.getUID(), selectedPos, 1);
          break;
        case 2:
          p2Inventory.moveCardToBoard(selectedToPlace.getUID(), selectedPos, 1);
          break;
        case 3:
          p3Inventory.moveCardToBoard(selectedToPlace.getUID(), selectedPos, 1);
          break;
        case 4:
          p4Inventory.moveCardToBoard(selectedToPlace.getUID(), selectedPos, 1);
          break;
      }
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
