//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums
//@include jsutils.point

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.dominoEntryConditions();
}

//This function handles all events
function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "turnStarted")
  {
    card.setShowFullSize(1);
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    var eT = [];
    var eAT = boardutils.getAdjacentEmptyTiles(card.getPosition());
    if(eAT.length > 0)
    {
      //Check for Perfume Bottles, they can deny this ability
      if(boardutils.limitedByPerfume() == false)
      {
        eT = boardutils.getAdjacentEmptyTiles(card.getPosition());
      }
      else
      {
        var ePT = boardutils.getTilesAbovePerfume();
        for(i = 0; i < eAT.length; i++)
        {
          for(j = 0; j < ePT.length; j++)
          {
            if(ePT[j].x == eAT[i].x && ePT[j].y == eAT[i].y)
            {
              eT.push(eAT[i]);
              break;
            }
          }
        }
      }
    }

    if(eT.length > 0)
    {
      if(gameState.getGameMode() > 0 && gameState.getGameMode() < 4)
      {
        viewablePlayers = [] //This will store the possible players for the first choice
        if(gameState.getCurrentPlayer() == 2 || (gameState.getCurrentPlayer() == 3 && gameState.getGameMode() != 1) || gameState.getCurrentPlayer() == 4)
        {
          if(!p1Inventory.isEmpty())
          {
            viewablePlayers.push(1);
          }
        }
        if(gameState.getCurrentPlayer() == 1 || (gameState.getCurrentPlayer() == 3) || (gameState.getCurrentPlayer() == 4 && gameState.getGameMode() != 1))
        {
          if(!p2Inventory.isEmpty())
          {
            viewablePlayers.push(2);
          }
        }
        if((gameState.getCurrentPlayer() == 1 && gameState.getGameMode() != 1) || gameState.getCurrentPlayer() == 2 || gameState.getCurrentPlayer() == 4)
        {
          if(!p3Inventory.isEmpty())
          {
            viewablePlayers.push(3);
          }
        }
        if((gameState.getCurrentPlayer() == 1 && gameState.getGameMode() != 2) || (gameState.getCurrentPlayer() == 2 && gameState.getGameMode() == 3) || (gameState.getCurrentPlayer() == 3 && gameState.getGameMode() != 2 ))
        {
          if(!p4Inventory.isEmpty())
          {
            viewablePlayers.push(4);
          }
        }
        menu.showInformation(qsTranslate("horseshoemagnet", "Which player's hand would you like to view?"), -1);
        //Get the player's answer
        choice = graphicsViewMenu.getPlayer(viewablePlayers, 0);
        //Now we have the choice, it's time to make the chosen player's inventory visible
        switch(choice)
        {
          case 1:
            p1Inventory.setViewable(true, true);
            break;
          case 2:
            p2Inventory.setViewable(true, true);
            break;
          case 3:
            p3Inventory.setViewable(true, true);
            break;
          case 4:
            p4Inventory.setViewable(true, true);
            break;
        }
        //Hide the 'Which player's inventory...' label
        menu.hideInformation();
      }
      else
      {
        //1v1 opposing
        if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 1)
        {
          p2Inventory.setViewable(true, true);
          choice = 2;
        }
        if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 2)
        {
          p1Inventory.setViewable(true, true);
          choice = 1;
        }
        //1v1 adjacent
        if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 1)
        {
          p3Inventory.setViewable(true, true);
          choice = 3;
        }
        if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 3)
        {
          p1Inventory.setViewable(true, true);
          choice = 1;
        }
      }

      //Log
      boardutils.logSilverActivation(card.getName());
      var userNames = gameState.getPlayerNickNames();
      gameState.writeToGameLog(qsTranslate("horseshoemagnet", "<font color=\"paleturquoise\">The %1 revealed %2's hand.</font>").arg(card.getName()).arg(userNames[choice-1]), [1, 2, 3, 4], 1, 0);

      switch(choice)
      {
        case 1:
          var cardsInHand = p1Inventory.getAllCards();
          break;
        case 2:
          var cardsInHand = p2Inventory.getAllCards();
          break;
        case 3:
          var cardsInHand = p3Inventory.getAllCards();
          break;
        case 4:
          var cardsInHand = p4Inventory.getAllCards();
          break;
      }
      metalCardsInHand = [];
      for(i = 0; i < cardsInHand.length; i++)
      {
        if(cardsInHand[i].getBackground() == "metal")
        {
          metalCardsInHand.push(cardsInHand[i]);
        }
      }

      if(metalCardsInHand.length > 0)
      {
        var randomisedToAttract = 0
        //Ask the player where to place the metal item
        menu.showInformation(qsTranslate("horseshoemagnet", "Choose the tile"), -1);
        //The player chooses one from the good positions
        selectedPos = boardutils.tileChoice((eT), 0);
        //We got the choice, hide the info
        menu.hideInformation();
        randomlyPlacedMetal = metalCardsInHand[rng.randomBetween(0, metalCardsInHand.length)];
        randomlyPlacedMetal.setGrayScale(1);
        switch(choice)
        {
          case 1:
            p1Inventory.moveCardToBoard(randomlyPlacedMetal.getUID(), selectedPos, 0);
            break;
          case 2:
            p2Inventory.moveCardToBoard(randomlyPlacedMetal.getUID(), selectedPos, 0);
            break;
          case 3:
            p3Inventory.moveCardToBoard(randomlyPlacedMetal.getUID(), selectedPos, 0);
            break;
          case 4:
            p4Inventory.moveCardToBoard(randomlyPlacedMetal.getUID(), selectedPos, 0);
            break;
        }

        //Log
        gameState.writeToGameLog(qsTranslate("horseshoemagnet", "<font color=\"paleturquoise\">The %1 disabled the %2 and played it from %3's hand to %4%5.</font>").arg(card.getName()).arg(randomlyPlacedMetal.getName()).arg(userNames[choice-1]).arg(boardutils.convertToABC(selectedPos.x)).arg(selectedPos.y+1), [1, 2, 3, 4], 1, 0);
        //Innovating
        boardutils.Innovate(selectedPos.x, selectedPos.y, gameState.getCurrentPlayer());
      }
      else
      {
        menu.showInformation(qsTranslate("horseshoemagnet", "No metal items in this hand!"));
        card.setGrayScale(1);

        //Log
        gameState.writeToGameLog(qsTranslate("horseshoemagnet", "<font color=\"paleturquoise\">The %1 lost its abilities.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
      }
      //Crop
      card.setShowFullSize(0);
    }
    else
    {
      menu.showInformation(qsTranslate("horseshoemagnet", "You cannot use this ability!"));
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
