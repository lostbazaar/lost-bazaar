//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "turnStarted")
  {
    card.setShowFullSize(1);
  }

  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    if(boardutils.checkForImmovable(card) == false)
    {
      var divination = 0;
      var choice = 0;

      if(gameState.getGameMode() == 0 || gameState.getGameMode() == 4)
      {
        //1v1 opposing
        if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 1 && p2Inventory.getAllCards().length > 0)
        {
          divination = p2Inventory.getAllCards()[rng.randomBetween(0, p2Inventory.getAllCards().length)];
          choice = 1;
        }
        if(gameState.getGameMode() == 0 && gameState.getCurrentPlayer() == 2 && p1Inventory.getAllCards().length > 0)
        {
          divination = p1Inventory.getAllCards()[rng.randomBetween(0, p1Inventory.getAllCards().length)];
          choice = 2;
        }
        //1v1 adjacent
        if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 1 && p3Inventory.getAllCards().length > 0)
        {
          divination = p3Inventory.getAllCards()[rng.randomBetween(0, p3Inventory.getAllCards().length)];
          choice = 1;
        }
        if(gameState.getGameMode() == 4 && gameState.getCurrentPlayer() == 3 && p1Inventory.getAllCards().length > 0)
        {
          divination = p1Inventory.getAllCards()[rng.randomBetween(0, p1Inventory.getAllCards().length)];
          choice = 3;
        }
      }
      else
      {
        viewablePlayers = [] //This will store the possible players for the first choice
        if(gameState.getCurrentPlayer() == 2 || gameState.getCurrentPlayer() == 3 || (gameState.getCurrentPlayer() == 4 && gameState.getGameMode() == 3))
        {
          if(!p1Inventory.isEmpty())
          {
            viewablePlayers.push(1);
          }
        }
        if(gameState.getCurrentPlayer() == 1 || (gameState.getCurrentPlayer() == 3 && (gameState.getGameMode() == 2 || gameState.getGameMode() == 3)) || gameState.getCurrentPlayer() == 4)
        {
          if(!p2Inventory.isEmpty())
          {
            viewablePlayers.push(2);
          }
        }
        if(gameState.getCurrentPlayer() == 1 || (gameState.getCurrentPlayer() == 2 && gameState.getGameMode() > 1) || gameState.getCurrentPlayer() == 4)
        {
          if(!p3Inventory.isEmpty())
          {
            viewablePlayers.push(3);
          }
        }
        if((gameState.getCurrentPlayer() == 1 && gameState.getGameMode() == 3) || (gameState.getCurrentPlayer() == 2 && gameState.getGameMode() != 2) || (gameState.getCurrentPlayer() == 3 && gameState.getGameMode() != 2 ))
        {
          if(!p4Inventory.isEmpty())
          {
            viewablePlayers.push(4);
          }
        }

        if(viewablePlayers.length > 0)
        {
          menu.showInformation(qsTranslate("sneaker", "Which player's item would you like to view?"), -1);
          //Get the player's answer
          choice = graphicsViewMenu.getPlayer(viewablePlayers, 0);
          //Now we have the choice, it's time to make a random card of the chosen player's inventory visible
          switch(choice)
          {
            case 1:
              divination = p1Inventory.getAllCards()[rng.randomBetween(0, p1Inventory.getAllCards().length)];
              break;
            case 2:
              divination = p2Inventory.getAllCards()[rng.randomBetween(0, p2Inventory.getAllCards().length)];
              break;
            case 3:
              divination = p3Inventory.getAllCards()[rng.randomBetween(0, p3Inventory.getAllCards().length)];
              break;
            case 4:
              divination = p4Inventory.getAllCards()[rng.randomBetween(0, p4Inventory.getAllCards().length)];
              break;
          }
          //Hide the 'Which player's card...' label
          menu.hideInformation();
        }
      }

      if(divination != 0)
      {
        menu.showInformation(qsTranslate("sneaker", "You revealed this item:"), -1);
        choiceArray = [qsTranslate("sneaker", "%1").arg(divination.getName())]
        enableArray = ["true"];
        toolTipArray = [""];
        var choice2 = menu.getChoice(choiceArray, enableArray, toolTipArray);
        menu.hideInformation();
        var divinedDistance = divination.getBaseValue();
        if(divinedDistance < 0)
        {
          divinedDistance = (divinedDistance * -1)
        }

        if(boardutils.getPossibleSneakerMoves(card.getPosition(), divinedDistance).length > 0)
        {
          //Ask the player where to move the shoe
          menu.showInformation(qsTranslate("sneaker", "Choose where to move the %1").arg(card.getName()), -1);
          //The player chooses one from the good positions
          selectedPos = boardutils.tileChoice(boardutils.getPossibleSneakerMoves(card.getPosition(), divinedDistance), 0);
          //We got the choice, hide the info
          menu.hideInformation();
          //Log
          boardutils.logSilverActivation(card.getName());
          //Move the card
          board.moveCard(card.getPosition(), selectedPos);
          //Crop
          card.setShowFullSize(0);
          
          //Log
          var userNames = gameState.getPlayerNickNames();
          if(gameState.getGameMode() != 0 && gameState.getGameMode() != 4)
          {
            var thirdParty = [];
            for(tp = 1; tp < 5; tp++)
            {
              if(tp != gameState.getCurrentPlayer() && tp != choice)
              {
                thirdParty.push(tp);
              }
            }
            gameState.writeToGameLog(qsTranslate("sneaker", "<font color=\"paleturquoise\">The %1 revealed an item of %2 value in %3's hand.</font>").arg(card.getName()).arg(divinedDistance).arg(userNames[choice-1]), [thirdParty[0], thirdParty[1]], 1, 0);
            gameState.writeToGameLog(qsTranslate("sneaker", "<font color=\"paleturquoise\">The %1 revealed %2 in %3's hand.</font>").arg(card.getName()).arg(divination.getName()).arg(userNames[choice-1]), [gameState.getCurrentPlayer(), choice-1], 1, 0);
          }
          else
          {
            gameState.writeToGameLog(qsTranslate("sneaker", "<font color=\"paleturquoise\">The %1 revealed %2 in %3's hand.</font>").arg(card.getName()).arg(divination.getName()).arg(userNames[choice-1]), [1, 2, 3, 4], 1, 0);
          }
          gameState.writeToGameLog(qsTranslate("sneaker", "<font color=\"paleturquoise\">The %1 has been moved to %2%3.</font>").arg(card.getName()).arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1), [1, 2, 3, 4], 1, 0);
        }
        else
        {
          menu.showInformation(qsTranslate("sneaker", "Nowhere to move!"));
          //Crop
          card.setShowFullSize(0);
          //Log
          boardutils.logSilverActivation(card.getName());

          //Log
          var userNames = gameState.getPlayerNickNames();
          if(gameState.getGameMode() != 0 && gameState.getGameMode() != 4)
          {
            var thirdParty = [];
            for(tp = 1; tp < 5; tp++)
            {
              if(tp != gameState.getCurrentPlayer() && tp != choice)
              {
                thirdParty.push(tp);
              }
            }
            gameState.writeToGameLog(qsTranslate("sneaker", "<font color=\"paleturquoise\">The %1 revealed an item of %2 value in %3's hand.</font>").arg(card.getName()).arg(divinedDistance).arg(userNames[choice-1]), [thirdParty[0], thirdParty[1]], 1, 0);
            gameState.writeToGameLog(qsTranslate("sneaker", "<font color=\"paleturquoise\">The %1 revealed %2 in %3's hand.</font>").arg(card.getName()).arg(divination.getName()).arg(userNames[choice-1]), [gameState.getCurrentPlayer(), choice-1], 1, 0);
          }
          else
          {
            gameState.writeToGameLog(qsTranslate("sneaker", "<font color=\"paleturquoise\">The %1 revealed %2 in %3's hand.</font>").arg(card.getName()).arg(divination.getName()).arg(userNames[choice-1]), [1, 2, 3, 4], 1, 0);
          }

        }
      }
      else
      {
        menu.showInformation(qsTranslate("sneaker", "No items to reveal!"));
      }
    }
    else
    {
      menu.showInformation(qsTranslate("sneaker", "You cannot move this item!"));
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
