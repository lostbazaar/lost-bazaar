//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.dominoEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer() && card.isGrayScale() == false)
  {
    cardsOK = [];
    cardsOK2 = [];
    cardsOKValue = [];
    if(board.getCardAtPosition(Point(card.getPosX()+1,card.getPosY())) != null)
    {
      cardsOK.push(board.getCardAtPosition(Point(card.getPosX()+1,card.getPosY())));
    }
    if(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()+1)) != null)
    {
      cardsOK.push(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()+1)));
    }
    if(board.getCardAtPosition(Point(card.getPosX()-1,card.getPosY())) != null)
    {
      cardsOK.push(board.getCardAtPosition(Point(card.getPosX()-1,card.getPosY())));
    }
    if(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()-1)) != null)
    {
      cardsOK.push(board.getCardAtPosition(Point(card.getPosX(),card.getPosY()-1)));
    }
    if(cardsOK.length > 0)
    {
      for(i = 0; i < cardsOK.length; i++)
      {
        cardsOKValue.push(cardsOK[i].getCurrentValue());
      }
      var lowestValue = Math.min.apply(Math, cardsOKValue);
      for(i = 0; i < cardsOK.length; i++)
      {
        if(cardsOKValue[i] == lowestValue)
        {
          cardsOK2.push(cardsOK[i]);
        }
      }
      var rotatedByFlag = cardsOK2[rng.randomBetween(0, cardsOK2.length)];
      if(boardutils.checkForUnrotatable(rotatedByFlag) == false)
      {
        var prevOwner = rotatedByFlag.getPlayer();
        if(prevOwner == 1) {rotatedByFlag.changeOwner(2)}
        if(prevOwner == 2) {rotatedByFlag.changeOwner(1)}
        if(prevOwner == 3) {rotatedByFlag.changeOwner(4)}
        if(prevOwner == 4) {rotatedByFlag.changeOwner(3)}
        //Log
        gameState.writeToGameLog(qsTranslate("flag", "<font color=\"khaki\">%1%2 %3 rotated %4%5 %6 by 180°.</khaki>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()).arg(boardutils.convertToABC(rotatedByFlag.getPosX())).arg(rotatedByFlag.getPosY()+1).arg(rotatedByFlag.getName()), [1, 2, 3, 4], 1, 0);
      }
    }
    card.setGrayScale(1);
    gameState.writeToGameLog(qsTranslate("flag", "<font color=\"khaki\">%1%2 %3 has been disabled.</khaki>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()), [1, 2, 3, 4], 1, 0);
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}


//This function returns with the current value of this card which is increased for own cards in the same column
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
