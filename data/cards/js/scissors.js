//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

//This function handles all events
function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    //Logging
    boardutils.logCardPlacement(card.getName());
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1] && boardutils.checkForUnrotatable(card) == false)
  {
    menu.showInformation(qsTranslate("scissors", "Choose the direction for rotating the %1").arg(card.getName()), -1);

    choiceArray = [
    qsTranslate("scissors", "Rotate counter-clockwise"),
    qsTranslate("scissors", "Rotate 180 degrees"),
    qsTranslate("scissors", "Rotate clockwise")
    ];
    enableArray = ["true", "true", "true"];
    toolTipArray = ["", "", ""];

    var choice = menu.getChoice(choiceArray, enableArray, toolTipArray);
    menu.hideInformation();

    //Log
    boardutils.logSilverActivation(card.getName());

    if(choice == 0)
    {
      var prevOwner = card.getPlayer()
      if(prevOwner == 1) {card.changeOwner(4)}
      if(prevOwner == 2) {card.changeOwner(3)}
      if(prevOwner == 3) {card.changeOwner(1)}
      if(prevOwner == 4) {card.changeOwner(2)}

      //Log
      gameState.writeToGameLog(qsTranslate("scissors", "<font color=\"paleturquoise\">The %1 have been rotated by 90° counter-clockwise.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
    if(choice == 1)
    {
      var prevOwner = card.getPlayer()
      if(prevOwner == 1) {card.changeOwner(2)}
      if(prevOwner == 2) {card.changeOwner(1)}
      if(prevOwner == 3) {card.changeOwner(4)}
      if(prevOwner == 4) {card.changeOwner(3)}

      //Log
      gameState.writeToGameLog(qsTranslate("scissors", "<font color=\"paleturquoise\">The %1 have been rotated by 180°.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
    if(choice == 2)
    {
      var prevOwner = card.getPlayer()
      if(prevOwner == 1) {card.changeOwner(3)}
      if(prevOwner == 2) {card.changeOwner(4)}
      if(prevOwner == 3) {card.changeOwner(2)}
      if(prevOwner == 4) {card.changeOwner(1)}

      //Log
      gameState.writeToGameLog(qsTranslate("scissors", "<font color=\"paleturquoise\">The %1 have been rotated by 90° clockwise.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
  }

  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer() && card.isGrayScale() == false)
  {
    if(card.getCounter() < 12)
    {
      card.setCounter(card.getCounter()+1);

      //Log
      gameState.writeToGameLog(qsTranslate("scissors", "<font color=\"khaki\">%1%2 %3 gained 1 counter.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }
  }

  if(card.isGrayScale() == false)
  {
    var xCorri = 0;
    var yCorri = 0;
    if(card.getPlayer() == 1) {xCorri = -1;}
    if(card.getPlayer() == 2) {xCorri = 1;}
    if(card.getPlayer() == 3) {yCorri = -1;}
    if(card.getPlayer() == 4) {yCorri = 1;}

    //Do it once, but repeat it if conditions were met
    for(i = 0; i < 1; i++)
    {
      var CTC = board.getCardAtPosition(Point(card.getPosX()+xCorri,card.getPosY()+yCorri));
      if(CTC != null && CTC.getCurrentValue() <= card.getCounter())
      {
        if(boardutils.checkForUntrashable(CTC) == false)
        {
          var hitByDamage = CTC;
          damageOutcome = boardutils.damageEvents(card.getID(), hitByDamage, hitByDamage.getPosX(), hitByDamage.getPosY())
          var explodingItems = damageOutcome[0];

          //Log
          gameState.writeToGameLog(qsTranslate("scissors", "<font color=\"khaki\">%1%2 %3 destroyed %4%5 %6.</font>").arg(boardutils.convertToABC(card.getPosX())).arg(card.getPosY()+1).arg(card.getName()).arg(boardutils.convertToABC(hitByDamage.getPosX())).arg(hitByDamage.getPosY()+1).arg(hitByDamage.getName()), [1, 2, 3, 4], 1, 0);

          board.moveCardToTrash(CTC.getUID());
          var scissorsPrevPosX = card.getPosX();
          var scissorsPrevPosY = card.getPosY();

          if(boardutils.checkForImmovable(card) == false)
          {
            //Log
            gameState.writeToGameLog(qsTranslate("scissors", "<font color=\"khaki\">The %1 has been moved to %2%3.</font>").arg(card.getName()).arg(boardutils.convertToABC(card.getPosX()+xCorri)).arg(card.getPosY()+yCorri+1), [1, 2, 3, 4], 1, 0);

            board.moveCard(card.getPosition(), Point(card.getPosX()+xCorri,card.getPosY()+yCorri));
          }

          if(explodingItems.length > 1)
          {
            //It has to be decided whether the Scissors remain intact or not
            var scissorsSurvive = true;
            if(scissorsPrevPosX = card.getPosX() || scissorsPrevPosY != card.getPosY())
            {
              for(ctc = 2; ctc < 10; ctc = ctc+2)
              {
                var xCorri2 = 0;
                var yCorri2 = 0;
                if(ctc == 2 && card.getPosY() < board.getHeight() - 1) {yCorri2 = 1;}
                if(ctc == 4 && card.getPosX() > 0) {xCorri2 = -1;}
                if(ctc == 6 && card.getPosX() < board.getWidth() - 1) {xCorri2 = 1;}
                if(ctc == 8 && card.getPosY() > 0) {yCorri2 = -1;}
                TTC = Point(card.getPosX()+xCorri2,card.getPosY()+yCorri2);
                CTC = board.getCardAtPosition(TTC);
                if(CTC != null && CTC.getID() == "bomb" && CTC.isGrayScale() == false && boardutils.checkForUntrashable(CTC) == false)
                {
                  //There was another adjacent trashable Bomb
                  var scissorsSurvive = false;
                  break;
                }
              }
            }

            //Bomb events
            for(boomOrder = 1; boomOrder < explodingItems.length; boomOrder++)
            {
              var BBV = gameState.cardForUID(damageOutcome[0][boomOrder]);
              if(damageOutcome[0][boomOrder] != card.getUID() || scissorsSurvive == false)
              {
                board.moveCardToTrash(damageOutcome[0][boomOrder]);

                //Log
                gameState.writeToGameLog(qsTranslate("bomb", "<font color=\"khaki\">The %1 destroyed %2%3 %4.</font>").arg(hitByDamage.getName()).arg(boardutils.convertToABC(BBV.getPosX())).arg(BBV.getPosY()+1).arg(BBV.getName()), [1, 2, 3, 4], 1, 0);
              }
            }
          }
          //Piggy Bank events
          if(damageOutcome[1] > 0) {for(drawCount = 0; drawCount < damageOutcome[1]; drawCount++) {gameState.drawCard(1, 0, 0, 0, 0, "khaki");}}
          if(damageOutcome[2] > 0) {for(drawCount = 0; drawCount < damageOutcome[2]; drawCount++) {gameState.drawCard(2, 0, 0, 0, 0, "khaki");}}
          if(damageOutcome[3] > 0) {for(drawCount = 0; drawCount < damageOutcome[3]; drawCount++) {gameState.drawCard(3, 0, 0, 0, 0, "khaki");}}
          if(damageOutcome[4] > 0) {for(drawCount = 0; drawCount < damageOutcome[4]; drawCount++) {gameState.drawCard(4, 0, 0, 0, 0, "khaki");}}

          //Repeat the loop
          i--;
        }

        if(card.getCounter() == 1)
        {
          gameState.writeToGameLog(qsTranslate("scissors", "<font color=\"khaki\">The %1 cast 1 counter.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
          card.setCounter(0);
        }
        if(card.getCounter() > 1)
        {
          gameState.writeToGameLog(qsTranslate("scissors", "<font color=\"khaki\">The %1 cast %2 counters.</font>").arg(card.getName()).arg(card.getCounter()), [1, 2, 3, 4], 1, 0);
          card.setCounter(0);
        }
      }
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
