//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    eT = boardutils.getEmptyTiles();
    if(boardutils.isHalted(eT) == false && board.isDoubleTile(card.getPosition()) == false)
    {
      var GGM = gameState.getGameMode();
      var p1Discards = Math.floor((p1Inventory.getAllCards().length)/2);
      var p2Discards = 0;
      var p3Discards = 0;
      var p4Discards = 0;
      if(GGM != 4) {p2Discards = Math.floor((p2Inventory.getAllCards().length)/2);}
      if(GGM != 0) {p3Discards = Math.floor((p3Inventory.getAllCards().length)/2);}
      if(GGM == 1 || GGM == 3) {p4Discards = Math.floor((p4Inventory.getAllCards().length)/2);}

      //Log
      boardutils.logCardPlacement(card.getName());
      var userNames = gameState.getPlayerNickNames();

      if(p1Discards > 0)
      {
        for(i = 0; i < p1Discards; i++)
        {
          handDiscardedFrom = p1Inventory.getAllCards();
          cardRandomlyDiscarded = handDiscardedFrom[rng.randomBetween(0, p1Inventory.getAllCards().length)];
          p1Inventory.removeCard(cardRandomlyDiscarded.getUID());
          trash.addCard(cardRandomlyDiscarded);
          gameState.writeToGameLog(qsTranslate("buzzsaw", "<font color=\"bisque\">The %1 discarded the %2 from %3's hand.</font>").arg(card.getName()).arg(cardRandomlyDiscarded.getName()).arg(userNames[0]), [1, 2, 3, 4], 1, 0);
        }
      }
      if(p2Discards > 0)
      {
        for(i = 0; i < p2Discards; i++)
        {
          handDiscardedFrom = p2Inventory.getAllCards();
          cardRandomlyDiscarded = handDiscardedFrom[rng.randomBetween(0, p2Inventory.getAllCards().length)];
          p2Inventory.removeCard(cardRandomlyDiscarded.getUID());
          trash.addCard(cardRandomlyDiscarded);
          gameState.writeToGameLog(qsTranslate("buzzsaw", "<font color=\"bisque\">The %1 discarded the %2 from %3's hand.</font>").arg(card.getName()).arg(cardRandomlyDiscarded.getName()).arg(userNames[1]), [1, 2, 3, 4], 1, 0);
        }
      }
      if(p3Discards > 0)
      {
        for(i = 0; i < p3Discards; i++)
        {
          handDiscardedFrom = p3Inventory.getAllCards();
          cardRandomlyDiscarded = handDiscardedFrom[rng.randomBetween(0, p3Inventory.getAllCards().length)];
          p3Inventory.removeCard(cardRandomlyDiscarded.getUID());
          trash.addCard(cardRandomlyDiscarded);
          gameState.writeToGameLog(qsTranslate("buzzsaw", "<font color=\"bisque\">The %1 discarded the %2 from %3's hand.</font>").arg(card.getName()).arg(cardRandomlyDiscarded.getName()).arg(userNames[2]), [1, 2, 3, 4], 1, 0);
        }
      }
      if(p4Discards > 0)
      {
        for(i = 0; i < p4Discards; i++)
        {
          handDiscardedFrom = p4Inventory.getAllCards();
          cardRandomlyDiscarded = handDiscardedFrom[rng.randomBetween(0, p4Inventory.getAllCards().length)];
          p4Inventory.removeCard(cardRandomlyDiscarded.getUID());
          trash.addCard(cardRandomlyDiscarded);
          gameState.writeToGameLog(qsTranslate("buzzsaw", "<font color=\"bisque\">The %1 discarded the %2 from %3's hand.</font>").arg(card.getName()).arg(cardRandomlyDiscarded.getName()).arg(userNames[3]), [1, 2, 3, 4], 1, 0);
        }
      }
    }
    else
    {
      card.setGrayScale(1);
      //Log
      boardutils.logCardPlacement(card.getName());
      gameState.writeToGameLog(qsTranslate("buzzsaw", "The %1 has been disabled.").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    }

    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
