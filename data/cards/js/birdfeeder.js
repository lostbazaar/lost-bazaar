//We'll need some utility functions for handling the board, so we include them:
//@include jsutils.board
//@include jsutils.enums

//This function is called before card placement
//It returns with the coordinates of tiles available for placing this card
function getAvailableTiles()
{
  return boardutils.defaultEntryConditions();
}

function event(eventData)
{
  if(eventData[0] == "cardPlacedOnBoard" && card.getUID().toString() == eventData[1])
  {
    var xPlay = card.getPosX();
    var yPlay = card.getPosY();
    card.setCounter(3);
    //Log
    boardutils.logCardPlacement(card.getName());
    gameState.writeToGameLog(qsTranslate("birdfeeder", "<font color=\"bisque\">The %1 gained 3 counters.</font>").arg(card.getName()), [1, 2, 3, 4], 1, 0);
    //Innovating
    boardutils.Innovate(xPlay, yPlay, gameState.getCurrentPlayer());
  }

  if(eventData[0] == "turnStarted")
  {
    card.setShowFullSize(1);
  }

  if(eventData[0] == "cardSilverActionActivated" && card.getUID().toString() == eventData[1])
  {
    cards = board.getAllCards(); //Retrieve all cards on the board
    cardsOK = []; //This will store the cards which can be targeted
    for(i = 0; i < cards.length; i++) //Check all cards
    {
      //If the card is adjacent to the Bird feeder
      if(Math.pow(card.getPosX()-cards[i].getPosX(), 2) + Math.pow(card.getPosY()-cards[i].getPosY(), 2) == 1)
      {
        //If it is targetable by active abilities - BIRD FEEDER ITSELF IS AN EXCEPTION
        if(cards[i].getID() != "mask" || (cards[i].getID() == "handmirror" && boardutils.itemMirrored(cards[i]) == "mask"))
        {
          //Store the coordinates of the good card
          cardsOK.push(cards[i].getPosition());
        }
      }
    }
    if(cardsOK.length > 0 && card.getCounter() > 0)
    {
      //Ask the player which card would he like to move
      menu.showInformation(qsTranslate("birdfeeder", "Choose which item would you like to place the counter on"), -1);
      //The player chooses one from the good cards
      selectedTile = boardutils.tileChoice(cardsOK, 1);
      selectedCard = board.getCardAtPosition(selectedTile);
      //Hide the 'Choose which card...' label
      menu.hideInformation();
      //Unless canceled
      if(selectedTile.x != -1)
      {
        //Log
        boardutils.logSilverActivation(card.getName());

        //Take down the 1 counter
        card.setCounter(card.getCounter()-1);
        //Add 1 counter
        selectedCard.setCounter(selectedCard.getCounter()+1);
        //It gets capped at 12
        if(selectedCard.getCounter() > 11)
        {
          selectedCard.setCounter(12);
        }
        //Log
        gameState.writeToGameLog(qsTranslate("birdfeeder", "<font color=\"paleturquoise\">The %1 transfered 1 counter from itself to %2%3 %4.</font>").arg(card.getName()).arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()), [1, 2, 3, 4], 1, 0);
        //Check for Golf Club
        if( ( (selectedCard.getID() == "golfclub" && selectedCard.isGrayScale() == false) || (selectedCard.getID() == "handmirror" && boardutils.itemMirrored(selectedCard) == "golfclub") ) && boardutils.checkForUnrotatable(selectedCard) == false && selectedCard.getPlayer() != gameState.getCurrentPlayer())
        {
          selectedCard.changeOwner(gameState.getCurrentPlayer());
          //Log
          var userNames = gameState.getPlayerNickNames();
          gameState.writeToGameLog(qsTranslate("birdfeeder", "<font color=\"khaki\">%1%2 %3 has been rotated toward %4.</font>").arg(boardutils.convertToABC(selectedCard.getPosX())).arg(selectedCard.getPosY()+1).arg(selectedCard.getName()).arg(userNames[gameState.getCurrentPlayer()-1]), [1, 2, 3, 4], 1, 0);
        }
        //Crop
        card.setShowFullSize(0);
      }
    }
    else
    {
      menu.showInformation(qsTranslate("birdfeeder", "You cannot use this ability!"));
    }
  }
  
  //Enpowering
  if(eventData[0] == "turnStarted" && card.getPlayer() == gameState.getCurrentPlayer())
  {
    boardutils.Enpower(card.getPosX(), card.getPosY());
  }
}

//This function returns with the current value of this card
function getCurrentValue()
{
  return boardutils.defaultCurrentValue();
}
