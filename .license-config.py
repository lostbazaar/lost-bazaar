#List of licenses used in the project
allowed_licenses=["GPLv3+"]
#List of ignored filename/directory patterns
ignore_fname_list = []
#List of filename patterns that should be checked
match_list = ['*.cpp', '*.h']
#E-mail addresses (regex)
emails_regex = "ktamasw@gmail.com"
#Name(s) of the copyright holder(s) as regex
names_regex = "Tamás Krutki"
#The name of your project (regex)
project_name = "Lost Bazaar"
