#Lost Bazaar
#Copyright (C) 2014-2020  prkc  <prkc@lost-bazaar.org>
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import re

funcregex = re.compile(" *(static)? *Q_SCRIPTABLE (.*);")
classregex = re.compile(" *class ([a-z][a-zA-Z0-9]*) : p.*")
eventregex = re.compile(" *#event (.*)")
geninforegex = re.compile(" *#info (.*)")
commentstartregex = re.compile(" */\*! *")
commentendregex = re.compile(" *\*/ *")

htmlstart = """
<!DOCTYPE html>
<html>
<head>
<title>Lost Bazaar script reference</title>
<style>
table,th,td
{
border:1px solid black;
border-collapse:collapse
}
</style>
</head>
<body>
"""
tablestart = """
<h1>Script functions</h1>
<table>
<tr>
  <th>Class</th>
  <th>Function</th>             
  <th>Description</th>
</tr>
"""
tabledata = ""
htmlend = """
</body>
</html>
"""

htmlfile = open("script_reference.html",'w')
htmlfile.write(htmlstart)
events = ""
geninfo = ""
for fname in os.listdir("."):
    if fname.endswith(".cpp") or fname.endswith(".h"):
        f = open(fname, 'r')
        contents = f.readlines()
        comment = False
        commentText = ""
        for line in contents:
          if commentstartregex.match(line):
            commentText = ""
            comment = True
          elif commentendregex.match(line):
            comment = False
          elif comment:
            commentText = commentText + '\n' + line.strip()
          if classregex.match(line):
            classname = classregex.match(line).group(1)
          if eventregex.match(line):
            events = events + "<br>" + eventregex.match(line).group(1)
          if geninforegex.match(line):
            geninfo = geninfo + "<br>" + geninforegex.match(line).group(1)
          if funcregex.match(line):
            funcname = funcregex.match(line).group(2)
            if funcname.endswith(' const'): funcname = funcname[:-6]
            tabledata = '\n' + tabledata + "<tr><td>" + classname + "</td><td>" + funcname + "</td><td>" + commentText[1:].replace('\n',"<br>") + "</td></tr>"
        f.close()

htmlfile.write("<h1>General information</h1>" + geninfo[4:] + tablestart + tabledata + "</table>" + "<h1>Events</h1>" + events[4:])
htmlfile.write(htmlend)
htmlfile.close()
