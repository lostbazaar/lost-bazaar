/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
C++ include file for MT19937, with initialization improved 2002/1/26.
Coded by Takuji Nishimura and Makoto Matsumoto.
Ported to C++ by Jasper Bedaux 2003/1/1 (see http://www.bedaux.net/mtrand/).
The generators returning floating point numbers are based on
a version by Isaku Wada, 2002/01/09

Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

3. The names of its contributors may not be used to endorse or promote
   products derived from this software without specific prior written
   permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef RANDOM_H
#define RANDOM_H

#include <QObject>
#include <QByteArray>
#include <QDataStream>
#include <QIODevice>

class rng : public QObject
{
        Q_OBJECT

    public:
        /*!
        Returns a random integer between a and b (a is included, b is not)
        */
        Q_SCRIPTABLE static unsigned long randomBetween(unsigned long a, unsigned long b);
        /*!
        Seeds all random number generators
        */
        static void seedRNGs(unsigned long seed);
        /*!
        Returns randomly true or false
        */
        Q_SCRIPTABLE static bool randomBoolean();
        /*!
        Returns a random integer between zero and n (n is included)
        */
        Q_SCRIPTABLE static unsigned long randomBetweenZeroAndN(unsigned long n);
        void saveState();
        void restoreSavedState();
        QByteArray getState() const;

        /*!
        Mersenne Twister RNG
        Generates 32-bit integers
        */
        class mersenneTwisterInt32
        {
            public:
                /*!
                Default constructor: uses default seed only if this is the first instance
                */
                mersenneTwisterInt32()
                {
                    if (!init) seed(5489UL);
                    init = true;
                }
                /*!
                Constructor with 32 bit int as seed.
                */
                mersenneTwisterInt32(unsigned long s)
                {
                    seed(s);
                    init = true;
                }
                /*!
                Constructor with array of size 32 bit ints as seed.
                */
                mersenneTwisterInt32(const unsigned long* array, int size)
                {
                    seed(array, size);
                    init = true;
                }
                /*!
                Seed with 32 bit integer.
                */
                void seed(unsigned long);
                /*!
                Seed with array.
                */
                void seed(const unsigned long*, int size);
                /*!
                Overload operator() to make this a generator (functor)
                */
                unsigned long operator()()
                {
                    return rand_int32();
                }
                /*!
                Destructor.
                */
                virtual ~mersenneTwisterInt32() {}
                void saveState()
                {
                    for (int i = 0; i < n; ++i)
                    {
                        saved_state[i] = state[i];
                    }
                    saved_p = p;
                }
                QByteArray getState() const
                {
                    QByteArray res;
                    QDataStream stream(&res, QIODevice::WriteOnly);
                    for (int i = 0; i < n; ++i)
                    {
                        stream << quint64(state[i]);
                    }
                    stream << p;
                    return res;
                }
                void restoreSavedState()
                {
                    p = saved_p;
                    for (int i = 0; i < n; ++i)
                    {
                        state[i] = saved_state[i];
                    }
                }

            protected:
                /*!
                Generate 32 bit random integer.
                */
                inline unsigned long rand_int32()
                {
                    if (p == n) gen_state(); //New state vector needed

                    //gen_state() is split off to be non-inline, because it is only called once
                    //in every 624 calls and otherwise irand() would become too big to get inlined

                    unsigned long x = state[p++];
                    x ^= (x >> 11);
                    x ^= (x << 7) & 0x9D2C5680UL;
                    x ^= (x << 15) & 0xEFC60000UL;
                    return x ^ (x >> 18);
                }

            private:
                /*!
                Compile time constants.
                */
                static const int n = 624, m = 397;
                /*!
                State vector array.
                */
                static unsigned long state[n];
                static unsigned long saved_state[n];
                /*!
                Position in state array.
                */
                static int p;
                static int saved_p;
                /*!
                True if init function is called.
                */
                static bool init;
                /*!
                Private function used to generate the pseudo random numbers.
                Used by gen_state().
                Inline for speed, must therefore reside in header file
                */
                inline unsigned long twiddle(unsigned long u, unsigned long v)
                {
                    return (((u & 0x80000000UL) | (v & 0x7FFFFFFFUL)) >> 1) ^ ((v & 1UL) ? 0x9908B0DFUL : 0x0UL);
                }
                /*!
                Private function used to generate the pseudo random numbers.
                Generate new state.
                */
                void gen_state();
                /*!
                Make copy constructor and assignment operator unavailable, they don't make sense.
                Copy constructor not defined.
                */
                mersenneTwisterInt32(const mersenneTwisterInt32&);
                /*!
                Make copy constructor and assignment operator unavailable, they don't make sense.
                Assignment operator not defined.
                */
                void operator=(const mersenneTwisterInt32&);
        };

        /*!
        Generates double floating point numbers in the half-open interval [0, 1).
        */
        class mersenneTwister : public mersenneTwisterInt32
        {
            public:
                mersenneTwister() : mersenneTwisterInt32() {}
                mersenneTwister(unsigned long seed) : mersenneTwisterInt32(seed) {}
                mersenneTwister(const unsigned long* seed, int size) : mersenneTwisterInt32(seed, size) {}
                ~mersenneTwister() {}
                double operator()()
                {
                    return static_cast<double>(rand_int32()) * (1. / 4294967296.);  //Divided by 2^32
                }

            private:
                /*!
                Copy constructor not defined.
                */
                mersenneTwister(const mersenneTwister&);
                /*!
                Assignment operator not defined.
                */
                void operator=(const mersenneTwister&);
        };

        /*!
        Generates double floating point numbers in the closed interval [0, 1].
        */
        class mersenneTwisterClosed : public mersenneTwisterInt32
        {
            public:
                mersenneTwisterClosed() : mersenneTwisterInt32() {}
                mersenneTwisterClosed(unsigned long seed) : mersenneTwisterInt32(seed) {}
                mersenneTwisterClosed(const unsigned long* seed, int size) : mersenneTwisterInt32(seed, size) {}
                ~mersenneTwisterClosed() {}
                double operator()()
                {
                    return static_cast<double>(rand_int32()) * (1. / 4294967295.);  //Divided by 2^32 - 1
                }

            private:
                /*!
                Copy constructor not defined.
                */
                mersenneTwisterClosed(const mersenneTwisterClosed&);
                /*!
                Assignment operator not defined.
                */
                void operator=(const mersenneTwisterClosed&);
        };

        /*!
        Generates double floating point numbers in the open interval (0, 1).
        */
        class mersenneTwisterOpen : public mersenneTwisterInt32
        {
            public:
                mersenneTwisterOpen() : mersenneTwisterInt32() {}
                mersenneTwisterOpen(unsigned long seed) : mersenneTwisterInt32(seed) {}
                mersenneTwisterOpen(const unsigned long* seed, int size) : mersenneTwisterInt32(seed, size) {}
                ~mersenneTwisterOpen() {}
                double operator()()
                {
                    return (static_cast<double>(rand_int32()) + .5) * (1. / 4294967296.);  //Divided by 2^32
                }

            private:
                /*!
                Copy constructor not defined.
                */
                mersenneTwisterOpen(const mersenneTwisterOpen&);
                /*!
                Assignment operator not defined.
                */
                void operator=(const mersenneTwisterOpen&);
        };

        /*!
        Generates 53 bit resolution doubles in the half-open interval [0, 1).
        */
        class mersenneTwister53 : public mersenneTwisterInt32
        {
            public:
                mersenneTwister53() : mersenneTwisterInt32() {}
                mersenneTwister53(unsigned long seed) : mersenneTwisterInt32(seed) {}
                mersenneTwister53(const unsigned long* seed, int size) : mersenneTwisterInt32(seed, size) {}
                ~mersenneTwister53() {}
                double operator()()
                {
                    return (static_cast<double>(rand_int32() >> 5) * 67108864. + static_cast<double>(rand_int32() >> 6)) * (1. / 9007199254740992.);
                }

            private:
                /*!
                Copy constructor not defined.
                */
                mersenneTwister53(const mersenneTwister53&);
                /*!
                Assignment operator not defined.
                */
                void operator=(const mersenneTwister53&);
        };
};

#endif // RANDOM_H
