/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <QTextStream>
#include <QFile>
#include "fileformat.h"
#include "mainwindow.h"
#include "globals.h"

void fileFormat::saveFile::saveToFile(QString fileName, bool compressed)
{
    QFile file(fileName);
    if (file.open(QFile::WriteOnly | QFile::Truncate))
    {
        QTextStream out(&file);
        //out.setCodec("UTF-8");
        out << this->toString(compressed);
        out.flush();
        file.close();
    }
    else
    {
        mainWindow::reportError(qApp->translate("global", "Error: cannot open file for writing: %1").arg(fileName));
    }
}

QString fileFormat::saveFile::toString(bool compressed) const
{
    QString str;

    QXmlStreamWriter xml(&str);
    xml.setAutoFormatting(true);
    xml.setAutoFormattingIndent(4);
    xml.writeStartDocument();
    xml.writeDTD("<!DOCTYPE saveFile>");
    xml.writeStartElement("saveFile");
    xml.writeAttribute("version", FILEFORMAT_VERSION);

    //Write elements
    for (int i = 0; i < this->items.size(); i++)
    {
        xml.writeStartElement(this->items[i].getKeyAsString());
        QMapIterator<QString, QString> it(this->items[i].getAllAttributes());
        while (it.hasNext())
        {
            it.next();
            xml.writeAttribute(it.key(), it.value());
        }
        xml.writeCharacters(this->items[i].getValueAsString());
        xml.writeEndElement();
    }

    xml.writeEndDocument();

    if (compressed)
    {
        QByteArray compressedData;
        QDataStream outData(&compressedData, QIODevice::ReadWrite);
        outData << str;
        compressedData = qCompress(compressedData, 9);
        return QString::fromLatin1(compressedData.toHex());
    }
    return str;
}

fileFormat::saveFile::saveFile()
{
}

void fileFormat::saveFile::clear()
{
    this->items.clear();
}

qint64 fileFormat::saveFile::itemCount() const
{
    return this->items.size();
}

uint fileFormat::saveFile::getHash() const
{
    QString str = this->toString(false);
    QStringList data;
    if (str.indexOf(lineEnd) != -1)
    {
        data = str.split(lineEnd);
    }
    else
    {
        data = str.split("\n");
    }
    data.sort();
    return qHash(data.join(""));
}

bool fileFormat::saveFile::loadFromFile(QString fileName, bool compressed)
{
    QFile file(fileName);
    if (file.open(QFile::ReadOnly))
    {
        QTextStream in(&file);
        //in.setCodec("UTF-8");
        if (!this->fromString(in.readAll(), compressed)) return false;
        file.close();
    }
    else
    {
        mainWindow::reportError(qApp->translate("global", "Error: cannot open file for reading: %1").arg(fileName));
        return false;
    }
    return true;
}

bool fileFormat::saveFile::fromString(QString str, bool compressed)
{
    if (compressed)
    {
        QByteArray compressedData = QByteArray::fromHex(str.toLatin1());
        compressedData = qUncompress(compressedData);
        if (!compressedData.size())
        {
            mainWindow::reportError(qApp->translate("global", "Error: invalid file"));
            return false;
        }
        QDataStream stream(&compressedData, QIODevice::ReadOnly);
        str.clear();
        stream >> str;
    }
    QXmlStreamReader xml(str);
    xml.readNextStartElement();
    if (!(xml.isStartElement() && xml.name() == QStringLiteral("saveFile") && xml.attributes().value("version") == QStringLiteral(FILEFORMAT_VERSION)))
    {
        mainWindow::reportError(qApp->translate("global", "Error: invalid file or wrong file version"));
        return false;
    }
    else
    {
        this->clear();
        while (!xml.atEnd())
        {
            xml.readNext();
            if (!xml.name().isEmpty() && xml.name() != QStringLiteral("saveFile"))
            {
                this->items.resize(this->items.size() + 1);
                this->items[this->items.size() - 1].setStringKey(xml.name().toString());
                for (int i = 0; i < xml.attributes().size(); i++)
                {
                    this->items[this->items.size() - 1].setStringAttribute(xml.attributes().at(i).name().toString(), xml.attributes().at(i).value().toString());
                }
                this->items[this->items.size() - 1].setStringValue(xml.readElementText());
                this->items[this->items.size() - 1].setParent(this);
            }
        }
        if (xml.hasError())
        {
            mainWindow::reportError(qApp->translate("global", "XML error: %1").arg(xml.errorString()));
            return false;
        }
    }
    return true;
}

fileFormat::saveFileItem::saveFileItem()
{
    this->value = "";
    this->key = "";
    this->attributes.clear();
    this->p = nullptr;
}

fileFormat::saveFileItem::saveFileItem(QString _key, QString _value, QMap<QString, QString> _attributes, fileFormat::saveFile* parent)
{
    this->value = _value;
    this->key = _key;
    this->attributes = _attributes;
    this->p = parent;
}

QString fileFormat::saveFileItem::getValueAsString() const
{
    return this->value;
}

QString fileFormat::saveFileItem::getKeyAsString() const
{
    return this->key;
}

void fileFormat::saveFileItem::setStringAttribute(QString attr, QString value)
{
    this->attributes[attr] = value;
}

void fileFormat::saveFileItem::setStringKey(QString _key)
{
    this->key = _key;
}

void fileFormat::saveFileItem::setStringValue(QString _value)
{
    this->value = _value;
}

fileFormat::saveFileItem fileFormat::saveFile::getItemByKey(QString key) const
{
    for (int i = 0; i < this->items.size(); i++)
    {
        if (items[i].getKeyAsString() == key)
            return items[i];
    }
    return fileFormat::saveFileItem();
}

fileFormat::saveFileItem fileFormat::saveFile::getItemByIndex(quint64 index) const
{
    return this->items[index];
}

void fileFormat::saveFile::addItem(QString key, QString value, QMap<QString, QString> attributes)
{
    this->items.resize(this->items.size() + 1);
    this->items[this->items.size() - 1].setStringKey(key);
    this->items[this->items.size() - 1].setStringValue(value);
    this->items[this->items.size() - 1].setParent(this);
    QMapIterator<QString, QString> it(attributes);
    while (it.hasNext())
    {
        it.next();
        this->items[this->items.size() - 1].setStringAttribute(it.key(), it.value());
    }
}

void fileFormat::saveFile::addItem(saveFileItem item)
{
    this->addItem(item.getKeyAsString(), item.getRawValueAsString(), item.getAllRawAttributes());
}

void fileFormat::saveFile::addItems(saveFile f)
{
    for (auto& it : f.getItems())
    {
        this->addItem(it);
    }
}

QMap<QString, QString> fileFormat::saveFileItem::getAllAttributes() const
{
    return this->attributes;
}

bool fileFormat::saveFile::isContentEqual(fileFormat::saveFile* a, fileFormat::saveFile* b)
{
    return a->toString(false) == b->toString(false);
}

QList<fileFormat::saveFileItem> fileFormat::saveFile::getItems() const
{
    return items;
}

QString fileFormat::saveFileItem::getAttributeAsString(QString attr) const
{
    return this->getAllAttributes().value(attr, {});
}

QString fileFormat::saveFileItem::getRawAttributeAsString(QString attr) const
{
    return this->getAllRawAttributes().value(attr, {});
}

void fileFormat::saveFileItem::setParent(fileFormat::saveFile* parent)
{
    this->p = parent;
}

QString fileFormat::saveFileItem::getRawValueAsString() const
{
    return this->value;
}

QMap<QString, QString> fileFormat::saveFileItem::getAllRawAttributes() const
{
    return this->attributes;
}
