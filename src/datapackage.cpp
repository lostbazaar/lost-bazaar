/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QDataStream>
#include "datapackage.h"
#include "globals.h"

network::networkDataPackage::networkDataPackage()
{
    this->data.clear();
    this->msgType = network::MessageType::InvalidMessage;
}

QByteArray network::networkDataPackage::serialize() const
{
    QStringList tmpList = this->data;
    tmpList << QString::number(static_cast<int>(this->msgType));
    tmpList << NETWORK_PROTOCOL_VERSION;

    QByteArray compressedData;
    QDataStream outData(&compressedData, QIODevice::WriteOnly);
    outData << tmpList;
    //compressedData = qCompress(compressedData);

    return compressedData;
}

void network::networkDataPackage::unserialize(QByteArray _data)
{
    this->data.clear();
    //_data = qUncompress(_data);

    if (_data.size())
    {
        QDataStream in(&_data, QIODevice::ReadOnly);
        in >> this->data;

        if (this->data.size() > 1)
        {
            if (this->data.back() == NETWORK_PROTOCOL_VERSION)
            {
                this->data.removeLast();
                this->msgType = MessageType(this->data.back().toInt());
                this->data.removeLast();
            }
            else
            {
                this->msgType = MessageType::InvalidMessage;
            }
        }
        else
        {
            this->msgType = MessageType::InvalidMessage;
        }
    }
    else
    {
        this->msgType = MessageType::InvalidMessage;
    }
}

void network::networkDataPackage::setStrings(QStringList strList)
{
    this->data = strList;
}

QString network::networkDataPackage::getStringAt(int i) const
{
    return this->data.at(i);
}

network::MessageType network::networkDataPackage::getType() const
{
    return this->msgType;
}

void network::networkDataPackage::setType(MessageType newMsgType)
{
    this->msgType = newMsgType;
}

QString network::networkDataPackage::toDebugString() const
{
    QString res(QString::number(static_cast<int>(this->msgType)));
    res.append(" ");
    res.append(this->data.join(","));
    return res;
}

QStringList network::networkDataPackage::getData() const
{
    return this->data;
}

void network::networkDataPackage::addRelayTargetAddress(const QString& t, const QString& gId, const QString& sender)
{
    this->data.push_front(sender);
    this->data.push_front(t);
    this->data.push_front(gId);
}

void network::networkDataPackage::eraseRelayData()
{
    this->data.removeAt(0);
    this->data.removeAt(0);
    this->data.removeAt(0);
}

int network::networkDataPackage::length() const
{
    return this->data.length();
}

network::networkDataPackage::networkDataPackage(network::MessageType type, QStringList strList)
{
    this->data.clear();
    this->data = strList;
    this->msgType = type;
}

network::networkDataPackage::networkDataPackage(QByteArray _data)
{
    this->data.clear();
    this->msgType = network::MessageType::InvalidMessage;
    this->unserialize(_data);
}
