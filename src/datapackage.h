/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QString>
#include <QStringList>
#include <QByteArray>
#include <QObject>

namespace network
{
    enum class MessageType
    {
        InvalidMessage,
        InitialMessage,
        DisconnectedFromGame,
        LobbyChatMessage,
        LobbyUpdate,
        GameEvent,
        UserConnected,
        RelayMsg
    };

    /*!
    Object for storing a single network message
    */
    class networkDataPackage
    {
        public:
            networkDataPackage();
            /*!
            Constructor - the second parameter is the message data
            */
            networkDataPackage(network::MessageType type, QStringList strList);
            /*!
            Constructor - unserializes the given (serialized networkDataPackage) data
            */
            networkDataPackage(QByteArray _data);
            /*!
            Serialize the current instance
            */
            QByteArray serialize() const;
            /*!
            Unserialize a serialized networkDataPackage and set the recovered data as the current data
            */
            void unserialize(QByteArray _data);
            /*!
            Returns with the stored string data
            */
            QString getStringAt(int i) const;
            /*!
            Sets the stored string data
            */
            void setStrings(QStringList strList);
            /*!
            Returns with the type of the message
            */
            network::MessageType getType() const;
            /*!
            Sets the type of the message
            */
            void setType(network::MessageType newMsgType);
            QString toDebugString() const;
            QStringList getData() const;
            void addRelayTargetAddress(const QString& t, const QString& gId, const QString& sender);
            void eraseRelayData();
            int length() const;

        private:
            QStringList data;
            network::MessageType msgType;
    };
}
