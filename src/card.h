/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CARD_H
#define CARD_H

#include <QObject>
#include <QString>
#include <QList>
#include <QGraphicsItemGroup>
#include <QGraphicsPixmapItem>
#include <QGraphicsSceneHoverEvent>
#include <ctime>
#include "mainwindow.h"
#include "fileformat.h"
#include "cfgfile.h"

enum class DrawingType
{
    Full,
    Inventory,
    InGame
};

enum class CardInventoryOverlay
{
    NoOverlay = -1,
    TransparentOverlay = 0,
    PlayerChoiceOverlay = 1,
    DisabledOverlay = 2,
};

class graphicsPixmapItem : public QObject, public QGraphicsPixmapItem
{
        Q_OBJECT

    public:
        graphicsPixmapItem();
        ~graphicsPixmapItem();

    protected:
        virtual void mousePressEvent(QGraphicsSceneMouseEvent* ev) override;
        virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* ev) override;
        virtual void hoverEnterEvent(QGraphicsSceneHoverEvent* event) override;
        virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent* event) override;
        virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;

    signals:
        void clicked();
        void rightClicked();
        void mouseHeldDown();
        void mouseEntered();
        void mouseLeft();
        void startDrag();

    private:
        QTimer* pTimer = nullptr;
        QPointF dragStartPosition;
};

class graphicsItemGroup : public QObject, public QGraphicsItemGroup
{
        Q_OBJECT
        Q_PROPERTY(qreal scale READ scale WRITE setScale NOTIFY scaleChanged)

    public:
        graphicsItemGroup();
        ~graphicsItemGroup();
        qreal scale()
        {
            return QGraphicsItemGroup::scale();
        }
        void setScale(qreal sc)
        {
            QGraphicsItemGroup::setScale(sc);
            emit scaleChanged(sc);
        }

    protected:
        virtual void mousePressEvent(QGraphicsSceneMouseEvent* ev) override;
        virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* ev) override;
        virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* ev) override;
        virtual void hoverEnterEvent(QGraphicsSceneHoverEvent* event) override;
        virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent* event) override;

    signals:
        void clicked();
        void rightClicked();
        void mouseHeldDown();
        void mouseEntered();
        void mouseLeft();
        void startDrag();
        void scaleChanged(qreal scale);

    private:
        QTimer* pTimer = nullptr;
        QPointF dragStartPosition;
};

class card : public QObject
{
        Q_OBJECT

    public:
        card(const QString& _id, int player, gameState* gS);
        card(const QString& _id, int player, gameState* gS, int globalUID);
        card(fileFormat::saveFileItem it, gameState* gS);
        /*!
        Draws the card on the given scene, at the given position using the given drawing type
        */
        void draw(QGraphicsScene* scene, DrawingType t, QPointF pos, bool do_not_save_details = false, QString cardPictureSet = QString(), QString namePictureSet = QString(), QString forceLang = QString(), bool inGameDnD = false);
        QImage renderToPixmap(DrawingType t, QString forceLang = QString(), QString cardPictureSet = QString(), QString namePictureSet = QString(), bool inGameDnD = false);
        /*!
        Redraws the card with the last used scene, position, drawing type
        */
        void redraw();
        /*!
        Used in the inventory
        */
        void setSelectedForPlacement(bool sel);
        /*!
        Used in the inventory
        */
        void setMarkedForTrash(bool marked);
        /*!
        Used in the inventory
        */
        void setMarkedForHuckster(bool marked);
        /*!
        Used in the inventory
        */
        bool isMarkedForHuckster() const;
        void setHighlightedInInventory(bool h);
        void setHighlightedOnBoard(bool h);
        void deleteCardLater();
        void setDisabledInInventory(bool d);
        bool isDisabledInInventory() const;
        void moveOnBoardTo(const QPoint& target, bool async = false);
        void animateBoardPlacement(QGraphicsScene* sc);
        void animateSilverAction();
        void scheduleRedraw(QGraphicsScene* scene, DrawingType t, QPointF pos, bool do_not_save_details, QString cardPictureSet, QString namePictureSet);
        void scheduleRemoveFromScene(QGraphicsScene* scene);
        void redrawAsNeeded();
        void removeFromSceneAsNeeded();
        void animateRemovalFromBoard(QGraphicsScene* sc);
        /*!
        Removes all graphicsitems of this card from the given scene
        */
        void removeFromScene(QGraphicsScene* scene);
        /*!
        Rotates the card to the given player's orientation
        Changes ownership of the card
        */
        Q_SCRIPTABLE void changeOwner(int playerNum);
        /*!
        Changes ownership of the card, does not do any rotation or redrawing
        */
        void setPlayer(int playerNum);
        /*!
        Returns the unique identifier of this card
        */
        Q_SCRIPTABLE int getUID() const;
        /*!
        Returns the current counter value on the card
        */
        Q_SCRIPTABLE int getCounter() const;
        /*!
        Returns true if the card is marked for trash (in the inventory)
        */
        Q_SCRIPTABLE bool isMarkedForTrash() const;
        /*!
        Returns the ID (type) of the card
        */
        Q_SCRIPTABLE QString getID() const;
        /*!
        Sets the counter value on the card
        */
        Q_SCRIPTABLE void setCounter(int c);
        /*!
        Returns the position of the card (on the board)
        */
        Q_SCRIPTABLE QPoint getPosition() const;
        /*!
        Returns the X position of the card (on the board)
        */
        Q_SCRIPTABLE int getPosX() const;
        /*!
        Returns the Y position of the card (on the board)
        */
        Q_SCRIPTABLE int getPosY() const;
        /*!
        Returns the owner of the card
        */
        Q_SCRIPTABLE int getPlayer() const;
        /*!
        Returns the icons of this card
        */
        Q_SCRIPTABLE QStringList getIcons() const;
        /*!
        Returns true if the card has the icon given as parameter
        */
        Q_SCRIPTABLE bool hasIcon(const QString& icon) const;
        /*!
        Returns the base value of the card
        */
        Q_SCRIPTABLE int getBaseValue() const;
        /*!
        Returns the (localized) name of the card
        */
        Q_SCRIPTABLE QString getName() const;
        /*!
        Returns the background of the card.
        */
        Q_SCRIPTABLE QString getBackground() const;
        /*!
        Returns the current value of the card (executes the getCurrentValue javascript function)
        */
        Q_SCRIPTABLE int getCurrentValue();
        /*!
        Sets whether the card picture is shown in grayscale or not
        */
        Q_SCRIPTABLE void setGrayScale(bool grayScale, bool disableAnimation = false);
        /*!
        Sets whether the card picture is shown as full size or not
        */
        Q_SCRIPTABLE void setShowFullSize(bool fullSize, bool disableAnimation = false);
        /*!
        Returns whether the card picture is shown as full size or not
        */
        Q_SCRIPTABLE bool isFullSize() const;
        /*!
        Returns whether the card picture is shown as grayscale or not
        */
        Q_SCRIPTABLE bool isGrayScale() const;
        const QString& getScriptCode() const;
        void setPosition(const QPoint& p);
        void removeFromPrevScene();
        void setInventoryOverlay(CardInventoryOverlay overlay);
        /*!
        The second parameter specifies whether the rotation is only temporary (for easier viewing of the cards on board)
        */
        void rotate(int playerNum);
        bool isBeingAnimated() const;
        bool hasSilverAction() const;
        bool hasEvent(const QString& evName) const;
        fileFormat::saveFile save(bool forOOSCheck = false) const;
        static int globalUID;
        card* clone(gameState* newState) const;
        static QMap<QString, cfgFile> cardData;
        static QMap<QString, QString> cardCode;
        static QMap<int, card*> currentCards;
        static QMap<QString, QSet<QString>> cardEvs;
        ~card();

    signals:
        /*!
        Left mouse click
        */
        void clicked();
        /*!
        Emitted if the mouse is held down on the card for a predefined amount of time
        */
        void mouseHeldDown();
        void rightMouseClicked();
        void mouseEntered();
        void mouseLeft();
        void startDrag();
        void moveAnimationFinished();
        void boardRemovalAnimFinished();
        void silverActionAnimationFinished();

    private:
        QGraphicsScene* scheduledRemoveFromScene = nullptr;
        QGraphicsScene* scheduledDrawScene = nullptr;
        DrawingType scheduledDrawingType;
        QPointF scheduledPos;
        bool scheduledDoNotSaveDetails = false;
        QString scheduledCardPictureSet;
        QString scheduledNamePictureSet;
        QString id;
        void setZValue(int v);
        QTimer* rotationTimer = nullptr;
        int counter = 0;
        bool tmpRot = false;
        int oldRot;
        bool selectedForPlacement = false;
        bool markedForTrash = false;
        bool markedForHuckster = false;
        bool inventoryHighlight = false;
        bool boardHighlight = false;
        bool prevDrawn;
        bool animInProgress = false;
        bool disabled = false;
        QGraphicsScene* prevScene;
        QGraphicsPixmapItem* cardImgOnBoard = nullptr;
        QPoint pos;
        QPointF prevPos;
        DrawingType prevDrawingType;
        int uid = 0;
        int playerNum = -1;
        int val = 0;
        bool _grayScale = false;
        bool _fSize = true;
        QList<QGraphicsPixmapItem*> pixmapItems;
        graphicsPixmapItem* inventoryHighlightItem = nullptr;
        graphicsPixmapItem* boardHighlightItem = nullptr;
        graphicsItemGroup* itemGroup = nullptr;
        int currRotStep;
        int rotSteps;
        int rotDegree;
        bool hasSilver = false;
        gameState* gS;
        CardInventoryOverlay overlay;
        void init(const QString& _id, int player, gameState* gS, int globalUID = -1);

    private slots:
        void rotationStep();
};

#endif // CARD_H
