/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef AUDIO_H
#define AUDIO_H

#include <QObject>
#include <QString>

class QMediaPlaylist;
class QMediaPlayer;
class QTimer;

class musicPlayer : public QObject
{
        Q_OBJECT

    public:
        musicPlayer();
        void loadPlayList(const QString& filename);
        void playFileThenContinue(const QString& filename);
        void shuffle();
        void setupSignals();
        /*!
        The music player waits a random amount of time between tracks.
        This sets the maximal value (in msecs) of the silence.
        Set to -1 to disable.
        */
        void setMaxRandomSilence(int max);
        ~musicPlayer();

    public slots:
        void start();
        void stop();

    private:
        QMediaPlayer* player = nullptr;
        QMediaPlaylist* playList = nullptr;
        QTimer* nextTrackTimer = nullptr;
        int maxRand = 0;
        int savedIdx = 0;
};

#endif // AUDIO_H
