/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QTcpServer>
#include <QCoreApplication>
#include <QTcpSocket>
#include <QDataStream>
#include <QFile>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonObject>
#include <QRandomGenerator>
#include <QTimer>
#include <cstdlib>
#include "datapackage.h"
#include "globals.h"

bool logToStdOut = false;

struct gameInfo
{
    QString name;
    int64_t startTime = 0;
    QString username;
    QString desc;
    QString gameID;
    QString password;
    QString status;
    int64_t keepAliveTime = 0;
    QString secretKey;
    bool tournament = false;
    bool openForAnyone = false;
    QString board;
    QString deck;
    int gameMode = -1;
    bool passwordRequired = false;
};

struct userInfo
{
    QString gameID;
    QString username;
};

struct connectionReadStatusInfo
{
    bool blockSizeAlreadyRead = false;
    qint64 currentBlockSize = 0;
    QTcpSocket* user = nullptr;
};

static QList<gameInfo> games;
static QHash<QString, int> randomStrings;
static QString logPath;
static QString adminKey;
static QList<QPair<QHostAddress, int>> banList;
static QString banListFilename;
static QHash<qintptr, connectionReadStatusInfo> currentBlockSizes;

class networkManager : public QObject
{
        Q_OBJECT

    public:
        networkManager() {}
        virtual ~networkManager() {}

    private:
        QVector<QPair<QTcpSocket*, userInfo>> connectedUsers;

    public slots:
        void handleNewConnection();
        void handleNewMessage();
        void clearClientConnections();
        void notifyUserDisconnection(const QString& gid, const QString& uname);
        void sendMessage(QTcpSocket* s, network::networkDataPackage package);
        bool checkBanList(QTcpSocket* s);
        void removeGame(const QString& gameID);
};

#include "server_main.moc"

void logMsg(const QString& msg)
{
    if (logToStdOut)
    {
        QTextStream out(stdout);
        out << "[TIME: " << time(nullptr) << "] " << msg << "\n";
    }
    else
    {
        QFile f(logPath);
        if (f.open(QFile::Append))
        {
            QTextStream out(&f);
            out << "[TIME: " << time(nullptr) << "] " << msg << "\n";
            f.flush();
            f.close();
        }
        else
        {
            QTextStream out(stdout);
            out << "[TIME: " << time(nullptr) << "] " << "FATAL: Cannot open logfile" << "\n";
        }
    }
}

void writeBanList()
{
    QFile f{banListFilename};
    if(f.open(QFile::Truncate|QFile::WriteOnly))
    {
        QTextStream out(&f);
        for (const auto& ban: banList)
        {
            if(ban.second == 32)
            {
                out << ban.first.toString() << "\n";
            } else
            {
                out << ban.first.toString()+"/"+QString::number(ban.second) << "\n";
            }
        }
        out.flush();
        f.close();
    }
}

QStringList getGameData()
{
    QStringList res;
    for (const auto& game : games)
    {
        res << game.name << QString::number(game.startTime) << game.username << game.desc << game.gameID << game.status <<
            QString::number(game.tournament) <<
            QString::number(game.openForAnyone) <<
            game.board <<
            game.deck <<
            QString::number(game.gameMode) <<
            QString::number(game.passwordRequired);
    }
    return res;
}

QString getRandomString()
{
    const QString possibleCharacters("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
    const int randomStringLength = 16;

    QString randomString;
    for (int i = 0; i < randomStringLength; ++i)
    {
        int index = QRandomGenerator::global()->generate() % possibleCharacters.length();
        QChar nextChar = possibleCharacters.at(index);
        randomString.append(nextChar);
    }

    if (randomStrings.count(randomString))
    {
        logMsg("Random string generator: random string already used, generating new one");
        return getRandomString();
    }
    else
    {
        randomStrings.insert(randomString, 0);
    }
    return randomString;
}

bool networkManager::checkBanList(QTcpSocket* socket)
{
    if (socket)
    {
        for (const auto& ban : banList)
        {
            if (socket->peerAddress().isInSubnet(ban))
            {
                socket->disconnectFromHost();
                socket->close();
                return true;
            }
        }
    }
    else
    {
        for (const auto& usr : this->connectedUsers)
        {
            if(usr.first)
            {
                if(checkBanList(usr.first))
                {
                    if(usr.second.gameID != "gameid") removeGame(usr.second.gameID);
                }
            }
        }
        clearClientConnections();
    }
    return false;
}

void networkManager::removeGame(const QString &gameID)
{
    for (int i = 0; i < games.size(); ++i)
    {
        if (games[i].gameID == gameID)
        {
            games.removeAt(i);
            break;
        }
    }
    auto fullID = "gameid"+gameID;
    for (int i = 0; i < connectedUsers.size(); i++)
    {
        if(connectedUsers[i].first && connectedUsers[i].second.gameID == fullID)
        {
            connectedUsers[i].first->disconnectFromHost();
            connectedUsers[i].first->close();
        }
    }
}

void networkManager::handleNewConnection()
{
    QTcpSocket* socket = static_cast<QTcpServer*>(QObject::sender())->nextPendingConnection();
    if (checkBanList(socket))
    {
        socket->disconnectFromHost();
        socket->close();
        return;
    }

    this->connectedUsers.append(QPair<QTcpSocket*, userInfo>(socket, userInfo{}));
    logMsg("User connected from address: " + socket->peerAddress().toString());

    connect(socket, &QTcpSocket::readyRead, this, &networkManager::handleNewMessage);
    connect(socket, &QAbstractSocket::errorOccurred, this, [this, socket](QAbstractSocket::SocketError err)
    {
        logMsg("User socket error: " + QString::number(err));
        socket->disconnectFromHost();
        this->clearClientConnections();
    });
    connect(socket, &QTcpSocket::disconnected, this, [this]()
    {
        logMsg("User socket disconnected");
        this->clearClientConnections();
    });
}

void clearCurrBlockSize(QTcpSocket* remove = nullptr)
{
    auto keys = currentBlockSizes.keys();
    for (auto sock : keys)
    {
        auto ptr = currentBlockSizes[sock].user;
        if (!ptr || ptr == remove || ptr->state() == QAbstractSocket::ClosingState || ptr->state() == QAbstractSocket::UnconnectedState)
        {
            currentBlockSizes.remove(sock);
        }
    }
}

void networkManager::clearClientConnections()
{
    logMsg("Clearing client connections");
    for (int i = this->connectedUsers.size() - 1; i >= 0; --i)
    {
        if (this->connectedUsers[i].first->state() == QAbstractSocket::ClosingState || this->connectedUsers[i].first->state() == QAbstractSocket::UnconnectedState)
        {
            logMsg("Removing user: " + this->connectedUsers[i].second.username);
            disconnect(this->connectedUsers[i].first, nullptr, nullptr, nullptr);
            auto gid = this->connectedUsers[i].second.gameID;
            auto uname = this->connectedUsers[i].second.username;
            clearCurrBlockSize(this->connectedUsers[i].first);
            this->connectedUsers[i].first->deleteLater();
            this->connectedUsers.remove(i);
            this->notifyUserDisconnection(gid, uname);
            logMsg("User removed!");
        }
    }
}

void networkManager::notifyUserDisconnection(const QString& gid, const QString& uname)
{
    logMsg("Notifying others that user disconnected: " + uname + ", " + gid);
    network::networkDataPackage pkg(network::MessageType::RelayMsg, QStringList() << "userDisconnected" << uname);
    for (const auto& p : this->connectedUsers)
    {
        if (p.second.gameID == gid)
        {
            this->sendMessage(p.first, pkg);
        }
    }
    logMsg("Notification done");
}

QString getGamePassword(const QString& id)
{
    for (const auto& g : games)
    {
        if ("gameid" + g.gameID == id) return g.password;
    }
    return {};
}

void networkManager::handleNewMessage()
{
    clearCurrBlockSize();

    auto socket = dynamic_cast<QTcpSocket*>(QObject::sender());
start:
    if (!socket) return;

    QByteArray receivedMessage;
    QDataStream in(socket);
    in.setVersion(QDATASTREAM_VERSION);

    auto s_id = socket->socketDescriptor();

    if (!currentBlockSizes.count(s_id))
    {
        currentBlockSizes[s_id].user = socket;
    }

    if (in.status() == QDataStream::ReadCorruptData)
    {
        currentBlockSizes.remove(s_id);
        return;
    }

    if (currentBlockSizes[s_id].blockSizeAlreadyRead)
    {
        logMsg("Blocksize already read, waiting for content");
        if (socket->bytesAvailable() < currentBlockSizes[s_id].currentBlockSize) return;
        in >> receivedMessage;
        if (in.status() == QDataStream::ReadCorruptData)
        {
            currentBlockSizes.remove(s_id);
            return;
        }
        currentBlockSizes.remove(s_id);
        logMsg("Content read");
    }
    else
    {
        logMsg("Waiting to read blocksize");
        if (socket->bytesAvailable() < static_cast<qint64>(sizeof(qint64))) return;
        in >> currentBlockSizes[s_id].currentBlockSize;
        if (in.status() == QDataStream::ReadCorruptData)
        {
            currentBlockSizes.remove(s_id);
            return;
        }
        currentBlockSizes[s_id].blockSizeAlreadyRead = true;
        logMsg("Blocksize read");

        logMsg("Blocksize already read, waiting for content");
        if (socket->bytesAvailable() < currentBlockSizes[s_id].currentBlockSize) return;
        in >> receivedMessage;
        if (in.status() == QDataStream::ReadCorruptData)
        {
            currentBlockSizes.remove(s_id);
            return;
        }
        currentBlockSizes.remove(s_id);
        logMsg("Content read");
    }

    network::networkDataPackage p(receivedMessage);
    if (p.getType() == network::MessageType::InvalidMessage)
    {
        logMsg("Invalid message received");
        if (socket->bytesAvailable()) goto start;
        return;
    }

    logMsg(QString("Message received from %1: %2").arg(socket->peerAddress().toString(), p.toDebugString()));

    if (p.length() == 1 && p.getStringAt(0) == "gamelist" && p.getType() == network::MessageType::RelayMsg)
    {
        network::networkDataPackage msg(network::MessageType::RelayMsg, QStringList() << "publicLobbyData" << getGameData());
        this->sendMessage(socket, msg);
    }
    else if (p.length() == 2 && p.getStringAt(0) == "setgameongoing" && p.getType() == network::MessageType::RelayMsg)
    {
        logMsg("Setting game status to ongoing");
        for (int i = 0; i < games.size(); ++i)
        {
            if (games[i].secretKey == p.getStringAt(1))
            {
                games[i].keepAliveTime = time(nullptr);
                games[i].status = "ongoing";
                break;
            }
        }
    }
    else if (p.length() == 5 && p.getStringAt(0) == "updategamedata" && p.getType() == network::MessageType::RelayMsg)
    {
        logMsg("Updating game data");
        if (p.getStringAt(2).length() <= 200 && p.getStringAt(3).length() <= 200)
        {
            for (int i = 0; i < games.size(); ++i)
            {
                if (games[i].secretKey == p.getStringAt(1))
                {
                    games[i].keepAliveTime = time(nullptr);
                    games[i].board = p.getStringAt(2);
                    games[i].deck = p.getStringAt(3);
                    games[i].gameMode = p.getStringAt(4).toInt();
                    break;
                }
            }
        }
    }
    else if (p.length() == 2 && p.getStringAt(0) == "keepalive" && p.getType() == network::MessageType::RelayMsg)
    {
        for (int i = 0; i < games.size(); ++i)
        {
            if (games[i].secretKey == p.getStringAt(1))
            {
                logMsg("Game keepalive, secret key: " + p.getStringAt(1));
                games[i].keepAliveTime = time(nullptr);
                break;
            }
        }
    }
    else if (p.length() == 2 && p.getStringAt(0) == "removegame" && p.getType() == network::MessageType::RelayMsg)
    {
        logMsg("Removing game");
        for (int i = 0; i < games.size(); ++i)
        {
            if (games[i].secretKey == p.getStringAt(1))
            {
                games.removeAt(i);
                break;
            }
        }
    }
    else if (p.length() == 3 && p.getStringAt(0) == "removegameadmin" && p.getType() == network::MessageType::RelayMsg)
    {
        logMsg("Removing game [ADMIN ACTION]");
        if (p.getStringAt(2) == adminKey)
        {
            removeGame(p.getStringAt(1));
            clearClientConnections();
        }
    }
    else if (p.length() == 3 && p.getStringAt(0) == "unban" && p.getType() == network::MessageType::RelayMsg)
    {
        logMsg("Unbanning [ADMIN ACTION]");
        if(p.getStringAt(2) == adminKey)
        {
            auto addr = QHostAddress::parseSubnet(p.getStringAt(1).trimmed());
            banList.removeAll(addr);
            writeBanList();
        }
    }
    else if (p.length() == 3 && p.getStringAt(0) == "ban" && p.getType() == network::MessageType::RelayMsg)
    {
        logMsg("Banning [ADMIN ACTION]");
        if(p.getStringAt(2) == adminKey)
        {
            auto addr = QHostAddress::parseSubnet(p.getStringAt(1).trimmed());
            if(!banList.contains(addr)) banList.append(addr);
            writeBanList();
        }
    }
    else if (p.length() == 2 && p.getStringAt(0) == "playerinfo" && p.getType() == network::MessageType::RelayMsg)
    {
        logMsg("Player info [ADMIN ACTION]");
        if(p.getStringAt(1) == adminKey)
        {
            QString info;
            for(const auto& game : games)
            {
                info.append("Game ID:"+game.gameID+" Title:"+game.name+" User:"+game.username+"\n");
            }
            info.append("\n\n");
            for (const auto& usr : this->connectedUsers)
            {
                info.append("Username:"+usr.second.username+" Game ID:"+usr.second.gameID+" Address:"+(usr.first?usr.first->peerAddress().toString():"")+"\n");
            }
            info.append("\n\n");
            for (const auto& ban: banList)
            {
                if(ban.second == 32)
                {
                    info.append(ban.first.toString()+"\n");
                } else
                {
                    info.append(ban.first.toString()+"/"+QString::number(ban.second)+"\n");
                }
            }
            network::networkDataPackage msg(network::MessageType::RelayMsg, QStringList() << "serverinfo" << info);
            this->sendMessage(socket, msg);
        }
    }
    else if (p.length() == 10 && p.getStringAt(0) == "addgame" && p.getType() == network::MessageType::RelayMsg)
    {
        logMsg("Adding game");
        gameInfo g;
        g.name = p.getStringAt(1);
        g.desc = p.getStringAt(2);
        g.username = p.getStringAt(3);
        g.tournament = p.getStringAt(4).toInt();
        g.openForAnyone = p.getStringAt(5).toInt();
        g.board = p.getStringAt(6);
        g.deck = p.getStringAt(7);
        g.gameMode = p.getStringAt(8).toInt();
        g.password = p.getStringAt(9);
        g.passwordRequired = g.password.length();
        if (g.name.size() <= 200 && g.desc.size() <= 200 && g.username.size() <= 200 && g.board.size() <= 200 && g.deck.size() <= 200 && g.password.size() <= 200)
        {
            g.gameID = getRandomString();
            g.keepAliveTime = time(nullptr);
            g.startTime = time(nullptr);
            g.status = "lobby";
            g.secretKey = getRandomString();
            games.append(g);
            network::networkDataPackage msg(network::MessageType::RelayMsg, QStringList() << "publicGameKey" << g.gameID << g.secretKey);
            this->sendMessage(socket, msg);
        }
        else
        {
            logMsg("Invalid game data received: too long strings");
            socket->close();
            return;
        }
    }
    else if (p.length() == 4 && p.getStringAt(0) == "currpublicgameid")
    {
        bool ok = true;
        bool passwordFailed = false;
        int idx = -1;
        if (p.getStringAt(3) != getGamePassword(p.getStringAt(1)))
        {
            passwordFailed = true;
        }
        for (int i = 0; i < this->connectedUsers.size(); ++i)
        {
            if (this->connectedUsers[i].first == socket)
            {
                idx = i;
                if (!passwordFailed)
                {
                    this->connectedUsers[i].second = userInfo{p.getStringAt(1), p.getStringAt(2)};
                    logMsg("User data set: " + p.getStringAt(1) + ", " + p.getStringAt(2));
                }
            }
            else if (this->connectedUsers[i].second.gameID == p.getStringAt(1) && this->connectedUsers[i].second.username == p.getStringAt(2))
            {
                ok = false;
            }
        }

        if (!ok && idx > -1)
        {
            logMsg("Another user is already in-game with the same name (in the same game): " + p.getStringAt(1) + ", " + p.getStringAt(2));
            this->connectedUsers[idx].second = userInfo{};
            if (p.getStringAt(1) != "gameid")
            {
                network::networkDataPackage pkg(network::MessageType::RelayMsg, QStringList() << "nameAlreadyInUse");
                this->sendMessage(this->connectedUsers[idx].first, pkg);
            }
        }
        else if (passwordFailed && idx > -1)
        {
            logMsg("Wrong password for game: " + p.getStringAt(1) + ", " + p.getStringAt(2) + ", " + p.getStringAt(3));
            this->connectedUsers[idx].second = userInfo{};
            if (p.getStringAt(1) != "gameid")
            {
                network::networkDataPackage pkg(network::MessageType::RelayMsg, QStringList() << "wrongPassword");
                this->sendMessage(this->connectedUsers[idx].first, pkg);
            }
        }
    }
    else if (p.length() > 1)
    {
        logMsg("Forwarding message...");
        auto target = p.getStringAt(1);
        for (int i = 0; i < this->connectedUsers.size(); ++i)
        {
            if (this->connectedUsers[i].second.username == target && this->connectedUsers[i].first != socket && this->connectedUsers[i].second.gameID == p.getStringAt(0))
            {
                this->sendMessage(this->connectedUsers[i].first, p);
            }
        }
    }
    else
    {
        logMsg("Don't know what to do with this message: " + p.toDebugString());
    }

    if (socket->bytesAvailable()) goto start;
}

void networkManager::sendMessage(QTcpSocket* socket, network::networkDataPackage package)
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDATASTREAM_VERSION);
    out << static_cast<qint64>(0);
    out << package.serialize();
    out.device()->seek(0);
    out << static_cast<quint64>(block.size() - sizeof(qint64));

    socket->write(block);
}

int main(int argc, char* argv[])
{
    QCoreApplication a(argc, argv);

    auto tcpServer = new QTcpServer{};
    auto nM = new networkManager{};

    if (a.arguments().length() != 2)
    {
        QTextStream out(stdout);
        out << "Invalid arguments, first argument should be the config file path." << "\n";
        out << "======================" << "\n";
        out << "SERVER FAILED TO START" << "\n";
        out << "======================" << "\n";
        out.flush();
        std::exit(0);
    }

    int port = 0;
    QFile cfg(a.arguments().at(1));
    if (cfg.open(QFile::ReadOnly))
    {
        QJsonDocument conf = QJsonDocument::fromJson(cfg.readAll());
        adminKey = qgetenv("LB_SERVER_ADMIN_KEY");
        if (!conf.isNull() && !conf.object().isEmpty() && conf.object().contains("port") && conf.object().contains("logPath") && conf.object().contains("banList"))
        {
            if(adminKey.isEmpty())
            {
                adminKey = conf.object()["adminKey"].toString();
            }
            port = conf.object()["port"].toInt();
            logPath = conf.object()["logPath"].toString();
            banListFilename = conf.object()["banList"].toString();
        }
        else
        {
            QTextStream out(stdout);
            out << "Invalid configuration." << "\n";
            out << "======================" << "\n";
            out << "SERVER FAILED TO START" << "\n";
            out << "======================" << "\n";
            out.flush();
            std::exit(0);
        }
        if (adminKey.isEmpty())
        {
            QTextStream out(stdout);
            out << "Admin key not set." << "\n";
            out << "======================" << "\n";
            out << "SERVER FAILED TO START" << "\n";
            out << "======================" << "\n";
            out.flush();
            std::exit(0);
        }
    }
    else
    {
        QTextStream out(stdout);
        out << "Failed to open configuration file." << "\n";
        out << "======================" << "\n";
        out << "SERVER FAILED TO START" << "\n";
        out << "======================" << "\n";
        out.flush();
        std::exit(0);
    }

    if (logPath == "-") logToStdOut = true;

    QObject::connect(tcpServer, &QTcpServer::newConnection, nM, &networkManager::handleNewConnection);
    QObject::connect(tcpServer, &QTcpServer::acceptError, tcpServer, [tcpServer, port](QAbstractSocket::SocketError err)
    {
        logMsg("TCP server accept error: " + QString::number(err));
        if (!tcpServer->isListening())
        {
            logMsg("TCP server not listening, restarting...");
            tcpServer->close();
            tcpServer->listen(QHostAddress::Any, port);
        }
    });

    QTimer* maintenanceTimer = new QTimer;
    maintenanceTimer->setInterval(1000);
    QObject::connect(maintenanceTimer, &QTimer::timeout, tcpServer, [tcpServer, port]()
    {
        for (int i = 0; i < games.size(); ++i)
        {
            if (time(nullptr) - games[i].keepAliveTime > 30 || time(nullptr) - games[i].startTime > 36 * 60 * 60)
            {
                games.removeAt(i);
                --i;
                logMsg("Maintenance: removed game");
            }
        }
        if (!tcpServer->isListening())
        {
            logMsg("TCP server not listening, trying to restart");
            tcpServer->close();
            tcpServer->listen(QHostAddress::Any, port);
        }
    });

    QTimer* banTimer = new QTimer;
    banTimer->setInterval(5000);
    QObject::connect(banTimer, &QTimer::timeout, nM, [nM]()
    {
        QFile f(banListFilename);
        if (f.open(QFile::ReadOnly))
        {
            banList.clear();
            QTextStream in(&f);
            while (!in.atEnd())
            {
                QString line = in.readLine().trimmed();
                if (!line.isEmpty()) banList.append(QHostAddress::parseSubnet(line));
            }
            f.close();
            nM->checkBanList(nullptr);
        }
    });
    banTimer->start();

    if (!tcpServer->listen(QHostAddress::Any, port))
    {
        logMsg("======================");
        logMsg("SERVER FAILED TO START");
        logMsg("======================");
        std::exit(0);
    }
    else
    {
        logMsg("==============");
        logMsg("SERVER STARTED");
        logMsg("==============");
    }

    maintenanceTimer->start();

    return a.exec();
}
