/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <QGraphicsScene>
#include <QFile>
#include <QGraphicsPixmapItem>
#include <QWindow>
#include <QTextStream>
#include <QLabel>
#include <QtNetwork>
#include <QInputDialog>
#include <QUrl>
#include <QVBoxLayout>
#include <QImage>
#include <iostream>
#include <cmath>
#include <QDir>
#include <QSysInfo>
#include <QTreeWidgetItemIterator>
#include <QStyleFactory>
#include <QTextEdit>
#include <QMessageBox>
#include <QGraphicsDropShadowEffect>
#include <QScrollBar>
#include <QListWidget>
#include <QSpinBox>
#include <QtCore>
#include <QComboBox>
#include <QTreeWidget>
#include <QShortcut>
#include <QScreen>
#include <ctime>
#include <QTime>
#include <QOpenGLFunctions>
#include <QOffscreenSurface>
#include <QFileDialog>
#include "card.h"
#include "fileformat.h"
#include "globals.h"
#include "graphicsviewmenu.h"
#include "mainwindow.h"
#include "network.h"
#include "cfgfile.h"
#include "random.h"
#include "audio.h"
#include "slidingstackedwidget.h"
#include "utils.h"
#include "ui_mainwindow.h"
#include <cstdlib>
#include <limits>
#include <ctime>
#ifndef Q_OS_WASM
#include <QSoundEffect>
#endif

static bool suppressChanges = true;

cfgFile mainWindow::autoCfg{};

QIcon proxyStyle::standardIcon(StandardPixmap px, const QStyleOption* opt, const QWidget* w) const
{
    static const QSet<QStyle::StandardPixmap> pxList =
    {
        QStyle::SP_DialogOkButton,
        QStyle::SP_DialogCancelButton,
        QStyle::SP_DialogHelpButton,
        QStyle::SP_DialogNoButton,
        QStyle::SP_DialogYesButton,
        QStyle::SP_DialogCloseButton,
        QStyle::SP_MessageBoxInformation,
        QStyle::SP_MessageBoxWarning,
        QStyle::SP_MessageBoxCritical,
        QStyle::SP_MessageBoxQuestion
    };
    if (pxList.contains(px)) return QIcon();
    return QProxyStyle::standardIcon(px, opt, w);
}

bool endGameEventFilter::eventFilter(QObject*, QEvent* event)
{
    if (event->type() == QEvent::SockAct)
    {
        if (auto ptr = dynamic_cast<bazaarEvent*>(event))
        {
            if (ptr->mW)
            {
                if (ptr->data == "endGame")
                {
                    ptr->mW->endGame();
                }
                else if (ptr->data == "returnToPuzzleScreen")
                {
                    ptr->mW->endGame(false, true);
                }
                else if (ptr->data == "retryCurrentPuzzle")
                {
                    ptr->mW->endGame(false, true);
                    ptr->mW->loadGameDirectly(ptr->mW->lastPuzzleSaveFile, ptr->mW->lastPuzzleScript);
                }
            }
            return true;
        }
    }
    return false;
}

mainWindow::mainWindow(QWidget* parent, QSettings* settings) : QMainWindow(parent), ui(new Ui::mainWindow), zoomCounter(0)
{
    mainWindow::instance = this;
    mainWindow::settings = settings;
    ui->setupUi(this);
    this->ui->helpBrowser->document()->setDefaultStyleSheet("a {color: #2370aa; font-weight:bold;}");
    this->ui->helpContentsBrowser->document()->setDefaultStyleSheet("a {color: #2370aa; font-weight:bold;}");
    this->ui->replayWidget->setVisible(false);
    this->ui->nextStepButton->setVisible(false);
    this->mPlayer = new musicPlayer;
    if (this->getConfigVariable("notFirstStart").isEmpty()) this->resetSettings();
    if (this->getConfigVariable("slidingAnim").isEmpty()) this->setConfigVariable("slidingAnim", "1");

    auto* shortcut1 = new QShortcut(QKeySequence("Ctrl+Q"), this);
    connect(shortcut1, &QShortcut::activated, this, [this]()
    {
        if (shortcutsEnabled())
        {
            this->ui->abortButton->click();
        }
    });
    auto* shortcut2 = new QShortcut(QKeySequence("Ctrl+Space"), this);
    connect(shortcut2, &QShortcut::activated, this, [this]()
    {
        if (shortcutsEnabled())
        {
            if (this->ui->endTurnButton->isEnabled()) this->ui->endTurnButton->click();
        }
    });
    auto* shortcut3 = new QShortcut(QKeySequence("Ctrl+P"), this);
    connect(shortcut3, &QShortcut::activated, this, [this]()
    {
        if (shortcutsEnabled())
        {
            this->ui->accessSettingsButton->click();
        }
    });
    auto* shortcut4 = new QShortcut(QKeySequence("Ctrl+H"), this);
    connect(shortcut4, &QShortcut::activated, this, [this]()
    {
        if (shortcutsEnabled())
        {
            this->ui->accessHelpButton->click();
        }
    });
    auto* shortcut5 = new QShortcut(QKeySequence("Ctrl+D"), this);
    connect(shortcut5, &QShortcut::activated, this, [this]()
    {
        if (shortcutsEnabled())
        {
            this->ui->accessBoardsDecksButton->click();
        }
    });
    auto* shortcut6 = new QShortcut(QKeySequence("Ctrl+S"), this);
    connect(shortcut6, &QShortcut::activated, this, [this]()
    {
        if (shortcutsEnabled())
        {
            this->ui->saveButton->click();
        }
    });
    auto* shortcut7 = new QShortcut(QKeySequence("Ctrl+0"), this);
    connect(shortcut7, &QShortcut::activated, this, [this]()
    {
        if (shortcutsEnabled())
        {
            this->ui->resetZoomButton->click();
        }
    });
    auto* shortcut8 = new QShortcut(QKeySequence(Qt::Key_0), this);
    connect(shortcut8, &QShortcut::activated, this, [this]()
    {
        if (shortcutsEnabled())
        {
            this->ui->resetZoomButton->click();
        }
    });
    auto* shortcut9 = new QShortcut(QKeySequence(Qt::Key_Plus), this);
    connect(shortcut9, &QShortcut::activated, this, [this]()
    {
        if (shortcutsEnabled())
        {
            this->ui->zoomInButton->click();
        }
    });
    auto* shortcut10 = new QShortcut(QKeySequence(Qt::Key_Minus), this);
    connect(shortcut10, &QShortcut::activated, this, [this]()
    {
        if (shortcutsEnabled())
        {
            this->ui->zoomOutButton->click();
        }
    });
    auto* shortcut11 = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::Key_Plus), this);
    connect(shortcut11, &QShortcut::activated, this, [this]()
    {
        if (shortcutsEnabled())
        {
            this->ui->zoomInButton->click();
        }
    });
    auto* shortcut12 = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::Key_Minus), this);
    connect(shortcut12, &QShortcut::activated, this, [this]()
    {
        if (shortcutsEnabled())
        {
            this->ui->zoomOutButton->click();
        }
    });
    auto* shortcut13 = new QShortcut(QKeySequence(Qt::KeypadModifier | Qt::Key_0), this);
    connect(shortcut13, &QShortcut::activated, this, [this]()
    {
        if (shortcutsEnabled())
        {
            this->ui->resetZoomButton->click();
        }
    });
    auto* shortcut14 = new QShortcut(QKeySequence(Qt::KeypadModifier | Qt::Key_Insert), this);
    connect(shortcut14, &QShortcut::activated, this, [this]()
    {
        if (shortcutsEnabled())
        {
            this->ui->resetZoomButton->click();
        }
    });
    auto* shortcut15 = new QShortcut(QKeySequence(Qt::Key_Insert), this);
    connect(shortcut15, &QShortcut::activated, this, [this]()
    {
        if (shortcutsEnabled())
        {
            this->ui->resetZoomButton->click();
        }
    });

#if !defined(Q_OS_HAIKU) && !defined(Q_OS_WASM)
    if (this->getConfigVariable("gpuAccel").isEmpty())
    {
        QString renderer;
        bool glContextCreated = false;
        QOffscreenSurface surf;
        surf.create();
        QOpenGLContext ctx;
        if (ctx.create() && ctx.makeCurrent(&surf))
        {
            QOpenGLFunctions glFuncs(QOpenGLContext::currentContext());
            renderer = QString::fromLatin1(QByteArray((const char*)glFuncs.glGetString(GL_RENDERER)));
            glContextCreated = true;
        }
        if (glContextCreated && !renderer.contains("intel", Qt::CaseInsensitive))
        {
            this->setConfigVariable("gpuAccel", "1");
        }
        else
        {
            this->setConfigVariable("gpuAccel", "0");
        }
    }
#else
    if (this->getConfigVariable("gpuAccel").isEmpty()) this->setConfigVariable("gpuAccel", "0");
#endif
    if (this->getConfigVariable("allowChatSplitterResize").toInt())
    {
        this->ui->bottomWidget->setMaximumHeight(std::numeric_limits<int>::max());
    }
    else
    {
        this->ui->bottomWidget->setMaximumHeight(CHAT_PANEL_MAX_HEIGHT);
    }
    this->ui->adminBanButton->setVisible(!this->getConfigVariable("serverAdminKey").isEmpty());
    this->ui->adminUnbanButton->setVisible(!this->getConfigVariable("serverAdminKey").isEmpty());
    this->ui->adminInfoButton->setVisible(!this->getConfigVariable("serverAdminKey").isEmpty());
    this->drawOrientationOnSidebarDelayTimer = new QTimer;
    connect(this->ui->adminBanButton, &QPushButton::clicked, this, [this](){
        QString banText = QInputDialog::getText(this, "Ban", "Address or address range to ban:").trimmed();
        if(!banText.isEmpty())
        {
            this->sendReqToPublicServer("ban", QStringList() << banText << mainWindow::getConfigVariable("serverAdminKey"));
        }
    });
    connect(this->ui->adminUnbanButton, &QPushButton::clicked, this, [this](){
        QString banText = QInputDialog::getText(this, "Unban", "Address or address range to unban (have to match exactly with an existing ban):").trimmed();
        if(!banText.isEmpty())
        {
            this->sendReqToPublicServer("unban", QStringList() << banText << mainWindow::getConfigVariable("serverAdminKey"));
        }
    });
    connect(this->ui->adminInfoButton, &QPushButton::clicked, this, [this](){
        this->sendReqToPublicServer("playerinfo", QStringList() << mainWindow::getConfigVariable("serverAdminKey"));
    });
    connect(this->drawOrientationOnSidebarDelayTimer, &QTimer::timeout, this, [this]()
    {
        if (!this->currentlyDisplayedOnSideBarID.isEmpty())
        {
            this->displayCardsCache[this->currentlyDisplayedOnSideBarID]->removeFromScene(this->ui->sideBarCardGV->scene());
        }
        this->ui->sideBarCardGV->scene()->clear();
        this->currentlyDisplayedOnSideBarID.clear();
        this->drawOrientationOnSideBarCardGV();
    });
    this->ui->decksTreeWidget->setHeaderHidden(false);
    this->ui->boardsTreeWidget->setHeaderHidden(false);
    this->publicServerTimer = new QTimer;
    this->publicServerTimer->setInterval(2000);
    this->publicServerKeepAliveTimer = new QTimer;
    this->publicServerKeepAliveTimer->setInterval(10000);
    this->nam = new QNetworkAccessManager;
    if (QGuiApplication::primaryScreen()->availableGeometry().width() < 1248 || QGuiApplication::primaryScreen()->availableGeometry().height() < 702)
    {
        this->resize(this->minimumSize().width(), this->minimumSize().height());
    }
    else if (this->getConfigVariable("hiresMode").toInt())
    {
        this->setMinimumSize(QSize(1472, 828));
        this->ui->sideBarCardGV->setFixedSize(QSize(314, 314));
        this->ui->sideBarStackedWidget->setFixedWidth(382);
        this->ui->sideBarPage->setFixedWidth(382);
        this->ui->sideBarTabWidget->setFixedHeight(338);
    }
    connect(this->publicServerTimer, &QTimer::timeout, this, [this]()
    {
        this->sendReqToPublicServer("gamelist", {});
    });
    connect(this->ui->hucksterCardsComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(lobbyChangedByUser()));
    connect(this->ui->gameModeComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(lobbyChangedByUser()));
    connect(this->ui->boardsTreeWidget, &QTreeWidget::itemSelectionChanged, this, [this]()
    {
        this->lobbyChangedByUser("board");
        if (this->hostMode)
        {
            this->enableStartGameButtonIfPossible();
            this->clearRandomBoardDeck();
        }
    });
    connect(this->ui->decksTreeWidget, &QTreeWidget::itemSelectionChanged, this, [this]()
    {
        this->lobbyChangedByUser("deck");
        if (this->hostMode)
        {
            this->enableStartGameButtonIfPossible();
            this->clearRandomBoardDeck();
        }
    });
    connect(this->ui->aboutQtButton, &QPushButton::clicked, this, []()
    {
        QApplication::aboutQt();
    });
    connect(this->ui->useHucksterCheckBox, SIGNAL(toggled(bool)), this, SLOT(lobbyChangedByUser()));
    connect(this->ui->openHandsCheckBox, SIGNAL(toggled(bool)), this, SLOT(lobbyChangedByUser()));
    connect(this->ui->p1NickLineEdit, SIGNAL(textChanged(QString)), this, SLOT(lobbyChangedByUser()));
    connect(this->ui->p1TypeComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(lobbyChangedByUser()));
    connect(this->ui->p2NickLineEdit, SIGNAL(textChanged(QString)), this, SLOT(lobbyChangedByUser()));
    connect(this->ui->p2TypeComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(lobbyChangedByUser()));
    connect(this->ui->p3NickLineEdit, SIGNAL(textChanged(QString)), this, SLOT(lobbyChangedByUser()));
    connect(this->ui->p3TypeComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(lobbyChangedByUser()));
    connect(this->ui->p4NickLineEdit, SIGNAL(textChanged(QString)), this, SLOT(lobbyChangedByUser()));
    connect(this->ui->p4TypeComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(lobbyChangedByUser()));
    connect(this->ui->sendLobbyMsgButton, &QPushButton::clicked, this, &mainWindow::sendLobbyChatMsg);
    connect(this->ui->lobbyChatMsgEdit, &QLineEdit::returnPressed, this, &mainWindow::sendLobbyChatMsg);
    //TODOSconnect(this->ui->stackedWidget, &slidingStackedWidget::animationFinished, this, &mainWindow::updateCurrentPlayerAndScoreLabels);
    connect(this->ui->sendMsgButton, &QPushButton::clicked, this, &mainWindow::sendChatMsg);
    connect(this->ui->chatMsgLineEdit, &QLineEdit::returnPressed, this, &mainWindow::sendChatMsg);
    connect(this->ui->loadSaveGameButton, &QPushButton::pressed, this, [this]()
    {
        static bool gameloaded = false;
        if (!gameloaded)
        {
            QString path = QFileDialog::getOpenFileName(nullptr, tr("Load game"), getSavePath(), tr("Lost Bazaar saved games (%1)").arg("*.lbs"));
            if (!path.isEmpty())
            {
                gameloaded = !gameloaded;
                if (!this->updateNewGameUIForLoadedGame(path))
                {
                    gameloaded = false;
                    this->cleanUpAfterUnloadingGame();
                }
            }
        }
        else
        {
            gameloaded = !gameloaded;
            this->updateNewGameUIForLoadedGame({});
            this->cleanUpAfterUnloadingGame();
        }
    });

    this->ui->versionLabel->setText(tr("Join our Discord server at <a href=\"http://discord.lost-bazaar.org\">discord.lost-bazaar.org</a>!<br>version %1").arg(LOST_BAZAAR_VERSION));
    this->ui->verticalLayout_2->setAlignment(this->ui->tutorialButton, Qt::AlignHCenter);
    this->ui->verticalLayout_2->setAlignment(this->ui->newGameButton, Qt::AlignHCenter);
    this->ui->verticalLayout_2->setAlignment(this->ui->networkButton, Qt::AlignHCenter);
    this->ui->verticalLayout_2->setAlignment(this->ui->puzzleGameButton, Qt::AlignHCenter);
    this->ui->verticalLayout_2->setAlignment(this->ui->editorButton, Qt::AlignHCenter);
    this->ui->verticalLayout_2->setAlignment(this->ui->settingsButton, Qt::AlignHCenter);
    this->ui->verticalLayout_2->setAlignment(this->ui->reportBugButton, Qt::AlignHCenter);
    this->ui->verticalLayout_2->setAlignment(this->ui->helpButton, Qt::AlignHCenter);
    this->ui->menuTitleLabel->setPixmap(QPixmap(getPrimaryPath("img/menu_logo.png")));
    this->ui->horizontalLayout_5->setAlignment(this->ui->sideBarCardGV, Qt::AlignHCenter);
    this->ui->titleLabel->setMainWindow(this);
    this->networkManager = new network::networkConnectionManager;
    connect(this->networkManager, &network::networkConnectionManager::connectedToHost, this, &mainWindow::joinGame);
    connect(this->publicServerKeepAliveTimer, &QTimer::timeout, this, &mainWindow::publicServerKeepAlive);
    connect(this->networkManager, &network::networkConnectionManager::clientConnected, this, &mainWindow::handleNetworkClientConnected);
    connect(this->networkManager, &network::networkConnectionManager::gameEvent, this, &mainWindow::handleGameEvent);
    connect(this->networkManager, &network::networkConnectionManager::lobbyChatMessage, this, &mainWindow::handleNetworkLobbyChatMessage);
    connect(this->networkManager, &network::networkConnectionManager::lobbyUpdate, this, &mainWindow::handleNetworkLobbyUpdate);
    connect(this->networkManager, &network::networkConnectionManager::aUserDisconnected, this, &mainWindow::aUserDisconnectedFromGame);
    connect(this->networkManager, &network::networkConnectionManager::publicGameKeyReceived, this, &mainWindow::updatePublicServerSecretKey);
    connect(this->networkManager, &network::networkConnectionManager::serverInfoReceived, this, &mainWindow::serverInfoReceived);
    connect(this->networkManager, &network::networkConnectionManager::publicLobbyDataReceived, this, &mainWindow::updatePublicServerPage);
    connect(this->networkManager, &network::networkConnectionManager::nameAlreadyInUse, this, &mainWindow::usernameAlreadyInUse);
    connect(this->networkManager, &network::networkConnectionManager::wrongPassword, this, &mainWindow::wrongPassword);

    this->setWindowTitle("Lost Bazaar");
    this->ui->gameInfoButton->setVisible(false);
    this->setStyle(new proxyStyle("Fusion"));
    this->ui->sideBarLayout->setAlignment(this->ui->sideBarCardGV, Qt::AlignHCenter);
    this->ui->sideBarCardGV->setScene(new QGraphicsScene);
    this->ui->sideBarCardGV->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    this->ui->publicGameList->setVisible(false);
    this->ui->hotseatLogoLabel->setPixmap(QPixmap(getPrimaryPath(QString("img/hotseat_page_logo.png"))));
    this->ui->p1LogoLabel->setPixmap(QPixmap(getPrimaryPath(QString("img/player_1_symbol_newgamepage.png"))));
    this->ui->p1LogoLabel->setFixedSize(QPixmap(getPrimaryPath(QString("img/player_1_symbol_newgamepage.png"))).size());
    this->ui->p2LogoLabel->setPixmap(QPixmap(getPrimaryPath(QString("img/player_2_symbol_newgamepage.png"))));
    this->ui->p2LogoLabel->setFixedSize(QPixmap(getPrimaryPath(QString("img/player_2_symbol_newgamepage.png"))).size());
    this->ui->p3LogoLabel->setPixmap(QPixmap(getPrimaryPath(QString("img/player_3_symbol_newgamepage.png"))));
    this->ui->p3LogoLabel->setFixedSize(QPixmap(getPrimaryPath(QString("img/player_3_symbol_newgamepage.png"))).size());
    this->ui->p4LogoLabel->setPixmap(QPixmap(getPrimaryPath(QString("img/player_4_symbol_newgamepage.png"))));
    this->ui->p4LogoLabel->setFixedSize(QPixmap(getPrimaryPath(QString("img/player_4_symbol_newgamepage.png"))).size());
    connect(this->ui->fastForwardButton, &QPushButton::clicked, this, [this]()
    {
        this->ui->fastForwardButton->setEnabled(false);
        this->ui->stepByStepButton->setEnabled(true);
        this->ui->normalSpeedButton->setEnabled(true);
        this->ui->stepByStepButton->setChecked(false);
        this->ui->nextStepButton->setVisible(false);
        this->ui->normalSpeedButton->setChecked(false);
        this->ui->fastForwardButton->setChecked(true);
        this->fastForward = true;
        this->replayTimer->stop();
        this->replayTimer->setInterval(10);
        this->replayTimer->start();
    });
    connect(this->ui->normalSpeedButton, &QPushButton::clicked, this, [this]()
    {
        this->ui->fastForwardButton->setEnabled(true);
        this->ui->stepByStepButton->setEnabled(true);
        this->ui->normalSpeedButton->setEnabled(false);
        this->ui->stepByStepButton->setChecked(false);
        this->ui->nextStepButton->setVisible(false);
        this->ui->normalSpeedButton->setChecked(true);
        this->ui->fastForwardButton->setChecked(false);
        this->fastForward = false;
        this->replayTimer->stop();
        this->replayTimer->setInterval(1000);
        this->replayTimer->start();
    });
    connect(this->ui->stepByStepButton, &QPushButton::clicked, this, [this]()
    {
        this->ui->fastForwardButton->setEnabled(true);
        this->ui->stepByStepButton->setEnabled(false);
        this->ui->normalSpeedButton->setEnabled(true);
        this->ui->stepByStepButton->setChecked(true);
        this->ui->nextStepButton->setVisible(true);
        this->ui->normalSpeedButton->setChecked(false);
        this->ui->fastForwardButton->setChecked(false);
        this->fastForward = false;
        if (this->replayTimer->isActive())
        {
            this->replayTimer->stop();
        }
    });
    connect(this->ui->nextStepButton, &QPushButton::clicked, this, &mainWindow::replayNextGameEvent);
    connect(this->networkManager, &network::networkConnectionManager::relayConnected, this, [this]()
    {
        this->ui->startNewPublicServerGamebutton->setEnabled(false);
        this->ui->publicServerNetworkInfoLabel->setText(tr("Loading..."));
        this->sendReqToPublicServer("gamelist", {});
    });
    connect(this->networkManager, &network::networkConnectionManager::relayDisconnected, this, [this]()
    {
        if (this->waitingForPublicGameJoin && !this->gameInProgress)
        {
            this->waitingForPublicGameJoin = false;
            this->ui->publicGameList->setVisible(true);
            this->ui->startNewPublicServerGamebutton->setEnabled(true);
            this->ui->publicServerNetworkInfoLabel->setVisible(false);
            this->sendReqToPublicServer("gamelist", {});
            this->publicServerTimer->start();
        }
        else if (this->ongoingNetworkGame || this->gameInProgress)
        {
            this->endGame(false, false, true);
        }
        else if (this->waitingForAddressJoin)
        {
            this->waitingForAddressJoin = false;
        }
        this->ui->publicServerNetworkInfoLabel->setVisible(true);
        this->ui->publicGameList->setVisible(false);
        this->ui->startNewPublicServerGamebutton->setEnabled(false);
        this->ui->publicServerNetworkInfoLabel->setText(tr("No connection to the server"));
    });
    this->ui->eyeLabel->setPixmap(QPixmap(getPrimaryPath(QString("img/eye.png"))));
    if (!mainWindow::getConfigVariable("hiresMode").toInt())
    {
        this->ui->sideBarCardGV->viewport()->setFixedSize(240, 240);
    }
    else
    {
        this->ui->sideBarCardGV->viewport()->setFixedSize(312, 312);
    }
    this->ui->sideBarCardGV->setRenderHints(QPainter::SmoothPixmapTransform | QPainter::Antialiasing | QPainter::TextAntialiasing);
    this->ui->gameBoard->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->ui->gameBoard->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->ui->gameBoard->setDragMode(QGraphicsView::ScrollHandDrag);
    this->ui->gameBoard->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    this->ui->boardPreview->setDragMode(QGraphicsView::ScrollHandDrag);
    this->replayTimer = new QTimer(this);
    connect(this->replayTimer, &QTimer::timeout, this, &mainWindow::replayNextGameEvent);
    connect(this->ui->zoomInButton, &QPushButton::clicked, this, &mainWindow::zoomIn);
    connect(this->ui->zoomOutButton, &QPushButton::clicked, this, &mainWindow::zoomOut);
    connect(this->ui->resetZoomButton, &QPushButton::clicked, this, &mainWindow::zoomReset);
    connect(this->ui->nextTipButton, &QPushButton::clicked, this, &mainWindow::updateTipOfTheDay);
    this->ui->stackedWidget->setCurrentWidget(this->ui->titlePage);
    srand(time(nullptr));
    this->random.seedRNGs(time(nullptr));
    this->state = new gameState(this);
    this->ui->sideBarCardGV->setBackgroundBrush(QPixmap(getPrimaryPath("img/sidebarcardview_bkg.png")));

    connect(this->ui->publicServerAddressEdit, &QLineEdit::returnPressed, this, [this]()
    {
        this->ui->startNewPublicServerGamebutton->setEnabled(false);
        this->networkManager->resetRelay();
        this->networkManager->connectToRelay(this->ui->publicServerAddressEdit->text());
    });
    connect(this->ui->decksFilter, &QLineEdit::textChanged, this, [this](const QString & text)
    {
        this->filter(text, this->ui->decksTreeWidget);
    });
    connect(this->ui->boardsFilter, &QLineEdit::textChanged, this, [this](const QString & text)
    {
        this->filter(text, this->ui->boardsTreeWidget);
    });
    connect(this->ui->startPublicGameButton, &QPushButton::clicked, this, [this]()
    {
        this->publicServerRegisterGame();
        this->hostMode = true;
        this->privateGame = false;
        this->resetStartGameScreen();
        this->networkManager->reset(false);
        this->networkManager->start(network::NetworkGameMode::HostMode, {}, this->ui->publicGamePasswordEdit->text());
        this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->newGamePage), slidingStackedWidget::AnimationDirection::RightToLeft);
        this->ui->gameModeComboBox->setFocus();
        this->ui->splitter->setSizes(QList<int>() << 4 << 1);
    });
    connect(this->ui->joinPublicGameWithPasswordButton, &QPushButton::clicked, this, &mainWindow::joinPublicGameWithPassword);
    connect(this->ui->passwordPagePasswordEdit, &QLineEdit::returnPressed, this, &mainWindow::joinPublicGameWithPassword);
    connect(this->ui->backFromPublicGameDescPageButton, &QPushButton::clicked, this, [this]()
    {
        this->ui->startNewPublicServerGamebutton->setEnabled(false);
        this->sendReqToPublicServer("gamelist", {});
        this->publicServerTimer->start();
        this->networkManager->resetRelay();
        this->networkManager->connectToRelay(this->ui->publicServerAddressEdit->text());
        this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->publicServerPage), slidingStackedWidget::AnimationDirection::LeftToRight);
    });
    connect(this->ui->backFromPasswordPageButton, &QPushButton::clicked, this, [this]()
    {
        this->sendReqToPublicServer("gamelist", {});
        this->publicServerTimer->start();
        this->networkManager->resetRelay();
        this->networkManager->connectToRelay(this->ui->publicServerAddressEdit->text());
        this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->publicServerPage), slidingStackedWidget::AnimationDirection::LeftToRight);
    });
    connect(this->ui->backFromEditorButton, &QPushButton::clicked, this, [this]()
    {
        this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->editorSourceWidget));
        suppressChanges = true;
        if (this->hostMode && this->editorSourceWidget == this->ui->newGamePage)
        {
            this->clearRandomBoardDeck();
        }
    });
    connect(this->ui->backFromPuzzleButton, &QPushButton::clicked, this, [this]()
    {
        this->resetMenuPage();
        this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->menuPage), slidingStackedWidget::AnimationDirection::LeftToRight);
    });
    connect(this->ui->editorButton, &QPushButton::clicked, this, [this]()
    {
        this->showEditor(this->ui->menuPage);
    });
    connect(this->ui->boardPreviewButton, &QPushButton::clicked, this, [this]()
    {
        if (this->hostMode)
        {
            this->clearRandomBoardDeck();
        }
        this->showEditor(this->ui->newGamePage, "board", this->ui->boardsTreeWidget->selectedItems(), this->ui->decksTreeWidget->selectedItems(), true, false);
    });
    connect(this->ui->deckPreviewButton, &QPushButton::clicked, this, [this]()
    {
        if (this->hostMode)
        {
            this->clearRandomBoardDeck();
        }
        this->showEditor(this->ui->newGamePage, "deck", this->ui->boardsTreeWidget->selectedItems(), this->ui->decksTreeWidget->selectedItems(), false, true);
    });
    connect(this->ui->aboutButton, &QPushButton::clicked, this, [this]()
    {
        QString lang = languageConfigValueToLocaleName(this->getConfigVariable("language"));
        if (lang.isEmpty() || getPrimaryPath("help/" + lang + "_about.html", true).isEmpty()) lang = DEFAULT_LANGUAGE;
        this->ui->aboutBrowser->setSource(QUrl::fromLocalFile(getPrimaryPath("help/" + lang + "_about.html")));
        this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->aboutPage), slidingStackedWidget::AnimationDirection::RightToLeft);
    });
    connect(this->ui->newGameButton, &QPushButton::clicked, this, [this]()
    {
        this->hostMode = true;
        this->privateGame = true;
        this->resetStartGameScreen();
        this->networkManager->reset();
        this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->newGamePage), slidingStackedWidget::AnimationDirection::RightToLeft);
        this->ui->gameModeComboBox->setFocus();
        this->ui->splitter->setSizes(QList<int>() << 4 << 1);
        if (this->autoCfg.hasKey("deck"))
        {
            QTreeWidgetItemIterator it(this->ui->decksTreeWidget);
            while (*it)
            {
                if ((*it)->data(0, Qt::UserRole) == this->autoCfg.readValue("deck"))
                {
                    (*it)->setSelected(true);
                    break;
                }
                ++it;
            }
        }
        if (this->autoCfg.hasKey("board"))
        {
            QTreeWidgetItemIterator it(this->ui->boardsTreeWidget);
            while (*it)
            {
                if ((*it)->data(0, Qt::UserRole) == this->autoCfg.readValue("board"))
                {
                    (*it)->setSelected(true);
                    break;
                }
                ++it;
            }
        }
        if (this->autoCfg.hasKey("autoStartGame") && this->autoCfg.readValue("autoStartGame") == "true")
        {
            this->ui->startGameButton->click();
        }
    });
    connect(this->ui->startNewPublicServerGamebutton, &QPushButton::clicked, this, [this]()
    {
        this->privateGame = false;
        this->hostMode = true;
        this->publicServerTimer->stop();
        this->ui->publicGameDescLineEdit->setText("");
        this->ui->publicGameNameLineEdit->setText(tr("Game hosted by %1").arg(this->getConfigVariable("username")));
        this->ui->publicGamePasswordEdit->setText("");
        this->ui->publicGameTournamentCheckbox->setChecked(false);
        this->ui->publicGameOpenForAnyoneCheckbox->setChecked(false);
        this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->publicGameInfoPage), slidingStackedWidget::AnimationDirection::RightToLeft);
        this->ui->splitter->setSizes(QList<int>() << 4 << 1);
    });
    connect(this->ui->puzzleGameButton, &QPushButton::clicked, this, [this]()
    {
        this->showPuzzleScreen();
    });
    connect(this->ui->networkButton, &QPushButton::clicked, this, [this]()
    {
        bool usernameOK = true;
        if (this->getConfigVariable("notNetworkFirstStart").isEmpty())
        {
            QInputDialog dlg{this};
            dlg.setInputMode(QInputDialog::TextInput);
            dlg.setOkButtonText(tr("OK"));
            dlg.setCancelButtonText(tr("Cancel"));
            dlg.setWindowTitle(tr("Username"));
            dlg.setLabelText( tr("It looks like this is the first time you are connecting to a multiplayer server. Please set the username you would like to use (you can change this later in the settings):"));
            dlg.setTextValue(getNewUsername());
            if (dlg.exec())
            {
                auto name = dlg.textValue();
                if(!name.isEmpty())
                {
                    this->setConfigVariable("notNetworkFirstStart", "1");
                    this->setConfigVariable("username", name.left(256));
                } else
                {
                    usernameOK = false;
                }
            }
            else
            {
                usernameOK = false;
            }
        }
        if (usernameOK)
        {
            this->ui->startNewPublicServerGamebutton->setEnabled(false);
            this->publicServerTimer->start();
            this->networkManager->connectToRelay(this->ui->publicServerAddressEdit->text());
            this->sendReqToPublicServer("gamelist", {});
            this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->publicServerPage), slidingStackedWidget::AnimationDirection::RightToLeft);
        }
    });
    connect(this->ui->reportBugButton, &QPushButton::clicked, this, [this]()
    {
        this->startErrorReport("main");
    });
    connect(this->ui->inGameBugreportButton, &QPushButton::clicked, this, [this]()
    {
        this->startErrorReport("game");
    });
    connect(this->ui->cancelErrorReportButton, &QPushButton::clicked, this, [this]()
    {
        if (this->errorReportBackLocation == "main")
        {
            this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->menuPage), slidingStackedWidget::AnimationDirection::LeftToRight);
        }
        else if (this->errorReportBackLocation == "game")
        {
            this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->gamePage), slidingStackedWidget::AnimationDirection::LeftToRight);
        }
    });
    connect(this->ui->saveErrorReportButton, &QPushButton::clicked, this, &mainWindow::saveErrorReport);
    connect(this->ui->backFromPublicServerPageButton, &QPushButton::clicked, this, [this]()
    {
        if (this->waitingForPublicGameJoin)
        {
            this->waitingForPublicGameJoin = false;
            this->networkManager->reset();
        }
        this->ui->startNewPublicServerGamebutton->setEnabled(false);
        this->publicServerTimer->stop();
        this->networkManager->resetRelay();
        this->resetMenuPage();
        this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->menuPage), slidingStackedWidget::AnimationDirection::LeftToRight);
    });
    connect(this->ui->settingsButton, &QPushButton::clicked, this, [this]()
    {
        this->setupSettingsUI();
        this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->settingsPage), slidingStackedWidget::AnimationDirection::RightToLeft);
    });
    connect(this->ui->accessSettingsButton, &QPushButton::clicked, this, [this]()
    {
        this->setupSettingsUI(1);
        this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->settingsPage), slidingStackedWidget::AnimationDirection::LeftToRight);
    });
    connect(this->ui->accessHelpButton, &QPushButton::clicked, this, [this]()
    {
        this->openHelp("ingame");
    });
    connect(this->ui->settingsFromLobbyButton, &QPushButton::clicked, this, [this]()
    {
        this->setupSettingsUI(2);
        this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->settingsPage), slidingStackedWidget::AnimationDirection::LeftToRight);
    });
    connect(this->ui->helpButton, &QPushButton::clicked, this, [this]()
    {
        this->openHelp("main");
    });
    connect(this->ui->helpContentsBrowser, &QTextBrowser::anchorClicked, this, [this](const QUrl & url)
    {
        QString page = url.toString().mid(url.toString().lastIndexOf(QChar('#')) + 1);
        QString lang = languageConfigValueToLocaleName(this->getConfigVariable("language"));
        if (lang.isEmpty() || getPrimaryPath(QString("help/" + lang + "_%1.html").arg(page), true).isEmpty()) lang = DEFAULT_LANGUAGE;
        this->ui->helpBrowser->setSource(QUrl::fromLocalFile(getPrimaryPath(QString("help/" + lang + "_%1.html").arg(page))));
    });
    connect(this->ui->backFromAboutButton, &QPushButton::clicked, this, [this]()
    {
        this->resetMenuPage();
        this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->menuPage), slidingStackedWidget::AnimationDirection::LeftToRight);
    });
    connect(this->ui->backFromTutorialPageButton, &QPushButton::clicked, this, [this]()
    {
        this->resetMenuPage();
        this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->menuPage), slidingStackedWidget::AnimationDirection::LeftToRight);
    });
    connect(this->ui->backFromNewGameButton, &QPushButton::clicked, this, [this]()
    {
        if (this->privateGame)
        {
            this->networkManager->reset();
            this->resetMenuPage();
            this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->menuPage), slidingStackedWidget::AnimationDirection::LeftToRight);
            this->resetStartGameScreen();
        }
        else
        {
            this->publicServerRemoveGame();
            this->disconnectFromCurrentNetworkGame();
            this->networkManager->connectToRelay(this->ui->publicServerAddressEdit->text());
            this->sendReqToPublicServer("gamelist", {});
            this->publicServerTimer->start();
            this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->publicServerPage), slidingStackedWidget::AnimationDirection::LeftToRight);
            this->resetStartGameScreen();
        }
    });
    connect(this->ui->backFromSettingsButton, &QPushButton::clicked, this, [this]()
    {
        if (this->settingsOpenSource == 0)
        {
            this->resetMenuPage();
            this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->menuPage), slidingStackedWidget::AnimationDirection::LeftToRight);
        }
        else if (this->settingsOpenSource == 1)
        {
            this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->gamePage), slidingStackedWidget::AnimationDirection::RightToLeft);
        }
        else if (this->settingsOpenSource == 2)
        {
            this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->newGamePage), slidingStackedWidget::AnimationDirection::RightToLeft);
        }
    });
    connect(this->ui->backFromHelpButton, &QPushButton::clicked, this, [this]()
    {
        if (this->helpOpenSource == "main")
        {
            this->resetMenuPage();
            this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->menuPage), slidingStackedWidget::AnimationDirection::LeftToRight);
        }
        else if (this->helpOpenSource == "ingame")
        {
            this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->gamePage), slidingStackedWidget::AnimationDirection::RightToLeft);
        }
    });

    this->ui->sideBarStackedWidget->setAnimation(QEasingCurve::InQuad);
    connect(this->ui->menuButton, &QPushButton::clicked, this, [this]()
    {
        this->ui->sideBarStackedWidget->slideInIndex(this->ui->sideBarStackedWidget->indexOf(this->ui->pausedPage), slidingStackedWidget::AnimationDirection::TopToBottom);
    });
    connect(this->ui->gameInfoButton, &QPushButton::clicked, this, [this]()
    {
        this->ui->sideBarStackedWidget->slideInIndex(this->ui->sideBarStackedWidget->indexOf(this->ui->boardControlPage), slidingStackedWidget::AnimationDirection::TopToBottom);
    });
    connect(this->ui->resumeButton, &QPushButton::clicked, this, [this]()
    {
        this->ui->sideBarStackedWidget->slideInIndex(this->ui->sideBarStackedWidget->indexOf(this->ui->sideBarPage), slidingStackedWidget::AnimationDirection::BottomToTop);
    });
    connect(this->ui->backToSideBarButton, &QPushButton::clicked, this, [this]()
    {
        this->ui->sideBarStackedWidget->slideInIndex(this->ui->sideBarStackedWidget->indexOf(this->ui->sideBarPage), slidingStackedWidget::AnimationDirection::BottomToTop);
    });
    connect(this->ui->executeScriptButton, &QPushButton::clicked, this, [this]()
    {
        this->ui->scriptResultEdit->appendPlainText(state->scriptEnv->executeScript(this->ui->scriptEditorEdit->toPlainText(), QString(), nullptr).constFirst().toString());
    });
    connect(this->ui->gameModeComboBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &mainWindow::gameModeComboBoxChanged);
    connect(this->ui->p1NickLineEdit, &QLineEdit::textChanged, this, &mainWindow::enableStartGameButtonIfPossible);
    connect(this->ui->p2NickLineEdit, &QLineEdit::textChanged, this, &mainWindow::enableStartGameButtonIfPossible);
    connect(this->ui->p3NickLineEdit, &QLineEdit::textChanged, this, &mainWindow::enableStartGameButtonIfPossible);
    connect(this->ui->p4NickLineEdit, &QLineEdit::textChanged, this, &mainWindow::enableStartGameButtonIfPossible);
    connect(this->ui->startGameButton, &QPushButton::clicked, this, [this]()
    {
        if (!this->ui->replayPlayBackCheckBox->isChecked())
        {
            if(this->hostMode && !this->privateGame)
            {
                bool allPlayersHost = true;
                if(this->ui->p1TypeComboBox->isVisible() && this->ui->p1TypeComboBox->currentIndex() > 0) allPlayersHost = false;
                if(this->ui->p2TypeComboBox->isVisible() && this->ui->p2TypeComboBox->currentIndex() > 0) allPlayersHost = false;
                if(this->ui->p3TypeComboBox->isVisible() && this->ui->p3TypeComboBox->currentIndex() > 0) allPlayersHost = false;
                if(this->ui->p4TypeComboBox->isVisible() && this->ui->p4TypeComboBox->currentIndex() > 0) allPlayersHost = false;
                if(allPlayersHost)
                {
                    if (mainWindow::msgBoxQuestion(this, tr("All sides controlled by host"), tr("You are about to start a network game where all sides are controlled by the host. Do you want to start the game like this?")) == QMessageBox::No)
                    {
                        return;
                    }
                }
            }
            if (this->initializeGame(this->loadedGameName, QString(), time(nullptr), true))
            {
                this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->gamePage), slidingStackedWidget::AnimationDirection::RightToLeft);
                this->updateCurrentPlayerAndScoreLabels();
            }
        }
        else
        {
            this->replayFromSave(this->loadedGameName);
        }
    });
    connect(this->ui->abortButton, &QPushButton::clicked, this, [this]()
    {
        if (mainWindow::msgBoxQuestion(this, tr("Leave game"), tr("Are you sure you want to leave the game?")) == QMessageBox::Yes)
        {
            this->endGame();
        }
    });
    connect(this->ui->startPuzzleButton, &QPushButton::clicked, this, [this]()
    {
        if (this->ui->puzzleSelectorTreeWidget->selectedItems().size() == 1)
        {
            auto p = this->ui->puzzleSelectorTreeWidget->indexOfTopLevelItem(this->ui->puzzleSelectorTreeWidget->selectedItems().first());
            cfgFile cfg(this->puzzles[p].absoluteFilePath());
            this->lastPuzzleScript = getPrimaryPath("puzzles/" + cfg.readValues("ai").constFirst());
            this->lastPuzzleSaveFile = getPrimaryPath("puzzles/" + cfg.readValues("save").constFirst());
            this->loadGameDirectly(this->lastPuzzleSaveFile, this->lastPuzzleScript);
        }
    });
    connect(this->ui->tutorialNextButton, &QPushButton::clicked, this, [this]()
    {
        this->currTutorialPage++;
        this->ui->tutorialNextButton->setEnabled(this->currTutorialPage < this->detectTutorialPageCount());
        this->ui->tutorialPreviousButton->setEnabled(this->currTutorialPage > 0);
        this->ui->tryoutButton->setVisible(!getGameFileForTutorialPage(this->currTutorialPage).first.isEmpty());
        QString lang = languageConfigValueToLocaleName(this->getConfigVariable("language"));
        if (lang.isEmpty() || tutorialPageCountToFilename(lang, currTutorialPage).isEmpty()) lang = DEFAULT_LANGUAGE;
        this->ui->tutorialBrowser->setPixmap(tutorialPageCountToFilename(lang, this->currTutorialPage));
    });
    connect(this->ui->tutorialPreviousButton, &QPushButton::clicked, this, [this]()
    {
        this->currTutorialPage--;
        this->ui->tutorialNextButton->setEnabled(this->currTutorialPage < this->detectTutorialPageCount());
        this->ui->tutorialPreviousButton->setEnabled(this->currTutorialPage > 0);
        this->ui->tryoutButton->setVisible(!getGameFileForTutorialPage(this->currTutorialPage).first.isEmpty());
        QString lang = languageConfigValueToLocaleName(this->getConfigVariable("language"));
        if (lang.isEmpty() || tutorialPageCountToFilename(lang, currTutorialPage).isEmpty()) lang = DEFAULT_LANGUAGE;
        this->ui->tutorialBrowser->setPixmap(tutorialPageCountToFilename(lang, this->currTutorialPage));
    });
    connect(this->ui->tryoutButton, &QPushButton::clicked, this, [this]()
    {
        auto gameData = this->getGameFileForTutorialPage(this->currTutorialPage);
        if (!gameData.first.isEmpty())
        {
            this->loadGameDirectly(gameData.first, gameData.second);
            this->backToTutorialFromGame = true;
        }
    });
    connect(this->ui->accessBoardsDecksButton, &QPushButton::clicked, this, [this]()
    {
        this->showEditor(this->ui->gamePage, "deck", this->ui->boardsTreeWidget->selectedItems(), this->ui->decksTreeWidget->selectedItems(), false, false);
    });
    connect(this->ui->contHotSeatButton, &QPushButton::clicked, this, [this]()
    {
        connect(this->ui->stackedWidget, &slidingStackedWidget::animationFinished,
                this, &mainWindow::contHotSeat);
        this->ui->stackedWidget->slideInWidget(this->ui->gamePage, slidingStackedWidget::AnimationDirection::RightToLeft);
    });
    connect(this->ui->quitButton, &QPushButton::clicked, this, &mainWindow::close);
    connect(this->ui->saveButton, &QPushButton::clicked, this, &mainWindow::saveGame);
    connect(this->ui->tutorialButton, &QPushButton::clicked, this, [this]()
    {
        this->currTutorialPage = 0;
        QString lang = languageConfigValueToLocaleName(this->getConfigVariable("language"));
        if (lang.isEmpty() || tutorialPageCountToFilename(lang, currTutorialPage).isEmpty()) lang = DEFAULT_LANGUAGE;
        this->ui->tutorialBrowser->setPixmap(tutorialPageCountToFilename(lang, this->currTutorialPage));
        this->ui->tutorialNextButton->setEnabled(this->currTutorialPage < this->detectTutorialPageCount());
        this->ui->tutorialPreviousButton->setEnabled(this->currTutorialPage > 0);
        this->ui->tryoutButton->setVisible(!getGameFileForTutorialPage(this->currTutorialPage).first.isEmpty());
        this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->tutorialPage), slidingStackedWidget::AnimationDirection::RightToLeft);
    });

    auto mainMenuButtons = {this->ui->helpButton, this->ui->tutorialButton, this->ui->newGameButton,
                            this->ui->networkButton, this->ui->puzzleGameButton, this->ui->editorButton,
                            this->ui->settingsButton};
    int mainMenuButtonWidth = 200;
    for(const auto button : mainMenuButtons) {
        auto width = std::max(button->sizeHint().width(),
                              button->fontMetrics().boundingRect(button->text()).width()+button->width()-button->contentsRect().width());
        if(width > mainMenuButtonWidth) mainMenuButtonWidth = width;
    }
    for(auto button: mainMenuButtons) {
        button->setFixedWidth(mainMenuButtonWidth+8);
    }

    if (this->getConfigVariable("fullScreen").toInt())
    {
        this->setWindowState(Qt::WindowFullScreen);
    }
}

bool mainWindow::initializeGame(const QString& fileName, const QString& script, int rngSeed, bool load_from_ui)
{
    this->sendReqToPublicServer("setgameongoing", QStringList() << this->secretKey);
    if (!this->disableLobbyUpdates) this->sendReqToPublicServer("updategamedata", (QStringList() << this->secretKey) + getGameDataUpdateForPublicServer());
    this->gameInProgress = true;
    auto savedGUID = card::globalUID;
    random.seedRNGs(rngSeed);
    this->doNotSendGameEvents = true;
    this->inventoryTabStack.clear();
    this->updateObserverList();
    disconnect(this, &mainWindow::cancelButtonClicked, this->state->storage, &cardInventory::cancelChoice);
    disconnect(this, &mainWindow::cancelButtonClicked, this->state->p1Inventory, &cardInventory::cancelChoice);
    disconnect(this, &mainWindow::cancelButtonClicked, this->state->p2Inventory, &cardInventory::cancelChoice);
    disconnect(this, &mainWindow::cancelButtonClicked, this->state->p3Inventory, &cardInventory::cancelChoice);
    disconnect(this, &mainWindow::cancelButtonClicked, this->state->p4Inventory, &cardInventory::cancelChoice);
    disconnect(this, &mainWindow::cancelButtonClicked, this->state->huckster, &cardInventory::cancelChoice);
    disconnect(this, &mainWindow::cancelButtonClicked, this->state->trash, &cardInventory::cancelChoice);
    disconnect(this, &mainWindow::cancelButtonClicked, this->state->mainInventory, &cardInventory::cancelChoice);
    disconnect(this->ui->endTurnButton, &QPushButton::clicked, nullptr, nullptr);
    disconnect(this, &mainWindow::cancelButtonClicked, this->state->gameBoard, &board::cancelChoice);
    //TODO: needed? disconnect(this->state->storage, &cardInventory::changed, this, &mainWindow::updateCardLabels);
    disconnect(this->state->p1Inventory, &cardInventory::changed, this, &mainWindow::updateCardLabels);
    disconnect(this->state->p2Inventory, &cardInventory::changed, this, &mainWindow::updateCardLabels);
    disconnect(this->state->p3Inventory, &cardInventory::changed, this, &mainWindow::updateCardLabels);
    disconnect(this->state->p4Inventory, &cardInventory::changed, this, &mainWindow::updateCardLabels);
    disconnect(this->state->huckster, &cardInventory::changed, this, &mainWindow::updateCardLabels);
    disconnect(this->state->trash, &cardInventory::changed, this, &mainWindow::updateCardLabels);
    disconnect(this->state->mainInventory, &cardInventory::changed, this, &mainWindow::updateCardLabels);
    disconnect(this->ui->sideBarTabWidget, &QTabWidget::currentChanged, this, &mainWindow::updateDisabledCardsInInventories);

    this->state->deleteLater();
    QApplication::sendPostedEvents(nullptr, QEvent::DeferredDelete);
    QApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
    this->state = new gameState(this);

    connect(this, &mainWindow::cancelButtonClicked, this->state->storage, &cardInventory::cancelChoice);
    connect(this, &mainWindow::cancelButtonClicked, this->state->p1Inventory, &cardInventory::cancelChoice);
    connect(this, &mainWindow::cancelButtonClicked, this->state->p2Inventory, &cardInventory::cancelChoice);
    connect(this, &mainWindow::cancelButtonClicked, this->state->p3Inventory, &cardInventory::cancelChoice);
    connect(this, &mainWindow::cancelButtonClicked, this->state->p4Inventory, &cardInventory::cancelChoice);
    connect(this, &mainWindow::cancelButtonClicked, this->state->huckster, &cardInventory::cancelChoice);
    connect(this, &mainWindow::cancelButtonClicked, this->state->trash, &cardInventory::cancelChoice);
    connect(this, &mainWindow::cancelButtonClicked, this->state->mainInventory, &cardInventory::cancelChoice);
    connect(this->ui->endTurnButton, &QPushButton::clicked, this, [this]()
    {
        this->state->endTurnButtonClicked(false, false);
    });
    connect(this, &mainWindow::cancelButtonClicked, this->state->gameBoard, &board::cancelChoice);
    //TODO: needed? connect(this->state->storage, &cardInventory::changed, this, &mainWindow::updateCardLabels);
    connect(this->state->p1Inventory, &cardInventory::changed, this, &mainWindow::updateCardLabels);
    connect(this->state->p2Inventory, &cardInventory::changed, this, &mainWindow::updateCardLabels);
    connect(this->state->p3Inventory, &cardInventory::changed, this, &mainWindow::updateCardLabels);
    connect(this->state->p4Inventory, &cardInventory::changed, this, &mainWindow::updateCardLabels);
    connect(this->state->huckster, &cardInventory::changed, this, &mainWindow::updateCardLabels);
    connect(this->state->trash, &cardInventory::changed, this, &mainWindow::updateCardLabels);
    connect(this->state->mainInventory, &cardInventory::changed, this, &mainWindow::updateCardLabels);
    connect(this->ui->sideBarTabWidget, &QTabWidget::currentChanged, this, &mainWindow::updateDisabledCardsInInventories);

    const bool load_from_save = !fileName.isEmpty();
    fileFormat::saveFile f;
    if (load_from_save)
    {
        if (fileName == ":::::")
        {
            if (!f.fromString(this->loadedGameFileContent, false))
            {
                this->endGame();
                return false;
            }
        }
        else
        {
            if (!f.loadFromFile(fileName, false))
            {
                this->endGame();
                return false;
            }
        }
    }

    if (!load_from_save)
    {
        this->playerNicknames = QStringList()
                                << this->ui->p1NickLineEdit->text()
                                << this->ui->p2NickLineEdit->text()
                                << this->ui->p3NickLineEdit->text()
                                << this->ui->p4NickLineEdit->text();
    }
    else
    {
        this->playerNicknames = QStringList()
                                << (this->ui->p1NickLineEdit->text().isEmpty() ? f.getItemByKey("playerNicknames").getAttributeAsString("u0") : this->ui->p1NickLineEdit->text())
                                << (this->ui->p2NickLineEdit->text().isEmpty() ? f.getItemByKey("playerNicknames").getAttributeAsString("u1") : this->ui->p2NickLineEdit->text())
                                << (this->ui->p3NickLineEdit->text().isEmpty() ? f.getItemByKey("playerNicknames").getAttributeAsString("u2") : this->ui->p3NickLineEdit->text())
                                << (this->ui->p4NickLineEdit->text().isEmpty() ? f.getItemByKey("playerNicknames").getAttributeAsString("u3") : this->ui->p4NickLineEdit->text());
    }

    /* TODO
    if(s)
    {
        doNotProcessGameEvents = true;
        int evCounter = 0;
        while(true)
        {
            auto it = f.getItemByKey("event"+QString::number(evCounter)).getValueAsString();
            if(it.isEmpty())
            {
                break;
            } else
            {
                auto pkg = network::networkDataPackage(QByteArray::fromBase64(it.toUtf8()));
                this->handleGameEvent(pkg);
            }
            evCounter++;
        }
        doNotProcessGameEvents = false;
    }
    */

    this->ui->eyeLabel->setToolTip({});
    this->ui->p1Inventory->setViewable(true);
    this->ui->p2Inventory->setViewable(true);
    this->ui->p3Inventory->setViewable(true);
    this->ui->p4Inventory->setViewable(true);
    this->ui->storage->setViewable(false);
    int playerTabMaxWidth = 100;
    setElidedTabText(this->ui->sideBarTabWidget,
                     this->ui->sideBarTabWidget->indexOf(this->ui->tab_p1Inventory),
                     this->playerNicknames[0],
                     playerTabMaxWidth);
    setElidedTabText(this->ui->sideBarTabWidget,
                     this->ui->sideBarTabWidget->indexOf(this->ui->tab_p2Inventory),
                     this->playerNicknames[1],
                     playerTabMaxWidth);
    setElidedTabText(this->ui->sideBarTabWidget,
                     this->ui->sideBarTabWidget->indexOf(this->ui->tab_p3Inventory),
                     this->playerNicknames[2],
                     playerTabMaxWidth);
    setElidedTabText(this->ui->sideBarTabWidget,
                     this->ui->sideBarTabWidget->indexOf(this->ui->tab_p4Inventory),
                     this->playerNicknames[3],
                     playerTabMaxWidth);
    this->ui->sideBarTabWidget->setTabText(this->ui->sideBarTabWidget->indexOf(this->ui->tab_Storage), tr("Storage"));
    this->ui->sideBarTabWidget->setTabText(this->ui->sideBarTabWidget->indexOf(this->ui->tab_Huckster), tr("Huckster"));
    this->ui->sideBarTabWidget->setTabText(
        this->ui->sideBarTabWidget->indexOf(this->ui->tab_Trash), tr("Trash"));
    this->ui->sideBarTabWidget->setTabText(
        this->ui->sideBarTabWidget->indexOf(this->ui->tab_MainInventory), tr("Main deck"));
    this->startingDeck = !load_from_save ? this->getLobbySelectedDeck()
                         : f.getItemByKey("deck").getValueAsString();
    if (!load_from_save)
    {
        this->cardPictureSet = this->decks[this->startingDeck].readValue("pictureSet");
        this->cardNamePictureSet = this->decks[this->startingDeck].readValue("namePictureSet");
    }
    else
    {
        this->cardPictureSet = f.getItemByKey("cardPictureSet").getValueAsString();
        this->cardNamePictureSet = f.getItemByKey("cardNamePictureSet").getValueAsString();
    }

    this->uiDisabledCounter = 0;
    this->ui->splitter->setSizes(QList<int>() << 4 << 1);
    this->crdRedrawInProgress = false;
    for (auto& crd : this->displayCardsCache) crd->deleteCardLater();
    this->displayCardsCache.clear();
    this->ui->sideBarStackedWidget->setCurrentWidget(this->ui->sideBarPage);

    int gameMode = !load_from_save ? this->ui->gameModeComboBox->currentIndex() : mainWindow::gameModeToIdx(f.getItemByKey("gameMode").getValueAsString().toInt());

    if (gameMode != 2)
    {
        this->ui->p1ScoreLabel->setText("0");
        this->ui->p2ScoreLabel->setText("0");
        this->ui->p3ScoreLabel->setText("0");
        this->ui->p4ScoreLabel->setText("0");
    }
    else
    {
        this->ui->p1ScoreLabel->setText("0 (0)");
        this->ui->p2ScoreLabel->setText("0 (0)");
        this->ui->p3ScoreLabel->setText("0 (0)");
        this->ui->p4ScoreLabel->setText("0 (0)");
    }
    this->ui->p1CardsLabel->setText("0");
    this->ui->p2CardsLabel->setText("0");
    this->ui->p3CardsLabel->setText("0");
    this->ui->p4CardsLabel->setText("0");

    this->ui->chatTabWidget->setCurrentIndex(1);

    if (!this->hostMode || this->ui->lobbyConnectedUserList->count())
    {
        this->ui->splitter->setSizes(QList<int>() << 4 << 1);
    }
    else
    {
        this->ui->splitter->setSizes(QList<int>() << 1000 << 0);
    }
    this->ui->chatMsgLineEdit->clear();
    this->ui->chatHistoryTextEdit->clear();
    this->ui->scriptEditorEdit->clear();
    this->ui->scriptResultEdit->clear();

    if (gameMode == 0)
    {
        this->ui->p1NameLabel->setVisible(true);
        this->ui->p2NameLabel->setVisible(true);
        this->ui->p3NameLabel->setVisible(false);
        this->ui->p4NameLabel->setVisible(false);
        this->ui->p1CardsLabel->setVisible(true);
        this->ui->p2CardsLabel->setVisible(true);
        this->ui->p3CardsLabel->setVisible(false);
        this->ui->p4CardsLabel->setVisible(false);
        this->ui->p1ScoreLabel->setVisible(true);
        this->ui->p2ScoreLabel->setVisible(true);
        this->ui->p3ScoreLabel->setVisible(false);
        this->ui->p4ScoreLabel->setVisible(false);
    }
    else if (gameMode == 1)
    {
        this->ui->p1NameLabel->setVisible(true);
        this->ui->p2NameLabel->setVisible(false);
        this->ui->p3NameLabel->setVisible(true);
        this->ui->p4NameLabel->setVisible(false);
        this->ui->p1CardsLabel->setVisible(true);
        this->ui->p2CardsLabel->setVisible(false);
        this->ui->p3CardsLabel->setVisible(true);
        this->ui->p4CardsLabel->setVisible(false);
        this->ui->p1ScoreLabel->setVisible(true);
        this->ui->p2ScoreLabel->setVisible(false);
        this->ui->p3ScoreLabel->setVisible(true);
        this->ui->p4ScoreLabel->setVisible(false);
    }
    else if (gameMode == 2)
    {
        this->ui->p1NameLabel->setVisible(true);
        this->ui->p2NameLabel->setVisible(true);
        this->ui->p3NameLabel->setVisible(true);
        this->ui->p4NameLabel->setVisible(true);
        this->ui->p1CardsLabel->setVisible(true);
        this->ui->p2CardsLabel->setVisible(true);
        this->ui->p3CardsLabel->setVisible(true);
        this->ui->p4CardsLabel->setVisible(true);
        this->ui->p1ScoreLabel->setVisible(true);
        this->ui->p2ScoreLabel->setVisible(true);
        this->ui->p3ScoreLabel->setVisible(true);
        this->ui->p4ScoreLabel->setVisible(true);
    }
    else if (gameMode == 3)
    {
        this->ui->p1NameLabel->setVisible(true);
        this->ui->p2NameLabel->setVisible(true);
        this->ui->p3NameLabel->setVisible(true);
        this->ui->p4NameLabel->setVisible(false);
        this->ui->p1CardsLabel->setVisible(true);
        this->ui->p2CardsLabel->setVisible(true);
        this->ui->p3CardsLabel->setVisible(true);
        this->ui->p4CardsLabel->setVisible(false);
        this->ui->p1ScoreLabel->setVisible(true);
        this->ui->p2ScoreLabel->setVisible(true);
        this->ui->p3ScoreLabel->setVisible(true);
        this->ui->p4ScoreLabel->setVisible(false);
    }
    else if (gameMode == 4)
    {
        this->ui->p1NameLabel->setVisible(true);
        this->ui->p2NameLabel->setVisible(true);
        this->ui->p3NameLabel->setVisible(true);
        this->ui->p4NameLabel->setVisible(true);
        this->ui->p1CardsLabel->setVisible(true);
        this->ui->p2CardsLabel->setVisible(true);
        this->ui->p3CardsLabel->setVisible(true);
        this->ui->p4CardsLabel->setVisible(true);
        this->ui->p1ScoreLabel->setVisible(true);
        this->ui->p2ScoreLabel->setVisible(true);
        this->ui->p3ScoreLabel->setVisible(true);
        this->ui->p4ScoreLabel->setVisible(true);
    }
    else
    {
        reportError(tr("Error: invalid game mode, game aborted"));
        this->endGame();
        return false;
    }

    auto cardList = getDeckCardList(startingDeck);
    for (const auto& p : cardList.first)
    {
        if (!this->displayCardsCache.count(p.first)) this->displayCardsCache[p.first] = new card(p.first, -1, this->state, -2);
    }
    for (const auto& p : cardList.second)
    {
        if (!this->displayCardsCache.count(p.first)) this->displayCardsCache[p.first] = new card(p.first, -1, this->state, -2);
    }

    this->state->loadGame(f, script, load_from_ui);

    this->ui->currentPlayerLabel->clear();

    if (!this->state->hucksterEnabled)
    {
        this->ui->sideBarTabWidget->removeTab(this->ui->sideBarTabWidget->indexOf(this->ui->tab_Huckster));
    }

    this->setUIDisabledForAnimation(false);
    this->updateEndTurnButtonState();
    this->updateScoreTable();

    if (this->state->isLocalUser(this->state->getCurrentPlayer()))
    {
        this->switchToPlayerInventory(this->state->getCurrentPlayer());
    }
    else
    {
        if (this->isObserver())
        {
            this->switchToPlayerInventory(this->state->getCurrentPlayer());
        }
        else
        {
            if (this->state->isLocalUser(1) && this->ui->sideBarTabWidget->indexOf(this->ui->tab_p1Inventory) != -1)
            {
                this->switchToPlayerInventory(1);
            }
            else if (this->state->isLocalUser(2) && this->ui->sideBarTabWidget->indexOf(this->ui->tab_p2Inventory) != -1)
            {
                this->switchToPlayerInventory(2);
            }
            else if (this->state->isLocalUser(3) && this->ui->sideBarTabWidget->indexOf(this->ui->tab_p3Inventory) != -1)
            {
                this->switchToPlayerInventory(3);
            }
            else if (this->state->isLocalUser(4) && this->ui->sideBarTabWidget->indexOf(this->ui->tab_p4Inventory) != -1)
            {
                this->switchToPlayerInventory(4);
            }
        }
    }

    if (load_from_save)
    {
        this->updateDisplayedLogs();
    }

    if (this->isPassiveTurn())
    {
        this->disableGUIForPassiveTurn();
    }

    for (auto& crd : state->gameBoard->getAllCards())
    {
        static_cast<card*>(crd)->redraw();
    }

    doNotSendGameEvents = false;
    QString saveFileContent;
    if (!this->loadedGameName.isEmpty())
    {
        QFile f(this->loadedGameName);
        if (f.open(QFile::ReadOnly))
        {
            QTextStream in(&f);
            saveFileContent = in.readAll();
            f.close();
        }
        else
        {
            mainWindow::reportError(tr("Error: can't read saved game"));
            this->endGame();
            return false;
        }
    }
    if (this->hostMode)
    {
        this->sendGameEvent(QStringList() << "updateRandomBoardDeckSelection" << this->randomBoard << this->randomDeck, true);
        this->sendGameEvent(QStringList() << "initializeGame" << QString::number(savedGUID) << QString::number(rngSeed) << saveFileContent, true);
    }
    if (this->state->isAITurn())
    {
        QTimer::singleShot(2000, this, [this]()
        {
            this->state->aiTurn();
        });
    }

    return true;
}

void mainWindow::startTitleSequence()
{
    if (autoCfg.hasKey("skipToStartGame") && autoCfg.readValue("skipToStartGame") == "true")
    {
        this->resetMenuPage();
        this->titleSequenceFinished();
        this->ui->newGameButton->click();
        return;
    }
    connect(this->ui->titleLabel, &fadingLabel::done, this, &mainWindow::titleSequenceFinished);
    this->ui->menuPage->setGeometry(this->ui->titlePage->geometry());
    QPixmap* img = new QPixmap(this->ui->menuPage->size());
    QPainter painter(img);
    this->resetMenuPage();
    this->ui->menuPage->render(&painter);
#ifdef LONG_INTRO
#ifndef Q_OS_ANDROID
    this->ui->titleLabel->setData(QStringList() << "/img/license_logos.png" << "/img/title_screen_logo.png", QList<int>() << 1800 << 200 << 700 << 1500 << 2600 << 700 << 850, img);
    //this->ui->titleLabel->setData(QStringList() << "/img/license_logos.png", QList<int>() << 1800 << 500 << 850 << 850, img);
#else
    this->ui->titleLabel->setData(QStringList() << "/img/license_logos.png" << "/img/title_screen_logo.png", QList<int>() << 900 << 100 << 250 << 750 << 900 << 250 << 425, img);
    //this->ui->titleLabel->setData(QStringList() << "/img/license_logos.png", QList<int>() << 1800 << 500 << 850 << 850, img);
#endif
#else
    this->ui->titleLabel->setData(QStringList() << "/img/title_screen_logo.png", QList<int>() << 4000 << 500 << 1400 << 850, img);
#endif
    if (this->getConfigVariable("enableMusic").toInt())
    {
        QTimer::singleShot(0, this, [this](){
#ifdef Q_OS_WIN
        this->mPlayer->loadPlayList(getPrimaryPath("music/default_playlist_wma.txt"));
#else
        this->mPlayer->loadPlayList(getPrimaryPath("music/default_playlist.txt"));
#endif
        this->mPlayer->setMaxRandomSilence(0);
        this->mPlayer->start();
        this->mPlayer->shuffle();
        this->mPlayer->setMaxRandomSilence(180000); //3 mins
        });
    }
    this->ui->titleLabel->start();
}

void mainWindow::drawOrientationOnSideBarCardGV()
{
    if (this->currentlyDisplayedOnSideBarID.isEmpty())
    {
        this->ui->sideBarCardGV->scene()->clear();
        auto item = new QGraphicsPixmapItem(QPixmap(getPrimaryPath(QString("img/sidebar_p%1_g%2.png").arg(QString::number(this->state->getCurrentPlayer()), QString::number(this->state->getGameMode())))));
        this->ui->sideBarCardGV->scene()->addItem(item);
        item->setPos(this->ui->sideBarCardGV->sceneRect().center() - item->boundingRect().center());
    }
}

void mainWindow::setReplayMode(bool replayMode)
{
    this->replayMode = replayMode;
    this->ui->replayWidget->setVisible(replayMode);
    if (replayMode)
    {
        if (!this->replayTimer->isActive())
        {
            this->replayTimer->setInterval(1000);
            this->replayTimer->start();
        }
    }
    else
    {
        this->replayTimer->stop();
        this->fastForward = false;
        this->ui->nextStepButton->setVisible(false);
        this->ui->stepByStepButton->setChecked(false);
        this->ui->normalSpeedButton->setChecked(true);
        this->ui->fastForwardButton->setChecked(false);
        this->ui->fastForwardButton->setEnabled(true);
        this->ui->stepByStepButton->setEnabled(true);
        this->ui->normalSpeedButton->setEnabled(false);
    }
}

void mainWindow::updateCardLabels()
{
    this->sendGameEvent(QStringList() << "updateCardLabels");
    this->ui->p1CardsLabel->setText(tr("%1/%2").arg(QString::number(this->state->p1Inventory->getAllCardsInternal().size()), QString::number(this->state->gameBoard->getNumberOfPlayerCards(1))));
    this->ui->p2CardsLabel->setText(tr("%1/%2").arg(QString::number(this->state->p2Inventory->getAllCardsInternal().size()), QString::number(this->state->gameBoard->getNumberOfPlayerCards(2))));
    this->ui->p3CardsLabel->setText(tr("%1/%2").arg(QString::number(this->state->p3Inventory->getAllCardsInternal().size()), QString::number(this->state->gameBoard->getNumberOfPlayerCards(3))));
    this->ui->p4CardsLabel->setText(tr("%1/%2").arg(QString::number(this->state->p4Inventory->getAllCardsInternal().size()), QString::number(this->state->gameBoard->getNumberOfPlayerCards(4))));
    this->ui->cardNumInfoLabel->setText(tr("Items in deck/trash: %1/%2").arg(QString::number(this->state->mainInventory->getAllCardsInternal().size()), QString::number(this->state->trash->getAllCardsInternal().size())));
}

void mainWindow::displayCardOnSideBar(const QString& id, bool delayed)
{
    if (delayed && id.isEmpty())
    {
        this->drawOrientationOnSidebarDelayTimer->stop();
        this->drawOrientationOnSidebarDelayTimer->setSingleShot(true);
        this->drawOrientationOnSidebarDelayTimer->setInterval(160);
        this->drawOrientationOnSidebarDelayTimer->start();
        return;
    }
    else
    {
        this->drawOrientationOnSidebarDelayTimer->stop();
    }

    if (!this->currentlyDisplayedOnSideBarID.isEmpty())
    {
        this->displayCardsCache[this->currentlyDisplayedOnSideBarID]->removeFromScene(this->ui->sideBarCardGV->scene());
    }
    this->ui->sideBarCardGV->scene()->clear();
    this->currentlyDisplayedOnSideBarID = id;
    if (id == "")
    {
        this->drawOrientationOnSideBarCardGV();
        return;
    }
    this->displayCardsCache[id]->draw(this->ui->sideBarCardGV->scene(), DrawingType::Full, QPointF(0, 0));
}


void mainWindow::resetMenuPage()
{
    this->updateTipOfTheDay();
}

void mainWindow::filter(const QString& text, QTreeWidget* widget)
{
    QTreeWidgetItemIterator it_init(widget);
    while (*it_init)
    {
        (*it_init)->setHidden(!text.isEmpty());
        ++it_init;
    }
    if (text.isEmpty()) return;
    QTreeWidgetItemIterator it(widget);
    while (*it)
    {
        (*it)->setHidden(!(*it)->text(0).contains(text, Qt::CaseInsensitive));
        if ((*it)->parent())
        {
            (*it)->parent()->setHidden((*it)->parent()->isHidden() && (*it)->isHidden());
            if(!(*it)->isHidden()) (*it)->parent()->setExpanded(true);
        }
        ++it;
    }
}

void mainWindow::updateTipOfTheDay()
{
    QString lang = languageConfigValueToLocaleName(this->getConfigVariable("language"));
    if (lang.isEmpty() || getPrimaryPath("translations/" + lang + "/tips.txt", true).isEmpty()) lang = DEFAULT_LANGUAGE;
    QFile tipsFile(getPrimaryPath("translations/" + lang + "/tips.txt"));
    if (tipsFile.open(QFile::ReadOnly))
    {
        QTextStream in(&tipsFile);
        //in.setCodec("UTF-8");
        QStringList tipList = in.readAll().split(lineEnd, Qt::SkipEmptyParts);
        int random;
        do
        {
            random = rand() % tipList.size();
        }
        while (this->ui->tipLabel->text() == tipList[random] && tipList.size() > 1);
        this->ui->tipLabel->setText(tipList[random]);
    }
    else
    {
        this->ui->tipLabel->setVisible(false);
        this->ui->nextTipButton->setVisible(false);
    }
}

void mainWindow::updatePublicServerPage(const QStringList& pubServerData)
{
    auto idx = this->ui->publicGameList->indexAt(QPoint(0, 0)).row();

    this->ui->publicGameList->setVisible(false);
    this->ui->publicServerNetworkInfoLabel->setText(tr("Loading..."));
    this->ui->publicGameList->clear();
    this->ui->publicServerNetworkInfoLabel->setVisible(true);

    auto createPublGameWidget = [&](const QString & name, const QString & starttime, const QString & hostusername, const QString & desc, const QString & gameid, const QString & gamestatus,
                                    bool tournament, bool openForAnyone, const QString & board, const QString & deck, int gameMode, bool passwordRequired) -> QWidget *
    {
        QWidget* base = new QWidget;
        base->setObjectName("publicGameInfoWidget");
        base->setLayout(new QHBoxLayout);
        QVBoxLayout* textInfoLayout = new QVBoxLayout;

        QString gameTitle = QString("<b><big><big>%1</big></big></b>").arg(name);

        QDateTime timestamp;
        timestamp.setTime(QTime::fromMSecsSinceStartOfDay(starttime.toInt()*1000));
        QString started = tr("Created by %1 at %2").arg(hostusername, timestamp.toString());

        QString gameDesc = tr("Description: %1").arg(desc);

        auto statusLabel = new QLabel{};
        if (gamestatus == "ongoing")
        {
            statusLabel->setText(tr("Status: ongoing"));
        }
        else
        {
            statusLabel->setText(tr("Status: in lobby (game not started yet)"));
        }

        bool joinDisabled = !isBoardDeckValid(board, deck);
        QString gameModeText = tr("Game mode: %1").arg(textForGameMode(gameMode));
        QString boardText = tr("Board: %1").arg(textForBoard(board));
        QString deckText = tr("Deck: %1").arg(textForDeck(deck));
        QString boardPreviewPixmapPath = getBoardPreviewPixmapPath(board);

        auto titleLabel = new QLabel(gameTitle);
        auto boardPreviewLabel = new QLabel{};
        boardPreviewLabel->setPixmap(boardPreviewPixmapPath);
        boardPreviewLabel->setMinimumSize(75, 75);
        titleLabel->setObjectName("publicGameTitleLabel");
        textInfoLayout->addWidget(titleLabel);
        textInfoLayout->addWidget(new QLabel(started));
        textInfoLayout->addWidget(statusLabel);
        if (passwordRequired) textInfoLayout->addWidget(new QLabel(tr("This game is password protected.")));
        if (openForAnyone) textInfoLayout->addWidget(new QLabel(tr("Anyone is welcome to join this game.")));
        if (tournament) textInfoLayout->addWidget(new QLabel(tr("This game is part of a tournament.")));
        auto gameInfoLayout = new QHBoxLayout{};
        auto leftGInfoLayout = new QVBoxLayout{};
        auto rightGInfoLayout = new QVBoxLayout{};
        gameInfoLayout->addLayout(leftGInfoLayout);
        gameInfoLayout->addLayout(rightGInfoLayout);
        gameInfoLayout->addStretch(1);
        leftGInfoLayout->addWidget(boardPreviewLabel, 1, Qt::AlignVCenter);
        leftGInfoLayout->addWidget(new QLabel{}); //was unable to get boardPreviewLabel to show correctly without adding these two dummy labels
        leftGInfoLayout->addWidget(new QLabel{});
        rightGInfoLayout->addWidget(new QLabel(gameModeText));
        rightGInfoLayout->addWidget(new QLabel(boardText));
        rightGInfoLayout->addWidget(new QLabel(deckText));
        rightGInfoLayout->addStretch(1);
        if (!desc.isEmpty()) textInfoLayout->addWidget(new QLabel(gameDesc));
        textInfoLayout->addLayout(gameInfoLayout);
        textInfoLayout->addStretch(1);

        static_cast<QHBoxLayout*>(base->layout())->addLayout(textInfoLayout);

        auto joinText = tr("Join");
        if (passwordRequired) joinText = tr("Join with password");
        auto joinBtn = new QPushButton(joinText);
        joinBtn->setEnabled(!joinDisabled);
        connect(joinBtn, &QPushButton::clicked, this, [&, hostusername, gameid, passwordRequired]()
        {
            this->startWaitingForPublicGameJoin();
            this->hostMode = false;
            this->hostUsername = hostusername;
            this->gameIDToJoin = gameid;
            this->networkManager->reset(false);
            if (passwordRequired)
            {
                this->publicServerTimer->stop();
                this->ui->passwordPagePasswordEdit->clear();
                this->ui->joinPublicGameWithPasswordButton->setEnabled(true);
                this->ui->passwordPagePasswordEdit->setEnabled(true);
                this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->passwordPage), slidingStackedWidget::AnimationDirection::RightToLeft);
            }
            else
            {
                this->networkManager->setCurrentPublicGameID(gameid, {});
                this->networkManager->start(network::NetworkGameMode::ClientMode, hostusername, {});
            }
        });
        joinBtn->setShortcut({});
        base->layout()->addWidget(joinBtn);
        base->layout()->setAlignment(joinBtn, Qt::AlignRight | Qt::AlignBottom);
        if (!mainWindow::getConfigVariable("serverAdminKey").isEmpty())
        {
            auto removeBtn = new QPushButton("[ADMIN] Remove");
            base->layout()->addWidget(removeBtn);
            base->layout()->setAlignment(removeBtn, Qt::AlignRight | Qt::AlignBottom);
            connect(removeBtn, &QPushButton::clicked, this, [&, gameid]()
            {
                this->sendReqToPublicServer("removegameadmin", QStringList() << gameid << mainWindow::getConfigVariable("serverAdminKey"));
            });
        }
        return base;
    };

    if (pubServerData.isEmpty())
    {
        this->ui->publicServerNetworkInfoLabel->setText(tr("No ongoing games"));
    }
    else
    {
        this->ui->publicServerNetworkInfoLabel->setVisible(false);
        this->ui->publicGameList->setVisible(true);
    }
    for (int i = 0; i < pubServerData.size() / 12; ++i)
    {
        auto newW = createPublGameWidget(pubServerData[12 * i], pubServerData[12 * i + 1], pubServerData[12 * i + 2], pubServerData[12 * i + 3], pubServerData[12 * i + 4], pubServerData[12 * i + 5],
                                         pubServerData[12 * i + 6].toInt(), pubServerData[12 * i + 7].toInt(), pubServerData[12 * i + 8], pubServerData[12 * i + 9], pubServerData[12 * i + 10].toInt(), pubServerData[12 * i + 11].toInt());
        QListWidgetItem* it = new QListWidgetItem;
        it->setSizeHint(newW->sizeHint());
        this->ui->publicGameList->addItem(it);
        this->ui->publicGameList->setItemWidget(it, newW);
    }

    if (this->ui->publicGameList->count())
        this->ui->publicGameList->scrollToItem(this->ui->publicGameList->item(idx), QAbstractItemView::PositionAtTop);

    this->ui->startNewPublicServerGamebutton->setEnabled(true);
}

void mainWindow::lobbyChangedByUser(const QString& changeSource)
{
    this->updateLobbyBoardDeckSelector(changeSource);
    if (!this->disableLobbyUpdates)
    {
        this->sendReqToPublicServer("updategamedata", (QStringList() << this->secretKey) + getGameDataUpdateForPublicServer());
        this->networkManager->sendMessage(this->getLobbyState());
    }
}

void mainWindow::updateLobbyBoardDeckSelector(const QString& changeSource)
{
    if (changeSource == "other")
    {
    }
    else if (changeSource == "deck")
    {
        this->clearBoardDeckSelectorSidebar();
        this->setSelectedDeckInLobby(this->ui->decksTreeWidget->selectedItems().empty() ? nullptr : this->ui->decksTreeWidget->selectedItems().front(), true);
    }
    else if (changeSource == "board")
    {
        this->clearBoardDeckSelectorSidebar();
        this->setSelectedBoardInLobby(this->ui->boardsTreeWidget->selectedItems().empty() ? nullptr : this->ui->boardsTreeWidget->selectedItems().front(), true);
    }
}

void mainWindow::clearBoardDeckSelectorSidebar()
{
    this->ui->boardPreviewButton->setEnabled(false);
    this->ui->deckPreviewButton->setEnabled(false);
    this->ui->boardImgLabel->setPixmap(QPixmap(getPrimaryPath("img/unknown_board_img.png")));
    this->ui->deckDescLabel->setText(tr("No deck selected."));
    this->ui->boardDescLabel->setText(tr("No board selected."));
    this->updateNewGamePagePlayerSymbols({});
}

void mainWindow::setSelectedBoardInLobby(QTreeWidgetItem* it, bool noJump)
{
    bool block = this->ui->boardsTreeWidget->blockSignals(true);
    this->ui->boardsTreeWidget->clearSelection();
    if (it)
    {
        it->setSelected(true);
        this->setDeckListHighlight(it->data(0, Qt::UserRole).toString());
    }
    else
    {
        this->setDeckListHighlight("");
    }
    this->ui->decksTreeWidget->setEnabled(this->hostMode);
    if (!this->disableLobbyUpdates)
    {
        this->networkManager->sendMessage(this->getLobbyState());
        this->sendReqToPublicServer("updategamedata", (QStringList() << this->secretKey) + getGameDataUpdateForPublicServer());
    }
    this->updateBoardDeckSelectorSidebar();
    if (!noJump)
    {
        if (it)
        {
            this->ui->boardsTreeWidget->scrollToItem(it, QAbstractItemView::PositionAtCenter);
        }
        else
        {
            this->ui->boardsTreeWidget->scrollToTop();
        }
    }
    this->ui->boardsTreeWidget->blockSignals(block);
}

void mainWindow::setSelectedDeckInLobby(QTreeWidgetItem* it, bool noJump)
{
    bool block = this->ui->decksTreeWidget->blockSignals(true);
    this->ui->decksTreeWidget->clearSelection();
    if (it)
    {
        it->setSelected(true);
        this->setBoardListHighlight(it->data(0, Qt::UserRole).toString());
    }
    else
    {
        this->setBoardListHighlight("");
    }
    this->ui->boardsTreeWidget->setEnabled(this->hostMode);
    if (!this->disableLobbyUpdates)
    {
        this->networkManager->sendMessage(this->getLobbyState());
        this->sendReqToPublicServer("updategamedata", (QStringList() << this->secretKey) + getGameDataUpdateForPublicServer());
    }
    this->updateBoardDeckSelectorSidebar();
    if (!noJump)
    {
        if (it)
        {
            this->ui->decksTreeWidget->scrollToItem(it, QAbstractItemView::PositionAtCenter);
        }
        else
        {
            this->ui->decksTreeWidget->scrollToTop();
        }
    }
    this->ui->decksTreeWidget->blockSignals(block);
}

void mainWindow::aUserDisconnectedFromGame(const QString& username)
{
    this->disableLobbyUpdates = true;
    auto obs = this->getObservers();
    auto its = this->ui->lobbyConnectedUserList->findItems(username, Qt::MatchExactly);
    for (auto& it : its)
    {
        delete this->ui->lobbyConnectedUserList->takeItem(this->ui->lobbyConnectedUserList->row(it));
    }
    int idx;
    idx = this->findDataInComboBox(this->ui->p1TypeComboBox, username);
    if (idx == this->ui->p1TypeComboBox->currentIndex()) this->ui->p1TypeComboBox->setCurrentIndex(0);
    if (idx != -1) this->ui->p1TypeComboBox->removeItem(idx);
    idx = this->findDataInComboBox(this->ui->p2TypeComboBox, username);
    if (idx == this->ui->p2TypeComboBox->currentIndex()) this->ui->p2TypeComboBox->setCurrentIndex(0);
    if (idx != -1) this->ui->p2TypeComboBox->removeItem(idx);
    idx = this->findDataInComboBox(this->ui->p3TypeComboBox, username);
    if (idx == this->ui->p3TypeComboBox->currentIndex()) this->ui->p3TypeComboBox->setCurrentIndex(0);
    if (idx != -1) this->ui->p3TypeComboBox->removeItem(idx);
    idx = this->findDataInComboBox(this->ui->p4TypeComboBox, username);
    if (idx == this->ui->p4TypeComboBox->currentIndex()) this->ui->p4TypeComboBox->setCurrentIndex(0);
    if (idx != -1) this->ui->p4TypeComboBox->removeItem(idx);
    this->disableLobbyUpdates = false;
    this->networkManager->sendMessage(this->getLobbyState());

    if (this->gameInProgress)
    {
        if (!obs.contains(username))
        {
            if (mainWindow::msgBoxQuestion(this, tr("Player disconnected"), tr("The game cannot continue because a player disconnected.\nWould you like to save the current state of the game?")) == QMessageBox::Yes)
            {
                this->saveGame();
            }
            this->endGame();
        }
        else
        {
            this->updateObserverList();
        }
    }
    else if (!this->hostMode && username == this->hostUsername)
    {
        this->reportError(tr("The host has abandoned the game"));
        this->ongoingNetworkGame = false;
        this->sendReqToPublicServer("gamelist", {});
        this->publicServerTimer->start();
        this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->publicServerPage), slidingStackedWidget::AnimationDirection::LeftToRight);
        this->resetStartGameScreen();
    }
}

void mainWindow::sendLobbyChatMsg()
{
    if (!this->ui->lobbyChatMsgEdit->text().isEmpty())
    {
        this->networkManager->sendMessage(network::networkDataPackage(network::MessageType::LobbyChatMessage, QStringList()
                                                                      << this->getConfigVariable("username")
                                                                      << this->ui->lobbyChatMsgEdit->text()));
        this->ui->lobbyChatTextEdit->append(QString("<%1> %2").arg(this->getConfigVariable("username"), this->ui->lobbyChatMsgEdit->text()));
        this->ui->lobbyChatMsgEdit->setText("");
    }
}

void mainWindow::updateNewGamePagePlayerSymbols(const QString& board)
{
    if (board.isEmpty())
    {
        this->ui->p1LogoLabel->setPixmap(getPrimaryPath("img/player_1_symbol_newgamepage.png"));
        this->ui->p2LogoLabel->setPixmap(getPrimaryPath("img/player_2_symbol_newgamepage.png"));
        this->ui->p3LogoLabel->setPixmap(getPrimaryPath("img/player_3_symbol_newgamepage.png"));
        this->ui->p4LogoLabel->setPixmap(getPrimaryPath("img/player_4_symbol_newgamepage.png"));
    }
    else
    {
        if (boards[board].hasKey("player1_symbol"))
        {
            this->ui->p1LogoLabel->setPixmap(getPrimaryPath("boards/" + boards[board].readValue("player1_symbol") + ".png"));
        }
        else
        {
            this->ui->p1LogoLabel->setPixmap(getPrimaryPath("img/player_1_symbol_newgamepage.png"));
        }
        if (boards[board].hasKey("player2_symbol"))
        {
            this->ui->p2LogoLabel->setPixmap(getPrimaryPath("boards/" + boards[board].readValue("player2_symbol") + ".png"));
        }
        else
        {
            this->ui->p2LogoLabel->setPixmap(getPrimaryPath("img/player_2_symbol_newgamepage.png"));
        }
        if (boards[board].hasKey("player3_symbol"))
        {
            this->ui->p3LogoLabel->setPixmap(getPrimaryPath("boards/" + boards[board].readValue("player3_symbol") + ".png"));
        }
        else
        {
            this->ui->p3LogoLabel->setPixmap(getPrimaryPath("img/player_3_symbol_newgamepage.png"));
        }
        if (boards[board].hasKey("player4_symbol"))
        {
            this->ui->p4LogoLabel->setPixmap(getPrimaryPath("boards/" + boards[board].readValue("player4_symbol") + ".png"));
        }
        else
        {
            this->ui->p4LogoLabel->setPixmap(getPrimaryPath("img/player_4_symbol_newgamepage.png"));
        }
    }
}

void mainWindow::startErrorReport(const QString& from)
{
    this->errorReportBackLocation = from;
    this->errorReportSave.clear();
    QScreen* screen = QGuiApplication::primaryScreen();
    if (const QWindow* window = windowHandle()) screen = window->screen();
    this->errorReportScreenShot = {};
    if (screen)
    {
        this->errorReportScreenShot = screen->grabWindow(0);
    }

    if (from == "game")
    {
        this->errorReportSave = this->save();
    }

    for (const auto& k : this->settings->allKeys())
    {
        this->errorReportSave.addItem("bugReportSetting_" + k, this->settings->value(k).toString());
    }

    this->ui->bugDescEdit->clear();

    this->ui->stackedWidget->slideInWidget(this->ui->bugReportPage, slidingStackedWidget::AnimationDirection::RightToLeft);
}

void mainWindow::saveErrorReport()
{
    auto filt = tr("Lost Bazaar error reports (%1)").arg("*.bazaarbug");
    QString path = QFileDialog::getSaveFileName(nullptr, tr("Save error report"), getSavePath(), filt, &filt);

    if (!path.isEmpty())
    {
        if (!path.endsWith(".bazaarbug"))
        {
            path += ".bazaarbug";
        }
        this->errorReportSave.addItem("bugReportDescription", this->ui->bugDescEdit->toPlainText());
        this->errorReportSave.addItem("bugReportBuildAbi", QSysInfo::buildAbi());
        this->errorReportSave.addItem("bugReportBuildCpuArch", QSysInfo::buildCpuArchitecture());
        this->errorReportSave.addItem("bugReportCurrCpuArch", QSysInfo::currentCpuArchitecture());
        this->errorReportSave.addItem("bugReportKernelType", QSysInfo::kernelType());
        this->errorReportSave.addItem("bugReportKernelVersion", QSysInfo::kernelVersion());
#if QT_VERSION_MINOR > 5
        this->errorReportSave.addItem("bugReportHostname", QSysInfo::machineHostName());
#endif
        this->errorReportSave.addItem("bugReportProductName", QSysInfo::prettyProductName());
        this->errorReportSave.addItem("bugReportProductType", QSysInfo::productType());
        this->errorReportSave.addItem("bugReportProductVersion", QSysInfo::productVersion());
        this->errorReportSave.addItem("bugReportBackFrom", this->errorReportBackLocation);
        this->errorReportSave.addItem("bugReportCurrQtVersion", QString(qVersion()));
        this->errorReportSave.addItem("bugReportBuildQtVersion", QT_VERSION_STR);
        this->errorReportSave.addItem("bugReportGameVersion", LOST_BAZAAR_VERSION);

        QByteArray bArray;
        QBuffer buffer(&bArray);
        buffer.open(QIODevice::WriteOnly);
        this->errorReportScreenShot.save(&buffer, "PNG");
        this->errorReportSave.addItem("bugReportScreenshot", QString(bArray.toBase64()));

        this->errorReportSave.saveToFile(path, false);
        this->errorReportSave.clear();
        mainWindow::msgBoxInfo(this, tr("Error report saved"), tr("Thank you for your error report. Please do not forget to submit it either in e-mail to bugs@lost-bazaar.org or to our Discord server."));

        if (this->errorReportBackLocation == "main")
        {
            this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->menuPage), slidingStackedWidget::AnimationDirection::LeftToRight);
        }
        else if (this->errorReportBackLocation == "game")
        {
            this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->gamePage), slidingStackedWidget::AnimationDirection::LeftToRight);
        }
    }
}

void mainWindow::openHelp(QString source)
{
    QString lang = languageConfigValueToLocaleName(this->getConfigVariable("language"));
    if (lang.isEmpty() || getPrimaryPath("help/" + lang + "_main.html", true).isEmpty()) lang = DEFAULT_LANGUAGE;
    this->ui->helpBrowser->setSource(QUrl::fromLocalFile(getPrimaryPath("help/" + lang + "_main.html")));
    lang = languageConfigValueToLocaleName(this->getConfigVariable("language"));
    if (getPrimaryPath("help/" + lang + "_contents.html", true).isEmpty()) lang = DEFAULT_LANGUAGE;
    this->ui->helpContentsBrowser->setSource(QUrl::fromLocalFile(getPrimaryPath("help/" + lang + "_contents.html")));
    this->helpOpenSource = source;
    if (source == "main")
    {
        this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->helpPage), slidingStackedWidget::AnimationDirection::RightToLeft);
    }
    else if (source == "ingame")
    {
        this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->helpPage), slidingStackedWidget::AnimationDirection::LeftToRight);
    }
}

void mainWindow::sendChatMsg()
{
    if (this->ui->chatMsgLineEdit->text() == "/showhands")
    {
        this->ui->p1Inventory->setViewable(true);
        this->ui->p2Inventory->setViewable(true);
        this->ui->p3Inventory->setViewable(true);
        this->ui->p4Inventory->setViewable(true);
        this->ui->storage->setViewable(true);
        this->ui->trash->setViewable(true);
        this->ui->huckster->setViewable(true);
        this->ui->mainInventory->setViewable(true);
    }
    else if (this->ui->chatMsgLineEdit->text() == "/unlimitedmoves")
    {
        this->state->unlimitedMoves = true;
    }
    else if (this->ui->chatMsgLineEdit->text() == "/printcards")
    {
        auto b = this->ui->gameBoard->getAllCards();
        auto p1 = this->ui->p1Inventory->getAllCards();
        auto p2 = this->ui->p2Inventory->getAllCards();
        auto p3 = this->ui->p3Inventory->getAllCards();
        auto p4 = this->ui->p4Inventory->getAllCards();
        auto stor = this->ui->storage->getAllCards();
        auto trash = this->ui->trash->getAllCards();
        auto main = this->ui->mainInventory->getAllCards();
        auto huck = this->ui->huckster->getAllCards();

        this->ui->chatHistoryTextEdit->append("Items of " + this->getConfigVariable("username"));
        const auto cardKeys = card::currentCards.keys();
        for (const auto& k : cardKeys)
        {
            QString str;
            if (b.contains(card::currentCards[k])) str += QString("board (%1;%2)").arg(QString::number(card::currentCards[k]->getPosX()),
                                                                                           QString::number(card::currentCards[k]->getPosY()));
            if (p1.contains(card::currentCards[k])) str += "p1 ";
            if (p2.contains(card::currentCards[k])) str += "p2 ";
            if (p3.contains(card::currentCards[k])) str += "p3 ";
            if (p4.contains(card::currentCards[k])) str += "p4 ";
            if (trash.contains(card::currentCards[k])) str += "trash ";
            if (stor.contains(card::currentCards[k])) str += "storage ";
            if (main.contains(card::currentCards[k])) str += "main ";
            if (huck.contains(card::currentCards[k])) str += "huckster ";
            this->ui->chatHistoryTextEdit->append(QString("%1 %2 | %3").arg(QString::number(k), card::currentCards[k]->getID(), str));
        }
    }
    else if (this->ui->chatMsgLineEdit->text() == "/networkdbg")
    {
        this->networkdbg = !this->networkdbg;
    }
    else if (!this->ui->chatMsgLineEdit->text().isEmpty())
    {
        this->sendGameEvent(QStringList() << "chatMsg" << this->getConfigVariable("username") << this->ui->chatMsgLineEdit->text(), true);
        this->ui->chatHistoryTextEdit->append(QString("<%1> %2").arg(this->getConfigVariable("username"), this->ui->chatMsgLineEdit->text()));
        this->ui->chatMsgLineEdit->setText("");
    }
    this->ui->chatHistoryTextEdit->verticalScrollBar()->setValue(this->ui->chatHistoryTextEdit->verticalScrollBar()->maximum());
    this->ui->chatMsgLineEdit->setText("");
}

void mainWindow::contHotSeat()
{
    disconnect(this->ui->stackedWidget, &slidingStackedWidget::animationFinished, this, &mainWindow::contHotSeat);
    this->waitingForHotSeat = false;
}

void mainWindow::updateLobby(const network::networkDataPackage& data)
{
    this->disableLobbyUpdates = true;
    this->ui->startGameButton->setEnabled(false);
    if (data.length() < 14)
    {
        if (!this->hostMode)
        {
            this->ui->useHucksterCheckBox->setEnabled(false);
            this->ui->openHandsCheckBox->setEnabled(false);
            this->ui->hucksterCardsComboBox->setEnabled(false);
            this->ui->replayPlayBackCheckBox->setEnabled(false);
            this->ui->gameModeComboBox->setEnabled(false);
            this->ui->boardsTreeWidget->setEnabled(false);
            this->ui->decksTreeWidget->setEnabled(false);
            this->ui->loadSaveGameButton->setVisible(false);
            this->ui->p1NickLineEdit->setEnabled(false);
            this->ui->p1TypeComboBox->setEnabled(false);
            this->ui->p2NickLineEdit->setEnabled(false);
            this->ui->p2TypeComboBox->setEnabled(false);
            this->ui->p3NickLineEdit->setEnabled(false);
            this->ui->p3TypeComboBox->setEnabled(false);
            this->ui->p4NickLineEdit->setEnabled(false);
            this->ui->p4TypeComboBox->setEnabled(false);
        }
    }
    else
    {
        QStringList usernames;
        this->ui->useHucksterCheckBox->setChecked(data.getStringAt(0).toInt());
        this->ui->openHandsCheckBox->setChecked(data.getStringAt(14).toInt());
        this->ui->hucksterCardsComboBox->setCurrentIndex(data.getStringAt(1).toInt()-1);
        this->ui->gameModeComboBox->setCurrentIndex(data.getStringAt(2).toInt());
        this->setLobbyBoardSelectionFromSerializedState(data.getStringAt(3));
        this->setLobbyDeckSelectionFromSerializedState(data.getStringAt(4));
        for (int i = 15; i < data.length(); ++i)
        {
            usernames.append(data.getStringAt(i));
        }
        this->refillPlayerTypeComboBoxes(usernames);

        this->ui->lobbyConnectedUserList->clear();
        for (const auto& uname : usernames)
        {
            if (uname != this->getConfigVariable("username")) this->ui->lobbyConnectedUserList->addItem(uname);
        }
        this->ui->lobbyConnectedUserList->sortItems();

        this->ui->p1TypeComboBox->setCurrentIndex(std::abs(findDataInComboBox(this->ui->p1TypeComboBox, data.getStringAt(5))));
        this->ui->p2TypeComboBox->setCurrentIndex(std::abs(findDataInComboBox(this->ui->p2TypeComboBox, data.getStringAt(7))));
        this->ui->p3TypeComboBox->setCurrentIndex(std::abs(findDataInComboBox(this->ui->p3TypeComboBox, data.getStringAt(9))));
        this->ui->p4TypeComboBox->setCurrentIndex(std::abs(findDataInComboBox(this->ui->p4TypeComboBox, data.getStringAt(11))));
        this->ui->p1NickLineEdit->setText(data.getStringAt(6));
        this->ui->p2NickLineEdit->setText(data.getStringAt(8));
        this->ui->p3NickLineEdit->setText(data.getStringAt(10));
        this->ui->p4NickLineEdit->setText(data.getStringAt(12));
        this->ui->p1NickLineEdit->setEnabled(data.getStringAt(5) == this->getConfigVariable("username") || this->hostMode);
        this->ui->p2NickLineEdit->setEnabled(data.getStringAt(7) == this->getConfigVariable("username") || this->hostMode);
        this->ui->p3NickLineEdit->setEnabled(data.getStringAt(9) == this->getConfigVariable("username") || this->hostMode);
        this->ui->p4NickLineEdit->setEnabled(data.getStringAt(11) == this->getConfigVariable("username") || this->hostMode);
        this->ui->hucksterCardsComboBox->setVisible(data.getStringAt(13).isEmpty());
        this->ui->hucksterCardsLabel->setVisible(data.getStringAt(13).isEmpty());
        this->ui->p1TypeComboBox->setEnabled(this->hostMode);
        this->ui->p1TypeComboBox->setEnabled(this->hostMode);
        this->ui->p1TypeComboBox->setEnabled(this->hostMode);
        this->ui->p1TypeComboBox->setEnabled(this->hostMode);
        if (!data.getStringAt(13).isEmpty())
        {
            this->ui->loadedSaveNameLabel->setText(tr("Loaded game: %1").arg(QFileInfo(data.getStringAt(13)).fileName()));
        }
        else
        {
            this->ui->loadedSaveNameLabel->setText(tr("New game (no saved game loaded)"));
        }
    }
    this->disableLobbyUpdates = false;
    this->updateObserverList();
}

void mainWindow::updatePublicServerSecretKey(const QString& gameID, const QString& key)
{
    this->secretKey = key;
    this->networkManager->setCurrentPublicGameID(gameID, this->hostMode ? this->ui->publicGamePasswordEdit->text() : this->ui->passwordPagePasswordEdit->text());
}

void mainWindow::sendReqToPublicServer(const QString& type, const QStringList& data)
{
    this->networkManager->sendRelayMsg(QStringList() << type << data);
}

void mainWindow::reorderInventoryTabs()
{
    QList<QWidget*> widgetList;
    QWidget* activeWidget = nullptr;
    if (this->ui->sideBarTabWidget->currentIndex() != -1)
    {
        activeWidget = this->ui->sideBarTabWidget->widget(this->ui->sideBarTabWidget->currentIndex());
    }
    widgetList << this->ui->tab_Storage;
    switch (this->state->getCurrentPlayer())
    {
        case 1:
            widgetList << this->ui->tab_p1Inventory << this->ui->tab_p3Inventory << this->ui->tab_p2Inventory << this->ui->tab_p4Inventory;
            break;
        case 2:
            widgetList << this->ui->tab_p2Inventory << this->ui->tab_p1Inventory << this->ui->tab_p3Inventory << this->ui->tab_p4Inventory ;
            break;
        case 3:
            widgetList << this->ui->tab_p3Inventory << this->ui->tab_p1Inventory << this->ui->tab_p2Inventory << this->ui->tab_p4Inventory;
            break;
        case 4:
            widgetList << this->ui->tab_p4Inventory << this->ui->tab_p1Inventory << this->ui->tab_p3Inventory << this->ui->tab_p2Inventory ;
            break;
    }
    if (this->state->hucksterEnabled) widgetList << this->ui->tab_Huckster;
    widgetList << this->ui->tab_Trash;
    for (int i = 0; i < widgetList.size(); i++)
    {
        int idx = this->ui->sideBarTabWidget->indexOf(widgetList[i]);
        if (idx != -1)
        {
            QString tmpText = this->ui->sideBarTabWidget->tabText(idx);
            this->ui->sideBarTabWidget->removeTab(idx);
            this->ui->sideBarTabWidget->insertTab(i, widgetList[i], tmpText);
        }
    }
    if (activeWidget) this->ui->sideBarTabWidget->setCurrentIndex(this->ui->sideBarTabWidget->indexOf(activeWidget));
}

void mainWindow::switchToPlayerInventory(int playerNum)
{
    switch (playerNum)
    {
        case 1:
            this->ui->sideBarTabWidget->setCurrentWidget(this->ui->tab_p1Inventory);
            this->state->p1Inventory->updateDisabledCards();
            break;
        case 2:
            this->ui->sideBarTabWidget->setCurrentWidget(this->ui->tab_p2Inventory);
            this->state->p2Inventory->updateDisabledCards();
            break;
        case 3:
            this->ui->sideBarTabWidget->setCurrentWidget(this->ui->tab_p3Inventory);
            this->state->p3Inventory->updateDisabledCards();
            break;
        case 4:
            this->ui->sideBarTabWidget->setCurrentWidget(this->ui->tab_p4Inventory);
            this->state->p4Inventory->updateDisabledCards();
            break;
        default:
            reportError(tr("Error: invalid player in switchToPlayerInventory(...)"));
    }
}

void mainWindow::setUIDisabledForUserChoice(bool disabled)
{
    if (disabled) uiDisabledCounter++;
    else if (uiDisabledCounter > 0) uiDisabledCounter--;
    this->ui->menuButton->setEnabled(uiDisabledCounter == 0);
    this->updateEndTurnButtonState();
}

void mainWindow::setUIDisabledForAnimation(bool disabled)
{
    this->uiDisabledForAnim = disabled;
    this->ui->menuButton->setEnabled(!uiDisabledForAnim && (uiDisabledCounter == 0));
    this->updateEndTurnButtonState();
    this->ui->sideBarTabWidget->setEnabled(!disabled);
    this->ui->nextStepButton->setEnabled(!disabled);
}

void mainWindow::setUIDisabledForAI(bool disabled)
{
    this->setUIDisabledForAnimation(disabled);
}

bool mainWindow::shortcutsEnabled()
{
    return this->gameInProgress;
}

void mainWindow::saveGame()
{
    auto filt = tr("Lost Bazaar saved games (%1)").arg("*.lbs");
    QString path = QFileDialog::getSaveFileName(nullptr, tr("Save game"), getSavePath(), filt, &filt);
    if (path.isEmpty()) return;
    if (!path.endsWith(".lbs")) path += ".lbs";
    this->save().saveToFile(path, false);
}

void mainWindow::disableGUIForPassiveTurn()
{
    this->state->storage->viewport()->setEnabled(false);
    this->state->p1Inventory->viewport()->setEnabled(false);
    this->state->p2Inventory->viewport()->setEnabled(false);
    this->state->p3Inventory->viewport()->setEnabled(false);
    this->state->p4Inventory->viewport()->setEnabled(false);
    this->state->huckster->viewport()->setEnabled(false);
    this->state->trash->viewport()->setEnabled(false);
    this->ui->endTurnButton->setEnabled(false);
}

void mainWindow::joinPublicGameWithPassword()
{
    this->ui->joinPublicGameWithPasswordButton->setEnabled(false);
    this->ui->passwordPagePasswordEdit->setEnabled(false);
    this->networkManager->setCurrentPublicGameID(this->gameIDToJoin, this->ui->passwordPagePasswordEdit->text());
    this->networkManager->start(network::NetworkGameMode::ClientMode, this->hostUsername, {});
}

void mainWindow::popInventoryTab()
{
    if (this->inventoryTabStack.isEmpty())
    {
        mainWindow::reportError(tr("Error: popInventoryTab() called but the stack is empty. There is a bug somewhere, please report this."));
    }
    else
    {
        int idx = this->ui->sideBarTabWidget->indexOf(this->inventoryTabStack.back());
        if (idx < 0)
        {
            mainWindow::reportError(tr("Error: trying to switch to a hidden or invisible inventory tab (in popInventoryTab()). There is a bug in some script somewhere..."));
        }
        else
        {
            this->ui->sideBarTabWidget->setCurrentIndex(idx);
        }
        this->inventoryTabStack.pop_back();
    }
}

void mainWindow::pushInventoryTab(QWidget* oldTab, QWidget* newTab)
{
    int idx = this->ui->sideBarTabWidget->indexOf(newTab);
    if (idx < 0)
    {
        mainWindow::reportError(tr("Error: trying to switch to a hidden or invisible inventory tab. There is a bug in some script somewhere..."));
    }
    else
    {
        this->ui->sideBarTabWidget->setCurrentIndex(idx);
    }
    this->inventoryTabStack.append(oldTab);
}

void mainWindow::loadGameDirectly(QString fileName, QString script)
{
    if (!fileName.isEmpty())
    {
        this->endGame(true);
        if (this->initializeGame(fileName, script, time(nullptr), false))
        {
            this->ui->stackedWidget->slideInWidget(this->ui->gamePage, slidingStackedWidget::AnimationDirection::RightToLeft);
            this->updateCurrentPlayerAndScoreLabels();
        }
    }
}

QString mainWindow::getLanguageCode()
{
    if (settings->value("language").toString() == "")
    {
        return DEFAULT_LANGUAGE;
    }
    else
    {
        if (settings->value("language").toString() == "system")
        {
            return QLocale::system().name();
        }
        else
        {
            return settings->value("language").toString();
        }
    }
}

void mainWindow::keyPressEvent(QKeyEvent* ev)
{
    Q_UNUSED(ev)
}

void mainWindow::closeEvent(QCloseEvent*)
{
    if (this->networkManager)
    {
        this->networkManager->disconnectFromCurrentGame();
        this->networkManager->resetRelay();
    }
    std::_Exit(0);
    //QCoreApplication::quit();
}

void mainWindow::setupSettingsUI(int source)
{
    disconnect(this->ui->enableMusicCheckBox, &QCheckBox::toggled, nullptr, nullptr);
    disconnect(this->ui->enableSoundEffectsCheckBox, &QCheckBox::toggled, nullptr, nullptr);
    disconnect(this->ui->fullscreenCheckbox,  &QCheckBox::toggled, nullptr, nullptr);
    disconnect(this->ui->disableFontHintingCheckBox,  &QCheckBox::toggled, nullptr, nullptr);
    disconnect(this->ui->overrideDefaultFontSizesCheckBox, &QCheckBox::toggled, nullptr, nullptr);
    disconnect(this->ui->neverChangePerspectiveCheckBox, &QCheckBox::toggled, nullptr, nullptr);
    disconnect(this->ui->disablePerspectiveChangeForNetworkPlayersCheckBox, &QCheckBox::toggled, nullptr, nullptr);
    disconnect(this->ui->aiDifficultySlider, &QSlider::sliderMoved, nullptr, nullptr);
    disconnect(this->ui->slidingAnimCheckBox,  &QCheckBox::toggled, nullptr, nullptr);
    disconnect(this->ui->gpuAccelCheckBox,  &QCheckBox::toggled, nullptr, nullptr);
    disconnect(this->ui->hiresModeCheckBox,  &QCheckBox::toggled, nullptr, nullptr);
    disconnect(this->ui->coordGridCheckBox,  &QCheckBox::toggled, nullptr, nullptr);
    disconnect(this->ui->allowChatSplitterResizeCheckBox,  &QCheckBox::toggled, nullptr, nullptr);
    disconnect(this->ui->useHotSeatPageCheckBox, &QCheckBox::toggled, nullptr, nullptr);
    disconnect(this->ui->endOfTurnConfirmCheckBox, &QCheckBox::toggled, nullptr, nullptr);
    disconnect(this->ui->orderCardsInHandAlphaCheckBox, &QCheckBox::toggled, nullptr, nullptr);
    disconnect(this->ui->langSelectionComboBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), nullptr, nullptr);
    disconnect(this->ui->gameAnimationsCheckBox, &QCheckBox::toggled, nullptr, nullptr);
    disconnect(this->ui->boardRotAnimCheckBox, &QCheckBox::toggled, nullptr, nullptr);
    disconnect(this->ui->usernameEdit, &QLineEdit::textChanged, nullptr, nullptr);
    this->ui->usernameEdit->setEnabled(!source);
    this->ui->langSelectionComboBox->setEnabled(!source);
    this->ui->resetSettingsButton->setEnabled(!source);
    this->ui->hiresModeCheckBox->setEnabled(!source);
    this->ui->gpuAccelCheckBox->setEnabled(!source);
    this->settingsOpenSource = source;
    this->ui->langSelectionComboBox->clear();
    this->ui->langSelectionComboBox->addItem(tr("Detect system language"), QVariant("system"));
    this->ui->langSelectionComboBox->addItem(tr("English (US)"), QVariant("en_US"));
    this->ui->langSelectionComboBox->addItem(tr("Hungarian"), QVariant("hu_HU"));
    //this->ui->langSelectionComboBox->addItem(tr("Polish"), QVariant("pl_PL"));
    if (this->ui->langSelectionComboBox->findData(QVariant(this->getConfigVariable("language"))) == -1)
    {
        this->setConfigVariable("language", DEFAULT_LANGUAGE);
    }
    auto setUIElementValues = [this]()
    {
        this->ui->langSelectionComboBox->setCurrentIndex(this->ui->langSelectionComboBox->findData(QVariant(this->getConfigVariable("language"))));
        this->ui->enableMusicCheckBox->setChecked(this->getConfigVariable("enableMusic").toInt());
        this->ui->enableSoundEffectsCheckBox->setChecked(this->getConfigVariable("enableSoundEffects").toInt());
        this->ui->usernameEdit->setText(this->getConfigVariable("username"));
        this->ui->fullscreenCheckbox->setChecked(this->getConfigVariable("fullScreen").toInt());
        this->ui->overrideDefaultFontSizesCheckBox->setChecked(this->getConfigVariable("overrideDefaultFontSizes").toInt());
        this->ui->disableFontHintingCheckBox->setChecked(this->getConfigVariable("disableFontHinting").toInt());
        this->ui->neverChangePerspectiveCheckBox->setChecked(this->getConfigVariable("neverChangePersp").toInt());
        this->ui->dontUsePrerenderedCardsCheckBox->setChecked(this->getConfigVariable("noPrerenderedCards").toInt());
        this->ui->hiresModeCheckBox->setChecked(this->getConfigVariable("hiresMode").toInt());
        this->ui->disablePerspectiveChangeForNetworkPlayersCheckBox->setChecked(this->getConfigVariable("doNotChangeOnlinePersp").toInt());
        this->ui->disablePerspectiveChangeForNetworkPlayersCheckBox->setEnabled(!this->ui->neverChangePerspectiveCheckBox->isChecked());
        this->ui->aiDifficultySlider->setValue(this->getConfigVariable("aiDifficulty").toInt());
        this->ui->slidingAnimCheckBox->setChecked(this->getConfigVariable("slidingAnim").toInt());
        this->ui->gpuAccelCheckBox->setChecked(this->getConfigVariable("gpuAccel").toInt());
        this->ui->allowChatSplitterResizeCheckBox->setChecked(this->getConfigVariable("allowChatSplitterResize").toInt());
        this->ui->endOfTurnConfirmCheckBox->setChecked(this->getConfigVariable("turnEndWarning").toInt());
        this->ui->useHotSeatPageCheckBox->setChecked(this->getConfigVariable("useHotSeatContinuePage").toInt());
        this->ui->orderCardsInHandAlphaCheckBox->setChecked(this->getConfigVariable("orderCardsInHandAlpha").toInt());
        this->ui->gameAnimationsCheckBox->setChecked(this->getConfigVariable("gameAnimations").toInt());
        this->ui->shrinkInsteadOfBlurCheckBox->setChecked(this->getConfigVariable("shrinkInsteadOfBlur").toInt());
        this->ui->coordGridCheckBox->setChecked(this->getConfigVariable("coordGrid").toInt());
        this->ui->boardRotAnimCheckBox->setChecked(this->getConfigVariable("boardRotAnim").toInt());
    };
    setUIElementValues();
    connect(this->ui->enableMusicCheckBox, &QCheckBox::toggled, this, [this](bool state)
    {
        auto oldState = this->getConfigVariable("enableMusic").toInt();
        this->setConfigVariable("enableMusic", QString::number(state));
        if (state && !oldState)
        {
#ifdef Q_OS_WIN
            this->mPlayer->loadPlayList(getPrimaryPath("music/default_playlist_wma.txt"));
#else
            this->mPlayer->loadPlayList(getPrimaryPath("music/default_playlist.txt"));
#endif
            this->mPlayer->setMaxRandomSilence(0);
            this->mPlayer->start();
            this->mPlayer->shuffle();
            this->mPlayer->setMaxRandomSilence(180000); //3 mins
        }
        else if (!state)
        {
            this->mPlayer->stop();
        }
    });
    connect(this->ui->enableSoundEffectsCheckBox, &QCheckBox::toggled, this, [this](bool state)
    {
        this->setConfigVariable("enableSoundEffects", QString::number(state));
    });
    connect(this->ui->usernameEdit, &QLineEdit::textChanged, this, [this](const QString & txt)
    {
        this->setConfigVariable("username", txt);
        this->refillPlayerTypeComboBoxes();
    });
    connect(this->ui->neverChangePerspectiveCheckBox, &QCheckBox::clicked, this, [this](bool checked)
    {
        this->setConfigVariable("neverChangePersp", QString::number(checked));
        this->ui->disablePerspectiveChangeForNetworkPlayersCheckBox->setEnabled(!checked);
    });
    connect(this->ui->dontUsePrerenderedCardsCheckBox, &QCheckBox::clicked, this, [this](bool checked)
    {
        this->setConfigVariable("noPrerenderedCards", QString::number(checked));
    });
    connect(this->ui->hiresModeCheckBox, &QCheckBox::clicked, this, [this](bool checked)
    {
        this->setConfigVariable("hiresMode", QString::number(checked));
        mainWindow::msgBoxInfo(this, tr("Applying configuration change"), tr("You must restart the application for this change to fully take effect."));
    });
    connect(this->ui->disablePerspectiveChangeForNetworkPlayersCheckBox, &QCheckBox::clicked, this, [this](bool checked)
    {
        this->setConfigVariable("doNotChangeOnlinePersp", QString::number(checked));
    });
    connect(this->ui->aiDifficultySlider, &QSlider::sliderMoved, this, [this](int position)
    {
        this->setConfigVariable("aiDifficulty", QString::number(position));
    });
    connect(this->ui->fullscreenCheckbox, &QCheckBox::toggled, this, [this](bool state)
    {
        this->setConfigVariable("fullScreen", QString::number(state));
        if (state)
        {
            this->setWindowState(Qt::WindowFullScreen);
        }
        else
        {
            this->setWindowState(Qt::WindowNoState);
        }
    });
    connect(this->ui->disableFontHintingCheckBox, &QCheckBox::toggled, this, [this](bool state)
    {
        this->setConfigVariable("disableFontHinting", QString::number(state));
        QFont appFont = QApplication::font();
        if (state)
        {
            appFont.setHintingPreference(QFont::PreferNoHinting);
        }
        else
        {
            appFont.setHintingPreference(QFont::PreferDefaultHinting);
        }
        QApplication::setFont(appFont);
        mainWindow::msgBoxInfo(this, tr("Applying configuration change"), tr("You must restart the application for this change to fully take effect."));
    });
    connect(this->ui->overrideDefaultFontSizesCheckBox, &QCheckBox::toggled, this, [this](bool state)
    {
        this->setConfigVariable("overrideDefaultFontSizes", QString::number(state));
        this->setStyleSheet(mainWindow::readStyleSheet());
        mainWindow::msgBoxInfo(this, tr("Applying configuration change"), tr("You must restart the application for this change to fully take effect."));
    });
    connect(this->ui->slidingAnimCheckBox, &QCheckBox::toggled, this, [this](bool state)
    {
        this->setConfigVariable("slidingAnim", QString::number(state));
    });
    connect(this->ui->gpuAccelCheckBox, &QCheckBox::toggled, this, [this](bool state)
    {
        this->setConfigVariable("gpuAccel", QString::number(state));
        mainWindow::msgBoxInfo(this, tr("Applying configuration change"), tr("You must restart the application for this change to fully take effect."));
    });
    connect(this->ui->allowChatSplitterResizeCheckBox, &QCheckBox::toggled, this, [this](bool state)
    {
        this->setConfigVariable("allowChatSplitterResize", QString::number(state));
        if (state)
        {
            this->ui->bottomWidget->setMaximumHeight(std::numeric_limits<int>::max());
        }
        else
        {
            this->ui->bottomWidget->setMaximumHeight(CHAT_PANEL_MAX_HEIGHT);
        }
    });
    connect(this->ui->useHotSeatPageCheckBox, &QCheckBox::toggled, this, [this](bool state)
    {
        this->setConfigVariable("useHotSeatContinuePage", QString::number(state));
    });
    connect(this->ui->endOfTurnConfirmCheckBox, &QCheckBox::toggled, this, [this](bool state)
    {
        this->setConfigVariable("turnEndWarning", QString::number(state));
    });
    connect(this->ui->orderCardsInHandAlphaCheckBox, &QCheckBox::toggled, this, [this](bool state)
    {
        this->setConfigVariable("orderCardsInHandAlpha", QString::number(state));
        if (state)
        {
            this->ui->storage->orderCardsAlpha();
            this->ui->p1Inventory->orderCardsAlpha();
            this->ui->p2Inventory->orderCardsAlpha();
            this->ui->p3Inventory->orderCardsAlpha();
            this->ui->p4Inventory->orderCardsAlpha();
            this->ui->trash->orderCardsAlpha();
            this->ui->huckster->orderCardsAlpha();
        }
    });
    connect(this->ui->langSelectionComboBox, qOverload<int>(&QComboBox::currentIndexChanged), this, [this](int index)
    {
        this->setConfigVariable("language", this->ui->langSelectionComboBox->itemData(index).toString());
        mainWindow::msgBoxInfo(this, tr("Applying configuration change"), tr("You must restart the application for the language switch to fully take effect."));
    });
    connect(this->ui->gameAnimationsCheckBox, &QCheckBox::toggled, this, [this](bool state)
    {
        this->setConfigVariable("gameAnimations", QString::number(state));
    });
    connect(this->ui->shrinkInsteadOfBlurCheckBox, &QCheckBox::toggled, this, [this](bool state)
    {
        this->setConfigVariable("shrinkInsteadOfBlur", QString::number(state));
    });
    connect(this->ui->coordGridCheckBox, &QCheckBox::toggled, this, [this](bool state)
    {
        this->setConfigVariable("coordGrid", QString::number(state));
        if (this->state && this->state->gameBoard) this->state->gameBoard->updateCoordGrid();
    });
    connect(this->ui->boardRotAnimCheckBox, &QCheckBox::toggled, this, [this](bool state)
    {
        this->setConfigVariable("boardRotAnim", QString::number(state));
    });
    connect(this->ui->resetSettingsButton, &QPushButton::clicked, this, [this, setUIElementValues]()
    {
        resetSettings();
        setUIElementValues();
    });
}

QStringList mainWindow::getObservers()
{
    QStringList res;
    if (!(this->findDataInComboBox(this->ui->p1TypeComboBox, this->getConfigVariable("username")) == this->ui->p1TypeComboBox->currentIndex() ||
            this->findDataInComboBox(this->ui->p2TypeComboBox, this->getConfigVariable("username")) == this->ui->p2TypeComboBox->currentIndex() ||
            this->findDataInComboBox(this->ui->p3TypeComboBox, this->getConfigVariable("username")) == this->ui->p3TypeComboBox->currentIndex() ||
            this->findDataInComboBox(this->ui->p4TypeComboBox, this->getConfigVariable("username")) == this->ui->p4TypeComboBox->currentIndex()))
    {
        res.append(this->getConfigVariable("username"));
    }
    for (int i = 0; i < this->ui->lobbyConnectedUserList->count(); ++i)
    {
        QString username = this->ui->lobbyConnectedUserList->item(i)->text();
        if (!(this->findDataInComboBox(this->ui->p1TypeComboBox, username) == this->ui->p1TypeComboBox->currentIndex() ||
                this->findDataInComboBox(this->ui->p2TypeComboBox, username) == this->ui->p2TypeComboBox->currentIndex() ||
                this->findDataInComboBox(this->ui->p3TypeComboBox, username) == this->ui->p3TypeComboBox->currentIndex() ||
                this->findDataInComboBox(this->ui->p4TypeComboBox, username) == this->ui->p4TypeComboBox->currentIndex()))
        {
            res.append(username);
        }
    }
    return res;
}

void mainWindow::updateObserverList()
{
    auto obs = this->getObservers();
    this->ui->eyeLabel->setVisible(obs.size());
    //this->ui->eyeLabel->setPixmap(QPixmap(getPrimaryPath(QString("img/eye.png"))));
    QString str;
    for (const auto& o : obs)
    {
        str = str + o + "\n";
    }
    if (str.length()) str.resize(str.size() - 1);
    disconnect(this->ui->eyeLabel, SIGNAL(clicked()), nullptr, nullptr);
    connect(this->ui->eyeLabel, &clickableLabel::clicked, this, [this, str]()
    {
        QMessageBox mBox;
        mBox.addButton(tr("OK"), QMessageBox::AcceptRole);
        mBox.setStyleSheet(readStyleSheet());
        mBox.setWindowTitle(tr("Observers"));
        mBox.setText(tr("Observers:\n%1").arg(str));
        mBox.exec();
    });
}

QString mainWindow::tutorialPageCountToFilename(const QString &lang, int cnt)
{
    QDir d{getPrimaryPath("img/"+lang+"/")};
    auto entries = d.entryList(QStringList{} << "*.png", QDir::Files, QDir::Name);
    if(cnt >= entries.size()) return {};
    return getPrimaryPath("img/"+lang+"/"+entries[cnt], true);
}

bool mainWindow::processGameEvent(const network::networkDataPackage& pkg)
{
    if (pkg.getStringAt(0) == "initializeGame" && pkg.length() == 4)
    {
        card::globalUID = pkg.getStringAt(1).toInt();
        this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->gamePage), slidingStackedWidget::AnimationDirection::RightToLeft);
        this->loadedGameFileContent = pkg.getStringAt(3);
        if (this->loadedGameFileContent.isEmpty())
        {
            this->initializeGame({}, {}, pkg.getStringAt(2).toInt(), false);
        }
        else
        {
            this->initializeGame(":::::", {}, pkg.getStringAt(2).toInt(), true);
        }
        this->updateCurrentPlayerAndScoreLabels();
        return true;
    }
    else if (pkg.getStringAt(0) == "replayMode" && pkg.length() == 2)
    {
        this->setReplayMode(pkg.getStringAt(1).toInt());
    }
    else if (pkg.getStringAt(0) == "setVariable" && pkg.length() == 3)
    {
        this->state->setVariable(pkg.getStringAt(1), pkg.getStringAt(2));
    }
    else if (pkg.getStringAt(0) == "setCheckVictory" && pkg.length() == 2)
    {
        this->state->setCheckVictory(pkg.getStringAt(1).toInt());
    }
    else if (pkg.getStringAt(0) == "setCardPlacementEnabled" && pkg.length() == 2)
    {
        this->state->setCardPlacementEnabled(pkg.getStringAt(1).toInt());
    }
    else if (pkg.getStringAt(0) == "writeToGameLog" && pkg.length() >= 4)
    {
        QList<int> pList;
        for (int i = 4; i < pkg.length(); ++i)
        {
            pList.append(pkg.getStringAt(i).toInt());
        }
        this->state->writeToGameLog(pkg.getStringAt(1), pList, pkg.getStringAt(2).toInt(), pkg.getStringAt(3).toInt());
    }
    else if (pkg.getStringAt(0) == "soundEffect" && pkg.length() == 2)
    {
        this->playSoundEffect(pkg.getStringAt(1), false);
    }
    else if (pkg.getStringAt(0) == "chatMsg" && pkg.length() == 3)
    {
        this->ui->chatHistoryTextEdit->append(QString("<%1> %2").arg(pkg.getStringAt(1), pkg.getStringAt(2)));
        this->ui->chatHistoryTextEdit->verticalScrollBar()->setValue(this->ui->chatHistoryTextEdit->verticalScrollBar()->maximum());
    }
    else if (pkg.getStringAt(0) == "checkVictory")
    {
        this->state->checkVictory();
    }
    else if (pkg.getStringAt(0) == "updateUIForTurnSwitch")
    {
        this->state->updateUIForTurnSwitch();
    }
    else if (pkg.getStringAt(0) == "updateCardLabels")
    {
        this->updateCardLabels();
    }
    else if (pkg.getStringAt(0) == "updateRandomBoardDeckSelection" && pkg.length() == 3)
    {
        this->randomBoard = pkg.getStringAt(1);
        this->randomDeck = pkg.getStringAt(2);
    }
    else if (pkg.getStringAt(0) == "rotateBoardToPlayer" && pkg.length() == 2)
    {
        this->state->gameBoard->rotateBoardToPlayer(pkg.getStringAt(1).toInt());
        return true;
    }
    else if (pkg.getStringAt(0) == "removeCardFromBoard" && pkg.length() == 2)
    {
        this->state->gameBoard->removeCard(pkg.getStringAt(1).toInt());
        return true;
    }
    else if (pkg.getStringAt(0) == "moveCard" && pkg.length() == 5)
    {
        this->state->gameBoard->moveCard(QPoint(pkg.getStringAt(1).toInt(), pkg.getStringAt(2).toInt()), QPoint(pkg.getStringAt(3).toInt(), pkg.getStringAt(4).toInt()));
        return true;
    }
    else if (pkg.getStringAt(0) == "swapCards" && pkg.length() == 5)
    {
        this->state->gameBoard->swapCards(QPoint(pkg.getStringAt(1).toInt(), pkg.getStringAt(2).toInt()), QPoint(pkg.getStringAt(3).toInt(), pkg.getStringAt(4).toInt()));
        return true;
    }
    else if (pkg.getStringAt(0) == "setCurrentPlayerDrawnCard" && pkg.length() == 2)
    {
        this->state->setCurrentPlayerDrawnCard(pkg.getStringAt(1).toInt());
    }
    else if (pkg.getStringAt(0) == "overrideNextPlayer" && pkg.length() == 2)
    {
        this->state->overrideNextPlayer(pkg.getStringAt(1).toInt());
    }
    else if (pkg.getStringAt(0) == "setCardDrawDisabledForTurn" && pkg.length() == 2)
    {
        this->state->setCardDrawDisabledForTurn(pkg.getStringAt(1).toInt());
    }
    else if (pkg.getStringAt(0) == "setCurrentPlayer" && pkg.length() >= 2)
    {
        this->checkOOS(pkg.getData().mid(2));
        this->state->setCurrentPlayer(pkg.getStringAt(1).toInt());
    }
    /*else if(pkg.getStringAt(0) == "newCard" && pkg.length() == 5)
    {
        card * crd = new card(pkg.getStringAt(1), pkg.getStringAt(2).toInt(), pkg.getStringAt(3).toInt() ? this->state : nullptr, pkg.getStringAt(4).toInt());
        Q_UNUSED(crd)
    }*/
    else if (pkg.getStringAt(0) == "changeOwner" && pkg.length() == 3)
    {
        if (card::currentCards.contains(pkg.getStringAt(1).toInt()))
        {
            card::currentCards[pkg.getStringAt(1).toInt()]->changeOwner(pkg.getStringAt(2).toInt());
        }
        else
        {
            mainWindow::reportError(tr("Error: invalid card UID received in network message, out of sync"));
        }
        return true;
    }
    else if (pkg.getStringAt(0) == "redrawCard" && pkg.length() == 2)
    {
        if (card::currentCards.contains(pkg.getStringAt(1).toInt()))
        {
            card::currentCards[pkg.getStringAt(1).toInt()]->redraw();
        }
        else
        {
            mainWindow::reportError(tr("Error: invalid card UID received in network message, out of sync"));
        }
    }
    else if (pkg.getStringAt(0) == "setCounter" && pkg.length() == 3)
    {
        if (card::currentCards.contains(pkg.getStringAt(1).toInt()))
        {
            card::currentCards[pkg.getStringAt(1).toInt()]->setCounter(pkg.getStringAt(2).toInt());
        }
        else
        {
            mainWindow::reportError(tr("Error: invalid card UID received in network message, out of sync"));
        }
    }
    else if (pkg.getStringAt(0) == "setGrayScale" && pkg.length() == 4)
    {
        if (card::currentCards.contains(pkg.getStringAt(1).toInt()))
        {
            card::currentCards[pkg.getStringAt(1).toInt()]->setGrayScale(pkg.getStringAt(2).toInt(), pkg.getStringAt(3).toInt());
        }
        else
        {
            mainWindow::reportError(tr("Error: invalid card UID received in network message, out of sync"));
        }
    }
    else if (pkg.getStringAt(0) == "setShowFullSize" && pkg.length() == 4)
    {
        if (card::currentCards.contains(pkg.getStringAt(1).toInt()))
        {
            card::currentCards[pkg.getStringAt(1).toInt()]->setShowFullSize(pkg.getStringAt(2).toInt(), pkg.getStringAt(3).toInt());
        }
        else
        {
            mainWindow::reportError(tr("Error: invalid card UID received in network message, out of sync"));
        }
    }
    else if (pkg.getStringAt(0) == "setPosition" && pkg.length() == 4)
    {
        if (card::currentCards.contains(pkg.getStringAt(1).toInt()))
        {
            card::currentCards[pkg.getStringAt(1).toInt()]->setPosition(QPoint(pkg.getStringAt(2).toInt(), pkg.getStringAt(3).toInt()));
        }
        else
        {
            mainWindow::reportError(tr("Error: invalid card UID received in network message, out of sync"));
        }
    }
    else if (pkg.getStringAt(0) == "addCard" && pkg.length() == 3)
    {
        auto nv = this->getInventoryFromID(pkg.getStringAt(1).toInt());
        if (nv)
        {
            if (card::currentCards.contains(pkg.getStringAt(2).toInt()))
            {
                nv->addCard(card::currentCards[pkg.getStringAt(2).toInt()]);
            }
            else
            {
                mainWindow::reportError(tr("Error: invalid card UID received in network message, out of sync"));
            }
        }
        else
        {
            mainWindow::reportError(tr("Error: invalid inventory ID received in network message, out of sync"));
        }
    }
    else if (pkg.getStringAt(0) == "removeCard" && pkg.length() == 3)
    {
        auto nv = this->getInventoryFromID(pkg.getStringAt(1).toInt());
        if (nv)
        {
            if (card::currentCards.contains(pkg.getStringAt(2).toInt()))
            {
                nv->removeCard(pkg.getStringAt(2).toInt());
            }
            else
            {
                mainWindow::reportError(tr("Error: invalid card UID received in network message, out of sync"));
            }
        }
        else
        {
            mainWindow::reportError(tr("Error: invalid inventory ID received in network message, out of sync"));
        }
    }
    else if (pkg.getStringAt(0) == "addCardToBoard" && pkg.length() == 4)
    {
        if (card::currentCards.contains(pkg.getStringAt(1).toInt()))
        {
            this->state->gameBoard->addCardToBoard(card::currentCards[pkg.getStringAt(1).toInt()], QPoint(pkg.getStringAt(2).toInt(), pkg.getStringAt(3).toInt()));
        }
        else
        {
            mainWindow::reportError(tr("Error: invalid card UID received in network message, out of sync"));
        }
        return true;
    }
    else if (pkg.getStringAt(0) ==  "animateCardSilverAction" && pkg.length() == 2)
    {
        if (card::currentCards.contains(pkg.getStringAt(1).toInt()))
        {
            card::currentCards[pkg.getStringAt(1).toInt()]->animateSilverAction();
        }
        else
        {
            mainWindow::reportError(tr("Error: invalid card UID received in network message, out of sync"));
        }
        return true;
    }
    else if (pkg.getStringAt(0) == "setSilverActionsDisabledForTurn" && pkg.length() == 2)
    {
        this->state->setSilverActionsDisabledForTurn(pkg.getStringAt(1).toInt());
    }
    else if (pkg.getStringAt(0) == "setPlayerPlacedCardOnBoard" && pkg.length() == 2)
    {
        this->state->setPlayerPlacedCardOnBoard(pkg.getStringAt(1).toInt());
    }
    else if (pkg.getStringAt(0) == "runTurnStartedEvent")
    {
        doNotSendGameEvents = false;
        this->state->runEvent(QStringList() << "turnStarted" << QString::number(this->state->getCurrentPlayer()));
        this->state->checkConsistency();
        if (this->state->isLocalUser(this->state->getCurrentPlayer()) || this->isObserver())
        {
            this->playSoundEffect("cardsounds/Passturn.wav", false);
        }
        return true;
    }
    else if (pkg.getStringAt(0) == "updateScoreTable")
    {
        this->updateScoreTable();
    }
    else
    {
        mainWindow::reportError(tr("Error: invalid game event received from the network (%1)").arg(pkg.toDebugString()));
    }
    return false;
}

void mainWindow::showEditor(QWidget* sourceWidget, QString catName, const QList<QTreeWidgetItem*>& board_id, const QList<QTreeWidgetItem*>& deck_id, bool allow_board_change, bool allow_deck_change)
{
    static bool init = false;

    this->_editorAllowBoardChange = allow_board_change;
    this->_editorAllowDeckChange = allow_deck_change;
    this->editorSourceWidget = sourceWidget;

    if (!init)
        connect(this->ui->editorComponentTypeComboBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, [this](int idx)
    {
        if (idx == 0)
        {
            this->ui->editorSideBarStackedWidget->setCurrentIndex(this->ui->editorSideBarStackedWidget->indexOf(this->ui->boardPage));
            this->ui->editorViewStackedWidget->setCurrentIndex(this->ui->editorViewStackedWidget->indexOf(this->ui->boardViewPage));
            this->ui->editorTitleLabel->setText(this->ui->editorBoardSelectComboBox->count() ? this->ui->editorBoardSelectComboBox->currentText() : "");
        }
        else if (idx == 1)
        {
            this->ui->editorSideBarStackedWidget->setCurrentIndex(this->ui->editorSideBarStackedWidget->indexOf(this->ui->deckPage));
            this->ui->editorViewStackedWidget->setCurrentIndex(this->ui->editorViewStackedWidget->indexOf(this->ui->deckViewPage));
            this->ui->editorTitleLabel->setText(this->ui->editorDeckSelectComboBox->count() ? this->ui->editorDeckSelectComboBox->currentText() : "");
        }
    });

    this->ui->editorBoardSelectComboBox->clear();
    QFileInfoList boards_ = QDir(DATA_PATH "/boards/").entryInfoList(QStringList() << "*.cfg");
    boards_.append(QDir(userContentPath() + "/boards/").entryInfoList(QStringList() << "*.cfg"));
    for (auto& board : boards_)
    {
        cfgFile cfg(board.absoluteFilePath());
        if (!QCoreApplication::arguments().contains("--hidden-boards") && cfg.hasKey("hidden") && cfg.readValue("hidden") == "true") continue;
        this->ui->editorBoardSelectComboBox->addItem(cfg.readLocalizedValue("name", getLanguageCode()), QVariant(cfg.readValues("background") + cfg.readValues("tilemap") + cfg.readValues("graphicsview_background") + (QStringList() << cfg.readLocalizedValue("description", this->getLanguageCode()))
                                                     + cfg.readValues("whiteTile") + cfg.readValues("yellowTile") + cfg.readValues("blueTile") + cfg.readValues("greenTile") + cfg.readValues("disabledTile") + cfg.readValues("orangeTile")+ cfg.readValues("magentaTile") + (QStringList() << cfg.fileName()) +
                                                     (cfg.hasKey("playerTileFilenamePrefix") ? cfg.readValues("playerTileFilenamePrefix") : QStringList{""}) +
                                                     (cfg.hasKey("tileOverlayFilenamePrefix") ? cfg.readValues("tileOverlayFilenamePrefix") : QStringList{""})));
    }

    this->ui->editorDeckSelectComboBox->clear();
    QFileInfoList decks_ = QDir(DATA_PATH "/cards/decks/").entryInfoList(QStringList() << "*.cfg");
    decks_.append(QDir(userContentPath() + "/cards/decks/").entryInfoList(QStringList() << "*.cfg"));
    for (auto& deck : decks_)
    {
        cfgFile cfg(deck.absoluteFilePath());
        this->ui->editorDeckSelectComboBox->addItem(cfg.readLocalizedValue("name", getLanguageCode()), QVariant(cfg.readValues("cardlist") + cfg.readValues("pictureSet") + cfg.readValues("namePictureSet") + (QStringList() << cfg.readLocalizedValue("description", this->getLanguageCode()) << cfg.fileName())));
    }

    suppressChanges = true;

    if (!init)
    {
        connect(this->ui->editorBoardSelectComboBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, [this](int idx)
        {
            if (idx != -1)
            {
                if (this->hostMode && this->_editorAllowBoardChange && !suppressChanges)
                {
                    this->setLobbyBoardFromEditor(this->ui->editorBoardSelectComboBox->itemData(idx).toStringList()[8]);
                }
                this->ui->boardPreview->drawBackground(this->ui->editorBoardSelectComboBox->itemData(idx).toStringList()[11],
                                                       this->ui->editorBoardSelectComboBox->itemData(idx).toStringList()[0],
                                                       this->ui->editorBoardSelectComboBox->itemData(idx).toStringList()[1],
                                                       this->ui->editorBoardSelectComboBox->itemData(idx).toStringList()[2],
                                                       this->ui->editorBoardSelectComboBox->itemData(idx).toStringList()[4],
                                                       this->ui->editorBoardSelectComboBox->itemData(idx).toStringList()[5],
                                                       this->ui->editorBoardSelectComboBox->itemData(idx).toStringList()[6],
                                                       this->ui->editorBoardSelectComboBox->itemData(idx).toStringList()[7],
                                                       this->ui->editorBoardSelectComboBox->itemData(idx).toStringList()[8],
                                                       this->ui->editorBoardSelectComboBox->itemData(idx).toStringList()[9],
                                                       this->ui->editorBoardSelectComboBox->itemData(idx).toStringList()[10],
                                                       this->ui->editorBoardSelectComboBox->itemData(idx).toStringList()[12],
                                                       this->ui->editorBoardSelectComboBox->itemData(idx).toStringList()[13],
                                                       QString{});
                this->ui->editorBoardDescLabel->setText(this->ui->editorBoardSelectComboBox->itemData(idx).toStringList()[3]);
                if (this->ui->editorComponentTypeComboBox->currentIndex() == 0)
                    this->ui->editorTitleLabel->setText(this->ui->editorBoardSelectComboBox->currentText());
            }
        });

        connect(this->ui->editorDeckSelectComboBox, qOverload<int>(&QComboBox::currentIndexChanged), this, [this](int idx)
        {
            if (idx != -1)
            {
                if (this->hostMode && this->_editorAllowDeckChange && !suppressChanges)
                {
                    this->setLobbyDeckFromEditor(this->ui->editorDeckSelectComboBox->itemData(idx).toStringList()[4]);
                }
                if (this->ui->editorComponentTypeComboBox->currentIndex() == 1)
                {
                    this->ui->editorTitleLabel->setText(this->ui->editorDeckSelectComboBox->currentText());
                }
                this->ui->editorDeckDescLabel->setText(this->ui->editorDeckSelectComboBox->itemData(idx).toStringList()[3]);
                for (auto l : this->deckLabels)
                {
                    if (l)
                    {
                        this->ui->scrollAreaWidgetContents->layout()->removeWidget(l);
                        l->deleteLater();
                    }
                }
                this->deckLabels.clear();
                QString cardlist = this->ui->editorDeckSelectComboBox->itemData(idx).toStringList()[0];
                QString picset = this->ui->editorDeckSelectComboBox->itemData(idx).toStringList()[1];
                QString namepicset = this->ui->editorDeckSelectComboBox->itemData(idx).toStringList()[2];

                auto cardList = getDeckCardList(cardlist);
                int colcount = std::max(1, (this->width() - 300) / 260);
                int rowcounter = 0;
                int colcounter = 0;
                int hucksterCounter = 1;
                for (const auto& p : cardList.first)
                {
                    QLabel* cplabel = new QLabel;
                    QLabel* cclabel = new QLabel;
                    cclabel->setTextFormat(Qt::RichText);
                    this->deckLabels.push_back(cplabel);
                    this->deckLabels.push_back(cclabel);
                    cclabel->setText(QString("×%1").arg(QString::number(p.second)));
                    cclabel->setStyleSheet("font-size:20pt;");
                    card* crd = new card(p.first, -1, this->state, -2);
                    cplabel->setPixmap(QPixmap::fromImage(crd->renderToPixmap(DrawingType::Full, {}, picset, namepicset)));
                    dynamic_cast<QGridLayout*>(this->ui->scrollAreaWidgetContents->layout())->addWidget(cplabel, rowcounter, colcounter % colcount);
                    dynamic_cast<QGridLayout*>(this->ui->scrollAreaWidgetContents->layout())->addWidget(cclabel, rowcounter + 1, colcounter % colcount);
                    dynamic_cast<QGridLayout*>(this->ui->scrollAreaWidgetContents->layout())->addItem(new QSpacerItem(10, 20), rowcounter + 2, colcounter % colcount);
                    this->ui->scrollAreaWidgetContents->layout()->setAlignment(cclabel, Qt::AlignHCenter);
                    this->ui->scrollAreaWidgetContents->layout()->setAlignment(cplabel, Qt::AlignHCenter);
                    if (colcounter % colcount == colcount - 1) rowcounter += 3;
                    ++colcounter;
                    if (crd) crd->deleteCardLater();
                }
                if (colcounter % colcount != 0)
                {
                    rowcounter += 3;
                    colcounter += colcount - (colcounter % colcount);
                }
                for (const auto& p : cardList.second)
                {
                    QLabel* cplabel = new QLabel;
                    QLabel* cclabel = new QLabel;
                    cclabel->setTextFormat(Qt::RichText);
                    this->deckLabels.push_back(cplabel);
                    this->deckLabels.push_back(cclabel);
                    cclabel->setText(QString(tr("Huckster #%1").arg(QString::number(hucksterCounter))));
                    cclabel->setStyleSheet("font-size:20pt;");
                    card* crd = new card(p.first, -1, this->state, -2);
                    cplabel->setPixmap(QPixmap::fromImage(crd->renderToPixmap(DrawingType::Full, {}, picset, namepicset)));
                    dynamic_cast<QGridLayout*>(this->ui->scrollAreaWidgetContents->layout())->addWidget(cplabel, rowcounter, colcounter % colcount);
                    dynamic_cast<QGridLayout*>(this->ui->scrollAreaWidgetContents->layout())->addWidget(cclabel, rowcounter + 1, colcounter % colcount);
                    dynamic_cast<QGridLayout*>(this->ui->scrollAreaWidgetContents->layout())->addItem(new QSpacerItem(10, 20), rowcounter + 2, colcounter % colcount);
                    this->ui->scrollAreaWidgetContents->layout()->setAlignment(cclabel, Qt::AlignHCenter);
                    this->ui->scrollAreaWidgetContents->layout()->setAlignment(cplabel, Qt::AlignHCenter);
                    if (colcounter % colcount == colcount - 1) rowcounter += 3;
                    ++colcounter;
                    ++hucksterCounter;
                    if (crd) crd->deleteCardLater();
                }
            }
        });
    }


    if (catName.isEmpty() || catName == "board")
    {
        this->ui->editorSideBarStackedWidget->setCurrentIndex(this->ui->editorSideBarStackedWidget->indexOf(this->ui->boardPage));
        this->ui->editorViewStackedWidget->setCurrentIndex(this->ui->editorViewStackedWidget->indexOf(this->ui->boardViewPage));
        this->ui->editorComponentTypeComboBox->setCurrentIndex(0);
    }
    else if (catName == "deck")
    {
        this->ui->editorSideBarStackedWidget->setCurrentIndex(this->ui->editorSideBarStackedWidget->indexOf(this->ui->deckPage));
        this->ui->editorViewStackedWidget->setCurrentIndex(this->ui->editorViewStackedWidget->indexOf(this->ui->deckViewPage));
        this->ui->editorComponentTypeComboBox->setCurrentIndex(1);
    }
    this->ui->editorDeckSelectComboBox->setCurrentIndex(-1);
    this->ui->editorBoardSelectComboBox->setCurrentIndex(-1);
    if (deck_id.size())
    {
        auto idx = this->findDataInComboBox(this->ui->editorDeckSelectComboBox, this->getLobbySelectedDeck(), 4);
        this->ui->editorDeckSelectComboBox->setCurrentIndex(idx < 0 ? 0 : idx);
    }
    if ((deck_id.isEmpty() || this->ui->editorDeckSelectComboBox->currentIndex() < 0)) this->ui->editorDeckSelectComboBox->setCurrentIndex(0);
    if (board_id.size())
    {
        auto idx = this->findDataInComboBox(this->ui->editorBoardSelectComboBox, this->getLobbySelectedBoard(), 8);
        this->ui->editorBoardSelectComboBox->setCurrentIndex(idx < 0 ? 0 : idx);
    }
    if ((board_id.isEmpty() || this->ui->editorBoardSelectComboBox->currentIndex() < 0)) this->ui->editorBoardSelectComboBox->setCurrentIndex(0);

    this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->editorPage));

    suppressChanges = false;
    init = true;
}

QStringList mainWindow::getGameDataUpdateForPublicServer()
{
    QString deck = "";
    auto sel_decks = this->ui->decksTreeWidget->selectedItems();
    if (sel_decks.size())
    {
        deck = sel_decks.front()->data(0, Qt::UserRole).toString();
    }

    QString board = "";
    auto sel_boards = this->ui->boardsTreeWidget->selectedItems();
    if (sel_boards.size())
    {
        board = sel_boards.front()->data(0, Qt::UserRole).toString();
    }

    return QStringList()    << board
           << deck
           << QString::number(this->ui->gameModeComboBox->currentIndex());
}

QString mainWindow::saveGameAfterError()
{
    int counter = 0;
    QString savePath;
    while (true)
    {
        QFileInfo inf(QDir::homePath() + "/lost-bazaar-" + QString::number(counter) + ".lbs");
        if (inf.exists())
        {
            ++counter;
        }
        else
        {
            savePath = inf.filePath();
            this->save().saveToFile(savePath, false);
            return savePath;
        }
    }
}

void mainWindow::enableGUIAfterPassiveTurn()
{
    this->state->storage->viewport()->setEnabled(true);
    this->state->p1Inventory->viewport()->setEnabled(true);
    this->state->p2Inventory->viewport()->setEnabled(true);
    this->state->p3Inventory->viewport()->setEnabled(true);
    this->state->p4Inventory->viewport()->setEnabled(true);
    this->state->huckster->viewport()->setEnabled(true);
    this->state->trash->viewport()->setEnabled(true);
}

QPair<QString, QString> mainWindow::getGameFileForTutorialPage(int pagenum)
{
    QString lang = languageConfigValueToLocaleName(this->getConfigVariable("language"));
    if (lang.isEmpty() || tutorialPageCountToFilename(lang, pagenum).isEmpty()) lang = DEFAULT_LANGUAGE;
    auto gameFile = getPrimaryPath("help/" + lang + "_tutorial_" + QString::number(pagenum) + ".lbs", true);
    auto script = getPrimaryPath("help/" + lang + "_tutorial_" + QString::number(pagenum) + ".js", true);
    return QPair<QString, QString>(gameFile, script);
}

void mainWindow::refillPlayerTypeComboBoxes(QStringList networkNames)
{
    if (this->gameInProgress) return;
    this->ui->p1TypeComboBox->clear();
    this->ui->p2TypeComboBox->clear();
    this->ui->p3TypeComboBox->clear();
    this->ui->p4TypeComboBox->clear();
    this->ui->p1TypeComboBox->addItem(tr("Local player (%1)").arg(this->getConfigVariable("username")), QVariant(QStringList() << this->getConfigVariable("username")));
    this->ui->p2TypeComboBox->addItem(tr("Local player (%1)").arg(this->getConfigVariable("username")), QVariant(QStringList() << this->getConfigVariable("username")));
    this->ui->p3TypeComboBox->addItem(tr("Local player (%1)").arg(this->getConfigVariable("username")), QVariant(QStringList() << this->getConfigVariable("username")));
    this->ui->p4TypeComboBox->addItem(tr("Local player (%1)").arg(this->getConfigVariable("username")), QVariant(QStringList() << this->getConfigVariable("username")));
    this->ui->p1TypeComboBox->addItem(tr("AI"));
    this->ui->p2TypeComboBox->addItem(tr("AI"));
    this->ui->p3TypeComboBox->addItem(tr("AI"));
    this->ui->p4TypeComboBox->addItem(tr("AI"));
    for (const auto& name : networkNames)
    {
        if (name != this->getConfigVariable("username"))
        {
            this->ui->p1TypeComboBox->addItem(tr("Network player: %1").arg(name), QVariant(QStringList() << name));
            this->ui->p2TypeComboBox->addItem(tr("Network player: %1").arg(name), QVariant(QStringList() << name));
            this->ui->p3TypeComboBox->addItem(tr("Network player: %1").arg(name), QVariant(QStringList() << name));
            this->ui->p4TypeComboBox->addItem(tr("Network player: %1").arg(name), QVariant(QStringList() << name));
        }
    }
}

void mainWindow::cleanUpAfterUnloadingGame()
{
    this->loadedGameName = QString();
    this->ui->gameModeComboBox->setCurrentIndex(0);
    this->ui->gameModeComboBox->setEnabled(true);
    this->setPlayerSlotVisible(1, true);
    this->setPlayerSlotVisible(2, true);
    this->setPlayerSlotVisible(3, false);
    this->setPlayerSlotVisible(4, false);
    this->ui->p1Widget->setEnabled(true);
    this->ui->p2Widget->setEnabled(true);
    this->ui->p3Widget->setEnabled(true);
    this->ui->p4Widget->setEnabled(true);
    this->ui->hucksterCardsComboBox->setVisible(true);
    this->ui->hucksterCardsLabel->setVisible(true);
    this->clearPlayerSlots();
    this->setTeamLabels(QStringList() << "" << "" << "" << "");
    this->ui->loadSaveGameButton->setVisible(true);
    this->ui->p1TypeComboBox->setEnabled(true);
    this->ui->p2TypeComboBox->setEnabled(true);
    this->ui->p3TypeComboBox->setEnabled(true);
    this->ui->p4TypeComboBox->setEnabled(true);
    this->ui->p1NickLineEdit->setEnabled(true);
    this->ui->p2NickLineEdit->setEnabled(true);
    this->ui->p3NickLineEdit->setEnabled(true);
    this->ui->p4NickLineEdit->setEnabled(true);
}

void mainWindow::setConfigVariable(const QString& key, const QString& value)
{
    this->settings->setValue(key, value);
}

QString mainWindow::getConfigVariable(const QString& key)
{
    if (key == "username" && QCoreApplication::arguments().contains("--test-user")) return "test user";
    if (key == "username" && QCoreApplication::arguments().contains("--test-user2")) return "test user 2";
    if ((key == "gameAnimations" || key == "boardRotAnim" || key == "enableSoundEffects") && fastForward) return "0";
    return settings->value(key).toString();
}

mainWindow::~mainWindow()
{
    this->mPlayer->stop();
    this->mPlayer->deleteLater();
    delete ui;
}

QString mainWindow::processIncludes(const QString& code)
{
    QRegularExpression includeRegex(" *\\t* *//@include ([0-9a-z]+\\.)+[0-9a-z]+ *\\t* *");
    QStringList codeL = code.split(lineEnd);
    QSet<QString> fileSet;
    QSet<QString> doneSet;

    bool done = false;
    while (!done)
    {
        QSetIterator<QString> it(fileSet);
        while (it.hasNext())
        {
            QString fName = it.next();
            fName.replace(".", "/");
            QFile f(getPrimaryPath("" + fName + ".js"));
            if (f.open(QFile::ReadOnly))
            {
                QTextStream s(&f);
                //s.setCodec("UTF-8");
                codeL = s.readAll().split(lineEnd) + codeL;
                f.close();
            }
            else
            {
                mainWindow::reportError(tr("Error: invalid include directive (wrong path or filename): %1").arg(fName));
                return "";
            }
        }
        it.toFront();
        while (it.hasNext())
        {
            doneSet.insert(it.next());
        }
        fileSet.clear();

        while (codeL.indexOf(includeRegex) != -1)
        {
            qint64 pos = codeL.indexOf(includeRegex);
            QString str = codeL[pos];
            codeL.removeAt(pos);
            while (str.endsWith(' ') || str.endsWith('\t')) str.chop(1);
            while (str.startsWith(' ') || str.startsWith('\t')) str = str.mid(1);
            str = str.mid(11);
            if (!doneSet.contains(str))
            {
                fileSet.insert(str);
            }
        }

        if (fileSet.isEmpty() && codeL.indexOf(includeRegex) == -1) done = true;
    }

    return codeL.join(lineEnd);
}

fileFormat::saveFile mainWindow::save(bool forOOSCheck) const
{
    fileFormat::saveFile saved;
    saved.addItem("deck", this->startingDeck);
    saved.addItem("cardPictureSet", this->cardPictureSet);
    saved.addItem("cardNamePictureSet", this->cardNamePictureSet);
    QStringMap um;
    for (int i = 0; i < this->playerNicknames.size(); i++)
    {
        um["u" + QString::number(i)] = this->playerNicknames[i];
    }
    saved.addItem("playerNicknames", QString(), um);
    saved.addItem("cardGlobalUID", QString::number(card::globalUID));
    if (!forOOSCheck)
    {
        saved.addItems(this->state->save());
        for (int i = 0; i < this->gameEvents.size(); ++i)
        {
            saved.addItem("event" + QString::number(i), QString::fromUtf8(this->gameEvents[i].serialize().toBase64()));
        }
    }
    return saved;
}

bool mainWindow::isPassiveTurn() const
{
    return (((this->state->p1AI || this->state->p1Network) && this->state->getCurrentPlayer() == 1) ||
            ((this->state->p2AI || this->state->p2Network) && this->state->getCurrentPlayer() == 2) ||
            ((this->state->p3AI || this->state->p3Network) && this->state->getCurrentPlayer() == 3) ||
            ((this->state->p4AI || this->state->p4Network) && this->state->getCurrentPlayer() == 4));
}

bool mainWindow::isGamePageVisible() const
{
    return this->ui->stackedWidget->currentWidget() == this->ui->gamePage;
}

int mainWindow::singleLocalUser() const
{
    if (this->state)
    {
        if (this->state->isLocalUser(1) && !this->state->isLocalUser(2) && !this->state->isLocalUser(3) && !this->state->isLocalUser(4)) return 1;
        if (this->state->isLocalUser(2) && !this->state->isLocalUser(3) && !this->state->isLocalUser(4) && !this->state->isLocalUser(1)) return 2;
        if (this->state->isLocalUser(3) && !this->state->isLocalUser(4) && !this->state->isLocalUser(1) && !this->state->isLocalUser(2)) return 3;
        if (this->state->isLocalUser(4) && !this->state->isLocalUser(1) && !this->state->isLocalUser(2) && !this->state->isLocalUser(3)) return 4;
    }
    return 0;
}

QString mainWindow::getSavePath()
{
    auto locs = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation);

    if (!locs.isEmpty())
    {
        if (!QDir().exists(locs[0]))
        {
            QDir().mkpath(locs[0]);
        }
        return locs[0];
    }
    return {};
}

QString mainWindow::readStyleSheet()
{
    QString baseStyleSheet = " ";
    if(getConfigVariable("overrideDefaultFontSizes").toInt())
    {
        baseStyleSheet.append("QWidget, QMessageBox, QFileDialog, QInputDialog { font-size: 12pt; }");
    }
    QFile styleSheetFile(getPrimaryPath("theme.css"));
    if (styleSheetFile.open(QFile::ReadOnly))
    {
        QTextStream ssStream(&styleSheetFile);
#ifdef Q_OS_HAIKU
        return baseStyleSheet+ssStream.readAll().replace("./data/", "/boot/system/data/lost-bazaar/data/");
#else
#ifdef Q_OS_ANDROID
        return baseStyleSheet+ssStream.readAll().replace("./data/", "assets:/data/");
#else
#ifdef Q_OS_WASM
        return baseStyleSheet+ssStream.readAll().replace("./data/", ":/data/");
#else
        return baseStyleSheet+ssStream.readAll();
#endif
#endif
#endif
        styleSheetFile.close();
    }
    return {};
}

QString mainWindow::getNewUsername()
{
    QString name = qEnvironmentVariable("USER");
    if (name.isEmpty()) name = qEnvironmentVariable("USERNAME");
    if (name.isEmpty()) name = tr("unnamed");
    return name;
}

void mainWindow::playSoundEffect(const QString& name, bool everyone)
{
    if (mainWindow::getConfigVariable("enableSoundEffects").toInt())
    {
#ifndef Q_OS_WASM
        //QSound snd(getPrimaryPath("soundeffects/" + name));
        //snd.play();
        QSoundEffect* effect = new QSoundEffect(this);
        effect->setLoopCount(1);
        effect->setSource(QUrl::fromLocalFile(getPrimaryPath("soundeffects/" + name)));
        connect(effect, &QSoundEffect::playingChanged, [effect]()
        {
            if (!effect->isPlaying()) effect->deleteLater();
        });
        effect->play();
#endif
    }
    if (everyone)
    {
        this->sendGameEvent(QStringList() << "soundEffect" << name);
    }
}

QPair<QList<QPair<QString, int>>, QList<QPair<QString, int>>> mainWindow::getDeckCardList(const QString& cardList)
{
    QFile deckF(getPrimaryPath("cards/decks/" + cardList + ".txt"));
    if (!deckF.open(QFile::ReadOnly))
    {
        mainWindow::reportError(tr("Error: cannot open deck file (%1)").arg(startingDeck));
        return {};
    }
    QTextStream deckStream(&deckF);
    //deckStream.setCodec("UTF-8");
    QList<QPair<QString, int>> cards;
    QList<QPair<QString, int>> hucksterCards;
    while (!deckStream.atEnd())
    {
        QString cardLine = deckStream.readLine();
        auto cardLineSplit = cardLine.split(" ");
        QString cardName = cardLineSplit.first();
        int cardNum = 1;
        bool hucksterCard = false;
        if (cardLineSplit.size() > 1)
        {
            cardNum = cardLine.split(" ")[1].toInt();
            if (cardLineSplit.size() > 2 && cardLineSplit[2] == "h") hucksterCard = true;
        }
        if (!cardName.isEmpty())
        {
            if (hucksterCard)
            {
                hucksterCards.append(QPair<QString, int>(cardName, cardNum));
            }
            else
            {
                cards.append(QPair<QString, int>(cardName, cardNum));
            }
        }
    }
    return QPair<QList<QPair<QString, int>>, QList<QPair<QString, int>>> {cards, hucksterCards};
}

void mainWindow::applyStylesheetBugWorkAround()
{
    //There is a bug in Qt stylesheets where the popup lists of QComboBox widgets that are not on the default/selected page of a QStackedWidget are not styled properly.
    //Giving the ComboBoxes a new view after setting the stylesheet on mainWindow seems to fix the problem.
    QList<QComboBox*> comboBoxes = {
        ui->langSelectionComboBox,
        ui->gameModeComboBox,
        ui->hucksterCardsComboBox,
        ui->p4TypeComboBox,
        ui->p1TypeComboBox,
        ui->p2TypeComboBox,
        ui->p3TypeComboBox,
        ui->editorComponentTypeComboBox,
        ui->editorBoardSelectComboBox,
        ui->editorDeckSelectComboBox,
    };
    for(auto cB: comboBoxes) cB->setView(new QListView{});
}

QMessageBox::StandardButton mainWindow::msgBoxQuestion(QWidget *parent, const QString &title, const QString &text)
{
    QMessageBox msgBox{QMessageBox::NoIcon, title, text, QMessageBox::NoButton, parent};
    const auto* yesButton = msgBox.addButton(tr("Yes"), QMessageBox::YesRole);
    msgBox.addButton(tr("No"), QMessageBox::NoRole);
    msgBox.exec();
    if(msgBox.clickedButton() == yesButton) {
        return QMessageBox::Yes;
    }
    return QMessageBox::No;
}

QMessageBox::StandardButton mainWindow::msgBoxInfo(QWidget *parent, const QString &title, const QString &text)
{
    QMessageBox msgBox{QMessageBox::NoIcon, title, text, QMessageBox::NoButton, parent};
    msgBox.addButton(tr("OK"), QMessageBox::AcceptRole);
    return static_cast<QMessageBox::StandardButton>(msgBox.exec());
}

void mainWindow::reportError(const QString& error)
{
    QMessageBox mBox;
    mBox.addButton(tr("OK"), QMessageBox::AcceptRole);
    QFile styleSheetFile(getPrimaryPath("theme.css"));
    if (styleSheetFile.open(QFile::ReadOnly))
    {
        QTextStream ssStream(&styleSheetFile);
#ifdef Q_OS_HAIKU
        mBox.setStyleSheet(ssStream.readAll().replace("./data/", "/boot/system/data/lost-bazaar/data/"));
#else
#ifdef Q_OS_ANDROID
        mBox.setStyleSheet(ssStream.readAll().replace("./data/", "assets:/data/"));
#else
#ifdef Q_OS_WASM
        mBox.setStyleSheet(ssStream.readAll().replace("./data/", ":/data/"));
#else
        mBox.setStyleSheet(ssStream.readAll());
#endif
#endif
#endif
        styleSheetFile.close();
    }
    if (instance->gameInProgress && instance->state)
    {
        instance->state->writeToGameLog(tr("[!!ERROR!!, PLAYER %1] %2").arg(QString::number(instance->state->getCurrentPlayer()), error), {1, 2, 3, 4}, true, false, true);
    }
    mBox.setWindowTitle(tr("Error"));
    mBox.setText(error);
    mBox.exec();
}

QStringList mainWindow::getPlayerNickNames()
{
    return playerNicknames;
}

void mainWindow::startWaitingForPublicGameJoin()
{
    this->ui->startNewPublicServerGamebutton->setEnabled(false);
    this->ui->publicServerNetworkInfoLabel->setText(tr("Connecting to game..."));
    this->ui->publicServerNetworkInfoLabel->setVisible(true);
    this->ui->publicGameList->setVisible(false);
    this->publicServerTimer->stop();
    this->waitingForPublicGameJoin = true;
}

void mainWindow::renderCards()
{
    QStringList langs = {"hu_HU", "pl_PL", "en_US"};

    QDir directory("data/cards/cfg");
    QStringList cfgs = directory.entryList(QStringList() << "*.cfg");

    for (auto cfg : cfgs)
    {
        cfg.chop(4);
        for (const auto& lang : langs)
        {
            card* crd = new card(cfg, -1, this->state);

            auto saveFileName = "data/cards/prerendered/" + lang + "_" + cfg + ".png";
            if (QFile::exists(saveFileName))
            {
                QFile::remove(saveFileName);
            }
            crd->renderToPixmap(DrawingType::Full, lang, "pictures", "names").save(saveFileName);

            crd->deleteCardLater();
        }
    }
}

void mainWindow::disconnectFromCurrentNetworkGame()
{
    this->networkManager->disconnectFromCurrentGame();
    this->networkManager->reset();
}

void mainWindow::serverInfoReceived(const QString &info)
{
    auto msgBox = new QPlainTextEdit{nullptr};
    msgBox->setPlainText(info);
    msgBox->setAttribute(Qt::WA_DeleteOnClose);
    msgBox->show();
}

void mainWindow::usernameAlreadyInUse()
{
    mainWindow::reportError(tr("Can't connect, there is already a player with your username in the game!"));
    this->waitingForPublicGameJoin = false;
    this->ui->publicGameList->setVisible(true);
    this->ui->startNewPublicServerGamebutton->setEnabled(true);
    this->ui->publicServerNetworkInfoLabel->setVisible(false);
    this->sendReqToPublicServer("gamelist", {});
    this->publicServerTimer->start();
}

void mainWindow::wrongPassword()
{
    mainWindow::reportError(tr("Can't connect: wrong password."));
    this->waitingForPublicGameJoin = false;
    this->ui->publicGameList->setVisible(true);
    this->ui->startNewPublicServerGamebutton->setEnabled(true);
    this->ui->publicServerNetworkInfoLabel->setVisible(false);
    this->sendReqToPublicServer("gamelist", {});
    this->publicServerTimer->start();
    this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->publicServerPage), slidingStackedWidget::AnimationDirection::LeftToRight);
}

void mainWindow::updateEndTurnButtonState()
{
    bool disableUI = (this->uiDisabledCounter > 0 && this->state->cancelEnabledCounter == 0) || this->isPassiveTurn() || this->uiDisabledForAnim ||
                     this->state->p1Inventory->isHucksterChoiceModeActive() || this->state->p2Inventory->isHucksterChoiceModeActive() || this->state->p3Inventory->isHucksterChoiceModeActive() || this->state->p4Inventory->isHucksterChoiceModeActive();

    this->ui->cardNumInfoLabel->setText(tr("Items in deck/trash: %1/%2").arg(QString::number(this->state->mainInventory->getAllCardsInternal().size()), QString::number(this->state->trash->getAllCardsInternal().size())));
    if (this->state->cancelEnabledCounter > 0)
    {
        this->ui->endTurnButton->setEnabled(!this->isPassiveTurn());
        this->ui->endTurnButton->setText(tr("Cancel"));
        return;
    }
    if (this->state->currentPlayerCanDrawCard())
    {
        this->ui->endTurnButton->setEnabled(!disableUI);
        this->ui->endTurnButton->setText(tr("Draw Item"));
        return;
    }
    auto r = this->state->currentPlayerCanEndTurn();
    if (r == 0 && !this->state->isAITurn() && !this->state->isNetworkTurn())
    {
        this->ui->endTurnButton->setText(tr("Too many items"));
        return;
    }
    if (r == -1 && !this->state->isAITurn() && !this->state->isNetworkTurn())
    {
        this->ui->endTurnButton->setText(tr("Too few items"));
        return;
    }
    this->ui->endTurnButton->setText(tr("End Turn"));

    r = this->state->currentPlayerCanEndTurn();
    if (r == -2)
    {
        this->ui->endTurnButton->setEnabled(!disableUI);
    }
    else
    {
        this->ui->endTurnButton->setEnabled(r);
    }
}

void mainWindow::replayFromSave(const QString& fileName)
{
    fileFormat::saveFile f;
    f.loadFromFile(fileName, false);

    int evCounter = 0;
    this->setReplayMode(true);
    while (true)
    {
        auto it = f.getItemByKey("event" + QString::number(evCounter)).getValueAsString();
        if (it.isEmpty())
        {
            break;
        }
        else
        {
            auto pkg = network::networkDataPackage(QByteArray::fromBase64(it.toUtf8()));
            this->handleGameEvent(pkg);
        }
        evCounter++;
    }
    if (evCounter == 0)
    {
        mainWindow::reportError(tr("No replay data found in the save file!"));
        this->setReplayMode(false);
    }
}

void mainWindow::zoomIn()
{
    zoomCounter++;
    this->ui->gameBoard->scale(1.25, 1.25);
}

void mainWindow::zoomOut()
{
    zoomCounter--;
    this->ui->gameBoard->scale(0.8, 0.8);
}

void mainWindow::setDeckListHighlight(const QString& board)
{
    QStringList recommends;
    if (!board.isEmpty())
    {
        if (boards.count(board) && boards[board].hasKey("recommended_decks"))
        {
            recommends = boards[board].readValues("recommended_decks");
        }
    }
    for (int i = 0; i < recommends.size(); ++i)
    {
        recommends[i] = recommends[i].trimmed();
    }
    QTreeWidgetItemIterator it(this->ui->decksTreeWidget);
    while (*it)
    {
        if (recommends.contains((*it)->data(0, Qt::UserRole).toString()))
        {
            QFont f = (*it)->font(0);
            f.setBold(true);
            (*it)->setFont(0, f);
            //if ((*it)->parent()) (*it)->parent()->setExpanded(true);
        }
        else
        {
            QFont f = (*it)->font(0);
            f.setBold(false);
            if (!board.isEmpty())
            {
                f.setWeight(QFont::Light);
            }
            else
            {
                f.setWeight(QFont::Normal);
            }
            (*it)->setFont(0, f);
        }
        ++it;
    }
}

void mainWindow::setBoardListHighlight(const QString& deck)
{
    QStringList recommends;
    if (!deck.isEmpty())
    {
        if (decks.count(deck) && decks[deck].hasKey("recommended_boards"))
        {
            recommends = decks[deck].readValues("recommended_boards");
        }
    }
    recommends = trimStringList(recommends);
    QTreeWidgetItemIterator it(this->ui->boardsTreeWidget);
    while (*it)
    {
        if (recommends.contains((*it)->data(0, Qt::UserRole).toString()))
        {
            QFont f = (*it)->font(0);
            f.setBold(true);
            (*it)->setFont(0, f);
            //if ((*it)->parent()) (*it)->parent()->setExpanded(true);
        }
        else
        {
            QFont f = (*it)->font(0);
            f.setBold(false);
            if (!deck.isEmpty())
            {
                f.setWeight(QFont::Light);
            }
            else
            {
                f.setWeight(QFont::Normal);
            }
            (*it)->setFont(0, f);
        }
        ++it;
    }
}

void mainWindow::updateBoardDeckSelectorSidebar()
{
    if (this->ui->boardsTreeWidget->selectedItems().size())
    {
        QString boardn = this->ui->boardsTreeWidget->selectedItems().front()->data(0, Qt::UserRole).toString();
        if (boardn.startsWith("__rand"))
        {
            this->ui->boardImgLabel->setPixmap(QPixmap(getPrimaryPath("img/random.png")));
            this->ui->boardDescLabel->setText(tr("Random board."));
            this->ui->boardPreviewButton->setEnabled(false);
            this->updateNewGamePagePlayerSymbols({});
        }
        else if (boards.count(boardn))
        {
            if (boards[boardn].hasKey("board_preview_img"))
            {
                this->ui->boardImgLabel->setPixmap(QPixmap(getPrimaryPath("boards/" + boards[boardn].readValue("board_preview_img") + ".png")));
            }
            else
            {
                this->ui->boardImgLabel->setPixmap(QPixmap(getPrimaryPath("img/missing_board_img.png")));
            }
            this->ui->boardPreviewButton->setEnabled(true);
            this->updateNewGamePagePlayerSymbols(boardn);
            this->ui->boardDescLabel->setText(this->boards[boardn].readLocalizedValue("description", getLanguageCode()));
        }
        else
        {
            this->ui->boardPreviewButton->setEnabled(false);
            this->ui->boardImgLabel->setPixmap(QPixmap(getPrimaryPath("img/unknown_board_img.png")));
            this->ui->boardDescLabel->setText(tr("No board selected."));
            this->updateNewGamePagePlayerSymbols({});
        }
    }
    else
    {
        this->ui->boardPreviewButton->setEnabled(false);
        this->ui->boardImgLabel->setPixmap(QPixmap(getPrimaryPath("img/unknown_board_img.png")));
        this->ui->boardDescLabel->setText(tr("No board selected."));
        this->updateNewGamePagePlayerSymbols({});
    }

    if (this->ui->decksTreeWidget->selectedItems().size())
    {
        QString deckn = this->ui->decksTreeWidget->selectedItems().front()->data(0, Qt::UserRole).toString();
        if (deckn.startsWith("__rand"))
        {
            this->ui->deckPreviewButton->setEnabled(false);
            this->ui->deckDescLabel->setText(tr("Random deck."));
        }
        else if (decks.count(deckn))
        {
            QString descText = this->decks[deckn].readLocalizedValue("description", getLanguageCode());
            descText += tr("\nItems:");
            auto cardList = getDeckCardList(this->decks[deckn].readValue("cardlist"));
            QStringList deckCards;
            for (const auto& p : cardList.first)
            {
                deckCards.push_back(cfgFile(getPrimaryPath("cards/cfg/" + p.first + ".cfg")).readLocalizedValue("name", getLanguageCode()) + QString::fromUtf8(" ×") + QString::number(p.second));
            }
            deckCards.sort();
            descText += "\n" + deckCards.join("\n");
            descText += tr("\nHuckster:");
            for (const auto& p : cardList.second)
            {
                descText += "\n" + cfgFile(getPrimaryPath("cards/cfg/" + p.first + ".cfg")).readLocalizedValue("name", getLanguageCode()) + QString::fromUtf8(" ×") + QString::number(p.second);
            }
            this->ui->deckPreviewButton->setEnabled(true);
            this->ui->deckDescLabel->setText(descText);
        }
        else
        {
            this->ui->deckPreviewButton->setEnabled(false);
            this->ui->deckDescLabel->setText(tr("No deck selected."));
        }
    }
    else
    {
        this->ui->deckPreviewButton->setEnabled(false);
        this->ui->deckDescLabel->setText(tr("No deck selected."));
    }
}

void mainWindow::zoomReset()
{
    QTransform t = this->ui->gameBoard->transform();
    t.scale(std::pow(1.25, -zoomCounter), std::pow(1.25, -zoomCounter));
    zoomCounter = 0;
    this->ui->gameBoard->setTransform(t);
}

void mainWindow::titleSequenceFinished()
{
    this->ui->stackedWidget->setCurrentWidget(this->ui->menuPage);
}

void mainWindow::resetStartGameScreen()
{
    disableLobbyUpdates = true;
    this->refillPlayerTypeComboBoxes();
    if (this->hostMode)
    {
        if (this->privateGame)
        {
            this->ui->privateGameInfoLabel->setText(tr("This game is local. It will not be listed on any public server and network players will not be able to connect."));
        }
        else
        {
            this->ui->privateGameInfoLabel->setText(tr("This game is public. It will be listed on the server you connected to on the previous screen. Network players can join. You can start private games from the main menu."));
        }
    }
    if (!this->hostMode)
    {
        this->ui->privateGameInfoLabel->setVisible(false);
    }
    this->loadedGameName = QString();
    this->ui->replayPlayBackCheckBox->setEnabled(false);
    this->ui->replayPlayBackCheckBox->setChecked(false);
    this->updateNewGameUIForLoadedGame(QString{});
    this->ui->puzzleSelectorTreeWidget->clear();
    this->ui->lobbyConnectedUserList->clear();
    this->puzzles = QDir(DATA_PATH "/puzzles/").entryInfoList(QStringList() << "*.cfg");
    puzzles.append(QDir(userContentPath() + "/puzzles/").entryInfoList(QStringList() << "*.cfg"));
    for (auto& puzzle : puzzles)
    {
        cfgFile cfg(puzzle.absoluteFilePath());
        QTreeWidgetItem* it = new QTreeWidgetItem;
        it->setText(0, cfg.readLocalizedValue("name", this->getLanguageCode()));
        it->setText(1, cfg.readLocalizedValue("difficulty", this->getLanguageCode()));
        it->setText(2, cfg.readLocalizedValue("description", this->getLanguageCode()));
        this->ui->puzzleSelectorTreeWidget->setSortingEnabled(false);
        this->ui->puzzleSelectorTreeWidget->addTopLevelItem(it);
    }

    this->ui->lobbyChatMsgEdit->clear();
    this->ui->lobbyChatTextEdit->clear();
    this->ui->decksFilter->clear();
    this->ui->boardsFilter->clear();
    this->ui->gameModeComboBox->setCurrentIndex(0);
    this->ui->gameModeComboBox->setEnabled(true);
    this->setPlayerSlotVisible(1, true);
    this->setPlayerSlotVisible(2, true);
    this->setPlayerSlotVisible(3, false);
    this->setPlayerSlotVisible(4, false);
    this->ui->p1Widget->setEnabled(true);
    this->ui->p2Widget->setEnabled(true);
    this->ui->p3Widget->setEnabled(true);
    this->ui->p4Widget->setEnabled(true);
    this->ui->hucksterCardsComboBox->setVisible(true);
    this->ui->hucksterCardsLabel->setVisible(true);
    this->clearPlayerSlots();
    this->setTeamLabels(QStringList() << "" << "" << "" << "");
    this->ui->decksTreeWidget->setEnabled(true);
    this->ui->loadSaveGameButton->setVisible(true);
    this->ui->boardsTreeWidget->setEnabled(true);
    this->ui->p1TypeComboBox->clear();
    this->ui->p2TypeComboBox->clear();
    this->ui->p3TypeComboBox->clear();
    this->ui->p4TypeComboBox->clear();
    this->ui->p1TypeComboBox->setEnabled(true);
    this->ui->p2TypeComboBox->setEnabled(true);
    this->ui->p3TypeComboBox->setEnabled(true);
    this->ui->p4TypeComboBox->setEnabled(true);
    this->ui->p1NickLineEdit->setEnabled(true);
    this->ui->p2NickLineEdit->setEnabled(true);
    this->ui->p3NickLineEdit->setEnabled(true);
    this->ui->p4NickLineEdit->setEnabled(true);
    this->refillPlayerTypeComboBoxes();
    this->loadBoardsDecks();
    if (autoCfg.hasKey("prefillPlayers") && autoCfg.readValue("prefillPlayers") == "AIvsAI")
    {
        this->ui->p1NickLineEdit->setText("AI1");
        this->ui->p2NickLineEdit->setText("AI2");
        this->ui->p1TypeComboBox->setCurrentIndex(1);
        this->ui->p2TypeComboBox->setCurrentIndex(1);
    }
    if (autoCfg.hasKey("openHands") && autoCfg.readValue("openHands") == "true")
    {
        this->ui->openHandsCheckBox->setChecked(true);
    }
    disableLobbyUpdates = false;
}

void mainWindow::setPlayerSlotVisible(int slot, bool visible)
{
    switch (slot)
    {
        case 1:
            this->ui->p1Widget->setVisible(visible);
            break;
        case 2:
            this->ui->p2Widget->setVisible(visible);
            break;
        case 3:
            this->ui->p3Widget->setVisible(visible);
            break;
        case 4:
            this->ui->p4Widget->setVisible(visible);
            break;
    }
}

void mainWindow::gameModeComboBoxChanged(int index)
{
    if (index == 0) //1 vs. 1 adjacent
    {
        this->clearPlayerSlots();
        this->setPlayerSlotVisible(1, true);
        this->setPlayerSlotVisible(2, true);
        this->setPlayerSlotVisible(3, false);
        this->setPlayerSlotVisible(4, false);
        this->setTeamLabels(QStringList() << "" << "" << "" << "");
    }
    else if (index == 1)  //1 vs. 1 opposite
    {
        this->clearPlayerSlots();
        this->setPlayerSlotVisible(1, true);
        this->setPlayerSlotVisible(2, false);
        this->setPlayerSlotVisible(3, true);
        this->setPlayerSlotVisible(4, false);
        this->setTeamLabels(QStringList() << "" << "" << "" << "");
    }
    else if (index == 2)  //2 vs. 2
    {
        this->clearPlayerSlots();
        this->setPlayerSlotVisible(1, true);
        this->setPlayerSlotVisible(2, true);
        this->setPlayerSlotVisible(3, true);
        this->setPlayerSlotVisible(4, true);
        this->setTeamLabels(QStringList() << tr("Team A") << tr("Team B") << tr("Team A") << tr("Team B"));
    }
    else if (index == 3)  //3 players, ffa
    {
        this->clearPlayerSlots();
        this->setPlayerSlotVisible(1, true);
        this->setPlayerSlotVisible(2, true);
        this->setPlayerSlotVisible(3, true);
        this->setPlayerSlotVisible(4, false);
        this->setTeamLabels(QStringList() << "" << "" << "" << "");
    }
    else if (index == 4)  //4 players, ffa
    {
        this->clearPlayerSlots();
        this->setPlayerSlotVisible(1, true);
        this->setPlayerSlotVisible(2, true);
        this->setPlayerSlotVisible(3, true);
        this->setPlayerSlotVisible(4, true);
        this->setTeamLabels(QStringList() << "" << "" << "" << "");
    }
}

void mainWindow::clearPlayerSlots()
{
    this->ui->p1NickLineEdit->clear();
    this->ui->p2NickLineEdit->clear();
    this->ui->p3NickLineEdit->clear();
    this->ui->p4NickLineEdit->clear();
    this->ui->startGameButton->setEnabled(false);
    this->ui->p1TypeComboBox->setCurrentIndex(0);
    this->ui->p2TypeComboBox->setCurrentIndex(0);
    this->ui->p3TypeComboBox->setCurrentIndex(0);
    this->ui->p4TypeComboBox->setCurrentIndex(0);
}

void mainWindow::enableStartGameButtonIfPossible()
{
    if (!this->hostMode)
    {
        this->ui->startGameButton->setEnabled(false);
        return;
    }
    if (this->getLobbySelectedDeck() == "" || this->getLobbySelectedBoard() == "")
    {
        this->ui->startGameButton->setEnabled(false);
        return;
    }
    bool ok = true;
    if (this->ui->gameModeComboBox->currentIndex() == 2 || this->ui->gameModeComboBox->currentIndex() == 4)
    {
        ok = ok && !this->ui->p1NickLineEdit->text().isEmpty() &&
             !this->ui->p2NickLineEdit->text().isEmpty() &&
             !this->ui->p3NickLineEdit->text().isEmpty() &&
             !this->ui->p4NickLineEdit->text().isEmpty();
        ok = ok && this->ui->p1NickLineEdit->text() != this->ui->p2NickLineEdit->text() &&
             this->ui->p2NickLineEdit->text() != this->ui->p3NickLineEdit->text() &&
             this->ui->p3NickLineEdit->text() != this->ui->p4NickLineEdit->text();
    }
    if (this->ui->gameModeComboBox->currentIndex() == 0)
    {
        ok = ok && !this->ui->p1NickLineEdit->text().isEmpty() &&
             !this->ui->p2NickLineEdit->text().isEmpty();
        ok = ok && this->ui->p1NickLineEdit->text() != this->ui->p2NickLineEdit->text();
    }
    if (this->ui->gameModeComboBox->currentIndex() == 1)
    {
        ok = ok && !this->ui->p1NickLineEdit->text().isEmpty() &&
             !this->ui->p3NickLineEdit->text().isEmpty();
        ok = ok && this->ui->p1NickLineEdit->text() != this->ui->p3NickLineEdit->text();
    }
    if (this->ui->gameModeComboBox->currentIndex() == 3)
    {
        ok = ok && !this->ui->p1NickLineEdit->text().isEmpty() &&
             !this->ui->p2NickLineEdit->text().isEmpty() &&
             !this->ui->p3NickLineEdit->text().isEmpty();
        ok = ok && this->ui->p1NickLineEdit->text() != this->ui->p2NickLineEdit->text() &&
             this->ui->p2NickLineEdit->text() != this->ui->p3NickLineEdit->text();
    }
    this->ui->startGameButton->setEnabled(ok);
}

void mainWindow::setTeamLabels(const QStringList& l)
{
    this->ui->p1TeamLabel->setVisible(!l[0].isEmpty());
    this->ui->p1TeamText->setVisible(!l[0].isEmpty());
    this->ui->p2TeamLabel->setVisible(!l[1].isEmpty());
    this->ui->p2TeamText->setVisible(!l[1].isEmpty());
    this->ui->p3TeamLabel->setVisible(!l[2].isEmpty());
    this->ui->p3TeamText->setVisible(!l[2].isEmpty());
    this->ui->p4TeamLabel->setVisible(!l[3].isEmpty());
    this->ui->p4TeamText->setVisible(!l[3].isEmpty());
    this->ui->p1TeamText->setText(l[0]);
    this->ui->p2TeamText->setText(l[1]);
    this->ui->p3TeamText->setText(l[2]);
    this->ui->p4TeamText->setText(l[3]);
}

void mainWindow::endGame(bool loadgame, bool toPuzzleScreen, bool relayDisconnected)
{
    if(!relayDisconnected) this->publicServerRemoveGame();
    this->disconnectFromCurrentNetworkGame();
    doNotSendGameEvents = true;
    this->gameInProgress = false;
    this->setReplayMode(false);
    this->networkManager->reset();
    this->gameEvents.clear();
    this->zoomReset();
    this->state->menu->reset();
    this->hostMode = true;
    this->hostUsername.clear();
    this->gameIDToJoin.clear();
    this->inventoryTabStack.clear();
    this->ongoingNetworkGame = false;
    this->drawOrientationOnSidebarDelayTimer->stop();
    for (auto& crd : this->displayCardsCache) crd->deleteCardLater();
    this->displayCardsCache.clear();
    //this->currentlyDisplayedOnSideBarID.clear();
    this->state->clear();
    this->uiDisabledForAnim = false;
    if (!loadgame)
    {
        if (toPuzzleScreen)
        {
            this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->newPuzzlePage), slidingStackedWidget::AnimationDirection::LeftToRight);
        }
        else if (this->backToTutorialFromGame)
        {
            this->backToTutorialFromGame = false;
            this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->tutorialPage), slidingStackedWidget::AnimationDirection::LeftToRight);
        }
        else
        {
            this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->menuPage), slidingStackedWidget::AnimationDirection::LeftToRight);
        }
        this->resetStartGameScreen();
    }
    QApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
}

int mainWindow::detectTutorialPageCount()
{
    QString lang = languageConfigValueToLocaleName(this->getConfigVariable("language"));
    if (lang.isEmpty() || getPrimaryPath("img/" + lang + "/1-1.png", true).isEmpty()) lang = DEFAULT_LANGUAGE;
    QDir d{getPrimaryPath("img/" + lang)};
    auto entries = d.entryList(QStringList{} << "*.png", QDir::Files);
    return entries.size()-1;
}

void mainWindow::resetSettings()
{
    bool firstStart = this->getConfigVariable("notFirstStart").isEmpty();
    bool shownRestartWarning = false;
    this->setConfigVariable("notFirstStart", "1");
    if (this->getConfigVariable("fullScreen").toInt())
    {
        this->setConfigVariable("fullScreen", "0");
        this->setWindowState(Qt::WindowNoState);
    }
    this->setConfigVariable("neverChangePersp", "0");
    this->setConfigVariable("noPrerenderedCards", "0");
    this->ui->disablePerspectiveChangeForNetworkPlayersCheckBox->setEnabled(true);
    this->setConfigVariable("doNotChangeOnlinePersp", "0");
    if (!this->getConfigVariable("gpuAccel").toInt())
    {
#if !defined(Q_OS_HAIKU) && !defined(Q_OS_WASM)
        QString renderer;
        bool glContextCreated = false;
        QOffscreenSurface surf;
        surf.create();
        QOpenGLContext ctx;
        if (ctx.create() && ctx.makeCurrent(&surf))
        {
            QOpenGLFunctions glFuncs(QOpenGLContext::currentContext());
            renderer = QString::fromLatin1(QByteArray((const char*)glFuncs.glGetString(GL_RENDERER)));
            glContextCreated = true;
        }
        if (glContextCreated && !renderer.contains("intel", Qt::CaseInsensitive))
        {
            this->setConfigVariable("gpuAccel", "1");
        }
        else
        {
            this->setConfigVariable("gpuAccel", "0");
        }
#else
        this->setConfigVariable("gpuAccel", "0");
#endif
        if (!firstStart && !shownRestartWarning)
        {
            mainWindow::msgBoxInfo(this, tr("Applying configuration change"), tr("You must restart the application for this change to fully take effect."));
            shownRestartWarning = true;
        }
    }
    if (this->getConfigVariable("hiresMode").toInt())
    {
        this->setConfigVariable("hiresMode", "0");
        if (!firstStart && !shownRestartWarning)
        {
            mainWindow::msgBoxInfo(this, tr("Applying configuration change"), tr("You must restart the application for this change to fully take effect."));
            shownRestartWarning = true;
        }
    }
    if(this->getConfigVariable("disableFontHinting").toInt() != 0 && !shownRestartWarning && !firstStart) {
        mainWindow::msgBoxInfo(this, tr("Applying configuration change"), tr("You must restart the application for this change to fully take effect."));
        shownRestartWarning = true;
    }
    this->setConfigVariable("disableFontHinting", "0");
    QFont appFont = QApplication::font();
    appFont.setHintingPreference(QFont::PreferDefaultHinting);
    QApplication::setFont(appFont);
    if(this->getConfigVariable("overrideDefaultFontSizes").toInt() != 1 && !shownRestartWarning && !firstStart) {
        mainWindow::msgBoxInfo(this, tr("Applying configuration change"), tr("You must restart the application for this change to fully take effect."));
        shownRestartWarning = true;
    }
    this->setConfigVariable("overrideDefaultFontSizes", "1");
    this->setStyleSheet(mainWindow::readStyleSheet());
    this->setConfigVariable("coordGrid", "0");
    if (this->state && this->state->gameBoard) this->state->gameBoard->updateCoordGrid();
    this->setConfigVariable("slidingAnim", "1");
    if (!this->getConfigVariable("enableMusic").toInt())
    {
        this->setConfigVariable("enableMusic", "1");
#ifdef Q_OS_WIN
        this->mPlayer->loadPlayList(getPrimaryPath("music/default_playlist_wma.txt"));
#else
        this->mPlayer->loadPlayList(getPrimaryPath("music/default_playlist.txt"));
#endif
        this->mPlayer->setMaxRandomSilence(0);
        this->mPlayer->start();
        this->mPlayer->shuffle();
        this->mPlayer->setMaxRandomSilence(180000); //3 mins
    }
    this->setConfigVariable("aiDifficulty", "160");
    this->setConfigVariable("allowChatSplitterResize", "0");
    this->ui->bottomWidget->setMaximumHeight(CHAT_PANEL_MAX_HEIGHT);
    this->setConfigVariable("enableSoundEffects", "1");
    if (this->getConfigVariable("language") != QLocale::system().name())
    {
        this->setConfigVariable("language", QLocale::system().name());
        if (!firstStart && !shownRestartWarning)
        {
            mainWindow::msgBoxInfo(this, tr("Applying configuration change"), tr("You must restart the application for the language switch to fully take effect."));
        }
    }
    this->setConfigVariable("useHotSeatContinuePage", "0");
    this->setConfigVariable("orderCardsInHandAlpha", "0");
    this->setConfigVariable("gameAnimations", "1");
    this->setConfigVariable("shrinkInsteadOfBlur", "0");
    this->setConfigVariable("boardRotAnim", "0");
    this->setConfigVariable("turnEndWarning", "1");
    this->setConfigVariable("username", mainWindow::getNewUsername());
    this->refillPlayerTypeComboBoxes();
}

void mainWindow::resetPuzzleScreen()
{
    this->resetStartGameScreen();
    this->hostMode = true;
    this->hostUsername.clear();
    this->ui->puzzleSelectorTreeWidget->header()->resizeSection(0, 300);
    this->ui->puzzleSelectorTreeWidget->header()->resizeSection(1, 100);
}

void mainWindow::publicServerKeepAlive()
{
    this->sendReqToPublicServer("keepalive", QStringList() << this->secretKey);
}

void mainWindow::publicServerRegisterGame()
{
    this->sendReqToPublicServer("addgame", QStringList() << this->ui->publicGameNameLineEdit->text() << this->ui->publicGameDescLineEdit->text() << mainWindow::getConfigVariable("username") <<
                                QString::number(this->ui->publicGameTournamentCheckbox->isChecked()) <<
                                QString::number(this->ui->publicGameOpenForAnyoneCheckbox->isChecked()) <<
                                QString{} << QString{} << QString::number(-1) << this->ui->publicGamePasswordEdit->text());
    this->publicServerKeepAliveTimer->start();
}

int mainWindow::gameModeToIdx(int g)
{
    if (g == 4)
    {
        g = 1;
    }
    else if (g > 0) ++g;
    return g;
}

QString mainWindow::textForBoard(const QString& board)
{
    if (this->boards.isEmpty()) this->loadBoardsDecks();
    if (board.isEmpty())
    {
        return tr("Not selected yet");
    }
    else if (board.startsWith("__rand"))
    {
        return tr("Random board");
    }
    else if (boards.count(board))
    {
        return this->boards[board].readLocalizedValues("name", getLanguageCode()).first();
    }
    return tr("Unknown board");
}

QString mainWindow::textForDeck(const QString& deck)
{
    if (this->boards.isEmpty()) this->loadBoardsDecks();
    if (deck.isEmpty())
    {
        return tr("Not selected yet");
    }
    else if (deck.startsWith("__rand"))
    {
        return tr("Random deck");
    }
    else if (decks.count(deck))
    {
        return this->decks[deck].readLocalizedValues("name", getLanguageCode()).first();
    }
    return tr("Unknown deck");
}

QString mainWindow::getBoardPreviewPixmapPath(const QString& board)
{
    if (this->boards.isEmpty()) this->loadBoardsDecks();
    if (boards.count(board)) return getPrimaryPath("boards/" + boards[board].readValue("board_preview_img") + ".png");
    return getPrimaryPath("img/random.png");
}

void mainWindow::publicServerRemoveGame()
{
    this->publicServerKeepAliveTimer->stop();
    this->sendReqToPublicServer("removegame", QStringList() << this->secretKey);
}

QString mainWindow::selectRandomDeck(const QString& tag)
{
    if (this->randomDeck.size()) return randomDeck;
    QStringList okDecks;
    for (const auto& d : std::as_const(decks))
    {
        if (tag != "")
        {
            if (d.hasKey("tags"))
            {
                auto t = trimStringList(d.readValues("tags"));
                if (t.contains(tag))
                {
                    okDecks.append(d.fileName());
                }
            }
            else if (tag == "untagged")
            {
                okDecks.append(d.fileName());
            }
        }
        else
        {
            okDecks.append(d.fileName());
        }
    }
    if (okDecks.size())
    {
        this->randomDeck = okDecks[rand() % okDecks.size()];
    }
    else
    {
        mainWindow::reportError(tr("Can't generate random deck (not enough decks to choose from)!"));
    }
    return this->randomDeck;
}

QString mainWindow::selectRandomBoard(const QString& tag)
{
    if (this->randomBoard.size()) return randomBoard;
    QStringList okBoards;
    for (const auto& b : std::as_const(boards))
    {
        if (!QCoreApplication::arguments().contains("--hidden-boards") && b.hasKey("hidden") && b.readValue("hidden") == "true") continue;
        if (tag != "")
        {
            if (b.hasKey("tags"))
            {
                auto t = trimStringList(b.readValues("tags"));
                if (t.contains(tag))
                {
                    okBoards.append(b.fileName());
                }
            }
            else if (tag == "untagged")
            {
                okBoards.append(b.fileName());
            }
        }
        else
        {
            okBoards.append(b.fileName());
        }
    }
    if (okBoards.size())
    {
        this->randomBoard = okBoards[rand() % okBoards.size()];
    }
    else
    {
        mainWindow::reportError(tr("Can't generate random board (not enough boards to choose from)!"));
    }
    return this->randomBoard;
}

QString mainWindow::selectBoardForDeck()
{
    if (this->randomBoard.size()) return randomBoard;
    if (this->decks.count(this->randomDeck) && this->decks[this->randomDeck].hasKey("recommended_boards") && this->decks[this->randomDeck].readValues("recommended_boards").size())
    {
        auto okBoards = trimStringList(this->decks[this->randomDeck].readValues("recommended_boards"));
        this->randomBoard = okBoards[rand() % okBoards.size()];
    }
    else
    {
        //mainWindow::reportError(tr("Can't generate random recommended board for deck (not enough boards to choose from), all boards will be used."));
        return this->selectRandomBoard("");
    }
    return this->randomBoard;
}

QString mainWindow::selectDeckForBoard()
{
    if (this->randomDeck.size()) return randomDeck;
    if (this->boards.count(this->randomBoard) && this->boards[this->randomBoard].hasKey("recommended_decks") && this->boards[this->randomBoard].readValues("recommended_decks").size())
    {
        auto okDecks = trimStringList(this->boards[this->randomBoard].readValues("recommended_decks"));
        this->randomDeck = okDecks[rand() % okDecks.size()];
    }
    else
    {
        //mainWindow::reportError(tr("Can't generate random recommended deck for board (not enough decks to choose from), all decks will be used."));
        return this->selectRandomDeck("");
    }
    return this->randomDeck;
}

void mainWindow::clearRandomBoardDeck()
{
    this->randomBoard = "";
    this->randomDeck = "";
}

void mainWindow::updateCurrentPlayerAndScoreLabels()
{
    if (this->state)
    {
        int playerNameWidth = this->ui->sideBarPage->width() * (1 / 3.0);
        setElidedText(this->ui->p1NameLabel, this->playerNicknames[0], playerNameWidth);
        setElidedText(this->ui->p2NameLabel, this->playerNicknames[1], playerNameWidth);
        setElidedText(this->ui->p3NameLabel, this->playerNicknames[2], playerNameWidth);
        setElidedText(this->ui->p4NameLabel, this->playerNicknames[3], playerNameWidth);
        setElidedText(this->ui->currentPlayerLabel,
                      tr("Turn of %1").arg(mainWindow::getPlayerNickNames().at(this->state->getCurrentPlayer() - 1)),
                      this->ui->sideBarPage->width() - 30 - this->ui->inGameBugreportButton->width() - this->ui->zoomInButton->width() - this->ui->zoomOutButton->width() - this->ui->resetZoomButton->width() - this->ui->menuButton->width()
                     );
    }
}

QString mainWindow::serializeLobbyBoardSelection() const
{
    auto its = this->ui->boardsTreeWidget->selectedItems();
    if (its.size())
    {
        QString res = its.front()->data(0, Qt::UserRole).toString() + "," + its.front()->data(1, Qt::UserRole).toString();
        if (its.front()->parent())
        {
            res += "," + its.front()->parent()->data(0, Qt::UserRole).toString() + "," + its.front()->parent()->data(1, Qt::UserRole).toString();
        }
        else
        {
            res += ",,";
        }
        return res;
    }
    return ",,,";
}

QString mainWindow::serializeLobbyDeckSelection() const
{
    auto its = this->ui->decksTreeWidget->selectedItems();
    if (its.size())
    {
        QString res = its.front()->data(0, Qt::UserRole).toString() + "," + its.front()->data(1, Qt::UserRole).toString();
        if (its.front()->parent())
        {
            res += "," + its.front()->parent()->data(0, Qt::UserRole).toString() + "," + its.front()->parent()->data(1, Qt::UserRole).toString();
        }
        else
        {
            res += ",,";
        }
        return res;
    }
    return ",,,";
}

void mainWindow::setSelectedDeckByFilename(const QString& name)
{
    QTreeWidgetItemIterator it(this->ui->decksTreeWidget);
    while (*it)
    {
        if ((*it)->data(0, Qt::UserRole).toString() == name)
        {
            this->setSelectedDeckInLobby(*it);
            break;
        }
        ++it;
    }
}

void mainWindow::setSelectedBoardByFilename(const QString& name)
{
    QTreeWidgetItemIterator it(this->ui->boardsTreeWidget);
    while (*it)
    {
        if ((*it)->data(0, Qt::UserRole).toString() == name)
        {
            this->setSelectedBoardInLobby(*it);
            break;
        }
        ++it;
    }
}

void mainWindow::setLobbyDeckFromEditor(const QString& name)
{
    this->setSelectedDeckByFilename(name);
}

void mainWindow::setLobbyBoardFromEditor(const QString& name)
{
    this->setSelectedBoardByFilename(name);
}

QStringList mainWindow::getOOSCheckData()
{
    return QStringList()
           << "mainWindow:" + QString::number(this->save(true).getHash())
           << "gameState:" + QString::number(this->state->save(true).getHash())
           << "storage:" + QString::number(this->state->storage->save(true).getHash())
           << "p1Inventory:" + QString::number(this->state->p1Inventory->save(true).getHash())
           << "p2Inventory:" + QString::number(this->state->p2Inventory->save(true).getHash())
           << "p3Inventory:" + QString::number(this->state->p3Inventory->save(true).getHash())
           << "p4Inventory:" + QString::number(this->state->p4Inventory->save(true).getHash())
           << "mainInventory:" + QString::number(this->state->mainInventory->save(true).getHash())
           << "huckster:" + QString::number(this->state->huckster->save(true).getHash())
           << "trash:" + QString::number(this->state->trash->save(true).getHash())
           << "board:" + QString::number(this->state->gameBoard->save(true).getHash());
}

void mainWindow::checkOOS(const QStringList& remoteData)
{
    if (remoteData.isEmpty() && this->replayMode) return;
    QString fail;
    auto localData = this->getOOSCheckData();
    for (int i = 0; i < localData.size(); ++i)
    {
        if (i < remoteData.size())
        {
            if (localData[i] != remoteData[i])
            {
                fail += tr("Local: [%1], remote: [%2]\n").arg(localData[i], remoteData[i]);
            }
        }
        else
        {
            fail += tr("Local: [%1], remote: none\n").arg(localData[i]);
        }
    }
    if (!fail.isEmpty())
    {
        mainWindow::reportError(tr("The game is out of sync. It is highly recommended to save now and reopen.\nIf you report this bug, all players should save now and all saves should be submitted with the report.\nDetails:\n%1").arg(fail));
    }
}

void mainWindow::setLobbyBoardSelectionFromSerializedState(const QString& state)
{
    QTreeWidgetItemIterator it(this->ui->boardsTreeWidget);
    QStringList stateList = state.split(",");
    if (state == ",,,")
    {
        this->setSelectedBoardInLobby(nullptr);
        return;
    }
    while (*it)
    {
        if ((stateList[2] != "" || stateList[3] != "") && !(*it)->parent())
        {
            ++it;
            continue;
        }
        if ((*it)->data(0, Qt::UserRole).toString() == stateList[0] && (*it)->data(1, Qt::UserRole).toString() == stateList[1])
        {
            if ((*it)->parent())
            {
                if ((*it)->parent()->data(0, Qt::UserRole).toString() == stateList[2] && (*it)->parent()->data(1, Qt::UserRole).toString() == stateList[3])
                {
                    this->setSelectedBoardInLobby(*it);
                    break;
                }
            }
            else
            {
                this->setSelectedBoardInLobby(*it);
                break;
            }
        }
        ++it;
    }
}

void mainWindow::setLobbyDeckSelectionFromSerializedState(const QString& state)
{
    QTreeWidgetItemIterator it(this->ui->decksTreeWidget);
    QStringList stateList = state.split(",");
    if (state == ",,,")
    {
        this->setSelectedDeckInLobby(nullptr);
        return;
    }
    while (*it)
    {
        if ((stateList[2] != "" || stateList[3] != "") && !(*it)->parent())
        {
            ++it;
            continue;
        }
        if ((*it)->data(0, Qt::UserRole).toString() == stateList[0] && (*it)->data(1, Qt::UserRole).toString() == stateList[1])
        {
            if ((*it)->parent())
            {
                if ((*it)->parent()->data(0, Qt::UserRole).toString() == stateList[2] && (*it)->parent()->data(1, Qt::UserRole).toString() == stateList[3])
                {
                    this->setSelectedDeckInLobby(*it);
                    break;
                }
            }
            else
            {
                this->setSelectedDeckInLobby(*it);
                break;
            }
        }
        ++it;
    }
}

QString mainWindow::getLobbySelectedDeck()
{
    if (this->ui->decksTreeWidget->selectedItems().size())
    {
        if (this->ui->decksTreeWidget->selectedItems().front()->data(0, Qt::UserRole).toString() != "__random_deck_rec")
        {
            if (this->ui->decksTreeWidget->selectedItems().front()->data(0, Qt::UserRole).toString() == "__random_deck")
            {
                return this->selectRandomDeck("");
            }
            else if (this->ui->decksTreeWidget->selectedItems().front()->data(0, Qt::UserRole).toString() == "__random_deck_from_tag")
            {
                return this->selectRandomDeck(this->ui->decksTreeWidget->selectedItems().front()->data(1, Qt::UserRole).toString());
            }
            else
            {
                return this->ui->decksTreeWidget->selectedItems().front()->data(0, Qt::UserRole).toString();
            }
        }
        else
        {
            if (this->ui->boardsTreeWidget->selectedItems().size())
            {
                if (this->ui->boardsTreeWidget->selectedItems().front()->data(0, Qt::UserRole).toString().startsWith("__random"))
                {
                    this->selectRandomBoard(this->ui->boardsTreeWidget->selectedItems().front()->data(1, Qt::UserRole).toString());
                    return this->selectDeckForBoard();
                }
                else
                {
                    this->randomBoard = this->ui->boardsTreeWidget->selectedItems().front()->data(0, Qt::UserRole).toString();
                    return this->selectDeckForBoard();
                }
            }
        }
    }
    return "";
}

QString mainWindow::getLobbySelectedBoard()
{
    if (this->ui->boardsTreeWidget->selectedItems().size())
    {
        if (this->ui->boardsTreeWidget->selectedItems().front()->data(0, Qt::UserRole).toString() != "__random_board_rec")
        {
            if (this->ui->boardsTreeWidget->selectedItems().front()->data(0, Qt::UserRole).toString() == "__random_board")
            {
                return this->selectRandomBoard("");
            }
            else if (this->ui->boardsTreeWidget->selectedItems().front()->data(0, Qt::UserRole).toString() == "__random_board_from_tag")
            {
                return this->selectRandomBoard(this->ui->boardsTreeWidget->selectedItems().front()->data(1, Qt::UserRole).toString());
            }
            else
            {
                return this->ui->boardsTreeWidget->selectedItems().front()->data(0, Qt::UserRole).toString();
            }
        }
        else
        {
            if (this->ui->decksTreeWidget->selectedItems().size())
            {
                if (this->ui->decksTreeWidget->selectedItems().front()->data(0, Qt::UserRole).toString().startsWith("__random"))
                {
                    this->selectRandomDeck(this->ui->decksTreeWidget->selectedItems().front()->data(1, Qt::UserRole).toString());
                    return this->selectBoardForDeck();
                }
                else
                {
                    this->randomDeck = this->ui->decksTreeWidget->selectedItems().front()->data(0, Qt::UserRole).toString();
                    return this->selectBoardForDeck();
                }
            }
        }
    }
    return "";
}

network::networkDataPackage mainWindow::getLobbyState() const
{
    auto comboBoxData = [](QComboBox * box) -> QString
    {
        if (box->currentData().toStringList().size())
        {
            return box->currentData().toStringList().first();
        }
        return {};
    };

    QStringList data;
    data             << QString::number(this->ui->useHucksterCheckBox->isChecked())
                     << this->ui->hucksterCardsComboBox->currentText()
                     << QString::number(this->ui->gameModeComboBox->currentIndex())
                     << this->serializeLobbyBoardSelection()
                     << this->serializeLobbyDeckSelection()
                     << comboBoxData(this->ui->p1TypeComboBox)
                     << this->ui->p1NickLineEdit->text()
                     << comboBoxData(this->ui->p2TypeComboBox)
                     << this->ui->p2NickLineEdit->text()
                     << comboBoxData(this->ui->p3TypeComboBox)
                     << this->ui->p3NickLineEdit->text()
                     << comboBoxData(this->ui->p4TypeComboBox)
                     << this->ui->p4NickLineEdit->text()
                     << this->loadedGameName
                     << QString::number(this->ui->openHandsCheckBox->isChecked())
                     << this->getConfigVariable("username");
    for (int i = 0; i < this->ui->lobbyConnectedUserList->count(); ++i) data << this->ui->lobbyConnectedUserList->item(i)->text();
    network::networkDataPackage m(network::MessageType::LobbyUpdate, data);
    return m;
}

int mainWindow::findDataInComboBox(QComboBox* comboBox, const QString& data, int itemIdx)
{
    for (int i = 0; i < comboBox->count(); ++i)
    {
        auto sl = comboBox->itemData(i).toStringList();
        if (sl.size() > itemIdx && sl[itemIdx] == data) return i;
    }
    return -1;
}

cardInventory* mainWindow::getInventoryFromID(int id) const
{
    switch (id)
    {
        case -3:
            return this->state->storage;
        case -2:
            return this->state->huckster;
        case -1:
            return this->state->trash;
        case 0:
            return this->state->mainInventory;
        case 1:
            return this->state->p1Inventory;
        case 2:
            return this->state->p2Inventory;
        case 3:
            return this->state->p3Inventory;
        case 4:
            return this->state->p4Inventory;
        default:
            return nullptr;
    }
}

void mainWindow::updateScoreTable()
{
    this->sendGameEvent(QStringList() << "updateScoreTable");
    auto scores = state->getScore();
    this->state->scoreCache = scores;
    this->ui->p1ScoreLabel->setText(state->getGameMode() != 1 ?  QString::number(scores[0]) : QString::number(scores[0] + scores[2]) + " (" + QString::number(scores[0]) + ")");
    this->ui->p2ScoreLabel->setText(state->getGameMode() != 1 ?  QString::number(scores[1]) : QString::number(scores[1] + scores[3]) + " (" + QString::number(scores[1]) + ")");
    this->ui->p3ScoreLabel->setText(state->getGameMode() != 1 ?  QString::number(scores[2]) : QString::number(scores[0] + scores[2]) + " (" + QString::number(scores[2]) + ")");
    this->ui->p4ScoreLabel->setText(state->getGameMode() != 1 ?  QString::number(scores[3]) : QString::number(scores[1] + scores[3]) + " (" + QString::number(scores[3]) + ")");
    this->updateEndTurnButtonState();
}

QString mainWindow::getTagTranslation(const QString& tag)
{
    if (getPrimaryPath("translations/" + getLanguageCode() + "/tags.cfg", true).size())
    {
        cfgFile f(getPrimaryPath("translations/" + getLanguageCode() + "/tags.cfg"));
        if (f.hasKey(tag)) return f.readValue(tag);
    }
    return tag;
}

int mainWindow::getTagWeight(const QString& tag)
{
    if (getPrimaryPath("tagweights/tagweights.cfg").size())
    {
        cfgFile f(getPrimaryPath("tagweights/tagweights.cfg"));
        if (f.hasKey(tag)) return f.readValue(tag).toInt();
    }
    return 100;
}

void mainWindow::joinGame()
{
    this->hostMode = false;
    this->resetStartGameScreen();
    network::networkDataPackage msg(network::MessageType::InitialMessage, QStringList()
                                    << LOST_BAZAAR_VERSION
                                    << this->ui->passwordPagePasswordEdit->text()
                                    << this->getConfigVariable("username"));
    this->networkManager->sendMessage(msg);
    this->hostMode = false;
    this->updateLobby();
}

void mainWindow::handleNetworkClientConnected(const QString& uname)
{
    if (this->getConfigVariable("username") == uname)
    {
        if (!this->hostMode)
        {
            this->ongoingNetworkGame = true;
            this->waitingForPublicGameJoin = false;
            this->ui->startNewPublicServerGamebutton->setEnabled(true);
            this->privateGame = false;
            if (this->ui->stackedWidget->currentWidget() != this->ui->gamePage) this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->newGamePage), slidingStackedWidget::AnimationDirection::RightToLeft);
            this->ui->splitter->setSizes(QList<int>() << 4 << 1);
        }
    }
    if (this->hostMode)
    {
        this->ongoingNetworkGame = true;
        this->disableLobbyUpdates = true;
        this->ui->lobbyConnectedUserList->addItem(uname);
        this->ui->p1TypeComboBox->addItem(tr("Network player: %1").arg(uname), QVariant(QList<QString>() << uname));
        this->ui->p2TypeComboBox->addItem(tr("Network player: %1").arg(uname), QVariant(QList<QString>() << uname));
        this->ui->p3TypeComboBox->addItem(tr("Network player: %1").arg(uname), QVariant(QList<QString>() << uname));
        this->ui->p4TypeComboBox->addItem(tr("Network player: %1").arg(uname), QVariant(QList<QString>() << uname));
        this->disableLobbyUpdates = false;
        this->updateObserverList();
        this->networkManager->sendMessage(this->getLobbyState());
        this->networkManager->sendMessageToUser(uname, network::networkDataPackage(network::MessageType::GameEvent, QStringList() << "replayMode" << "1"));
        for (int i = 0; i < this->gameEvents.size(); ++i)
        {
            this->networkManager->sendMessageToUser(uname, this->gameEvents[i]);
        }
        this->networkManager->sendMessageToUser(uname, network::networkDataPackage(network::MessageType::GameEvent, QStringList() << "replayMode" << "0"));
    }
}

void mainWindow::handleGameEvent(const network::networkDataPackage& pkg)
{
    doNotSendGameEvents = true;

#ifdef BAZAAR_DEBUG
    if (this->networkdbg)
    {
        this->ui->chatHistoryTextEdit->append("RECV:" + pkg.toDebugString());
        this->ui->chatHistoryTextEdit->verticalScrollBar()->setValue(this->ui->chatHistoryTextEdit->verticalScrollBar()->maximum());
    }
#endif

    if (!pkg.length())
    {
        mainWindow::reportError(tr("Error: invalid game event received"));
        return;
    }

    if (pkg.getStringAt(0) != "replayMode")
    {
        this->gameEvents.append(pkg);
    }

    if (!this->doNotProcessGameEvents)
    {
        if (this->replayMode)
        {
            this->replayQueue.append(pkg);
        }
        else
        {
            this->processGameEvent(pkg);
        }
    }

    doNotSendGameEvents = false;
}

void mainWindow::handleNetworkLobbyChatMessage(const QString& user, const QString& msg)
{
    this->ui->lobbyChatTextEdit->append(QString("<%1> %2").arg(user, msg));
    this->ui->lobbyChatTextEdit->verticalScrollBar()->setValue(this->ui->lobbyChatTextEdit->verticalScrollBar()->maximum());
}

void mainWindow::handleNetworkLobbyUpdate(const network::networkDataPackage& pkg)
{
    this->updateLobby(pkg);
}

void mainWindow::sendGameEvent(const QStringList& gameEvent, bool force)
{
    if ((!doNotSendGameEvents && !this->state->isNetworkTurn()) || force)
    {
        network::networkDataPackage msg(network::MessageType::GameEvent, gameEvent);
#ifdef BAZAAR_DEBUG
        if (this->networkdbg)
        {
            this->ui->chatHistoryTextEdit->append("SEND:" + msg.toDebugString());
            this->ui->chatHistoryTextEdit->verticalScrollBar()->setValue(this->ui->chatHistoryTextEdit->verticalScrollBar()->maximum());
        }
#endif
        this->networkManager->sendMessage(msg);
        this->gameEvents.append(msg);
    }
}

void mainWindow::updateDisabledCardsInInventories()
{
    if (this->state->storage->isViewable() && this->ui->sideBarTabWidget->currentWidget() == this->state->storage) this->state->storage->updateDisabledCards();
    if (this->state->p1Inventory->isViewable() && this->ui->sideBarTabWidget->currentWidget() == this->state->p1Inventory) this->state->p1Inventory->updateDisabledCards();
    if (this->state->p2Inventory->isViewable() && this->ui->sideBarTabWidget->currentWidget() == this->state->p2Inventory) this->state->p2Inventory->updateDisabledCards();
    if (this->state->p3Inventory->isViewable() && this->ui->sideBarTabWidget->currentWidget() == this->state->p3Inventory) this->state->p3Inventory->updateDisabledCards();
    if (this->state->p4Inventory->isViewable() && this->ui->sideBarTabWidget->currentWidget() == this->state->p4Inventory) this->state->p4Inventory->updateDisabledCards();
    if (this->state->mainInventory->isViewable() && this->ui->sideBarTabWidget->currentWidget() == this->state->mainInventory) this->state->mainInventory->updateDisabledCards();
    if (this->state->huckster->isViewable() && this->ui->sideBarTabWidget->currentWidget() == this->state->huckster) this->state->huckster->updateDisabledCards();
    if (this->state->trash->isViewable() && this->ui->sideBarTabWidget->currentWidget() == this->state->trash) this->state->trash->updateDisabledCards();
}

bool mainWindow::isObserver()
{
    return !(this->findDataInComboBox(this->ui->p1TypeComboBox, this->getConfigVariable("username")) == this->ui->p1TypeComboBox->currentIndex() ||
             this->findDataInComboBox(this->ui->p2TypeComboBox, this->getConfigVariable("username")) == this->ui->p2TypeComboBox->currentIndex() ||
             this->findDataInComboBox(this->ui->p3TypeComboBox, this->getConfigVariable("username")) == this->ui->p3TypeComboBox->currentIndex() ||
             this->findDataInComboBox(this->ui->p4TypeComboBox, this->getConfigVariable("username")) == this->ui->p4TypeComboBox->currentIndex());
}

bool mainWindow::updateNewGameUIForLoadedGame(const QString& path)
{
    auto lob = this->disableLobbyUpdates;
    this->disableLobbyUpdates = true;
    if (!path.isEmpty())
    {
        fileFormat::saveFile f;
        if (!f.loadFromFile(path, false))
        {
            mainWindow::reportError(tr("Error: invalid or unreadable saved game"));
            updateNewGameUIForLoadedGame({});
            return false;
        }
        QFileInfo inf(path);
        this->ui->loadedSaveNameLabel->setText(tr("Loaded game: %1").arg(inf.fileName()));
        this->ui->replayPlayBackCheckBox->setEnabled(hostMode);
        this->ui->replayPlayBackCheckBox->setChecked(false);
        this->loadedGameName = path;
        this->setSelectedDeckByFilename(f.getItemByKey("deck").getValueAsString());
        this->setSelectedBoardByFilename(f.getItemByKey("board").getValueAsString());
        this->ui->decksTreeWidget->setEnabled(false);
        this->ui->boardsTreeWidget->setEnabled(false);
        this->ui->gameModeComboBox->setCurrentIndex(gameModeToIdx(f.getItemByKey("gameMode").getValueAsString().toInt()));
        this->ui->gameModeComboBox->setEnabled(false);
        this->ui->useHucksterCheckBox->setEnabled(false);
        this->ui->useHucksterCheckBox->setChecked(f.getItemByKey("useHuckster").getValueAsString().toInt());
        this->ui->openHandsCheckBox->setEnabled(false);
        this->ui->openHandsCheckBox->setChecked(f.getItemByKey("openHands").getValueAsString().toInt());
        this->ui->hucksterCardsComboBox->setVisible(false);
        this->ui->hucksterCardsLabel->setVisible(false);
        this->ui->loadSaveGameButton->setText(tr("Clear loaded game"));
        this->ui->p1NickLineEdit->setText(f.getItemByKey("playerNicknames").getAttributeAsString("u0"));
        this->ui->p2NickLineEdit->setText(f.getItemByKey("playerNicknames").getAttributeAsString("u1"));
        this->ui->p3NickLineEdit->setText(f.getItemByKey("playerNicknames").getAttributeAsString("u2"));
        this->ui->p4NickLineEdit->setText(f.getItemByKey("playerNicknames").getAttributeAsString("u3"));
        if (f.getItemByKey("aiPlayers").getAttributeAsString("u0").toInt()) this->ui->p1TypeComboBox->setCurrentIndex(1);
        if (f.getItemByKey("aiPlayers").getAttributeAsString("u1").toInt()) this->ui->p2TypeComboBox->setCurrentIndex(1);
        if (f.getItemByKey("aiPlayers").getAttributeAsString("u2").toInt()) this->ui->p3TypeComboBox->setCurrentIndex(1);
        if (f.getItemByKey("aiPlayers").getAttributeAsString("u3").toInt()) this->ui->p4TypeComboBox->setCurrentIndex(1);
    }
    else
    {
        this->ui->loadSaveGameButton->setText(tr("Load a saved game..."));
        this->loadedGameName.clear();
        this->fillBoardsDecksTreeWidgets();
        this->ui->replayPlayBackCheckBox->setEnabled(false);
        this->ui->replayPlayBackCheckBox->setChecked(false);
        this->ui->decksTreeWidget->setEnabled(this->hostMode);
        this->ui->boardsTreeWidget->setEnabled(this->hostMode);
        this->ui->gameModeComboBox->setEnabled(true);
        this->ui->useHucksterCheckBox->setEnabled(true);
        this->ui->useHucksterCheckBox->setChecked(false);
        this->ui->openHandsCheckBox->setEnabled(true);
        this->ui->hucksterCardsComboBox->setVisible(true);
        this->ui->hucksterCardsComboBox->setCurrentIndex(4);
        this->ui->hucksterCardsLabel->setVisible(true);
        this->ui->loadedSaveNameLabel->setText(tr("New game (no saved game loaded)"));
        this->ui->p1NickLineEdit->clear();
        this->ui->p2NickLineEdit->clear();
        this->ui->p3NickLineEdit->clear();
        this->ui->p4NickLineEdit->clear();
    }
    if (this->hostMode)
    {
        this->disableLobbyUpdates = false;
        this->lobbyChangedByUser("other");
        this->disableLobbyUpdates = lob;
    }
    return true;
}

void mainWindow::showPuzzleScreen()
{
    this->resetPuzzleScreen();
    this->ui->stackedWidget->slideInIndex(this->ui->stackedWidget->indexOf(this->ui->newPuzzlePage), slidingStackedWidget::AnimationDirection::RightToLeft);
}

void mainWindow::updateDisplayedLogs()
{
    int slocalUser = this->singleLocalUser();
    this->ui->logListWidget->clear();
    if (this->isObserver())
    {
        for (const auto& logMsg : this->state->logs[-1])
        {
            QListWidgetItem* it = new QListWidgetItem;
            QLabel* lbl = new QLabel(this);
            lbl->setText(logMsg);
            lbl->setTextFormat(Qt::RichText);
            it->setSizeHint(lbl->sizeHint());
            this->ui->logListWidget->addItem(it);
            this->ui->logListWidget->setItemWidget(it, lbl);
        }
    }
    else
    {
        for (auto& p : this->state->logs)
        {
            if ((p.first == this->state->getCurrentPlayer() && this->state && this->state->isLocalUser(this->state->getCurrentPlayer())) || (slocalUser > 0 && p.first == slocalUser))
            {
                for (const auto& logMsg : p.second)
                {
                    QListWidgetItem* it = new QListWidgetItem;
                    QLabel* lbl = new QLabel(this);
                    lbl->setText(logMsg);
                    lbl->setTextFormat(Qt::RichText);
                    it->setSizeHint(lbl->sizeHint());
                    this->ui->logListWidget->addItem(it);
                    this->ui->logListWidget->setItemWidget(it, lbl);
                }
            }
        }
    }
    this->ui->logListWidget->scrollToBottom();
}

rng mainWindow::random{};
QStringList mainWindow::playerNicknames{};
QString mainWindow::startingDeck{};
QString mainWindow::cardPictureSet{};
QString mainWindow::cardNamePictureSet{};
QSettings* mainWindow::settings = nullptr;
mainWindow* mainWindow::instance = nullptr;
bool mainWindow::privateGame = false;
QString mainWindow::secretKey{};
bool mainWindow::replayMode = false;
bool mainWindow::fastForward = false;

void mainWindow::loadBoardsDecks()
{
    this->decks.clear();
    this->boards.clear();
    QFileInfoList decks_ = QDir(DATA_PATH "/cards/decks/").entryInfoList(QStringList() << "*.cfg");
    decks_.append(QDir(userContentPath() + "/cards/decks/").entryInfoList(QStringList() << "*.cfg"));
    QFileInfoList boards_ = QDir(DATA_PATH "/boards/").entryInfoList(QStringList() << "*.cfg");
    boards_.append(QDir(userContentPath() + "/boards/").entryInfoList(QStringList() << "*.cfg"));
    for (auto& deck : decks_)
    {
        cfgFile cfg(deck.absoluteFilePath());
        this->decks[cfg.fileName()] = cfg;
    }
    for (auto& board : boards_)
    {
        cfgFile cfg(board.absoluteFilePath());
        this->boards[cfg.fileName()] = cfg;
    }
    this->fillBoardsDecksTreeWidgets();
}

void mainWindow::replayNextGameEvent()
{
    while (true)
    {
        while (this->uiDisabledForAnim) QCoreApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
        if (this->replayQueue.isEmpty())
        {
            this->setReplayMode(false);
            return;
        }
        else
        {
            auto pkg = this->replayQueue.front();
            this->replayQueue.pop_front();
            if (this->processGameEvent(pkg)) return;
        }
    }
}

void mainWindow::fillBoardsDecksTreeWidgets()
{
    QMap<QString, QStringList> deckTags;
    QMap<QString, QStringList> boardTags;
    QStringList foundBoards, foundDecks;
    for (const auto& board : std::as_const(this->boards))
    {
        if (!QCoreApplication::arguments().contains("--hidden-boards") && board.hasKey("hidden") && board.readValue("hidden") == "true") continue;
        if (board.hasKey("tags"))
        {
            for (const auto& tag : board.readValues("tags"))
            {
                boardTags[tag.trimmed()].append(board.fileName());
                foundBoards.append(board.fileName());
            }
        }
    }
    for (const auto& deck : std::as_const(this->decks))
    {
        if (deck.hasKey("tags"))
        {
            for (const auto& tag : deck.readValues("tags"))
            {
                deckTags[tag.trimmed()].append(deck.fileName());
                foundDecks.append(deck.fileName());
            }
        }
    }
    for (const auto& deck : std::as_const(this->decks))
    {
        if (!foundDecks.contains(deck.fileName()))
        {
            deckTags["untagged"].append(deck.fileName());
        }
    }
    for (const auto& board : std::as_const(this->boards))
    {
        if (!QCoreApplication::arguments().contains("--hidden-boards") && board.hasKey("hidden") && board.readValue("hidden") == "true") continue;
        if (!foundBoards.contains(board.fileName()))
        {
            boardTags["untagged"].append(board.fileName());
        }
    }
    this->clearBoardDeckSelectorSidebar();
    this->ui->boardsTreeWidget->verticalScrollBar()->setValue(0);
    this->ui->decksTreeWidget->verticalScrollBar()->setValue(0);
    this->ui->decksTreeWidget->clearSelection();
    this->ui->boardsTreeWidget->clearSelection();
    this->ui->decksTreeWidget->clear();
    this->ui->boardsTreeWidget->clear();
    QTreeWidgetItem* random_board_it = new QTreeWidgetItem(QStringList() << tr("<Random>"));
    random_board_it->setData(0, Qt::UserRole, "__random_board");
    QTreeWidgetItem* random_board_rec_it = new QTreeWidgetItem(QStringList() << tr("<Random> (compatible with deck selection)"));
    random_board_rec_it->setData(0, Qt::UserRole, "__random_board_rec");
    QTreeWidgetItem* random_deck_it = new QTreeWidgetItem(QStringList() << tr("<Random>"));
    random_deck_it->setData(0, Qt::UserRole, "__random_deck");
    QTreeWidgetItem* random_deck_rec_it = new QTreeWidgetItem(QStringList() << tr("<Random> (compatible with board selection)"));
    random_deck_rec_it->setData(0, Qt::UserRole, "__random_deck_rec");
    this->ui->decksTreeWidget->addTopLevelItem(random_deck_it);
    this->ui->decksTreeWidget->addTopLevelItem(random_deck_rec_it);
    this->ui->boardsTreeWidget->addTopLevelItem(random_board_it);
    this->ui->boardsTreeWidget->addTopLevelItem(random_board_rec_it);

    auto boardKeyList = boardTags.keys();
    std::sort(boardKeyList.begin(), boardKeyList.end(), [this](const QString & a, const QString & b)
    {
        auto aW = getTagWeight(a);
        auto bW = getTagWeight(b);
        if (aW != bW) return aW < bW;
        auto aT = getTagTranslation(a);
        auto bT = getTagTranslation(b);
        return aT.localeAwareCompare(bT) < 0;
    });
    for (const auto& tag : boardKeyList)
    {
        QString tag_upper = getTagTranslation(tag);
        if (tag_upper.size()) tag_upper[0] = tag_upper[0].toTitleCase();
        QTreeWidgetItem* tag_it = new QTreeWidgetItem(QStringList() << tag_upper);
        tag_it->setFlags(Qt::ItemIsEnabled); //Do not make it selectable
        tag_it->setData(0, Qt::UserRole, "__board_tag");
        tag_it->setData(1, Qt::UserRole, tag);
        if (boardTags[tag].size() > 1)
        {
            QTreeWidgetItem* random_it = new QTreeWidgetItem(QStringList() << tr("<Random>"));
            random_it->setData(0, Qt::UserRole, "__random_board_from_tag");
            random_it->setData(1, Qt::UserRole, tag);
            tag_it->addChild(random_it);
        }
        for (const auto& board : boardTags[tag])
        {
            QTreeWidgetItem* board_it = new QTreeWidgetItem(QStringList() << this->boards[board].readLocalizedValues("name", getLanguageCode()).first());
            board_it->setData(0, Qt::UserRole, board);
            tag_it->addChild(board_it);
        }
        this->ui->boardsTreeWidget->addTopLevelItem(tag_it);
        tag_it->setExpanded(false);
    }
    auto deckKeyList = deckTags.keys();
    std::sort(deckKeyList.begin(), deckKeyList.end(), [this](const QString & a, const QString & b)
    {
        auto aW = getTagWeight(a);
        auto bW = getTagWeight(b);
        if (aW != bW) return aW < bW;
        auto aT = getTagTranslation(a);
        auto bT = getTagTranslation(b);
        return aT.localeAwareCompare(bT) < 0;
    });
    for (const auto& tag : deckKeyList)
    {
        QString tag_upper = getTagTranslation(tag);
        if (tag_upper.size()) tag_upper[0] = tag_upper[0].toTitleCase();
        QTreeWidgetItem* tag_it = new QTreeWidgetItem(QStringList() << tag_upper);
        tag_it->setFlags(Qt::ItemIsEnabled); //Do not make it selectable
        tag_it->setData(0, Qt::UserRole, "__deck_tag");
        tag_it->setData(1, Qt::UserRole, tag);
        if (deckTags[tag].size() > 1)
        {
            QTreeWidgetItem* random_it = new QTreeWidgetItem(QStringList() << tr("<Random>"));
            random_it->setData(0, Qt::UserRole, "__random_deck_from_tag");
            random_it->setData(1, Qt::UserRole, tag);
            tag_it->addChild(random_it);
        }
        for (const auto& deck : deckTags[tag])
        {
            QTreeWidgetItem* deck_it = new QTreeWidgetItem(QStringList() << this->decks[deck].readLocalizedValues("name", getLanguageCode()).first());
            deck_it->setData(0, Qt::UserRole, deck);
            tag_it->addChild(deck_it);
        }
        this->ui->decksTreeWidget->addTopLevelItem(tag_it);
        tag_it->setExpanded(false);
    }
    this->ui->decksTreeWidget->clearSelection();
    this->ui->boardsTreeWidget->clearSelection();
    this->filter(this->ui->boardsFilter->text(), this->ui->boardsTreeWidget);
    this->filter(this->ui->decksFilter->text(), this->ui->decksTreeWidget);
}

bool mainWindow::isBoardDeckValid(const QString& board, const QString& deck)
{
    if (!this->boards.size()) this->loadBoardsDecks();
    return (board.isEmpty() || board.startsWith("__rand") || this->boards.count(board)) && (deck.isEmpty() || deck.startsWith("__rand") || this->decks.count(deck));
}

QString mainWindow::textForGameMode(int gameMode) const
{
    if (gameMode < 0) return tr("No game mode selected yet");
    return this->ui->gameModeComboBox->itemText(gameMode);
}
