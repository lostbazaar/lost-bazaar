/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <QGraphicsOpacityEffect>
#include <QStyleOption>
#include <QPainter>
#include "fadinglabel.h"
#include "mainwindow.h"
#include "globals.h"
#include "audio.h"

fadingLabel::fadingLabel(QWidget* parent)
{
    this->setParent(parent);
    this->timer = new QTimer;
    this->timer->setTimerType(Qt::PreciseTimer);
    this->currentState = fadingLabel::LabelTimerState::FadeIn;
    this->t = 0;
    this->d = false;
    this->doneEmitted = false;
    connect(this->timer, &QTimer::timeout, this, &fadingLabel::timerStep);
}

void fadingLabel::start()
{
    QGraphicsOpacityEffect* effect = new QGraphicsOpacityEffect;
    effect->setOpacity(0.0);
    this->setGraphicsEffect(effect);
    if (!this->fSeq.isEmpty())
    {
        this->fadeDuration = this->timerSeq.first();
        if (this->fSeq.first().endsWith(".png"))
        {
            this->setPixmap(QPixmap(getPrimaryPath(this->fSeq.first())));
        }
        else
        {
            this->setText(this->fSeq.first());
        }
        this->timer->setInterval(25);
        this->elapsedTime.start();
        this->timer->start();
    }
}

void fadingLabel::setMainWindow(mainWindow* mw)
{
    this->mw = mw;
}

void fadingLabel::setData(const QStringList& textSeq, const QList<int>& seq, QPixmap* px)
{
    this->finalPixmap = px;
    this->timerSeq = seq;
    this->fSeq = textSeq;
}

void fadingLabel::timerFinished()
{
    if (this->currentState == LabelTimerState::FadeIn)
    {
        if (this->d)
        {
            if (!this->doneEmitted)
            {
                emit this->done();
                this->doneEmitted = true;
            }
            return;
        }
        this->currentState = LabelTimerState::Visible;
    }
    else if (this->currentState == LabelTimerState::Visible)
    {
        this->currentState = LabelTimerState::FadeOut;
    }
    else if (this->currentState == LabelTimerState::FadeOut)
    {
        this->fSeq.removeFirst();
        if (this->fSeq.isEmpty())
        {
            if (this->d)
            {
                if (!this->doneEmitted)
                {
                    emit this->done();
                    this->doneEmitted = true;
                }
                return;
            }
            else
            {
                this->d = true;
                this->setPixmap(*this->finalPixmap);
            }
        }
        else
        {
            if (this->fSeq.first().endsWith(".png"))
            {
                this->setPixmap(QPixmap(getPrimaryPath(this->fSeq.first())));
            }
            else
            {
                this->setText(this->fSeq.first());
            }
        }
        this->currentState = LabelTimerState::FadeIn;
    }
    if (this->timerSeq.size()) this->timerSeq.removeFirst();
    if (this->timerSeq.size()) this->fadeDuration = this->timerSeq.first();
    this->t = 0;
    this->timer->setInterval(50);
    this->elapsedTime.start();
    this->timer->start();
}

void fadingLabel::timerStep()
{
    QCoreApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
    this->t += this->elapsedTime.elapsed();
    if (this->currentState == LabelTimerState::FadeIn)
    {
        static_cast<QGraphicsOpacityEffect*>(this->graphicsEffect())->setOpacity(this->t / qreal(this->fadeDuration));
    }
    else if (this->currentState == LabelTimerState::FadeOut)
    {
        static_cast<QGraphicsOpacityEffect*>(this->graphicsEffect())->setOpacity(1.0 - this->t / qreal(this->fadeDuration));
    }
    this->elapsedTime.restart();
    if (t >= this->fadeDuration)
    {
        this->timer->stop();
        this->timerFinished();
    }
}

void fadingLabel::mousePressEvent(QMouseEvent* ev)
{
    Q_UNUSED(ev);

    if (!this->doneEmitted)
    {
        emit this->done();
        this->doneEmitted = true;
    }
}
