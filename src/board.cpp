/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <QFile>
#include <QTextStream>
#include <QGraphicsPixmapItem>
#include <QPoint>
#include <QMouseEvent>
#include <QDrag>
#include <QMimeData>
#include <QOpenGLWidget>
#include "globals.h"
#include "board.h"
#include "card.h"
#include "cardinventory.h"
#include <QTimer>

QMap<int, QMap<int, QPoint>> board::tilePosMap = QMap<int, QMap<int, QPoint>>();
QMap<int, QMap<int, QSize>> board::tileSizeMap = QMap<int, QMap<int, QSize>>();
QString board::playerTileFilenamePrefix = QString{};
QString board::tileOverlayFilenamePrefix = QString{};

board::board(QWidget* parent) : QGraphicsView(parent)
{
    this->waitingForTileChoice = false;
    this->choice = QPoint(-1, -1);
    this->prevPlayerNum = 1;
    this->setAcceptDrops(true);
    connect(this, &board::rotationFinished, [&]()
    {
        if (this->gS && this->gS->mW) this->gS->mW->setUIDisabledForAnimation(false);
        if (this->gS) this->gS->runEvent(QStringList() << "rotationFinished" << QString::number(this->gS->getCurrentPlayer()));
    });
    if (parent)
    {
        this->setMouseTracking(true);
        this->setScene(new QGraphicsScene);
        if (mainWindow::getConfigVariable("gpuAccel").toInt())
        {
            this->setViewport(new QOpenGLWidget());
        }
        this->rotationTimer = new QTimer;
        this->rotationTimer->setTimerType(Qt::PreciseTimer);
        connect(this->rotationTimer, &QTimer::timeout, this, &board::rotationStep);
        connect(this, &board::rotationFinished, [this]()
        {
            if (!this->waitingForTileChoice)
                this->setDragMode(QGraphicsView::ScrollHandDrag);
        });
    }
}

void board::rotationStep()
{
    if (this->currRotStep < 0)
    {
        QTransform t = this->transform();
        t.rotate(static_cast<double>(rotDegree) / this->rotSteps);
        this->setTransform(t);
        ++this->currRotStep;
        /*if (this->rotationTimer->remainingTime() == 0)
        {
            QTransform t = this->transform();
            t.rotate(static_cast<double>(rotDegree) / this->rotSteps);
            this->setTransform(t);
            ++this->currRotStep;
        }*/
    }
    else if (this->currRotStep > 0)
    {
        QTransform t = this->transform();
        t.rotate(static_cast<double>(rotDegree) / this->rotSteps);
        this->setTransform(t);
        --this->currRotStep;
        /*if (this->rotationTimer->remainingTime() == 0)
        {
            QTransform t = this->transform();
            t.rotate(static_cast<double>(rotDegree) / this->rotSteps);
            this->setTransform(t);
            --this->currRotStep;
        }*/
    }
    else
    {
        this->rotationTimer->stop();
        if (this->gS && this->gS->isAITurn())
        {
            QTimer::singleShot(1000, Qt::PreciseTimer, this, SIGNAL(rotationFinished()));
        }
        else
        {
            emit this->rotationFinished();
        }
        this->disableInteractionForRotation = false;
    }
}

void board::handleRightClickAction(QObject* sender)
{
    if (this->gS->mW && !this->disableInteractionForRotation)
    {
        if (!(this->gS->mW->isPassiveTurn() || this->gS->mW->uiDisabledCounter > 0) && !this->gS->silverActionsDisabledForTurn)
        {
            card* senderCard = static_cast<card*>(sender);
            if (this->gS->getCurrentPlayer() == senderCard->getPlayer() && (senderCard->isFullSize() || (!senderCard->isFullSize() && senderCard->getIcons().count("tptu"))) && !senderCard->isGrayScale())
            {
                this->gS->mW->setUIDisabledForUserChoice(true);
                if (mainWindow::getConfigVariable("gameAnimations").toInt() && this->gS && this->gS->mW && this->gS->mW->isGamePageVisible())
                {
                    connect(senderCard, &card::silverActionAnimationFinished, this, [senderCard, this]
                    {
                        disconnect(senderCard, &card::silverActionAnimationFinished, nullptr, nullptr);
                        this->gS->runEvent(QStringList() << "cardSilverActionActivated" << QString::number(senderCard->getUID()), senderCard);
                    });
                    senderCard->animateSilverAction();
                }
                else
                {
                    this->gS->runEvent(QStringList() << "cardSilverActionActivated" << QString::number(senderCard->getUID()), senderCard);
                }
                this->gS->mW->setUIDisabledForUserChoice(false);
            }
        }
    }
}

card* board::removeCard(int uID, bool addToTrash)
{
    if (this->gS) this->gS->sendGameEvent(QStringList() << "removeCardFromBoard" << QString::number(uID));
    for (auto key : this->cards.keys())
    {
        for (auto innerKey : this->cards[key].keys())
        {
            if (this->cards[key][innerKey] != nullptr && this->cards[key][innerKey]->getUID() == uID)
            {
                card* crd = this->cards[key][innerKey];
                if (!mainWindow::getConfigVariable("gameAnimations").toInt() || !this->gS || !this->gS->mW || !this->gS->mW->isGamePageVisible())
                {
                    crd->removeFromScene(this->scene());
                    if (addToTrash && this->gS) gS->trash->addCard(crd);
                }
                else
                {
                    if (addToTrash)
                    {
                        connect(crd, &card::boardRemovalAnimFinished, this, [this, crd]
                        {
                            disconnect(crd, &card::boardRemovalAnimFinished, nullptr, nullptr);
                            gS->trash->addCard(crd);
                        });
                    }
                    crd->animateRemovalFromBoard(this->scene());
                }
                this->cards[key][innerKey] = nullptr;
                disconnect(crd, &card::mouseEntered, this, &board::cardMouseEntered);
                disconnect(crd, &card::clicked, this, &board::cardClicked);
                disconnect(crd, &card::mouseHeldDown, this, &board::cardMouseHeldDown);
                disconnect(crd, &card::rightMouseClicked, this, &board::cardRightClicked);
                disconnect(crd, &card::mouseLeft, this, &board::cardMouseLeft);
                return crd;
            }
        }
    }
    return nullptr;
}

void board::drawBackground(const QString& fName, const QString& bkgName, const QString& tileMapName, const QString& graphicsViewBackgroundName, const QString& whiteName, const QString& yellowName, const QString& blueName, const QString& greenName, const QString& disableName, const QString& orangeName, const QString& magentaName, const QString& pTileFnamePrefix, const QString& tileOverlayFnamePrefix, const QString& coords)
{
    if (gS && !gS->mW) return;
    playerTileFilenamePrefix = pTileFnamePrefix;
    tileOverlayFilenamePrefix = tileOverlayFnamePrefix;
    this->tileMapName = tileMapName;
    this->fName = fName;
    this->bkgName = bkgName;
    this->gviewBkgName = graphicsViewBackgroundName;
    this->coordsName = coords;
    QGraphicsScene* oldScene = this->scene();
    this->setScene(new QGraphicsScene);
    if (oldScene) oldScene->deleteLater();
    this->setBackgroundBrush(QPixmap(getPrimaryPath("boards/" + graphicsViewBackgroundName + ".png")));
    QPixmap pxp(getPrimaryPathScaled("boards/" + bkgName + ".png", -1));
    QPixmap pxp_orig;
    if(mainWindow::getConfigVariable("hiresMode").toInt() && !getPrimaryPath("boards/" + bkgName + "_hires.png").isEmpty())
    {
        pxp_orig = QPixmap{getPrimaryPath("boards/" + bkgName + "_hires.png")};
    } else
    {
        pxp_orig = QPixmap{getPrimaryPath("boards/" + bkgName + ".png")};
    }
    auto px = scene()->addPixmap(pxp);
    px->setPos(-pxp.width() / 2, -pxp.height() / 2);
    QPixmap transp_bkg(QSize(pxp.width() + 128, pxp.height() + 128));
    transp_bkg.fill(Qt::transparent);
    auto tpkg = scene()->addPixmap(transp_bkg);
    tpkg->setPos(-transp_bkg.width() / 2, -transp_bkg.height() / 2);
    this->updateCoordGrid();
    double pos_scale_factor = 1.0;
    QFile mapFile;
    if(mainWindow::getConfigVariable("hiresMode").toInt())
    {
        if(!getPrimaryPath("boards/" + tileMapName + "_hires.txt").isEmpty())
        {
            mapFile.setFileName(getPrimaryPath("boards/" + tileMapName + "_hires.txt"));
        } else
        {
            mapFile.setFileName(getPrimaryPath("boards/" + tileMapName + ".txt"));
            pos_scale_factor = 1.9625;
        }
    } else
    {
        mapFile.setFileName((getPrimaryPath("boards/" + tileMapName + ".txt")));
    }

    if (mapFile.open(QFile::ReadOnly))
    {
        QTextStream in(&mapFile);
        //in.setCodec("UTF-8");
        int j, i = -1;
        while (!in.atEnd())
        {
            QString line = in.readLine().replace(QChar(' '), QString(""));
            line = line.replace(QChar('\t'), QString(""));
            QList<QStringList> lineTiles;
            for (auto& tileStr : line.split(",", Qt::SkipEmptyParts))
            {
                lineTiles.append(tileStr.split(":", Qt::SkipEmptyParts));
            }
            for (j = 0, i++; j < lineTiles.size(); j++)
            {
                if (lineTiles[j][4].isEmpty())
                {
                    mainWindow::reportError(tr("Error: invalid board tile map (%1)").arg(tileMapName));
                }
                else
                {
                    auto tileType = lineTiles[j][4][0];
                    auto tileName = lineTiles[j][4].mid(1);
                    if (tileType == 'W')
                    {
                        if (tileName.isEmpty())
                        {
                            tileName = whiteName;
                        }
                        else
                        {
                            tileName = whiteName + "-" + tileName;
                        }
                        QGraphicsPixmapItem* pItem = scene()->addPixmap(getPrimaryPathScaled("boards/" + tileName + ".png", 157));
                        pItem->setPos(pos_scale_factor * (lineTiles[j][0].toInt() - pxp_orig.width() / 2), pos_scale_factor * (lineTiles[j][1].toInt() - pxp_orig.height() / 2));
                    }
                    else if (tileType == 'Y')
                    {
                        if (tileName.isEmpty())
                        {
                            tileName = yellowName;
                        }
                        else
                        {
                            tileName = yellowName + "-" + tileName;
                        }
                        QGraphicsPixmapItem* pItem = scene()->addPixmap(getPrimaryPathScaled("boards/" + tileName + ".png", 157));
                        pItem->setPos(pos_scale_factor * (lineTiles[j][0].toInt() - pxp_orig.width() / 2), pos_scale_factor * (lineTiles[j][1].toInt() - pxp_orig.height() / 2));
                    }
                    else if (tileType == 'O')
                    {
                        if (tileName.isEmpty())
                        {
                            tileName = orangeName;
                        }
                        else
                        {
                            tileName = orangeName + "-" + tileName;
                        }
                        QGraphicsPixmapItem* pItem = scene()->addPixmap(getPrimaryPathScaled("boards/" + tileName + ".png", 157));
                        pItem->setPos(pos_scale_factor * (lineTiles[j][0].toInt() - pxp_orig.width() / 2), pos_scale_factor * (lineTiles[j][1].toInt() - pxp_orig.height() / 2));
                    }
                    else if (tileType == 'M')
                    {
                        if (tileName.isEmpty())
                        {
                            tileName = magentaName;
                        }
                        else
                        {
                            tileName = magentaName + "-" + tileName;
                        }
                        QGraphicsPixmapItem* pItem = scene()->addPixmap(getPrimaryPathScaled("boards/" + tileName + ".png", 157));
                        pItem->setPos(pos_scale_factor * (lineTiles[j][0].toInt() - pxp_orig.width() / 2), pos_scale_factor * (lineTiles[j][1].toInt() - pxp_orig.height() / 2));
                    }
                    else if (tileType == 'B')
                    {
                        if (tileName.isEmpty())
                        {
                            tileName = blueName;
                        }
                        else
                        {
                            tileName = blueName + "-" + tileName;
                        }
                        QGraphicsPixmapItem* pItem = scene()->addPixmap(getPrimaryPathScaled("boards/" + tileName + ".png", 157));
                        pItem->setPos(pos_scale_factor * (lineTiles[j][0].toInt() - pxp_orig.width() / 2), pos_scale_factor * (lineTiles[j][1].toInt() - pxp_orig.height() / 2));
                    }
                    else if (tileType == 'D')
                    {
                        if (tileName.isEmpty())
                        {
                            tileName = disableName;
                        }
                        else
                        {
                            tileName = disableName + "-" + tileName;
                        }
                        QGraphicsPixmapItem* pItem = scene()->addPixmap(getPrimaryPathScaled("boards/" + tileName + ".png", 157));
                        pItem->setPos(pos_scale_factor * (lineTiles[j][0].toInt() - pxp_orig.width() / 2), pos_scale_factor * (lineTiles[j][1].toInt() - pxp_orig.height() / 2));
                    }
                    else if (tileType == 'G')
                    {
                        if (tileName.isEmpty())
                        {
                            tileName = greenName;
                        }
                        else
                        {
                            tileName = greenName + "-" + tileName;
                        }
                        QGraphicsPixmapItem* pItem = scene()->addPixmap(getPrimaryPathScaled("boards/" + tileName + ".png", 157));
                        pItem->setPos(pos_scale_factor * (lineTiles[j][0].toInt() - pxp_orig.width() / 2), pos_scale_factor * (lineTiles[j][1].toInt() - pxp_orig.height() / 2));
                    }
                    this->tilePosMap[j][i] = pos_scale_factor * QPoint(lineTiles[j][0].toInt() - pxp_orig.width() / 2, lineTiles[j][1].toInt() - pxp_orig.height() / 2);
                    this->tileSizeMap[j][i] = QSize(pos_scale_factor * lineTiles[j][2].toInt(), pos_scale_factor * lineTiles[j][3].toInt());
                    this->tiles[j][i] = lineTiles[j][4][0];
                    this->cards[j][i] = nullptr;
                    this->w = j;
                }
            }
            this->h = i;
        }
        mapFile.close();
    }
    else
    {
        mainWindow::reportError(tr("Error: cannot open board data file"));
    }
}

void board::clear()
{
    for (const auto& m : std::as_const(this->cards))
    {
        for (const auto& m2 : m)
        {
            if (m2 != nullptr)
            {
                m2->removeFromScene(this->scene());
                m2->deleteCardLater();
            }
        }
    }
    this->tilePosMap.clear();
    this->tileSizeMap.clear();
    this->scene()->clear();
    this->cards.clear();
    this->tiles.clear();
    this->coordsPxItem = nullptr;
    this->waitingForTileChoice = false;
    this->cardPlacementGraphics.clear();
    this->playerTileChoiceGraphics.clear();
    this->choice = QPoint(-1, -1);
    this->prevPlayerNum = 1;
    this->currRotStep = 0;
    this->rotSteps = 0;
    this->rotDegree = 0;
    this->setTransform(QTransform());
}

CardList board::getAllCards() const
{
    CardList result;
    for(auto* card: getAllCardsInternal()) result.push_back(card);
    return result;
}

QList<card*> board::getAllCardsInternal() const
{
    QList<card*> res;
    for (const auto& m : this->cards)
    {
        for (const auto& m2 : m)
        {
            if (m2 != nullptr) res.append(m2);
        }
    }
    return res;
}

CardList board::getPlayerCards(int player)
{
    CardList result;
    for(auto* card: getPlayerCardsInternal(player)) result.push_back(card);
    return result;
}

QList<card*> board::getPlayerCardsInternal(int player)
{
    QList<card*> res;
    for (const auto& m : std::as_const(this->cards))
    {
        for (const auto& m2 : m)
        {
            if (m2 != nullptr && m2->getPlayer() == player) res.append(m2);
        }
    }
    return res;
}

bool board::isInnovativeTile(const QPoint& p) const
{
    return this->tiles.count(p.x()) && this->tiles[p.x()].count(p.y()) && this->tiles[p.x()][p.y()] == QChar('G');
}

bool board::isInnovativeTile(const QPointF &p) const
{
    return isInnovativeTile(p.toPoint());
}

bool board::isSafeTile(const QPoint& p) const
{
    return this->tiles.count(p.x()) && this->tiles[p.x()].count(p.y()) && this->tiles[p.x()][p.y()] == QChar('B');
}

bool board::isSafeTile(const QPointF &p) const
{
    return isSafeTile(p.toPoint());
}

bool board::isDisabledTile(const QPoint& p) const
{
    return this->tiles.count(p.x()) && this->tiles[p.x()].count(p.y()) && this->tiles[p.x()][p.y()] == QChar('D');
}

bool board::isDisabledTile(const QPointF &p) const
{
    return isDisabledTile(p.toPoint());
}

bool board::isDoubleTile(const QPoint& p) const
{
    return this->tiles.count(p.x()) && this->tiles[p.x()].count(p.y()) && this->tiles[p.x()][p.y()] == QChar('Y');
}

bool board::isDoubleTile(const QPointF &p) const
{
    return isDoubleTile(p.toPoint());
}

bool board::isFashionTile(const QPoint& p) const
{
    return this->tiles.count(p.x()) && this->tiles[p.x()].count(p.y()) && this->tiles[p.x()][p.y()] == QChar('M');
}

bool board::isFashionTile(const QPointF &p) const
{
    return isFashionTile(p.toPoint());
}

bool board::isPowerTile(const QPoint& p) const
{
    return this->tiles.count(p.x()) && this->tiles[p.x()].count(p.y()) && this->tiles[p.x()][p.y()] == QChar('O');
}

bool board::isPowerTile(const QPointF &p) const
{
    return isPowerTile(p.toPoint());
}

QList<QPoint> board::getEmptyTiles() const
{
    QList<QPoint> res;
    for (int i = 0; i < this->cards.count(); i++)
    {
        for (int j = 0; j < this->cards[i].count(); j++)
        {
            if (this->cards[i].value(j) == nullptr && this->tiles[i].value(j) != QChar('D'))
            {
                res.append(QPoint(i, j));
            }
        }
    }
    return res;
}

QList<int> board::getEmptyTilesAsIntList() const
{
    QList<int> res;
    for (int i = 0; i < this->cards.count(); i++)
    {
        for (int j = 0; j < this->cards[i].count(); j++)
        {
            if (this->cards[i].value(j) == nullptr && this->tiles[i].value(j) != QChar('D'))
            {
                res.append(i);
                res.append(j);
            }
        }
    }
    return res;
}

CardList board::getCardsWithName(const QString& name) const
{
    QList<QObject*> res;
    for (const auto& m : this->cards)
    {
        for (const auto& m2 : m)
        {
            if (m2 != nullptr && m2->getID() == name) res.append(m2);
        }
    }
    return res;
}

void board::setGameState(gameState* gS)
{
    this->gS = gS;
}

card* board::getCardAtPosition(const QPoint& pos)
{
    if (!this->cards.contains(pos.x())) return nullptr;
    if (!this->cards[pos.x()].contains(pos.y())) return nullptr;
    return this->cards[pos.x()][pos.y()];
}

card *board::getCardAtPosition(const QPointF &pos)
{
    return getCardAtPosition(pos.toPoint());
}

void board::moveCard(const QPoint& from, const QPoint& to)
{
    if (this->gS) this->gS->sendGameEvent(QStringList() << "moveCard"
                                              << QString::number(from.x())
                                              << QString::number(from.y())
                                              << QString::number(to.x())
                                              << QString::number(to.y()));
    if (this->cards[from.x()][from.y()])
    {
        if (this->cards[to.x()][to.y()])
        {
            mainWindow::reportError(tr("Error: there is already a card at the target position (in board::moveCard)"));
        }
        if (!mainWindow::getConfigVariable("gameAnimations").toInt() || !this->gS || !this->gS->mW || !this->gS->mW->isGamePageVisible())
        {
            this->cards[from.x()][from.y()]->setPosition(to);
            this->cards[to.x()][to.y()] = this->cards[from.x()][from.y()];
            this->cards[from.x()][from.y()] = nullptr;
            this->cards[to.x()][to.y()]->draw(this->scene(), DrawingType::InGame, QPointF(to));
        }
        else
        {
            this->cards[from.x()][from.y()]->moveOnBoardTo(to);
            this->cards[to.x()][to.y()] = this->cards[from.x()][from.y()];
            this->cards[from.x()][from.y()] = nullptr;
        }
    }
#ifdef BAZAAR_DEBUG
    else
    {
        mainWindow::reportError(tr("Script error: invalid 'from' argument in moveCard, this is probably a bug in the following script:\n%1").arg(gameState::scriptExecInfo.join("\n")));
    }
#endif
}

void board::moveCard(const QPointF &from, const QPointF &to)
{
    moveCard(from.toPoint(), to.toPoint());
}

void board::swapCards(const QPoint& p1, const QPoint& p2)
{
    if (this->gS) this->gS->sendGameEvent(QStringList() << "swapCards"
                                              << QString::number(p1.x())
                                              << QString::number(p1.y())
                                              << QString::number(p2.x())
                                              << QString::number(p2.y()));
    if (this->cards[p1.x()][p1.y()] && this->cards[p2.x()][p2.y()])
    {
        if (!mainWindow::getConfigVariable("gameAnimations").toInt() || !this->gS || !this->gS->mW || !this->gS->mW->isGamePageVisible())
        {
            std::swap(this->cards[p2.x()][p2.y()], this->cards[p1.x()][p1.y()]);
            this->cards[p1.x()][p1.y()]->setPosition(p2);
            this->cards[p2.x()][p2.y()]->setPosition(p1);
            this->cards[p1.x()][p1.y()]->draw(this->scene(), DrawingType::InGame, QPointF(p1));
            this->cards[p2.x()][p2.y()]->draw(this->scene(), DrawingType::InGame, QPointF(p2));
        }
        else
        {
            this->cards[p1.x()][p1.y()]->moveOnBoardTo(p2, true);
            this->cards[p2.x()][p2.y()]->moveOnBoardTo(p1, true);
            this->gS->mW->setUIDisabledForAnimation(true);
            while (this->cards[p1.x()][p1.y()]->isBeingAnimated() || this->cards[p2.x()][p2.y()]->isBeingAnimated()) QCoreApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
            this->gS->mW->setUIDisabledForAnimation(false);
            std::swap(this->cards[p2.x()][p2.y()], this->cards[p1.x()][p1.y()]);
        }
    }
    else
    {
        mainWindow::reportError(tr("Error: two cards are needed for swap (in board::swapCards)"));
    }
}

void board::swapCards(const QPointF &p1, const QPointF &p2)
{
    swapCards(p1.toPoint(), p2.toPoint());
}

QPoint board::tileChoiceInternal(const QList<QPoint>& points, bool cancellable, const QList<int>& overlayList)
{
#ifdef BAZAAR_DEBUG
    if (!points.size())
    {
        mainWindow::reportError(tr("Script error: empty list of targets in tileChoice, this is probably a bug in the following script:\n%1").arg(gameState::scriptExecInfo.join("\n")));
    }
#endif
    if (this->gS->isAITurn())
    {
        auto choice = this->gS->aiExecPath.getChoice(points.size());
#ifdef BAZAAR_DEBUG
        QString debugStr = QString("Tile choice was (%1, %2) from a list of size %3")
                           .arg(QString::number(points[choice].x()), QString::number(points[choice].y()), QString::number(points.size()));
        this->gS->aiExecPath.addDebugData(debugStr);
#endif
        return points[choice];
    }
    if (cancellable) this->gS->setCancelEnabled(true);
    if (this->gS->mW)
    {
        this->gS->mW->setUIDisabledForUserChoice(true);
    }
    for (auto& point : points)
    {
        if (point.x() > this->w || point.y() > this->h || point.y() < 0 || point.x() < 0)
        {
            mainWindow::reportError(tr("Error: invalid point (%1,%2) in tileChoice(..)").arg(point.x(), point.y()));
        }
    }
    this->drawPlayerTileChoiceGraphics(points, overlayList);
    this->getChoice(points, nullptr, nullptr);
    if (this->gS->mW)
    {
        if (cancellable) this->gS->setCancelEnabled(false);
        this->gS->mW->setUIDisabledForUserChoice(false);
    }
    this->removePlayerTileChoiceGraphics();
    return this->choice;
}

QPoint board::tileChoiceInternalIntList(const QList<int> &points, bool cancellable, const QList<int> &overlayList)
{
    QList<QPoint> newPoints;
    newPoints.reserve(points.size()/2);
    for(int i = 0; i < points.size(); i+=2) {
        newPoints.push_back(QPoint{points[i], points[i+1]});
    }
    return tileChoiceInternal(newPoints, cancellable, overlayList);
}

int board::getHeight() const
{
    return this->h + 1;
}

int board::getWidth() const
{
    return this->w + 1;
}

int board::getNumberOfPlayerCards(int player)
{
    int c = 0;
    for (const auto &v : std::as_const(this->cards))
    {
        for (auto crd : v)
        {
            if (crd != nullptr)
                c += crd->getPlayer() == player;
        }
    }
    return c;
}

void board::moveCardToTrash(int uID)
{
    this->removeCard(uID, true);
}

QSize board::getTileSize(const QPoint& p)
{
    if (tileSizeMap.contains(p.x()) && tileSizeMap[p.x()].contains(p.y()))
    {
        return tileSizeMap[p.x()][p.y()];
    }
    else
    {
        mainWindow::reportError(tr("Error: invalid position in getTileSize"));
        return QSize(0, 0);
    }
}

QSize board::getTileSize(const QPointF& p)
{
    return getTileSize(p.toPoint());
}

QString board::getPlayerTileFilenamePrefix()
{
    return playerTileFilenamePrefix;
}

QString board::getTileOverlayFilenamePrefix()
{
    return tileOverlayFilenamePrefix;
}

fileFormat::saveFile board::save(bool forOOSCheck) const
{
    fileFormat::saveFile boardSave;
    boardSave.addItem("board", this->fName);
    if (!forOOSCheck) boardSave.addItem("boardPrevPlayerNum", QString::number(this->prevPlayerNum));
    auto cards = this->getAllCardsInternal();
    for (auto& crd : cards)
    {
        auto savedCard = crd->save(forOOSCheck).getItemByKey("card");
        savedCard.setStringAttribute("location", "board");
        boardSave.addItem(savedCard);
    }
    return boardSave;
}

void board::load(fileFormat::saveFile f)
{
    QFileInfoList boards = QDir(DATA_PATH "/boards/").entryInfoList(QStringList() << "*.cfg");
    boards.append(QDir(userContentPath() + "/boards/").entryInfoList(QStringList() << "*.cfg"));
    for (auto& board : boards)
    {
        if (board.fileName() == f.getItemByKey("board").getValueAsString() + ".cfg")
        {
            cfgFile cfg(board.absoluteFilePath());
            this->drawBackground(cfg.fileName(), cfg.readValues("background").constFirst(), cfg.readValues("tilemap").constFirst(), cfg.readValues("graphicsview_background").constFirst(),
                                 cfg.readValues("whiteTile").constFirst(), cfg.readValues("yellowTile").constFirst(), cfg.readValues("blueTile").constFirst(), cfg.readValues("greenTile").constFirst(), cfg.readValues("disabledTile").constFirst(), cfg.readValues("orangeTile").constFirst(), cfg.readValues("magentaTile").constFirst(),
                                 cfg.hasKey("playerTileFilenamePrefix") ? cfg.readValues("playerTileFilenamePrefix").constFirst() : "",
                                 cfg.hasKey("tileOverlayFilenamePrefix") ? cfg.readValues("tileOverlayFilenamePrefix").constFirst() : "",
                                 cfg.hasKey("coords") ? cfg.readValue("coords") : "");
            break;
        }
    }

    this->prevPlayerNum = f.getItemByKey("boardPrevPlayerNum").getValueAsString().toInt();
    for (const fileFormat::saveFileItem& c : f.getItems())
    {
        if (c.getKeyAsString() == "card" && c.getAttributeAsString("location") == "board")
        {
            QList<QPoint> availCoords;
            availCoords.push_back(QPoint(c.getAttributeAsString("posX").toInt(), c.getAttributeAsString("posY").toInt()));
            this->placeCard(availCoords, new card(c, this->gS), true, true);
        }
    }
}

board* board::clone(gameState* newState) const
{
    board* res = new board(nullptr);
    res->setGameState(newState);
    res->prevPlayerNum = this->prevPlayerNum;
    res->w = this->w;
    res->h = this->h;
    res->choice = this->choice;
    res->availC = this->availC;
    res->waitingForTileChoice = this->waitingForTileChoice;
    res->tileMapName = this->tileMapName;
    res->bkgName = this->bkgName;
    res->fName = this->fName;
    res->gviewBkgName = this->gviewBkgName;
    res->coordsName = this->coordsName;
    res->cards = this->cards;
    for (auto& p1 : res->cards)
    {
        for (auto& p2 : p1)
        {
            if (p2) p2 = p2->clone(newState);
        }
    }
    return res;
}

void board::addCardToBoard(card* crd, const QPoint& p)
{
    if (this->gS) this->gS->sendGameEvent(QStringList() << "addCardToBoard" << QString::number(crd->getUID()) << QString::number(p.x()) << QString::number(p.y()));
#ifdef BAZAAR_DEBUG
    if (crd->getPlayer() == -1)
    {
        mainWindow::reportError(tr("Script error: trying to add a card without an owner to the board, this is probably a bug in the following script:\n%1").arg(gameState::scriptExecInfo.join("\n")));
    }
#endif
    if (this->cards[p.x()][p.y()])
    {
        mainWindow::reportError(tr("Error: there is already a card at the target position (in board::addCardToBoard)"));
    }
    this->cards[p.x()][p.y()] = crd;
    crd->setPosition(p);
    crd->setSelectedForPlacement(false);
    crd->setHighlightedInInventory(false);
    crd->setHighlightedOnBoard(false);
    crd->removeFromPrevScene();
    if (!mainWindow::getConfigVariable("gameAnimations").toInt() || !this->gS || !this->gS->mW || !this->gS->mW->isGamePageVisible())
    {
        crd->draw(this->scene(), DrawingType::InGame, QPointF(p.x(), p.y()));
    }
    else
    {
        crd->animateBoardPlacement(this->scene());
    }
    connect(crd, &card::mouseEntered, this, &board::cardMouseEntered);
    connect(crd, &card::clicked, this, &board::cardClicked);
    connect(crd, &card::mouseHeldDown, this, &board::cardMouseHeldDown);
    connect(crd, &card::rightMouseClicked, this, &board::cardRightClicked);
    connect(crd, &card::mouseLeft, this, &board::cardMouseLeft);
}

QList<QPoint> board::getPositionListForCardActivation(int player) const
{
    if (player <= 1 || player > 4)
    {
        QList<QPoint> result;
        if (result.isEmpty())
        {
            for (int j = 0; j <= h; j++)
            {
                for (int i = 0; i <= w; i++)
                {
                    result.append(QPoint(i, j));
                }
            }
        }
        return result;
    }
    else if (player == 2)
    {
        QList<QPoint> result;
        if (result.isEmpty())
        {
            for (int j = h; j >= 0; j--)
            {
                for (int i = w; i >= 0; i--)
                {
                    result.append(QPoint(i, j));
                }
            }
        }
        return result;
    }
    else if (player == 3)
    {
        QList<QPoint> result;
        if (result.isEmpty())
        {
            for (int i = w; i >= 0; i--)
            {
                for (int j = 0; j <= h; j++)
                {
                    result.append(QPoint(i, j));
                }
            }
        }
        return result;
    }
    else  // player == 4
    {
        QList<QPoint> result;
        if (result.isEmpty())
        {
            for (int i = 0; i <= w; i++)
            {
                for (int j = h; j >= 0; j--)
                {
                    result.append(QPoint(i, j));
                }
            }
        }
        return result;
    }
}

bool board::contains(card* c) const
{
    for (const auto& m : this->cards)
    {
        const auto vals = m.values();
        if (vals.contains(c)) return true;
    }
    return false;
}

board::~board()
{
    this->removeCardPlacementGraphics();
    this->removePlayerTileChoiceGraphics();
    if (this->rotationTimer)
    {
        this->rotationTimer->stop();
        this->rotationTimer->deleteLater();
    }
    for (const auto& p1 : std::as_const(cards))
    {
        for (const auto& p2 : p1)
        {
            if (p2) p2->deleteCardLater();
        }
    }
    if (this->scene()) this->scene()->clear();
}

void board::dragEnterEvent(QDragEnterEvent* event)
{
    if (event->proposedAction() == Qt::MoveAction) event->acceptProposedAction();
}

void board::dragMoveEvent(QDragMoveEvent* event)
{
    if (event->proposedAction() == Qt::MoveAction) event->acceptProposedAction();
}

void board::dropEvent(QDropEvent* event)
{
    if (this->waitingForTileChoice)
    {
        this->choice = this->positionForChoice(this->mapToScene(event->position().toPoint()).toPoint());
        this->stopTileChoice();
        if (this->choice != QPoint(-1, -1)) event->acceptProposedAction();
    }
}

QPoint board::getPixelPosForTilePos(const QPoint& p)
{
    if (tilePosMap.contains(p.x()) && tilePosMap[p.x()].contains(p.y()))
    {
        return tilePosMap[p.x()][p.y()];
    }
    else
    {
        mainWindow::reportError("Error: invalid position in getPixelPosForTilePos");
        return QPoint(-1, -1);
    }
}

QPoint board::getPixelPosForTilePos(const QPointF& p)
{
    return getPixelPosForTilePos(p.toPoint());
}

void board::getChoice(const QList<QPoint>& points, QWidget* dragSource, card* sender)
{
    this->choice = QPoint(-1, -1);
    this->availC = points;
    this->waitingForTileChoice = true;
    auto stopC = [this]()
    {
        this->waitingForTileChoice = false;
    };
    connect(gS->storage, &cardInventory::stopTileChoice, stopC);
    connect(gS->p1Inventory, &cardInventory::stopTileChoice, stopC);
    connect(gS->p2Inventory, &cardInventory::stopTileChoice, stopC);
    connect(gS->p3Inventory, &cardInventory::stopTileChoice, stopC);
    connect(gS->p4Inventory, &cardInventory::stopTileChoice, stopC);
    connect(gS->huckster, &cardInventory::stopTileChoice, stopC);
    connect(gS->trash, &cardInventory::stopTileChoice, stopC);
    connect(gS->mainInventory, &cardInventory::stopTileChoice, stopC);
    if (this->gS->mW) connect(this->gS->mW, &mainWindow::cancelButtonClicked, stopC);
    connect(this, &board::mouseClicked, stopC);
    if (!dragSource)
    {
        while (this->waitingForTileChoice)
        {
            QCoreApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
        }
    }
    else
    {
        QDrag* drag = new QDrag{dragSource};
        drag->setPixmap(QPixmap::fromImage(sender->renderToPixmap(DrawingType::InGame, {}, {}, {}, true)));
        drag->setHotSpot(QPoint(drag->pixmap().width() / 2, drag->pixmap().height() / 2));
        drag->setMimeData(new QMimeData{});
        drag->exec();
        this->stopTileChoice();
    }
}

void board::stopTileChoice()
{
    this->waitingForTileChoice = false;
    this->removeCardPlacementGraphics();
    this->removePlayerTileChoiceGraphics();
    this->setDragMode(QGraphicsView::ScrollHandDrag);
}

void board::updateCoordGrid()
{
    if (mainWindow::getConfigVariable("coordGrid").toInt() && !this->coordsPxItem && !this->coordsName.isEmpty())
    {
        QPixmap coords = getPrimaryPathScaled("boards/coord/" + this->coordsName + ".png", -1);
        if (!coords.isNull())
        {
            this->coordsPxItem = scene()->addPixmap(coords);
            this->coordsPxItem->setPos(-coords.width() / 2, -coords.height() / 2);
            this->coordsPxItem->setZValue(3000);
        }
    }
    else if (!mainWindow::getConfigVariable("coordGrid").toInt() && this->coordsPxItem)
    {
        this->scene()->removeItem(this->coordsPxItem);
        delete this->coordsPxItem;
        this->coordsPxItem = nullptr;
    }
}

bool board::placeCard(const QList<QPoint>& availableCoords, card* sender, bool forced, bool noEvent, QWidget* dragSource)
{
    this->setDragMode(QGraphicsView::NoDrag);
    if (!forced)
    {
        this->drawCardPlacementGraphics(availableCoords);
        if (this->gS->mW) this->gS->mW->setUIDisabledForUserChoice(true);
        getChoice(availableCoords, dragSource, sender);
        if (this->gS->mW) this->gS->mW->setUIDisabledForUserChoice(false);
    }
    else
    {
        this->choice = availableCoords.constFirst();
    }
    if (this->choice != QPoint(-1, -1))
    {
        this->gS->storage->removeCard(sender->getUID());
        this->gS->p1Inventory->removeCard(sender->getUID());
        this->gS->p2Inventory->removeCard(sender->getUID());
        this->gS->p3Inventory->removeCard(sender->getUID());
        this->gS->p4Inventory->removeCard(sender->getUID());
        this->gS->trash->removeCard(sender->getUID());
        this->gS->mainInventory->removeCard(sender->getUID());
        this->gS->huckster->removeCard(sender->getUID());
        this->addCardToBoard(sender, this->choice);
        if (!noEvent)
        {
            this->gS->runEvent(QStringList() << "cardPlacedOnBoard" << QString::number(sender->getUID())
                               << QString::number(sender->getPosition().x())
                               << QString::number(sender->getPosition().y()), sender);
        }
        this->choice = QPoint(-1, -1);
        this->setDragMode(QGraphicsView::ScrollHandDrag);
        return true;
    }
    this->setDragMode(QGraphicsView::ScrollHandDrag);
    return false;
}

void board::rotateBoardToPlayer(int playerNum, bool forceRotateAndSkipAnim)
{
    if (this->gS) this->gS->sendGameEvent(QStringList() << "rotateBoardToPlayer" << QString::number(playerNum));

    if (this->gS && !forceRotateAndSkipAnim && !this->gS->isLocalUser(playerNum) && this->gS->getConfigVariable("neverChangePersp").toInt())
    {
        emit this->rotationFinished();
        return;
    }
    if (this->gS && !forceRotateAndSkipAnim && this->gS->isNetworkTurn() && this->gS->getConfigVariable("doNotChangeOnlinePersp").toInt())
    {
        emit this->rotationFinished();
        return;
    }

    QMap<int, QMap<int, int>> rotationMap =
    {
        {1, {{1, 0}, {2, -180}, {3, 90}, {4, -90}}},
        {2, {{2, 0}, {1, -180}, {4, 90}, {3, -90}}},
        {3, {{3, 0}, {2, 90}, {1, -90}, {4, -180}}},
        {4, {{4, 0}, {2, -90}, {3, -180}, {1, 90}}},
    };
    if (this->rotationTimer->isActive()) //finish current rotation immediately
    {
        this->rotationTimer->stop();
        QTransform t = this->transform();
        t.rotate((static_cast<double>(rotDegree) / this->rotSteps) * (abs(this->currRotStep)));
        this->setTransform(t);
        emit this->rotationFinished();
    }
    this->rotDegree = -rotationMap[prevPlayerNum][playerNum];
    if (this->gS && this->gS->mW) this->gS->mW->setUIDisabledForAnimation(true);
    if (!mainWindow::getConfigVariable("boardRotAnim").toInt() || forceRotateAndSkipAnim)
    {
        QTransform t = this->transform();
        t.rotate(static_cast<double>(rotDegree));
        this->setTransform(t);
        this->prevPlayerNum = playerNum;
        emit this->rotationFinished();
    }
    else
    {
        this->rotSteps = this->currRotStep = abs(rotationMap[prevPlayerNum][playerNum]);
        this->rotationTimer->setInterval(1000.0 / 120.0);
        this->setDragMode(QGraphicsView::NoDrag);
        this->disableInteractionForRotation = true;
        this->prevPlayerNum = playerNum;
        this->rotationTimer->start();
    }
}

void board::cancelChoice()
{
    this->waitingForTileChoice = false;
}

void board::mousePressEvent(QMouseEvent* event)
{
    QGraphicsView::mousePressEvent(event);
    if (this->waitingForTileChoice)
    {
        this->choice = this->positionForChoice(this->mapToScene(event->pos()).toPoint());
        if (this->availC.contains(this->choice))
        {
            emit this->mouseClicked();
            this->stopTileChoice();
        }
    }
}

void board::drawBackground(QPainter* painter, const QRectF& rect)
{
    if (gS && !gS->mW) return;
    const QTransform old = painter->transform();
    painter->setTransform(QTransform());
    painter->fillRect(old.mapRect(rect), this->backgroundBrush());
    //painter->drawPixmap(old.mapRect(r), d.backgroundPixmap, old.mapRect(r));
    painter->setTransform(old);
}

void board::cardMouseEntered()
{
    card* sender = static_cast<card*>(QObject::sender());
    if (this->gS->mW)
    {
        sender->setHighlightedOnBoard(true);
        this->gS->mW->displayCardOnSideBar(static_cast<card*>(QObject::sender())->getID());
    }
}

void board::cardMouseLeft()
{
    card* sender = static_cast<card*>(QObject::sender());
    if (this->gS->mW)
    {
        sender->setHighlightedOnBoard(false);
        this->gS->mW->displayCardOnSideBar("", true);
    }
}

void board::cardClicked()
{
    card* sender = static_cast<card*>(QObject::sender());
    if (this->gS->mW) this->gS->mW->displayCardOnSideBar(sender->getID());
}

void board::cardRightClicked()
{
    this->handleRightClickAction(QObject::sender());
}

void board::cardMouseHeldDown()
{
    this->handleRightClickAction(QObject::sender());
}

void board::drawCardPlacementGraphics(const QList<QPoint>& availableCoords)
{
    if (!gS->mW) return;
    for (auto& cord : availableCoords)
    {
        graphicsPixmapItem* it = new graphicsPixmapItem;
        it->setPixmap(getPrimaryPathScaled(QString("boards/%1tileoverlay_cardplacement.png").arg(getTileOverlayFilenamePrefix()), 157));
        this->cardPlacementGraphics.append(it);
        this->scene()->addItem(it);
        it->setZValue(BOARD_CHOICE_ZVALUE);
        it->setPos(getPixelPosForTilePos(cord));
    }
}

void board::removeCardPlacementGraphics()
{
    for (auto ptr : this->cardPlacementGraphics)
    {
        if (ptr->scene()) ptr->scene()->removeItem(ptr);
        ptr->deleteLater();
    }
    this->cardPlacementGraphics.clear();
}

void board::drawPlayerTileChoiceGraphics(const QList<QPoint>& availableCoords, const QList<int>& overlayList)
{
    if (!gS->mW) return;
    for (int i = 0; i < availableCoords.size(); i++)
    {
        auto coord = availableCoords[i];
        graphicsPixmapItem* it = new graphicsPixmapItem;
        connect(it, &graphicsPixmapItem::mouseEntered, this, [this, coord]()
        {
            if (auto crd = this->getCardAtPosition(coord))
            {
                if (this->gS->mW)
                {
                    this->gS->mW->displayCardOnSideBar(crd->getID());
                }
            }
        });
        connect(it, &graphicsPixmapItem::mouseLeft, this, [this, coord]()
        {
            if (this->getCardAtPosition(coord))
            {
                if (this->gS->mW)
                {
                    this->gS->mW->displayCardOnSideBar("", true);
                }
            }
        });
        if (overlayList.isEmpty())
        {
            it->setPixmap(getPrimaryPathScaled(QString("boards/%1tileoverlay_playerchoice.png").arg(getTileOverlayFilenamePrefix()), 157));
        }
        else
        {
            if (static_cast<TileOverlay>(overlayList[i]) == TileOverlay::CancelChoiceOverlay)
            {
                it->setPixmap(getPrimaryPathScaled(QString("boards/%1tileoverlay_cancelchoice.png").arg(getTileOverlayFilenamePrefix()), 157));

            }
            else if (static_cast<TileOverlay>(overlayList[i]) == TileOverlay::TransparentOverlay)
            {
                it->setPixmap(getPrimaryPathScaled(QString("boards/%1tileoverlay_transparent.png").arg(getTileOverlayFilenamePrefix()), 157));

            }
            else if (static_cast<TileOverlay>(overlayList[i]) == TileOverlay::CardPlacementOverlay)
            {
                it->setPixmap(getPrimaryPathScaled(QString("boards/%1tileoverlay_cardplacement.png").arg(getTileOverlayFilenamePrefix()), 157));

            }
            else if (static_cast<TileOverlay>(overlayList[i]) == TileOverlay::PlayerChoiceOverlay)
            {
                it->setPixmap(getPrimaryPathScaled(QString("boards/%1tileoverlay_playerchoice.png").arg(getTileOverlayFilenamePrefix()), 157));
            }
        }
        this->playerTileChoiceGraphics.append(it);
        this->scene()->addItem(it);
        it->setZValue(BOARD_CHOICE_ZVALUE);
        it->setPos(getPixelPosForTilePos(coord));
    }
}

void board::removePlayerTileChoiceGraphics()
{
    for (auto ptr : this->playerTileChoiceGraphics)
    {
        if (ptr->scene()) ptr->scene()->removeItem(ptr);
        ptr->deleteLater();
    }
    this->playerTileChoiceGraphics.clear();
}

QPoint board::positionForChoice(const QPoint& p)
{
    for (auto& apoint : this->availC)
    {
        if (QRect(getPixelPosForTilePos(apoint), getTileSize(apoint)).contains(p))
        {
            return QPoint(apoint.x(), apoint.y());
        }
    }
    return QPoint(-1, -1);
}

QMap<int, QMap<int, QChar>> board::tiles{};
