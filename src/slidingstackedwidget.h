/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SLIDINGSTACKEDWIDGET_H
#define SLIDINGSTACKEDWIDGET_H

#include <QStackedWidget>
#include <QWidget>
#include <QEasingCurve>

/*!
slidingStackedWidget is a class that is derived from QStackedWidget
and allows smooth side shifting of widgets, in addition
to the original hard switching from one to another as offered by
QStackedWidget itself
*/
class slidingStackedWidget : public QStackedWidget
{
        Q_OBJECT

    public:
        enum class AnimationDirection
        {
            LeftToRight,
            RightToLeft,
            TopToBottom,
            BottomToTop,
            Automatic
        };
        slidingStackedWidget(QWidget* parent);
        ~slidingStackedWidget() {}

    public slots:
        /*!
        Animation duration in milliseconds
        */
        void setSpeed(int speed);
        void setAnimation(QEasingCurve::Type animationType);
        void setVerticalMode(bool vertical = true);
        /*!
        Wrapping is related to slideInNext/Previous.
        It defines the behaviour when reaching first/last page
        */
        void setWrap(bool wrap);
        void slideInNext();
        void slideInPrevious();
        void slideInIndex(int index, slidingStackedWidget::AnimationDirection direction = slidingStackedWidget::AnimationDirection::Automatic);
        void slideInWidget(QWidget* newWidget, slidingStackedWidget::AnimationDirection direction = slidingStackedWidget::AnimationDirection::Automatic);

    signals:
        /*!
        This is used for internal purposes in the class engine
        */
        void animationFinished();

    protected slots:
        /*!
        This is used for internal purposes in the class engine
        */
        void animationDoneSlot();

    protected:
        /*!
        This is used for internal purposes in the class engine
        */
        int mSpeed;
        QEasingCurve::Type mAnimationtype;
        void paintEvent(QPaintEvent* event) override;
        bool mVertical;
        int mNow;
        int mNext;
        bool mWrap;
        QPoint mPnow;
        bool mActive;
};

#endif // SLIDINGSTACKEDWIDGET_H
