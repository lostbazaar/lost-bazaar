/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <QGraphicsPixmapItem>
#include <QTimer>
#include <QTextStream>
#include <QApplication>
#include <QVBoxLayout>
#include <QFont>
#include <QFontMetrics>
#include <QSpacerItem>
#include "graphicsviewmenu.h"
#include "mainwindow.h"
#include "board.h"

graphicsViewMenu::graphicsViewMenu(board* view)
{
    this->v = view;
    this->gS = nullptr;
    this->res = -1;
    if (view->parent())
    {
        if (this->v->layout() == nullptr)
        {
            this->v->setLayout(new QVBoxLayout);
        }
        static_cast<QVBoxLayout*>(this->v->layout())->addStretch(1);
        this->longInfoLabel = new QLabel;
        this->v->layout()->addWidget(this->longInfoLabel);
        this->v->layout()->setAlignment(this->longInfoLabel, Qt::AlignCenter);
        this->longInfoLabel->setVisible(false);
        this->longInfoLabel->setObjectName("graphicsViewMenuInfoLabel");
        this->longInfoOKButton = new QPushButton;
        this->v->layout()->addWidget(this->longInfoOKButton);
        this->v->layout()->setAlignment(this->longInfoOKButton, Qt::AlignCenter);
        this->longInfoOKButton->setVisible(false);
        this->longInfoOKButton->setObjectName("graphicsViewMenuChoiceButton");
        for (int i = 0; i < 7; i++)
        {
            this->buttons.append(new QPushButton);
            this->v->layout()->addWidget(this->buttons.last());
            this->v->layout()->setAlignment(this->buttons.last(), Qt::AlignCenter);
            this->buttons.last()->setVisible(false);
            this->buttons.last()->setObjectName("graphicsViewMenuChoiceButton");
        }
        static_cast<QVBoxLayout*>(this->v->layout())->addStretch(1);
        connect(longInfoOKButton, &QPushButton::clicked, this, &graphicsViewMenu::okButtonClicked);
    }
}

int graphicsViewMenu::getChoice(const QStringList& values, const QStringList& enableList, const QStringList& toolTips, bool overrideAI)
{
    if (gS->isAITurn() && !(this->gS->mW && overrideAI))
    {
        QList<int> enabledValList;
        for (auto i = 0; i < values.size(); ++i)
        {
            if (enableList.length() > i)
            {
                if (enableList[i] == "true")
                {
                    enabledValList.append(i);
                }
            }
            else
            {
                enabledValList.append(i);
            }
        }
        auto choice = this->gS->aiExecPath.getChoice(enabledValList.size(), overrideAI ? 0 : -1);
#ifdef BAZAAR_DEBUG
        QString debugStr;
        debugStr += "Menu choice was (" + QString::number(choice) + ") from:\n";
        for (int i = 0; i < values.size(); ++i)
        {
            debugStr += "   (" + QString::number(i) + ") " + values[i] + (enabledValList.count(i) ? "\n" : " (disabled)\n");
        }
        this->gS->aiExecPath.addDebugData(debugStr);
#endif
        return enabledValList[choice];
    }
    if (this->gS->mW)
    {
        this->gS->mW->setUIDisabledForUserChoice(true);
    }
    this->res = -1;
    int fwidth = 646;
    this->currentVals = values;
    for (int i = 0; i < values.size(); i++)
    {
        this->buttons[i]->setText(values[i]);
        this->buttons[i]->setVisible(true);
        if (enableList.length() > i)
        {
            this->buttons[i]->setEnabled(enableList[i] == "true");
        }
        else
        {
            this->buttons[i]->setEnabled(true);
        }
        if (toolTips.length() > i)
        {
            this->buttons[i]->setToolTip(toolTips[i]);
        }
        else
        {
            this->buttons[i]->setToolTip(QString());
        }
        connect(this->buttons[i], &QPushButton::clicked, this, &graphicsViewMenu::buttonClicked);
        this->buttons[i]->setFixedWidth(fwidth);
    }
    QString maxval;
    for (int i = 0; i < values.size(); i++)
    {
        if (values[i].length() > maxval.length()) maxval = values[i];
    }
    QFont f(this->buttons[0]->font());
    for (int i = 10; i < 1000; i += 2)
    {
        f.setPointSize(i);
        if (QFontMetrics(f).horizontalAdvance(maxval) > fwidth)
        {
            break;
        }
    }
    f.setPointSize(f.pointSize() - 2);
    for (int i = 0; i < buttons.size(); i++)
    {
        buttons[i]->setFont(f);
    }
    this->blocked = true;
    while (this->blocked)
    {
        QCoreApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
    }
    if (this->gS->mW)
    {
        this->gS->mW->setUIDisabledForUserChoice(false);
    }
    return this->res;
}

int graphicsViewMenu::getPlayer(const QList<int>& values, bool cancellable)
{
    QStringList players;
    for (int i = 0; i < values.size(); i++)
    {
        players.append(mainWindow::getPlayerNickNames().at(values[i] - 1));
    }
    if (cancellable) players.append("Cancel");
    int r = this->getChoice(players);
    if (cancellable && r == values.size()) return -1;
    return values[r];
}

void graphicsViewMenu::showInformation(const QString& info, int msecs, bool clearPrevious, bool overrideAI)
{
    if (!gS->isAITurn() || (this->gS->mW && overrideAI)) this->addInfoLabel(info, msecs, clearPrevious);
}

void graphicsViewMenu::showLongInformation(const QString& info, const QString& okButtonText)
{
    if (gS->isAITurn())
    {
        return;
    }
    if (this->gS->mW)
    {
        this->gS->mW->setUIDisabledForUserChoice(true);
    }
    int fwidth = 646;

    this->longInfoOKButton->setText(okButtonText);
    this->longInfoLabel->setText(info);
    this->longInfoOKButton->setVisible(true);
    this->longInfoOKButton->setFixedWidth(fwidth);

    QFont f(this->longInfoOKButton->font());
    for (int i = 10; i < 1000; i += 2)
    {
        f.setPointSize(i);
        if (QFontMetrics(f).horizontalAdvance(okButtonText) > fwidth)
        {
            break;
        }
    }
    f.setPointSize(f.pointSize() - 2);

    longInfoOKButton->setFont(f);

    longInfoLabel->setWordWrap(true);
    longInfoLabel->setText(info);

    QFont f2(longInfoLabel->font());
    for (int i = 12; i < 64; i += 1)
    {
        f2.setPointSize(i);
        if (QFontMetrics(f2).boundingRect(info).width() > fwidth)
        {
            break;
        }
    }
    f2.setPointSize(f2.pointSize() - 2);
    auto mHeight = QFontMetrics(f2).boundingRect(QRect(0, 0, fwidth, 1000), Qt::TextWordWrap, info).height() + 10;
    longInfoLabel->setStyleSheet("font-size: " + QString::number(f2.pointSize()) + "pt;");
    longInfoLabel->setAlignment(Qt::AlignCenter);
    longInfoLabel->setMaximumSize(fwidth, mHeight);
    longInfoLabel->setFixedWidth(fwidth);
    longInfoLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Maximum);
    longInfoLabel->setVisible(true);
    static_cast<QVBoxLayout*>(this->v->layout())->setAlignment(longInfoLabel, Qt::AlignHCenter);

    this->blocked = true;
    while (this->blocked)
    {
        QCoreApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
    }
    if (this->gS->mW)
    {
        this->gS->mW->setUIDisabledForUserChoice(false);
    }
}

void graphicsViewMenu::reset()
{
    for (auto btn : this->buttons)
    {
        disconnect(btn, nullptr, this, nullptr);
        btn->setVisible(false);
    }
    this->hideInformation();
}

bool graphicsViewMenu::declareWinnerPlayers(const QList<int>& winners, const QString& backToText)
{
    if (winners.isEmpty())
    {
        this->showInformation(tr("It's a draw!<br>What would you like to do?"), -1, true, true);
    }
    else if (winners.size() == 1)
    {
        this->showInformation(tr("%1 won the game!<br>What would you like to do?").arg(mainWindow::getPlayerNickNames().at(winners[0] - 1)), -1, true, true);
    }
    else if (winners.size() == 2)
    {
        this->showInformation(tr("%1 and %2 won the game!<br>What would you like to do?").arg(
                                  mainWindow::getPlayerNickNames().at(winners[0] - 1),
                                  mainWindow::getPlayerNickNames().at(winners[1] - 1)), -1, true, true);
    }
    else if (winners.size() == 3)
    {
        this->showInformation(tr("%1, %2 and %3 won the game!<br>What would you like to do?").arg(
                                  mainWindow::getPlayerNickNames().at(winners[0] - 1),
                                  mainWindow::getPlayerNickNames().at(winners[1] - 1),
                                  mainWindow::getPlayerNickNames().at(winners[2] - 1)), -1, true, true);
    }
    else if (winners.size() == 4)
    {
        this->showInformation(tr("%1, %2, %3 and %4 won the game!<br>What would you like to do?").arg(
                                  mainWindow::getPlayerNickNames().at(winners[0] - 1),
                                  mainWindow::getPlayerNickNames().at(winners[1] - 1),
                                  mainWindow::getPlayerNickNames().at(winners[2] - 1),
                                  mainWindow::getPlayerNickNames().at(winners[3] - 1)), -1, true, true);
    }
    if (this->gS->mW && this->gS->mW->autoCfg.hasKey("autoExit") && this->gS->mW->autoCfg.readValue("autoExit") == "true")
    {
        std::_Exit(0);
    }
    if (this->getChoice(QStringList() << tr("Continue") << backToText, {}, {}, true))
    {
        this->hideInformation();
        //back to main
        return false;
    }
    else   //continue
    {
        this->hideInformation();
        return true;
    }
}

bool graphicsViewMenu::declareWinnerTeam(int team, const QString& backToText)
{
    if (team == -1)
    {
        return this->declareWinnerPlayers(QList<int>(), backToText);
    }
    else if (team == 1)
    {
        return this->declareWinnerPlayers(QList<int>() << 1 << 3, backToText);
    }
    else if (team == 2)
    {
        return this->declareWinnerPlayers(QList<int>() << 2 << 4, backToText);
    }
    else
    {
        mainWindow::reportError(tr("Error: invalid team (%1) in declareWinnerTeam(..)").arg(QString::number(team)));
        return true; //continue the game
    }
}

void graphicsViewMenu::setGameState(gameState* gS)
{
    this->gS = gS;
}

graphicsViewMenu::~graphicsViewMenu()
{
    for (auto* btn : this->buttons)
    {
        if (this->v && this->v->layout())
        {
            this->v->layout()->removeWidget(btn);
        }
        btn->deleteLater();
    }
    for (auto* lbl : this->infoLabels)
    {
        if (this->v && this->v->layout())
        {
            this->v->layout()->removeWidget(lbl);
        }
        lbl->deleteLater();
    }
    if (this->longInfoLabel) this->longInfoLabel->deleteLater();
    if (this->longInfoOKButton) this->longInfoOKButton->deleteLater();
    this->v = nullptr;
}

void graphicsViewMenu::hideInformation()
{
    auto labs = this->infoLabels;
    this->infoLabels.clear();
    for (const auto& lab : labs)
    {
        QGraphicsOpacityEffect* eff = new QGraphicsOpacityEffect();
        lab->setGraphicsEffect(eff);
        QPropertyAnimation* a = new QPropertyAnimation(eff, "opacity");
        QPropertyAnimation* a2 = new QPropertyAnimation(lab, "maximumHeight");
        a2->setDuration(650);
        a2->setStartValue(lab->maximumHeight());
        a2->setEndValue(0);
        a->setDuration(800);
        a->setStartValue(1);
        a->setEndValue(0);
        a->start(QPropertyAnimation::DeleteWhenStopped);
        a2->start(QPropertyAnimation::DeleteWhenStopped);
        QTimer* t = new QTimer(this);
        t->setInterval(1000);
        t->setSingleShot(true);
        connect(t, &QTimer::timeout, this, [=]()
        {
            if (lab)
            {
                this->v->layout()->removeWidget(lab);
                lab->deleteLater();
            }
            t->deleteLater();
        });
        t->start();
    }
}

void graphicsViewMenu::buttonClicked()
{
    QPushButton* btn = static_cast<QPushButton*>(QObject::sender());
    for (auto btn : this->buttons)
    {
        disconnect(btn, nullptr, this, nullptr);
        btn->setVisible(false);
    }
    this->res = this->currentVals.indexOf(btn->text());
    this->blocked = false;
}

void graphicsViewMenu::okButtonClicked()
{
    this->longInfoLabel->setVisible(false);
    this->longInfoOKButton->setVisible(false);
    this->blocked = false;
}

void graphicsViewMenu::addInfoLabel(const QString& info, int msecs, bool clearPrevious)
{
    if (!this->gS || !this->gS->mW) return;
    if (clearPrevious)
    {
        this->hideInformation();
    }
    QLabel* infoLabel = new QLabel;
    static_cast<QVBoxLayout*>(this->v->layout())->insertWidget(0, infoLabel, Qt::AlignHCenter | Qt::AlignTop);
    infoLabel->setObjectName("graphicsViewMenuInfoLabel");
    infoLabel->setWordWrap(true);
    infoLabel->setText(info);
    int fwidth = 646;
    QFont f(infoLabel->font());
    for (int i = 12; i < 22; i += 1)
    {
        f.setPointSize(i);
        if (QFontMetrics(f).horizontalAdvance(info) > fwidth)
        {
            break;
        }
    }
    f.setPointSize(f.pointSize() - 2);
    //auto mHeight = QFontMetrics(f).boundingRect(QRect(0, 0, fwidth, 1000), Qt::TextWordWrap, info).height() + 10;
    infoLabel->setStyleSheet("font-size: " + QString::number(f.pointSize()) + "pt;");
    infoLabel->setAlignment(Qt::AlignCenter);
    QGraphicsOpacityEffect* eff = new QGraphicsOpacityEffect(this);
    infoLabel->setGraphicsEffect(eff);
    infoLabel->setFixedWidth(fwidth);
    infoLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Maximum);
    infoLabel->setFont(f);
    QPropertyAnimation* a = new QPropertyAnimation(eff, "opacity");
    QPropertyAnimation* a2 = new QPropertyAnimation(infoLabel, "maximumHeight");
    static_cast<QVBoxLayout*>(this->v->layout())->setAlignment(infoLabel, Qt::AlignHCenter);
    a->setDuration(1000);
    a2->setDuration(650);
    a2->setStartValue(0);
    a2->setEndValue(infoLabel->sizeHint().height());
    a->setStartValue(0);
    a->setEndValue(1);
    a->start(QPropertyAnimation::DeleteWhenStopped);
    a2->start(QPropertyAnimation::DeleteWhenStopped);
    infoLabels.push_back(infoLabel);
    if (msecs > 0)
    {
        QTimer* t = new QTimer(this);
        t->setInterval(1000 + msecs);
        t->setSingleShot(true);
        connect(t, &QTimer::timeout, this, [=]()
        {
            if (infoLabels.contains(infoLabel))
            {
                infoLabels.removeOne(infoLabel);
                QGraphicsOpacityEffect* eff = new QGraphicsOpacityEffect();
                infoLabel->setGraphicsEffect(eff);
                QPropertyAnimation* a = new QPropertyAnimation(eff, "opacity");
                QPropertyAnimation* a2 = new QPropertyAnimation(infoLabel, "maximumHeight");
                a2->setDuration(650);
                a2->setStartValue(infoLabel->sizeHint().height());
                a2->setEndValue(0);
                a->setDuration(800);
                a->setStartValue(1);
                a->setEndValue(0);
                a->start(QPropertyAnimation::DeleteWhenStopped);
                a2->start(QPropertyAnimation::DeleteWhenStopped);
                QTimer* t2 = new QTimer(this);
                t2->setInterval(1000);
                t2->setSingleShot(true);
                connect(t2, &QTimer::timeout, this, [=]()
                {
                    this->v->layout()->removeWidget(infoLabel);
                    if (infoLabel) infoLabel->deleteLater();
                    t2->deleteLater();
                });
                t2->start();
            }
            t->deleteLater();
        });
        t->start();
    }
}
