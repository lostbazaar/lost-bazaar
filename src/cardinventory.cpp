/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <cmath>
#include <QVBoxLayout>
#include <QGraphicsScene>
#include <QScrollBar>
#include <QMapIterator>
#include <QTextStream>
#include <QDrag>
#include <QMimeData>
#include <QGraphicsPixmapItem>
#include "globals.h"
#include "card.h"
#include "board.h"
#include <algorithm>
#include "cardinventory.h"
#include "mainwindow.h"
#include "graphicsviewmenu.h"
#include "ui_mainwindow.h"

cardInventory::cardInventory(QWidget* parent) : currentPage(0)
{
    this->setParent(parent);
    if (parent)
    {
        this->setLayout(new QHBoxLayout);
        this->setScene(new QGraphicsScene);
        this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    }
    this->gS = nullptr;
    this->reinitialize();
}

void cardInventory::addCard(card* crd)
{
    if (this->gS) this->gS->sendGameEvent(QStringList() << "addCard" << QString::number(this->getInventoryID()) << QString::number(crd->getUID()));
    this->contents.append(crd);
#ifdef BAZAAR_DEBUG
    if (this->trash && (this->gS->p1Inventory->contents.count(crd)
                        || this->gS->p2Inventory->contents.count(crd)
                        || this->gS->p3Inventory->contents.count(crd)
                        || this->gS->p4Inventory->contents.count(crd)))
    {
        mainWindow::reportError(tr("Script error: trying to to add a card to an inventory but the card is already present in another inventory, this is probably a bug in the following script:\n%1").arg(gameState::scriptExecInfo.join("\n")));
    }
#endif
    crd->setPlayer(this->player);
    crd->setShowFullSize(true, true);
    crd->setGrayScale(false, true);
    crd->setCounter(0);
    if (!(this->player >= 1 && this->player <= 4))
    {
        crd->setDisabledInInventory(false);
    }
    crd->removeFromPrevScene();
    connect(crd, &card::clicked, this, &cardInventory::cardClicked);
    connect(crd, &card::startDrag, this, &cardInventory::cardDragged);
    connect(crd, &card::mouseHeldDown, this, &cardInventory::cardRightClicked);
    connect(crd, &card::rightMouseClicked, this, &cardInventory::cardRightClicked);
    connect(crd, &card::mouseEntered, this, &cardInventory::cardMouseEntered);
    connect(crd, &card::mouseLeft, this, &cardInventory::cardMouseLeft);
    if (mainWindow::getConfigVariable("orderCardsInHandAlpha").toInt())
    {
        this->orderCardsAlpha();
    }
    this->redraw();
    if (this->gS && this->gS->mW) this->gS->mW->updateEndTurnButtonState();
    emit this->changed();
}

card* cardInventory::cardChoice(CardList cardList_, bool cancellable, QList<int> overlayList)
{
    QList<card*> cardList;
    for(auto* crd: cardList_) cardList.push_back(static_cast<card*>(crd));
#ifdef BAZAAR_DEBUG
    if (cardList.isEmpty())
    {
        mainWindow::reportError(tr("Script error: empty card list in cardInventory::cardChoice, this is probably a bug in the following script:\n%1").arg(gameState::scriptExecInfo.join("\n")));
    }
#endif
    if (this->gS->isAITurn())
    {
        auto choice = this->gS->aiExecPath.getChoice(cardList.size());
#ifdef BAZAAR_DEBUG
        QString debugStr = QString("The card %1 was chosen from the inventory (inventory player: %2, trash: %3, huckster: %4)")
                           .arg(cardList[choice]->getID(), QString::number(this->player),
                                QString::number(this->trash), QString::number(this->isHuckster));
        this->gS->aiExecPath.addDebugData(debugStr);
#endif
        return cardList[choice];
    }
    this->setHucksterChoiceMode(false);
    if (cancellable) this->gS->setCancelEnabled(true);
    if (this->gS->mW)
    {
        this->gS->mW->setUIDisabledForUserChoice(true);
        this->gS->mW->pushInventoryTab(this->gS->mW->ui->sideBarTabWidget->currentWidget(), this->tab);
    }
    this->choiceMode = true;
    this->choice = nullptr;
    for (int i = 0; i < cardList_.size(); i++)
    {
        for (auto crd : this->contents)
        {
            if (crd == cardList_[i])
            {
                if (!overlayList.isEmpty())
                {
                    crd->setInventoryOverlay(static_cast<CardInventoryOverlay>(overlayList[i]));
                }
                else
                {
                    crd->setInventoryOverlay(CardInventoryOverlay::PlayerChoiceOverlay);
                }
                crd->redraw();
                break;
            }
        }
    }
    while (this->choiceMode)
    {
        QCoreApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
    }
    if (cancellable) this->gS->setCancelEnabled(false);
    if (this->gS->mW)
    {
        this->gS->mW->setUIDisabledForUserChoice(false);
        this->gS->mW->popInventoryTab();
    }
    for (auto crd : this->contents)
    {
        crd->setInventoryOverlay(CardInventoryOverlay::NoOverlay);
        crd->setDisabledInInventory(false);
        crd->redraw();
    }
    return this->choice;
}

card* cardInventory::moveCardToBoard(int uID, QPoint targetPosition, bool activateAbility)
{
    for (auto crd : this->contents)
    {
        if (crd->getUID() == uID)
        {
            gS->gameBoard->placeCard(QList<QPoint>() << targetPosition, crd, true, !activateAbility);
#ifdef BAZAAR_DEBUG
            if (crd->getPlayer() == -1 && !this->gS->trash->contains(crd))
            {
                mainWindow::reportError(tr("Script error: trying to move a card with an invalid owner to the board, this is probably a bug in the following script:\n%1").arg(gameState::scriptExecInfo.join("\n")));
            }
#endif
            emit this->changed();
            return crd;
        }
    }
    return nullptr;
}

card *cardInventory::moveCardToBoard(int uID, QPointF targetPosition, bool activateAbility)
{
    return moveCardToBoard(uID, targetPosition.toPoint(), activateAbility);
}

void cardInventory::setGameState(gameState* gS)
{
    this->gS = gS;
}

void cardInventory::setTab(QWidget* t)
{
    this->tab = t;
}

void cardInventory::setStorage(bool t)
{
    this->isStorage = t;
    if (t) this->tabtext = tr("Storage");
}

card* cardInventory::removeCardByPtr(card* crd)
{
    this->contents.removeAll(crd);
    crd->removeFromScene(this->scene());
    disconnect(crd, 0, this, 0);
    this->redraw();
    emit this->changed();
    return crd;
}

void cardInventory::setTrash(bool t)
{
    this->trash = t;
    if (t) this->tabtext = tr("Trash");
}

bool cardInventory::isEmpty() const
{
    return this->contents.isEmpty();
}

fileFormat::saveFile cardInventory::save(bool forOOSCheck) const
{
    fileFormat::saveFile invSave;
    auto cards = this->contents;
    for (auto& crd : cards)
    {
        auto savedCard = crd->save(forOOSCheck).getItemByKey("card");
        savedCard.setStringAttribute("location", this->objectName());
        invSave.addItem(savedCard);
    }
    QStringMap m;
    m["id"] = this->objectName();
    m["isHuckster"] = QString::number(isHuckster);
    m["storage"] = QString::number(isStorage);
    if (!forOOSCheck)
    {
        m["viewable"] = QString::number(viewable);
        m["readOnly"] = QString::number(ro);
        m["currentPage"] = QString::number(currentPage);
        if (this->gS->mW)
        {
            m["active"] = QString::number(this->gS->mW->ui->sideBarTabWidget->indexOf(this->parentWidget()) == this->gS->mW->ui->sideBarTabWidget->currentIndex()
                                          && this->gS->mW->ui->sideBarTabWidget->currentIndex() != -1);
        }
        else
        {
            m["active"] = "0";
        }
    }
    m["trash"] = QString::number(trash);
    m["player"] = QString::number(player);
    invSave.addItem("cardInventory", QString(), m);
    return invSave;
}

void cardInventory::load(fileFormat::saveFile f)
{
    for (const fileFormat::saveFileItem&  i : f.getItems())
    {
        if (i.getKeyAsString() == "cardInventory" && i.getAttributeAsString("id") == this->objectName())
        {
            this->setHuckster(i.getAttributeAsString("isHuckster").toInt());
            //this->setViewable(i.getAttributeAsString("viewable").toInt(), i.getAttributeAsString("active").toInt());
            //this->tabtext = i.getAttributeAsString("tabText");
            this->setTrash(i.getAttributeAsString("trash").toInt());
            this->setStorage(i.getAttributeAsString("storage").toInt());
            //this->setReadOnly(i.getAttributeAsString("readOnly").toInt());
            this->setPlayer(i.getAttributeAsString("player").toInt());
            for (const fileFormat::saveFileItem& c : f.getItems())
            {
                if (c.getKeyAsString() == "card" && c.getAttributeAsString("location") == this->objectName())
                {
                    this->addCard(new card(c, this->gS));
                }
            }
            this->currentPage = 0;
            //this->currentPage = i.getAttributeAsString("currentPage").toInt();
            this->redraw();
            break;
        }
    }
}

void cardInventory::updateDisabledCards()
{
    if (this->gS && this->gS->mW)
    {
        if (this->player >= 1 && this->player <= 4)
        {
            if (this->gS->mW->ui->sideBarTabWidget->indexOf(this->tab) == this->gS->mW->ui->sideBarTabWidget->currentIndex())
            {
                for (card* crd : this->contents)
                {
#ifdef BAZAAR_DEBUG
                    gameState::scriptExecInfo.append(crd->getName() + " (getAvailableTiles)");
#endif
                    crd->setDisabledInInventory(this->gS->scriptEnv->executeScript(crd->getScriptCode(), "getAvailableTiles", crd, QStringList() << crd->getID() << QString::number(this->gS->getCurrentPlayer())).isEmpty());
#ifdef BAZAAR_DEBUG
                    gameState::scriptExecInfo.pop_back();
#endif
                }
            }
        }
        else if (this->isHuckster)
        {
            int cards = 0;
            switch (this->gS->getCurrentPlayer())
            {
                case 1:
                    cards = this->gS->p1Inventory->contents.size();
                    break;
                case 2:
                    cards = this->gS->p2Inventory->contents.size();
                    break;
                case 3:
                    cards = this->gS->p3Inventory->contents.size();
                    break;
                case 4:
                    cards = this->gS->p4Inventory->contents.size();
                    break;
                default:
                    return;
            }
            for (int i = 0; i < this->contents.size(); ++i)
            {
                this->contents[i]->setDisabledInInventory(i < cards - 1);
            }
        }
    }
}

int cardInventory::indexOf(card* c) const
{
    return this->contents.indexOf(c);
}

bool cardInventory::contains(card* c) const
{
    return this->contents.contains(c);
}

cardInventory* cardInventory::clone(gameState* newState) const
{
    cardInventory* res = new cardInventory(nullptr);
    res->setGameState(newState);
    res->isHuckster = this->isHuckster;
    res->viewable = this->viewable;
    res->tabtext = this->tabtext;
    res->trash = this->trash;
    res->ro = this->ro;
    res->player = this->player;
    res->choiceMode = this->choiceMode;
    res->hucksterChoiceMode = this->hucksterChoiceMode;
    for (const auto& c : contents)
    {
        res->addCard(c->clone(newState));
    }
    res->currentPage = this->currentPage;
    return res;
}

cardInventory::~cardInventory()
{
    for (auto& c : contents) c->deleteCardLater();
}

void cardInventory::setReadOnly(bool r)
{
    this->ro = r;
}

void cardInventory::setViewable(bool p, bool setActive)
{
    if (this->gS->mW)
    {
        if (this->viewable && !p)
        {
            int tabpos = this->gS->mW->ui->sideBarTabWidget->indexOf(this->parentWidget());
            this->tabtext = this->gS->mW->ui->sideBarTabWidget->tabText(tabpos);
            this->gS->mW->ui->sideBarTabWidget->removeTab(tabpos);
        }
        else if (!this->viewable && p)
        {
            this->gS->mW->ui->sideBarTabWidget->addTab(this->parentWidget(), this->tabtext);
            if (setActive)
            {
                int tabpos = this->gS->mW->ui->sideBarTabWidget->indexOf(this->parentWidget());
                this->gS->mW->ui->sideBarTabWidget->setCurrentIndex(tabpos);
            }
            this->updateDisabledCards();
        }
        this->viewable = p;
        this->gS->mW->reorderInventoryTabs();
    }
}

bool cardInventory::isViewable()
{
    return this->viewable;
}

void cardInventory::reinitialize()
{
    this->contents.clear();
    if (this->parent())
    {
        this->scene()->clear();
        int card_width = 124;
        int card_height = 82;
        if (mainWindow::getConfigVariable("hiresMode").toInt())
        {
            card_width = 161;
            card_height = 106;
        }
        QPixmap* px = new QPixmap(QSize(3 * 5 + card_width * 2 + 25, 3 * card_height + 4 * 5));
        px->fill(Qt::transparent);
        this->scene()->addPixmap(*px)->setPos(0, 0);
        this->setMouseTracking(true);
        this->setBackgroundBrush(QPixmap(getPrimaryPath("img/cardinventory_bkg.png")));
        this->setAlignment(Qt::AlignCenter);
    }
    this->tab = nullptr;
    this->player = -1;
    this->viewable = true;
    this->choiceMode = false;
    this->setHucksterChoiceMode(false);
    this->rejectNextMouseEnterEvent = false;
    this->trash = false;
    this->ro = true;
    this->redraw();
}

void cardInventory::clear()
{
    for (auto& crd : this->contents)
    {
        crd->removeFromScene(this->scene());
        crd->deleteCardLater();
    }
    this->tabtext = "";
    this->contents.clear();
    this->setHucksterChoiceMode(false);
    this->redraw();
}

QList<card*> cardInventory::cardsMarkedForTrash()
{
    QList<card*> res;
    for (auto crd : this->contents)
    {
        if (crd->isMarkedForTrash()) res.append(crd);
    }
    return res;
}

CardList cardInventory::getAllCards() const
{
    CardList result;
    for(auto* card: getAllCardsInternal()) result.append(card);
    return result;
}

QList<card*> cardInventory::getAllCardsInternal() const
{
    return this->contents;
}

card* cardInventory::removeCard(int uID)
{
    if (this->gS) this->gS->sendGameEvent(QStringList() << "removeCard" << QString::number(this->getInventoryID()) << QString::number(uID));
    if (this->placeCard && this->placeCard->getUID() == uID) this->placeCard = nullptr;
    for (auto crd : this->contents)
    {
        if (crd->getUID() == uID)
        {
            return removeCardByPtr(crd);
        }
    }
    return nullptr;
}

void cardInventory::setHucksterChoiceMode(bool h)
{
    if (!h && this->hucksterChoiceMode && this->gS) this->gS->huckster->endHucksterChoice();
    if (this->gS && this->gS->menu && this->gS->getCurrentPlayer() == this->player)
    {
        if (h && !this->hucksterChoiceMode)
        {
            this->gS->menu->showInformation(tr("Choose which item you want to trade with the Huckster"), -1);
        }
        else if (!h && this->hucksterChoiceMode)
        {
            this->gS->menu->hideInformation();
        }
    }
    this->hucksterChoiceMode = h;
    if (this->gS && this->gS->mW)
    {
        if (this->gS->getCurrentPlayer() >= 1 && this->gS->getCurrentPlayer() <= 4)
            this->gS->mW->updateEndTurnButtonState();
    }
}

void cardInventory::removeCardsMarkedForTrash(cardInventory* trash)
{
    QList<card*> rem;
    for (auto crd : this->contents)
    {
        if (crd->isMarkedForTrash()) rem.append(crd);
    }
    for (auto crd : rem)
    {
        this->removeCard(crd->getUID());
        if (this->gS && this->player >= 1 && this->player <= 4)
        {
            this->gS->writeToGameLog(tr("%1 discarded %2 at the end of the turn").arg(mainWindow::getPlayerNickNames().at(this->player - 1)), {}, true, false);
        }
        crd->setMarkedForTrash(false);
        trash->addCard(crd);
    }
}

void cardInventory::cardClicked()
{
    card* sender = static_cast<card*>(QObject::sender());
    if (this->choiceMode)
    {
        this->choice = sender;
        this->choiceMode = false;
        return;
    }
    if (this->gS->mW) this->gS->mW->displayCardOnSideBar(sender->getID());
    if (!this->ro)
    {
        if (this->placeCard != nullptr)
        {
            emit this->stopTileChoice();
            this->placeCard->setSelectedForPlacement(false);
            this->placeCard->redraw();
            if (this->placeCard == sender)
            {
                this->placeCard = nullptr;
                return;
            }
            else
            {
                this->placeCard = nullptr;
            }
        }

        if (this->hucksterChoiceMode)
        {
            for (card* crd : gS->huckster->getAllCardsInternal())
            {
                if (crd->isMarkedForHuckster())
                {
                    this->removeCard(sender->getUID());
                    gS->huckster->removeCard(crd->getUID());
                    crd->setMarkedForHuckster(false);
                    this->addCard(crd);
                    gS->huckster->addCard(sender);
                    break;
                }
            }
            this->setHucksterChoiceMode(false);
        }
        else if (this->placeCard == nullptr && (gS->isCardPlacementEnabled() || gS->unlimitedMoves))
        {
            QList<QPoint> availableCoords;
#ifdef BAZAAR_DEBUG
            gameState::scriptExecInfo.append(sender->getName() + " (getAvailableTiles)");
#endif
            QVariantList tmpL = gS->scriptEnv->executeScript(sender->getScriptCode(), "getAvailableTiles", sender, QStringList() << sender->getID() << QString::number(gS->getCurrentPlayer()));
#ifdef BAZAAR_DEBUG
            gameState::scriptExecInfo.pop_back();
#endif
            for (auto& var : tmpL)
            {
                availableCoords.append(var.toPoint());
            }
            if (!availableCoords.isEmpty())
            {
                this->placeCard = sender;
                sender->setSelectedForPlacement(true);
                sender->redraw();
                if (gS->gameBoard->placeCard(availableCoords, sender, false, false))
                {
                    this->rejectNextMouseEnterEvent = true;
                    this->placeCard = nullptr;
                    this->gS->setCardPlacementEnabled(false);
                    this->gS->setPlayerPlacedCardOnBoard(true);
                    emit this->changed();
                }
                else
                {
                    this->placeCard = nullptr;
                    sender->setSelectedForPlacement(false);
                    sender->redraw();
                }
            }
        }
    }
}

void cardInventory::cardDragged()
{
    card* sender = static_cast<card*>(QObject::sender());
    if (this->choiceMode)
    {
        return;
    }
    if (this->gS->mW) this->gS->mW->displayCardOnSideBar(sender->getID());
    if (!this->ro)
    {
        if (this->placeCard != nullptr)
        {
            emit this->stopTileChoice();
            this->placeCard->setSelectedForPlacement(false);
            this->placeCard->redraw();
            if (this->placeCard == sender)
            {
                this->placeCard = nullptr;
                return;
            }
            else
            {
                this->placeCard = nullptr;
            }
        }

        if (this->placeCard == nullptr && (gS->isCardPlacementEnabled() || gS->unlimitedMoves))
        {
            QList<QPoint> availableCoords;
#ifdef BAZAAR_DEBUG
            gameState::scriptExecInfo.append(sender->getName() + " (getAvailableTiles)");
#endif
            QVariantList tmpL = gS->scriptEnv->executeScript(sender->getScriptCode(), "getAvailableTiles", sender, QStringList() << sender->getID() << QString::number(gS->getCurrentPlayer()));
#ifdef BAZAAR_DEBUG
            gameState::scriptExecInfo.pop_back();
#endif
            for (auto& var : tmpL)
            {
                availableCoords.append(var.toPoint());
            }
            if (!availableCoords.isEmpty())
            {
                this->placeCard = sender;
                sender->setSelectedForPlacement(true);
                sender->redraw();
                if (gS->gameBoard->placeCard(availableCoords, sender, false, false, this))
                {
                    this->placeCard = nullptr;
                    this->gS->setCardPlacementEnabled(false);
                    this->gS->setPlayerPlacedCardOnBoard(true);
                    emit this->changed();
                }
                else
                {
                    this->placeCard = nullptr;
                    sender->setSelectedForPlacement(false);
                    sender->redraw();
                }
            }
        }
    }
}

void cardInventory::cardRightClicked()
{
    card* sender = static_cast<card*>(QObject::sender());
    if (!this->isHuckster)
    {
        if (!this->ro)
        {
            sender->setMarkedForTrash(!sender->isMarkedForTrash());
            sender->redraw();
            if (this->gS->mW) this->gS->mW->updateEndTurnButtonState();
        }
    }
    else
    {
        if (sender->isMarkedForHuckster())
        {
            sender->setMarkedForHuckster(false);
            sender->redraw();
            this->gS->p1Inventory->setHucksterChoiceMode(false);
            this->gS->p2Inventory->setHucksterChoiceMode(false);
            this->gS->p3Inventory->setHucksterChoiceMode(false);
            this->gS->p4Inventory->setHucksterChoiceMode(false);
            return;
        }
        this->gS->p1Inventory->setHucksterChoiceMode(false);
        this->gS->p2Inventory->setHucksterChoiceMode(false);
        this->gS->p3Inventory->setHucksterChoiceMode(false);
        this->gS->p4Inventory->setHucksterChoiceMode(false);
        int cards = 0;
        switch (this->gS->getCurrentPlayer())
        {
            case 1:
                cards = this->gS->p1Inventory->contents.size();
                break;
            case 2:
                cards = this->gS->p2Inventory->contents.size();
                break;
            case 3:
                cards = this->gS->p3Inventory->contents.size();
                break;
            case 4:
                cards = this->gS->p4Inventory->contents.size();
                break;
            default:
                mainWindow::reportError(tr("Invalid player in cardRightClicked(...)"));
                return;
        }
        auto idx = this->contents.indexOf(sender);
        if (idx == -1)
        {
            mainWindow::reportError(tr("Invalid card in cardRightClicked(...)"));
            return;
        }
        if (idx >= cards - 1)
        {
            sender->setMarkedForHuckster(true);
            for (card* crd : this->contents)
            {
                if (crd != sender)
                {
                    crd->setMarkedForHuckster(false);
                }
                crd->redraw();
            }
            switch (this->gS->getCurrentPlayer())
            {
                case 1:
                    this->gS->p1Inventory->setHucksterChoiceMode(true);
                    break;
                case 2:
                    this->gS->p2Inventory->setHucksterChoiceMode(true);
                    break;
                case 3:
                    this->gS->p3Inventory->setHucksterChoiceMode(true);
                    break;
                case 4:
                    this->gS->p4Inventory->setHucksterChoiceMode(true);
                    break;
                default:
                    mainWindow::reportError(tr("Invalid player in cardRightClicked(...)"));
                    return;
            }
            if (this->gS->mW) this->gS->mW->switchToPlayerInventory(this->gS->getCurrentPlayer());
        }
    }
}

void cardInventory::cardMouseEntered()
{
    if (!rejectNextMouseEnterEvent)
    {
        card* sender = static_cast<card*>(QObject::sender());
        sender->setHighlightedInInventory(true);
        if (this->gS->mW) this->gS->mW->displayCardOnSideBar(static_cast<card*>(QObject::sender())->getID());
    }
    else
    {
        rejectNextMouseEnterEvent = false;
    }
}

void cardInventory::setHuckster(bool huckster)
{
    this->isHuckster = huckster;
    if (huckster) this->tabtext = tr("Huckster");
}

bool cardInventory::hasCard(const QString& name) const
{
    for (const card* c : contents)
    {
        if (c->getID() == name) return true;
    }
    return false;
}

bool cardInventory::hasPlayableCard()
{
    this->updateDisabledCards();
    for (const card* crd : this->contents)
    {
        if (!crd->isDisabledInInventory()) return true;
    }
    return false;
}

bool cardInventory::isHucksterChoiceModeActive() const
{
    return this->hucksterChoiceMode;
}

void cardInventory::endHucksterChoice()
{
    for (const auto c : this->contents)
    {
        c->setMarkedForHuckster(false);
        c->redraw();
    }
}

void cardInventory::cardMouseLeft()
{
    card* sender = static_cast<card*>(QObject::sender());
    sender->setHighlightedInInventory(false);
    if (this->gS->mW) this->gS->mW->displayCardOnSideBar("", true);
}

int cardInventory::getInventoryID() const
{
    if (this->player >= 1 && this->player <= 4) return this->player;
    if (this->trash) return -1;
    if (this->isHuckster) return -2;
    if (this->isStorage) return -3;
    return 0;
}

void cardInventory::moveCardsToTrash()
{
    for (auto crd : this->contents)
    {
        if (crd->isMarkedForTrash())
        {
            this->contents.removeOne(crd);
            disconnect(crd, 0, this, 0);
            this->gS->trash->addCard(crd);
        }
    }
    this->redraw();
}

void cardInventory::setPlayer(int player)
{
    this->player = player;
}

void cardInventory::cancelChoice()
{
    this->choiceMode = false;
}

void cardInventory::orderCardsAlpha()
{
    if (!this->isHuckster)
    {
        std::sort(this->contents.begin(), this->contents.end(), [](card * crd1, card * crd2)
        {
            return crd1->getName() < crd2->getName();
        });
    }
}

void cardInventory::redraw()
{
    if (!this->gS || !this->gS->mW) return;

    this->updateDisabledCards();

    int r = 0;
    int c = 0;
    for (auto& crd : this->contents)
    {
        crd->removeFromScene(this->scene());
    }
    int card_width = 124;
    int card_height = 82;
    double card_rows = 3.0;
    if (mainWindow::getConfigVariable("hiresMode").toInt())
    {
        card_width = 161;
        card_height = 106;
    }
    for (int i = 0; i < this->contents.size(); i++)
    {
        this->contents[i]->draw(this->scene(), DrawingType::Inventory, QPointF(10 + r * 5 + r * card_width, 5 + c * 5 + c * card_height));
        r++;
        if (r == 2)
        {
            r = 0;
            c++;
        }
    }
    this->scene()->setSceneRect(0, 0, 8 + 2 * 5 + 2 * card_width + 20, std::max(5 + card_rows * 5 + card_rows * card_height, std::ceil(this->contents.size() / 2.0) * 5 + std::ceil(this->contents.size() / 2.0) * card_height + 5));
}
