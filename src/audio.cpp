/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "globals.h"
#include <QFile>
#include <QFileInfo>
#ifndef Q_OS_WASM
#include <QMediaPlayer>
#include <QAudioOutput>
#include "qmediaplaylist.h"
#endif
#include <QTimer>
#include "audio.h"
#include "mainwindow.h"

#ifndef Q_OS_WASM

musicPlayer::musicPlayer()
{
    this->player = new QMediaPlayer;
    this->player->setAudioOutput(new QAudioOutput);
    this->player->audioOutput()->setVolume(50);
    this->maxRand = -1;
    this->nextTrackTimer = new QTimer;
    connect(this->nextTrackTimer, &QTimer::timeout, this, [this]()
    {
        this->nextTrackTimer->stop();
        disconnect(this->player, &QMediaPlayer::playbackStateChanged, nullptr, nullptr);
        if (this->playList->mediaCount()) this->playList->setCurrentIndex(this->savedIdx % this->playList->mediaCount());
        this->player->setSource(this->playList->currentMedia());
        this->player->play();
        this->setupSignals();
    });
    this->setupSignals();
}

void musicPlayer::loadPlayList(const QString& filename)
{
    QFile f(filename);
    if (f.open(QFile::ReadOnly))
    {
        QMediaPlaylist* pl = new QMediaPlaylist;
        QTextStream stream(&f);
        stream.readLine(); //Skip the first line
        while (!stream.atEnd())
        {
            QString line = stream.readLine();
            if (!line.trimmed().isEmpty())
            {
#ifdef Q_OS_ANDROID
                pl->addMedia(QFileInfo(getPrimaryPath("music/" + line.trimmed())).absoluteFilePath());
#else
                pl->addMedia(QUrl::fromLocalFile(QFileInfo(getPrimaryPath("music/" + line.trimmed())).absoluteFilePath()));
#endif
            }
        }
        pl->setCurrentIndex(0);
        pl->setPlaybackMode(QMediaPlaylist::Loop);
        this->playList = pl;
        f.close();
    }
    else
    {
        mainWindow::reportError(tr("Error: cannot open playlist (%1)").arg(filename));
    }
}

void musicPlayer::shuffle()
{
    if (this->playList) this->playList->shuffle();
}

void musicPlayer::setupSignals()
{
    disconnect(this->player, &QMediaPlayer::playbackStateChanged, nullptr, nullptr);
    connect(this->player, &QMediaPlayer::playbackStateChanged, this, [this]()
    {
        if(this->player->playbackState() != QMediaPlayer::StoppedState) return;
        if (this->maxRand != -1)
        {
            this->savedIdx = this->playList->currentIndex();
            if (this->savedIdx < 0) this->savedIdx = 0;
            this->player->setSource({});
            this->nextTrackTimer->setSingleShot(true);
            if (maxRand > 0)
            {
                this->nextTrackTimer->setInterval(rand() % maxRand);
            }
            else
            {
                this->nextTrackTimer->setInterval(0);
            }
            this->nextTrackTimer->start();
        }
    });
}

void musicPlayer::setMaxRandomSilence(int max)
{
    this->maxRand = max;
}

void musicPlayer::start()
{
    this->player->setSource(this->playList->currentMedia());
    this->player->play();
    this->setupSignals();
}

void musicPlayer::stop()
{
    this->nextTrackTimer->stop();
    this->player->stop();
    this->player->setSource({});
    disconnect(this->player, &QMediaPlayer::playbackStateChanged, nullptr, nullptr);
}

musicPlayer::~musicPlayer()
{
    this->player->stop();
    this->player->setSource({});
    disconnect(this->player, &QMediaPlayer::playbackStateChanged, nullptr, nullptr);
    this->player->deleteLater();
    this->nextTrackTimer->deleteLater();
}

#else //All stubs since no QtMultimedia support

musicPlayer::musicPlayer()
{
}

void musicPlayer::loadPlayList(const QString&)
{
}

void musicPlayer::shuffle()
{
}

void musicPlayer::setupSignals()
{
}

void musicPlayer::setMaxRandomSilence(int)
{
}

void musicPlayer::start()
{
}

void musicPlayer::stop()
{
}

musicPlayer::~musicPlayer()
{
}

#endif
