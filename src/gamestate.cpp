/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <set>
#include <QTime>
#include <QTextStream>
#include <QMessageBox>
#include <QJSEngine>
#include <QJSValue>
#include <QQmlEngine>
#include "gamestate.h"
#include "graphicsviewmenu.h"
#include "board.h"
#include "cardinventory.h"
#include "card.h"
#include "mainwindow.h"
#include "utils.h"
#include "audio.h"
#include "globals.h"
#include "ui_mainwindow.h"
#include "random.h"

gameState::gameState(mainWindow* mW)
{
    this->init(mW, nullptr);
}

gameState::gameState(gameState* state)
{
    this->init(nullptr, state);
}

gameState::~gameState()
{
    if (this->mW && this == this->mW->state) delete this->scriptEnv;
    this->menu->deleteLater();
    if (!this->mW || (this->mW->ui->gameBoard != this->gameBoard))
    {
        this->gameBoard->deleteLater();
    }
    if (!this->mW || (this->mW->ui->storage != this->storage))
    {
        this->storage->deleteLater();
    }
    if (!this->mW || (this->mW->ui->p1Inventory != this->p1Inventory))
    {
        this->p1Inventory->deleteLater();
    }
    if (!this->mW || (this->mW->ui->p2Inventory != this->p2Inventory))
    {
        this->p2Inventory->deleteLater();
    }
    if (!this->mW || (this->mW->ui->p3Inventory != this->p3Inventory))
    {
        this->p3Inventory->deleteLater();
    }
    if (!this->mW || (this->mW->ui->p4Inventory != this->p4Inventory))
    {
        this->p4Inventory->deleteLater();
    }
    if (!this->mW || (this->mW->ui->huckster != this->huckster))
    {
        this->huckster->deleteLater();
    }
    if (!this->mW || (this->mW->ui->mainInventory != this->mainInventory))
    {
        this->mainInventory->deleteLater();
    }
    if (!this->mW || (this->mW->ui->trash != this->trash))
    {
        this->trash->deleteLater();
    }
}

bool gameState::loadGame(const fileFormat::saveFile& f, const QString& script, const bool load_from_ui)
{
    const bool load_from_save = f.itemCount() > 0;

    this->clear();

    if (!load_from_save || load_from_ui)
    {
        this->p1AI = this->mW->ui->p1TypeComboBox->currentIndex() == 1;
        this->p2AI = this->mW->ui->p2TypeComboBox->currentIndex() == 1;
        this->p3AI = this->mW->ui->p3TypeComboBox->currentIndex() == 1;
        this->p4AI = this->mW->ui->p4TypeComboBox->currentIndex() == 1;
    }
    else
    {
        this->p1AI = (this->mW->ui->p1TypeComboBox->currentIndex() == 1) || f.getItemByKey("aiPlayers").getAttributeAsString("u0").toInt();
        this->p2AI = (this->mW->ui->p2TypeComboBox->currentIndex() == 1) || f.getItemByKey("aiPlayers").getAttributeAsString("u1").toInt();
        this->p3AI = (this->mW->ui->p3TypeComboBox->currentIndex() == 1) || f.getItemByKey("aiPlayers").getAttributeAsString("u2").toInt();
        this->p4AI = (this->mW->ui->p4TypeComboBox->currentIndex() == 1) || f.getItemByKey("aiPlayers").getAttributeAsString("u3").toInt();
    }

    this->p1Network = this->mW->ui->p1TypeComboBox->currentIndex() > 1;
    this->p2Network = this->mW->ui->p2TypeComboBox->currentIndex() > 1;
    this->p3Network = this->mW->ui->p3TypeComboBox->currentIndex() > 1;
    this->p4Network = this->mW->ui->p4TypeComboBox->currentIndex() > 1;

    this->bkgScripts.clear();
    if (!script.isEmpty())
    {
        if (!this->addBkgScript(-1, script, script)) return false;
    }
    if (load_from_save)
    {
        for (const fileFormat::saveFileItem& it : f.getItems())
        {
            if (it.getKeyAsString() == "bkgScript")
            {
                if (!this->addBkgScript(it.getAttributeAsString("player").toInt(), getPrimaryPath(it.getAttributeAsString("fname")), it.getAttributeAsString("fname"))) return false;
            }
        }
    }

    if (!load_from_save)
    {
        this->silverActionsDisabledForTurn = false;
        this->playerPlacedCardOnBoard = false;
    }
    else
    {
        this->silverActionsDisabledForTurn = f.getItemByKey("silverActionsDisabledForTurn").getValueAsString().toInt();
        this->playerPlacedCardOnBoard = f.getItemByKey("playerPlacedCardOnBoard").getValueAsString().toInt();
    }
    this->trash->setTrash(true);
    this->storage->setStorage(true);

    if (!load_from_save)
    {
        QString boardn = this->mW->getLobbySelectedBoard();
        this->gameBoard->drawBackground(this->mW->boards[boardn].fileName(),
                                        this->mW->boards[boardn].readValue("background"),
                                        this->mW->boards[boardn].readValue("tilemap"),
                                        this->mW->boards[boardn].readValue("graphicsview_background"),
                                        this->mW->boards[boardn].readValue("whiteTile"),
                                        this->mW->boards[boardn].readValue("yellowTile"),
                                        this->mW->boards[boardn].readValue("blueTile"),
                                        this->mW->boards[boardn].readValue("greenTile"),
                                        this->mW->boards[boardn].readValue("disabledTile"),
                                        this->mW->boards[boardn].readValue("orangeTile"),
                                        this->mW->boards[boardn].readValue("magentaTile"),
                                        this->mW->boards[boardn].hasKey("playerTileFilenamePrefix") ? this->mW->boards[boardn].readValue("playerTileFilenamePrefix") : "",
                                        this->mW->boards[boardn].hasKey("tileOverlayFilenamePrefix") ? this->mW->boards[boardn].readValue("tileOverlayFilenamePrefix") : "",
                                        this->mW->boards[boardn].hasKey("coords") ? this->mW->boards[boardn].readValue("coords") : ""
                                       );
    }

    this->currentPlayerDrawnCard = load_from_save ? f.getItemByKey("currentPlayerDrawnCard").getValueAsString().toInt() : true;
    this->nextPlayerOverride = load_from_save ? f.getItemByKey("nextPlayerOverride").getValueAsString().toInt() : -1;
    this->cardDrawDisabledForTurn = load_from_save ? f.getItemByKey("cardDrawDisabledForTurn").getValueAsString().toInt() : false;
    this->alreadyOver = load_from_save ? f.getItemByKey("gameFinished").getValueAsString().toInt() : false;
    this->victoryCheckEnabled = load_from_save ? f.getItemByKey("victoryCheckEnabled").getValueAsString().toInt() : true;
    this->cardPlacementEnabled = load_from_save ? f.getItemByKey("cardPlacementEnabled").getValueAsString().toInt() : true;

    this->p1Inventory->setReadOnly(true);
    this->p2Inventory->setReadOnly(true);
    this->p3Inventory->setReadOnly(true);
    this->p4Inventory->setReadOnly(true);
    this->p1Inventory->setViewable(false);
    this->p2Inventory->setViewable(false);
    this->p3Inventory->setViewable(false);
    this->p4Inventory->setViewable(false);

    if (!load_from_save)
    {
        this->openHands = this->mW->ui->openHandsCheckBox->isChecked();
    }
    else
    {
        this->openHands = f.getItemByKey("openHands").getValueAsString().toInt();
    }

    int gMode = !load_from_save ? this->mW->ui->gameModeComboBox->currentIndex() : mainWindow::gameModeToIdx(f.getItemByKey("gameMode").getValueAsString().toInt());
    if (gMode == 0)
    {
        this->gameMode = 0;
        this->playerTurnOrder = QList<int>() << 1 << 2;
        if (load_from_save)
        {
            this->setCurrentPlayer(f.getItemByKey("currentPlayer").getValueAsString().toInt());
        }
        else
        {
            this->setCurrentPlayer(getPlayerTurnOrder().constFirst());
        }
        this->p1Inventory->setReadOnly(!this->isLocalUser(1) || this->getCurrentPlayer() != 1);
        this->p1Inventory->setViewable(this->openHands || (this->mW && this->mW->singleLocalUser() == 1) || this->isObserver() || (this->isLocalUser(1) && this->getCurrentPlayer() == 1));
        this->p2Inventory->setReadOnly(!this->isLocalUser(2) || this->getCurrentPlayer() != 2);
        this->p2Inventory->setViewable(this->openHands || (this->mW && this->mW->singleLocalUser() == 2) || this->isObserver() || (this->isLocalUser(2) && this->getCurrentPlayer() == 2));
    }
    else if (gMode == 1)
    {
        this->gameMode = 4;
        this->playerTurnOrder = QList<int>() << 1 << 3;
        if (load_from_save)
        {
            this->setCurrentPlayer(f.getItemByKey("currentPlayer").getValueAsString().toInt());
        }
        else
        {
            this->setCurrentPlayer(getPlayerTurnOrder().constFirst());
        }
        this->p1Inventory->setReadOnly(!this->isLocalUser(1) || this->getCurrentPlayer() != 1);
        this->p1Inventory->setViewable(this->openHands || (this->mW && this->mW->singleLocalUser() == 1) || this->isObserver() || (this->isLocalUser(1) && this->getCurrentPlayer() == 1));
        this->p3Inventory->setReadOnly(!this->isLocalUser(3) || this->getCurrentPlayer() != 3);
        this->p3Inventory->setViewable(this->openHands || (this->mW && this->mW->singleLocalUser() == 3) || this->isObserver() || (this->isLocalUser(3) && this->getCurrentPlayer() == 3));
    }
    else if (gMode == 2)
    {
        this->gameMode = 1;
        this->playerTurnOrder = QList<int>() << 1 << 2 << 3 << 4;
        if (load_from_save)
        {
            this->setCurrentPlayer(f.getItemByKey("currentPlayer").getValueAsString().toInt());
        }
        else
        {
            this->setCurrentPlayer(getPlayerTurnOrder().constFirst());
        }
        this->p1Inventory->setReadOnly(!this->isLocalUser(1) || this->getCurrentPlayer() != 1);
        this->p1Inventory->setViewable(this->openHands || (this->mW && this->mW->singleLocalUser() == 1) || this->isObserver() || (this->isLocalUser(1) && this->getCurrentPlayer() == 1));
        this->p2Inventory->setReadOnly(!this->isLocalUser(2) || this->getCurrentPlayer() != 2);
        this->p2Inventory->setViewable(this->openHands || (this->mW && this->mW->singleLocalUser() == 2) || this->isObserver() || (this->isLocalUser(2) && this->getCurrentPlayer() == 2));
        this->p3Inventory->setReadOnly(!this->isLocalUser(3) || this->getCurrentPlayer() != 3);
        this->p3Inventory->setViewable(this->openHands || (this->mW && this->mW->singleLocalUser() == 3) || this->isObserver() || (this->isLocalUser(3) && this->getCurrentPlayer() == 3));
        this->p4Inventory->setReadOnly(!this->isLocalUser(4) || this->getCurrentPlayer() != 4);
        this->p4Inventory->setViewable(this->openHands || (this->mW && this->mW->singleLocalUser() == 4) || this->isObserver() || (this->isLocalUser(4) && this->getCurrentPlayer() == 4));
        if (this->p1Inventory->isViewable()) this->p3Inventory->setViewable(true);
        if (this->p3Inventory->isViewable()) this->p1Inventory->setViewable(true);
        if (this->p2Inventory->isViewable()) this->p4Inventory->setViewable(true);
        if (this->p4Inventory->isViewable()) this->p2Inventory->setViewable(true);
    }
    else if (gMode == 3)
    {
        this->gameMode = 2;
        this->playerTurnOrder = QList<int>() << 1 << 2 << 3;
        if (load_from_save)
        {
            this->setCurrentPlayer(f.getItemByKey("currentPlayer").getValueAsString().toInt());
        }
        else
        {
            this->setCurrentPlayer(getPlayerTurnOrder().constFirst());
        }
        this->p1Inventory->setReadOnly(!this->isLocalUser(1) || this->getCurrentPlayer() != 1);
        this->p1Inventory->setViewable(this->openHands || (this->mW && this->mW->singleLocalUser() == 1) || this->isObserver() || (this->isLocalUser(1) && this->getCurrentPlayer() == 1));
        this->p3Inventory->setReadOnly(!this->isLocalUser(3) || this->getCurrentPlayer() != 3);
        this->p3Inventory->setViewable(this->openHands || (this->mW && this->mW->singleLocalUser() == 3) || this->isObserver() || (this->isLocalUser(3) && this->getCurrentPlayer() == 3));
    }
    else if (gMode == 4)
    {
        this->gameMode = 3;
        this->playerTurnOrder = QList<int>() << 1 << 2 << 3 << 4;
        if (load_from_save)
        {
            this->setCurrentPlayer(f.getItemByKey("currentPlayer").getValueAsString().toInt());
        }
        else
        {
            this->setCurrentPlayer(getPlayerTurnOrder().constFirst());
        }
        this->p1Inventory->setReadOnly(!this->isLocalUser(1) || this->getCurrentPlayer() != 1);
        this->p1Inventory->setViewable(this->openHands || (this->mW && this->mW->singleLocalUser() == 1) || this->isObserver() || (this->isLocalUser(1) && this->getCurrentPlayer() == 1));
        this->p2Inventory->setReadOnly(!this->isLocalUser(2) || this->getCurrentPlayer() != 2);
        this->p2Inventory->setViewable(this->openHands || (this->mW && this->mW->singleLocalUser() == 2) || this->isObserver() || (this->isLocalUser(2) && this->getCurrentPlayer() == 2));
        this->p3Inventory->setReadOnly(!this->isLocalUser(3) || this->getCurrentPlayer() != 3);
        this->p3Inventory->setViewable(this->openHands || (this->mW && this->mW->singleLocalUser() == 3) || this->isObserver() || (this->isLocalUser(3) && this->getCurrentPlayer() == 3));
        this->p4Inventory->setReadOnly(!this->isLocalUser(4) || this->getCurrentPlayer() != 4);
        this->p4Inventory->setViewable(this->openHands || (this->mW && this->mW->singleLocalUser() == 4) || this->isObserver() || (this->isLocalUser(4) && this->getCurrentPlayer() == 4));
    }
    if (this->mW) this->mW->switchToPlayerInventory(this->getCurrentPlayer());
    this->mainInventory->setReadOnly(true);
    this->mainInventory->setViewable(false);

    this->logs[-1] = QStringList{};
    for (auto p : getPlayerTurnOrder())
    {
        this->logs[p] = QStringList{};
    }

    if (!load_from_save)
    {
        this->writeToGameLog(tr("Turn started<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Turn of %1</b><br>").arg(mainWindow::getPlayerNickNames().at(this->currentPlayer - 1)), {1, 2, 3, 4}, true, false);
    }

    if (!load_from_save)
    {
        auto cardList = mainWindow::getDeckCardList(mainWindow::startingDeck);
        for (const auto& p : cardList.first)
        {
            for (int i = 0; i < p.second; i++)
            {
                card* crd = new card(p.first, -1, this);
                this->mainInventory->addCard(crd);
            }
        }
        for (int i = 0; i < 4; i++)
        {
            if (getPlayerTurnOrder().contains(1))
            {
                this->drawCard(1, false, true);
            }
            if (getPlayerTurnOrder().contains(2))
            {
                this->drawCard(2, false, true);
            }
            if (getPlayerTurnOrder().contains(3))
            {
                this->drawCard(3, false, true);
            }
            if (getPlayerTurnOrder().contains(4))
            {
                this->drawCard(4, false, true);
            }
        }
    }

    this->huckster->setHuckster(true);
    if (!load_from_save)
    {
        this->hucksterEnabled = this->mW->ui->useHucksterCheckBox->isChecked();
        if (this->hucksterEnabled)
        {
            auto cardList = mainWindow::getDeckCardList(mainWindow::startingDeck);
            int allHucksterCardsAdded = 0;
            for (int i = 0; i < this->mW->ui->hucksterCardsComboBox->currentText().toInt(); i++)
            {
                if (i < cardList.second.length())
                {
                    for (int j = 0; j < cardList.second[i].second; ++j)
                    {
                        if (allHucksterCardsAdded < this->mW->ui->hucksterCardsComboBox->currentText().toInt())
                        {
                            card* crd = new card(cardList.second[i].first, -1, this);
                            this->huckster->addCard(crd);
                            ++allHucksterCardsAdded;
                        }
                        else break;
                    }
                }
                else break;
            }
        }
    }
    else
    {
        this->hucksterEnabled = f.getItemByKey("useHuckster").getValueAsString().toInt();
    }
    this->huckster->setViewable(this->hucksterEnabled);

    if (this->getConfigVariable("doNotChangeOnlinePersp").toInt() || this->getConfigVariable("neverChangePersp").toInt())
    {
        bool found = false;
        for (const auto p : this->getPlayerTurnOrder())
        {
            if (this->isLocalUser(p))
            {
                this->gameBoard->rotateBoardToPlayer(p, true);
                found = true;
                break;
            }
        }
        if (!found)
        {
            this->gameBoard->rotateBoardToPlayer(this->currentPlayer, true);
        }
    }
    else
    {
        this->gameBoard->rotateBoardToPlayer(this->currentPlayer, true);
    }

    if (load_from_save)
    {
        for (const fileFormat::saveFileItem& it : f.getItems())
        {
            if (it.getKeyAsString() == "logMsg")
            {
                this->logs[it.getAttributeAsString("log").toInt()].append(it.getValueAsString());
            }
            if (it.getKeyAsString() == "scriptVar")
            {
                this->scriptVarMap[it.getAttributeAsString("name")] = it.getAttributeAsString("value");
            }
        }

        card::globalUID = f.getItemByKey("cardGlobalUID").getValueAsString().toInt();

        this->gameBoard->load(f);
        this->mainInventory->load(f);
        this->trash->load(f);
        this->huckster->load(f);
        this->storage->load(f);

        this->p1Inventory->load(f);
        this->p2Inventory->load(f);
        this->p3Inventory->load(f);
        this->p4Inventory->load(f);
    }

    return true;
}

fileFormat::saveFile gameState::save(bool forOOSCheck) const
{
    fileFormat::saveFile saved;

    for (auto& s : this->bkgScripts)
    {
        QStringMap m;
        m["player"] = QString::number(s.first.first);
        m["fname"] = s.first.second;
        saved.addItem("bkgScript", QString(), m);
    }
    for (auto& v : this->scriptVarMap.keys())
    {
        QStringMap m;
        m["name"] = v;
        m["value"] = this->scriptVarMap[v];
        saved.addItem("scriptVar", QString(), m);
    }
    QStringMap aiPlayers;
    aiPlayers["u0"] = QString::number(this->p1AI);
    aiPlayers["u1"] = QString::number(this->p2AI);
    aiPlayers["u2"] = QString::number(this->p3AI);
    aiPlayers["u3"] = QString::number(this->p4AI);
    saved.addItem("aiPlayers", QString(), aiPlayers);
    if (!forOOSCheck)
    {
        for (auto& l : this->logs)
        {
            for (auto& str : l.second)
            {
                QStringMap attr;
                attr["log"] = QString::number(l.first);
                saved.addItem("logMsg", str, attr);
            }
        }
        saved.addItems(this->p1Inventory->save());
        saved.addItems(this->p2Inventory->save());
        saved.addItems(this->p3Inventory->save());
        saved.addItems(this->p4Inventory->save());
        saved.addItems(this->huckster->save());
        saved.addItems(this->trash->save());
        saved.addItems(this->mainInventory->save());
        saved.addItems(this->gameBoard->save());
        saved.addItem("gameFinished", QString::number(this->alreadyOver));
    }
    saved.addItem("cardPlacementEnabled", QString::number(this->cardPlacementEnabled));
    saved.addItem("playerPlacedCardOnBoard", QString::number(this->playerPlacedCardOnBoard));
    saved.addItem("gameMode", QString::number(this->gameMode));
    saved.addItem("useHuckster", QString::number(this->hucksterEnabled));
    saved.addItem("openHands", QString::number(this->openHands));
    saved.addItem("nextPlayerOverride", QString::number(this->nextPlayerOverride));
    saved.addItem("victoryCheckEnabled", QString::number(this->victoryCheckEnabled));
    saved.addItem("currentPlayer", QString::number(this->currentPlayer));
    saved.addItem("currentPlayerDrawnCard", QString::number(this->currentPlayerDrawnCard));
    saved.addItem("silverActionsDisabledForTurn", QString::number(this->silverActionsDisabledForTurn));
    saved.addItem("cardDrawDisabledForTurn", QString::number(this->cardDrawDisabledForTurn));

    return saved;
}

void gameState::drawCard(int playerNum, bool cDrawnCard, bool doNotShow, bool soundEffect, bool soundEffectForEveryone, QString logMsgColor)
{
    if (this->cardDrawDisabledForTurn) return;
    int size = this->mainInventory->getAllCardsInternal().size();
    if (size > 0)
    {
        if (soundEffect)
        {
            this->playSoundEffect("cardsounds/draw.wav", soundEffectForEveryone);
        }
        card* crd = this->mainInventory->getAllCardsInternal().at(mainWindow::random.randomBetweenZeroAndN(size - 1));
        this->mainInventory->removeCard(crd->getUID());
        switch (playerNum)
        {
            case -1:
                this->huckster->addCard(crd);
                break;
            case 1:
                this->p1Inventory->addCard(crd);
                break;
            case 2:
                this->p2Inventory->addCard(crd);
                break;
            case 3:
                this->p3Inventory->addCard(crd);
                break;
            case 4:
                this->p4Inventory->addCard(crd);
                break;
        }
        if (this->mW && !doNotShow) this->mW->displayCardOnSideBar(crd->getID());
        if (cDrawnCard)
        {
            this->setCurrentPlayerDrawnCard(true);
        }
        QString cStart, cEnd;
        if (!logMsgColor.isEmpty())
        {
            cStart = "<font color=" + logMsgColor + ">";
            cEnd = "</font>";
        }
        if (playerNum != -1)
        {
            this->writeToGameLog(cStart + tr("%1 drew an item (%2)").arg(mainWindow::getPlayerNickNames().at(playerNum - 1), crd->getName()) + cEnd, teamList(playerNum), true, false);
            this->writeToGameLog(cStart + tr("%1 drew an item").arg(mainWindow::getPlayerNickNames().at(playerNum - 1)) + cEnd, teamList(playerNum), false, true);
        }
        this->runEvent(QStringList() << "cardDrawn" << QString::number(playerNum) << crd->getID() << QString::number(crd->getUID()));
        if (this->mW) this->mW->updateEndTurnButtonState();
    }
}

void gameState::runEvent(const QStringList& eventData, card* priorityCard)
{
    if (this->isNetworkTurn()) return;
    if (eventData[0] == "cardPlacedOnBoard")
    {
        this->cardsWithActiveBronzeAbility.push_back(eventData[1].toInt());
    }
    bool p = this->cardPlacementEnabled;
    this->cardPlacementEnabled = false;
    auto boardPosList = this->gameBoard->getPositionListForCardActivation(this->currentPlayer);
    if (priorityCard)
    {
#ifdef BAZAAR_DEBUG
        scriptExecInfo.append(priorityCard->getName() + ", event :" + eventData.join(","));
#endif
        scriptEnv->executeScript(priorityCard->getScriptCode(), "event", priorityCard, eventData);
#ifdef BAZAAR_DEBUG
        scriptExecInfo.pop_back();
#endif
    }
    if (eventData[0] == "cardPlacedOnBoard")
    {
        this->cardsWithActiveBronzeAbility.pop_back();
    }
    for (auto p : boardPosList)
    {
        card* crd = this->gameBoard->getCardAtPosition(p);
        if (crd && crd != priorityCard)
        {
#ifdef BAZAAR_DEBUG
            scriptExecInfo.append(crd->getName() + ", event :" + eventData.join(","));
#endif
            scriptEnv->executeScript(crd->getScriptCode(), "event", crd, eventData);
#ifdef BAZAAR_DEBUG
            scriptExecInfo.pop_back();
#endif
        }
    }
    if (this->mW && /*scriptEnv->getCurrentEngineLevel() == -1 &&*/ !this->mW->crdRedrawInProgress)
    {
        for (auto p : boardPosList)
        {
            card* crd = this->gameBoard->getCardAtPosition(p);
            if (crd)
            {
                this->mW->crdRedrawInProgress = true;
                crd->redraw();
                crd->removeFromSceneAsNeeded();
                this->mW->crdRedrawInProgress = false;
            }
        }
    }
    for (const auto &scr : this->bkgScripts)
    {
#ifdef BAZAAR_DEBUG
        scriptExecInfo.append("background script:" + scr.first.second + ", event :" + eventData.join(","));
#endif
        scriptEnv->executeScript(scr.second, "event", nullptr, eventData);
#ifdef BAZAAR_DEBUG
        scriptExecInfo.pop_back();
#endif
    }
    this->cardPlacementEnabled = p;
    if (this->mW)
    {
        this->mW->updateEndTurnButtonState();
        this->mW->updateScoreTable();
        this->mW->updateDisabledCardsInInventories();
    }
}

scriptExecutionEnvironment::scriptExecutionEnvironment(gameState* state)
{
    this->currentEngine = -1;
    this->state = state;
    for (int i = 0; i < 8; i++)
    {
        this->addScriptEngine();
    }
}

void scriptExecutionEnvironment::setGameState(gameState* state)
{
    this->state = state;
    for (const auto& eng : this->scriptEngines)
    {
        eng->globalObject().setProperty("graphicsViewMenu", eng->newQObject(state->menu));
        eng->globalObject().setProperty("menu", eng->newQObject(state->menu));
        QJSEngine::setObjectOwnership(state->menu, QJSEngine::CppOwnership);
        eng->globalObject().setProperty("board", eng->newQObject(state->gameBoard));
        QJSEngine::setObjectOwnership(state->gameBoard, QJSEngine::CppOwnership);
        eng->globalObject().setProperty("p1Inventory", eng->newQObject(state->p1Inventory));
        QJSEngine::setObjectOwnership(state->p1Inventory, QJSEngine::CppOwnership);
        eng->globalObject().setProperty("p2Inventory", eng->newQObject(state->p2Inventory));
        QJSEngine::setObjectOwnership(state->p2Inventory, QJSEngine::CppOwnership);
        eng->globalObject().setProperty("p3Inventory", eng->newQObject(state->p3Inventory));
        QJSEngine::setObjectOwnership(state->p3Inventory, QJSEngine::CppOwnership);
        eng->globalObject().setProperty("p4Inventory", eng->newQObject(state->p4Inventory));
        QJSEngine::setObjectOwnership(state->p4Inventory, QJSEngine::CppOwnership);
        eng->globalObject().setProperty("huckster", eng->newQObject(state->huckster));
        QJSEngine::setObjectOwnership(state->huckster, QJSEngine::CppOwnership);
        eng->globalObject().setProperty("storage", eng->newQObject(state->storage));
        QJSEngine::setObjectOwnership(state->storage, QJSEngine::CppOwnership);
        eng->globalObject().setProperty("trash", eng->newQObject(state->trash));
        QJSEngine::setObjectOwnership(state->trash, QJSEngine::CppOwnership);
        eng->globalObject().setProperty("mainInventory", eng->newQObject(state->mainInventory));
        QJSEngine::setObjectOwnership(state->mainInventory, QJSEngine::CppOwnership);
        eng->globalObject().setProperty("gameState", eng->newQObject(state));
        QJSEngine::setObjectOwnership(state, QJSEngine::CppOwnership);
    }
}

QVariantList scriptExecutionEnvironment::executeScript(const QString& code, const QString& funcName, card* currentCard, const QStringList& args)
{
    this->currentEngine++; //decrease this value before returning from the function !!!
    if (this->currentEngine >= this->scriptEngines.size())
    {
        this->addScriptEngine();
    }

    if (currentCard != nullptr)
    {
        this->scriptEngines[this->currentEngine]->globalObject().setProperty("card", this->scriptEngines[this->currentEngine]->newQObject(currentCard));
        QJSEngine::setObjectOwnership(currentCard, QJSEngine::CppOwnership);
    }
    if (funcName.isEmpty())
    {
        auto evRes = this->scriptEngines[this->currentEngine]->evaluate(code);
        auto list = QVariantList() << evRes.toVariant();
        this->currentEngine--;
        return list;
    }
    this->scriptEngines[this->currentEngine]->evaluate(code);

    if (!funcName.isEmpty())
    {
        QJSValue array = this->scriptEngines[this->currentEngine]->newArray(args.size());
        for (int i = 0; i < args.size(); i++)
        {
            array.setProperty(i, QJSValue(args[i]));
        }

        QJSValue funcResult = this->scriptEngines[this->currentEngine]->globalObject().property(funcName).callWithInstance(this->scriptEngines[this->currentEngine]->globalObject(), {array});
        if (funcResult.hasProperty("x") && funcResult.hasProperty("y"))
        {
            QPoint p;
            QPointFromScriptValue(funcResult, p);
            this->currentEngine--;
            return QVariantList() << p;
        }
        else
        {
            QList<QPoint> l;
            pointListFromScriptValue(funcResult, l);
            if (!l.isEmpty())
            {
                QVariantList vlist;
                for (auto pp : l)
                {
                    vlist << pp;
                }
                this->currentEngine--;
                return vlist;
            }
            else if (static_cast<QMetaType::Type>(funcResult.toVariant().userType()) != QMetaType::QVariantList
                     && static_cast<QMetaType::Type>(funcResult.toVariant().userType()) != QMetaType::QStringList)
            {
                this->currentEngine--;
                return QVariantList() << funcResult.toVariant();
            }
            else
            {
                this->currentEngine--;
                return funcResult.toVariant().toList();
            }
        }
    }
    return QVariantList();
}

int scriptExecutionEnvironment::getCurrentEngineLevel()
{
    return this->currentEngine;
}

scriptExecutionEnvironment::~scriptExecutionEnvironment()
{
    for (auto i = 0; i < this->scriptEngines.size(); ++i)
    {
        this->scriptEngines[i]->deleteLater();
    }
}

bool gameState::isCardPlacementEnabled()
{
    return this->cardPlacementEnabled;
}

bool gameState::didPlayerPlaceCardOnBoardThisTurn()
{
    return this->playerPlacedCardOnBoard;
}

void gameState::setCardPlacementEnabled(bool enabled)
{
    this->sendGameEvent(QStringList() << "setCardPlacementEnabled" << QString::number(enabled));
    this->cardPlacementEnabled = enabled;
}

QList<int> gameState::getPlayerTurnOrder() const
{
    return this->playerTurnOrder;
}

int gameState::getGameMode() const
{
    return this->gameMode;
}

QString gameState::getConfigVariable(const QString& key) const
{
    return mainWindow::getConfigVariable(key);
}

QPoint gameState::newPoint(int x, int y) const
{
    return QPoint(x, y);
}

QStringList gameState::getPlayerNickNames() const
{
    return mainWindow::getPlayerNickNames();
}

void gameState::setSilverActionsDisabledForTurn(bool disabled)
{
    this->sendGameEvent(QStringList() << "setSilverActionsDisabledForTurn" << QString::number(disabled));
    this->silverActionsDisabledForTurn = disabled;
}

bool gameState::areSilverActionsDisabledForTurn() const
{
    return this->silverActionsDisabledForTurn;
}

void gameState::reportError(const QString& error) const
{
    return mainWindow::reportError(error);
}

bool gameState::isHucksterEnabled() const
{
    return this->hucksterEnabled;
}

bool gameState::isOpenHandsEnabled() const
{
    return this->openHands;
}

int gameState::getCurrentPlayer() const
{
    return this->currentPlayer;
}

void gameState::setVariable(const QString& name, const QString& value)
{
    this->sendGameEvent(QStringList() << "setVariable" << name << value);
    this->scriptVarMap[name] = value;
}

QString gameState::getVariable(const QString& name) const
{
    return this->scriptVarMap[name];
}

void scriptExecutionEnvironment::addScriptEngine()
{
    this->scriptEngines.append(new QJSEngine);
    qmlRegisterAnonymousType<card>("LostBazaar", 1);
    this->scriptEngines.constLast()->globalObject().setProperty("rng", this->scriptEngines.constLast()->newQObject(&mainWindow::random));
    QJSEngine::setObjectOwnership(&mainWindow::random, QJSEngine::CppOwnership);
    this->scriptEngines.constLast()->globalObject().setProperty("graphicsViewMenu", this->scriptEngines.constLast()->newQObject(state->menu));
    this->scriptEngines.constLast()->globalObject().setProperty("menu", this->scriptEngines.constLast()->newQObject(state->menu));
    QJSEngine::setObjectOwnership(state->menu, QJSEngine::CppOwnership);
    this->scriptEngines.constLast()->globalObject().setProperty("board", this->scriptEngines.constLast()->newQObject(state->gameBoard));
    QJSEngine::setObjectOwnership(state->gameBoard, QJSEngine::CppOwnership);
    this->scriptEngines.constLast()->globalObject().setProperty("p1Inventory", this->scriptEngines.constLast()->newQObject(state->p1Inventory));
    QJSEngine::setObjectOwnership(state->p1Inventory, QJSEngine::CppOwnership);
    this->scriptEngines.constLast()->globalObject().setProperty("p2Inventory", this->scriptEngines.constLast()->newQObject(state->p2Inventory));
    QJSEngine::setObjectOwnership(state->p2Inventory, QJSEngine::CppOwnership);
    this->scriptEngines.constLast()->globalObject().setProperty("p3Inventory", this->scriptEngines.constLast()->newQObject(state->p3Inventory));
    QJSEngine::setObjectOwnership(state->p3Inventory, QJSEngine::CppOwnership);
    this->scriptEngines.constLast()->globalObject().setProperty("p4Inventory", this->scriptEngines.constLast()->newQObject(state->p4Inventory));
    QJSEngine::setObjectOwnership(state->p4Inventory, QJSEngine::CppOwnership);
    this->scriptEngines.constLast()->globalObject().setProperty("huckster", this->scriptEngines.constLast()->newQObject(state->huckster));
    QJSEngine::setObjectOwnership(state->huckster, QJSEngine::CppOwnership);
    this->scriptEngines.constLast()->globalObject().setProperty("trash", this->scriptEngines.constLast()->newQObject(state->trash));
    QJSEngine::setObjectOwnership(state->trash, QJSEngine::CppOwnership);
    this->scriptEngines.constLast()->globalObject().setProperty("mainInventory", this->scriptEngines.constLast()->newQObject(state->mainInventory));
    QJSEngine::setObjectOwnership(state->mainInventory, QJSEngine::CppOwnership);
    this->scriptEngines.constLast()->globalObject().setProperty("gameState", this->scriptEngines.constLast()->newQObject(state));
    QJSEngine::setObjectOwnership(state, QJSEngine::CppOwnership);
    this->scriptEngines.constLast()->globalObject().setProperty("storage", this->scriptEngines.constLast()->newQObject(state->storage));
    QJSEngine::setObjectOwnership(state->storage, QJSEngine::CppOwnership);

    this->scriptEngines.constLast()->installExtensions(QJSEngine::TranslationExtension);
    this->scriptEngines.constLast()->installExtensions(QJSEngine::ConsoleExtension);
}

void gameState::endTurnButtonClicked(bool forceSwitch, bool aiPlayer)
{
    if (this->mW)
    {
        if (this->cancelEnabledCounter > 0) //cancel
        {
            emit this->mW->cancelButtonClicked();
            return;
        }
    }
    if (!this->currentPlayerDrawnCard && !this->cardDrawDisabledForTurn) //draw card
    {
        this->setCurrentPlayerDrawnCard(true);
        this->drawCard(this->currentPlayer, false, false, true);
        if (!forceSwitch) return;
    }

    auto r = this->currentPlayerCanEndTurn();
    if (r == 0 && !this->isAITurn() && !this->isNetworkTurn()) //Too many cards
    {
#ifndef Q_OS_ANDROID
        mainWindow::msgBoxInfo(this->mW, tr("Too many items in hand"), tr("You have too many items in your hand.\nTo end your turn, you need to send some to the trash. Select which ones by right clicking on them."));
#else
        mainWindow::msgBoxInfo(this->mW, tr("Too many items in hand"), tr("You have too many items in your hand.\nTo end your turn, you need to send some to the trash. Select which ones by keeping your finger on them for a few seconds."));
#endif
        return;
    }
    else if (r == -1 && !this->isAITurn() && !this->isNetworkTurn())
    {
        mainWindow::msgBoxInfo(this->mW, tr("Too few items in hand"), tr("You can't go under 5 items by throwing items into the trash at the end of the turn.\nUnmark some of the items which you have marked for throwing away."));
        return;
    }

    if (this->currentPlayerDrawnCard || this->cardDrawDisabledForTurn)//switch turns
    {
        if (!aiPlayer && !forceSwitch && !this->isObserver())
        {
            if (this->isCardPlacementEnabled() && this->mW && this->mW->getConfigVariable("turnEndWarning").toInt())
            {
                if ((this->currentPlayer == 1 && this->p1Inventory->hasPlayableCard()) ||
                        (this->currentPlayer == 2 && this->p2Inventory->hasPlayableCard()) ||
                        (this->currentPlayer == 3 && this->p3Inventory->hasPlayableCard()) ||
                        (this->currentPlayer == 4 && this->p4Inventory->hasPlayableCard()))
                {
                    if (mainWindow::msgBoxQuestion(this->mW, tr("End of turn confirmation"), tr("Are you sure you want to end your turn without placing an item?")) != QMessageBox::Yes) return;
                }
            }
        }

        if (this->mW) this->mW->displayCardOnSideBar("");

        this->runEvent(QStringList() << "endOfTurn" << QString::number(this->currentPlayer));
        this->checkVictory();
        this->setSilverActionsDisabledForTurn(false);
        this->setPlayerPlacedCardOnBoard(false);
        if (this->mW)
        {
            if (mainWindow::getConfigVariable("useHotSeatContinuePage").toInt() && this->isLocalUser(this->currentPlayer) && this->isLocalUser(this->nextPlayer()))
            {
                this->mW->ui->stackedWidget->slideInWidget(this->mW->ui->contHotSeatPage, slidingStackedWidget::AnimationDirection::RightToLeft);
                this->mW->waitingForHotSeat = true;
                delay(250);
            }
        }
        if (this->currentPlayer == 1)
        {
            this->p1Inventory->removeCardsMarkedForTrash(this->trash);
        }
        else if (this->currentPlayer == 2)
        {
            this->p2Inventory->removeCardsMarkedForTrash(this->trash);
        }
        else if (this->currentPlayer == 3)
        {
            this->p3Inventory->removeCardsMarkedForTrash(this->trash);
        }
        else if (this->currentPlayer == 4)
        {
            this->p4Inventory->removeCardsMarkedForTrash(this->trash);
        }
        else
        {
            mainWindow::reportError(tr("Error: invalid player in endTurnButtonClicked(...)"));
        }

        this->setCardPlacementEnabled(true);

        this->setCurrentPlayer(this->nextPlayer());

        this->overrideNextPlayer(-1);
        this->setCurrentPlayerDrawnCard(false);
        this->setCardDrawDisabledForTurn(false);
        if (!this->isLocalUser(this->currentPlayer)) this->sendGameEvent(QStringList() << "overrideNextPlayer" << QString::number(-1), true);
        if (!this->isLocalUser(this->currentPlayer)) this->sendGameEvent(QStringList() << "setCurrentPlayerDrawnCard" << QString::number(false), true);
        if (!this->isLocalUser(this->currentPlayer)) this->sendGameEvent(QStringList() << "setCardDrawDisabledForTurn" << QString::number(false), true);

        this->updateUIForTurnSwitch();
        if (!this->isLocalUser(this->currentPlayer)) this->sendGameEvent(QStringList() << "updateUIForTurnSwitch", true);

        if (this->mW) while (this->mW->waitingForHotSeat) QCoreApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);

        if ((!this->mW || this->mW->hostMode) && this->isAITurn())
        {
            if (!this->alreadyOver) connect(this->gameBoard, &board::rotationFinished, this, &gameState::aiTurn);
            this->gameBoard->rotateBoardToPlayer(this->currentPlayer);
        }
        else
        {
            this->gameBoard->rotateBoardToPlayer(this->currentPlayer);
            this->doThingsThatNeedToBeDoneAtTurnStart();
        }
    }
}

int gameState::nextPlayer() const
{
    if (this->nextPlayerOverride != -1) return this->nextPlayerOverride;
    if (this->currentPlayer == getPlayerTurnOrder().constLast())
    {
        return getPlayerTurnOrder().constFirst();
    }
    else
    {
        return getPlayerTurnOrder().at(getPlayerTurnOrder().indexOf(this->currentPlayer) + 1);
    }
}

void gameState::overrideNextPlayer(int playerNum)
{
    this->nextPlayerOverride = playerNum;
    this->sendGameEvent(QStringList() << "overrideNextPlayer" << QString::number(playerNum));
}

void gameState::endGame()
{
    QApplication::postEvent(QApplication::instance(), new bazaarEvent{"endGame", this->mW});
}

void gameState::retryCurrentPuzzle()
{
    QApplication::postEvent(QApplication::instance(), new bazaarEvent{"retryCurrentPuzzle", this->mW});
}

void gameState::returnToPuzzleScreen()
{
    QApplication::postEvent(QApplication::instance(), new bazaarEvent{"returnToPuzzleScreen", this->mW});
}

void gameState::setCancelEnabled(bool enabled)
{
    if (enabled) cancelEnabledCounter++;
    else if (cancelEnabledCounter > 0) cancelEnabledCounter--;
    if (cancelEnabledCounter == 1) //enable cancel
    {
        if (this->mW) this->mW->updateEndTurnButtonState();
    }
    else if (cancelEnabledCounter == 0)  //disable cancel
    {
        if (this->mW) this->mW->updateEndTurnButtonState();
    }
}

void gameState::playSoundEffect(const QString& name, bool everyone)
{
    if (this->mW)
    {
        this->mW->playSoundEffect(name, everyone);
    }
}

QList<int> gameState::getScore() const
{
    QList<int> scores(4, 0);
    for (auto crd : this->gameBoard->getAllCardsInternal())
    {
#ifdef BAZAAR_DEBUG
        scriptExecInfo.append(crd->getID() + " (getCurrentValue)");
#endif
        scores[crd->getPlayer() - 1] += scriptEnv->executeScript(crd->getScriptCode(), "getCurrentValue", crd).constFirst().toInt();
#ifdef BAZAAR_DEBUG
        scriptExecInfo.pop_back();
#endif
    }
    return scores;
}

void gameState::init(mainWindow* mainW, gameState* state)
{
    connect(this, &gameState::aiFinished, this, &gameState::processAIFinished);

    if (mainW)
    {
        this->mW = mainW;
        this->menu = new graphicsViewMenu(this->mW->ui->gameBoard);
        this->gameBoard = this->mW->ui->gameBoard;
        this->storage = this->mW->ui->storage;
        this->p1Inventory = this->mW->ui->p1Inventory;
        this->p2Inventory = this->mW->ui->p2Inventory;
        this->p3Inventory = this->mW->ui->p3Inventory;
        this->p4Inventory = this->mW->ui->p4Inventory;
        this->huckster = this->mW->ui->huckster;
        this->trash = this->mW->ui->trash;
        this->mainInventory = this->mW->ui->mainInventory;
        this->p1Inventory->setTab(this->mW->ui->tab_p1Inventory);
        this->p2Inventory->setTab(this->mW->ui->tab_p2Inventory);
        this->p3Inventory->setTab(this->mW->ui->tab_p3Inventory);
        this->p4Inventory->setTab(this->mW->ui->tab_p4Inventory);
        this->huckster->setTab(this->mW->ui->tab_Huckster);
        this->storage->setTab(this->mW->ui->tab_Storage);
        this->trash->setTab(this->mW->ui->tab_Trash);
        this->mainInventory->setTab(this->mW->ui->tab_MainInventory);
        this->cancelEnabledCounter = 0;
    }
    else if (state)
    {
        this->mW = nullptr;
        this->gameBoard = state->gameBoard->clone(this);
        this->p1Inventory = state->p1Inventory->clone(this);
        this->p2Inventory = state->p2Inventory->clone(this);
        this->p3Inventory = state->p3Inventory->clone(this);
        this->p4Inventory = state->p4Inventory->clone(this);
        this->huckster = state->huckster->clone(this);
        this->storage = state->storage->clone(this);
        this->trash = state->trash->clone(this);
        this->mainInventory = state->mainInventory->clone(this);
        this->menu = new graphicsViewMenu(this->gameBoard);
        this->scriptEnv = state->scriptEnv;
        this->scriptEnv->setGameState(this);
        this->p1Inventory->setTab(nullptr);
        this->p2Inventory->setTab(nullptr);
        this->p3Inventory->setTab(nullptr);
        this->p4Inventory->setTab(nullptr);
        this->huckster->setTab(nullptr);
        this->storage->setTab(nullptr);
        this->trash->setTab(nullptr);
        this->mainInventory->setTab(nullptr);
        this->p1AI = state->p1AI;
        this->p2AI = state->p2AI;
        this->p3AI = state->p3AI;
        this->p4AI = state->p4AI;
        this->p1Network = state->p1Network;
        this->p2Network = state->p2Network;
        this->p3Network = state->p3Network;
        this->p4Network = state->p4Network;
        this->gameMode = state->gameMode;
        this->playerTurnOrder = state->playerTurnOrder;
        this->cancelEnabledCounter = state->cancelEnabledCounter;
        this->hucksterEnabled = state->hucksterEnabled;
        this->openHands = state->openHands;
        this->silverActionsDisabledForTurn = state->silverActionsDisabledForTurn;
        this->playerPlacedCardOnBoard = state->playerPlacedCardOnBoard;
        this->scoreCache = state->scoreCache;
        this->scriptVarMap = state->scriptVarMap;
        this->nextPlayerOverride = state->nextPlayerOverride;
        this->victoryCheckEnabled = state->victoryCheckEnabled;
        this->currentPlayerDrawnCard = state->currentPlayerDrawnCard;
        this->cardDrawDisabledForTurn = state->cardDrawDisabledForTurn;
        this->alreadyOver = state->alreadyOver;
        this->currentPlayer = state->currentPlayer;
        this->cardPlacementEnabled = state->cardPlacementEnabled;
        this->cardsWithActiveBronzeAbility = state->cardsWithActiveBronzeAbility;
    }
    this->huckster->setGameState(this);
    this->storage->setGameState(this);
    this->trash->setGameState(this);
    this->mainInventory->setGameState(this);
    this->p1Inventory->setGameState(this);
    this->p2Inventory->setGameState(this);
    this->p3Inventory->setGameState(this);
    this->p4Inventory->setGameState(this);
    this->gameBoard->setGameState(this);
    this->menu->setGameState(this);

    if (mainW)
    {
        connect(this->storage, &cardInventory::stopTileChoice, this->gameBoard, &board::stopTileChoice);
        connect(this->p1Inventory, &cardInventory::stopTileChoice, this->gameBoard, &board::stopTileChoice);
        connect(this->p2Inventory, &cardInventory::stopTileChoice, this->gameBoard, &board::stopTileChoice);
        connect(this->p3Inventory, &cardInventory::stopTileChoice, this->gameBoard, &board::stopTileChoice);
        connect(this->p4Inventory, &cardInventory::stopTileChoice, this->gameBoard, &board::stopTileChoice);
        connect(this->huckster, &cardInventory::stopTileChoice, this->gameBoard, &board::stopTileChoice);
        connect(this->trash, &cardInventory::stopTileChoice, this->gameBoard, &board::stopTileChoice);
        connect(this->mainInventory, &cardInventory::stopTileChoice, this->gameBoard, &board::stopTileChoice);
    }

    this->p1Inventory->setPlayer(1);
    this->p2Inventory->setPlayer(2);
    this->p3Inventory->setPlayer(3);
    this->p4Inventory->setPlayer(4);
    if (!this->scriptEnv) this->scriptEnv = new scriptExecutionEnvironment(this);
}

void gameState::doThingsThatNeedToBeDoneAtTurnStart()
{
    if (!this->isLocalUser(this->currentPlayer)) this->sendGameEvent(QStringList() << "rotateBoardToPlayer" << QString::number(this->currentPlayer), true);

    if (!this->isLocalUser(this->currentPlayer)) this->sendGameEvent(QStringList() << "runTurnStartedEvent", true);

    this->runEvent(QStringList() << "turnStarted" << QString::number(this->currentPlayer));

    checkConsistency();

    static int testTurnCounter = 1;
    if (this->mW)
    {
        auto gameSaveIdx = QCoreApplication::arguments().indexOf("--game-save-id");
        if (gameSaveIdx > -1)
        {
            this->mW->save().saveToFile(mainWindow::getSavePath() + QString("/%1_turn_%2.lbs").arg(QCoreApplication::arguments().at(gameSaveIdx + 1), QString::number(testTurnCounter)), false);
            testTurnCounter++;
            this->mW->setWindowTitle(QString("Lost Bazaar [%1, %2]").arg(QCoreApplication::arguments().at(gameSaveIdx + 1), QString::number(testTurnCounter)));
        }
        else
        {
            this->mW->save().saveToFile(mainWindow::getSavePath() + "/last_turn.lbs", false);
        }
    }
}

bool gameState::isLocalUser(int playerNum)
{
    switch (playerNum)
    {
        case 1:
            return !this->p1AI && !this->p1Network;
        case 2:
            return !this->p2AI && !this->p2Network && this->gameMode != 4;
        case 3:
            return !this->p3AI && !this->p3Network && this->gameMode != 0;
        case 4:
            return !this->p4AI && !this->p4Network && this->gameMode != 0 && this->gameMode != 4 && this->gameMode != 2;
        default:
            mainWindow::reportError(tr("Invalid player number in isLocalUser(...)"));
            return true;
    }
}

void gameState::processAIFinished()
{
    if (this->mW) this->mW->setUIDisabledForAI(false);
    this->endTurnButtonClicked(this->alreadyOver, true);
}

bool gameState::isObserver()
{
    return this->mW && this->mW->isObserver();
}

void gameState::updateUIForTurnSwitch()
{
    this->sendGameEvent(QStringList() << "updateUIForTurnSwitch");
    this->p1Inventory->setReadOnly(this->currentPlayer != 1 || this->isObserver());
    this->p2Inventory->setReadOnly(this->currentPlayer != 2 || this->isObserver());
    this->p3Inventory->setReadOnly(this->currentPlayer != 3 || this->isObserver());
    this->p4Inventory->setReadOnly(this->currentPlayer != 4 || this->isObserver());
    this->p1Inventory->setViewable(((this->openHands || this->isObserver() || (this->mW && this->mW->singleLocalUser() == 1)) && this->playerTurnOrder.contains(1)) || (this->currentPlayer == 1 && this->isLocalUser(1)), true);
    this->p2Inventory->setViewable(((this->openHands || this->isObserver() || (this->mW && this->mW->singleLocalUser() == 2)) && this->playerTurnOrder.contains(2)) || (this->currentPlayer == 2 && this->isLocalUser(2)), true);
    this->p3Inventory->setViewable(((this->openHands || this->isObserver() || (this->mW && this->mW->singleLocalUser() == 3)) && this->playerTurnOrder.contains(3)) || (this->currentPlayer == 3 && this->isLocalUser(3)), true);
    this->p4Inventory->setViewable(((this->openHands || this->isObserver() || (this->mW && this->mW->singleLocalUser() == 4)) && this->playerTurnOrder.contains(4)) || (this->currentPlayer == 4 && this->isLocalUser(4)), true);
    if (this->getGameMode() == 1) //2 vs. 2
    {
        if (this->p1Inventory->isViewable()) this->p3Inventory->setViewable(true);
        if (this->p3Inventory->isViewable()) this->p1Inventory->setViewable(true);
        if (this->p2Inventory->isViewable()) this->p4Inventory->setViewable(true);
        if (this->p4Inventory->isViewable()) this->p2Inventory->setViewable(true);
    }
    if (this->mW) this->mW->switchToPlayerInventory(this->currentPlayer);
    if (this->mW)
    {
        this->mW->updateCurrentPlayerAndScoreLabels();

        this->writeToGameLog(tr("Turn started<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Turn of %1</b><br>").arg(mainWindow::getPlayerNickNames().at(this->currentPlayer - 1)), {1, 2, 3, 4}, true, false);

        this->mW->updateDisplayedLogs();
    }
    if (this->isAITurn() || this->isNetworkTurn())
    {
        if (this->mW) this->mW->disableGUIForPassiveTurn();
    }
    else
    {
        if (this->mW) this->mW->enableGUIAfterPassiveTurn();
    }
}

void gameState::sendGameEvent(const QStringList& ev, bool force)
{
    if (this->mW)
    {
        this->mW->sendGameEvent(ev, force);
    }
}

void gameState::writeToGameLog(const QString& msg, QList<int> playerList, bool global, bool inverted, bool no_update)
{
    QStringList netData;
    netData << "writeToGameLog" << msg << QString::number(global) << QString::number(inverted);
    for (auto p : playerList)
    {
        netData << QString::number(p);
    }
    if (!no_update) this->sendGameEvent(netData);

    QString newMsg = QString("[%1] %2").arg(QTime::currentTime().toString(), msg);
    if (!inverted)
    {
        for (auto p : playerList) this->logs[p].append(newMsg);
    }
    else
    {
        for (auto& k : this->logs)
        {
            if (k.first != -1 && !playerList.contains(k.first))
            {
                this->logs[k.first].append(newMsg);
            }
        }
    }
    if (global) this->logs[-1].append(newMsg);
    if (this->mW && !no_update) this->mW->updateDisplayedLogs();
}

void gameState::checkVictory()
{
    QString backToText = tr("Back to main menu");;
    if(this->mW && this->mW->backToTutorialFromGame)
    {
        backToText = tr("Back to tutorial");
    }
    if (this->victoryCheckEnabled)
    {
        this->sendGameEvent(QStringList() << "checkVictory");
        if (!alreadyOver && (this->gameBoard->getEmptyTiles().isEmpty() || this->mainInventory->isEmpty()))
        {
            bool res;
            if (this->mW) this->mW->updateScoreTable();
            if (this->gameMode == 1) //2 vs. 2
            {
                if (scoreCache[0] + scoreCache[2] > scoreCache[1] + scoreCache[3])
                {
                    res = this->menu->declareWinnerTeam(1, backToText);
                }
                else if (scoreCache[0] + scoreCache[2] == scoreCache[1] + scoreCache[3])
                {
                    res = this->menu->declareWinnerTeam(-1, backToText);
                }
                else
                {
                    res = this->menu->declareWinnerTeam(2, backToText);
                }
            }
            else   //FFA
            {
                QList<long> scores;
                QList<long> players;
                QList<int> winners;
                scores << scoreCache[0];
                players << 1;
                if (this->gameMode == 4)
                {
                    scores << scoreCache[2];
                    players << 3;
                }
                else
                {
                    scores << scoreCache[1];
                    players << 2;
                }
                if (this->gameMode == 2) //3 player ffa
                {
                    scores << scoreCache[2];
                    players << 3;
                }
                else if (this->gameMode == 3)  //4 player ffa
                {
                    scores << scoreCache[2];
                    scores << scoreCache[3];
                    players << 3;
                    players << 4;
                }
                int max = scores[0];
                for (int i = 1; i < scores.size(); i++)
                    if (max < scores[i]) max = scores[i];
                for (int i = 0; i < scores.size(); i++)
                {
                    if (scores[i] == max)
                    {
                        winners.append(players[i]);
                    }
                }
                if (winners.size() == players.size()) //draw
                {
                    res = this->menu->declareWinnerPlayers({}, backToText);
                }
                else
                {
                    res = this->menu->declareWinnerPlayers(winners, backToText);
                }
            }
            if (res)
            {
                this->alreadyOver = true;
            }
            else
            {
                this->alreadyOver = true;
                if (this->mW) this->mW->endGame();
            }
        }
    }
}

bool gameState::currentPlayerCanDrawCard() const
{
    return !this->currentPlayerDrawnCard && !this->mainInventory->isEmpty() && !this->cardDrawDisabledForTurn;
}

bool gameState::isAITurn() const
{
    return ((this->p1AI && this->getCurrentPlayer() == 1) ||
            (this->p2AI && this->getCurrentPlayer() == 2) ||
            (this->p3AI && this->getCurrentPlayer() == 3) ||
            (this->p4AI && this->getCurrentPlayer() == 4));
}

QList<int> gameState::getCardsWithActiveBronzeAbility() const
{
    return this->cardsWithActiveBronzeAbility;
}

bool gameState::isNetworkTurn() const
{
    return ((this->p1Network && this->getCurrentPlayer() == 1) ||
            (this->p2Network && this->getCurrentPlayer() == 2) ||
            (this->p3Network && this->getCurrentPlayer() == 3) ||
            (this->p4Network && this->getCurrentPlayer() == 4));
}

void gameState::checkConsistency()
{
#ifdef BAZAAR_DEBUG
    QMapIterator<int, card*> it(card::currentCards);
    QString multipleCards;
    QString lostCards;
    while (it.hasNext())
    {
        it.next();
        QStringList locs;
        if (this->gameBoard->contains(it.value())) locs.append("board");
        if (this->p1Inventory->contains(it.value())) locs.append("p1Inventory");
        if (this->p2Inventory->contains(it.value())) locs.append("p2Inventory");
        if (this->p3Inventory->contains(it.value())) locs.append("p3Inventory");
        if (this->p4Inventory->contains(it.value())) locs.append("p4Inventory");
        if (this->huckster->contains(it.value())) locs.append("huckster");
        if (this->storage->contains(it.value())) locs.append("storage");
        if (this->mainInventory->contains(it.value())) locs.append("mainInventory");
        if (this->trash->contains(it.value())) locs.append("trash");
        if (locs.isEmpty())
        {
            lostCards += tr("%1 (UID: %2)\n").arg(it.value()->getName(), QString::number(it.value()->getUID()));
        }
        else if (locs.size() > 1)
        {
            multipleCards += tr("%1 (UID: %2), locations: %3\n").arg(it.value()->getName(), QString::number(it.value()->getUID()), locs.join(", "));
        }
    }
    if (!lostCards.isEmpty() || !multipleCards.isEmpty())
    {
        QString msg = tr("The game is in an inconsistent state. You may continue, but it will not end well.\n");
        if (!lostCards.isEmpty())
        {
            msg += tr("The following cards are not in any inventory or on the board:\n%1They are forever lost to the void.\n").arg(lostCards);
        }
        if (!multipleCards.isEmpty())
        {
            msg += tr("The following cards appear in multiple places:\n%1").arg(multipleCards);
        }
        msg += tr("Please submit a bug report.");
        mainWindow::reportError(msg);
    }
#endif
}

int64_t gameState::getAIScore()
{
    auto score = this->getScore();
    if (this->gameMode == 2)
    {
        if (this->currentPlayer == 1 || this->currentPlayer == 3)
        {
            return score[0] + score[2];
        }
        else if (this->currentPlayer == 2 || this->currentPlayer == 4)
        {
            return score[1] + score[3];
        }
    }
    else
    {
        return score[this->currentPlayer - 1];
    }
    return 0;
}

void gameState::runAIExecPath(const aiExecutionPath& p)
{
    this->aiExecPath = p;
    this->aiExecPath.reset();

    auto firstSilverActionMaxCount = this->aiExecPath.getChoice(4);
#ifdef BAZAAR_DEBUG
    this->aiExecPath.addDebugData("First silver action max count: " + QString::number(firstSilverActionMaxCount));
#endif
    auto secondSilverActionMaxCount = this->aiExecPath.getChoice(4);
#ifdef BAZAAR_DEBUG
    this->aiExecPath.addDebugData("Second silver action max count: " + QString::number(secondSilverActionMaxCount));
#endif
    auto cardPlacementCount = 1;

    std::set<card*> triedCards;

    for (auto i = 0; i < firstSilverActionMaxCount; ++i)
    {
        auto _cards = this->gameBoard->getPlayerCardsInternal(this->currentPlayer);
        std::vector<card*> cards;
        for (const auto& crd : _cards)
        {
            if (!triedCards.count(crd) && !crd->isGrayScale() && (crd->hasSilverAction() || crd->getName() == "handmirror"/*TODO special case hack*/) && (crd->isFullSize() || (!crd->isFullSize() && crd->getIcons().count("tptu")))) cards.push_back(crd);
        }
        if (!cards.size()) break;
        std::sort(cards.begin(), cards.end(), [](card  * const & a, card * const & b)
        {
            if (a->getPosX() != b->getPosX())
                return a->getPosX() < b->getPosX();
            return a->getPosY() < b->getPosY();
        });
        auto cardIdx = this->aiExecPath.getChoice(cards.size());
        triedCards.insert(cards[cardIdx]);
#ifdef BAZAAR_DEBUG
        this->aiExecPath.addDebugData("Card in first silver choice:" + cards[cardIdx]->getID() + ", all cards:" + QString::number(cards.size()));
#endif
        if (!this->silverActionsDisabledForTurn)
        {
            if (mainWindow::getConfigVariable("gameAnimations").toInt() && this->mW && this->mW->isGamePageVisible())
            {
                connect(cards[cardIdx], &card::silverActionAnimationFinished, this, [cardIdx, cards, this]
                {
                    disconnect(cards[cardIdx], &card::silverActionAnimationFinished, nullptr, nullptr);
                    this->runEvent(QStringList() << "cardSilverActionActivated" << QString::number(cards[cardIdx]->getUID()), cards[cardIdx]);
                });
                cards[cardIdx]->animateSilverAction();
            }
            else
            {
                this->runEvent(QStringList() << "cardSilverActionActivated" << QString::number(cards[cardIdx]->getUID()), cards[cardIdx]);
            }
            if (p.isRealRun()) delay(1500);
        }
    }

    for (auto i = 0; i < cardPlacementCount; ++i)
    {
        auto crds = this->getCurrentPlayerInventory()->getAllCardsInternal();
        std::vector<std::pair<card*, QVariantList>> availTiles;
        for (auto j = 0; j < crds.size(); ++j)
        {
            auto res = scriptEnv->executeScript(crds[j]->getScriptCode(), "getAvailableTiles", crds[j], QStringList() << crds[j]->getID() << QString::number(this->currentPlayer));
            if (!res.isEmpty())
            {
                availTiles.push_back(std::pair<card*, QVariantList>(crds[j], res));
            }
        }
        if (!availTiles.size()) break;
        auto cardChoice = this->aiExecPath.getChoice(availTiles.size());
#ifdef BAZAAR_DEBUG
        this->aiExecPath.addDebugData("Card placement choice in first silver action:" + availTiles[cardChoice].first->getID()
                                      + ", list size: " + QString::number(availTiles.size()));
#endif
        auto posChoice = this->aiExecPath.getChoice(availTiles[cardChoice].second.size());
#ifdef BAZAAR_DEBUG
        this->aiExecPath.addDebugData("Card placement position in first silver action X: " +
                                      QString::number(availTiles[cardChoice].second[posChoice].toPoint().x()) +
                                      " Y: " +
                                      QString::number(availTiles[cardChoice].second[posChoice].toPoint().y()) +
                                      " list size: " + QString::number(availTiles[cardChoice].second.size()));
#endif
        this->getCurrentPlayerInventory()->moveCardToBoard(availTiles[cardChoice].first->getUID(), availTiles[cardChoice].second[posChoice].toPoint(), true);
        if (p.isRealRun()) delay(1500);
    }

    triedCards.clear();

    for (auto i = 0; i < secondSilverActionMaxCount; ++i)
    {
        auto _cards = this->gameBoard->getPlayerCardsInternal(this->currentPlayer);
        std::vector<card*> cards;
        for (const auto& crd : _cards)
        {
            if (!triedCards.count(crd) && (crd->hasSilverAction() || crd->getName() == "handmirror"/*TODO special case hack*/) && !crd->isGrayScale() && (crd->isFullSize() || (!crd->isFullSize() && crd->getIcons().count("tptu")))) cards.push_back(crd);
        }
        if (!cards.size()) break;
        std::sort(cards.begin(), cards.end(), [](card * const & a, card * const & b)
        {
            if (a->getPosX() != b->getPosX())
                return a->getPosX() < b->getPosX();
            return a->getPosY() < b->getPosY();
        });
        auto cardIdx = this->aiExecPath.getChoice(cards.size());
        triedCards.insert(cards[cardIdx]);
#ifdef BAZAAR_DEBUG
        this->aiExecPath.addDebugData("Card in second silver choice:" + cards[cardIdx]->getID() + ", all cards:" + QString::number(cards.size()));
#endif
        if (!this->silverActionsDisabledForTurn)
        {
            if (mainWindow::getConfigVariable("gameAnimations").toInt() && this->mW && this->mW->isGamePageVisible())
            {
                connect(cards[cardIdx], &card::silverActionAnimationFinished, this, [cardIdx, cards, this]
                {
                    disconnect(cards[cardIdx], &card::silverActionAnimationFinished, nullptr, nullptr);
                    this->runEvent(QStringList() << "cardSilverActionActivated" << QString::number(cards[cardIdx]->getUID()), cards[cardIdx]);
                });
                cards[cardIdx]->animateSilverAction();
            }
            else
            {
                this->runEvent(QStringList() << "cardSilverActionActivated" << QString::number(cards[cardIdx]->getUID()), cards[cardIdx]);
            }
            if (p.isRealRun()) delay(1500);
        }
    }

    if (p.isRealRun()) delay(1500);
}

cardInventory* gameState::getCurrentPlayerInventory()
{
    switch (this->getCurrentPlayer())
    {
        case 1:
            return this->p1Inventory;
        case 2:
            return this->p2Inventory;
        case 3:
            return this->p3Inventory;
        case 4:
            return this->p4Inventory;
        default:
            this->mW->reportError("Error: getCurrentPlayerInventory");
    }
    return nullptr;
}

void gameState::setCurrentPlayer(int player)
{
    this->sendGameEvent(QStringList() << "setCurrentPlayer" << QString::number(player) << (this->mW && this->mW->ongoingNetworkGame ? this->mW->getOOSCheckData() : QStringList{}));
    this->currentPlayer = player;
    if (this->mW) this->mW->drawOrientationOnSideBarCardGV();
}

void gameState::setCurrentPlayerDrawnCard(bool d)
{
    this->sendGameEvent(QStringList() << "setCurrentPlayerDrawnCard" << QString::number(d));
    this->currentPlayerDrawnCard = d;
}

void gameState::clear()
{
    this->logs.clear();
    this->huckster->clear();
    this->storage->clear();
    this->scriptVarMap.clear();
    this->mainInventory->clear();
    this->gameBoard->clear();
    this->p1Inventory->clear();
    this->p2Inventory->clear();
    this->p3Inventory->clear();
    this->p4Inventory->clear();
    this->trash->clear();
}

void gameState::setPlayerPlacedCardOnBoard(bool placed)
{
    this->sendGameEvent(QStringList() << "setPlayerPlacedCardOnBoard" << QString::number(placed));
    this->playerPlacedCardOnBoard = placed;
}

void gameState::setCardDrawDisabledForTurn(bool d)
{
    this->sendGameEvent(QStringList() << "setCardDrawDisabledForTurn" << QString::number(d));
    this->cardDrawDisabledForTurn = d;
    if (this->mW) this->mW->updateEndTurnButtonState();
}

bool gameState::hasCurrentPlayerDrawnCard() const
{
    return this->currentPlayerDrawnCard;
}

bool gameState::isCardDrawDisabledForTurn() const
{
    return this->cardDrawDisabledForTurn;
}

card* gameState::cardForUID(int uid) const
{
    if (card::currentCards.count(uid)) return card::currentCards[uid];
    return nullptr;
}

int gameState::currentPlayerCanEndTurn() const
{
    int cardDiff = 0;
    switch (this->getCurrentPlayer())
    {
        case 1:
            cardDiff = this->p1Inventory->getAllCardsInternal().size() - this->p1Inventory->cardsMarkedForTrash().size();
            if (cardDiff < 5 && this->p1Inventory->cardsMarkedForTrash().size() > 0) return -1;
            if (this->p1Inventory->getAllCardsInternal().size() > 5) return cardDiff <= 5;
            break;
        case 2:
            cardDiff = this->p2Inventory->getAllCardsInternal().size() - this->p2Inventory->cardsMarkedForTrash().size();
            if (cardDiff < 5 && this->p2Inventory->cardsMarkedForTrash().size() > 0) return -1;
            if (this->p2Inventory->getAllCardsInternal().size() > 5) return cardDiff <= 5;
            break;
        case 3:
            cardDiff = this->p3Inventory->getAllCardsInternal().size() - this->p3Inventory->cardsMarkedForTrash().size();
            if (cardDiff < 5 && this->p3Inventory->cardsMarkedForTrash().size() > 0) return -1;
            if (this->p3Inventory->getAllCardsInternal().size() > 5) return cardDiff <= 5;
            break;
        case 4:
            cardDiff = this->p4Inventory->getAllCardsInternal().size() - this->p4Inventory->cardsMarkedForTrash().size();
            if (cardDiff < 5 && this->p4Inventory->cardsMarkedForTrash().size() > 0) return -1;
            if (this->p4Inventory->getAllCardsInternal().size() > 5) return cardDiff <= 5;
            break;
        default:
            this->mW->reportError(tr("Error: invalid player in currentPlayerCanEndTurn(...)"));
    }
    return -2;
}

bool gameState::addBkgScript(int player, QString fileName, QString fnameShort)
{
    QFile f(QFileInfo(fileName).absoluteFilePath());
    if (f.open(QFile::ReadOnly))
    {
        this->bkgScripts.append(std::pair<std::pair<int, QString>, QString>(std::make_pair(player, fnameShort), QString(f.readAll())));
        f.close();
    }
    else
    {
        mainWindow::reportError(tr("Missing file: %1").arg(fileName));
        return false;
    }
    return true;
}

void gameState::aiTurn()
{
    this->doThingsThatNeedToBeDoneAtTurnStart();

    if (this->mW) this->mW->setUIDisabledForAI(true);

    disconnect(this->gameBoard, &board::rotationFinished, this, &gameState::aiTurn);

    if (this->isNetworkTurn()) return;

    if (this->alreadyOver)
    {
        emit this->aiFinished();
        return;
    }

    while (this->currentPlayerCanDrawCard()) this->drawCard(this->currentPlayer, true, true);

    auto count = this->getConfigVariable("aiDifficulty").toInt();
    if (count < 1) count = 100;

    int64_t bestScore = 0;
    aiExecutionPath bestPath;
    int prevHandCardCount = -1;
    mainWindow::random.saveState();
    for (auto i = 0; i < count; ++i)
    {
        gameState* testState = new gameState(this);
        testState->runAIExecPath(aiExecutionPath{});
        auto newScore = testState->getAIScore();
        if (newScore > bestScore
                || (newScore == bestScore && prevHandCardCount < testState->getCurrentPlayerInventory()->getAllCardsInternal().size())
                || (newScore == bestScore && prevHandCardCount == testState->getCurrentPlayerInventory()->getAllCardsInternal().size() && bestPath.length() > testState->aiExecPath.length()))
        {
            bestScore = newScore;
            prevHandCardCount = testState->getCurrentPlayerInventory()->getAllCardsInternal().size();
            bestPath = testState->aiExecPath;
        }
        this->scriptEnv->setGameState(this);
        testState->deleteLater();
        QApplication::sendPostedEvents(nullptr, QEvent::DeferredDelete);
        QApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
        mainWindow::random.restoreSavedState();
    }

    runAIExecPath(bestPath);

    if (this->alreadyOver)
    {
        emit this->aiFinished();
        return;
    }

    while (!(currentPlayerCanEndTurn() == 1 || currentPlayerCanEndTurn() == -2) || this->currentPlayerCanDrawCard())
    {
        while (currentPlayerCanDrawCard())
        {
            this->drawCard(this->getCurrentPlayer(), true, true);
            continue;
        }
        if (!currentPlayerCanDrawCard())
        {
            auto crds = this->getCurrentPlayerInventory()->getAllCardsInternal();
            if (crds.size())
            {
                crds[rng::randomBetweenZeroAndN(crds.size() - 1)]->setMarkedForTrash(true);
            }
        }
    }

    this->aiExecPath = {};

    emit this->aiFinished();
}

QList<int> gameState::teamList(int player)
{
    if (this->gameMode == 1)
    {
        if (player == 1 || player == 3)
        {
            return QList<int>() << 1 << 3;
        }
        else
        {
            return QList<int>() << 2 << 4;
        }
    }
    else
    {
        return QList<int>() << player;
    }
}

QList<int> gameState::getPlayerTurnOrder()
{
    return playerTurnOrder;
}

void gameState::setCheckVictory(bool enabled)
{
    this->sendGameEvent(QStringList() << "setCheckVictory" << QString::number(enabled));
    this->victoryCheckEnabled = enabled;
}

void aiExecutionPath::reset()
{
    this->curr_choice = 0;
}

int16_t aiExecutionPath::getChoice(int16_t maxChoice, int forceValue)
{
    if (curr_choice < choice.size()) //replay
    {
        ++curr_choice;
        if (curr_choice == choice.size()) atEnd = true;
        if (maxChoice != maxChoices[curr_choice - 1])
        {
            mainWindow::reportError("Error: AI divergence, unplanned action taken");
            if (maxChoice) return forceValue > -1 ? forceValue : (rand() % maxChoice);
            return forceValue > -1 ? forceValue : 0;
        }

        return choice[curr_choice - 1];
    }
    else   //new choice
    {
        ++curr_choice;
        int16_t ch = 0;
        if (maxChoice) ch = forceValue > -1 ? forceValue : (rand() % maxChoice);
        choice.push_back(ch);
        maxChoices.push_back(maxChoice);
        return ch;
    }
}

bool aiExecutionPath::isRealRun() const
{
    return curr_choice < choice.size() || atEnd;
}

int aiExecutionPath::length()
{
    return this->choice.size();
}

#ifdef BAZAAR_DEBUG
void aiExecutionPath::addDebugData(const QString& data)
{
    if (curr_choice < choice.size() || atEnd)
    {
        /*
        QTextStream out(stdout);
        if (!(curr_choice == 0 || curr_choice > size_t(debugData.size())))
        {
            out << "AI  Plan: " << this->debugData[curr_choice - 1] << endl;
        }
        else
        {
            out << "AI  Plan: ??????" << endl;
        }
        out << "AI Final: " << data << endl;
        out.flush();
        */
    }
    else
    {
        this->debugData.append(data);
    }
}
#endif

QList<std::pair<std::pair<int, QString>, QString>> gameState::bkgScripts{};

QStringList gameState::scriptExecInfo{};
