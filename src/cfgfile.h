/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CFGFILE_H
#define CFGFILE_H

#include <QString>
#include <QObject>
#include <QStringList>
#include <QMap>

class cfgFile
{
    public:
        cfgFile() {}
        cfgFile(const QString& fileName);
        QStringList readValues(const QString& key) const;
        QStringList readLocalizedValues(const QString& key, const QString& loc) const;
        QString readValue(const QString& key) const;
        QString readLocalizedValue(const QString& key, const QString& loc) const;
        QString fileName() const;
        bool hasKey(const QString& key) const;
        bool hasLocalizedKey(const QString& key, const QString& loc) const;
        void writeValues(const QString& key, const QString& value);
        void writeValues(const QString& key, const QStringList& value);

    private:
        QMap<QString, QStringList> data;
        void save();
        QString fName;
};

#endif // CFGFILE_H
