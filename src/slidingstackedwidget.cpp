/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <QPropertyAnimation>
#include <QCoreApplication>
#include <QParallelAnimationGroup>
#include "slidingstackedwidget.h"
#include "mainwindow.h"

slidingStackedWidget::slidingStackedWidget(QWidget* parent) : QStackedWidget(parent)
{
    mVertical = false;
    mSpeed = 500;
    mAnimationtype = QEasingCurve::OutQuad;
    mNow = 0;
    mNext = 0;
    mWrap = false;
    mPnow = QPoint(0, 0);
    mActive = false;
}

void slidingStackedWidget::setVerticalMode(bool vertical)
{
    mVertical = vertical;
}

void slidingStackedWidget::setSpeed(int speed)
{
    mSpeed = speed;
}

void slidingStackedWidget::setAnimation(QEasingCurve::Type animationtype)
{
    mAnimationtype = animationtype;
}

void slidingStackedWidget::setWrap(bool wrap)
{
    mWrap = wrap;
}

void slidingStackedWidget::slideInNext()
{
    int now = currentIndex();
    if (mWrap || (now < count() - 1)) slideInIndex(now + 1);
}

void slidingStackedWidget::slideInPrevious()
{
    int now = currentIndex();
    if (mWrap || (now > 0)) slideInIndex(now - 1);
}

void slidingStackedWidget::slideInIndex(int idx, AnimationDirection direction)
{
    if (idx > count() - 1)
    {
        direction = mVertical ? AnimationDirection::TopToBottom : AnimationDirection::RightToLeft;
        idx = (idx) % count();
    }
    else if (idx < 0)
    {
        direction = mVertical ? AnimationDirection::BottomToTop : AnimationDirection::LeftToRight;
        idx = (idx + count()) % count();
    }
    slideInWidget(widget(idx), direction);
}

void slidingStackedWidget::slideInWidget(QWidget* newWidget, AnimationDirection direction)
{
    if (!mainWindow::getConfigVariable("slidingAnim").toInt())
    {
        this->setCurrentWidget(newWidget);
        return;
    }

    if (mActive)
    {
        while (mActive) qApp->processEvents();
        //at the moment, do not allow re-entrance before an animation is completed.
        //other possibility may be to finish the previous animation abrupt, or
        //to revert the previous animation with a counter animation, before going ahead
        //or to revert the previous animation abrupt
        //and all those only, if the newwidget is not the same as that of the previous running animation.
    }
    else mActive = true;

    AnimationDirection directionhint;
    int now = currentIndex();
    int next = indexOf(newWidget);
    if (now == next)
    {
        mActive = false;
        return;
    }
    else if (now < next)
    {
        directionhint = mVertical ? AnimationDirection::TopToBottom : AnimationDirection::RightToLeft;
    }
    else
    {
        directionhint = mVertical ? AnimationDirection::BottomToTop : AnimationDirection::LeftToRight;
    }
    if (direction == AnimationDirection::Automatic)
    {
        direction = directionhint;
    }

    //calculate the shifts
    int offsetX = frameRect().width();
    int offsetY = frameRect().height();

    //the following is important, to ensure that the new widget
    //has correct geometry information when sliding in first time
    widget(next)->setGeometry(0,  0, offsetX, offsetY);

    if (direction == AnimationDirection::BottomToTop)
    {
        offsetX = 0;
    }
    else if (direction == AnimationDirection::TopToBottom)
    {
        offsetX = 0;
        offsetY = -offsetY;
    }
    else if (direction == AnimationDirection::RightToLeft)
    {
        offsetX = -offsetX;
        offsetY = 0;
    }
    else if (direction == AnimationDirection::LeftToRight)
    {
        offsetY = 0;
    }

    //re-position the next widget outside/aside of the display area
    QPoint pnext = widget(next)->pos();
    QPoint pnow = widget(now)->pos();
    mPnow = pnow;

    widget(next)->move(pnext.x() - offsetX, pnext.y() - offsetY);

    //make it visible/show
    widget(next)->show();
    widget(next)->raise();

    //animate both, the now and next widget to the side, using animation framework
    QPropertyAnimation* animnow = new QPropertyAnimation(widget(now), "pos");

    animnow->setDuration(mSpeed);
    animnow->setEasingCurve(mAnimationtype);
    animnow->setStartValue(QPoint(pnow.x(), pnow.y()));
    animnow->setEndValue(QPoint(offsetX + pnow.x(), offsetY + pnow.y()));
    QPropertyAnimation* animnext = new QPropertyAnimation(widget(next), "pos");
    animnext->setDuration(mSpeed);
    animnext->setEasingCurve(mAnimationtype);
    animnext->setStartValue(QPoint(-offsetX + pnext.x(), -offsetY + pnext.y()));
    animnext->setEndValue(QPoint(pnext.x(), pnext.y()));

    QParallelAnimationGroup* animgroup = new QParallelAnimationGroup;

    animgroup->addAnimation(animnow);
    animgroup->addAnimation(animnext);

    connect(animgroup, &QParallelAnimationGroup::finished, this, &slidingStackedWidget::animationDoneSlot);
    mNext = next;
    mNow = now;
    mActive = true;
    animgroup->start();

    //the rest is done via a connect when the animation is finished;
    //animation->finished() provides a signal when animation is done;
    //so we connect this to some post processing slot,
    //that we implement here in animationDoneSlot
}

void slidingStackedWidget::paintEvent(QPaintEvent* event)
{
    QStackedWidget::paintEvent(event);
}

void slidingStackedWidget::animationDoneSlot()
{
    //When ready, call the QStackedWidget slot setCurrentIndex(int)
    setCurrentIndex(mNext);
    //then hide the outshifted widget now, and (may be done already implicitely by QStackedWidget)
    widget(mNow)->hide();
    //then set the position of the outshifted widget now back to its original
    widget(mNow)->move(mPnow);
    //so that the application could also still call the QStackedWidget original functions/slots for changings
    mActive = false;
    emit animationFinished();
}
