/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <map>
#include <QMainWindow>
#include <QSettings>
#include <QFileInfoList>
#include <QProxyStyle>
#include <QLabel>
#include <QtPlugin>
#include <QTreeWidget>
#include <QCloseEvent>
#include <QPixmap>
#include <QMessageBox>
#include "datapackage.h"
#include "network.h"
#include "gamestate.h"
#include "cfgfile.h"
#include "fileformat.h"

class board;
class cardInventory;
class graphicsViewMenu;
class rng;
class musicPlayer;
class card;
class scriptExecutionEnvironment;
class QNetworkAccessManager;
class QComboBox;

namespace network
{
    class networkConnectionManager;
}
namespace Ui
{
    class mainWindow;
}

class proxyStyle : public QProxyStyle
{
    public:
        proxyStyle(const QString& name) :  QProxyStyle(name) {}

        virtual QIcon standardIcon(StandardPixmap px, const QStyleOption* opt, const QWidget* w) const override;
};

class mainWindow : public QMainWindow
{
        Q_OBJECT

    public:
        explicit mainWindow(QWidget* parent = nullptr, QSettings* settings = nullptr);
        musicPlayer* mPlayer = nullptr;
        static rng random;
        static QStringList playerNicknames;
        static QString startingDeck;
        static QString cardPictureSet, cardNamePictureSet;
        Ui::mainWindow* ui;
        ~mainWindow() override;
        static QString processIncludes(const QString& code);
        fileFormat::saveFile save(bool forOOSCheck = false) const;
        network::networkConnectionManager* networkManager = nullptr;
        gameState* state = nullptr;
        bool isPassiveTurn() const;
        bool isGamePageVisible() const;
        int singleLocalUser() const;
        static QString getSavePath();
        static QString readStyleSheet();
        int uiDisabledCounter = 0;
        friend class gameState;
        static QString getNewUsername();
        void endGame(bool loadgame = false, bool toPuzzleScreen = false, bool relayDisconnected = false);
        static bool privateGame;
        static QString secretKey;
        QTimer* publicServerKeepAliveTimer = nullptr;
        QList<network::networkDataPackage> gameEvents;
        QList<network::networkDataPackage> replayQueue;
        void playSoundEffect(const QString& name, bool everyone);
        static bool replayMode;
        static bool fastForward;
        QMap<QString, cfgFile> decks;
        QMap<QString, cfgFile> boards;
        static QPair<QList<QPair<QString, int>>, QList<QPair<QString, int>>> getDeckCardList(const QString& cardList);
        QString lastPuzzleSaveFile;
        QString lastPuzzleScript;
        bool gameInProgress = false;
        void applyStylesheetBugWorkAround();
        static cfgFile autoCfg;
        static QMessageBox::StandardButton msgBoxQuestion(QWidget *parent, const QString &title, const QString &text);
        static QMessageBox::StandardButton msgBoxInfo(QWidget *parent, const QString &title, const QString &text);

    public slots:
        static QString getConfigVariable(const QString& key);
        void setConfigVariable(const QString& key, const QString& value);
        bool initializeGame(const QString& fileName, const QString& script, int rngSeed, bool load_from_ui);
        void updateEndTurnButtonState();
        void replayFromSave(const QString& fileName);
        void startTitleSequence();
        void drawOrientationOnSideBarCardGV();
        void setReplayMode(bool replayMode);
        void displayCardOnSideBar(const QString& id, bool delayed = false);
        static QStringList getPlayerNickNames();
        void startWaitingForPublicGameJoin();
        void renderCards();
        void disconnectFromCurrentNetworkGame();
        void serverInfoReceived(const QString& info);
        void usernameAlreadyInUse();
        void wrongPassword();
        void reorderInventoryTabs();
        void switchToPlayerInventory(int playerNum);
        void setUIDisabledForUserChoice(bool disabled);
        void setUIDisabledForAnimation(bool disabled);
        void setUIDisabledForAI(bool disabled);
        bool shortcutsEnabled();
        void saveGame();
        void disableGUIForPassiveTurn();
        void joinPublicGameWithPassword();
        void popInventoryTab();
        void pushInventoryTab(QWidget* oldTab, QWidget* newTab);
        void loadGameDirectly(QString fileName, QString script);
        static QString getLanguageCode();
        static void reportError(const QString& error);
        void updateDisplayedLogs();
        void updateScoreTable();
        QString getTagTranslation(const QString& tag);
        int getTagWeight(const QString& tag);
        void joinGame();
        void handleNetworkClientConnected(const QString& uname);
        void handleGameEvent(const network::networkDataPackage& pkg);
        void handleNetworkLobbyChatMessage(const QString& user, const QString& msg);
        void handleNetworkLobbyUpdate(const network::networkDataPackage& pkg);
        void sendGameEvent(const QStringList& gameEvent, bool force = false);
        void updateDisabledCardsInInventories();
        bool isObserver();
        bool updateNewGameUIForLoadedGame(const QString& path);
        void showPuzzleScreen();
        void updateCurrentPlayerAndScoreLabels();

    signals:
        void cancelButtonClicked();

    protected:
        void keyPressEvent(QKeyEvent* ev) override;
        void closeEvent(QCloseEvent*) override;

    private:
        /*!
        Fills out the settings UI form with the current settings
        source 0: opened from main menu
        source 1: opened from in-game
        source 2: opened from lobby
        */
        void setupSettingsUI(int source = 0);
        bool waitingForPublicGameJoin = false;
        bool waitingForAddressJoin = false;
        bool ongoingNetworkGame = false;
        QStringList getObservers();
        void updateObserverList();
        QString tutorialPageCountToFilename(const QString& lang, int cnt);
        QTimer* drawOrientationOnSidebarDelayTimer = nullptr;
        //Returns true for events where replay stops after the event
        bool processGameEvent(const network::networkDataPackage& pkg);
        void showEditor(QWidget* sourceWidget, QString catName = QString(), const QList<QTreeWidgetItem*>& board_id = {}, const QList<QTreeWidgetItem*>& deck_id = {}, bool allow_board_change = false, bool allow_deck_change = false);
        bool crdRedrawInProgress = false;
        QStringList getGameDataUpdateForPublicServer();
        QString saveGameAfterError();
        void enableGUIAfterPassiveTurn();
        QWidget* editorSourceWidget;
        QPair<QString, QString> getGameFileForTutorialPage(int pagenum);
        void refillPlayerTypeComboBoxes(QStringList networkNames = {});
        static QSettings* settings;
        static mainWindow* instance;
        void cleanUpAfterUnloadingGame();
        bool doNotSendGameEvents = true;
        bool doNotProcessGameEvents = false;
        QString currentlyDisplayedOnSideBarID;
        void setTeamLabels(const QStringList& l);
        void loadBoardsDecks();
        void fillBoardsDecksTreeWidgets();
        bool isBoardDeckValid(const QString& board, const QString& deck);
        bool networkdbg = false;
        QString textForGameMode(int gameMode) const;
        bool waitingForHotSeat = false;
        QString loadedGameName;
        QMap<QString, card*> displayCardsCache;
        QTimer* publicServerTimer = nullptr;
        int zoomCounter = 0;
        int detectTutorialPageCount();
        void resetSettings();
        void resetPuzzleScreen();
        int settingsOpenSource = 0;
        int currTutorialPage = 0;
        QString helpOpenSource;
        QList<QLabel*> deckLabels;
        QFileInfoList puzzles;
        QNetworkAccessManager* nam = nullptr;
        void publicServerKeepAlive();
        void publicServerRegisterGame();
        static int gameModeToIdx(int g);
        QString textForBoard(const QString& board);
        QString textForDeck(const QString& deck);
        QString getBoardPreviewPixmapPath(const QString& board);
        void publicServerRemoveGame();
        QString selectRandomDeck(const QString& tag);
        QString selectRandomBoard(const QString& tag);
        QString selectBoardForDeck();
        QString selectDeckForBoard();
        void clearRandomBoardDeck();
        QString serializeLobbyBoardSelection() const;
        QString serializeLobbyDeckSelection() const;
        void setSelectedDeckByFilename(const QString& name);
        void setSelectedBoardByFilename(const QString& name);
        void setLobbyDeckFromEditor(const QString& name);
        void setLobbyBoardFromEditor(const QString& name);
        QStringList getOOSCheckData();
        void checkOOS(const QStringList& remoteData);
        void setLobbyBoardSelectionFromSerializedState(const QString& state);
        void setLobbyDeckSelectionFromSerializedState(const QString& state);
        QString getLobbySelectedDeck();
        QString getLobbySelectedBoard();
        bool disableLobbyUpdates = false;
        bool hostMode = true;
        QString hostUsername;
        QString gameIDToJoin;
        bool uiDisabledForAnim = false;
        network::networkDataPackage getLobbyState() const;
        int findDataInComboBox(QComboBox* comboBox, const QString& data, int itemIdx = 0);
        cardInventory* getInventoryFromID(int id) const;
        QString loadedGameFileContent;
        bool _editorAllowBoardChange = false;
        bool _editorAllowDeckChange = false;
        QString errorReportBackLocation = "";
        QString randomBoard;
        QString randomDeck;
        QTimer* replayTimer = nullptr;
        QPixmap errorReportScreenShot;
        fileFormat::saveFile errorReportSave;
        void updatePublicServerPage(const QStringList& pubServerData);
        void updatePublicServerSecretKey(const QString& gameIDToJoin, const QString& key);
        QList<QWidget*> inventoryTabStack;
        bool backToTutorialFromGame = false;

    private slots:
        void resetMenuPage();
        void filter(const QString& text, QTreeWidget* widget);
        void updateTipOfTheDay();
        void lobbyChangedByUser(const QString& changeSource = "other");
        void updateLobbyBoardDeckSelector(const QString& changeSource = "other");
        void clearBoardDeckSelectorSidebar();
        void setSelectedBoardInLobby(QTreeWidgetItem* it, bool noJump = false);
        void setSelectedDeckInLobby(QTreeWidgetItem* it, bool noJump = false);
        void aUserDisconnectedFromGame(const QString& username);
        void sendLobbyChatMsg();
        void updateNewGamePagePlayerSymbols(const QString& board);
        void startErrorReport(const QString& from);
        void saveErrorReport();
        void openHelp(QString source);
        void sendChatMsg();
        void contHotSeat();
        void updateLobby(const network::networkDataPackage& data = {});
        void sendReqToPublicServer(const QString& type, const QStringList& data);
        void zoomIn();
        void replayNextGameEvent();
        void zoomOut();
        void setDeckListHighlight(const QString& board);
        void setBoardListHighlight(const QString& deck);
        void updateBoardDeckSelectorSidebar();
        void zoomReset();
        void updateCardLabels();
        void titleSequenceFinished();
        void resetStartGameScreen();
        void setPlayerSlotVisible(int slot, bool visible);
        void gameModeComboBoxChanged(int index);
        void clearPlayerSlots();
        void enableStartGameButtonIfPossible();
};

#endif // MAINWINDOW_H
