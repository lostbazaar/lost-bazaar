/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NETWORK_H
#define NETWORK_H

#include <QString>
#include <QTcpSocket>
#include <QObject>
#include <QTcpServer>
#include <QSet>
#include "datapackage.h"

class mainWindow;

namespace network
{
    /*!
    Enum of network game modes
    */
    enum class NetworkGameMode
    {
        ClientMode,
        HostMode
    };

    /*!
    Class for communication between game clients.
    Usable as both server and client.
    The username *must always be* a unique identifier of the clients (and server).
    */
    class networkConnectionManager : public QObject
    {
            Q_OBJECT

        public:
            networkConnectionManager();
            void start(NetworkGameMode mode, const QString& hostUsername, const QString& password);
            /*!
            Returns with the object's current mode (client or host)
            */
            NetworkGameMode getMode() const;

        signals:
            void aUserDisconnected(const QString& username);
            void clientConnected(const QString& uname);
            void gameEvent(const network::networkDataPackage& pkg);
            void lobbyChatMessage(const QString& user, const QString& msg);
            void lobbyUpdate(const network::networkDataPackage& pkg);
            void connectedToHost();
            void nameAlreadyInUse();
            void wrongPassword();
            void serverInfoReceived(const QString& info);
            void relayDisconnected();
            void relayConnected();
            void publicGameKeyReceived(const QString& gameID, const QString& secretKey);
            void publicLobbyDataReceived(const QStringList& data);

        private:
            QTcpSocket* internalTcpSocket;
            QTcpServer* internalTcpServer;
            QTcpSocket* relaySocket;
            NetworkGameMode m;
            QString password;
            QString relayedHostIP;
            QSet<QString> relayClients;
            QString getCurrentPublicGameID() const;
            /*!
             * Tries to add the given socket as a client if the username is not already in use,
             * return true if successful
             */
            bool tryAddUser(const QString& username);
            QString currPubID;

        public slots:
            /*!
            Disconnects from the game (in client mode) or ends the game (in host mode) and cleans up all connections
            */
            void reset(bool resetRelay = true);
            /*!
            Sends a network data package to the host (in client mode) or to all clients (in host mode).
            */
            void sendMessage(network::networkDataPackage package);
            /*!
            Kicks the client with the given username (used only when in host mode)
            */
            void kickClient(const QString& username);
            void sendMessageToUser(const QString& username, const network::networkDataPackage& pkg);
            void connectToRelay(QString addr);
            void resetRelay();
            void setCurrentPublicGameID(const QString& ID, const QString& password);
            void sendRelayMsg(const QStringList& data);
            void disconnectFromCurrentGame();

        private slots:
            /*!
            Closes and removes all disconnected and/or invalid TCP sockets
            */
            void clearClientConnections(bool all = false, const QString& relayID = {});
            /*!
            Handles incoming messages
            */
            void handleNewMessage(QTcpSocket* s = nullptr);
            /*!
            Sends a network data package to the given socket (only in host mode!).
            The second parameter is the destination, the third is the original sender.
            */
            void sendMessageToSocket(network::networkDataPackage package, const QString& relayID);
            /*!
            Forward message to all clients except the original sender
            */
            void forwardMessageToClients(const QString& origSenderRelay, const network::networkDataPackage& msg);
    };
}

#endif // NETWORK_H
