/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CARDINVENTORY_H
#define CARDINVENTORY_H

#include <QGraphicsView>
#include "mainwindow.h"
#include "fileformat.h"

class card;
class graphicsPixmapItem;

using CardList = QList<QObject*>;

class cardInventory : public QGraphicsView
{
        Q_OBJECT

    public:
        cardInventory(QWidget* parent = 0);
        /*!
        Adds the card to the inventory
        */
        Q_SCRIPTABLE void addCard(card* crd);
        /*!
        Returns the selected card, or null if the choice was cancelled.
        The first parameter is a list of card pointers not UIDs.
        If the second parameter is true, the user can cancel the choice otherwise they can'.t
        The third parameter specifies which overlay to use for which card (see the InventoryCardOverlay enum).
        */
        Q_SCRIPTABLE card* cardChoice(CardList cardList, bool cancellable, QList<int> overlayList = QList<int>());
        /*!
        Moves the card with the given uID from the inventory to the board
        If the third parameter is true the card's bronze ability will be activated (if it has one)
        */
        Q_SCRIPTABLE card* moveCardToBoard(int uID, QPoint targetPosition, bool activateAbility);
        Q_SCRIPTABLE card* moveCardToBoard(int uID, QPointF targetPosition, bool activateAbility);
        void setGameState(gameState* gS);
        void setTab(QWidget* t);
        /*!
        Sets this inventory as the Storage
        */
        void setStorage(bool t);
        /*!
        Sets this inventory as the Trash
        */
        void setTrash(bool t);
        /*!
        Sets this inventory read-only
        */
        void setReadOnly(bool r);
        /*!
        Sets this inventory public (visible)
        */
        Q_SCRIPTABLE void setViewable(bool p, bool setActive = false);
        /*!
        Returns true if the inventory is currently visible
        */
        Q_SCRIPTABLE bool isViewable();
        void reinitialize();
        void clear();
        QList<card*> cardsMarkedForTrash();
        /*!
        Returns with a list of cards in the inventory
        */
        Q_SCRIPTABLE CardList getAllCards() const;
        QList<card*> getAllCardsInternal() const;
        /*!
        Removes the card with the given uID from the inventory and returns it
        */
        Q_SCRIPTABLE card* removeCard(int uID);
        void setHucksterChoiceMode(bool h);
        void setHuckster(bool huckster);
        /*!
        Returns true if there is a card with the name in this inventory.
        */
        bool hasCard(const QString& name) const;
        bool hasPlayableCard();
        bool isHucksterChoiceModeActive() const;
        void endHucksterChoice();
        void removeCardsMarkedForTrash(cardInventory* trash);
        /*!
        Returns true if the inventory is empty
        */
        Q_SCRIPTABLE bool isEmpty() const;
        fileFormat::saveFile save(bool forOOSCheck = false) const;
        void load(fileFormat::saveFile f);
        void updateDisabledCards();
        int indexOf(card* c) const;
        bool contains(card* c) const;
        cardInventory* clone(gameState* newState) const;
        ~cardInventory();

    signals:
        void changed();
        void stopTileChoice();

    public slots:
        /*!
        Moves all cards which were marked for trash to the trash
        */
        void moveCardsToTrash();
        void setPlayer(int player);
        void cancelChoice();
        void orderCardsAlpha();

    private slots:
        void cardClicked();
        void cardDragged();
        void cardRightClicked();
        void cardMouseEntered();
        void cardMouseLeft();

    private:
        int getInventoryID() const;
        card* removeCardByPtr(card* crd);
        gameState* gS = nullptr;
        bool isStorage = false;
        bool isHuckster = false;
        bool viewable = false;
        QString tabtext;
        card* placeCard = nullptr;
        bool rejectNextMouseEnterEvent; //only workaround
        bool trash = false;
        bool ro = true;
        int player = -1;
        bool choiceMode = false;
        card* choice = nullptr;
        bool hucksterChoiceMode = false;
        int currentPage = 0;
        QWidget* tab = nullptr;
        QList<card*> contents;
        void redraw();
};

#endif // CARDINVENTORY_H
