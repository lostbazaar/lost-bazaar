/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FADINGLABEL_H
#define FADINGLABEL_H

#include <QObject>
#include <QLabel>
#include <QElapsedTimer>
#include <QTimer>

class mainWindow;

class clickableLabel : public QLabel
{
        Q_OBJECT

    public:
        explicit clickableLabel(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags()) : QLabel(parent, f) {}
        ~clickableLabel() {}

    signals:
        void clicked();

    protected:
        void mousePressEvent(QMouseEvent*)
        {
            emit this->clicked();
        }
};

/*!
A label class with support for fading in/out text and pictures
*/
class fadingLabel : public QLabel
{
        Q_OBJECT

    public:
        fadingLabel(QWidget* parent = 0);
        /*!
        Starts the fade sequence
        */
        void start();
        void setMainWindow(mainWindow* mw);
        /*!
        The first parameter is the list of texts to be faded in/out.
        If an item ends with .png, it is interpreted as a filename.
        The second parameter is a vector of integers, its size must be three times the length of
        the first parameter + 1. It contains the fade in / wait / fade out times (in milliseconds) for every item in the first parameter.
        Its last entry is the fade in time of the final pixmap which is shown at the end of the fade sequence (this pixmap is the 3rd parameter).
        */
        void setData(const QStringList& textSeq, const QList<int>& seq, QPixmap* px);

    private:
        enum class LabelTimerState
        {
            FadeIn,
            FadeOut,
            Visible
        };
        LabelTimerState currentState;
        QTimer* timer = nullptr;
        QStringList fSeq;
        QList<int> timerSeq;
        QPixmap* finalPixmap = nullptr;
        int fadeDuration = 0;
        int t;
        bool doneEmitted;
        bool d;
        mainWindow* mw = nullptr;
        QElapsedTimer elapsedTime;

    signals:
        /*!
        Emitted when the fade sequence is finished or interrupted
        */
        void done();

    protected:
        void mousePressEvent(QMouseEvent* ev);

    private slots:
        void timerFinished();
        void timerStep();
};

#endif // FADINGLABEL_H
