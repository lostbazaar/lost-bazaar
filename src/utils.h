/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UTILS_H
#define UTILS_H

#include <QEvent>
#include <QObject>
#include <QPoint>
#include <QList>
#include <QJSValue>
#include <QJSEngine>
#include "card.h"

Q_DECLARE_METATYPE(QList<card*>)
Q_DECLARE_METATYPE(card*)

class endGameEventFilter : public QObject
{
        Q_OBJECT

    public:
        endGameEventFilter() {}
        virtual ~endGameEventFilter() {}
    protected:
        bool eventFilter(QObject*, QEvent* event);
};

class bazaarEvent : public QEvent
{
    public:
        bazaarEvent(const QString& data, mainWindow* mW);
        virtual ~bazaarEvent();
        QString data;
        mainWindow* mW = nullptr;
};

void delay(int msecs);

QJSValue QPointToScriptValue(QJSEngine* e, QPoint const& o);
void QPointFromScriptValue(const QJSValue& val, QPoint& o);

template<typename T>
QJSValue customObjectToScriptValue(QJSEngine* e, T const& o)
{
    return e->newQObject(o);
}

template<typename T>
void customObjectFromScriptValue(const QJSValue& val, T& o)
{
    o = qobject_cast<T>(val.toQObject());
}

void setElidedText(QLabel* label, const QString& text, int width);
void setElidedTabText(QTabWidget* tw, int idx, const QString& text, int width);

QString languageConfigValueToLocaleName(const QString& configValue);

QJSValue cardListToScriptValue(QJSEngine* e, const QList<card*>& o);
void cardListFromScriptValue(const QJSValue& val, QList<card*>& o);
QJSValue pointListToScriptValue(QJSEngine* e, const QList<QPoint>& o);
void pointListFromScriptValue(const QJSValue& val, QList<QPoint>& o);
QJSValue intVectorToScriptValue(QJSEngine* e, const QList<int>& o);
void intVectorFromScriptValue(const QJSValue& val, QList<int>& o);

bool operator<(const QList<int>& l1, const QList<int>& l2);

QStringList trimStringList(QStringList l);

#endif // UTILS_H
