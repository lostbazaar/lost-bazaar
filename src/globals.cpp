/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "globals.h"
#include "mainwindow.h"

#ifdef Q_OS_WIN
QString lineEnd = "\r\n";
#else
QString lineEnd = "\n";
#endif

QString getPrimaryPath(const QString& requestedPath, bool silent)
{
    if (QFileInfo::exists(QStandardPaths::standardLocations(QStandardPaths::ConfigLocation).constFirst() +
                  "Lost Bazaar/usercontent/" + requestedPath))
    {
        return QStandardPaths::standardLocations(QStandardPaths::ConfigLocation).constFirst() +
               "Lost Bazaar/usercontent/" + requestedPath;
    }
#if !defined(Q_OS_ANDROID) && !defined(Q_OS_WASM)
    else if (QFileInfo::exists(DATA_PATH "/" + requestedPath))
    {
        return DATA_PATH "/" + requestedPath;
    }
    else
    {
        if (!silent)
        {
            QTextStream out(stdout);
            out << qApp->translate("global", "Error: requested file or path does not exist: %1").arg(requestedPath) << Qt::endl;
        }
        return QString();
    }
#else
    else
    {
        QString p = DATA_PATH "/" + requestedPath;
        p = p.replace("//", "/");
        if (QFileInfo(p).exists())
        {
            return p;
        }
        else
        {
            if (!silent)
            {
                QTextStream out(stdout);
                out << qApp->translate("global", "Error: requested file or path does not exist: %1").arg(requestedPath) << Qt::endl;
            }
            return QString();
        }
    }
#endif
}

QString userContentPath()
{
    return QStandardPaths::standardLocations(QStandardPaths::ConfigLocation).constFirst() +
           "Lost Bazaar/usercontent/";
}

QPixmap getPrimaryPathScaled(const QString& requestedPath, int originalSize)
{
    if (mainWindow::getConfigVariable("hiresMode").toInt())
    {
        auto scale_factor = 1.0;
        auto hq_path = getPrimaryPath(requestedPath.left(requestedPath.size() - 4) + "_hires.png", true);
        if (hq_path.isEmpty())
        {
            QPixmap px = QPixmap(getPrimaryPath(requestedPath));
            if (px.isNull()) return px;
            if (px.size().width() != originalSize)
            {
                QTextStream out(stdout);
                out << "Missing high resolution image: " << requestedPath.left(requestedPath.size() - 4) + "_hires.png" << Qt::endl;
                if (originalSize >= 0)
                {
                    px = px.scaled(originalSize * scale_factor, px.height() * (double(originalSize) / px.width()) * scale_factor, Qt::KeepAspectRatio, Qt::SmoothTransformation);
                }
                else if (originalSize == -1)  //scaling used on the board
                {
                    px = px.scaled(px.width() * 1.9625 * scale_factor, px.height() * 1.9625 * scale_factor, Qt::KeepAspectRatio, Qt::SmoothTransformation);
                }
                else if (originalSize == -2)  //scaling used on card name pictures in full size drawings
                {
                    px = px.scaled(px.width() * 1.3 * scale_factor, px.height() * 1.3 * scale_factor, Qt::KeepAspectRatio, Qt::SmoothTransformation);
                }
            }
            else
            {
                if (scale_factor != 1.0) px = px.scaled(px.width() * scale_factor, px.height() * scale_factor, Qt::KeepAspectRatio, Qt::SmoothTransformation);
            }
            return px;
        }
        else
        {
            QPixmap px = QPixmap(hq_path);
            if (scale_factor != 1.0) px = px.scaled(px.width() * scale_factor, px.height() * scale_factor, Qt::KeepAspectRatio, Qt::SmoothTransformation);
            return px;
        }
    }
    else
    {
        return QPixmap(getPrimaryPath(requestedPath));
    }
}
