/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BOARD_H
#define BOARD_H

#include <QObject>
#include <QString>
#include <QList>
#include <QKeyEvent>
#include <QGraphicsView>
#include <QMap>
#include <QSize>
#include "fileformat.h"
#include "utils.h"

class card;
class graphicsPixmapItem;
class gameState;

using CardList = QList<QObject*>;

enum class TileOverlay
{
    TransparentOverlay = 0,
    PlayerChoiceOverlay = 1,
    CancelChoiceOverlay = 2,
    CardPlacementOverlay = 3,
};

class board : public QGraphicsView
{
        Q_OBJECT

    public:
        board(QWidget* parent = nullptr);
        /*!
        Draws the board background (tiles and frame)
        */
        void drawBackground(const QString& fName, const QString& bkgName, const QString& tileMapName, const QString& graphicsViewBackgroundName, const QString& whiteName, const QString& yellowName, const QString& blueName, const QString& greenName, const QString& disableName, const QString& orangeName, const QString& magentaName, const QString& pTileFnamePrefix, const QString& tileOverlayFnamePrefix, const QString& coords);
        /*!
        Calls scene()->clear()
        */
        void clear();
        /*!
        Returns a list of all cards on the board
        */
        Q_SCRIPTABLE CardList getAllCards() const;
        QList<card*> getAllCardsInternal() const;
        /*!
        Returns a list of all cards on the board belonging to a player
        */
        Q_SCRIPTABLE CardList getPlayerCards(int player);
        QList<card*> getPlayerCardsInternal(int player);
        /*!
        True if the tile at the given coordinates is a safe tile
        */
        Q_SCRIPTABLE bool isSafeTile(const QPoint& p) const;
        Q_SCRIPTABLE bool isSafeTile(const QPointF& p) const;
        /*!
        True if the tile at the given coordinates is a disabled tile
        */
        Q_SCRIPTABLE bool isDisabledTile(const QPoint& p) const;
        Q_SCRIPTABLE bool isDisabledTile(const QPointF& p) const;
        /*!
        True if the tile at the given coordinates is an innovative tile
        */
        Q_SCRIPTABLE bool isInnovativeTile(const QPoint& p) const;
        Q_SCRIPTABLE bool isInnovativeTile(const QPointF& p) const;
        /*!
        True if the tile at the given coordinates is a double score tile
        */
        Q_SCRIPTABLE bool isDoubleTile(const QPoint& p) const;
        Q_SCRIPTABLE bool isDoubleTile(const QPointF& p) const;
        /*!
        True if the tile at the given coordinates is a power tile
        */
        Q_SCRIPTABLE bool isPowerTile(const QPoint& p) const;
        Q_SCRIPTABLE bool isPowerTile(const QPointF& p) const;
        /*!
        True if the tile at the given coordinates is a fashion tile
        */
        Q_SCRIPTABLE bool isFashionTile(const QPoint& p) const;
        Q_SCRIPTABLE bool isFashionTile(const QPointF& p) const;
        /*!
        Returns with the list of the coordinates of all empty tiles
        */
        QList<QPoint> getEmptyTiles() const;
        /*!
        Returns with the list of the coordinates of all empty tiles (as a list of x,y coordinate pairs)
        */
        Q_SCRIPTABLE QList<int> getEmptyTilesAsIntList() const;
        /*!
        Returns all cards on the board with the given name.
        */
        Q_SCRIPTABLE CardList getCardsWithName(const QString& name) const;
        void setGameState(gameState* state);
        /*!
        Returns the card at the given position (or nullptr if there is no card there)
        */
        Q_SCRIPTABLE card* getCardAtPosition(const QPoint& pos);
        Q_SCRIPTABLE card* getCardAtPosition(const QPointF& pos);
        /*!
        Moves a card from (x1,y1) to (x2,y2)
        */
        Q_SCRIPTABLE void moveCard(const QPoint& from, const QPoint& to);
        Q_SCRIPTABLE void moveCard(const QPointF& from, const QPointF& to);
        /*!
        Swaps cards between (x1,y1) and (x2,y2)
        */
        Q_SCRIPTABLE void swapCards(const QPoint& p1, const QPoint& p2);
        Q_SCRIPTABLE void swapCards(const QPointF& p1, const QPointF& p2);
        /*!
        Do not use this directly, use boardutils.tileChoice instead.
        Prompts the player for a choice between the given tiles.
        Returns the coordinates of the selected tile, returns (-1,-1) if the choice was cancelled by the user
        If the second parameter is true, the user can cancel the choice otherwise they can't
        The third parameter specifies which overlay to use for which point (see the TileOverlay enum)
        */
        Q_SCRIPTABLE QPoint tileChoiceInternal(const QList<QPoint>& points, bool cancellable, const QList<int>& overlayList = QList<int>());
        Q_SCRIPTABLE QPoint tileChoiceInternalIntList(const QList<int>& points, bool cancellable, const QList<int>& overlayList = QList<int>());
        /*!
        Returns the height of the board (in tiles)
        */
        Q_SCRIPTABLE int getHeight() const;
        /*!
        Returns the width of the board (in tiles)
        */
        Q_SCRIPTABLE int getWidth() const;
        int getNumberOfPlayerCards(int player);
        /*!
        Moves the card with the given ID to the trash
        */
        Q_SCRIPTABLE void moveCardToTrash(int uID);
        /*!
        Removes the card with the given UID from the board and returns with it
        */
        Q_SCRIPTABLE card* removeCard(int uID, bool addToTrash = false);
        static QPoint getPixelPosForTilePos(const QPoint& p);
        static QSize getTileSize(const QPoint& p);
        static QPoint getPixelPosForTilePos(const QPointF& p);
        static QSize getTileSize(const QPointF& p);
        static QString getPlayerTileFilenamePrefix();
        static QString getTileOverlayFilenamePrefix();
        fileFormat::saveFile save(bool forOOSCheck = false) const;
        void load(fileFormat::saveFile f);
        board* clone(gameState* newState) const;
        void addCardToBoard(card* crd, const QPoint& p);
        QList<QPoint> getPositionListForCardActivation(int player) const;
        bool contains(card* c) const;
        ~board() override;
        QTimer* rotationTimer = nullptr;

    protected:
        void dragEnterEvent(QDragEnterEvent* event) override;
        void dragMoveEvent(QDragMoveEvent* event) override;
        void dropEvent(QDropEvent* event) override;

    public slots:
        /*!
        Stops card placement
        */
        void stopTileChoice();
        void updateCoordGrid();
        /*!
        Starts card placement
        The user can select a tile from availableCoords and the card will be placed there
        */
        bool placeCard(const QList<QPoint>& availableCoords, card* sender, bool forced, bool noEvent, QWidget* dragSource = nullptr);
        void rotateBoardToPlayer(int playerNum, bool forceRotateAndSkipAnim = false);
        void cancelChoice();

    protected:
        void mousePressEvent(QMouseEvent* event) override;
        void drawBackground(QPainter* painter, const QRectF& rect) override;

    private slots:
        void cardMouseEntered();
        void cardMouseLeft();
        void cardClicked();
        void cardRightClicked();
        void cardMouseHeldDown();
        void rotationStep();
        void handleRightClickAction(QObject* sender);

    signals:
        void rotationFinished();
        void mouseClicked();

    private:
        QMap<int, QMap<int, card*>> cards;
        static QMap<int, QMap<int, QChar>> tiles;
        QList<graphicsPixmapItem*> cardPlacementGraphics;
        QList<graphicsPixmapItem*> playerTileChoiceGraphics;
        int currRotStep;
        int rotSteps;
        int rotDegree;
        bool disableInteractionForRotation = false;
        int prevPlayerNum = -1;
        int w = -1;
        int h = -1;
        QPoint choice;
        QList<QPoint> availC;
        gameState* gS = nullptr;
        /*!
        Doesn't do any drawing
        */
        void getChoice(const QList<QPoint>& points, QWidget* dragSource, card* sender);
        bool waitingForTileChoice;
        void drawCardPlacementGraphics(const QList<QPoint>& availableCoords);
        void removeCardPlacementGraphics();
        void drawPlayerTileChoiceGraphics(const QList<QPoint>& availableCoords, const QList<int>& overlayList);
        void removePlayerTileChoiceGraphics();
        /*!
        Returns the coordinates of the tile (0<=x,y<=7)
        if the given point (in scene coordinates) is on a tile which is available for card placement,
        otherwise returns (-1, -1)
        */
        QPoint positionForChoice(const QPoint& p);
        static QString playerTileFilenamePrefix;
        static QString tileOverlayFilenamePrefix;
        static QMap<int, QMap<int, QPoint>> tilePosMap;
        static QMap<int, QMap<int, QSize>> tileSizeMap;
        QGraphicsPixmapItem* coordsPxItem = nullptr;
        QString tileMapName, bkgName, gviewBkgName, fName, coordsName;
};

#endif // BOARD_H
