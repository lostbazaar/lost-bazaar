#!/usr/bin/python

import os

qrc_template = """
<RCC>
    <qresource prefix="/">
        %%FILES%%
    </qresource>
</RCC>
"""

if os.path.isdir("../data"):
    datadir = "../data"
    targetdir = "../"
else:
    datadir = "./data"
    targetdir = "./"

filelist = []

for root, dirs, files in os.walk(datadir):
   for name in files:
      lname = name.lower()
      if not lname.endswith(".ogg") and not lname.endswith(".wav") and not lname.endswith(".mp3") and not lname.endswith(".wma"):
          path = os.path.join(root, name)
          if path.startswith("./"):
              path = path[2:]
          elif path.startswith("../"):
              path = path[3:]
          path = "<file>"+path+"</file>\n"
          filelist.append(path)

cnt = 0
while len(filelist[100*cnt:100*cnt+100]) > 0:
    filetext = "".join(filelist[100*cnt:100*cnt+100])
    cnt = cnt + 1
    final_content = qrc_template.replace("%%FILES%%", filetext)
    open(targetdir+"/lb"+str(cnt)+".qrc", 'w').write(final_content)
