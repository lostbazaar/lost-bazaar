/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "utils.h"
#include <QTime>

void delay(int msecs)
{
    QTime dieTime = QTime::currentTime().addMSecs(msecs);
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
}

QJSValue cardListToScriptValue(QJSEngine* e, const QList<card*>& o)
{
    QJSValue val = e->newArray(o.size());
    for (int i = 0; i < o.size(); i++)
    {
        val.setProperty(i, e->newQObject(o[i]));
    }
    return val;
}

void cardListFromScriptValue(const QJSValue& val, QList<card*>& o)
{
    int i = 0;
    while (val.hasProperty(QString::number(i)))
    {
        o.append(qobject_cast<card*>(val.property(i).toQObject()));
        i++;
    }
}

QJSValue QPointToScriptValue(QJSEngine* e, QPoint const& o)
{
    QJSValue obj = e->newObject();
    obj.setProperty("x", QJSValue(o.x()));
    obj.setProperty("y", QJSValue(o.y()));
    return obj;
}

void QPointFromScriptValue(const QJSValue& val, QPoint& o)
{
    if (val.hasProperty("x") && val.hasProperty("y"))
    {
        o.setX(int(val.property("x").toInt()));
        o.setY(int(val.property("y").toInt()));
    }
}

void pointListFromScriptValue(const QJSValue& val, QList<QPoint>& o)
{
    int i = 0;
    while (val.hasProperty(QString::number(i)))
    {
        QPoint p;
        QPointFromScriptValue(val.property(i), p);
        o.append(p);
        i++;
    }
}

QJSValue pointListToScriptValue(QJSEngine* e, const QList<QPoint>& o)
{
    QJSValue val = e->newArray(o.size());
    for (int i = 0; i < o.size(); i++)
    {
        val.setProperty(i, QPointToScriptValue(e, o[i]));
    }
    return val;
}

bool operator<(const QList<int>& l1, const QList<int>& l2)
{
    int m = l1.size() < l2.size() ? l1.size() : l2.size();
    for (int i = 0; i < m; i++)
    {
        if (l1[i] != l2[i])
        {
            return l1[i] < l2[i];
        }
    }
    return false;
}

QJSValue intVectorToScriptValue(QJSEngine* e, const QList<int>& o)
{
    QJSValue val = e->newArray(o.size());
    for (int i = 0; i < o.size(); i++)
    {
        val.setProperty(i, o[i]);
    }
    return val;
}

void intVectorFromScriptValue(const QJSValue& val, QList<int>& o)
{
    int i = 0;
    while (val.hasProperty(QString::number(i)))
    {
        o.append(val.property(i).toInt());
        ++i;
    }
}

QStringList trimStringList(QStringList l)
{
    for (int i = 0; i < l.size(); ++i)
    {
        l[i] = l[i].trimmed();
    }
    return l;
}

void setElidedText(QLabel* label, const QString& text, int width)
{
    label->setText(label->fontMetrics().elidedText(text, Qt::ElideRight, width));
}

void setElidedTabText(QTabWidget* tw, int idx, const QString& text, int width)
{
    tw->setTabText(idx, tw->fontMetrics().elidedText(text, Qt::ElideRight, width));
}

bazaarEvent::bazaarEvent(const QString& data, mainWindow* mW) : QEvent(QEvent::SockAct), data(data), mW(mW) {}

bazaarEvent::~bazaarEvent() {}

QString languageConfigValueToLocaleName(const QString& configValue)
{
    if (configValue == "system")
    {
        return QLocale::system().name();
    }
    if (configValue.isEmpty())
    {
        return DEFAULT_LANGUAGE;
    }
    return configValue;
}
