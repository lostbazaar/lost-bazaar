/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <QPainter>
#include "globals.h"
#include "mainbkgwidget.h"

mainBkgWidget::mainBkgWidget(QWidget* parent) : QWidget(parent)
{
    this->bkgPixmap2 = QPixmap(getPrimaryPath("img/button_bkg_disabled.png"));
    this->bkgPixmap = QPixmap(getPrimaryPath("img/xbazaar art 100.png"));
    this->borderPixmap = QPixmap(getPrimaryPath("img/main_bkg_border.png"));
}

void mainBkgWidget::paintEvent(QPaintEvent*)
{
    QPainter painter(this);
    auto r = this->rect();
    painter.drawTiledPixmap(r, this->bkgPixmap2);
    auto rCenter = r;
    rCenter.setHeight(rCenter.width() * 0.5625);
    if (rCenter.height() > r.height())
    {
        auto scl = r.height() / double(rCenter.height());
        rCenter.setHeight(scl * rCenter.height());
        rCenter.setWidth(scl * rCenter.width());
    }
    rCenter.moveTopLeft(QPoint(r.width() / 2.0 - rCenter.width() / 2.0, r.height() / 2.0 - rCenter.height() / 2.0));
    painter.drawPixmap(rCenter, this->bkgPixmap.scaled(rCenter.width(), rCenter.height(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    rCenter.setHeight(rCenter.height() + (20 * rCenter.height() / double(1080)));
    rCenter.setWidth(rCenter.width() + (20 * rCenter.width() / double(1920)));
    rCenter.moveTopLeft(QPoint(r.width() / 2.0 - rCenter.width() / 2.0, r.height() / 2.0 - rCenter.height() / 2.0));
    painter.drawPixmap(rCenter, this->borderPixmap.scaled(rCenter.width(), rCenter.height(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
}
