/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FILEFORMAT_H
#define FILEFORMAT_H

#include <QMap>
#include <QObject>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QPair>

namespace fileFormat
{
    class saveFile;

    /*!
    This class stores an XML tag (name, value, attributes)
    */
    class saveFileItem
    {
        public:
            saveFileItem();
            saveFileItem(QString _key, QString _value, QMap<QString, QString> _attributes, fileFormat::saveFile* parent);
            QString getValueAsString() const;
            QString getKeyAsString() const;
            QString getRawValueAsString() const;
            QMap<QString, QString> getAllAttributes() const;
            QMap<QString, QString> getAllRawAttributes() const;
            QString getAttributeAsString(QString attr) const;
            QString getRawAttributeAsString(QString attr) const;
            void setStringKey(QString _key);
            void setStringValue(QString _value);
            void setStringAttribute(QString attr, QString value);
            void setParent(fileFormat::saveFile* parent);

        private:
            QString value;
            QString key;
            QMap<QString, QString> attributes;
            fileFormat::saveFile* p = nullptr;
    };

    /*!
    This class stores a gamefile.
    It saves and loads data from file or from a string
    */
    class saveFile
    {
        public:
            saveFile();
            bool loadFromFile(QString fileName, bool compressed);
            void saveToFile(QString fileName, bool compressed);
            qint64 itemCount() const;
            uint getHash() const;
            QString toString(bool compressed) const;
            bool fromString(QString str, bool compressed);
            void clear();
            saveFileItem getItemByKey(QString key) const;
            saveFileItem getItemByIndex(quint64 index) const;
            void addItem(QString key, QString value, QMap<QString, QString> attributes = QMap<QString, QString>());
            void addItem(saveFileItem item);
            void addItems(fileFormat::saveFile f);
            /*!
            Checks if two saveFile objects' content is equal
            */
            static bool isContentEqual(fileFormat::saveFile* a, fileFormat::saveFile* b);
            QList<saveFileItem> getItems() const;

        private:
            QList<saveFileItem> items;
    };
}

#endif // FILEFORMAT_H
