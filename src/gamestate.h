/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GAMESTATE_H
#define GAMESTATE_H

#include <QObject>
#include <QPoint>
#include "fileformat.h"
#include "globals.h"

class mainWindow;
class board;
class cardInventory;
class graphicsViewMenu;
class gameState;
class card;
class QJSEngineDebugger;
class QJSEngine;

class aiExecutionPath
{
    public:
        void reset();
        size_t curr_choice = 0;
        bool atEnd = false;
        std::vector<int16_t> choice;
        std::vector<int16_t> maxChoices;
        int16_t getChoice(int16_t maxChoice, int forceValue = -1);
        bool isRealRun() const;
        int length();
#ifdef BAZAAR_DEBUG
        QStringList debugData;
        void addDebugData(const QString& data);
#endif
};

class scriptExecutionEnvironment
{
    public:
        scriptExecutionEnvironment(gameState* state);
        void setGameState(gameState* state);
        /*!
         #info Cards: the JavaScript functions getCurrentValue and getAvailableTiles should NEVER change the state of the game (including user interaction and displaying any text).
         */
        QVariantList executeScript(const QString& code, const QString& funcName, card* currentCard, const QStringList& args = QStringList());
        int getCurrentEngineLevel();
        ~scriptExecutionEnvironment();

    private:
        int currentEngine;
        void addScriptEngine();
        gameState* state;
        QList<QJSEngine*> scriptEngines;
};

class gameState : public QObject
{
        Q_OBJECT

    public:
        gameState(mainWindow* mW = nullptr);
        gameState(gameState* state);
        ~gameState();
        bool loadGame(const fileFormat::saveFile& f, const QString& script, const bool load_from_ui);
        board* gameBoard = nullptr;
        cardInventory* storage = nullptr;
        cardInventory* p1Inventory = nullptr;
        cardInventory* p2Inventory = nullptr;
        cardInventory* p3Inventory = nullptr;
        cardInventory* p4Inventory = nullptr;
        cardInventory* huckster = nullptr;
        cardInventory* trash = nullptr;
        cardInventory* mainInventory = nullptr;
        graphicsViewMenu* menu = nullptr;
        fileFormat::saveFile save(bool forOOSCheck = false) const;
        scriptExecutionEnvironment* scriptEnv = nullptr;
        int currentPlayerCanEndTurn() const;//-2: indeterminate, -1: too few cards, 0 too many cards, 1 if true
        bool currentPlayerCanDrawCard() const;
        aiExecutionPath aiExecPath;
        bool isNetworkTurn() const;
        void checkConsistency();
        mainWindow* mW;
        int64_t getAIScore();
        void runAIExecPath(const aiExecutionPath& p);
        cardInventory* getCurrentPlayerInventory();
        std::map<int, QStringList> logs;
        bool p1AI = false;
        bool p2AI = false;
        bool p3AI = false;
        bool p4AI = false;
        bool p1Network = false;
        bool p2Network = false;
        bool p3Network = false;
        bool p4Network = false;
        bool playerPlacedCardOnBoard = false;
        bool unlimitedMoves = false;
        bool silverActionsDisabledForTurn = false;
        int gameMode = -1;
        QList<int> playerTurnOrder;
        int cancelEnabledCounter = 0;
        bool hucksterEnabled = false;
        bool openHands = false;
        QList<int> scoreCache;
        void setCurrentPlayer(int player);
        void setCurrentPlayerDrawnCard(bool d);
        void clear();
        static QStringList scriptExecInfo;

    signals:
        void aiFinished();

    public slots:
        void setPlayerPlacedCardOnBoard(bool placed);
        void setCancelEnabled(bool enabled);
        /*!
         * Plays the sound effect with the given name. If the second parameter is true, the effect will be played for all
         * users (network players too).
         */
        Q_SCRIPTABLE void playSoundEffect(const QString& name, bool everyone);
        /*!
        Draws a card for the given player (from the main inventory)
        If the second parameter is true, this counts as the once-a-turn mandatory drawing of a card,
        so the player won't be forced to draw a card to end his turn after this.
        If the third parameter is true, the card won't be shown on the sidebar preview area.
        The sixth parameter determines the color of the log message (color name as string). Empty string means the default color.
        */
        Q_SCRIPTABLE void drawCard(int playerNum, bool cDrawnCard = false, bool doNotShow = false, bool soundEffect = false, bool soundEffectForEveryone = false, QString logMsgColor = "");
        /*!
        Executes an event.
        Events:
        #event turnStarted [playerNum]
        #event rotationFinished [playerNum]
        #event endOfTurn [playerNum]
        #event cardDrawn [playerNum] [card ID] [card UID]
        #event cardPlacedOnBoard [card UID] [pos X] [pos Y]
        #event cardSilverActionActivated [card UID]
        */
        void runEvent(const QStringList& eventData, card* priorityCard = nullptr);
        /*!
        Returns the number of the current player
        */
        Q_SCRIPTABLE int getCurrentPlayer() const;
        /*!
        Writes the message to the game log of the given player group
        The second parameter is a list of integer player numbers, for example, use [1,2] to write to the log of the first and second players
        If global is true, the message is also written to the global log
        If inverted is true, the message will be written to all logs except the given player group's log
        */
        Q_SCRIPTABLE void writeToGameLog(const QString& msg, QList<int> playerList, bool global, bool inverted, bool no_update = false);
        /*!
        Returns true if card placement is enabled
        */
        Q_SCRIPTABLE bool isCardPlacementEnabled();
        /*!
        Returns true if the *player* placed a card on the board in the current turn
        */
        Q_SCRIPTABLE bool didPlayerPlaceCardOnBoardThisTurn();
        /*!
        Enables/disables card placement
        */
        Q_SCRIPTABLE void setCardPlacementEnabled(bool enabled);
        /*!
        Sets the value of a permanent variable (overwrites existing value)
        */
        Q_SCRIPTABLE void setVariable(const QString& name, const QString& value);
        /*!
        Returns the value of a permanent variable (or an empty string if the variable hasn't been set yet)
        */
        Q_SCRIPTABLE QString getVariable(const QString& name) const;
        /*!
        Ends the turn
        */
        void endTurnButtonClicked(bool forceSwitch = false, bool aiPlayer = false);
        /*!
        Returns an array of length 4 with the current scores of the 4 players (score is 0 for unused players if there are less than 4 players in the current game mode).
        */
        Q_SCRIPTABLE QList<int> getScore() const;
        /*!
        Returns a list which contains the turn order of the players
        */
        Q_SCRIPTABLE QList<int> getPlayerTurnOrder() const;
        /*!
        Returns the game mode
        0 means 1 vs. 1 (opposite players)
        1 means 2 vs. 2
        2 means 3 player FFA
        3 means 4 player FFA
        4 means 1 vs. 1 (adjacent players)
        */
        Q_SCRIPTABLE int getGameMode() const;
        /*!
        Returns the value of the given configuration variable
        */
        Q_SCRIPTABLE QString getConfigVariable(const QString& key) const;
        /*!
        Creates a new QPoint with the given coordinates
        */
        Q_SCRIPTABLE QPoint newPoint(int x, int y) const;
        /*!
        Returns an array of player nicknames (in order from p1 to p4)
        */
        Q_SCRIPTABLE QStringList getPlayerNickNames() const;
        /*!
        Disables silver actions for the current turn
        */
        Q_SCRIPTABLE void setSilverActionsDisabledForTurn(bool disabled);
        /*!
        Returns true if silver actions are disabled for the current turn
        */
        Q_SCRIPTABLE bool areSilverActionsDisabledForTurn() const;
        /*!
        Displays an error message
        */
        Q_SCRIPTABLE void reportError(const QString& error) const;
        /*!
        Returns true if the Huckster is enabled, otherwise returns false
        */
        Q_SCRIPTABLE bool isHucksterEnabled() const;
        /*!
        Returns true if the "Open hands" gameplay option was enabled, otherwise returns false
        */
        Q_SCRIPTABLE bool isOpenHandsEnabled() const;
        void checkVictory();
        /*!
        Returns a list which contains the turn order of the players
        */
        Q_SCRIPTABLE QList<int> getPlayerTurnOrder();
        /*!
        Sets whether the built-in victory condition check is enabled
        */
        Q_SCRIPTABLE void setCheckVictory(bool enabled);
        /*!
        Enable/disable card drawing for the current turn.
        This applies to both the player and all scripts.
        */
        Q_SCRIPTABLE void setCardDrawDisabledForTurn(bool d);
        /*!
        True if the player has already drawn a card in this turn.
        When drawCard is called from a script with it second parameter set to false it does not count
        as card drawing for this function.
        */
        Q_SCRIPTABLE bool hasCurrentPlayerDrawnCard() const;
        /*!
        True if card drawing was disabled by calling setCardDrawDisabledForTurn(false)
        */
        Q_SCRIPTABLE bool isCardDrawDisabledForTurn() const;
        /*!
        Returns the card object belonging to the given UID or 0 if there is no such card.
        */
        Q_SCRIPTABLE card* cardForUID(int uid) const;
        /*!
        Returns the next player.
        */
        Q_SCRIPTABLE int nextPlayer() const;
        /*!
        Overrides the next player, so instead of the normal turn order, the player set with this function will have the next turn.
        Call with -1 to disable the override.
        */
        Q_SCRIPTABLE void overrideNextPlayer(int playerNum);
        /*!
        Ends the game and returns to the main menu.
        Use ONLY at the end of a puzzle (in an event handler).
         */
        Q_SCRIPTABLE void endGame();
        /*!
        Reloads the current puzzle.
        Use ONLY at the end of a puzzle (in an event handler).
         */
        Q_SCRIPTABLE void retryCurrentPuzzle();
        /*!
        Ends the game and the returns to the puzzle selection screen.
        Use ONLY at the end of a puzzle (in an event handler).
         */
        Q_SCRIPTABLE void returnToPuzzleScreen();
        /*!
        Returns true if it's currently the AI's turn.
         */
        Q_SCRIPTABLE bool isAITurn() const;
        /*!
        Returns a list of cards (IDs) which currently have bronze abilities being executed.
         */
        Q_SCRIPTABLE QList<int> getCardsWithActiveBronzeAbility() const;
        void aiTurn();
        void sendGameEvent(const QStringList& ev, bool force = false);
        void updateUIForTurnSwitch();
        bool isLocalUser(int playerNum);

    private:
        void processAIFinished();
        void init(mainWindow* mainW, gameState* state);
        void doThingsThatNeedToBeDoneAtTurnStart();
        bool isObserver();
        QMap<QString, QString> scriptVarMap;
        QList<int> cardsWithActiveBronzeAbility;
        bool victoryCheckEnabled = true;
        bool currentPlayerDrawnCard;
        bool cardDrawDisabledForTurn = false;
        bool alreadyOver = false;
        int currentPlayer;
        int nextPlayerOverride = -1;
        bool cardPlacementEnabled;
        bool addBkgScript(int player, QString fileName, QString fnameShort);
        static QList<std::pair<std::pair<int, QString>, QString>> bkgScripts;
        QList<int> teamList(int player);
};

#endif // GAMESTATE_H
