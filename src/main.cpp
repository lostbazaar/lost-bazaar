/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <QObject>
#include <QApplication>
#include <QFile>
#include <QTextStream>
#include <QTranslator>
#include <QFontDatabase>
#include <QDir>
#include <QLibraryInfo>
#include <cstdlib>
#include <ctime>
#include "mainwindow.h"
#include "globals.h"
#include "utils.h"
#ifdef Q_OS_ANDROID
#include <QtAndroid>
#endif

void customMsgHandler(QtMsgType type, const QMessageLogContext& context, const QString& msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    const char* file = context.file ? context.file : "";
    const char* function = context.function ? context.function : "";
    switch(type)
    {
        case QtDebugMsg:
            fprintf(stderr, "Debug: %s (%s:%u, %s)\n", localMsg.constData(), file, context.line, function);
            break;
        case QtInfoMsg:
            fprintf(stderr, "Info: %s (%s:%u, %s)\n", localMsg.constData(), file, context.line, function);
            break;
        case QtWarningMsg:
            fprintf(stderr, "Warning: %s (%s:%u, %s)\n", localMsg.constData(), file, context.line, function);
            break;
        case QtCriticalMsg:
            fprintf(stderr, "Critical: %s (%s:%u, %s)\n", localMsg.constData(), file, context.line, function);
            break;
        case QtFatalMsg:
            fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), file, context.line, function);
            break;
    }
}

int main(int argc, char* argv[])
{
    (void)QT_TRANSLATE_NOOP("QPlatformTheme", "&Yes");
    (void)QT_TRANSLATE_NOOP("QPlatformTheme", "&No");

#ifdef Q_OS_WIN
    QApplication::setAttribute(Qt::AA_UseOpenGLES);
#endif

    QApplication a(argc, argv);

    //qInstallMessageHandler(&customMsgHandler);

    a.installEventFilter(new endGameEventFilter {});

    if (a.arguments().contains("--unix-endl"))
    {
        lineEnd = "\n";
    }

    srand(time(nullptr));
    QCoreApplication::setApplicationName("Lost Bazaar");
    QCoreApplication::setOrganizationName("Lost Bazaar");
    QCoreApplication::setOrganizationDomain("lost-bazaar.org");
    QApplication::setQuitOnLastWindowClosed(true);
    QSettings::setDefaultFormat(QSettings::IniFormat);
    QSettings* settings = new QSettings;

#ifdef Q_OS_HAIKU
    QDir::setCurrent(QStringLiteral("/boot/system/data/lost-bazaar"));
#endif

#ifdef Q_OS_ANDROID
    QtAndroid::requestPermissions({"android.permission.INTERNET"}, [](const QtAndroid::PermissionResultMap&){});
#endif

    if (a.arguments().contains("--linux-multiarch"))
    {
        QDir::setCurrent(QDir::currentPath() + "/../");
    }

    auto autocfg_idx = a.arguments().indexOf("--autostart-config");
    if (autocfg_idx > -1)
    {
        mainWindow::autoCfg = cfgFile(a.arguments().at(autocfg_idx + 1));
    }

    QFontDatabase::addApplicationFont(getPrimaryPath("fonts/530/LinLibertine_DR.otf"));
    QFontDatabase::addApplicationFont(getPrimaryPath("fonts/513/LinLibertine_aS.otf"));

    //QIcon::setThemeSearchPaths(QStringList() << "invalid____");
    //QIcon::setThemeName("invalid__________");

    //QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));

    QTranslator appTranslator;
    if (settings->value("language").toString() == "")
    {
        if(!appTranslator.load(getPrimaryPath("translations/lostbazaar_" + QLocale::system().name().split("_")[0] + ".qm")))
        {
            qDebug() << "Failed to load translation";
        }
    }
    else
    {
        if (settings->value("language").toString() == "system")
        {
            if(!appTranslator.load(getPrimaryPath("translations/lostbazaar_" + QLocale::system().name().split("_")[0] + ".qm")))
            {
                qDebug() << "Failed to load translation";
            }
        }
        else
        {
            if(!appTranslator.load(getPrimaryPath("translations/lostbazaar_" + settings->value("language").toString().split("_")[0] + ".qm")))
            {
                qDebug() << "Failed to load translation";
            }
        }
    }
    a.installTranslator(&appTranslator);

    mainWindow w(nullptr, settings);
    w.setWindowIcon(QIcon(getPrimaryPath("img/icon.png")));

    auto styleSheet = mainWindow::readStyleSheet();
    if (!styleSheet.isEmpty())
    {
        w.setStyleSheet(styleSheet);
    }
    else
    {
        mainWindow::reportError(qApp->translate("global", "Error: cannot open stylesheet file (%1)!").arg(getPrimaryPath("theme.css")));
        return EXIT_FAILURE;
    }

    if(settings->value("disableFontHinting").toInt())
    {
        QFont appFont = QApplication::font();
        appFont.setHintingPreference(QFont::PreferNoHinting);
        QApplication::setFont(appFont);
    }

    w.show();
    w.applyStylesheetBugWorkAround();
    w.startTitleSequence();
    if (a.arguments().contains("--render-cards"))
    {
        w.renderCards();
    }

    a.exec();

    return EXIT_SUCCESS;
}
