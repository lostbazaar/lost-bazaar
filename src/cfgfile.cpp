/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <QFile>
#include <QMap>
#include <QTextStream>
#include "cfgfile.h"
#include "mainwindow.h"
#include "globals.h"

cfgFile::cfgFile(const QString& fileName)
{
    this->fName = fileName;
    QFile f(fileName);
    if (f.open(QIODevice::ReadOnly))
    {
        QTextStream in(&f);
        //in.setCodec("UTF-8");
        while (!in.atEnd())
        {
            QString line = in.readLine();
            if (line.contains("="))
            {
                this->data[line.left(line.indexOf("="))] = line.mid(line.indexOf("=") + 1).split(',', Qt::SkipEmptyParts);
            }
        }
        f.close();
    }
    else
    {
        mainWindow::reportError(QString("Error: cannot open %1").arg(f.fileName()));
    }
}

QStringList cfgFile::readValues(const QString& key) const
{
    return this->data.value(key);
}

QStringList cfgFile::readLocalizedValues(const QString& key, const QString& loc) const
{
    if (this->data.contains(key + "." + loc))
    {
        return this->data[key + "." + loc];
    }
    else
    {
        return this->data[key + "." DEFAULT_LANGUAGE];
    }
}

QString cfgFile::readValue(const QString& key) const
{
    return this->readValues(key).join(",");
}

QString cfgFile::readLocalizedValue(const QString& key, const QString& loc) const
{
    return this->readLocalizedValues(key, loc).join(",");
}

QString cfgFile::fileName() const
{
    auto n = QFileInfo(this->fName).fileName();
    if (n.endsWith(".cfg")) n.resize(n.size() - 4);
    return n;
}

bool cfgFile::hasKey(const QString& key) const
{
    return this->data.count(key);
}

bool cfgFile::hasLocalizedKey(const QString& key, const QString& loc) const
{
    return this->data.count(key + "." + loc) || this->data.count(key + "." DEFAULT_LANGUAGE);
}

void cfgFile::writeValues(const QString& key, const QString& value)
{
    this->writeValues(key, QStringList() << value);
}

void cfgFile::writeValues(const QString& key, const QStringList& value)
{
    this->data[key] = value;
    this->save();
}

void cfgFile::save()
{
    QFile f(this->fName);
    if (f.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        QTextStream out(&f);
        //out.setCodec("UTF-8");
        QMapIterator<QString, QStringList> it(this->data);
        while (it.hasNext())
        {
            it.next();
            out << it.key() << "=" << it.value().join(QChar(',')) << Qt::endl;
        }
        f.close();
    }
    else
    {
        mainWindow::reportError(QString("Error: cannot open %1").arg(f.fileName()));
    }
}
