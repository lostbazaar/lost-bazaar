/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GRAPHICSVIEWMENU_H
#define GRAPHICSVIEWMENU_H

#include <QObject>
#include <QGraphicsView>
#include <QPushButton>
#include <QLabel>
#include <QGraphicsOpacityEffect>
#include <QPropertyAnimation>

class mainWindow;
class gameState;
class board;

class graphicsViewMenu : public QObject
{
        Q_OBJECT

    public:
        graphicsViewMenu(board* view);
        /*!
        Prompts the player for a choice between the given values.
        Returns the index of the selected value
        */
        Q_SCRIPTABLE int getChoice(const QStringList& values, const QStringList& enableList = QStringList(), const QStringList& toolTips = QStringList(), bool overrideAI = false);
        /*!
        Prompts the player for a choice between the players with the given numbers.
        Returns the number of the selected player
        Returns -1 if the choice was cancelled
        */
        Q_SCRIPTABLE int getPlayer(const QList<int>& values, bool cancellable);
        /*!
        Shows an information message with the given text for msecs milliseconds (or indefinitely if -1)
        The default value is 2500 msecs.
        */
        Q_SCRIPTABLE void showInformation(const QString& info, int msecs = 2500, bool clearPrevious = false, bool overrideAI = false);
        /*!
        Shows an information message centered on the screen with an OK button.
        */
        Q_SCRIPTABLE void showLongInformation(const QString& info, const QString& okButtonText);
        void reset();
        /*!
        Declares winner players. If the return value is true, the user chose to continue the game otherwise the user chose to end the game.
        Call with empty list to declare draw.
         */
        Q_SCRIPTABLE bool declareWinnerPlayers(const QList<int>& winners, const QString &backToText = tr("Back to main menu"));
        /*!
        Declares a winner team. If the return value is true, the user chose to continue the game otherwise the user chose to end the game.
        Call with -1 to declare draw.
         */
        Q_SCRIPTABLE bool declareWinnerTeam(int team, const QString& backToText = tr("Back to main menu"));
        void setGameState(gameState* gS);
        ~graphicsViewMenu();

    public slots:
        /*!
        Hides the information message
        */
        Q_SCRIPTABLE void hideInformation();

    private slots:
        void buttonClicked();
        void okButtonClicked();

    private:
        void addInfoLabel(const QString& info, int msecs, bool clearPrevious);
        QList<QPushButton*> buttons;
        QStringList currentVals;
        QList<QLabel*> infoLabels;
        QLabel* longInfoLabel = nullptr;
        QPushButton* longInfoOKButton = nullptr;
        bool blocked = false;
        board* v;
        int res;
        gameState* gS = nullptr;
};

#endif // GRAPHICSVIEWMENU_H
