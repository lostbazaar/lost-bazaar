/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "random.h"
#include "gamestate.h"
#include "mainwindow.h"

unsigned long rng::mersenneTwisterInt32::state[n] = {0x0UL};
int rng::mersenneTwisterInt32::p = 0;
unsigned long rng::mersenneTwisterInt32::saved_state[n] = {0x0UL};
int rng::mersenneTwisterInt32::saved_p = 0;
bool rng::mersenneTwisterInt32::init = false;

rng::mersenneTwisterInt32 mtInt32;
rng::mersenneTwister53 mt53;
rng::mersenneTwisterOpen mtOpen;
rng::mersenneTwisterClosed mtClosed;
rng::mersenneTwister mtDefault;

void rng::mersenneTwisterInt32::gen_state()
{
    //Generate new state vector
    for (int i = 0; i < (n - m); ++i) state[i] = state[i + m] ^ twiddle(state[i], state[i + 1]);
    for (int i = n - m; i < (n - 1); ++i) state[i] = state[i + m - n] ^ twiddle(state[i], state[i + 1]);
    state[n - 1] = state[m - 1] ^ twiddle(state[n - 1], state[0]);
    p = 0; //Reset position
}

void rng::mersenneTwisterInt32::seed(unsigned long s)
{
    //Init by 32 bit seed
    state[0] = s & 0xFFFFFFFFUL; //For > 32 bit machines
    for (int i = 1; i < n; ++i)
    {
        state[i] = 1812433253UL * (state[i - 1] ^ (state[i - 1] >> 30)) + i;
        //See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier
        state[i] &= 0xFFFFFFFFUL; //For > 32 bit machines
    }
    p = n; //Force gen_state() to be called for next random number
}

void rng::mersenneTwisterInt32::seed(const unsigned long* array, int size)
{
    //Init by array
    seed(19650218UL);
    int i = 1, j = 0;
    for (int k = ((n > size) ? n : size); k; --k)
    {
        state[i] = (state[i] ^ ((state[i - 1] ^ (state[i - 1] >> 30)) * 1664525UL)) + array[j] + j; //Non linear
        state[i] &= 0xFFFFFFFFUL; //For > 32 bit machines
        ++j;
        j %= size;
        if ((++i) == n)
        {
            state[0] = state[n - 1];
            i = 1;
        }
    }
    for (int k = n - 1; k; --k)
    {
        state[i] = (state[i] ^ ((state[i - 1] ^ (state[i - 1] >> 30)) * 1566083941UL)) - i;
        state[i] &= 0xFFFFFFFFUL; //For > 32 bit machines
        if ((++i) == n)
        {
            state[0] = state[n - 1];
            i = 1;
        }
    }
    state[0] = 0x80000000UL; //MSB is 1; assuring non-zero initial array
    p = n; //Force gen_state() to be called for next random number
}

unsigned long rng::randomBetween(unsigned long a, unsigned long b)
{
    if (!(a < b))
    {
        mainWindow::reportError(tr("Script error: randomBetween was called with an invalid range (%2, %3), this is probably a bug in the following script:\n%1").arg(gameState::scriptExecInfo.join("\n"), QString::number(a), QString::number(b)));
    }
    return a + mtInt32() % (b - a);
}

bool rng::randomBoolean()
{
    return mtInt32() % 2 == 0;
}

void rng::seedRNGs(unsigned long seed)
{
    mtInt32.seed(seed);
    mt53.seed(seed);
    mtOpen.seed(seed);
    mtClosed.seed(seed);
    mtDefault.seed(seed);
}

unsigned long rng::randomBetweenZeroAndN(unsigned long n)
{
    return mtInt32() % (n + 1);
}

void rng::saveState()
{
    mtInt32.saveState();
}

void rng::restoreSavedState()
{
    mtInt32.restoreSavedState();
}

QByteArray rng::getState() const
{
    return mtInt32.getState();
}
