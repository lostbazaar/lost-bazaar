/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <QGraphicsScene>
#include <QFont>
#include <QPainter>
#include <QApplication>
#include <QFontMetrics>
#include <QGraphicsItemAnimation>
#include <QFile>
#include <QTextStream>
#include <QGraphicsPixmapItem>
#include <QPropertyAnimation>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsBlurEffect>
#include <QTimer>
#include <QTimeLine>
#include <cmath>
#include "card.h"
#include "cfgfile.h"
#include "globals.h"
#include "mainwindow.h"
#include "cardinventory.h"
#include "board.h"

#define CARD_BLUR 3
#define CARD_ROTATION_ZVALUE 1500

int card::globalUID = 0;

const char* ALL_EVENTS[] = {
    "cardPlacedOnBoard",
    "cardSilverActionActivated",
    "cardDrawn",
    "endOfTurn"
    "turnStarted"
    "rotationFinished"
};

QImage blurred(const QImage& image, const QRect& rect, int radius, bool alphaOnly = false)
{
    int tab[] = { 14, 10, 8, 6, 5, 5, 4, 3, 3, 3, 3, 2, 2, 2, 2, 2, 2 };
    int alpha = (radius < 1)  ? 16 : (radius > 17) ? 1 : tab[radius - 1];

    QImage result = image.convertToFormat(QImage::Format_ARGB32_Premultiplied);
    int r1 = rect.top();
    int r2 = rect.bottom();
    int c1 = rect.left();
    int c2 = rect.right();

    int bpl = result.bytesPerLine();
    int rgba[4];
    unsigned char* p;

    int i1 = 0;
    int i2 = 3;

    if (alphaOnly)
        i1 = i2 = (QSysInfo::ByteOrder == QSysInfo::BigEndian ? 0 : 3);

    for (int col = c1; col <= c2; col++)
    {
        p = result.scanLine(r1) + col * 4;
        for (int i = i1; i <= i2; i++)
            rgba[i] = p[i] << 4;

        p += bpl;
        for (int j = r1; j < r2; j++, p += bpl)
            for (int i = i1; i <= i2; i++)
                p[i] = (rgba[i] += ((p[i] << 4) - rgba[i]) * alpha / 16) >> 4;
    }

    for (int row = r1; row <= r2; row++)
    {
        p = result.scanLine(row) + c1 * 4;
        for (int i = i1; i <= i2; i++)
            rgba[i] = p[i] << 4;

        p += 4;
        for (int j = c1; j < c2; j++, p += 4)
            for (int i = i1; i <= i2; i++)
                p[i] = (rgba[i] += ((p[i] << 4) - rgba[i]) * alpha / 16) >> 4;
    }

    for (int col = c1; col <= c2; col++)
    {
        p = result.scanLine(r2) + col * 4;
        for (int i = i1; i <= i2; i++)
            rgba[i] = p[i] << 4;

        p -= bpl;
        for (int j = r1; j < r2; j++, p -= bpl)
            for (int i = i1; i <= i2; i++)
                p[i] = (rgba[i] += ((p[i] << 4) - rgba[i]) * alpha / 16) >> 4;
    }

    for (int row = r1; row <= r2; row++)
    {
        p = result.scanLine(row) + c2 * 4;
        for (int i = i1; i <= i2; i++)
            rgba[i] = p[i] << 4;

        p -= 4;
        for (int j = c1; j < c2; j++, p -= 4)
            for (int i = i1; i <= i2; i++)
                p[i] = (rgba[i] += ((p[i] << 4) - rgba[i]) * alpha / 16) >> 4;
    }

    return result;
}

card::card(const QString& _id, int player, gameState* gS)
{
    this->init(_id, player, gS);
}

card::card(const QString& _id, int player, gameState* gS, int globalUID)
{
    this->init(_id, player, gS, globalUID);
}

card::card(fileFormat::saveFileItem it, gameState* gS)
{
    this->init(it.getAttributeAsString("id"), it.getAttributeAsString("playerNum").toInt(), gS, it.getAttributeAsString("uid").toInt());
    this->counter = it.getAttributeAsString("counter").toInt();
    this->markedForTrash = it.getAttributeAsString("markedForTrash").toInt();
    this->markedForHuckster = it.getAttributeAsString("markedForHuckster").toInt();
    this->setGrayScale(it.getAttributeAsString("grayScale").toInt(), true);
    this->setShowFullSize(it.getAttributeAsString("showFullSize").toInt(), true);
    this->uid = it.getAttributeAsString("uid").toInt();
    this->overlay = static_cast<CardInventoryOverlay>(it.getAttributeAsString("overlay").toInt());
    this->pos = QPoint(it.getAttributeAsString("posX").toInt(), it.getAttributeAsString("posY").toInt());
}

void card::draw(QGraphicsScene* scene, DrawingType t, QPointF pos_, bool do_not_save_details, QString cardPictureSet, QString namePictureSet, QString forceLang, bool inGameDnD)
{
    if (this->isBeingAnimated())
    {
        this->scheduleRedraw(scene, t, pos_, do_not_save_details, cardPictureSet, namePictureSet);
        return;
    }
    this->scheduledDrawScene = nullptr;
    if (!gS->mW) return;
    if (cardPictureSet.isEmpty())
    {
        cardPictureSet = mainWindow::cardPictureSet;
    }
    if (namePictureSet.isEmpty())
    {
        namePictureSet = mainWindow::cardNamePictureSet;
    }
    QString lang;
    if (!forceLang.isEmpty())
    {
        lang = forceLang;
    }
    else
    {
        lang = mainWindow::getLanguageCode();
    }
    if (!do_not_save_details)
    {
        this->prevDrawn = true;
        this->prevScene = scene;
        this->prevDrawingType = t;
        this->prevPos = pos_;
    }
    auto createGraphicsItem = [](const QString & name)
    {
        QGraphicsPixmapItem* it = new QGraphicsPixmapItem;
        it->setPixmap(QPixmap(name));
        return it;
    };
    auto createGraphicsItemPx = [](const QPixmap & px)
    {
        QGraphicsPixmapItem* it = new QGraphicsPixmapItem;
        it->setPixmap(px);
        return it;
    };
    this->removeFromScene(scene);
    this->cardImgOnBoard = nullptr;
    if (t == DrawingType::Full)
    {
        QString prerenderedPath;
        if (mainWindow::getConfigVariable("hiresMode").toInt())
        {
            if (!mainWindow::getConfigVariable("noPrerenderedCards").toInt())
            {
                prerenderedPath = getPrimaryPath(QString("cards/prerendered_hires/") + lang + "_" + this->getID() + ".png", true);
            }
        }
        else
        {
            if (!mainWindow::getConfigVariable("noPrerenderedCards").toInt())
            {
                prerenderedPath = getPrimaryPath(QString("cards/prerendered/") + lang + "_" + this->getID() + ".png", true);
            }
        }
        if (!prerenderedPath.isEmpty())
        {
            QPixmap* px = new QPixmap(prerenderedPath);
            this->pixmapItems.append(createGraphicsItemPx(*px));
            scene->addItem(this->pixmapItems.last());
            this->pixmapItems.last()->setPos(pos_);
            this->pixmapItems.last()->setVisible(true);
        }
        else
        {
            graphicsPixmapItem* px = new graphicsPixmapItem;
            px->setPixmap(getPrimaryPathScaled(QString("cards/backgrounds/%1.png").arg(cardData[id].readValues("background").first()), 312));
            this->pixmapItems.append(px);
            scene->addItem(px);
            this->pixmapItems.last()->setVisible(true);
            this->pixmapItems.last()->setPos(pos_);
            this->pixmapItems.append(createGraphicsItem(getPrimaryPath("cards/pictureSets/" + cardPictureSet + "/" + cardData[id].readValues("picture").first() + ".png")));
            scene->addItem(this->pixmapItems.last());
            this->pixmapItems.last()->setVisible(true);
            this->pixmapItems.last()->setPos(41 + pos_.x(), 17 + pos_.y());
            this->pixmapItems.append(createGraphicsItemPx(getPrimaryPathScaled("cards/" + namePictureSet + "/" + cardData[id].readLocalizedValues("namePicture", lang).first() + ".png", -2)));
            scene->addItem(this->pixmapItems.last());
            this->pixmapItems.last()->setVisible(true);
            this->pixmapItems.last()->setPos(px->pixmap().height() - this->pixmapItems.last()->pixmap().width() + pos_.x(), pos_.y());
            this->pixmapItems.append(createGraphicsItemPx(getPrimaryPathScaled("img/" + QString::number(this->val) + ".png", -2)));
            scene->addItem(this->pixmapItems.last());
            this->pixmapItems.last()->setVisible(true);
            this->pixmapItems.last()->setPos(pos_.x(), pos_.y());
            for (int i = 0; i < cardData[id].readValues("icons").size(); i++)
            {
                this->pixmapItems.append(createGraphicsItem(getPrimaryPath(QString("img/icon_") + cardData[id].readValues("icons")[i] + ".png")));
                scene->addItem(this->pixmapItems.last());
                this->pixmapItems.last()->setVisible(true);
                if (i == 0)
                {
                    this->pixmapItems.last()->setPos(pos_.x() + 202, pos_.y() + 86);
                }
                if (i == 1)
                {
                    this->pixmapItems.last()->setPos(pos_.x() + 202, pos_.y() + 48);
                }
                if (i == 2)
                {
                    this->pixmapItems.last()->setPos(pos_.x() + 1, pos_.y() + 86);
                }
                if (i == 3)
                {
                    this->pixmapItems.last()->setPos(pos_.x() + 1, pos_.y() + 48);
                }
            }
            int textbox_height = 98;
            int textbox_width = 217;
            int textbox_y = 127;
            if (mainWindow::getConfigVariable("hiresMode").toInt())
            {
                textbox_height = 167;
                textbox_width = 289;
                textbox_y = 131;
            }

            int i_bronze = !cardData[id].readLocalizedValues("bronzeText", lang).join(',').isEmpty();
            int i_gold = !cardData[id].readLocalizedValues("goldText", lang).join(',').isEmpty();
            int i_silver = !cardData[id].readLocalizedValues("silverText", lang).join(',').isEmpty();
            int sepHeight = (i_bronze + i_gold + i_silver - 1) * 2;
            int availableHeight = -sepHeight + textbox_height;
            QRect bronzeRect(0, 0, 0, 0);
            QRect silverRect(0, 0, 0, 0);
            QRect goldRect(0, 0, 0, 0);
            int s = 42;
            for (; s > 0; --s)
            {
                bronzeRect = QRect(0, 0, 0, 0);
                silverRect = QRect(0, 0, 0, 0);
                goldRect = QRect(0, 0, 0, 0);
                QFont font("Linux Libertine");
                font.setPixelSize(s);
                QFontMetrics metrics(font);
                QRect r{0,0,textbox_width,availableHeight};
                if (i_bronze) bronzeRect = metrics.boundingRect(r, Qt::TextWordWrap, cardData[id].readLocalizedValues("bronzeText", lang).join(','));
                if (i_silver) silverRect = metrics.boundingRect(r, Qt::TextWordWrap, cardData[id].readLocalizedValues("silverText", lang).join(','));
                if (i_gold) goldRect = metrics.boundingRect(r, Qt::TextWordWrap, cardData[id].readLocalizedValues("goldText", lang).join(','));
                if (goldRect.height() + silverRect.height() + bronzeRect.height() <= availableHeight) break;
            }
            int c = 0;
            while (goldRect.height() + silverRect.height() + bronzeRect.height() != availableHeight)
            {
                if (c % 3 == 0 && i_bronze) bronzeRect.setHeight(bronzeRect.height() + 1);
                if (c % 3 == 1 && i_silver) silverRect.setHeight(silverRect.height() + 1);
                if (c % 3 == 2 && i_gold) goldRect.setHeight(goldRect.height() + 1);
                c++;
            }
            if (i_bronze)
            {
                QPixmap* px = new QPixmap(QSize(textbox_width, bronzeRect.height()));
                QPainter p(px);
                QFont font("Linux Libertine");
                font.setPixelSize(s);
                p.setFont(font);
                //p.fillRect(px->rect(), QColor("#ECC48C"));
                p.fillRect(px->rect(), QColor("#eaba82"));
                p.drawText(0, 0, px->width(), px->height(), Qt::TextWordWrap | Qt::AlignHCenter | Qt::AlignVCenter, cardData[id].readLocalizedValues("bronzeText", lang).join(','));
                this->pixmapItems.append(createGraphicsItemPx(*px));
                scene->addItem(this->pixmapItems.last());
                this->pixmapItems.last()->setPos(11 + pos_.x(), textbox_y + pos_.y());
                this->pixmapItems.last()->setVisible(true);
            }
            if (i_silver)
            {
                QPixmap* px = new QPixmap(QSize(textbox_width, silverRect.height()));
                QPainter p(px);
                QFont font("Linux Libertine");
                font.setPixelSize(s);
                p.setFont(font);
                //p.fillRect(px->rect(), QColor("#DCF1FC"));
                p.fillRect(px->rect(), QColor("#e6f0fe"));
                p.drawText(0, 0, px->width(), px->height(), Qt::TextWordWrap | Qt::AlignHCenter | Qt::AlignVCenter, cardData[id].readLocalizedValues("silverText", lang).join(','));
                if (i_bronze)
                {
                    if (!mainWindow::getConfigVariable("hiresMode").toInt())
                    {
                        this->pixmapItems.append(createGraphicsItem(getPrimaryPath("img/separator.png")));
                    }
                    else
                    {
                        this->pixmapItems.append(createGraphicsItem(getPrimaryPath("img/separator_hires.png")));
                    }
                    scene->addItem(this->pixmapItems.last());
                    this->pixmapItems.last()->setVisible(true);
                    this->pixmapItems.last()->setPos(11 + pos_.x(), textbox_y + bronzeRect.height() + pos_.y());
                }
                this->pixmapItems.append(createGraphicsItemPx(*px));
                scene->addItem(this->pixmapItems.last());
                this->pixmapItems.last()->setPos(11 + pos_.x(), textbox_y + bronzeRect.height() + (i_bronze ? 2 : 0) + pos_.y());
                this->pixmapItems.last()->setVisible(true);
            }
            if (i_gold)
            {
                QPixmap* px = new QPixmap(QSize(textbox_width, goldRect.height()));
                QPainter p(px);
                QFont font("Linux Libertine");
                font.setPixelSize(s);
                p.setFont(font);
                //p.fillRect(px->rect(), QColor("#F9F993"));
                p.fillRect(px->rect(), QColor("#ffea61"));
                p.drawText(0, 0, px->width(), px->height(), Qt::TextWordWrap | Qt::AlignHCenter | Qt::AlignVCenter, cardData[id].readLocalizedValues("goldText", lang).join(','));
                if (i_bronze || i_silver)
                {
                    if (!mainWindow::getConfigVariable("hiresMode").toInt())
                    {
                        this->pixmapItems.append(createGraphicsItem(getPrimaryPath("img/separator.png")));
                    }
                    else
                    {
                        this->pixmapItems.append(createGraphicsItem(getPrimaryPath("img/separator_hires.png")));
                    }
                    scene->addItem(this->pixmapItems.last());
                    this->pixmapItems.last()->setVisible(true);
                    this->pixmapItems.last()->setPos(11 + pos_.x(), textbox_y + bronzeRect.height() + silverRect.height() + pos_.y() + ((i_bronze && i_silver) ? 2 : 0));
                }
                this->pixmapItems.append(createGraphicsItemPx(*px));
                scene->addItem(this->pixmapItems.last());
                this->pixmapItems.last()->setPos(11 + pos_.x(), textbox_y + bronzeRect.height() + silverRect.height() + (i_silver ? (i_bronze ? 4 : 2) : (i_bronze ? 2 : 0)) + pos_.y());
                this->pixmapItems.last()->setVisible(true);
            }
        }
    }
    else if (t == DrawingType::Inventory)
    {
        QPixmap px = QPixmap(QString(getPrimaryPath("cards/pictureSets/" + cardPictureSet + "/" + cardData[id].readValues("picture").first() + ".png")));
        if (!mainWindow::getConfigVariable("hiresMode").toInt()) px = px.scaled(QSize(120, 78), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
        QPixmap frame;
        if (this->selectedForPlacement)
        {
            frame = getPrimaryPathScaled("img/placement_inventory_frame.png", 161);
        }
        else if (this->markedForTrash)
        {
            frame = getPrimaryPathScaled("img/trash_inventory_frame.png", 161);
        }
        else if (this->markedForHuckster)
        {
            frame = getPrimaryPathScaled("img/huckster_inventory_frame.png", 161);
        }
        else
        {
            frame = getPrimaryPathScaled("img/default_inventory_frame.png", 161);
        }
        this->pixmapItems.append(createGraphicsItemPx(frame));
        scene->addItem(this->pixmapItems.last());
        this->pixmapItems.last()->setZValue(100);
        this->pixmapItems.last()->setPos(pos_.x(), pos_.y());
        this->pixmapItems.last()->setVisible(true);
        if (this->overlay != CardInventoryOverlay::NoOverlay)
        {
            QPixmap overlay_;
            if (this->overlay == CardInventoryOverlay::TransparentOverlay)
            {
                overlay_ = getPrimaryPathScaled("img/cardoverlay_transparent.png", 161);
            }
            else if (this->overlay == CardInventoryOverlay::PlayerChoiceOverlay)
            {
                overlay_ = getPrimaryPathScaled("img/cardoverlay_playerchoice.png", 161);
            }
            else if (this->overlay == CardInventoryOverlay::DisabledOverlay)
            {
                overlay_ = getPrimaryPathScaled("img/cardoverlay_disabled.png", 161);
            }
            this->pixmapItems.append(createGraphicsItemPx(overlay_));
            scene->addItem(this->pixmapItems.last());
            //this->pixmapItems.last()->setZValue(200);
            this->pixmapItems.last()->setPos(pos_.x(), pos_.y());
            this->pixmapItems.last()->setVisible(true);
            this->pixmapItems.last()->setZValue(100);
            if (this->overlay == CardInventoryOverlay::DisabledOverlay) //The disabled overlay should appear below any frames (e.g. marked for trash frame)
            {
                this->pixmapItems.last()->setZValue(99);
            }
        }
        if (this->gS && this->gS->huckster)
        {
            auto idx = this->gS->huckster->indexOf(this);
            if (idx != -1)
            {
                QPixmap h_overlay;
                h_overlay = QPixmap(getPrimaryPath(QString("img/huckster_%1.png").arg(QString::number(idx + 1))));
                this->pixmapItems.append(createGraphicsItemPx(h_overlay));
                scene->addItem(this->pixmapItems.last());
                this->pixmapItems.last()->setPos(pos_.x(), pos_.y());
                this->pixmapItems.last()->setVisible(true);
                this->pixmapItems.last()->setZValue(98);
            }
        }
        this->pixmapItems.append(createGraphicsItemPx(px));
        scene->addItem(this->pixmapItems.last());
        this->pixmapItems.last()->setPos(pos_.x() + 2, pos_.y() + 2);
        this->pixmapItems.last()->setVisible(true);
    }
    else if (t == DrawingType::InGame)
    {
        QString tileStr = "tile";
        if (this->gS->gameBoard->isSafeTile(this->getPosition()))
        {
            tileStr = "blue";
        }
        else if (this->gS->gameBoard->isDoubleTile(this->getPosition()))
        {
            tileStr = "yellow";
        }
        else if (this->gS->gameBoard->isInnovativeTile(this->getPosition()))
        {
            tileStr = "green";
        }
        else if (this->gS->gameBoard->isFashionTile(this->getPosition()))
        {
            tileStr = "magenta";
        }
        else if (this->gS->gameBoard->isPowerTile(this->getPosition()))
        {
            tileStr = "orange";
        }
        QPixmap bkg;
        if (this->playerNum != -1)
        {
            bkg = getPrimaryPathScaled(QString("boards/%1p%2_%3.png").arg(board::getPlayerTileFilenamePrefix(), QString::number(this->playerNum), tileStr), 157);
        }
        this->pixmapItems.append(createGraphicsItemPx(bkg));
        QGraphicsPixmapItem* bkgItem = this->pixmapItems.last();
        scene->addItem(this->pixmapItems.last());
        this->pixmapItems.last()->setPos(board::getPixelPosForTilePos(pos_));
        this->pixmapItems.last()->setVisible(true);
        if (this->counter != 0)
        {
            QPixmap counter_ = getPrimaryPathScaled(QString("img/c%1.png").arg(QString::number(this->counter)), 59);
            this->pixmapItems.append(createGraphicsItemPx(counter_));
            scene->addItem(this->pixmapItems.last());
            this->pixmapItems.last()->setPos(board::getPixelPosForTilePos(pos_).x() + board::getTileSize(pos_).width() - counter_.width(), board::getPixelPosForTilePos(pos_).y());
            this->pixmapItems.last()->setVisible(true);
        }
        QPixmap value = getPrimaryPathScaled(QString("img/s_%1.png").arg(QString::number(inGameDnD ? this->getBaseValue() : this->getCurrentValue())), 59);
        this->pixmapItems.append(createGraphicsItemPx(value));
        scene->addItem(this->pixmapItems.last());
        this->pixmapItems.last()->setPos(board::getPixelPosForTilePos(pos_));
        this->pixmapItems.last()->setVisible(true);
        QPixmap px = getPrimaryPathScaled(QString("cards/pictureSets/") + cardPictureSet + "/" + cardData[id].readValues("picture").first() + ".png", 157);
        bool shrinkInsteadOfBlur = mainWindow::getConfigVariable("shrinkInsteadOfBlur").toInt();
        px = px.scaled(QSize(board::getTileSize(pos_).width() * (this->_fSize || !shrinkInsteadOfBlur  ? 1.0 : 0.78), board::getTileSize(pos_).height()), Qt::KeepAspectRatio, Qt::SmoothTransformation);
        if (!this->_fSize && !shrinkInsteadOfBlur)
        {
            px = QPixmap::fromImage(blurred(px.toImage(), px.rect(), CARD_BLUR));
        }
        if (this->_grayScale)
        {
            QImage image = px.toImage();
            QRgb col;
            int gray;
            for (int i = 0; i < px.width(); ++i)
            {
                for (int j = 0; j < px.height(); ++j)
                {
                    col = image.pixel(i, j);
                    gray = qGray(col);
                    image.setPixel(i, j, qRgb(gray * 0.5, gray * 0.5, gray * 0.5));
                }
            }
            px = px.fromImage(image);
        }
        this->pixmapItems.append(createGraphicsItemPx(px));
        scene->addItem(this->pixmapItems.last());
        this->cardImgOnBoard = this->pixmapItems.last();
        this->pixmapItems.last()->setPos(board::getPixelPosForTilePos(pos_).x() + (board::getTileSize(pos_).width() - px.width()) / 2.0, board::getPixelPosForTilePos(pos_).y() + (board::getTileSize(pos_).height() - px.height()) / 2.0);
        this->pixmapItems.last()->setVisible(true);

        for (int i = 0; i < this->pixmapItems.size(); i++)
        {
            this->pixmapItems[i]->setTransformOriginPoint(this->pixmapItems[i]->mapFromScene(QPointF(bkgItem->pos().x() + board::getTileSize(pos_).width() / 2.0, bkgItem->pos().y() + board::getTileSize(pos_).height() / 2.0)));
            this->itemGroup->setPos(QPointF(bkgItem->pos().x() + board::getTileSize(pos_).width() / 2.0, bkgItem->pos().y() + board::getTileSize(pos_).height() / 2.0));
            if (!inGameDnD)
            {
                if (this->playerNum == 2)
                {
                    this->pixmapItems[i]->setRotation(180);
                }
                if (this->playerNum == 3)
                {
                    this->pixmapItems[i]->setRotation(90);
                }
                if (this->playerNum == 4)
                {
                    this->pixmapItems[i]->setRotation(-90);
                }
            }
        }
    }
    if (!do_not_save_details)
    {
        if (this->itemGroup->scene() != scene) scene->addItem(this->itemGroup);
        for (int i = 0; i < this->pixmapItems.size(); i++)
        {
            this->itemGroup->addToGroup(this->pixmapItems[i]);
        }
        this->itemGroup->disconnect();
        connect(this->itemGroup, &graphicsItemGroup::clicked, this, &card::clicked);
        connect(this->itemGroup, &graphicsItemGroup::rightClicked, this, &card::rightMouseClicked);
        connect(this->itemGroup, &graphicsItemGroup::mouseHeldDown, this, &card::mouseHeldDown);
        connect(this->itemGroup, &graphicsItemGroup::mouseEntered, this, &card::mouseEntered);
        connect(this->itemGroup, &graphicsItemGroup::mouseLeft, this, &card::mouseLeft);
        connect(this->itemGroup, &graphicsItemGroup::startDrag, this, &card::startDrag);
    }
}

QImage card::renderToPixmap(DrawingType t, QString forceLang, QString cardPictureSet, QString namePictureSet, bool noRotation)
{
    QGraphicsScene* scene = new QGraphicsScene;
    this->draw(scene, t, QPointF(0, 0), true, cardPictureSet, namePictureSet, forceLang, noRotation);
    scene->clearSelection();
    scene->setSceneRect(scene->itemsBoundingRect());
    QImage image(scene->sceneRect().size().toSize(), QImage::Format_ARGB32);
    image.fill(Qt::transparent);
    QPainter painter(&image);
    scene->render(&painter);
    this->removeFromScene(scene);
    scene->clear();
    scene->deleteLater();
    return image;
}

void card::redraw()
{
    if (this->gS) this->gS->sendGameEvent(QStringList() << "redrawCard" << QString::number(uid));
    if (prevDrawn && !this->scheduledDrawScene)
    {
        this->draw(prevScene, prevDrawingType, prevPos);
    }
    else this->redrawAsNeeded();
}

void card::setSelectedForPlacement(bool sel)
{
    this->selectedForPlacement = sel;
}

void card::setMarkedForTrash(bool marked)
{
    this->markedForTrash = marked;
}

void card::setMarkedForHuckster(bool marked)
{
    this->markedForHuckster = marked;
}

void card::setHighlightedInInventory(bool h)
{
    if (!this->gS || !this->gS->mW) return;

    if (!this->inventoryHighlight && h)
    {
        if (this->inventoryHighlightItem != nullptr)
        {
            this->itemGroup->removeFromGroup(this->inventoryHighlightItem);
            if (this->inventoryHighlightItem->scene() != nullptr)
            {
                this->inventoryHighlightItem->scene()->removeItem(this->inventoryHighlightItem);
                this->inventoryHighlightItem->deleteLater();
                this->inventoryHighlightItem = nullptr;
            }
        }
        QPixmap ih = getPrimaryPathScaled("img/inventory_highlight_overlay.png", 161);
        graphicsPixmapItem* it = new graphicsPixmapItem;
        it->setPixmap(ih);
        it->setPos(prevPos.x(), prevPos.y());
        it->setVisible(true);
        this->itemGroup->addToGroup(it);
        this->inventoryHighlightItem = it;
    }
    else if (this->inventoryHighlight && !h && this->inventoryHighlightItem != nullptr)
    {
        this->itemGroup->removeFromGroup(this->inventoryHighlightItem);
        if (this->inventoryHighlightItem->scene() != nullptr)
        {
            this->inventoryHighlightItem->scene()->removeItem(this->inventoryHighlightItem);
        }
        this->inventoryHighlightItem->deleteLater();
        this->inventoryHighlightItem = nullptr;
    }
    this->inventoryHighlight = h;
}

void card::setHighlightedOnBoard(bool)
{
    //TODO
    return;
}

void card::deleteCardLater()
{
    if (this->uid >= 0) this->currentCards.remove(this->uid);
    this->deleteLater();
}

void card::setDisabledInInventory(bool d)
{
    if (this->disabled != d)
    {
        this->disabled = d;
        if (d)
        {
            this->overlay = CardInventoryOverlay::DisabledOverlay;
        }
        else
        {
            this->overlay = CardInventoryOverlay::NoOverlay;
        }
        this->redraw();
    }
}

bool card::isDisabledInInventory() const
{
    return this->disabled;
}

void card::moveOnBoardTo(const QPoint& target, bool async)
{
    while (this->isBeingAnimated()) QApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
    if (this->itemGroup)
    {
        this->setZValue(100000);
        this->setPosition(target);
        auto target_f = QPointF(board::getPixelPosForTilePos(target).x() + board::getTileSize(target).width() / 2, board::getPixelPosForTilePos(target).y() + board::getTileSize(target).height() / 2);
        QGraphicsItemAnimation* anim = new QGraphicsItemAnimation();
        anim->setItem(this->itemGroup);
        QTimeLine* timer = new QTimeLine(850);
        timer->setEasingCurve(QEasingCurve::InOutQuad);
        anim->setTimeLine(timer);
        anim->setPosAt(1, target_f);
        if (this->gS && this->gS->mW) this->gS->mW->setUIDisabledForAnimation(true);
        this->animInProgress = true;
        connect(timer, &QTimeLine::finished, this, [this, anim, target, timer]
        {
            this->setZValue(0);
            if (anim) anim->deleteLater();
            if (timer) timer->deleteLater();
            this->animInProgress = false;
            this->draw(this->prevScene, this->prevDrawingType, target);
            if (this->gS && this->gS->mW) this->gS->mW->setUIDisabledForAnimation(false);
            emit this->moveAnimationFinished();
            this->removeFromSceneAsNeeded();
            this->redrawAsNeeded();
        });
        timer->start();
        if (!async) while (this->isBeingAnimated()) QApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
    }
}

void card::animateBoardPlacement(QGraphicsScene* sc)
{
    while (this->isBeingAnimated()) QApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
    if (this->itemGroup && sc)
    {
        this->draw(sc, DrawingType::InGame, QPointF(this->pos.x(), this->pos.y()));
        this->setZValue(100000);
        QPropertyAnimation* anim = new QPropertyAnimation(this->itemGroup, "scale", this);
        anim->setEasingCurve(QEasingCurve::InQuad);
        anim->setDuration(500);
        anim->setStartValue(qreal(1.4));
        anim->setEndValue(qreal(1.0));
        if (this->gS && this->gS->mW) this->gS->mW->setUIDisabledForAnimation(true);
        this->animInProgress = true;
        connect(anim, &QPropertyAnimation::finished, this, [this, anim]
        {
            this->setZValue(0);
            this->animInProgress = false;
            if (anim) anim->deleteLater();
            if (this->gS && this->gS->mW) this->gS->mW->setUIDisabledForAnimation(false);
            this->removeFromSceneAsNeeded();
            this->redrawAsNeeded();
        });
        anim->start();
        while (this->isBeingAnimated()) QApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
    }
}

void card::animateSilverAction()
{
    while (this->isBeingAnimated()) QApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
    if (this->gS) this->gS->sendGameEvent(QStringList() << "animateCardSilverAction" << QString::number(this->uid));
    if (this->itemGroup)
    {
        this->setZValue(100000);
        QGraphicsItemAnimation* anim = new QGraphicsItemAnimation();
        anim->setItem(this->itemGroup);
        QTimeLine* timer = new QTimeLine(850);
        timer->setEasingCurve(QEasingCurve::InOutQuad);
        anim->setTimeLine(timer);
        anim->setScaleAt(0, 1.0, 1.0);
        anim->setScaleAt(0.5, 1.4, 1.4);
        anim->setScaleAt(1, 1.0, 1.0);
        if (this->gS && this->gS->mW) this->gS->mW->setUIDisabledForAnimation(true);
        this->animInProgress = true;
        connect(timer, &QTimeLine::finished, this, [this, anim, timer]
        {
            this->setZValue(0);
            this->animInProgress = false;
            if (anim) anim->deleteLater();
            if (timer) timer->deleteLater();
            if (this->gS && this->gS->mW) this->gS->mW->setUIDisabledForAnimation(false);
            emit this->silverActionAnimationFinished();
            this->removeFromSceneAsNeeded();
            this->redrawAsNeeded();
        });
        timer->start();
        while (this->isBeingAnimated()) QApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
    }
}

void card::scheduleRedraw(QGraphicsScene* scene, DrawingType t, QPointF pos, bool do_not_save_details, QString cardPictureSet, QString namePictureSet)
{
    this->scheduledDrawScene = scene;
    this->scheduledDrawingType = t;
    this->scheduledPos = pos;
    this->scheduledDoNotSaveDetails = do_not_save_details;
    this->scheduledCardPictureSet = cardPictureSet;
    this->scheduledNamePictureSet = namePictureSet;
}

void card::scheduleRemoveFromScene(QGraphicsScene* scene)
{
    this->scheduledRemoveFromScene = scene;
}

void card::redrawAsNeeded()
{
    if (this->scheduledDrawScene)
    {
        this->draw(this->scheduledDrawScene, this->scheduledDrawingType, this->scheduledPos, this->scheduledDoNotSaveDetails, this->scheduledCardPictureSet, this->scheduledNamePictureSet);
    }
}

void card::removeFromSceneAsNeeded()
{
    if (this->scheduledRemoveFromScene)
    {
        this->removeFromScene(nullptr);
    }
}

void card::animateRemovalFromBoard(QGraphicsScene* sc)
{
    while (this->isBeingAnimated()) QApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
    if (this->itemGroup && sc)
    {
        this->draw(sc, DrawingType::InGame, QPointF(this->pos.x(), this->pos.y()));
        QGraphicsItemAnimation* anim = new QGraphicsItemAnimation();
        anim->setItem(this->itemGroup);
        QTimeLine* timer = new QTimeLine(600);
        timer->setEasingCurve(QEasingCurve::InQuad);
        anim->setTimeLine(timer);
        anim->setScaleAt(0, 1.0, 1.0);
        anim->setScaleAt(1, 0.001, 0.001);
        if (this->gS && this->gS->mW) this->gS->mW->setUIDisabledForAnimation(true);
        this->animInProgress = true;
        connect(timer, &QTimeLine::finished, this, [this, anim, timer, sc]
        {
            this->removeFromScene(sc);
            this->animInProgress = false;
            if (anim) anim->deleteLater();
            if (timer) timer->deleteLater();
            if (this->gS && this->gS->mW) this->gS->mW->setUIDisabledForAnimation(false);
            emit this->boardRemovalAnimFinished();
            this->removeFromSceneAsNeeded();
            this->redrawAsNeeded();
        });
        timer->start();
        while (this->isBeingAnimated()) QApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
    }
}

bool card::isMarkedForTrash() const
{
    return this->markedForTrash;
}

bool card::isMarkedForHuckster() const
{
    return this->markedForHuckster;
}

void card::removeFromScene(QGraphicsScene* scene)
{
    if (!this->gS || !this->gS->mW) return;

    if (scene == this->scheduledDrawScene) this->scheduledDrawScene = nullptr;

    if (this->isBeingAnimated())
    {
        this->scheduleRemoveFromScene(scene);
        return;
    }

    if (this->scheduledRemoveFromScene)
    {
        QGraphicsScene* scn = this->scheduledRemoveFromScene;
        this->scheduledRemoveFromScene = nullptr;
        this->removeFromScene(scn);
    }

    if (!scene) return;

    this->cardImgOnBoard = nullptr;

    if (this->inventoryHighlightItem != nullptr)
    {
        this->itemGroup->removeFromGroup(this->inventoryHighlightItem);
        scene->removeItem(this->inventoryHighlightItem);
        this->inventoryHighlightItem->deleteLater();
        this->inventoryHighlightItem = nullptr;
    }

    for (int i = 0; i < this->pixmapItems.size(); i++)
    {
        QGraphicsPixmapItem* it = this->pixmapItems[i];
        this->itemGroup->removeFromGroup(it);
        scene->removeItem(it);
        if (auto it_ = dynamic_cast<graphicsPixmapItem*>(it))
        {
            it_->deleteLater();
        }
        else
        {
            delete it;
        }
    }

    if (this->itemGroup->scene() != nullptr) //causes warnings, probably bc the scene of the itemgroup is different from the scene of the contained items (???), idk how to fix this
    {
        if (this->itemGroup)
        {
            scene->removeItem(this->itemGroup);
            this->itemGroup->deleteLater();
        }
        this->itemGroup = new graphicsItemGroup;
    }
    this->pixmapItems.clear();
}

void card::changeOwner(int playerNum)
{
#ifdef BAZAAR_DEBUG
    if (playerNum == -1)
    {
        mainWindow::reportError(tr("Script error: trying to change owner of card to invalid player, this is probably a bug in the following script:\n%1").arg(gameState::scriptExecInfo.join("\n")));
    }
#endif
    if (this->gS) this->gS->sendGameEvent(QStringList() << "changeOwner" << QString::number(uid) << QString::number(playerNum));
    this->rotate(playerNum);
}

void card::rotate(int playerNum)
{
    if (this->rotationTimer) this->rotationTimer->stop();
    QMap<int, QMap<int, int>> rotationMap =
    {
        {1, {{1, 0}, {2, 180}, {3, 90}, {4, -90}}},
        {2, {{2, 0}, {1, 180}, {4, 90}, {3, -90}}},
        {3, {{3, 0}, {2, 90}, {1, -90}, {4, -180}}},
        {4, {{4, 0}, {2, -90}, {3, -180}, {1, 90}}},
    };
    this->rotSteps = this->currRotStep = abs(rotationMap[this->playerNum][playerNum]);
    this->rotDegree = rotationMap[this->playerNum][playerNum];
    this->playerNum = playerNum;
    if (!this->rotationTimer || !this->gS) return;
    if (this->gS->gameBoard->getAllCards().contains(this))
    {
        for (auto it : this->pixmapItems)
        {
            it->setTransformOriginPoint(it->mapFromScene(QPointF(board::getPixelPosForTilePos(this->pos).x() + board::getTileSize(this->pos).width() / 2.0,
                                                                 board::getPixelPosForTilePos(this->pos).y() + board::getTileSize(this->pos).height() / 2.0)));
        }
        if (!mainWindow::getConfigVariable("gameAnimations").toInt() || !this->gS || !this->gS->mW || !this->gS->mW->isGamePageVisible())
        {
            for (auto it : this->pixmapItems)
            {
                it->setRotation(it->rotation() + static_cast<double>(rotDegree));
            }
            this->redraw();
        }
        else
        {
            this->rotationTimer->setInterval(1000.0 / 120.0);
            this->rotationTimer->start();
            if (this->gS && this->gS->mW) this->gS->mW->setUIDisabledForAnimation(true);
            while (this->isBeingAnimated()) QApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
            if (this->gS && this->gS->mW) this->gS->mW->setUIDisabledForAnimation(false);
        }
    }
}

bool card::isBeingAnimated() const
{
    return (this->rotationTimer && this->rotationTimer->isActive()) || this->animInProgress;
}

bool card::hasSilverAction() const
{
    return this->hasSilver;
}

fileFormat::saveFile card::save(bool forOOSCheck) const
{
    fileFormat::saveFile f;
    QStringMap m;
    m["id"] = this->id;
    m["counter"] = QString::number(counter);
    if (!forOOSCheck)
    {
        m["markedForTrash"] = QString::number(markedForTrash);
        m["markedForHuckster"] = QString::number(markedForHuckster);
        m["overlay"] = QString::number(int(overlay));
    }
    m["uid"] = QString::number(uid);
    m["playerNum"] = QString::number(playerNum);
    m["grayScale"] = QString::number(this->isGrayScale());
    m["showFullSize"] = QString::number(this->isFullSize());
    m["posX"] = QString::number(this->pos.x());
    m["posY"] = QString::number(this->pos.y());
    f.addItem("card", QString(), m);
    return f;
}

card* card::clone(gameState* newState) const
{
    card* res = new card(this->id, this->playerNum, newState);
    res->counter = this->counter;
    res->tmpRot = this->tmpRot;
    res->oldRot = this->oldRot;
    res->selectedForPlacement = this->selectedForPlacement;
    res->markedForHuckster = this->markedForHuckster;
    res->markedForTrash = this->markedForTrash;
    res->disabled = this->disabled;
    res->pos = this->pos;
    res->prevPos = this->prevPos;
    res->val = this->val;
    res->_grayScale = this->_grayScale;
    res->_fSize = this->_fSize;
    res->overlay = this->overlay;
    return res;
}

card::~card()
{
    if (this->uid >= 0) this->currentCards.remove(this->uid);
    this->removeFromPrevScene();
    if (itemGroup && itemGroup->scene())
    {
        itemGroup->scene()->destroyItemGroup(itemGroup);
        itemGroup = nullptr;
    }
    else if (itemGroup) itemGroup->deleteLater();
    if (inventoryHighlightItem && inventoryHighlightItem->scene()) inventoryHighlightItem->scene()->removeItem(inventoryHighlightItem);
    for (auto& px : pixmapItems)
    {
        if (px->scene())
        {
            px->scene()->removeItem(px);
        }
        delete px;
        px = nullptr;
    }
    if(this->inventoryHighlightItem) inventoryHighlightItem->deleteLater();
    if (rotationTimer) rotationTimer->deleteLater();
}

void card::setZValue(int v)
{
    if (this->itemGroup) this->itemGroup->setZValue(v);
    for (auto& it : this->pixmapItems)
    {
        if (it) it->setZValue(v);
    }
}

bool card::hasEvent(const QString& evName) const
{
    return cardEvs[this->id].contains(evName);
}

void card::init(const QString& _id, int player, gameState* gS, int globalUID)
{
    if (globalUID < 0)
    {
        if (globalUID < -1)
        {
            this->uid = globalUID;
        }
        else
        {
            this->uid = card::globalUID++;
        }
    }
    else
    {
        this->uid = globalUID;
    }
    if (this->uid >= 0)
    {
        if (this->currentCards.contains(this->uid))
        {
            mainWindow::reportError(tr("Error: duplicate card UID, game out of sync or script error"));
        }
        this->currentCards[this->uid] = this;
    }
    this->overlay = CardInventoryOverlay::NoOverlay;
    this->gS = gS;
    this->selectedForPlacement = false;
    this->prevDrawn = false;
    this->_fSize = true;
    this->_grayScale = false;
    this->disabled = false;
    this->tmpRot = false;
    this->markedForTrash = false;
    this->markedForHuckster = false;
    this->inventoryHighlightItem = nullptr;
    this->pos = QPoint(-1, -1);
    this->id = _id;
    if (!cardData.count(id))
    {
        cardData[id] = cfgFile(getPrimaryPath("cards/cfg/" + this->id + ".cfg"));
    }
    if (!cardCode.count(id))
    {
        QFile codeFile(getPrimaryPath("cards/js/" + this->id + ".js"));
        if (!codeFile.open(QFile::ReadOnly))
        {
            mainWindow::reportError(tr("Error: cannot open script file: %1").arg(this->id + ".js"));
        }
        else
        {
            QTextStream stream(&codeFile);
            //stream.setCodec("UTF-8");
            QString c = stream.readAll();
            for (const auto& e : ALL_EVENTS)
            {
                if (c.contains(e)) cardEvs[id].insert("\"" + QString{e} + "\"");
            }
            cardCode[id] = mainWindow::processIncludes(c);
            codeFile.close();
        }
    }
    this->val = cardData[id].readValues("score").first().toInt();
    this->counter = 0;
    this->prevScene = nullptr;
    this->prevDrawingType = DrawingType::Inventory;
    this->playerNum = player;
    this->inventoryHighlight = false;
    this->hasSilver = !cardData[id].readLocalizedValues("silverText", mainWindow::getLanguageCode()).join(',').isEmpty();

    if (this->gS && this->gS->mW)
    {
        this->itemGroup = new graphicsItemGroup;
        this->rotationTimer = new QTimer;
        this->rotationTimer->setTimerType(Qt::PreciseTimer);
        connect(this->rotationTimer, &QTimer::timeout, this, &card::rotationStep);
    }
}

void card::setPlayer(int playerNum)
{
    this->playerNum = playerNum;
}

int card::getUID() const
{
    return this->uid;
}

bool card::isFullSize() const
{
    return this->_fSize;
}

bool card::isGrayScale() const
{
    return this->_grayScale;
}

int card::getCounter() const
{
    return this->counter;
}

void card::setCounter(int c)
{
    if (this->gS) this->gS->sendGameEvent(QStringList() << "setCounter" << QString::number(uid) << QString::number(c));
    this->counter = c;
    if (this->prevDrawingType == DrawingType::InGame)
    {
        this->redraw();
    }
}

QStringList card::getIcons() const
{
    return cardData[id].readValues("icons");
}

bool card::hasIcon(const QString& icon) const
{
    return cardData[id].readValues("icons").contains(icon);
}

int card::getBaseValue() const
{
    return this->val;
}

QString card::getName() const
{
    return cardData[id].readLocalizedValues("name", mainWindow::getLanguageCode()).first();
}

QString card::getBackground() const
{
    return cardData[id].readValues("background").first();
}

int card::getCurrentValue()
{
#ifdef BAZAAR_DEBUG
    gameState::scriptExecInfo.append(this->getName() + " (getCurrentValue)");
#endif
    auto val = this->gS->scriptEnv->executeScript(this->getScriptCode(), "getCurrentValue", this).constFirst().toInt();
#ifdef BAZAAR_DEBUG
    gameState::scriptExecInfo.pop_back();
#endif
    return val;
}

void card::setGrayScale(bool grayScale, bool disableAnimation)
{
    if (this->gS) this->gS->sendGameEvent(QStringList() << "setGrayScale" << QString::number(uid) << QString::number(grayScale) << QString::number(disableAnimation));
    if (this->_grayScale == grayScale) return;
    this->_grayScale = grayScale;
    while (this->isBeingAnimated()) QApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
    if (!disableAnimation && mainWindow::getConfigVariable("gameAnimations").toInt() && this->gS && this->gS->mW && this->gS->mW->isGamePageVisible())
    {
        QTimer* animTimer = new QTimer;
        animTimer->setTimerType(Qt::PreciseTimer);
        if (this->gS && this->gS->mW) this->gS->mW->setUIDisabledForAnimation(true);
        this->animInProgress = true;
        int* animCounter = new int{0};
        bool shrinkInsteadOfBlur = mainWindow::getConfigVariable("shrinkInsteadOfBlur").toInt();
        connect(animTimer, &QTimer::timeout, animTimer, [animCounter, animTimer, this, grayScale, shrinkInsteadOfBlur]()
        {
            if (this->cardImgOnBoard)
            {
                double progress = *animCounter / 25.0;
                QImage image = QImage(getPrimaryPathScaled("cards/pictureSets/" + mainWindow::cardPictureSet + "/" + this->cardData[id].readValues("picture").first() + ".png", 157).toImage());
                image = image.scaled(QSize(board::getTileSize(this->getPosition()).width() * (this->_fSize || !shrinkInsteadOfBlur  ? 1.0 : 0.78), board::getTileSize(this->getPosition()).height()), Qt::KeepAspectRatio, Qt::SmoothTransformation);
                if (!this->_fSize && !shrinkInsteadOfBlur) image = blurred(image, image.rect(), CARD_BLUR);
                int w = image.width();
                int h = image.height();
                if (!grayScale) progress = 1.0 - progress;
                QRgb col;
                int gray;
                for (int i = 0; i < w; ++i)
                    //for (int i = 0+progress*w/2.0; i < w-progress*w/2.0; ++i)
                {
                    for (int j = 0; j < h; ++j)
                        //for (int j = 0+progress*h/2.0; j < h-progress*h/2.0; ++j)
                    {
                        col = image.pixel(i, j);
                        gray = qGray(col) * 0.5;
                        image.setPixel(i, j, qRgb(progress * gray + (1 - progress)*qRed(col),
                                                  progress * gray + (1 - progress)*qGreen(col),
                                                  progress * gray + (1 - progress)*qBlue(col)
                                                 ));
                    }
                }
                this->cardImgOnBoard->setPixmap(QPixmap::fromImage(image));
                this->cardImgOnBoard->update();
            }
            else
            {
                *animCounter = 25;
            }
            (*animCounter)++;
            if (*animCounter > 25)
            {
                *animCounter = 0;
                animTimer->stop();
                animTimer->deleteLater();
                this->animInProgress = false;
                if (this->gS && this->gS->mW) this->gS->mW->setUIDisabledForAnimation(false);
            }

        });
        animTimer->setInterval(30);
        animTimer->start();
        while (this->isBeingAnimated()) QApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
    }
    this->redraw();
}

void card::setShowFullSize(bool fullSize, bool disableAnimation)
{
    if (this->gS) this->gS->sendGameEvent(QStringList() << "setShowFullSize" << QString::number(uid) << QString::number(fullSize) << QString::number(disableAnimation));
    if (this->_fSize == fullSize) return;
    this->_fSize = fullSize;
    while (this->isBeingAnimated()) QApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
    if (this->getPosition() == QPoint(-1, -1)) return;
    QImage px = QImage(getPrimaryPathScaled("cards/pictureSets/" + mainWindow::cardPictureSet + "/" + this->cardData[id].readValues("picture").first() + ".png", 157).toImage());
    px = px.scaled(QSize(board::getTileSize(this->getPosition()).width(), board::getTileSize(this->getPosition()).height()), Qt::KeepAspectRatio, Qt::SmoothTransformation);
    if (this->_grayScale)
    {
        QRgb col;
        int gray;
        for (int i = 0; i < px.width(); ++i)
        {
            for (int j = 0; j < px.height(); ++j)
            {
                col = px.pixel(i, j);
                gray = qGray(col);
                px.setPixel(i, j, qRgb(gray * 0.5, gray * 0.5, gray * 0.5));
            }
        }
    }
    QImage px_blurred = blurred(px, px.rect(), CARD_BLUR);

    if (!disableAnimation && mainWindow::getConfigVariable("gameAnimations").toInt() && this->gS && this->gS->mW && this->gS->mW->isGamePageVisible())
    {
        QTimer* animTimer = new QTimer;
        animTimer->setTimerType(Qt::PreciseTimer);
        if (this->gS && this->gS->mW) this->gS->mW->setUIDisabledForAnimation(true);
        bool shrinkInsteadOfBlur = mainWindow::getConfigVariable("shrinkInsteadOfBlur").toInt();
        this->animInProgress = true;
        int* animCounter = new int{0};
        connect(animTimer, &QTimer::timeout, animTimer, [animCounter, shrinkInsteadOfBlur, this, fullSize, animTimer, px, px_blurred]()
        {
            if (shrinkInsteadOfBlur)
            {
                if (this->cardImgOnBoard)
                {
                    double scale = 0.22 * 2 * *animCounter / 50.0;
                    double invScale = 0;
                    QPixmap px = QPixmap(QString(getPrimaryPath("cards/pictureSets/" + mainWindow::cardPictureSet + "/" + this->cardData[id].readValues("picture").first() + ".png")));
                    if (fullSize)
                    {
                        invScale = 1.0 - scale;
                        scale = 0.78 + scale;
                    }
                    else
                    {
                        invScale = 0.78 + scale;
                        scale = 1.0 - scale;
                    }
                    px = px.scaled(QSize(board::getTileSize(this->getPosition()).width() * scale, board::getTileSize(this->getPosition()).height() * scale), Qt::KeepAspectRatio, Qt::SmoothTransformation);
                    if (this->_grayScale)
                    {
                        QImage image = px.toImage();
                        QRgb col;
                        int gray;
                        int w = px.width();
                        int h = px.height();
                        for (int i = 0; i < w; ++i)
                        {
                            for (int j = 0; j < h; ++j)
                            {
                                col = image.pixel(i, j);
                                gray = qGray(col) * 0.5;
                                image.setPixel(i, j, qRgb(gray, gray, gray));
                            }
                        }
                        px = px.fromImage(image);
                    }
                    this->cardImgOnBoard->setPixmap(px);
                    if(this->cardImgOnBoard->parentItem())
                    {
                        if(!fullSize) {
                            auto origWidth = px.width()/scale;
                            auto origHeight = px.height()/scale;
                            this->cardImgOnBoard->setOffset((origWidth - px.width()) / 2.0, (origHeight - px.height()) / 2.0);
                        } else {
                            auto origWidth = px.width()*invScale;
                            auto origHeight = px.height()*invScale;
                            this->cardImgOnBoard->setOffset((origWidth - px.width()) / 2.0, (origHeight - px.height()) / 2.0);
                        }
                    }
                    this->cardImgOnBoard->update();
                }
                else
                {
                    *animCounter = 25;
                }
                (*animCounter)++;
                if (*animCounter > 25)
                {
                    *animCounter = 0;
                    animTimer->stop();
                    animTimer->deleteLater();
                    this->animInProgress = false;
                    if (this->gS && this->gS->mW) this->gS->mW->setUIDisabledForAnimation(false);
                }
            }
            else
            {
                if (this->cardImgOnBoard)
                {
                    double progress = *animCounter / 25.0;
                    QImage image(px);
                    int w = px.width();
                    int h = px.height();
                    if (fullSize) progress = 1.0 - progress;
                    QRgb col, colb;
                    for (int i = 0; i < w; ++i)
                        //for (int i = 0+progress*w/2.0; i < w-progress*w/2.0; ++i)
                    {
                        for (int j = 0; j < h; ++j)
                            //for (int j = 0+progress*h/2.0; j < h-progress*h/2.0; ++j)
                        {
                            col = px.pixel(i, j);
                            colb = px_blurred.pixel(i, j);
                            image.setPixel(i, j, qRgb(progress * qRed(colb) + (1 - progress)*qRed(col),
                                                      progress * qGreen(colb) + (1 - progress)*qGreen(col),
                                                      progress * qBlue(colb) + (1 - progress)*qBlue(col)
                                                     ));
                        }
                    }
                    this->cardImgOnBoard->setPixmap(QPixmap::fromImage(image));
                    this->cardImgOnBoard->update();
                }
                else
                {
                    *animCounter = 25;
                }
                (*animCounter)++;
                if (*animCounter > 25)
                {
                    *animCounter = 0;
                    animTimer->stop();
                    animTimer->deleteLater();
                    this->animInProgress = false;
                    if (this->gS && this->gS->mW) this->gS->mW->setUIDisabledForAnimation(false);
                }
            }
        });
        animTimer->setInterval(30);
        animTimer->start();
        while (this->isBeingAnimated()) QApplication::processEvents(QEventLoop::ExcludeSocketNotifiers);
    }
    this->redraw();
}

const QString& card::getScriptCode() const
{
    return cardCode[this->id];
}

void card::rotationStep()
{
    double l = std::abs(this->rotSteps);
    double x = std::abs(this->currRotStep);
    double s = (x - l) * (x - l * 0.5) / (l * l * 0.5) + 1.4 * x * (x - l) / (-l * 0.5 * l * 0.5) + 1.0 * x * (x - l * 0.5) / (l * 0.5 * l);
    if (this->currRotStep < 0)
    {
        this->setZValue(CARD_ROTATION_ZVALUE);
        for (auto it : this->pixmapItems)
        {
            it->setRotation(it->rotation() + static_cast<double>(rotDegree) / this->rotSteps);
            it->setScale(s);
        }
        ++this->currRotStep;
    }
    else if (this->currRotStep > 0)
    {
        this->setZValue(CARD_ROTATION_ZVALUE);
        for (auto it : this->pixmapItems)
        {
            it->setRotation(it->rotation() + static_cast<double>(rotDegree) / this->rotSteps);
            it->setScale(s);
        }
        --this->currRotStep;
    }
    else
    {
        this->setZValue(0);
        for (auto it : this->pixmapItems)
        {
            it->setScale(1.0);
        }
        this->rotationTimer->stop();
        this->redraw();
    }
}

QString card::getID() const
{
    return this->id;
}

QPoint card::getPosition() const
{
    return this->pos;
}

int card::getPosX() const
{
    return this->pos.x();
}

int card::getPosY() const
{
    return this->pos.y();
}

void card::setPosition(const QPoint& p)
{
    if (this->gS) this->gS->sendGameEvent(QStringList() << "setPosition" << QString::number(uid) << QString::number(p.x()) << QString::number(p.y()));
    this->pos = p;
}

void card::removeFromPrevScene()
{
    this->removeFromScene(this->prevScene);
}

void card::setInventoryOverlay(CardInventoryOverlay overlay)
{
    this->overlay = overlay;
}

int card::getPlayer() const
{
    return this->playerNum;
}

graphicsPixmapItem::graphicsPixmapItem()
{
    this->pTimer = new QTimer;
    this->setAcceptHoverEvents(true);
    //this->setFiltersChildEvents(true);
    //this->setHandlesChildEvents(true);
    connect(pTimer, &QTimer::timeout, this, &graphicsPixmapItem::mouseHeldDown);
}

graphicsPixmapItem::~graphicsPixmapItem()
{
    pTimer->deleteLater();
}

void graphicsPixmapItem::mousePressEvent(QGraphicsSceneMouseEvent* ev)
{
    if (ev->button() == Qt::LeftButton)
    {
        dragStartPosition = ev->pos();

        pTimer->stop();
        pTimer->setSingleShot(true);
        pTimer->setInterval(1500);
        pTimer->start();
    }
}

void graphicsPixmapItem::mouseReleaseEvent(QGraphicsSceneMouseEvent* ev)
{
    this->pTimer->stop();
    if (ev->button() == Qt::LeftButton)
    {
        emit this->clicked();
    }
    if (ev->button() == Qt::RightButton)
    {
        emit this->rightClicked();
    }
}

void graphicsPixmapItem::hoverEnterEvent(QGraphicsSceneHoverEvent* event)
{
    emit this->mouseEntered();
    QGraphicsPixmapItem::hoverEnterEvent(event);
}

void graphicsPixmapItem::hoverLeaveEvent(QGraphicsSceneHoverEvent* event)
{
    emit this->mouseLeft();
    QGraphicsPixmapItem::hoverLeaveEvent(event);
}

void graphicsPixmapItem::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
    if (!(event->buttons() & Qt::LeftButton)) return;
    if ((event->pos() - dragStartPosition).manhattanLength() < QApplication::startDragDistance()) return;
    this->pTimer->stop();
    emit this->startDrag();
    QGraphicsPixmapItem::mouseMoveEvent(event);
}

graphicsItemGroup::graphicsItemGroup()
{
    this->pTimer = new QTimer;
    this->setAcceptHoverEvents(true);
    connect(pTimer, &QTimer::timeout, this, &graphicsItemGroup::mouseHeldDown);
}

graphicsItemGroup::~graphicsItemGroup()
{
    pTimer->deleteLater();
}

void graphicsItemGroup::mousePressEvent(QGraphicsSceneMouseEvent* ev)
{
    if (ev->button() == Qt::LeftButton)
    {
        dragStartPosition = ev->pos();

        pTimer->stop();
        pTimer->setSingleShot(true);
        pTimer->setInterval(1500);
        pTimer->start();
    }

    ev->accept();
}

void graphicsItemGroup::mouseReleaseEvent(QGraphicsSceneMouseEvent* ev)
{
    this->pTimer->stop();
    if (ev->button() == Qt::LeftButton)
    {
        emit this->clicked();
    }
    if (ev->button() == Qt::RightButton)
    {
        emit this->rightClicked();
    }

    ev->accept();
}

void graphicsItemGroup::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
    if (!(event->buttons() & Qt::LeftButton)) return;
    if ((event->pos() - dragStartPosition).manhattanLength() < QApplication::startDragDistance()) return;
    this->pTimer->stop();
    emit this->startDrag();
    event->accept();
}

void graphicsItemGroup::hoverEnterEvent(QGraphicsSceneHoverEvent* event)
{
    emit this->mouseEntered();
    event->accept();
}

void graphicsItemGroup::hoverLeaveEvent(QGraphicsSceneHoverEvent* event)
{
    emit this->mouseLeft();
    event->accept();
}

QMap<QString, cfgFile> card::cardData{};
QMap<QString, QString> card::cardCode{};
QMap<int, card*> card::currentCards{};
QMap<QString, QSet<QString>> card::cardEvs{};
