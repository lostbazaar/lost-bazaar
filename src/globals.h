/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QString>
#include <QDir>
#include <QStandardPaths>
#include <QTextStream>
#include <QCoreApplication>

#define LOST_BAZAAR_VERSION "1.0 alpha 9"
#define NETWORK_PROTOCOL_VERSION "1.0 alpha 7"

#ifdef Q_OS_ANDROID
#define DATA_PATH "assets:/data"
#elif defined(Q_OS_WASM)
#define DATA_PATH ":/data"
#else
#define DATA_PATH "./data"
#endif

using QStringMap = QMap<QString, QString>;

QString getPrimaryPath(const QString& requestedPath, bool silent = false);
QPixmap getPrimaryPathScaled(const QString& requestedPath, int originalSize);
QString userContentPath();

#define BAZAAR_DEBUG

#define USE_PRERENDERED_CARDS true

#define FILEFORMAT_VERSION "1.0 alpha 4"
#define QDATASTREAM_VERSION QDataStream::Qt_5_5

#define BOARD_CHOICE_ZVALUE 100

#define DEFAULT_LANGUAGE "en_US"

#define CHAT_PANEL_MAX_HEIGHT 200

extern QString lineEnd;
