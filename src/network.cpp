/*
Lost Bazaar
Copyright (C) 2014-2023  Lost Bazaar developers  <lostbazaar@lost-bazaar.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <QIcon>
#include "globals.h"
#include "network.h"
#include "mainwindow.h"

network::networkConnectionManager::networkConnectionManager()
{
    this->relaySocket = new QTcpSocket(this);

    connect(this->relaySocket, SIGNAL(readyRead()), this, SLOT(handleNewMessage())); //old style because new style does not support default parameters
    connect(this->relaySocket, &QTcpSocket::errorOccurred, this, [this](QAbstractSocket::SocketError)
    {
        emit this->relayDisconnected();
        mainWindow::reportError(tr("Network error: %1").arg(this->relaySocket->errorString()));
        this->reset(true);
    });
    connect(this->relaySocket, SIGNAL(disconnected()), this, SIGNAL(relayDisconnected()));
    connect(this->relaySocket, SIGNAL(connected()), this, SIGNAL(relayConnected()));
    connect(this->relaySocket, &QTcpSocket::connected, this, [this]()
    {
        this->setCurrentPublicGameID({}, {});
    });
}

void network::networkConnectionManager::start(network::NetworkGameMode mode, const QString& hostUsername, const QString& password)
{
    this->m = mode;
    this->password = password;
    if (hostUsername.isEmpty())
    {
        this->relayedHostIP = mainWindow::getConfigVariable("username");
    }
    else
    {
        this->relayedHostIP = hostUsername;
    }
    if (mode == network::NetworkGameMode::ClientMode)
    {
        emit this->connectedToHost();
    }
}

bool network::networkConnectionManager::tryAddUser(const QString& username)
{
    if (username == mainWindow::getConfigVariable("username")) return false;
    for (const auto& usr : std::as_const(this->relayClients))
    {
        if (usr == username) return false;
    }
    this->relayClients.insert(username);
    return true;
}

void network::networkConnectionManager::clearClientConnections(bool all, const QString& relayID)
{
    if (all)
    {
        this->relayClients.clear();
    }
    if (!relayID.isEmpty())
    {
        for (const auto& c : std::as_const(this->relayClients))
        {
            if (c == relayID)
            {
                this->relayClients.remove(c);
                break;
            }
        }
        emit this->aUserDisconnected(relayID);
    }
}

void network::networkConnectionManager::handleNewMessage(QTcpSocket* s)
{
    if (!s)
    {
        s = dynamic_cast<QTcpSocket*>(QObject::sender());
    }
    qint64 blockSize = 0;

    QByteArray receivedMessage;

    QTcpSocket* socket;
    if (s == nullptr)
    {
        socket = this->internalTcpSocket;
    }
    else
    {
        socket = s;
    }

    QDataStream in(socket);
    in.setVersion(QDATASTREAM_VERSION);

    if (socket->bytesAvailable() < static_cast<qint64>(sizeof(qint64)))
        return;

    auto s_p = socket->peek(sizeof(qint64));
    QDataStream inTmp(s_p);
    inTmp.setVersion(QDATASTREAM_VERSION);
    inTmp >> blockSize;

    if (!(socket->bytesAvailable() >= static_cast<qint64>(sizeof(qint64) + blockSize))) return;

    in >> blockSize;
    in >> receivedMessage;

    networkDataPackage p(receivedMessage);

    if (p.getType() == network::MessageType::InvalidMessage)
    {
        mainWindow::reportError(tr("Error: invalid network message received"));
        if (socket->bytesAvailable()) handleNewMessage(s);
        return;
    }

#ifdef BAZAAR_DEBUG
    QTextStream out(stdout);
    out << "MESSAGE RECEIVED: " << p.toDebugString() << Qt::endl;
#endif

    QString relayID;
    if (p.getType() != network::MessageType::RelayMsg)
    {
        relayID = p.getStringAt(2);
        p.eraseRelayData();
    }

    if (this->m == NetworkGameMode::HostMode)
    {
        if (p.getType() == MessageType::InitialMessage)
        {
            if (p.length() != 3)
            {
                this->sendMessageToSocket(networkDataPackage(MessageType::DisconnectedFromGame, QStringList() << "badMessage"), relayID);
                this->clearClientConnections();
            }
            else if (p.getStringAt(0) != LOST_BAZAAR_VERSION)
            {
                this->sendMessageToSocket(networkDataPackage(MessageType::DisconnectedFromGame, QStringList() << "badVersion"), relayID);
                this->clearClientConnections();
            }
            else if (p.getStringAt(1) != this->password)
            {
                this->sendMessageToSocket(networkDataPackage(MessageType::DisconnectedFromGame, QStringList() << "wrongPassword"), relayID);
                this->clearClientConnections();
            }
            else if (!tryAddUser(relayID))
            {
                this->sendMessageToSocket(networkDataPackage(MessageType::DisconnectedFromGame, QStringList() << "badUsername"), relayID);
                this->clearClientConnections();
            }
            else
            {
                this->forwardMessageToClients({}, networkDataPackage(MessageType::UserConnected, QStringList() << p.getStringAt(2)));
                this->sendMessageToSocket(networkDataPackage(MessageType::UserConnected, QStringList() << mainWindow::getConfigVariable("username")), relayID);
                emit this->clientConnected(p.getStringAt(2));
            }
        }
    }

    if (p.getType() == MessageType::DisconnectedFromGame)
    {
        if (this->m == NetworkGameMode::HostMode)
        {
            this->clearClientConnections(false, relayID);
        }
        else if (p.length())
        {
            this->clearClientConnections(false, p.getStringAt(0));
        }
    }
    else if (p.getType() == MessageType::RelayMsg)
    {
        if (p.length() == 2 && p.getStringAt(0) == "userDisconnected")
        {
            this->clearClientConnections(false, p.getStringAt(1));
        }
        else if (p.length() == 3 && p.getStringAt(0) == "publicGameKey")
        {
            emit this->publicGameKeyReceived(p.getStringAt(1), p.getStringAt(2));
        }
        else if (p.length() == 2 && p.getStringAt(0) == "serverinfo")
        {
            if(!mainWindow::getConfigVariable("serverAdminKey").isEmpty())
            {
                emit this->serverInfoReceived(p.getStringAt(1));
            }
        }
        else if (p.length() >= 1 && p.getStringAt(0) == "publicLobbyData")
        {
            emit this->publicLobbyDataReceived(p.getData().mid(1));
        }
        else if (p.length() == 1 && p.getStringAt(0) == "nameAlreadyInUse")
        {
            emit this->nameAlreadyInUse();
        }
        else if (p.length() == 1 && p.getStringAt(0) == "wrongPassword")
        {
            emit this->wrongPassword();
        }
    }
    else if (p.getType() == MessageType::GameEvent)
    {
        if (this->m == NetworkGameMode::HostMode)
        {
            this->forwardMessageToClients(relayID, p);
        }
        emit this->gameEvent(p);
    }
    else if (p.getType() == MessageType::LobbyChatMessage)
    {
        if (p.length() == 2)
        {
            if (this->m == NetworkGameMode::HostMode)
            {
                this->forwardMessageToClients(relayID, p);
            }
            emit this->lobbyChatMessage(p.getStringAt(0), p.getStringAt(1));
        }
    }
    else if (p.getType() == MessageType::LobbyUpdate)
    {
        emit this->lobbyUpdate(p);
        if (this->m == NetworkGameMode::HostMode) this->forwardMessageToClients(relayID, p);
    }
    else if (p.getType() == MessageType::UserConnected && this->m == NetworkGameMode::ClientMode)
    {
        if (p.length() == 1)
        {
            emit this->clientConnected(p.getStringAt(0));
        }
    }

    if (socket->bytesAvailable()) handleNewMessage(s);
}

void network::networkConnectionManager::sendMessage(networkDataPackage package)
{
    if (this->relaySocket->state() != QTcpSocket::ConnectedState) return;
    if (this->m == network::NetworkGameMode::ClientMode)
    {
        package.addRelayTargetAddress(this->relayedHostIP, this->getCurrentPublicGameID(), mainWindow::getConfigVariable("username"));

        QByteArray block;
        QDataStream out(&block, QIODevice::WriteOnly);
        out.setVersion(QDATASTREAM_VERSION);
        out << static_cast<qint64>(0);
        out << package.serialize();
        out.device()->seek(0);
        out << static_cast<quint64>(block.size() - sizeof(qint64));

        this->relaySocket->write(block);
        //if(this->relaySocket->state() == QTcpSocket::ConnectedState) this->relaySocket->waitForBytesWritten(-1);
    }
    else if (this->m == network::NetworkGameMode::HostMode)
    {
        for (const auto& usr : std::as_const(this->relayClients))
        {
            auto pkg_final = package;
            pkg_final.addRelayTargetAddress(usr, this->getCurrentPublicGameID(), mainWindow::getConfigVariable("username"));

            QByteArray block;
            QDataStream out(&block, QIODevice::WriteOnly);
            out.setVersion(QDATASTREAM_VERSION);
            out << static_cast<qint64>(0);
            out << pkg_final.serialize();
            out.device()->seek(0);
            out << static_cast<quint64>(block.size() - sizeof(qint64));

            this->relaySocket->write(block);
            //if(this->relaySocket->state() == QTcpSocket::ConnectedState) this->relaySocket->waitForBytesWritten(-1);
        }
    }
}

void network::networkConnectionManager::sendRelayMsg(const QStringList& data)
{
    network::networkDataPackage package(network::MessageType::RelayMsg, data);

    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDATASTREAM_VERSION);
    out << static_cast<qint64>(0);
    out << package.serialize();
    out.device()->seek(0);
    out << static_cast<quint64>(block.size() - sizeof(qint64));

    if (this->relaySocket && this->relaySocket->state() == QTcpSocket::ConnectedState)
    {
        this->relaySocket->write(block);
        this->relaySocket->waitForBytesWritten(-1);
    }
}

void network::networkConnectionManager::disconnectFromCurrentGame()
{
    this->sendMessage(networkDataPackage(MessageType::DisconnectedFromGame, QStringList() << mainWindow::getConfigVariable("username")));
}

QString network::networkConnectionManager::getCurrentPublicGameID() const
{
    return this->currPubID;
}

void network::networkConnectionManager::sendMessageToSocket(network::networkDataPackage package, const QString& relayID)
{
    package.addRelayTargetAddress(relayID, this->getCurrentPublicGameID(), mainWindow::getConfigVariable("username"));

    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDATASTREAM_VERSION);
    out << static_cast<qint64>(0);
    out << package.serialize();
    out.device()->seek(0);
    out << static_cast<quint64>(block.size() - sizeof(qint64));

    this->relaySocket->write(block);
    //this->relaySocket->waitForBytesWritten(-1);
}

void network::networkConnectionManager::forwardMessageToClients(const QString& origSenderRelay, const network::networkDataPackage& msg)
{
    for (const auto& usr : std::as_const(this->relayClients))
    {
        if (origSenderRelay != usr) this->sendMessageToSocket(msg, usr);
    }
}

void network::networkConnectionManager::reset(bool resetRelay)
{
    this->clearClientConnections(true);
    this->password.clear();
    this->m = NetworkGameMode::ClientMode;
    if (resetRelay) this->resetRelay();
}

network::NetworkGameMode network::networkConnectionManager::getMode() const
{
    return this->m;
}

void network::networkConnectionManager::kickClient(const QString& username)
{
    if (!username.isEmpty())
    {
        this->sendMessageToSocket(networkDataPackage(MessageType::DisconnectedFromGame, QStringList() << "kicked"), username);
        this->clearClientConnections(false, username);
    }
}

void network::networkConnectionManager::sendMessageToUser(const QString& username, const networkDataPackage &pkg)
{
    if (username.size())
    {
        this->sendMessageToSocket(pkg, username);
    }
    else
    {
        mainWindow::reportError(tr("Error: invalid user (%1) in sendMessageToUser(...)").arg(username));
    }
}

void network::networkConnectionManager::connectToRelay(QString addr)
{
    this->resetRelay();
    addr = addr.replace("http://", "").replace("https://", "");
    int port = 3425;
    static const QRegularExpression portRegex{"(.*):([0-9]+)$"};
    auto match = portRegex.match(addr);
    if(match.hasMatch()) {
        addr = match.captured(1);
        port = match.captured(2).toInt();
    }
    this->relaySocket->connectToHost(addr, port);
}

void network::networkConnectionManager::resetRelay()
{
    this->relayedHostIP = "";
    this->relaySocket->abort();
    this->setCurrentPublicGameID({}, {});
}

void network::networkConnectionManager::setCurrentPublicGameID(const QString& ID, const QString& password)
{
    this->currPubID = "gameid" + ID;
    this->sendRelayMsg(QStringList() << "currpublicgameid" << this->currPubID << mainWindow::getConfigVariable("username") << password);
}
