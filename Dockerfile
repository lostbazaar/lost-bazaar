FROM alpine:3.18
RUN apk update
RUN apk add qt6-qtbase-dev
RUN apk add g++ make
COPY src/datapackage.h src/globals.h src/server_main.cpp src/datapackage.cpp /bazaar/src/
COPY bazaar-relay.pro /bazaar/
WORKDIR /bazaar
RUN /usr/lib/qt6/bin/qmake .
RUN make
RUN mkdir /data
EXPOSE 3425
ENTRYPOINT /bazaar/bazaar-relay /data/bazaar-cfg.json
