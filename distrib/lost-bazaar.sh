#!/bin/sh

# Move to script's directory
cd "`dirname "$0"`"

# Get the kernel/architecture information
ARCH=`uname -m`

cd ./x86_64
export LD_LIBRARY_PATH=.
./lost-bazaar --linux-multiarch $@

# Set the libpath and pick the proper binary
#if [ "$ARCH" = "x86_64" ]; then
#    cd ./x86_64
#    export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH
#    ./lost-bazaar --linux-multiarch $@
#else
#    cd ./i686
#    export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH
#    ./lost-bazaar --linux-multiarch $@
#fi
