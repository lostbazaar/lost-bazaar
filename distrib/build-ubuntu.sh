#!/bin/bash
sudo mkdir -p /media/sf_shared/lost-bazaar-$(date -I)-linux/$(uname -m)
sudo cp -aR ~/lost-bazaar/data /media/sf_shared/lost-bazaar-$(date -I)-linux/
sudo cp -aR $1 /media/sf_shared/lost-bazaar-$(date -I)-linux/$(uname -m)/
cd ~/linuxdeployqt
sudo -E ./bin/linuxdeployqt /media/sf_shared/lost-bazaar-$(date -I)-linux/$(uname -m)/lost-bazaar -bundle-non-qt-libs -qmake=$QMAKE_PATH
#sudo rm -rf /media/sf_shared/lost-bazaar-$(date -I)-linux/$(uname -m)/lib
